(function($, exports){
    var contextPath = (window.actx || window.contextPath) || '';
    function Column() {
        var me = this;
        $('#datagrid').datagrid({
            //title: '管理员',
            idField: 'id',
            url: 'list',
//            sortName: 'startTime',
            sortOrder: 'DESC',
            fit: false,
            pagination: true,
            rownumbers: true,
            fitColumns: true,
            border: true,
            striped: true,
            checkOnSelect: true,
            pageSize: 10,
            pageList: [10, 20, 30 ],
            frozenColumns: [
                [
                    {
                        field: 'select',
                        checkbox: true
                    },
                    {
                        field: 'operate',
                        title: '操作',
                        formatter: function () {
                            return '<div class="operateBox"><img onclick="Column.edit(event);" src="' + contextPath + '/images/admin/icon-edit01.png" width="15" height="15" title="Edit" alt="Edit"><img onclick="Column.delete(event);" src="' + contextPath + '/images/admin/icon-delete01.png" width="15" height="15" title="Delete" alt="Delete"></div>';
                        }
                    }
                ]
            ],
            columns: [
                [
                    {
                        field: 'name',
                        title: '栏目名称',
                        width: 100,
                        filter: {
                            editor: 'text',
                            options: {
                                name: 'name'
                            }
                        }
                    },
                    {
                        field: 'href',
                        title: '链接地址',
                        width: 150,
                        align: "center",
                        filter: {
                            editor: 'text',
                            options: {
                                name: 'href'
                            }
                        }
                    },
                    {
                        field: 'remark',
                        title: '备注',
                        width: 150,
                        align: "center",
                        filter: {
                            editor: 'text',
                            options: {
                                name: 'remark'
                            }
                        }
                    },
                   
                    {
                        field: 'lastModifiedDate',
                        title: '上次修改时间',
                        width: 100,
                        align: 'center',
                        formatter: function(value) {
                            var windowsTimestamp = new Date(value);
                            return windowsTimestamp.toLocaleString();
                        }
                    }
                ]
            ]
        }).datagrid('enableFilter');

        $('.operateBar .operate-delete').on('click', function() {
            var rows = $('#datagrid').datagrid('getSelections');
            
            if (rows.length < 1) {
               Besture.Messager.alert('温馨提示', '您必须选择一条记录.');
            } else {
               Besture.Messager.confirm('温馨提示', ['您确定要删除所选的记录？', '如果是，请单击确定，否则单击取消'], function(ret) {
                  if (ret) {
                	  me._doDelete(rows);
                  }
               });
            }
         });
    }
    
    Column.prototype = {
        edit: function(event){
            var index = $(event.target || event.srcElement).closest('tr').attr('datagrid-row-index');
            var record = $("#datagrid").datagrid("getRows")[index];
            window.location.href = contextPath + "/column/edit?id="+ record.id;
        },
        'delete': function(event) {
            var me = this,
                index = $(event.target || event.srcElement).closest('tr').attr('datagrid-row-index'),
                record = $("#datagrid").datagrid('getRows')[index];

            event.stopPropagation && event.stopPropagation() || (event.cancelBubble = true);

            Besture.Messager.confirm('提示', ['您确定要删除选择的记录吗？', '如果是，请点击确定，否则请点击取消'], function(ret) {
                if (ret) {
                    $.ajax({
                        url : contextPath + "/column/delete?id="+ record.id,
                        type : 'post',
                        success: function(data){
                            if(data){
                                $.messager.show({
                                    title : '提示',
                                    msg : '删除成功'
                                });
                                $("#datagrid").datagrid('reload')
                            }else{
                                $.messager.show({
                                    title : '提示',
                                    msg : '删除失败'
                                });
                            }
                        }
                    })
                }
            });
        }
    };
    exports.Column = new Column();
})(jQuery, window);