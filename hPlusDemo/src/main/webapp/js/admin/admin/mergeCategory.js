require(['jquery','layer', 'jquery.validate','jqgrid.custom'], function($,layer) {
	function t(text) { return text; }
	var ctx = window.contextPath || '';
	
	var $grid1 = $('#category_list_grid').jqGrid({
		url: 'categoryList',
        datatype: 'json',
        colNames: [ 'id', '分类名称',  '使用模型','使用模型', '是否可见', '上级分类'/*, '高亮色'*/],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true, align: 'center'},
            {name: 'name', index: 'name' ,template: 'text', align: 'left',searchoptions: {sopt: ["cn"]},
            	formatter:function(value, m, record){
            		var i = 0, pad = '', level = record.depth || 0;
				 	for (i = 0; i < level; i++) {
						pad += '　';
					}
                    return pad + '|-' + value;
    			}
            },
            {name: 'model.id', index: 'model.id', hidden:true},
            {name: 'model.name', index: 'model.name', template: 'text', align: 'center',searchoptions: {sopt: ["cn"]}},
            {name: 'visible', index: 'visible', template: 'bool', align: 'center'},
            {name: 'parent.name', index: 'parent.name', template: 'text', align: 'center',searchoptions: {sopt: ["cn"]}}/*,
            {name: 'color', index: 'color', template: 'text', align: 'center'} */
        ],
        /*
        rownumbers: false,
        treeGrid: true,
        treeModel: 'adjacency',
        ExpandColumn: 'name',
        treeReader: {
            level_field: 'depth',
            parent_id_field: 'parent.id',
            leaf_field: 'isLeaf',
            expanded_field: 'name'
        },
        */
        multiselect: false,
        autowidth: true,
        pager: '#category_list_pagination',
        sortname: 'path',
        sortorder: 'asc',
        shrinkToFit: true,
        onSelectRow:function(id) {
        	var mergeType = $("#mergeType").val();
        	var rowName = $('#category_list_grid').jqGrid("getRowData",id);
        	var html = "";
        	var name = rowName.name;
        	name = name.replace("|-","").trim();
        	html = "<tr>"
        			+"<th width='35%'>分类名称:</th>"
        			+"<td>"+name+"<input type='hidden' name='id"+mergeType+"' value='"+id+"'/></td>"
        			+"</tr>"
        			+"<tr>"
        			+"<th>模型名称:</th>"
        			+"<td>"+rowName['model.name']+"<input type='hidden' name='modelId"+mergeType+"' value='"+rowName['model.id']+"'/></td>"
        			+"</tr>";
        	if (1 == mergeType) {
            	$("#mergeTable").html(html);
        	}
        	if (2 == mergeType) {
            	$("#mergeTable2").html(html);
        	}
        	$grid1.trigger("reloadGrid");
        	layer.closeAll();
        }
    })
	
	$('#chooseCategory,#chooseMergeCategory').click(function (){
		$("#mergeType").val($(this).attr("mergeType"));
		layer.open({
			type: 1 
	  		  ,title: '选择分类'
	  		  ,shade: false
	  		  ,area: ['880px', '550px']
	  		  ,content: $('#category_list_div') //这里content是一个普通的String或html标签
	  		  ,cancel: function(index){
	  			layer.close(index);
	  		  }
		});
		
	})
	
	$(".closeDiv").click(function() {
		$("#merge").hide();
		$(".mask").hide();
		$("#mergeTable").html("");
		$("#mergeTable2").html("");
	});
	
	$("#save").click(function() {
		var id = $("#mergeTable").find("input[name='id1']").val();
		var modelId = $("#mergeTable").find("input[name='modelId1']").val();
		var mergeId = $("#mergeTable2").find("input[name='id2']").val();
		var mergeModelId = $("#mergeTable2").find("input[name='modelId2']").val();
		if (undefined == id || undefined == mergeId) {
			Glanway.Messager.alert("提示", "请选择分类");
		} else if (id == mergeId) {
			Glanway.Messager.alert("提示", "分类相同不能进行合并");
		} else if (modelId != mergeModelId) {
			Glanway.Messager.alert("提示", "模型不同，请先去合并模型");
		} else {
			$.ajax({
				url:"merge",
				type:"post",
				data:{id:id,mergeId:mergeId},
				dataType:"json"
			}).done(function (data) {
                var removed;
                if (data.success) {
                    removed = data.success || [];
                    $("#merge").hide();
            		$(".mask").hide();
            		$("#mergeTable").html("");
            		$("#mergeTable2").html("");
                    $.messager.show({
                        title: '提示',
                        text: '操作成功',
                        sticky: false,
                        time: 3000
                    });
                }else if(data.message){
                    Glanway.Messager.alert("提示", data.message);
                }
            }).fail(function () {
                $.messager.show({
                    title: '提示',
                    text: '操作失败',
                    sticky: false,
                    time: 3000
                });
            });
		}
	})
    
    $(".merge").click(function() {
    	$("#merge").show();
    	$(".mask").show();
    });
	
});