require(['jquery','layer', 'jquery.validate','jqgrid.custom'], function($,layer) {
	function t(text) { return text; }
	var ctx = window.contextPath || '';
	
	var $grid1 = $('#model_list_grid').jqGrid({
		url: 'list',
        datatype: 'json',
        colNames: ['id', '模型名称', '是否启用规格', '是否启用属性', '是否启用参数'],
        colModel: [
            {name: 'id', index: 'id', hidden: true, key: true, align: 'center'},
            {name: 'name', index: 'name', template: 'text', align: 'center'},
            {
                name: 'useSpec', index: 'useSpec', formatter: function (va) {
                if (va) return "已开启"; else return "未开启";
            }, stype: 'select', searchoptions: {value: ':全部;0:未开启;1:已开启'}, align: 'center'
            },
            {
                name: 'useAttribute', index: 'useAttribute', formatter: function (va) {
                if (va) return "已开启"; else return "未开启";
            }, stype: 'select', searchoptions: {value: ':全部;0:未开启;1:已开启'}, align: 'center'
            },
            {
                name: 'useParameter', index: 'useParameter', formatter: function (va) {
                if (va) return "已开启"; else return "未开启";
            }, stype: 'select', searchoptions: {value: ':全部;0:未开启;1:已开启'}, align: 'center'
            }
        ],
        multiselect: false,
        autowidth: true,
        pager: '#model_list_pagination',
        sortname: 'lastModifiedDate',
        sortorder: 'desc',
        shrinkToFit: true,
        onSelectRow:function(id) {
        	var mergeType = $("#mergeType").val();
        	var rowName = $('#category_list_grid').jqGrid("getRowData",id);
        	var html = "";
        	var name = rowName.name;
        	$.ajax({
        		url:"getModel",
        		type:"post",
        		data:{id:id},
        		dataType:"json",
        		async:false,
        		success:function(data){
        			var model = data.model;
        			var html = getModelHtml(model,mergeType,id);
                	if (1 == mergeType) {
                    	$("#mergeTable").html(html);
                	}
                	if (2 == mergeType) {
                    	$("#mergeTable2").html(html);
                	}
                	
                	var modelId = $("#mergeTable").find("input[name=id1]").val();
                	var mergeModelId = $("#mergeTable2").find("input[name=id2]").val();
                	if (undefined != modelId && undefined != mergeModelId) {
                		$.ajax({
                			url:"getAllModel",
                			type:"post",
                			data:{modelId:modelId,mergeModelId:mergeModelId},
                			dataType:"json",
                			success:function(data){
                				var model = data.model;
                				var mergeModel = data.mergeModel;
                				$("#mergeTable").html("");
                		    	$("#mergeTable2").html("");
                				spellModel(model,mergeModel);
                			}
                		})
                		
                		$.ajax({
                			url:"getMergeModel",
                			type:'post',
                			data:{modelId:modelId,mergeModelId:mergeModelId},
                			dataType:"json",
                			success:function(data){
                				var model = data.newModel;
                				var html = getModelHtml(model,null,null);
                				$("#mergeTable3").html(html);
                				$("#newModel").val(model);
                			}
                		})
                	}
					if(null!=data.error){
						Glanway.Messager.alert("提示", data.error);
					}
        		}
        	})
        	layer.closeAll();
        }
    })
	
	$('#chooseModel,#chooseMergeModel').click(function (){
		$('#model_list_grid').trigger("reloadGrid");
		$("#mergeType").val($(this).attr("mergeType"));
		layer.open({
			type: 1 
	  		  ,title: '选择模型'
	  		  ,shade: false
	  		  ,area: ['880px', '550px']
	  		  ,content: $('#model_list_div') //这里content是一个普通的String或html标签
	  		  ,cancel: function(index){
	  			layer.close(index);
	  		  }
		});
		
	})
	
	$(".closeDiv").click(function() {
		closeMergeDiv();
	});
	
	$("#save").click(function() {
		var title = $("#title").val();
		var model = $("#newModel").val();
		var modelId = $("#mergeTable").find("input[name=id1]").val();
    	var mergeModelId = $("#mergeTable2").find("input[name=id2]").val();
		if (null == title || "" == title) {
			Glanway.Messager.alert("提示", "请输入模型名称");
			return;
		}
		if (null == model || "" == model) {
			Glanway.Messager.alert("提示", "请选择好模型");
			return;
		}
		var uploadIndex = layer.load(1, { shade: [0.1,'#000'] });
		$.ajax({
			url:"saveNewModel",
			type:"post",
			data:{"title":title,"modelId":modelId,"mergeModelId":mergeModelId},
			dataType:"json",
			success:function(data){
				layer.close(uploadIndex);
				if ("" != data.error) {
					Glanway.Messager.alert("提示", "模型名称已存在，请重新填写");
				} else {
					Glanway.Messager.alert("提示", "合并成功");
					$('#datagrid').trigger("reloadGrid");
					closeMergeDiv();
				}
			}
		})
	})
    
    $(".merge").click(function() {
    	$("#merge").show();
    	$(".mask").show();
    });
	
	function closeMergeDiv(){
		$("#merge").hide();
		$(".mask").hide();
		$("#title").val("");
		$("#newModel").val("");
		$("#mergeTable").html("");
		$("#mergeTable2").html("");
		$("#mergeTable3").html("");
	}
	
	function getModelHtml(model,mergeType,id) {
		//属性
		var attributes = model.attributes;
		//规格
		var modelSpecs = model.modelSpecs;
		//参数
		var parameters = model.parameters;
		var html = "";
		if (null != mergeType && null != id) {
			html += "<tr>"
				+"<th width:'30%'>模型名称:</th>"
				+"<td>"+model.name+"<input type='hidden' name='id"+mergeType+"' value='"+id+"'/></td>"
				+"</tr>";
		}
		//属性
		if (model.useAttribute) {
			for (var i=0;i<attributes.length;i++){
				var attributeName = "";
				if (0 == i) {
					attributeName = "属性"
				}
				html += "<tr>"
        			+"<th class='fontColor'>"+attributeName+"</th>"
        			+"<td>"+attributes[i].name+"</td>"
        			+"</tr>";
			}
		}
		//规格
		if (model.useSpec) {
			for (var i=0;i<modelSpecs.length;i++){
				var modelSpecName = "";
				if (0 == i) {
					modelSpecName = "规格"
				}
				html += "<tr>"
        			+"<th class='fontColor'>"+modelSpecName+"</th>"
        			+"<td>"+modelSpecs[i].spec.name+"</td>"
        			+"</tr>";
			}
		}

		//参数
		if (model.useParameter) {
			for (var i=0;i<parameters.length;i++){
				var paramHtml = "";
				if (0 == i) {
					paramHtml = "参数";
				}
				html += "<tr >"
	    			+"<th class='fontColor'>"+paramHtml+"</th>"
	    			+"<td>"+parameters[i].name+"</td>"
	    			+"</tr>";
//				for (var j=0;j<parameters[i].children.length;j++) {
//					var parameterName = parameters[i].name;
//					if (0 < j) {
//						parameterName = "";
//					}
//					html += paramHtml+"<tr>"
//            			+"<th>"+parameterName+"</th>"
//            			+"<td>"+parameters[i].children[j].name+"</td>"
//            			+"</tr>";
//					paramHtml = "";
//				}
			}
		}
		return html;
	}
	
	function spellModel(model,mergeModel){
		var html = "";
		var mergeHtml = "";
		
		if (null != model && null != mergeModel) {
			getModelName(model,mergeModel,html,mergeHtml);
			
			getAttribute(model,mergeModel,html,mergeHtml);
			
			getModelSpec(model,mergeModel,html,mergeHtml);
			
			getParamters(model,mergeModel,html,mergeHtml);
		}
	}
	
	//模型名称
	function getModelName(model,mergeModel,html,mergeHtml){
		html += "<tr>"
			+"<th width:'30%'>模型名称:</th>"
			+"<td>"+model.name+"<input type='hidden' name='id1' value='"+model.id+"'/></td>"
			+"</tr>";
		mergeHtml += "<tr>"
			+"<th width:'30%'>模型名称:</th>"
			+"<td>"+mergeModel.name+"<input type='hidden' name='id2' value='"+mergeModel.id+"'/></td>"
			+"</tr>";

    	$("#mergeTable").append(html);
    	$("#mergeTable2").append(mergeHtml);
	}
	
	function getAttributeHtml(attribute) {
		var html = "";
		for (var i=0;i<attribute.length;i++) {
			var attributeName = "";
			if (0 == i) {
				attributeName = "属性"
			}
			html+= "<tr>"
    			+"<th class='fontColor'>"+attributeName+"</th>"
    			+"<td>"+attribute[i].name+"</td>"
    			+"</tr>";
		}
		return html;
	}
	
	//模型属性
	function getAttribute(model,mergeModel,html,mergeHtml){
		//属性
		var attributes = model.attributes;
		var mergeModelAttributes = mergeModel.attributes;
		if (null != attributes && 0 < attributes.length) {
			if (null != mergeModelAttributes && 0 < mergeModelAttributes.length) {
				var attrLength = attributes.length;
				var mergeAttrLength = mergeModelAttributes.length;
				var num = "";
				if (attrLength > mergeAttrLength) {
					num = attrLength * 1 - mergeAttrLength * 1;
					
					mergeHtml += getAttributeHtml(mergeModelAttributes);
					
					for (var i=0;i<num;i++) {
						mergeHtml += "<tr><th></th><td></td</tr>";
					}
					
					html += getAttributeHtml(attributes);
				} else {
					num = mergeAttrLength * 1 - attrLength * 1;
					
					html += getAttributeHtml(attributes);
					
					for (var i=0;i<num;i++) {
						html += "<tr><th></th><td></td</tr>";
					}
					
					mergeHtml += getAttributeHtml(mergeModelAttributes);
				}
			} else {
				for (var i=0;i<attributes.length;i++){
					var attributeName = "";
					if (0 == i) {
						attributeName = "属性"
					}
					html += "<tr>"
	        			+"<th class='fontColor'>"+attributeName+"</th>"
	        			+"<td>"+attributes[i].name+"</td>"
	        			+"</tr>";
					
					mergeHtml += "<tr>"
	        			+"<th class='fontColor'>"+attributeName+"</th>"
	        			+"<td></td>"
	        			+"</tr>";
				}
			}
		} else {
			if (null != mergeModelAttributes && 0 < mergeModelAttributes.length) {
				for (var i=0;i<mergeModelAttributes.length;i++){
					var attributeName = "";
					if (0 == i) {
						attributeName = "属性"
					}
					mergeHtml += "<tr>"
	        			+"<th class='fontColor'>"+attributeName+"</th>"
	        			+"<td>"+mergeModelAttributes[i].name+"</td>"
	        			+"</tr>";
					html += "<tr>"
	        			+"<th class='fontColor'>"+attributeName+"</th>"
	        			+"<td></td>"
	        			+"</tr>";
				}
			}
		}
		
		$("#mergeTable").append(html);
    	$("#mergeTable2").append(mergeHtml);
	}
	
	function getModelSpecHtml(modelSpecs){
		var html = "";
		for (var i=0;i<modelSpecs.length;i++){
			var modelSpecName = "";
			if (0 == i) {
				modelSpecName = "规格"
			}
			html += "<tr>"
    			+"<th class='fontColor'>"+modelSpecName+"</th>"
    			+"<td>"+modelSpecs[i].spec.name+"</td>"
    			+"</tr>";
		}
		return html;
	}
	
	//模型规格
	function getModelSpec(model,mergeModel,html,mergeHtml){
		//规格
		var modelSpecs = model.modelSpecs;
		var mergeModelModelSpecs = mergeModel.modelSpecs;
		
		if (null != modelSpecs && 0 < modelSpecs.length) {
			if (null != mergeModelModelSpecs && 0 < mergeModelModelSpecs.length) {
				var specLength = modelSpecs.length;
				var mergeSpecLength = mergeModelModelSpecs.length;
				var num = "";
				if (specLength > mergeSpecLength) {
					num = specLength * 1 - mergeSpecLength * 1;
					
					mergeHtml += getModelSpecHtml(mergeModelModelSpecs);
					
					for (var i=0;i<num;i++){
						mergeHtml += "<tr><th></th><td></td></tr>"
					}
					
					html += getModelSpecHtml(modelSpecs);
					
				} else {
					num = mergeSpecLength * 1 - specLength * 1;
					
					html += getModelSpecHtml(modelSpecs);
					
					for (var i=0;i<num;i++){
						html += "<tr><th></th><td></td></tr>"
					}
					
					mergeHtml += getModelSpecHtml(mergeModelModelSpecs);
					
				}
			} else {
				for (var i=0;i<modelSpecs.length;i++){
					var modelSpecName = "";
					if (0 == i) {
						modelSpecName = "规格"
					}
					html += "<tr>"
	        			+"<th class='fontColor'>"+modelSpecName+"</th>"
	        			+"<td>"+modelSpecs[i].spec.name+"</td>"
	        			+"</tr>";
					mergeHtml += "<tr>"
	        			+"<th class='fontColor'>"+modelSpecName+"</th>"
	        			+"<td></td>"
	        			+"</tr>";
				}
			}
		} else {
			if (null != mergeModelModelSpecs && 0 < mergeModelModelSpecs.length) {
				for (var i=0;i<mergeModelModelSpecs.length;i++){
					var modelSpecName = "";
					if (0 == i) {
						modelSpecName = "规格"
					}
					mergeHtml += "<tr>"
	        			+"<th class='fontColor'>"+modelSpecName+"</th>"
	        			+"<td>"+mergeModelModelSpecs[i].spec.name+"</td>"
	        			+"</tr>";
					html += "<tr>"
	        			+"<th class='fontColor'>"+modelSpecName+"</th>"
	        			+"<td></td>"
	        			+"</tr>";
				}
			} 
		}

		$("#mergeTable").append(html);
    	$("#mergeTable2").append(mergeHtml);
	}
	
	function getParamterHtml(parameters){
		var html = "";
		for (var i=0;i<parameters.length;i++){
			var paramHtml = "";
			var paramName = "";
			if (0 == i) {
				paramName = "参数";
			}
			html += "<tr >"
    			+"<th class='fontColor'>"+paramName+"</th>"
    			+"<td>"+parameters[i].name+"</td>"
    			+"</tr>";
		}
		return html;
	}
	
	//模型参数
	function getParamters(model,mergeModel,html,mergeHtml){
		//参数
		var parameters = model.parameters;
		var mergeModelParameters = mergeModel.parameters;
		
		if (null != parameters && 0 < parameters.length) {
			if (null != mergeModelParameters && 0 < mergeModelParameters.length) {
				var paramLength = parameters.length;
				var mergeLength = mergeModelParameters.length;
				var num = "";
				if (paramLength > mergeLength) {
					num = paramLength * 1 - mergeLength * 1;
					
					mergeHtml += getParamterHtml(mergeModelParameters);
					
					for (var i=0;i<num;i++) {
						mergeHtml += "<tr >"
			    			+"<th class='fontColor'></th>"
			    			+"<td></td>"
			    			+"</tr>";
					}
					
					html += getParamterHtml(parameters);
					
				} else {
					num = mergeLength * 1 - paramLength * 1;
					
					html += getParamterHtml(parameters);
					
					for (var i=0;i<num;i++) {
						html += "<tr >"
			    			+"<th class='fontColor'></th>"
			    			+"<td></td>"
			    			+"</tr>";
					}
					
					mergeHtml += getParamterHtml(mergeModelParameters);
					
				}
			} else {
				for (var i=0;i<parameters.length;i++){
					var paramHtml = "";
					var paramName = "";
					if (0 == i) {
						paramName = "参数";
					}
					html += "<tr >"
		    			+"<th class='fontColor'>"+paramName+"</th>"
		    			+"<td>"+parameters[i].name+"</td>"
		    			+"</tr>";
					mergeHtml += "<tr >"
		    			+"<th class='fontColor'>"+paramName+"</th>"
		    			+"<td></td>"
		    			+"</tr>";
				}
				
			}
		} else {
			if (null != mergeModelParameters && 0 < mergeModelParameters.length) {
				for (var i=0;i<mergeModelParameters.length;i++){
					var paramHtml = "";
					var paramName = "";
					if (0 == i) {
						paramName = "参数";
					}
					mergeHtml += "<tr >"
		    			+"<th class='fontColor'>"+paramName+"</th>"
		    			+"<td>"+mergeModelParameters[i].name+"</td>"
		    			+"</tr>";
					html += "<tr >"
		    			+"<th class='fontColor'>"+paramName+"</th>"
		    			+"<td></td>"
		    			+"</tr>";
				}
			}
		}

		$("#mergeTable").append(html);
    	$("#mergeTable2").append(mergeHtml);
	}
});