/**
 * 发布商品
 */
require(['jquery', 'mustache', 'treepicker', 'uploader2'], function ($, Mustache, TreePicker, Uploader2) {
	var contextPath = (window.Glanway || window).contextPath || '';
    var adminContextPath = (window.actx || '');
    // 属性条目模板
    var ATTR_TPL = '<tr>' +
            '   <th align="right">{name}：</th>' +
            '   <td style="text-align:left;">' +
            '       <input type="hidden" name="attributeValues[].attribute.id" value="{id}" />' +
            '   </td>' +
            '</tr>',
    // 参数组模板
        PARAMS_GROUP_TPL = '<tr><th colspan="2" style="text-align: left; padding-left: 20px;">{name}</th></tr>',
    // 参数模板
        PARAMS_TPL = '<tr>' +
            '<th align="right">{name}：</th>' +
            '<td>' +
            '<input type="hidden" name="parameterValues[{index}].parameter.id" value="{id}">' +
            '<input type="text" name="parameterValues[{index}].value" maxlength="50">' +
            '</td>' +
            '</tr>',
        SPECS_TPL = $('#tpl-specs').html(),
        SPECS_TPL_IMG = $('#tpl-specs-img').html(),
        GOODS_TPL = $('#tpl-goods').html();

    /**
     * 更新品牌数据 DOM 元素
     * @param brands
     */
    function updateBrandsUI(brands) {
        brands = brands || [];

        var $select = $('select[name="brand.id"]'), i;

        $select.empty();
        for (i = 0; i < brands.length; i++) {
            $select.append(new Option(brands[i].name, brands[i].id));
        }
        $select.triggerHandler('chosen:updated')
    }

    /**
     * 更新作品数据 DOM 元素
     * @param works
     */
    function updateWorksUI(works) {
    	works = works || [];
    	
    	var $select = $('select[name="works.id"]'), i;
    	
    	$select.empty();
    	for (i = 0; i < works.length; i++) {
    		$select.append(new Option(works[i].name, works[i].id));
    	}
    	$select.triggerHandler('chosen:updated')
    }

    /**
     * 更新商品模型 DOM 元素
     *
     * @param model
     */
    function updateProductModelUI(model) {
        var attrs = null != model && model.useAttribute ? model.attributes : null,
            params = null != model && model.useParameter ? model.parameters : null,
            modelSpecs = null != model && model.useSpec ? model.modelSpecs : null;

        updateAttrsUI(attrs);
        updateParamsUI(params);
        updateModelSpecsUI(modelSpecs);
    }

    /**
     * 重新渲染属性 UI
     */
    function updateAttrsUI(attrs) {
        var $attrContainer = $('#attr-list'),
            type, values, $tr, $valueContainer, $el, i, j;

        // 重新渲染属性
        $attrContainer.empty();
        attrs = attrs || [];
        for (i = 0; i < attrs.length; i++) {
            type = attrs[i].displayType;
            values = attrs[i].attributeValues;
            $tr = $(render(ATTR_TPL, attrs[i]));
            $valueContainer = $tr.find('td');

            // 选择类型
            if (0 == type || 2 == type) {
                $el = $('<select name="attributeValues[].id" data-placeholder="请选择">');
                for (j = 0; j < values.length; j++) {
                    $el.append(new Option(values[j].value, values[j].id));
                }
            } else {
                // 自定义类型
                $el = $('<input type="text" name="attributeValues[].value">');
            }
            $valueContainer.append($el);
            $attrContainer.append($tr);
        }

        // 替换属性名索引, attributeValues[].id --> attributeValues[1].id
        // 起始索引, 需要加上基本模型中属性的索引
        i = +$attrContainer.attr('data-index') || 0;
        $attrContainer.find('tr').each(function (k, item) {
            $(item).find('input[name],select[name]').each(function (m, input) {
                input.name = input.name.replace(/^attributeValues\[\]/, 'attributeValues[' + (i + k) + ']');
            });
        });
        $attrContainer.find('select').chosen({allow_single_deselect: true});
    }

    /**
     * 更新参数 UI
     * @param params
     */
    function updateParamsUI(params) {
        var $paramsContainer = $('#param-list'), children, i, j, count;

        $paramsContainer.empty();
        params = params || [];

        count = 0;
        // 参数组
        for (i = 0; i < params.length; i++) {
            $paramsContainer.append($(render(PARAMS_GROUP_TPL, params[i])));

            // 参数
            children = params[i].children;
            children = children || [];
            for (j = 0; j < children.length; j++, count++) {
                children[j].index = count;
                $paramsContainer.append($(render(PARAMS_TPL, children[j])))
            }
        }
    }

    /**
     * 更新模型规格 UI
     *
     * @param modelSpecs
     */
    function updateModelSpecsUI(modelSpecs) {
        var $enableSpecBtn = $('input[name=enableSpecs]'),
            $specList = $('#spec-list'),
            specs = [];
        $specList.empty();

        modelSpecs = modelSpecs || [];
        $.each(modelSpecs, function (i, el) {
            el.spec.specId = el.spec.id; // for tpl
            // 是否影响货号
            el.spec['valid'] = el['isAffectGoods'] ? 1 : 0;
            /*$enableSpecBtn.closest('tr').show();*/
            if(el.spec['displayType'] === 1){
                $specList.append(Mustache.render(SPECS_TPL_IMG, el.spec));
            }else{
                $specList.append(Mustache.render(SPECS_TPL, el.spec));
            }
            parseSelectAll();
            specs.push(el.spec);
        });

        if (specs.length > 0) {
        	$enableSpecBtn.closest("tr").show();
            showUseSpec();
        } else {
        	$enableSpecBtn.removeAttr("checked").val(0).closest("tr").hide();
            showNoSpec();
        }
        window.specs = specs;
        if (!$enableSpecBtn.is(":checked")) {
        	showNoSpec();
        }
        
    }

    function render(tpl, data) {
        for (var p in data) {
            if (data.hasOwnProperty(p)) {
                tpl = tpl.replace(new RegExp('{' + p + '}', 'g'), data[p]);
            }
        }
        return tpl;
    }

    // 分类选择
    $('#category').treepicker({
        multiple: false
        , rootProp: 'data'
        , valueProp: 'id'
        , textProp: 'name'
        , nodeParam: 'search_parent.id_L'
        , rootId: ''
        , data: adminContextPath + '/category/categoryList?nolist=nolist'
        , extraParams: function (node) {
            var params = {};
            if ('#' == node.id) {
                params['search_parent.id_NU_L'] = '';
            }
            return params;
        }
        , setValue: function (value, node) {
            var $midEl = $('input[name="model.id"]'),
                mid = $midEl.val();

            $('#category').val(value[0] || '');
//            $('#categoryName').val(value[0] || '');
            if (0 == value.length) {
              /*  updateBrandsUI(null);
                updateProductModelUI(null);*/
                $midEl.val('');
                $('#mname').text('');
                return;
            }
            
            /*$.ajax({url: adminContextPath + '/category/detail?id=' + value[0]}).done(function (data) {
                updateBrandsUI(data && data.brands);
                updateWorksUI(data && data.works);
            });*/
            $.ajax({url: adminContextPath + '/model/detail/' + node[0].model.id}).done(function (data) {
                if (data.success) {
                    var model = data.data;

                    // 模型发生变化
                    if (model.id != mid) {
                        $midEl.val(node[0].model.id);
                        $('#mname').text(node[0].model.name);
                        $('#modelNameId').val(node[0].model.name);
                        window.model = model;
                        updateProductModelUI(model);
                    }
                }
            })
        }
    });
    
    var $form = $('#product-form');
    $form.validate({
        "ignore": ":hidden:not(.ni)",
        rules: {
        	  "category.id": {required: true}
    		, statusForValidate :{required: true}
    		/*, purchaseScopeForValidate :{required: true}*/ //update20160616 wentan 购买范围默认选中“零售”,且不可取消，所以不必再做必填验证
            , "brand.id": {required: true}
            , title: {required: true, maxlength: 250}
            , image: {required: true,maxlength: 250}
            , code: {required: true,maxlength: 250
                //,
            	//remote: {
            	//	url: adminContextPath + "/product/checkIsCodeExist",
            	//	type: "post",
            	//	data: {
            	//		id: function(){return $("input[name='id']").val() || "";},
            	//		code: function() {return $("input[name='code']").val();}
            	//	}
            	//}
               }
            , boxNumber: {maxlength: 10,digits: true}
            , reSellingNum: {maxlength: 10,digits: true}
            , designer: {maxlength: 250}
            , nameJp: {maxlength: 250}
            , price: {required: true,number: true, min: 0, max: 100000}
            , marketPrice: {required: true, number: true, min: 0, max: 100000}
            , sort: {required: true,digits: true, min:0, max:100000}
            , seoTitle: {maxlength: 240 }
            , seoKeywords: {maxlength: 900}
            , seoDescribe: {maxlength: 900}
        },
        messages: {
        	"category.id": {required: "请选择商品分类"}
          , statusForValidate :{required: "请至少选择一项商品状态"}
         /* , purchaseScopeForValidate :{required: "请至少选择一项购买范围"}*/
          , "brand.id": {required: "请选择品牌"}
          , title: {required: "请输入商品名称", maxlength:"输入的最大长度为{0}字符"}
          , image: {required: "请上传商品图片"}
          , code: {required: "请输入商品编号",maxlength:"输入的最大长度为{0}字符"
                //,remote:"该商品编号已存在"
            }
          , boxNumber: {maxlength: "输入的最大长度为{0}",digits: "只能输入整数"}
          , reSellingNum: {maxlength: "输入的最大长度为{0}",digits: "只能输入整数"}
          , designer: {maxlength: "输入的最大长度为{0}字符"}
          , nameJp: {maxlength: "输入的最大长度为{0}字符"}
          , price: {required: "请输入日元成本价格",number: "只能输入数字"}
          , marketPrice: {required: "请输入人民币销售价格", number: "只能输入数字"}
          , sort: {required: "请输入排序",digits: "只能输入整数"}        	
        },
        submitHandler: function(form) {
        	$("#product-form input[type='submit']").attr("disabled","disabled");
        	form.submit();
        }
    });
    
    //$.validator.addMethod("isCodeExist", function(value, element) {
    //    var exists = 0;
    //    var flag = true,
    //    	$curGoodsTr = $(element).closest("table").parents("tr"),
    //    	curEleVal = $(element).val();
    //    $curGoodsTr.siblings().each(function(i, el) {
    //    	if ($(el).find(".isCodeExist").val() == curEleVal) {
    //    		exists = true;
    //    		flag = false;
    //    		return false;
    //    	}
    //    });
    //    if(flag) {
    //    	var curGoodsId = $(element).closest("tr").find("#goodsId").val() || null;
    //        $.ajax({
    //            url: adminContextPath + '/product/checkIsGoodsCodeExist',
    //            type: 'post',
    //            async: false,
    //            data: {goodsId : curGoodsId, code: value},
    //            dataType: 'json',
    //            success: function(data) {
    //                exists = data.isExist;
    //            }
    //        });
    //    }
    //    return this.optional(element) || !exists;
    //},"该编码已存在");
    
    var $tabs = $('.tab-wrap ul li'),
        $tabCnts = $('.tab1');
    // 下一步
    $('.btn-next').click(function () {
        validateStatus();
        var validator = $form.validate(),
            errorElement, index, $target;
        if (!validator.checkForm() || !imgValidate()) {
            validator.showErrors();

            // 切换到错误的标签页
            errorElement = validator.errorList[0];
            errorElement = errorElement && errorElement.element;
            if (errorElement) {
                index = $tabCnts.index($(errorElement).closest('.tab1'));
                $tabs.eq(index).addClass('on').siblings().removeClass('on');
                $tabCnts.eq(index).show().siblings().hide();
            }
        } else {
            index = $tabs.filter('.on').index();

            // 如果下一个非禁用元素, 则切换, 否则提交表单
            $target = $tabs.eq(index).nextAll(':not(.disabled):first');
            if (0 < $target.length) {
                $target.addClass('on').siblings().removeClass('on');
                $tabCnts.eq($target.index()).show().siblings().hide();
            } else {
                $form.submit();
            }
        }
        return false;
    });

    // 上一步
    $('.btn-prev').click(function () {
        var index = $tabs.filter('.on').index(),
            $target = $tabs.eq(index).prevAll(':not(.disabled):first');

        // 如果存在上一个未禁用的标签则切换, 否则已经是第一个标签, 则取消操作
        if (0 < $target.length) {
            $target.addClass('on').siblings().removeClass('on');
            $tabCnts.eq($target.index()).show().siblings().hide();
        } else {
            history.go(-1);
        }
        return false;
    });

    // 启用规格
    $('#btn-enable-spec').on('change', function () {
    	var checked = $("#btn-enable-spec").is(":checked");
        if (!checked) {
        	showNoSpec();
        } else {
        	showUseSpec();
        }
        reserveDeadlineAddValidate();
        reserveNumAddValidate();
    });

    $('.btn-gen-goods').click(function () {
        var specs = window.specs || [],i;
        $('#goods-list').empty();
        
        specValues = getSelectedSpecValues();
        
        goods = [];
        if (specValues.length > 0) {
            // 计算所有的规格值组合类型
            specValues = cartesianProduct.apply(this, specValues) || [];
            // 遍历所有组合

            for (i = 0; i < specValues.length; i++) {
                var code = ($('[name=code]').val() || ''),
                    sorted = [];

                // 排序并添加索引
                for (j = 0; j < specValues[i].length; j++) {
                    var value = specValues[i][j];   // 规格值
                    value.index = j;     // for template
                    // 按照规格排序
                    for (k = sorted.length - 1; k > -1; k--) {
                        if (value.spec.sort >= sorted[k].spec.sort) {
                            sorted.splice(k - 1, k -1, value);
                            break;
                        }
                    }
                    if (k < 0) {
                        sorted.unshift(value);
                    }
                }

                // 按照顺序生成 code (product 8位 code + 8位 规格值 code)
                code = code + "" + $.map(sorted, function(item) { 
                		/*return item.code || ''*/ 
	                	if(item.spec.valid == 1){
	                		return item.code || ''
	                	}
                	}).join('');

                goods.push({
                    title: $('[name=title]').val(),
                    specValues: specValues[i],
                    code: code,
                    marketPrice: $('[name=marketPrice]').val(),
                    stock: $('[name=stock]').val(),  //product的form中没有该input，20160427，wentan
                    stockForewarn: $('[name=stockForewarn]').val(),//product的form中没有该input，20160427，wentan
                    providerList: window.providerList,
                    goodsIndex: i       // for template
                });
            }
            $('#goods-list').html(Mustache.render(GOODS_TPL, goods));
            $('.chosen-select').chosen({allow_single_deselect: true});
            // 拷贝图片
            $(".zc .cz .upload-list").each(function (i) {
                var cao=$(this).children("li");
                if (null!=cao&& 0 <cao.length) {
                    var rmm=new Array();
                    cao.each(function (c) {
                        if($(this).children("li").children().children('input[name="productImgs['+c+'].path"]').val()==null) {
                            if($(this).children("li").children('input[name="productImgs[' + c + '].path"]').val()!=null)
                            rmm[c] = $(this).children("li").children('input[name="productImgs[' + c + '].path"]').val();
                        }
                        else{
                            rmm[c]=$(this).children("li").children().children('input[name="productImgs['+c+'].path"]').val();
                        }


                    });
                    if(rmm.length>0) {
                        $('#goods-list .upload-list').each(function (r) {
                            var n = $(this).children("li");
                            n.each(function (n) {
                                  if(n+1<=rmm.length) {
                                      var cc = '<div data-saved-url="' + $(".meimeide").val() + '/' + rmm[n] + '">';
                                      cc += '<input type="hidden" name="goods[' + r + '].productImgs[' + n + '].path" value="'+rmm[n]+'">';
                                      cc += '</div>';
                                      $(this).append(cc);
                                  }
                            });
                        });
                    }
                }
            });
            //把product的日元成本价拷贝给goods
            $(".goodsprice").val($(".productprice").val());
            /**
             * 绑定上传
             */
            // parseUpload();
            
            $('#goods-list .upload-list').each(function (index, queue) {
            	$(queue).children('li').each(function (idx, up) {
            		if (up.uploader) {
            			return;
            		}
                    Uploader2({
                        browse_button:up ,
                        url: contextPath + '/img/preupload',
                        policy: true,
                        name: 'goods[' + index + '].productImgs[' + idx + '].path',
                        list: 'productImgs'+index+'-img'+idx,
                        filters: {mime_types: [{title: 'Allowd Files', extensions: 'jpg,png,gif'}]},
                        mode: 't',
                        max_file_count: '1',
                        max_file_size: '1m'
                    });
            	});
            })
        }

        //根据商品状态添加相应的验证信息
        reserveDeadlineAddValidate();
        reserveNumAddValidate();
    });

    /**
     * 获取所有选择的规格值对象数组
     */
    function getSelectedSpecValues() {
        var specs = window.specs,
            specValues = [],
            values = [], i;

        // 遍历所有的规格
        for (i = 0; i < specs.length; i++) {
            values = [];
            // 遍历每个规格选择的值, 获取其选择值的对象
            $('[name=' + specs[i].id + ']:checked').each(function (i, el) {
                values.push(getSpecValue(el.name, el.value));
            });

            if (values.length > 0) {
                specValues.push(values);
            }
        }

        return specValues;
    }

    function getSpecValue (specId, specValueId) {
        var me = this, ret;

        // 从规格列表中查找规格
        $.each(me.specs, function (i, spec) {
            if (specId == spec.id) {
                // 查找该规则值
                $.each(spec.specValues, function (i, value) {
                    if (specValueId == value.id) {
                        ret = value;
                        value.spec = spec;
                        return false;
                    }
                });
                return false;
            }
        });
        return ret;
    }

    /**
     * 计算的笛卡尔积
     * @param arguments 要生成组合的数组
     */
    function cartesianProduct () {
        function addTo(curr, args) {
            var i,
                copy,
                rest = args.slice(1),
                last = !rest.length,
                result = [];

            for (i = 0; i < args[0].length; i++) {
                copy = curr.slice();
                copy.push(args[0][i]);
                if (last) {
                    result.push(copy);
                } else {
                    result = result.concat(addTo(copy, rest));
                }
            }
            return result;
        }
        return addTo([], Array.prototype.slice.call(arguments));
    }

    $('.btn-clear-goods').click(function () {
        $('#goods-list').empty();
    });

    if (window.modelSpecs) {
        debugger;
        var selected = (window.selectedSpecValueIds || '').split(','),i ;
        updateModelSpecsUI(window.modelSpecs);

        for (i = 0; i < selected.length; i++) {
            if (selected[i]) {
                $('#spec-list').find('input:checkbox[value="' + selected[i] + '"]').attr("checked", "checked");
            }
        }
    }
});

//不启用规格时显示页面
function showNoSpec() {
	$('#createGoodsDiv').hide();
    $('#spec-list').hide();
    $('#createGoodsThead').hide();
    $('#no-spec').show();
    $("input[name='enableSpecs']").val(0);
    $("#no-spec :input[type='text'], #no-spec select").each(function(i,el) {
    	$(el).removeAttr("disabled");
    });
    $("#goods-list").hide().find("input,select").each(function(i, el) {
    	$(el).attr("disabled","true");
    })
}

//启用规格时的页面
function showUseSpec() {
	$('#createGoodsDiv').show();
    $('#spec-list').show();
    $('#createGoodsThead').show().parent().show();
    $('#goods-list').show();
    $('#no-spec').hide();
    $("input[name='enableSpecs']").val(1);
    $("#no-spec :input[type='text'], #no-spec select").each(function(i,el) {
    	$(el).attr("disabled","true");
    });
    $("#goods-list").find("input,select").each(function(i, el) {
    	$(el).removeAttr("disabled");
    })
}

function reserveDeadlineAddValidate() {
	if ($("input[name='reserved']").is(":checked")) {
		$("input[name*='reserveDeadline']").addClass("required");
	} else {
		$("input[name*='reserveDeadline']").removeClass("required");
	}
}

function reserveNumAddValidate() {
	if ($("input[name='limited']").is(":checked")) {
		$("input[name*='reserveNum']").addClass("required");
	} else {
		$("input[name*='reserveNum']").removeClass("required");
	}
}

function validateStatus(){
	$("#statusTd :checkbox").each(function(i,el){
		if($(el).is(":checked")){
			$("input[name='statusForValidate']").val("1").next(".error").empty();
			return false;
		}
		$("input[name='statusForValidate']").val("");
	});
}

function imgValidate() {
	var imgMsg = '<span class="imgMsg" style="position: absolute;color: red;top:45% ;left:23%;font-weight: 900">请上传图片</span>';
	var src;
	var flag = true,
		$img = $("#primary-queue");
	src = $img.find("input:hidden").val();
	$img.children(".imgMsg").remove();
	if(undefined == src || '' == src) {
		$(imgMsg).appendTo($img);
		flag = false;
	}
	return flag;
}
/*function validatePurchaseScope(){
	$("#purchaseScopeTd input:checkbox").each(function(i,el){
		if($(el).is(":checked")){
			$("input[name='purchaseScopeForValidate']").val("1").next(".error").empty();
			return false;
		}
		$("input[name='purchaseScopeForValidate']").val("");
	});
}*/
