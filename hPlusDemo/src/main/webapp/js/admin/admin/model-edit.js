;require(['jquery', 'mustache', 'jquery.validate', 'jqgrid.custom', 'layer'], function($, Mustache) {
    var actx = (window.actx) || '',
        DEL_MODEL_SPEC_URL = actx + '/model/delModelSpec',
        exports = window;

    var ModE = function() {
        var me = this;

        me.$attrList = $('#attr-list');
        me.attrTpl = $('#tpl-attr').html();

        me.$specList = $('#spec-list');
        me.specTpl = $('#tpl-spec').html();

        me.$paramGroupList = $('#param-group-list');
        me.paramGroupTpl = $('#tpl-param-group').html();

        me.paramTpl = $('#tpl-param').html();

        me.$specGrid = $('#spec-grid');

        /*
        me.$specDialog = $('#spec-sel-dlg').dialog({
            title: '选择规格',
            modal: true,
            closed: true,
            width: 600,
            height: 400,
            onOpen: function() {
                if (!me.$specGrid.data('datagrid')) {
                    me.$specGrid.datagrid({
                        url: contextPath + '/admin/spec/list',
                        singleSelect: true,
                        idField: 'id',
                        columns: [[
                            { field: 'name', title: '规格名称', sortable: true, filter: 'text', width: 100 },
                            { field: 'alias', title: '规格别名', sortable: true, filter: 'text', width: 100 }
                        ]],
                        onClickRow: $.proxy(me.addSpec, me)
                    }).datagrid('enableFilter');
                }
            }
        });
        */

        Mustache.parse(me.attrTpl);
        Mustache.parse(me.specTpl);
        Mustache.parse(me.paramGroupTpl);
        Mustache.parse(me.paramTpl);

        me.bindEvent();
        me.refresh();
    };

    ModE.prototype = {
        bindEvent: function() {
            var me = this;

            $('[name=useAttribute]').change($.proxy(me.refresh, me));
            $('[name=useSpec]').change($.proxy(me.refresh, me));
            $('[name=useParameter]').change($.proxy(me.refresh, me));

            $('#btn-cat-sel').click(function () { me.$catsDialog.dialog('open'); });

            $("#btn-add-attr").click($.proxy(me.addAttr, me));
            // $('#btn-add-spec').click($.proxy(me.addSpec, me));
            $('#btn-add-spec').click(function() {
                //me.$specDialog.dialog('open');
                layer.open({
                    zIndex: 10,
                    title: '添加规格值',
                    type: 1,
                    skin: 'layui-layer-rim', //加上边框
                    area: ['600px', '400px'], //宽高
                    shadeClose: true,
                    content: $('#spec-sel-dlg'),
                    btn: ['确定', '取消'],
                    cancel: function (index) {
                        layer.close(index);
                        //$dlg.remove();
                    },
                    yes: function (index) {
                        var keys = $g.jqGrid('getGridParam', 'selarrrow'), i;
                        for (i = 0; i < keys.length; i++) {
                            var row = $g.jqGrid('getRowData', keys[i]);
                            me.addSpec(i, row);
                        }
                        layer.close(index);
                    }
                });
                var $g = $('#spec-grid').jqGrid({
                    url: actx + '/spec/list',
                    datatype: 'json',
                    colNames: ['ID', '规格名称', '规格别名', '排序'],
                    colModel: [
                        {name: 'id', index: 'id', hidden: true, key: true, align: 'center'},
                        {name: 'name', index: 'name', template: 'text', align: 'center'},
                        {name: 'alias', index: 'alias', template: 'text', align: 'center'},
                        {name: 'sort', index: 'sort', template: 'text', align: 'center'}
                    ],
                    multiselect: true,
                    autowidth: true,
                    shrinkToFit: true,
                    height: '100%',
                    pager: '#spec-pagination',
                    sortname: 'lastModifiedDate',
                    sortorder: 'desc'
                });
            });
            $('#btn-add-param-group').click($.proxy(me.addParamGroup, me));
            me.rebindSpecRemoveEvent();
            me.rebindParamEvent();
        },

        rebindSpecRemoveEvent: function() {
            var me = this;
            me.$specList.find('tr').each(function(i, tr) {
                $(tr).find('img:last').off('click').on('click', function() {
                    me.removeSpec($(this).closest('tr'));
                });
            });
        },

        removeSpec: function($entry) {
            var id = $entry.data('id');
            Glanway.Messager.confirm("警告", "您确定要删除选择的行记录吗？",function(ret){
                if(ret){
                    if (!id) {
                        $entry.remove();
                    } else {
                        $.ajax({
                            url: DEL_MODEL_SPEC_URL,
                            data: {id: id},
                            dataType: 'json',
                            success: function(data) {
                                if (data.success) {
                                    $entry.remove();
                                } else {
                                	/* 异常处理 20160429 wentan */
                                	Glanway.Messager.alert("提醒","该规格在商品中已使用，不能进行删除");
                                    /*$.messager.show({ 
                                        title: '错误',
                                        msg: data.message || '未知错误'
                                    });*/
                                }
                            },
                            error: function() {
                            	Glanway.Messager.alert("提醒","删除失败");
                                /*$.messager.show({
                                    title: '错误',
                                    msg: '删除失败'
                                });*/
                            }
                        });
                    }
                }})
        },

        refresh: function() {
            var me = this,
                enableAttr = +$('[name=useAttribute]:checked').val(),
                enableSpec = +$('[name=useSpec]:checked').val(),
                enableParam = +$('[name=useParameter]:checked').val(),
                $attrTab = $('.tab-wrap>.tab>li:eq(1)'),
                $specTab = $('.tab-wrap>.tab>li:eq(2)'),
                $paramTab = $('.tab-wrap>.tab>li:eq(3)');

            if (enableAttr) {
                $attrTab.show();
            } else {
                me.$attrList.html('');
                $attrTab.hide();
            }

            if (enableSpec) {
                $specTab.show();
            } else {
                me.$specList.html('');
                $specTab.hide();
            }

            if (enableParam) {
                $paramTab.show();
            } else {
                me.$paramGroupList.html('');
                $paramTab.hide();
            }
        },

        addAttr: function() {
            var me = this;
            me.$attrList.append(Mustache.render(me.attrTpl));
            // 修正索引
            me.$attrList.find('tr').each(function (i, tr) {
                $(tr).find('input[name]:enabled,select[name]:enabled,textarea[name]:enabled').each(function (j, input) {
                    var $input = $(input);
                    $input.attr('name', $input.attr('name').replace(/[[0-9]*]/, '[' + i + ']'));
                });
            });
        },
        addSpec: function(index, row) {
            var me = this;

            if (me.$specList.find('tr[data-id="' + row.id + '"]').length > 0) {
                return;
            }

            me.$specList.append(Mustache.render(me.specTpl, row));

            me.$specList.find('tr').each(function (i, tr) {
                $(tr).find('input[name]:enabled,select[name]:enabled,textarea[name]:enabled').each(function (j, input) {
                    var $input = $(input);
                    $input.attr('name', $input.attr('name').replace(/[[0-9]*]/, '[' + i + ']'));
                });
            });

            // me.$specDialog.dialog('close');
            me.rebindSpecRemoveEvent();
        },
        addParamGroup: function() {
            var me = this,
                $group = $(Mustache.render(me.paramGroupTpl));

            me.$paramGroupList.append($group);
            /*
            $group.find('button').click(function() {
                var $paramList = $group.find('tbody'),
                    groupName = $group.find('input[name]:enabled:first').attr('name') || '',

                groupName = groupName.substring(0, groupName.indexOf("."));

                $paramList.append(Mustache.render(me.paramTpl));
                $paramList.find('tr').each(function (i, tr) {
                    $(tr).find('td>input[name]:enabled,td>select[name]:enabled,td>textarea[name]:enabled').each(function (j, input) {
                        var $input = $(input);
                        $input.attr('name', groupName + '.children[' + i + '].name');
                    });
                });

            });
             */
            me.rebindParamEvent();

            me.$paramGroupList.children('div').each(function (i, el) {
                $(el).find('input[name]:enabled,select[name]:enabled,textarea[name]:enabled').each(function (j, input) {
                    var $input = $(input);
                    $input.attr('name', $input.attr('name').replace(/[[0-9]*]/, '[' + i + ']'));
                });
            });
        },

        rebindParamEvent: function() {
            var me = this;
            // 参数组事件绑定
            me.$paramGroupList.find('thead').each(function(i, thead) {
                var $group = $(thead).closest('div');
                // 添加参数
                $(thead).find('tr button:first').off('click').on('click',function() {
                    var $paramList = $group.find('tbody'),
                        groupName = $group.find('input[name]:enabled:first').attr('name') || '',

                        groupName = groupName.substring(0, groupName.indexOf("."));

                    $paramList.append(Mustache.render(me.paramTpl));
                    $paramList.find('tr').each(function (i, tr) {
                        $(tr).find('td>input[name]:enabled,td>select[name]:enabled,td>textarea[name]:enabled').each(function (j, input) {
                            var $input = $(input);
                            var flag=$input.attr("name").indexOf("id");
                            if(flag<0){
                                $input.attr('name', groupName + '.children[' + i + '].name');
                            }
                        });
                    });
                });
                // 删除参数组
                $(thead).find('tr button:last').off('click').on('click', function() {me.removeParamGroup($group); });
            });

            // 参数删除事件
            me.$paramGroupList.find('tbody').each(function(i, tbody) {
                var $entry = $(tbody).find('tr');
                $entry.find('button:last').off('click').on('click', function() {
                    me.removeParam($entry[i]);
                });
            });
        },

        removeParamGroup: function($entry) {
            Glanway.Messager.confirm("警告", "您确定要删除选择的行记录吗？",function(ret){
                var referenced = $('input[name=referenced]').val();
                referenced = referenced === 'false' ? false : !!referenced;
                if(ret){
                    if (referenced) {
                        $.messager.show({
                            title: '错误',
                            msg: "此记录有关联数据，无法执行删除操作"
                        });
                        return;
                    }
                    $entry.remove();
                }})
        },


        removeParam: function($entry) {
            Glanway.Messager.confirm("警告", "您确定要删除选择的行记录吗？",function(ret){
                var referenced = $('input[name=referenced]').val();
                referenced = referenced === 'false' ? false : !!referenced;
                if(ret){
                    if (referenced) {
                        $.messager.show({
                            title: '错误',
                            msg: "此记录有关联数据，无法执行删除操作"
                        });
                        return;
                    }
                    $entry.remove();
                }});
        }
    };

    exports.modelEdit = new ModE();

    var $tabs = $('.tab-wrap ul li'),
        $tabCnts = $('.tab1'),
        $form = $('#model-form');
    // 下一步
    $('.btn-next').click(function () {
        var validator = $form.validate(),
            errorElement, index, $target;
        if (!validator.checkForm()) {
            validator.showErrors();

            // 切换到错误的标签页
            errorElement = validator.errorList[0];
            errorElement = errorElement && errorElement.element;
            if (errorElement) {
                index = $tabCnts.index($(errorElement).closest('.tab1'));
                $tabs.eq(index).addClass('on').siblings().removeClass('on');
                $tabCnts.eq(index).show().siblings().hide();
            }
        } else {
            index = $tabs.filter('.on').index();

            // 如果下一个非禁用元素, 则切换, 否则提交表单
            $target = $tabs.eq(index).nextAll(':not(:hidden):first');
            if (0 < $target.length) {
                $target.addClass('on').siblings().removeClass('on');
                $tabCnts.eq($target.index()).show().siblings().hide();
            } else {
                $form.submit();
            }
        }
        return false;
    });

    // 上一步
    $('.btn-prev').click(function () {
        var index = $tabs.filter('.on').index(),
            $target = $tabs.eq(index).prevAll(':not(:hidden):first');

        // 如果存在上一个未禁用的标签则切换, 否则已经是第一个标签, 则取消操作
        if (0 < $target.length) {
            $target.addClass('on').siblings().removeClass('on');
            $tabCnts.eq($target.index()).show().siblings().hide();
        } else {
            history.go(-1);
        }
        return false;
    });
});
