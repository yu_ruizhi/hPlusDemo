require(['jquery', 'layer', 'jquery.validate', 'jqgrid.custom'], function($, layer) {
    $('#flashSale-form').validate({
        "ignore" : ":hidden:not(.ni)",
        "rules" : {
            "name": {
                "required": true,
                "maxlength": 255
            },
            "tag" : {
                "minlength" : 0,
                "maxlength" : 255
            },
            "way" : {
                "required" : true
            },
            "discount" : {
                "minlength" : 0,
                "maxlength" : 8,
                "number" : true
            },
            "descend" : {
                "minlength" : 0,
                "maxlength" : 8,
                "number" : true
            },
            "limitQty" : {
                "digits": true,
                "min": 1,
                "max": 999
            },
            "startTime": {
                required: true
            },
            "endTime": {
                required: true
            },
            "target" : {
                "required" : true
            },
            "allowDeduction" : {
                "required" : true
            }
        }
    });

    $('.list-table').on('click', '.discount-input', function () {
        var me = this,
            $discount = $(me),
            $discountInput = $discount.find('input'),
            $descend = $discount.siblings('.descend-input'),
            $descendInput = $descend.find('input'),
            validator = $('#flashSale-form').validate();

        $descend.addClass('disabled');
        $descendInput.prop('disabled', 'disabled').val('');
        $discount.removeClass('disabled');
        $discountInput.removeProp('disabled').focus();
        validator.element($descendInput[0]);
    });

    $('.list-table').on('click', '.descend-input', function () {
        var me = this,
            $descend = $(me),
            $descendInput = $descend.find('input'),
            $discount = $descend.closest('td').find('.discount-input'),
            $discountInput = $discount.find('input'),
            validator = $('#flashSale-form').validate();

        $discount.addClass('disabled');
        $discountInput.prop('disabled', 'disabled').val('');
        $descend.removeClass('disabled');
        $descendInput.removeProp('disabled').focus();
        validator.element($discountInput[0]);
    });

    /**
     * 当打折失去焦点时计算实际售价
     */
    $('.list-table').on('blur', '.discount-input input', function() {
        var discountInput = this,
            $discountInput = $(discountInput),
            $discount = $discountInput.closest('.discount-input'),
            $saleInput = $discount.siblings('.sale-input').find('.input'),
            price = parseFloat($saleInput.data('orig-price')) || 0,
            discount = parseFloat($discountInput.val());

        if (!isNaN(discount) && 0 < discount) {
            $saleInput.text(Math.max(0, Math.round(price * discount) / 100));
        } else {
            $saleInput.text(Math.max(0, Math.round(price)));
        }
    });

    /**
     * 当减价输入失去焦点时, 计算实际售价
     */
    $('.list-table').on('blur', '.descend-input input', function() {
        var descendInput = this,
            $descendInput = $(descendInput),
            $descend = $descendInput.closest('.descend-input'),
            $saleInput = $descend.siblings('.sale-input').find('.input'),
            price = parseFloat($saleInput.data('orig-price')) || 0,
            descend = parseFloat($descendInput.val());

        if (!isNaN(descend)) {
            $saleInput.text(Math.max(0, Math.round(price - descend)));
        } else {
            $saleInput.text(Math.max(0, Math.round(price)));
        }
    });

    // 初始化状态
    $('.list-table .discount-input').each(function(i, item) {
        var $discount = $(item),
            $discountInput = $discount.find('input'),
            $descend = $discount.closest('td').find('.descend-input'),
            $descendInput = $descend.find('input');
        if ('' == $descendInput.val()) {
            $discountInput.blur();
            $discount.click();
        } else {
            $descendInput.blur();
            $descend.click();
        }
    });
    $('.list-table').on('click', '.td-actions .btn-delete', function() {
        var $self = $(this);
        Glanway.Messager.confirm('警告', '您确定要移除该记录？', function(r) {
            if (r) {
                $self.closest('tr').remove();
                adjustIndex();
            }
        })
    });

    function adjustIndex() {
        $('.list-table tr:not(.tpl)').each(function (i, item) {
            $(item).find('input').each(function (m, input) {
                var $input = $(input),
                    name = $input.attr('name') || '';
                $input.attr('name', name.replace(/^flashSaleDetails\[[0-9]*\]/, 'flashSaleDetails[' + (i - 1) + ']'))
            });
            $(item).find('.td-rn').text(i);
        })
    }

    // 批量操作
    $('.btn-batch').click(function() {
        var $batch = $('.batch-input'),
            discount = $batch.find('.discount-input input').val(),
            descend = $batch.find('.descend-input input').val(),
            $input;
        if ('' != discount) {
            $input = $('.td-sale .discount-input').click().find('input').val(discount);
        } else if ('' != descend) {
            $input = $('.td-sale .descend-input').click().find('input').val(descend);
        }

        if (null != $input) {
            setTimeout(function() {
                $input.trigger('blur');//.first().focus();
            });
        }
    });

    $('.btn-add').click(function() {
        var actx = window.actx || '';
//                layer.loading();
        var $grid = $('#datagrid').jqGrid({
            url: actx + '/goods/list',
            datatype: 'json',
            colNames: ['id', '商品名称', '所属分类', '售价', '库存', '商品状态', '上架日期'],
            colModel: [
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'title', index: 'title', template: 'text'},
                {name: 'product.category.name', index: 'category.name', template: 'text', sortable: false},
                {name: 'marketPrice', index: 'P.marketPrice', template: 'integer'},
                {name: 'stock', index: 'stock', template: 'text', search: false},
                {
                    name: 'product.onSell', index: 'product.onsell',
                    formatter: function (va) {
                        return va ? '已上架' : '未上架';
                    },
                    stype: 'select',
                    searchoptions: {value: ':全部;0:未上架;1:上架'}
                },
                {name: 'product.registerDate', index: 'product.registerDate', template: 'date', search: false }
            ],
            multiselect: true,
            autowidth: true,
            height: 'auto',
            shrinkToFit: true,
            pager: '#pagination',
            sortname: 'lastModifiedDate',
            sortorder: 'desc',
            loadComplete: function(data) {
                this.p._rowData = data.data;
            }
        });
        layer.open({
            zIndex: 10,
            title: '选择打折商品',
            type: 1,
            area: ['100%', '100%'], //宽高
            shadeClose: true,
            content: $('#goods-dialog'),
            btn: ['确定', '取消'],
            cancel: function (index) {
                layer.close(index);
            },
            yes: function (index) {
                var keys = $grid.jqGrid('getGridParam', 'selarrrow'),
                    records = [],
                    data = $grid[0].p._rowData,
                    i, k, s,
                    $tpl = $('.list-table .tpl'),
                    $tr, $sale, specs;
                for (i = 0; i < data.length; i++) {
                    for (k = 0; k < keys.length; k++) {
                        // 已经存在
                        if (0 < $('.list-table tr[data-id="' + keys[k] + '"]').length) {
                            continue;
                        }
                        if (data[i].id == keys[k]) {
                            records.push(data[i]);
                            specs = [];

                            $tr =  $tpl.clone().removeClass('tpl').insertBefore($tpl);
                            $tr.attr('data-id', data[i].id);
                            $tr.find('.td-rn').text($tr.index());
                            $tr.find('.item-pic img')[0].src = ctx + '/' + data[i].image;
                            $tr.find('.item-title').text(data[i].title);
                            $tr.find('.item-prop').text('');
                            $tr.find('.td-code').text(data[i].code);
                            $tr.find('input').val('');
                            $tr.find('.sale-input .input')
                                .attr('data-orig-price', data[i].price || 0)
                                .text(data[i].price || 0);
                            for (s = 0; s < data[i].specValues.length; s++) {
                                specs.push(data[i].specValues[s].name);
                            }
                            $tr.find('.item-prop').text(specs.join(','));

                            // 添加商品ID
                            $sale = $tr.find('.td-sale');
                            $sale.find('.discount-input input').attr('name', 'flashSaleDetails[].discount');
                            $sale.find('.descend-input input').attr('name', 'flashSaleDetails[].descend');
                            $('<input name="flashSaleDetails[].goods.id" type="hidden">').val(data[i].id).appendTo($sale);
                            $('<input name="flashSaleDetails[].product.id" type="hidden">').val(data[i].product.id).appendTo($sale);
                        }
                    }
                }
                adjustIndex();
                layer.close(index);
            }
        });
    });
    adjustIndex();
});
