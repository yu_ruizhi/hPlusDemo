require(['jquery', 'jqgrid.custom'], function ($) {
	var contextPath = (window.actx || window.contextPath) || '';
    var $g = $('#datagrid').jqGrid({
        url: 'list',
        datatype: 'json',
        colNames: ['操作', 'id', '规格名称', '规格别名', '排序'],
        colModel: [
            {template: 'actions2'},
            {name: 'id', index: 'id', hidden: true, key: true, align: 'center'},
            {name: 'name', index: 'name', template: 'text', align: 'center'},
            {name: 'alias', index: 'alias', template: 'text', align: 'center'},
            {name: 'sort', index: 'sort', template: 'text', align: 'center'}
        ],
        multiselect: true,
        autowidth: true,
        shrinkToFit: true,
        pager: '#pagination',
        sortorder: 'desc'
    });

    $('.operateBar .operate-delete').click(function () {
        var keys = $g.jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : Glanway.Messager.confirm("警告", "您确定要删除选择的" + keys.length + "行记录吗？", function (r) {
            r && $.ajax({
                url: 'delete',
                type: 'post',
                traditional: true,
                data: {id: keys}
            }).done(function (data) {
                var removed;
                if (data.error) {
                    Glanway.Messager.alert('提示', data.info);
                }
                
                if (data.success) {
                    removed = data.success || [];
                    $g.trigger("reloadGrid");
                    Glanway.Messager.alert('提示', '操作成功');
                }
            }).fail(function () {
            	Glanway.Messager.alert('提示', '操作失败');
            });
        });
    });
    
  //导入商品品牌
  	//实例化一个plupload上传对象
    var uploader = new plupload.Uploader({
        browse_button : 'importData', //触发文件选择对话框的按钮，为那个元素id
        url : contextPath+"/spec/importData", //服务器端的上传页面地址
        file_data_name : 'file',
        flash_swf_url : contextPath + '/js/lib/plupload-2.1.2/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url : contextPath + '/js/lib/plupload-2.1.2/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        filters: {
      	      mime_types : [ //只允许上传图片文件和rar压缩文件
      	        { title : "spec", extensions:"xls"}, 
      	      ],
      	      max_file_size : '102400kb', //最大只能上传100kb的文件
      	      prevent_duplicates : false //允许队列中存在重复文件
      	    },
        multipart:true,
      	multi_selection:false
    });    

    //在实例对象上调用init()方法进行初始化
    uploader.init();

    //当文件添加到上传队列后触发
    uploader.bind('FilesAdded',function(uploader,files){
         uploader.start(); //开始上传
    });
  	//当上传队列发生变化后触发，即上传队列新增了文件或移除了文件。QueueChanged事件会比FilesAdded或FilesRemoved事件先触发
    uploader.bind('QueueChanged',function(uploader,file){
    	 
    });
    
    //当队列中的某一个文件正要开始上传前触发
    uploader.bind('BeforeUpload',function(uploader,file){
    	$("#divInfoContent").html("数据处理中，请稍等。。。");	
        $(".pop-mask").show();
        $("#divInfo").show();
    });
    
    //当队列中的某一个文件上传完成后触发
    uploader.bind('FileUploaded', function (up, file, result) {
    	var data = $.parseJSON(result.response);
        if("" != data.error){
        	var num = data.error.split("<br>").length;
        	if (1 < num) {
        		var height = "height:"+ (num * 40) +"px;display: block;";
        		$("#divInfo").attr("style",height);
        	}
        	$("#divInfoContent").html(data.error);
        } else {
        	$("#divInfoContent").html("导入成功");	
        }
        $("#datagrid").trigger("reloadGrid");
    });
});

function hideDiv(){
	$(".pop-mask").hide();
    $("#divInfo").hide();
}