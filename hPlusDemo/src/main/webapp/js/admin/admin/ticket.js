require(['jquery', 'jqgrid.custom'], function ($) {
	var contextPath = (window.actx || window.contextPath) || '';
    $('#datagrid').jqGrid({
        url: 'getTicketList',
        datatype: 'json',
        colNames: ['ID', '名称', '是否可以退款', '是否PC显示', '是否APP显示', '价格', '展会开始时间','展会结束时间','剩余数量'],
        colModel: [
            {name: 'id', index: 'ID', key: true, align: 'center',search:false},
            {name: 'tkName', index: 'tkName', template: 'text', align: 'center',search:false},
            {name: 'isRefund', index: 'isRefund', template: 'text', align: 'center',search:false,formatter:booleanFmatter},
            {name: 'isPcShow', index: 'isPcShow', template: 'text', align: 'center',search:false,formatter:booleanFmatter},
            {name: 'isAppShow', index: 'isAppShow', template: 'text', align: 'center',search:false,formatter:booleanFmatter},
            {name: 'price', index: 'price', template: 'text', align: 'center',search:false},
            {name: 'startTime', index: 'startTime', template: 'text', align: 'center',search:false,formatter:timeFmatter},
            {name: 'endTime', index: 'endTime', template: 'text', align: 'center',search:false,formatter:timeFmatter},
            {name: 'qty', index: 'qty', template: 'text', align: 'center',search:false}
        ],
        autowidth: true,
        pager: '#pagination',
        sortname: 'LAST_MODIFIED_DATE',
        shrinkToFit: true,
        sortorder: 'desc'
    });

    function booleanFmatter(cellvalue, options, rowObject){
    	if(cellvalue){
    		return "是"
    	}else{
    		return "否"
    	}
    }
    
    function timeFmatter(cellvalue, options, rowObject){
    	var newDate = new Date();
    	newDate.setTime(cellvalue);
    	return newDate.toLocaleString()
    }
});



