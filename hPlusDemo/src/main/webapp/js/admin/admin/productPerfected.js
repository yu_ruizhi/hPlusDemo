require(['jquery', 'jqgrid.custom'], function ($) {
	var contextPath = (window.actx || window.contextPath) || '';

    var goodsCode="";
    var $g = $('#datagrid').jqGrid({
        url: 'list',
        datatype: 'json',
        colNames: ['操作', '商品ID', '商品图片', '商品名称', '商品状态', '商品类别', '商品品牌', '所属作品', /*'是否上架',*/ '日元成本价', '人民币销售价(含税)', '入箱数', '再贩次数', '库存', '截止日期', '发售日期'],
        colModel: [
            {template: 'actions222', width: 80, fixed: true},
            {name: 'id', index: 'P.id', key: true, align: 'center',searchoptions: {sopt: ["cn"]}},
            {name: 'image', index: 'P.image', template: 'img', align: 'center', searchoptions: {sopt: ["cn"]}},
            {name: 'title', index: 'P.title', template: 'text', align: 'center', searchoptions: {sopt: ["cn"]}},
            {name: 'reserved', index: 'P.reserved', template: 'text', stype: "select", align: 'center',formatter:formatStatus, sortable: false,searchoptions: {value: ':全部;0:预定;1:限购;2:现货'}},
            {name: 'category.name', index: 'C.name', template: 'text', align: 'center', sortable: false},
            {name: 'brand.name', index: 'B.name', template: 'text', align: 'center', sortable: false},
            {name: 'works.name', index: 'W.name', template: 'text', align: 'center', sortable: false},
            /*{
                name: 'onSell', index: 'P.onsell', formatter: function (va) {
                if (va) return "已上架"; else return "未上架";
            }, stype: 'select', searchoptions: {value: ':全部;0:未上架;1:上架'}, align: 'center'
            },*/
            {name: 'price', index: 'P.price', template: 'num', align: 'center', search: false},
            {name: 'marketPrice', index: 'P.marketPrice', template: 'num', align: 'center', search: false},
            {name: 'boxNumber', index: 'P.boxNumber', template: 'text', align: 'center', search: false},
            {name: 'reSellingNum', index: 'P.reSellingNum', template: 'text', align: 'center', search: false},
            {name: 'stock', index: 'P.stock', template: 'int', align: 'center', search: false},
            {name: 'salesOffDate', index: 'P.salesOffDate', template: 'date', search: false, align: 'center'},
            {name: 'registerDate', index: 'P.registerDate', template: 'date', search: false, align: 'center'}
        ],
        multiselect: true,
        autowidth: true,
        shrinkToFit: false,
        pager: '#pagination',
        sortname: 'P.lastModifiedDate',
        sortorder: 'desc'
    });

    function formatStatus(value,options,rowData){
        var str ="";
        if(rowData.reserved){
            str = str+ " <a href='javascrpt:;' style='color:red;margin-right:5px;'>预定</a>";
        }else{
            str = str+ " <a href='javascrpt:;' style='margin-right:5px;'>预定</a>";
        }

        if(rowData.limited){
            str = str+ " <a href='javascrpt:;' style='color:red;margin-right:5px;'>限购</a>";
        }else{
            str = str+ " <a href='javascrpt:;' style='margin-right:5px;'>限购</a>";
        }

        if(rowData.spot){
            str = str+ " <a href='javascrpt:;' style='color:red;margin-right:5px;'>现货</a>";
        }else{
            str = str+ " <a href='javascrpt:;' style='margin-right:5px;'>现货</a>";
        }

        return str ;

    }
    
    $('.operateBar .operate-delete').click(function () {
        var keys = $g.jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : Glanway.Messager.confirm("警告", "您确定要删除选择的" + keys.length + "行记录吗？", function (r) {
            r && $.ajax({
                url: 'delete',
                type: 'post',
                traditional: true,
                data: {id: keys}
            }).done(function (data) {
                var removed;
                if (data.success) {
                    removed = data.success || [];
                    $g.trigger("reloadGrid");
                    $.messager.show({
                        title: '提示',
                        text: '操作成功',
                        sticky: false,
                        time: 3000
                    });
                }
            }).fail(function () {
                $.messager.show({
                    title: '提示',
                    text: '操作失败',
                    sticky: false,
                    time: 3000
                });
            });
        });
    });

});
    



    

    

    

    

