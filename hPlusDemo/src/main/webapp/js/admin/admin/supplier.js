require(['jquery', 'jqgrid.custom'], function ($) {
    var $g = $('#datagrid').jqGrid({
        url: 'list',
        datatype: 'json',
        colNames: ['操作', 'ID', '供应商编号', '供应商名称', '邮箱','联系电话','创建时间'],
        colModel: [
            {template: 'actions2', width: 80, fixed: true},
            {name: 'id', index: 'id', key: true, align: 'center'},
            {name: 'numberCode', index: 'numberCode', template: 'text', searchoptions: {sopt: ["cn"]}},
            {name: 'supplierName', index: 'supplierName', template: 'text', searchoptions: {sopt: ["cn"]}},
            {name: 'email', index: 'email', template: 'text', searchoptions: {sopt: ["cn"]}},
            {name: 'phone', index: 'phone', template: 'text', searchoptions: {sopt: ["cn"]}},
            {name: 'createdDate', index: 'createdDate', template: 'date', search: false, align: 'center'}
        ],
        shrinkToFit: true,
        multiselect: true,
        autowidth: true,
        pager: '#pagination',
        sortname: 'createdDate',
        sortorder: 'desc'
    });

    //删除
    $('.operateBar .operate-delete').click(function () {
        var keys = $g.jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : Glanway.Messager.confirm("警告", "您确定要删除选择的" + keys.length + "行记录吗？", function (r) {
            r && $.ajax({
                url: 'delete',
                type: 'post',
                traditional: true,
                data: {id: keys}
            }).done(function (data) {

                var removed;
                if (data) {

                    removed = data.result || [];
                    $g.trigger("reloadGrid");
                    $.gritter.add({
                        title: '提示',
                        text: '操作成功',
                        sticky: false,
                        time: 3000
                    });
                }
            }).fail(function () {
                $.gritter.add({
                    title: '提示',
                    text: '操作失败',
                    sticky: false,
                    time: 3000
                });
            });
        });
    });


});