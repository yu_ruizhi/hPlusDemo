require(['jquery', 'mustache'], function ($, Mustache) {
    var contextPath = window.actx || window.contextPath || '',
        CATEGORY_TREE_URL = contextPath + '/category/tree',
        MODEL_DETAIL_URL = contextPath + '/model/detail/',
        M_TPL = '{{#.}}<li><a href="javascript:void(0);" data-id="{{id}}">{{name}}</a></li>{{/.}}',
        BASE_RULES = {
            title: {
                required: true,
                minlength: 1,
                maxlength: 250
            },
            alias: {
                maxlength: 250
            },
            sn: {
//                required: true,
                minlength: 14,
                maxlength: 14
            },
            limited: {
                digits: true,
                maxlength: 8
            },
            "category.name": "required",
            // "brand.id": "required",
            weight: {
//                required: true,
                number: true,
                min: 0,
                max: 100000
            },
            length: {
                // required: true,
                number: true,
                min: 0,
                max: 100000
            },
            width: {
//                required: true,
                number: true,
                min: 0,
                max: 100000
            },
       /*    marketPrice: {
                required: true,
                number: true,
                min: 0,
                max: 100000
            },*/
            height: {
//                required: true,
                number: true,
                min: 0,
                max: 100000
            }/*,
             modulus: {
             required: true,
             number: true,
             min: 0,
             max: 99
             }*/
        };

    var Goods = function () {
        var me = this;

        me.specs = window['currentSpecs'] || [];
        me.ignoreSpecIds = window['currentIgnoreSpecIds'] || [];  // 不生成货号的id
        me.$attrsList = $('#attr-list');
        me.$paramsList = $('#param-list');
        me.$specList = $('#spec-list');
        me.$productSpecs = $('#product-specs');
        me.$goodsList = $('#goods-list');
        me.$accessoryList = $('#acce-list');
        me.$enableSpecBtn = $('#btn-enable-spec');

        me.attrsTpl = $('#tpl-attrs').html();
        me.paramsTpl = $('#tpl-params').html();
        me.specsTpl = $('#tpl-specs').html();
        me.specsImgTpl = $('#tpl-specs-img').html();
        me.goodsTpl = $('#tpl-goods').html();

        Mustache.parse(me.attrsTpl);
        Mustache.parse(me.paramsTpl);
        Mustache.parse(me.specsTpl);
        Mustache.parse(me.specsImgTpl);
        Mustache.parse(me.goodsTpl);

        me.initEvent();
        me.rebindRemoveEvent();
    };

    Goods.prototype = {
        showTab: function (index) {
            $('.tab-wrap ul.tab li').eq(index).addClass("on").siblings().removeClass("on");
            $('.tab-ct-wrap .tab1').eq(index).show().siblings().hide();
        },

        rebindRemoveEvent: function () {
            var me = this;

            // 删除按钮事件
            me.$accessoryList.find('div > h2 > span > a').each(function (i, el) {
                $(el).off('click').on('click', function () {
                    $(this).closest('div').remove();
                });
            });
        },

        bindImage: function () {
            var me = this,
                $images = $('#pro-img .js-plupload input[name$=path]');
            var $goods = $('#goods-list ul');
            for (var i = 0; i < $goods.length; i++) {
                var $gImgs = $($goods[i]).find('li');
                for (var j = 0; j < $images.length; j++) {
                    var $li = $gImgs[j];
                    $('<img />').attr({'width': '100%', 'height': '100%', 'src': contextPath + '/' + $($images[j]).val()}).appendTo($li);
                    $('<input/>').attr({'type': 'hidden', 'name': 'goodsList[' + i + '].productImages[' + j + '].path'}).val(contextPath + '/' + $($images[j]).val()).appendTo($li);
                    $('<input/>').attr({'type': 'hidden', 'name': 'goodsList[' + i + '].productImages[' + j + '].sort'}).val(j).appendTo($li);
                }
            }
        },

        initEvent: function () {
            var me = this,
                $tab = $('.tab-ct-wrap'),
                $basicTab = $tab.find('.tab1:nth-child(1)'),
                $specTab = $tab.find('.tab1:nth-child(2)'),
                $detailTab = $tab.find('.tab1:nth-child(3)'),
                $acceTab = $tab.find('.tab1:nth-child(4)');

            // me.$enableSpecBtn.closest('tr').hide();
            me.showSpecTab(me.$enableSpecBtn.attr('checked'));
            me.$enableSpecBtn.change(function () {
                me.showSpecTab(this.checked);
                me.cleanGoods();
            });

            // 基本信息验证
            $('form:last').validate({
                rules: BASE_RULES,
                errorPlacement: function($error, $el) {
                    var $label = $el.siblings('label.error');
                    if($el.attr("name")=='category.name'){
                        $el.parent("label").after($error);
                    }
                    if ($label.length < 1) {
                        $label = $el.closest('ul').siblings('.group-error');
                    }


                    if ($label.length > 0) {
                        $label.text($error.text());
                    } else {
                        $el.after($error);
                        $el.parent("label").after($error);
                    }
//                    console.log(error, element);
                }
            });

            // basic info tab --> 规格/详细信息
            $basicTab.find('[data-act=next]').click(function () {

                var validator = $('form:last').validate(),
                    enableSpec = me.$enableSpecBtn.attr('checked');
                 flag=validator.checkForm()
                if (flag) {
                    var next = enableSpec ? 1 : 2;
                    me.showTab(next);

                    // 追加验证规则
                    // $.extend(true, validator.settings.rules, {});
                } else {
                    validator.showErrors();
                    validator.focusInvalid();
                }
                return false;
            });

            // 规格 --> 详细信息
            $specTab.find('[data-act=next]').click(function () {
                var validator = $('form:last').validate();
                flag=validator.checkForm()
                if (flag) {
                    me.showTab(2);

                    // $.extend(true, validator.settings.rules, { });
                } else {
                    validator.showErrors();
                    validator.focusInvalid();
                }
                return false;
            });

            // 规格 --> 基本信息
            $specTab.find('[data-act=prev]').click(function () {
				var validator = $('form:last').validate();
//				validator.settings.rules = BASE_RULES;
                flag=validator.checkForm();
                if (flag) {
                    me.showTab(0);

                    // $.extend(true, validator.settings.rules, { });
                } else {
                    validator.showErrors();
                    validator.focusInvalid();
                }
            });

            $detailTab.find('[data-act=next]').click($.proxy(me.showTab, me, 3));
            $detailTab.find('[data-act=prev]').click(function () {
                var enableSpec = me.$enableSpecBtn.attr('checked'),
                    prev = enableSpec ? 1 : 0;
                me.showTab(prev);
            });

            $acceTab.find('[data-act=prev]').click($.proxy(me.showTab, me, 2));

            // 现实选择配件
            $('#btn-acce-sel').click(function () {
                var $grid = $("#datagrid").datagrid({
                        numberbers: true,
                        idField: 'id',
                        pagination: true,
                        url: contextPath + '/product/list',
                        pageSize: 10,
                        columns: [
                            [
                                {
                                    field: 'title',
                                    title: '名称',
                                    sortable: true,
                                    filter: {
                                        editor: 'text',
                                        options: {
                                            name: 'product.title'
                                        }
                                    },
                                    width: 100
                                },
                                {
                                    field: 'alias',
                                    title: 'Alias',
                                    sortable: true,
                                    filter: {
                                        editor: 'text',
                                        options: {
                                            name: 'product.alias'
                                        }
                                    },
                                    width: 100
                                },
                                {
                                    field: 'category.name',
                                    title: 'Category',
                                    filter: {
                                        editor: 'text',
                                        options: {
                                            name: 'category.name'
                                        }
                                    },
                                    width: 100
                                },
                                {
                                    field: 'brand.name',
                                    title: 'Brand',
                                    filter: {
                                        editor: 'text',
                                        options: {
                                            name: 'brand.name'
                                        }
                                    },
                                    width: 100
                                },
                                {
                                    field: 'isPutaway',
                                    title: 'Is Putaway',
                                    formatter: util.fmtBoolean,
                                    filter: {
                                        editor: 'combobox',
                                        options: {
                                            name: 'product.isPutaway'
                                        }
                                    },
                                    width: 100
                                }
                            ]
                        ],
                        view: detailview,
                        detailFormatter: function (index, row) {
                            return '<div style="padding:10px"><table class="ddv"></table></div>';
                        },
                        onExpandRow: function (index, row) {
                            var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
                            ddv.datagrid({
                                url: contextPath + '/product/' + row.id + "/goods",
                                pagination: false,
                                rownumbers: true,
                                idField: 'id',
                                columns: [
                                    [
                                        {
                                            checkbox: true
                                        },
                                        {
                                            field: 'specValues',
                                            title: '规格',
                                            formatter: function (v) {
                                                if (!v) {
                                                    return v;
                                                }
                                                var specs = '';
                                                $.each(v, function (i, el) {
                                                    specs += '<div>' + el.name + '</div>';
                                                });
                                                return specs;
                                            }
                                        },
                                        {
                                            field: 'code',
                                            title: '编码',
                                            width: 100,
                                            align: "center"
                                        }
                                    ]
                                ],
                                onResize: function () {
                                    $('#readjustment_cdc').datagrid('fixDetailRowHeight', index);
                                },
                                onLoadSuccess: function () {
                                    setTimeout(function () {
                                        $('#readjustment_cdc').datagrid('fixDetailRowHeight', index);
                                        var $tHeight = $(".datagrid-body-inner").find(".datagrid-row-detail").parent("td").eq(index);
                                        var $dHeight = $(".datagrid-body").find(".datagrid-row-detail").children("div").eq(index).outerHeight(true);
                                        $tHeight.height($dHeight);
                                    }, 10);
                                }
                            });
                            $('#readjustment_cdc').datagrid('fixDetailRowHeight', index);
                        },
                        onLoadSuccess: function () {
                            setTimeout(function () {
                                $('#goods-dlg').dialog('center');
                            }, 20);
                        },
                        loadFilter: function (data) {
                            var rows = data.rows, id = $('input[name=id]').val(), i;
                            if (!id) {
                                return data;
                            }
                            for (i = 0; i < rows.length; i++) {
                                if (rows[i].id == id) {
                                    rows.splice(i, 1);
                                    data.total -= 1;
                                    break;
                                }
                            }
                            return data;
                        }
                    }
                ).datagrid('enableFilter');

                //
                var $dlg = $('#goods-dlg').dialog({
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                var products = $grid.datagrid('getRows'),
                                    i,
                                    j,
                                    $subgrid,
                                    sels,
                                    goods,
                                    all = [],
                                    count = 0;
                                for (i = 0; i < products.length; i++) {
                                    $subgrid = $grid.datagrid('getRowDetail', i).find('table.ddv');
                                    if (!$subgrid.data('datagrid')) {
                                        continue;
                                    }
                                    sels = $subgrid.datagrid({}).datagrid('getSelections');

                                    for (j = 0; j < sels.length; j++) {
                                        goods = sels[j];

                                        goods = $.extend({}, goods);
                                        goods.product = products[i];
                                        goods.index = count++;
                                        goods.image = ((products[i]['productImages'] || [])[0] || {}).path;

                                        all.push(goods);
                                    }
                                }
                                var accessries = Mustache.render($("#tpl-acce").html(), {
                                    productId: $('[name=id]').val(),
                                    accessories: all
                                });

                                // WARN: 因为不支持多个 配件所以这里要清空
                                me.$accessoryList.html('');
                                me.$accessoryList.append(accessries);

                                me.$accessoryList.find('[data-composes]').each(function (i, comp) {
                                    $(comp).find('input[name]:enabled').each(function (j, input) {
                                        $(input).attr('name', $(input).attr('name').replace(/composes\[[0-9]*\]/, 'composes[' + i + ']'));
                                    });
                                });

                                $dlg.dialog('close');

                                me.rebindRemoveEvent();
                                fixAccessoryList && fixAccessoryList();
                            }
                        }
                    ]
                }).dialog('open').dialog('center');
            });
        },

        onCategoryChange: function () {
            var me = this,
                currMid = $('[name="model.id"]').val();

            $.getJSON(MODEL_DETAIL_URL + currMid, function (json) {
                var model = json.data;

                me.$attrsList.html('');
                me.$paramsList.html('');
                me.$specList.html('');
                me.$productSpecs.html('');
                me.$goodsList.html('');

                if (model.modelSpecs && model.modelSpecs.length > 0) {
                    $('input[name=enableSpecs]').closest('tr').show();
                } else {
                    $('input[name=enableSpecs]').closest('tr').hide();
                }

                if (model.attributes && model.attributes.length > 0) {

                    $.each(model.attributes, function (i, attr) {
                        var type = attr['displayType'];
                        attr.textMode = (0 != type && 2 != type)?false:true;
                        attr.index = $('#attr-list').data('index') + i;
                    });
                    me.$attrsList.html(Mustache.render(me.attrsTpl, model));
                }

                if (model.parameters && model.parameters.length > 0) {
                    me.$paramsList.html(Mustache.render(me.paramsTpl, model));
                }
                me.$paramsList.find('tr').each(function (i, tr) {
                    $(tr).find('input[name]:enabled').each(function (j, el) {
                        $(el).attr('name', $(el).attr('name').replace(/\[[0-9]*\]/, '[' + i + ']'));
                    })
                });

                me.specs = [];
                $.each(model.modelSpecs || [], function (i, el) {
                    el.spec.specId = el.spec.id; // for tpl
                    me.specs.push(el.spec);
                    // 影响货号，暂时是使用这个
                    if (!el['isAffectGoods']) {
                        me.ignoreSpecIds.push(el.spec.id);
                    }
                });
                if (me.specs.length > 0) {
                    me.$enableSpecBtn.closest('tr').show();
                    me.$specList.html(Mustache.render(me.specsTpl, me.specs));
                    parseSelectAll();
                } else {
                    me.$enableSpecBtn.closest('tr').hide();
                }
            });
        },

        showSpecTab: function (show) {
            var $tab = $('[data-tab=spec]');
            show ? ($tab.show()) : ($tab.hide());
            $stock=$("#stock");
            $inputStock=$("#stock input[name='stock']");
            if(show){
                $stock.hide();$inputStock.removeClass("required digits")
            }else{
                $stock.show();$inputStock.addClass("required digits")
            }
        },

        getSpecValue: function (specId, specValueId) {
            var me = this, ret;
            $.each(me.specs, function (i, spec) {
                if (specId == spec.id) {
                    $.each(spec.specValues, function (i, value) {
                        if (specValueId == value.id) {
                            ret = value;
                            value.spec = spec;
                            return false;
                        }
                    });
                    return false;
                }
            });
            return ret;
        },
        /**
         * 生成所有货品
         */
        generateGoods: function (goods) {
            var me = this;

            // 是否是不影响货品编号的规格
            function isIgnoreSpec(id) {
                for (var i = 0; i < me.ignoreSpecIds.length; i++) {
                    var ignore = me.ignoreSpecIds[i];
                    if (id == ignore) {
                        return true;
                    }
                }
                return false;
            }

            // 如果没有传递则获取选中的规格
            if (undefined == goods) {
                var specValues = this.getSelectedSpecValues(),
                    i, 
                    j, 
                    k;

                goods = [];

                // 获取所有选中的规格值
                if (specValues.length > 0) {
                    // 计算所有的规格值组合类型
                    specValues = this.cartesianProduct.apply(this, specValues) || [];
                    // 遍历所有组合

                    for (i = 0; i < specValues.length; i++) {
                        var code = ($('[name=sn]').val() || ''),
                            sorted = [];
                        // 遍历每个规格组合中的规格值
                        for (j = 0; j < specValues[i].length; j++) {
                            var value = specValues[i][j];   // 规格值
                            value.index = j;     // for template
                            // 按照规格排序
                            for (k = sorted.length - 1; k > -1; k--) {
                                if (value.spec.sort >= sorted[k].spec.sort) {
                                    sorted.splice(k - 1, k -1, value);
                                    break;
                                }
                            }
                            if (k < 0) {
                                sorted.unshift(value);
                            }
                        }

                        // 按照顺序生成 code (product 8位 sn + 8位 规格值 code)
                        /*
                         for (k = 0; k < 4; k++) {
                         if (!sorted[k] || isIgnoreSpec(sorted[k].spec.id)) {
                         sorted[k] = {};
                         }
                         code += sorted[k].code || '';
                         }
                         */
                        code = Number(code) + Number($.map(sorted, function(item) { return item.code || '' }).join(''));

                        goods.push({ 
                            title: $('[name=title]').val(),
                            specValues: specValues[i],
                            code: code,
                            price: $('[name=price]').val(),
                            marketPrice: $('[name=marketPrice]').val(),
                            stock: $('[name=stock]').val(),  //
                            
                            goodsIndex: i       // for template
                        });
                    }
                }
            } else {
                $.each(goods, function (i, el) {
                    el.index = i;
                });
            }

            me.cleanGoods();
            if (goods.length > 0) {
                me.$goodsList.html(Mustache.render(me.goodsTpl, goods));
                parseUpload();
            }

            // 绑定商品主图
            me.bindImage();
        },

        /**
         * 清除所有货品
         */
        cleanGoods: function () {
            this.$goodsList.html('');
        },

        /**
         * 获取所有选择的规格值
         */
        getSelectedSpecValues: function () {
            var me = this,
                specs = me.specs,
                specValues = [],
                values = [],
                i;
            for (i = 0; i < specs.length; i++) {
                values = [];
                $('[name=' + specs[i].id + ']:checked').each(function (i, el) {
                    values.push(me.getSpecValue(el.name, el.value));
                });

                if (values.length > 0) {
                    specValues.push(values);
                }
            }

            return specValues;
        },

// 计算的笛卡尔积
        cartesianProduct: function () {
            function addTo(curr, args) {
                var i,
                    copy,
                    rest = args.slice(1),
                    last = !rest.length,
                    result = [];

                for (i = 0; i < args[0].length; i++) {
                    copy = curr.slice();
                    copy.push(args[0][i]);
                    if (last) {
                        result.push(copy);
                    } else {
                        result = result.concat(addTo(copy, rest));
                    }
                }
                return result;
            }
            return addTo([], Array.prototype.slice.call(arguments));
        }
    } ;

    return window.Goods = window.__g = new Goods();
});