require(['jquery', 'mustache', 'jquery.validate', 'spinner', 'uploader2'], function ($, Mustache, v, s, Uploader2) {
    var exports = window;
    var contextPath = window.actx || window.contextPath || '';
    
    function SpecValue() {
        var me = this;
        me.specValueTpl = $('#tpl-spec-value2').html();
        me.specValueTplTxt = $('#tpl-spec-value3').html();
        me.$specValueList = $("#spec-value-list");

        Mustache.parse(me.specValueTpl);

        me.bindEvent();
    }

    SpecValue.prototype = {
        bindEvent: function () {
            var me = this,
            	specId = $("input[name='id']").val();
            $('[name=displayType]').change(function () {
            	if($("#spec-value-list:has(thead)").length != 0){
            		/* 编辑页 */
	            	Glanway.Messager.confirm("警告", "确定清空该规格下的规格值吗？", function(r){
	            		if(r){
	            			if(specId){	            				
	            				$.ajax({
	            					url: contextPath+"/spec/deleteSpecValueBySpecId",
	            					type: "post",
	            					dataType: "json",
	            					data: {"specId": specId},
	            					success: function(data){
	            						me.$specValueList.empty();
	            					},
	            					error: function(){
	            						Glanway.Messager.alert("提示", "操作失败");
	            					}
	            				});
	            			}else{
	            				me.$specValueList.empty();
	            			}
	            			//FIXME 切换类型时验证信息清除不掉。
	            			/*$("#spec-value-list label.error").remove();*/
	            		}else{
	            			//若点击取消，则返回之前状态
	            			$("[name=displayType]:checked").siblings(":radio").prop("checked",true);
	            		}
	            	});
            	}
            });

            $('#btn-add-spec-value').click($.proxy(me, 'addSpecValue'));

            $('form:last').validate({
                rules: {
                    name: {
                        required: true,
                        minlength: 2,
                        maxlength: 12,
                        remote: {
                            url: contextPath + '/spec/check',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                id: function () {
                                    return $('[name=id]').val() || '';
                                },
                                name: function () {
                                    return $('[name=name]').val();
                                }
                            }
                        }
                    },
                    alias: {
                        maxlength: 12
                    },
                    sort: {
                        required: true,
                        digits: true,
                        max: 999
                    }
                },
                messages:{
                	name:{remote: "该规格名称已被使用"}
                },
                errorPlacement: function (error, element) {
                    var $target = element.siblings("span.errorTip");
                    if (1 > $target.length) {
                        $target = element.parent().siblings("span.errorTip");
                    }
                    if (1 > $target.length) {
                        $target = element.parent();
                    }
                    error.appendTo($target);
                },
                submitHandler: function (form) {
                    var ln = [];
                    $(".lname").each(function () {
                        ln.push($(this).val());
                    });
                    var lc = [];
                    $(".lcode").each(function () {
                        lc.push($(this).val());
                    })
                    var p = true;
                    for (var i = 0; i < ln.length; i++) {
                        var t = ln[i];
                        for (var j = 0; j < ln.length; j++) {
                            if (ln[j] == t) {
                                if (i != j) {
                                    p = false;
                                }
                            }
                        }
                    }
                    for (var m = 0; m < lc.length; m++) {
                        var t = lc[m];
                        for (var n = 0; n < ln.length; n++) {
                            if (lc[i] == t) {
                                if (n != m) {
                                    p = false;
                                }

                            }
                        }
                    }
                    if (!p) {
                        alert("请填写编码");
                    } else {
                        form.submit();
                    }
                }
            });
        },
        addSpecValue: function () {
            var me = this;
            var type = $('[name=displayType]:checked').val();
            if(0 == type){
                me.$specValueList.append(Mustache.render(me.specValueTplTxt));
                //
                me.$specValueList.find('tbody tr').each(function (i, tr) {
                    $(tr).find('input[name]:enabled,select[name]:enabled,textarea[name]:enabled').each(function (j, input) {
                        var $input = $(input);
                        $input.attr('name', $input.attr('name').replace(/[[0-9]*]/, '[' + i + ']'));
                    });
                });
            }else{
            	me.$specValueList.append(Mustache.render(me.specValueTpl));
            	//
            	me.$specValueList.find('tbody tr').each(function (i, tr) {
            		$(tr).find('input[name]:enabled,select[name]:enabled,textarea[name]:enabled').each(function (j, input) {
            			var $input = $(input);
            			$input.attr('name', $input.attr('name').replace(/[[0-9]*]/, '[' + i + ']'));
            		});
            	});
            	
            	me.$specValueList.find('tr ul>li').each(function(i){
            		$(this).attr("id", "specValues["+i+"]-img0").attr("data-list-target", "specValues["+i+"]-img0:thumb:img").attr('name', 'specValues['+i+'].image');
            	});            	
            }
            if(me.$specValueList.find("thead").length > 1){
            	me.$specValueList.find("thead:gt(0)").remove();
            	me.$specValueList.find("tfoot:gt(0)").remove();
            }
            me.rebindEvent();
            parseUpload();
        },
        
        rebindEvent: function () {
            $('#spec-value-list tbody').find('tr').each(function (i, el) {
                var $tr = $(el),
                    $delBtn = $tr.find('.operateBox img:last'),
                    index = $tr.index();

                $delBtn.off('click').on('click', function () {
                    //var id = $tr.find('[name="specValues[' + index + '].id"]').val();
                    var id=$tr.children().children(".specValueId").val();
                    Glanway.Messager.confirm('警告', '您确定要删除选中内容？', function (r) {
                        if (r) {
                            if (!id) {
                                $tr.remove();
                            } else {
                                debugger;
                                $.ajax({
                                    type: 'post',
                                    url: contextPath + '/spec/deleteSpecValue/' + id,
                                    dataType: 'json',
                                    success: function (data) {
                                        if (!data.success) {
                                            $.messager.show({
                                                title: 'Error',
                                                msg: data.message || 'Unknown Error'
                                            });
                                        } else {
                                            $tr.remove();
                                        }
                                    }
                                });
                            }
                        }
                    });
                });
            });

            $('.spinner').spinner({
                max: 2147483647
                , min: 0
                , step: 1
                , allowEmpty: true
                , minusBtn: '.btn-down'
                , plusBtn: '.btn-up'
            });

            $('#spec-value-list').find('tbody tr ul>li').each(function(index, el) {
                var id = 'specValues[' + index + ']-img0';
                if (!el.uploader) {
                    el.uploader = Uploader2({
                        url: contextPath + '/img/preupload',
                        policy: true,
                        list: id,
                        browse_button: id,
                        name: 'specValues[' + index + '].image',
                        mode: 't',
                        max_file_count: 1,
                        max_file_size: '20m',
                        filters: {mime_types: [{title: 'Allowd Files', extensions: 'jpg,png,gif'}]}
                    });
                }
            });
        }
    };

    exports.specvalue = new SpecValue();
    specvalue.rebindEvent();
});
