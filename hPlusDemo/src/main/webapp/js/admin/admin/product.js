require(['jquery', 'jqgrid.custom'], function ($) {
	var contextPath = (window.actx || window.contextPath) || '';

    var goodsCode="";
    var $g = $('#datagrid').jqGrid({
        url: 'list?goodsCode='+goodsCode,
        datatype: 'json',
        colNames: ['操作', '商品ID', '商品图片', '商品名称', '商品状态', '商品类别', '商品品牌', '所属作品', /*'是否上架',*/ '日元成本价', '人民币销售价(含税)', '入箱数', '再贩次数', '库存', '截止日期', '发售日期'],
        colModel: [
            {template: 'actions222', width: 80, fixed: true},
            {name: 'id', index: 'P.id', key: true, align: 'center',searchoptions: {sopt: ["cn"]}},
            {name: 'image', index: 'P.image', template: 'img', align: 'center', searchoptions: {sopt: ["cn"]}},
            {name: 'title', index: 'P.title', template: 'text', align: 'center', searchoptions: {sopt: ["cn"]}},
            {name: 'reserved', index: 'R.reserved', template: 'text', stype: "select", align: 'center',formatter:formatStatus, sortable: false,searchoptions: {value: ':全部;0:预定;1:限购;2:现货'}},
            {name: 'category.name', index: 'C.name', template: 'text', align: 'center', sortable: false},
            {name: 'brand.name', index: 'B.name', template: 'text', align: 'center', sortable: false},
            {name: 'works.name', index: 'W.name', template: 'text', align: 'center', sortable: false},
            /*{
                name: 'onSell', index: 'P.onsell', formatter: function (va) {
                if (va) return "已上架"; else return "未上架";
            }, stype: 'select', searchoptions: {value: ':全部;0:未上架;1:上架'}, align: 'center'
            },*/
            {name: 'price', index: 'P.price', template: 'num', align: 'center', search: false},
            {name: 'marketPrice', index: 'P.marketPrice', template: 'num', align: 'center', search: false},
            {name: 'boxNumber', index: 'P.boxNumber', template: 'text', align: 'center', search: false},
            {name: 'reSellingNum', index: 'P.reSellingNum', template: 'text', align: 'center', search: false},
            {name: 'stock', index: 'P.stock', template: 'int', align: 'center', search: false},
            {name: 'salesOffDate', index: 'P.salesOffDate', template: 'date', search: false, align: 'center'},
            {name: 'registerDate', index: 'P.registerDate', template: 'date', search: false, align: 'center'}
        ],
        multiselect: true,
        autowidth: true,
        shrinkToFit: false,
        pager: '#pagination',
        sortname: 'P.lastModifiedDate',
        sortorder: 'desc',
        subGrid : true,
        subGridRowExpanded : function(subgrid_id, row_id) {
            // we pass two parameters
            // subgrid_id is a id of the div tag created whitin a table data
            // the id of this elemenet is a combination of the "sg_" + id of the row
            // the row_id is the id of the row
            // If we wan to pass additinal parameters to the url we can use
            // a method getRowData(row_id) - which returns associative array in type name-value
            // here we can easy construct the flowing
            var subgrid_table_id, pager_id;
            subgrid_table_id = subgrid_id + "_t";
            pager_id = "p_" + subgrid_table_id;
            $("#" + subgrid_id).html(
                "<table id='" + subgrid_table_id
                + "' class='scroll'></table><div id='"
                + pager_id + "' class='scroll'></div>");
            $("#" + subgrid_table_id).jqGrid(
                {
                    url : contextPath+"/goods/listInProductId?productId="+row_id,
                    datatype : "json",
                    colNames : [ '操作','商品ID', '商品货号','商品名字', '商品图片'],
                    colModel : [
                        {template: 'actions223', width: 80, fixed: true},
                        {name : "id",  index : "id",search: false,width : 60},
                        {name : "code",index : "code",  width : 200, searchoptions: {sopt: ["cn"]}},
                        {name : "title",index : "title",width : 200, searchoptions: {sopt: ["cn"]}},
                        {name : "image",index : "image",width : 120,template: 'img'}
                    ],
                    multiselect: true,
                    autowidth: true,
                    shrinkToFit: false,
                    pager : pager_id,
                    sortname : 'num',
                    sortname: 'lastModifiedDate',
                    sortorder: 'desc',
                });
            $('#'+subgrid_table_id).setGridWidth(660);
            $('#'+pager_id).setGridWidth(660);
        },
    });

    function formatStatus(value,options,rowData){
    	var str ="";
    	if(rowData.reserved){
    		str = str+ " <a href='javascrpt:;' style='color:red;margin-right:5px;'>预定</a>";
    	}else{
    		str = str+ " <a href='javascrpt:;' style='margin-right:5px;'>预定</a>";
    	}
    	
    	if(rowData.limited){
    		str = str+ " <a href='javascrpt:;' style='color:red;margin-right:5px;'>限购</a>";
    	}else{
    		str = str+ " <a href='javascrpt:;' style='margin-right:5px;'>限购</a>";
    	}
    	
    	if(rowData.spot){
    		str = str+ " <a href='javascrpt:;' style='color:red;margin-right:5px;'>现货</a>";
    	}else{
    		str = str+ " <a href='javascrpt:;' style='margin-right:5px;'>现货</a>";
    	}
    	
    	return str ;
    	
    }

    $(".goodsCodeb").click(function(){
        goodsCode=$(".goodsCodez").val();
        $g.jqGrid('setGridParam', { url: "list?goodsCode="+goodsCode }).trigger('reloadGrid');
    });
    
    $('.operateBar .operate-delete').click(function () {
        var keys = $g.jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : Glanway.Messager.confirm("警告", "您确定要删除选择的" + keys.length + "行记录吗？", function (r) {
            r && $.ajax({
                url: 'delete',
                type: 'post',
                traditional: true,
                data: {id: keys}
            }).done(function (data) {
                var removed;
                if (data.success) {
                    removed = data.success || [];
                    $g.trigger("reloadGrid");
                    $.messager.show({
                        title: '提示',
                        text: '操作成功',
                        sticky: false,
                        time: 3000
                    });
                }
            }).fail(function () {
                $.messager.show({
                    title: '提示',
                    text: '操作失败',
                    sticky: false,
                    time: 3000
                });
            });
        });
    });
    
    //导出
    $("#export").on('click',function(){
    	var keys = $('#datagrid').jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : 
        	Glanway.Messager.confirm("提示", "您确定要导出选择的" + keys.length + "行记录吗？", function (r) {
               if (r) {
           		window.open(contextPath+"/product/exportData?idArr="+keys);
               }
        });
    });

    //按照截单时间、商品状态导出
    $("#export-goods").on('click',function(){
        debugger;
        var cutOffStartTime = $("#createDateFrom").val() == "" ? null : $("#createDateFrom").val();
        var cutOffEndTime = $("#createDateTo").val() == "" ? null : $("#createDateTo").val();
        var status = $("#status").val();
        window.open(contextPath+"/product/searchGoodsExport?cutOffStartTime="+cutOffStartTime+"&cutOffEndTime="+cutOffEndTime+"&status="+status);
    });
    
    //再贩
    $("#reSellingNum").on('click',function(){
    	var keys = $('#datagrid').jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : 
        	Glanway.Messager.confirm("提示", "您确定要修改选择的" + keys.length + "行记录吗？", function (r) {
               if (r) {
            	    $(".mask").show();
           			$("#reSellingNumDiv").show();
               }
        });
    });
    
    $("#save").on('click',function(){
    	var keys = $('#datagrid').jqGrid('getGridParam', 'selarrrow');
    	var reSellingNum = $("input[name='reSellingNum']").val();
        var reserveDeadline = $("input[name='reserveDeadline']").val();
        var registerDate = $("input[name='registerDate']").val();
    	var num = /^([0-9]+)$/;
    	if(null == reSellingNum || "" == reSellingNum) {
    		$(".errorTip").text("不能为空");
    		return;
    	} else if (4 < reSellingNum.length) {
    		$(".errorTip").text("长度过长");
    		return;
    	} else if (!num.test(reSellingNum)) {
    		$(".errorTip").text("只能输入数字");
    		return;
    	}
        if(null == reserveDeadline || "" == reserveDeadline) {
            $(".reserveDeadline").text("不能为空");
            return;
        }
    	$("#reSellingNumDiv").hide();
    	$("#reSellingNumDiv").prev("div[class=mask]").html("<img alt='loading' src='"+contextPath+"/images/loading.jpg' style=' margin-top:400px;' width='25px' height='25px'>");
		$.ajax({
			url:contextPath+"/product/updateReSellingNum",
			type:"post",
			traditional: true,
			data:{id:keys,reSellingNum:reSellingNum,reserveDeadline:reserveDeadline,registerDate:registerDate},
			dataType:"json",
			success:function(date){
				if (true == date.success) {
					window.location.reload();
				}
			}
		})
    })
    
     $(".closeDiv").on('click',function(){
    	$(".errorTip").text("");
    	$("input[name='reSellingNum']").val("");
    	$(".mask").hide();
		$("#reSellingNumDiv").hide();
    })
    
    //导入商品品牌
  	//实例化一个plupload上传对象
    var uploader = new plupload.Uploader({
        browse_button : 'importData', //触发文件选择对话框的按钮，为那个元素id
        url : contextPath+"/product/importData", //服务器端的上传页面地址
        file_data_name : 'file',
        flash_swf_url : contextPath + '/js/lib/plupload-2.1.2/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url : contextPath + '/js/lib/plupload-2.1.2/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        filters: {
      	      mime_types : [ //只允许上传图片文件和rar压缩文件
      	        { title : "product", extensions:"xls"}, 
      	      ],
      	      max_file_size : '102400kb', //最大只能上传100kb的文件
      	      prevent_duplicates : false //允许队列中存在重复文件
      	    },
        multipart:true,
      	multi_selection:false
    });

    //在实例对象上调用init()方法进行初始化
    uploader.init();

    //当文件添加到上传队列后触发
    uploader.bind('FilesAdded',function(uploader1,files){
        uploader1.start(); //开始上传
    });
    //当上传队列发生变化后触发，即上传队列新增了文件或移除了文件。QueueChanged事件会比FilesAdded或FilesRemoved事件先触发
    uploader.bind('QueueChanged',function(uploader1,file){

    });

    //当队列中的某一个文件正要开始上传前触发
    uploader.bind('BeforeUpload',function(uploader1,file){
        $("#divContent").html("数据处理中，请稍等。。。");
        $(".pop-mask").show();
        $("#divInfo").show();
    });

    //当队列中的某一个文件上传完成后触发
    uploader.bind('FileUploaded', function (up, file, result) {
        var data = $.parseJSON(result.response);
        $("#divContent").html(data.message);
        $("#datagrid").trigger("reloadGrid");
        if(data.message!="导入完成"){
            $("#divContent").append("<br><input type='button' style='color:red;' value='导出数据异常商品' id='importError'>");
        }
        if(null!=data.error&& data.error=="重复"){
            $("#divContent").append("<br><input type='button' style='color:red;' value='导出商品编号、供应商编号重复商品' id='importcf'>");
        }
//    	window.location.reload()
    });
    $("#divContent").on("click",'#importError',function(){
        window.open(contextPath + "/product/importError");
    });
    $("#divContent").on("click",'#importcf',function(){
        window.open(contextPath + "/product/importcf");
    });
    //实例化一个plupload上传对象
    var uploader1 = new plupload.Uploader({
        browse_button : 'importData1', //触发文件选择对话框的按钮，为那个元素id
        url : contextPath+"/product/zaifandaoru", //服务器端的上传页面地址
        file_data_name : 'file',
        flash_swf_url : contextPath + '/js/lib/plupload-2.1.2/Moxie.swf', //swf文件，当需要使用swf方式进行上传时需要配置该参数
        silverlight_xap_url : contextPath + '/js/lib/plupload-2.1.2/Moxie.xap', //silverlight文件，当需要使用silverlight方式进行上传时需要配置该参数
        filters: {
            mime_types : [ //只允许上传图片文件和rar压缩文件
                { title : "product", extensions:"xls"},
            ],
            max_file_size : '102400kb', //最大只能上传100kb的文件
            prevent_duplicates : false //允许队列中存在重复文件
        },
        multipart:true,
        multi_selection:false
    });
    //在实例对象上调用init()方法进行初始化
    uploader1.init();

    //当文件添加到上传队列后触发
    uploader1.bind('FilesAdded',function(uploader1,files){
        uploader1.start(); //开始上传
    });
    //当上传队列发生变化后触发，即上传队列新增了文件或移除了文件。QueueChanged事件会比FilesAdded或FilesRemoved事件先触发
    uploader1.bind('QueueChanged',function(uploader1,file){

    });

    //当队列中的某一个文件正要开始上传前触发
    uploader1.bind('BeforeUpload',function(uploader1,file){
        $("#divContent").html("数据处理中，请稍等。。。");
        $(".pop-mask").show();
        $("#divInfo").show();
    });

    //当队列中的某一个文件上传完成后触发
    uploader1.bind('FileUploaded', function (up, file, result) {
        var data = $.parseJSON(result.response);
        if("" != data.error){
        	if(data.error.indexOf("未能找到对应商品") != -1){
        		$("#notFindGoodsCode").val(data.notFindGoodsCode);
        		$("#divContent").html('<a class="operate-export" style="cursor:pointer" onclick="exportNotFindGoods();" id="export"><s class="icon-export-blue"></s>点击导出不存在商品</a><br/>'+data.error);
        	}else{
        		$("#divContent").html(data.error);
        	}   
        } else {
            $("#divContent").html("导入成功");
        }
        $("#datagrid").trigger("reloadGrid");
//    	window.location.reload()
    });


    
});

function hideDiv(){
	$(".pop-mask").hide();
    $("#divInfo").hide();
}

function exportNotFindGoods (){
	$("#notFindGoodsForm").submit();
}



function clickUpload(){
	var formData = new FormData($( "#uploadForm" )[0]); 
	$("#divContent").html("文件上传解压中，请稍等。。。");
    $(".pop-mask").show();
    $("#divInfo").show();
    $.ajax({
        url: 'uploadPackgeImg',  //server script to process data
        type: 'POST',
        xhr: function() {  // custom xhr
            myXhr = $.ajaxSettings.xhr();
            //if(myXhr.upload){ // check if upload property exists
            	//$("#loading").show();
                //myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // for handling the progress of the upload
            //}
            return myXhr;
        },
        //Ajax事件
        //beforeSend: beforeSendHandler,
        success: completeHandler,
        error: errorHandler,
        // Form数据
        data: formData,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false
    });
}
function progressHandlingFunction(e){
    if(e.lengthComputable){
        $('progress').attr({value:e.loaded,max:e.total});
    }
}
function completeHandler(msg){
	$("#divContent").html(msg);
}
function errorHandler(){
	$("#divContent").html("处理异常");
}
