require(['jquery','layer', 'jquery.validate','jqgrid.custom'], function($,layer) {
	function t(text) { return text; }
	var ctx = window.contextPath || '';
	
	var $grid1 = $('#member_list_grid').jqGrid({
        url: ctx+'/member/memberList'
        ,mtype : 'POST'
        ,datatype: 'json'
        
        ,colNames: ['id', '真实姓名', '联系电话', '邮箱', '用户昵称'],
            colModel: [
                
                {name: 'id', index: 'id', hidden: true, key: true},
                {name: 'lastName', index: 'lastName', template: 'text', searchoptions: {sopt: ["cn"]},width:150 },
                {name: 'phone', index: 'phone', template: 'text', searchoptions: {sopt: ["cn"]},width:150 },
                {name: 'email', index: 'email', template: 'text', searchoptions: {sopt: ["cn"]},width:235 },
                {name: 'nickName', index: 'nickName', template: 'text', searchoptions: {sopt: ["cn"]},width:250 },
            ]
        	, pager: '#member_list_pagination'
	        , rowNum: 10
	        //, filterbar: true
	        , altRows: false
	        , multiselect: true
	        , autowidth: false
	        , height: 'auto'
	        // , shrinkToFit: true
	        , sortname: 'lastModifiedDate'
	        , sortorder: 'desc'
    }).trigger('reloadGrid');
	
	
	
	
	$('#select_member_list').click(function (){
		var memberIds = $('#memberIds').val();
		var postData = {memberIds : memberIds};
		/*,postData:{
        	memberIds : memberIds
        }*/
		$('#member_list_grid').jqGrid("setGridParam", { postData: postData }).trigger("reloadGrid");
		
		
		
		
		
		layer.open({
			type: 1 
	  		  ,title: '添加会员'
	  		  ,area: ['880px', '550px']
	  		  ,content: $('#member_list_div') //这里content是一个普通的String或html标签
	  		  ,closeBtn:2
	  		  ,btn: ['确定', '取消']
			  ,yes:function(index){
				  var keys = $grid1.jqGrid('getGridParam', 'selarrrow');
				  if(keys.length<1){
					  layer.alert('至少选择一条数据！');
					  return false;
				  }else{
					  
					  $.ajax({
						  url: ctx+'/member/findMemberList',
			              type: 'post',
			              traditional: true,
			              data: {ids: keys}
					  }).done(function (data){
						  
						  if(null != data){
							  var content = '';
							  $.each(data,function(i,item){
								  content +='<tr>'
									  +'<td class="td_one"><a class="del_member" href="javascript:void(0)">删除</a>'
									  +'<input type="hidden" name="memberId" value="'+item.id+'"/></td>'
									  +'<td class="td_two">'+(item.nickName==null?'':item.nickName)+'</td>'
									  +'<td class="td_three">'+(item.phone==null?'':item.phone)+'</td>'
									  +'<td class="td_four">'+(item.email==null?'':item.email)+'</td>'
									  +'<td class="td_five">'+(item.lastName==null?'':item.lastName)+'</td>'
									  +'</tr>';
							  });
							  if(content != ''){
								  $('#select_list_div .member_list .content_tbody').append(content);
								  resetMemberIds();
								  $('#select_list_div').show();
							  }
							  
							  
						  }
							  
					  })
					  
				  }
			      layer.close(index);
			  },btn2:function(){
			  }
		});
		
		
		
	})
	
	$('.content_tbody').on('click','.del_member',function(){
        Glanway.Messager.confirm('警告', '您确定要删除选中的记录吗？',function (r){
            if(r){
                $(this).parent().parent().remove();
            }
        });
		resetMemberIds();
	});
	
	function resetMemberIds(){
		var $id_input = $('.content_tbody').find('input[name=memberId]');
		var ids = '';
		var len = $id_input.length;
		$id_input.each(function(i,item){
			var $item = $(item);
			if(i==len-1){
				ids += $item.val();
			}else{
				ids += $item.val()+',';
			}
		})
		$('#memberIds').val(ids);
	}
	$('#send').click(function(){
		resetMemberIds();
		var memberIds = $('#memberIds').val();
		if(null == memberIds||memberIds==''){
			layer.msg('请选择会员');
			return false;
		}
		$('#coupon-form').submit();
	});
	
});