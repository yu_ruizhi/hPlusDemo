<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/view/include/taglibs.jspf" %>
<layout:override name="head-ext">
    <script type="text/javascript" src="${ctx}/js/jquery-1.7.2.min.js"></script>
</layout:override>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<!-- Mirrored from www.zi-han.net/theme/hplus/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:18:23 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - 登录</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="${ctx}/favicon.ico">
    <link href="${ctx}/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="${ctx}/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">

    <link href="${ctx}/css/animate.min.css" rel="stylesheet">
    <link href="${ctx}/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html"/>
    <![endif]-->
    <script src="${ctx}/js/admin/modernizr-2.6.2.min.js"></script>
    <script>if (window.top !== window.self) {
        window.top.location = window.location;
    }</script>

    <style>

        .inp {
            border: 1px solid gray;
            padding: 0 10px;
            width: 200px;
            height: 30px;
            font-size: 18px;
        }

        /*.btn {
            border: 1px solid gray;
            width: 100px;
            height: 30px;
            font-size: 18px;
            cursor: pointer;
        }*/
        #embed-captcha {
            width: 300px;
            margin: 0 auto;
        }

        .show {
            display: block;
        }

        .hide {
            display: none;
        }

        #notice {
            color: red;
        }

        /* 以下遮罩层为demo.用户可自行设计实现 */
        #mask {
            display: none;
            position: fixed;
            text-align: center;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            overflow: auto;
        }

        /* 可自行设计实现captcha的位置大小 */
        .popup-mobile {
            position: relative;
        }

        #popup-captcha-mobile {
            position: fixed;
            display: none;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            z-index: 9999;
        }
    </style>

    <style type="text/css">
        .vail-code {
            width: 40% !important;
        }

        .form-group > input.vail-code, .form-group > label {
            text-align: left;
            float: left;
        }

        .form-group > label {
            margin: 5px 0 0 10px !important;
        }

        label > a {
            font-weight: 500;
            font-family: 宋体
        }
    </style>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>

            <%--<h1 class="logo-name">H+</h1>--%>
            <h1 class="logo-name">
                <img src="${ctx}/images/admin/admin-logo2.png" width="150" height="150" style="margin-bottom: 20px;"/>
            </h1>
        </div>
        <h3>欢迎使用X-Force管理系统</h3>
        <div class="m-t">
            <div class="form-group">
                <input type="text" class="form-control" name="loginName" placeholder="用户名" >
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="密码" >
            </div>
            <button type="submit" id="popup-submit" class="btn btn-primary block full-width m-b m-t">登 录</button>
            <div id="popup-captcha"></div>
           <%-- <p class="text-muted text-center">
                <a href="login.html#"><small>忘记密码了？</small></a> | <a href="register.html">注册一个新账号</a>
            </p>--%>

        </div>
    </div>
</div>
<script src="${ctx}/js/jquery.min.js?v=2.1.4"></script>
<script src="${ctx}/js/bootstrap.min.js?v=3.3.6"></script>
<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>


<!-- 引入封装了failback的接口--initGeetest -->
<script src="http://static.geetest.com/static/tools/gt.js"></script>

<script>
    var handlerPopup = function (captchaObj) {
        var $loginName = $('input[name=loginName]');
        var $password = $('input[name=password]');
        // 成功的回调
        captchaObj.onSuccess(function () {
            debugger;
            var validate = captchaObj.getValidate();
            $.ajax({
                url: "${ctx}/login/loginSubmit", // 进行二次验证
                type: "post",
                dataType: "json",
                data: {
                    username: $loginName.val(),
                    password: $password.val(),
                    geetest_challenge: validate.geetest_challenge,
                    geetest_validate: validate.geetest_validate,
                    geetest_seccode: validate.geetest_seccode
                },
                success: function (result) {
                    window.location.reload();
                },
                error:function (e) {
                    debugger;
                }
            });
        });
        $("#popup-submit").click(function () {
            if("" == $loginName.val()){
                $loginName.focus();
                return false;
            }
            if("" == $password.val()){
                $loginName.focus();
                return false;
            }
            captchaObj.show();
        });
        // 将验证码加到id为captcha的元素里
        captchaObj.appendTo("#popup-captcha");
        // 更多接口参考：http://www.geetest.com/install/sections/idx-client-sdk.html
    };
    // 验证开始需要向网站主后台获取id，challenge，success（是否启用failback）
    $.ajax({
        url: "pc-geetest/register?t=" + (new Date()).getTime(), // 加随机数防止缓存
        type: "get",
        dataType: "json",
        success: function (data) {
            // 使用initGeetest接口
            // 参数1：配置参数
            // 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
            initGeetest({
                gt: data.gt,
                challenge: data.challenge,
                product: "popup", // 产品形式，包括：float，embed，popup。注意只对PC版验证码有效
                offline: !data.success // 表示用户后台检测极验服务器是否宕机，一般不需要关注
                // 更多配置参数请参见：http://www.geetest.com/install/sections/idx-client-sdk.html#config
            }, handlerPopup);
        }
    });
</script>
<%--<script type="text/javascript">

    var handlerPopup = function (captchaObj) {
        // 成功的回调
        captchaObj.onSuccess(function () {
            var validate = captchaObj.getValidate();
            $.ajax({
                url: "${ctx}/login/loginSubmit", // 进行二次验证
                type: "post",
                dataType: "json",
                data: {
                    username: $('input[name=loginName]').val(),
                    password: $('input[name=password]').val(),
                    geetest_challenge: validate.geetest_challenge,
                    geetest_validate: validate.geetest_validate,
                    geetest_seccode: validate.geetest_seccode
                },
                success: function (data) {
                    if (data && (data.status === "success")) {
                        $(document.body).html('<h1>登录成功</h1>');
                    } else {
                        $(document.body).html('<h1>登录失败</h1>');
                    }

                }
            });
        });

        $("#popup-submit").click(function () {
            captchaObj.show();
        });


        captchaObj.appendTo("#popup-captcha"); // 将验证码加到id为captcha的元素里
    };


    // 验证开始需要向网站主后台获取id，challenge，success（是否启用failback）
    $.ajax({
        url: "${ctx}/login/generateGeetest?t=" + (new Date()).getTime(), // 加随机数防止缓存
        type: "get",
        dataType: "json",
        success: function (data) {
            // 使用initGeetest接口
            // 参数1：配置参数
            // 参数2：回调，回调的第一个参数验证码对象，之后可以使用它做appendTo之类的事件
            initGeetest({
                gt: data.gt,
                challenge: data.challenge,
                product: "popup", // 产品形式，包括：float，embed，popup。注意只对PC版验证码有效
                offline: !data.success // 表示用户后台检测极验服务器是否宕机，一般不需要关注
            }, handlerPopup);
        }
    });
</script>--%>
</body>


<!-- Mirrored from www.zi-han.net/theme/hplus/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Jan 2016 14:18:23 GMT -->
</html>
