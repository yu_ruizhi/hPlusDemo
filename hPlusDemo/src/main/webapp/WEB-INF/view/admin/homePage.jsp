<%@ page pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/view/include/taglibs.jspf" %>
<m:h>
    <%@include file="/WEB-INF/view/include/elibs.jspf" %>
</m:h>
<link rel="shortcut icon" href="favicon.ico">
<link href="${ctx}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${ctx}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="${ctx}/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="${ctx}/css/animate.min.css" rel="stylesheet">
<link href="${ctx}/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<link href="${ctx}/css/animate.min.css" rel="stylesheet">
<link href="${ctx}/css/plugins/chosen/chosen.css" rel="stylesheet">
<link href="${ctx}/css/style.min.css?v=4.1.0" rel="stylesheet">
<link href="${ctx}/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<link href="${ctx}/css/adminUser/admin.css" rel="stylesheet">

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <small>
                            <ul class="page-breadcrumb">
                                <li>
                                    <span>快捷菜单</span>
                                </li>
                            </ul>
                        </small>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="sitemap-two">
                        <c:forEach items="${modules}" var="module">
                            <h3 style="margin:20px 0 10px 0;color:#ccc"><b><a href="#" style='color:#666'>${module.name}</a></b></h3>
                            <p>
                                <c:forEach items="${module.pages}" var="pagez">
                                    <c:if test="${fn:contains(pagez.pageUrl,'index')}">
                                        <c:if test="${pagez.isShow}">
                                            <label><a href=${ctx}${pagez.pageUrl} style='margin-right:10px;font-weight:500;color:#666'>${pagez.name}</a></label> <%--|--%>
                                        </c:if>
                                    </c:if>
                                </c:forEach>
                            </p>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>