<%@ page pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/view/include/taglibs.jspf" %>
<m:h>
    <%@include file="/WEB-INF/view/include/elibs.jspf" %>
</m:h>
<!-- Begin 当前位置 -->
<div class="position">
    <span>当前位置：</span>
    <a href="${actx}/" title="首页">首页</a> &gt;
    <a href="${actx}/helpCenter/index">内容管理</a> &gt;
    <a href="${actx}/ad/index" title="广告管理">广告管理</a>
    &gt;编辑
</div>
<!-- //End 当前位置 -->
<div class="editPage">
    <sf:form id="ad-form" name="form" action="../update" method="post" commandName="ad">
        <table class="table-module01 form-table" cellpadding="0" cellspacing="0">
            <tbody>
            <input type="hidden" name="id" value="${ad.id}"/>
            <!-- E  ID id -->
            <!-- S  投放范围(广告位) adBlock -->
            <tr>
                <td align="right"><sf:label path="adBlock">投放范围：</sf:label>
                    <b class="color-red">*</b>
                </td>
                <td>
                    <sf:select path="adBlock" items="${adBlocks}"
                               itemLabel="name" itemValue="id" class="select chosen-select ni"
                               cssErrorClass="select chosen-select ni error"
                               data-placeholder="请选择">
                    </sf:select> <sf:label path="adBlock" cssClass="error">
                    <sf:errors path="adBlock"/>
                </sf:label>
                </td>
            </tr>
            <!-- E  投放范围(广告位) adBlock -->
            <!-- S  广告名称 name -->
            <tr>
                <td align="right">
                    <sf:label path="name">广告名称：</sf:label>
                    <b class="color-red">*</b>
                </td>
                <td>
                    <sf:input path="name" cssClass="input" cssErrorClass="input error"/>
                    <sf:label path="name" cssClass="error"><sf:errors path="name"/></sf:label>
                </td>
            </tr>
            <!-- E  广告名称 name -->
            <!-- S  广告类型(图片,flash) type -->
                <%--  <tr>
                     <td align="right" >
                         <sf:label path="type">广告类型(图片,flash)：</sf:label>

                     </td>
                     <td>
                         <sf:input path="type" cssClass="input" cssErrorClass="input error" />
                         <sf:label path="type" cssClass="error"><sf:errors path="type" /></sf:label>
                     </td>
                 </tr> --%>
            <!-- E  广告类型(图片,flash) type -->
            <!-- S  资源路径 path -->
            <tr>
                <td align="right">
                    <sf:label path="path">资源路径：</sf:label>

                </td>
                <td>
                    <div class="uploader" id="single-drop-zone">
                        <div class="upload-desc">请上传图片
                            <c:forEach items="${adBlocks}" var="adBlock">
                                <c:if test="${adBlock.id==1}">
                                    <span class="text1"
                                          style="display: none;color:#dd0000">请上传${adBlock.width}*${adBlock.height}像素的图片</span>
                                </c:if>
                                <c:if test="${adBlock.id==2}">
                                    <span class="text2"
                                          style="display: none;color:#dd0000">请上传${adBlock.width}*${adBlock.height}像素的图片</span>
                                </c:if>
                                <c:if test="${adBlock.id==4}">
                                    <span class="text3"
                                          style="display: none;color:#dd0000">请上传${adBlock.width}*${adBlock.height}像素的图片</span>
                                </c:if>
                                <c:if test="${adBlock.id==5}">
                                    <span class="text4"
                                          style="display: none;color:#dd0000">请上传${adBlock.width}*${adBlock.height}像素的图片</span>
                                </c:if>
                            </c:forEach>
                        </div>
                        <div class="upload-single">
                            <div class="actions">
                                <div class="actions">
                                        <span id="path" name="path" class="input">
                                            <a href="javascript:void(0);">选择文件</a>
                                        </span>
                                    <script type="text/javascript">
                                        require(['uploader2'], function (Uploader2) {
                                            Uploader2({
                                                file_data_name: 'file',
                                                browse_button: 'path',
                                                url: '${ctx}/img/preupload',
                                                policy: true,
                                                name: 'path',
                                                list: 'upload-queue',
                                                filters: {
                                                    mime_types: [{
                                                        title: 'Allowd Files',
                                                        extensions: 'jpg,png,gif'
                                                    }]
                                                },
                                                mode: 't',
                                                max_file_count: '1',
                                                max_file_size: '1m'
                                            });
                                        });
                                    </script>
                                </div>
                            </div>
                            <div class="upload-input">请选择...</div>
                            <ul class="upload-queue" id="upload-queue">
                                <li>
                                    <c:if test="${not empty ad.path}">
                                        <input type="hidden" name="path" value="${ad.path}"
                                               data-saved-url="${image_host}/${ad.path}"/>
                                    </c:if>
                                </li>
                            </ul>
                        </div>
                    </div>
                        <%-- <sf:input path="path" cssClass="input" cssErrorClass="input error" />
                                                <sf:label path="path" cssClass="error"><sf:errors path="path" /></sf:label> --%>
                </td>
            </tr>
            <!-- E  资源路径 path -->
            <!-- S  跳转链接 href -->
            <tr>
                <td align="right">
                    <sf:label path="href">跳转链接：</sf:label>

                </td>
                <td>
                    <sf:input path="href" cssClass="input" cssErrorClass="input error"/>
                    <sf:label path="href" cssClass="error"><sf:errors path="href"/></sf:label>
                </td>
            </tr>
            <!-- E  跳转链接 href -->
            <!-- S  广告说明 note -->
            <tr>
                <td align="right">
                    <sf:label path="note">广告说明：</sf:label>

                </td>
                <td>
                    <sf:input path="note" cssClass="input" cssErrorClass="input error"/>
                    <sf:label path="note" cssClass="error"><sf:errors path="note"/></sf:label>
                </td>
            </tr>
            <!-- E  广告说明 note -->
            <!-- S  排序 position -->
            <tr>
                <td align="right">
                    <sf:label path="position">排序：</sf:label>
                    <b class="color-red">*</b>
                </td>
                <td>
                    <ui:spinner path="position" cssClass="input ni" cssErrorClass="input ni error"/>
                    <sf:label path="position" cssClass="error"><sf:errors path="position"/></sf:label>
                </td>
            </tr>
            <!-- E  排序 position -->
            <!-- S  起始时间 startTime -->
            <tr>
                <td align="right">
                        <%--<sf:label path="startTime">--%>
                    起始时间：
                        <%--</sf:label>--%>
                    <b class="color-red">*</b>
                </td>
                <td>
                    <input id="startTime" name="startTimez" class="Wdate" type="text" readonly
                           value="<fmt:formatDate value="${ad.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                           onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss', minDate:'#F{$dp.$D(\'endTime\')}',maxDate:'2099-12-31'})"/>
                </td>
            </tr>
            <!-- E  起始时间 startTime -->
            <!-- S  结束时间 endTime -->
            <tr>
                <td align="right">
                    结束时间：
                    <b class="color-red">*</b>
                </td>
                <td>
                    <input id="endTime" name="endTimez" class="Wdate" type="text" readonly
                           value="<fmt:formatDate value="${ad.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
                           onFocus="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss', minDate:'#F{$dp.$D(\'startTime\')}',maxDate:'2099-12-31'})"/>
                </td>
            </tr>
            <!-- E  结束时间 endTime -->
            <!-- S  是否发布 enabled -->
            <tr>
                <td align="right">
                    <sf:label path="enabled">是否发布：</sf:label>

                </td>
                <td>
                    <sf:radiobutton path="enabled" label="是" value="1"/>
                    <sf:radiobutton path="enabled" label="否" value="0"/>
                    <sf:label path="enabled" cssClass="error"><sf:errors path="enabled"/></sf:label>
                </td>
            </tr>
            <!-- E  是否发布 enabled -->
            <tr>
                <td></td>
                <td align="left">
                        <span class="btn-sure-wrap">
                            <input class="btn-sure btn-edit" type="submit" value="保存"/>
                        </span>
                    <span class="btn-cancel-wrap">
                            <input type="button" class="btn-cancel" value="返回" onclick="history.go(-1);">
                        </span>
                </td>
            </tr>
            </tbody>
        </table>
    </sf:form>
</div>
<script type="text/javascript">
    <%--// <![CDATA[
    require(['jquery', 'jquery.validate','datepicker', 'chosen'], function($) {
        $('.chosen-select').chosen({});
        var checks= $("select[name='adBlock']").val();
        if(checks==1){
            $(".text1").show();
            $(".text2,.text3,.text4").hide();
        }else if(checks==2){
            $(".text2").show();
            $(".text1,.text3,.text4").hide();
        }else if(checks==4){
            $(".text1,.text2,.text4").hide();
            $(".text3").show();
        }else if(checks==5){
            $(".text1,.text2,.text3").hide();
            $(".text4").show();
        }
        $("select[name='adBlock']").change(function(){
            var val = $(this).val();
            if(val==1){
                $(".text1").show();
                $(".text2,.text3,.text4").hide();
            }else if(val==2){
                $(".text2").show();
                $(".text1,.text3,.text4").hide();
            }else if(val==4){
                $(".text1,.text2,.text4").hide();
                $(".text3").show();
            }else if(val==5){
                $(".text1,.text2,.text3").hide();
                $(".text4").show();
            }
        });
        $('#ad-form').validate({
            "ignore" : ":hidden:not(.ni)",
            "rules" : {
"id" : { },
"adBlock" : {
  "required" : true
},
"name" : {
"required" : true,
"minlength" : 0,
"maxlength" : 255
},
"type" : {
"minlength" : 0,
"maxlength" : 255
},
"path" : {
"minlength" : 0,
"maxlength" : 255
},
"href" : {
"minlength" : 0,
"maxlength" : 255
},
"note" : {
"minlength" : 0,
"maxlength" : 255
},
"position" : {"required" : true },
"startTime" : {"required":true },
"endTime" : { "required": true },
"enabled" : { "required": true },
"deleted" : {

},
"createdBy" : { },
"createdDate" : { },
"lastModifiedBy" : { },
"lastModifiedDate" : { }
}
        });
    });
    // ]]>
</script>
</layout:override>
<%@include file="/WEB-INF/view/include/admin-layout.jspf" %>--%>
