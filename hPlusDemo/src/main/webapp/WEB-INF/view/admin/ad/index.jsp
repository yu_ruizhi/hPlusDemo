<%@ page pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/view/include/taglibs.jspf" %>
<m:h>
    <%@include file="/WEB-INF/view/include/elibs.jspf" %>
</m:h>
<link rel="shortcut icon" href="favicon.ico">
<link href="${ctx}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
<link href="${ctx}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
<link href="${ctx}/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="${ctx}/css/animate.min.css" rel="stylesheet">
<link href="${ctx}/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
<link href="${ctx}/css/animate.min.css" rel="stylesheet">
<link href="${ctx}/css/plugins/chosen/chosen.css" rel="stylesheet">
<link href="${ctx}/css/style.min.css?v=4.1.0" rel="stylesheet">
<link href="${ctx}/css/style.min862f.css?v=4.1.0" rel="stylesheet">
<link href="${ctx}/css/adminUser/admin.css" rel="stylesheet">

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <small>
                            <ul class="page-breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="${actx}/" title="首页" target="_top">首页</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="${actx}/ad/index" title="广告管理">广告管理</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <span>广告列表</span>
                                </li>
                            </ul>
                        </small>
                    </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="${ctx}/ad/index">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="form_basic.html#">选项1</a>
                            </li>
                            <li><a href="form_basic.html#">选项2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="operateBar">
                        <%--<ul>
                            <li><a class="operate-delete" href="javascript:void(0);"><s class="icon-delete"></s>删除</a>
                            </li>
                            <li><a class="operate-add" href="add"><s class="icon-add-green"></s>新增</a></li>
                        </ul>--%>

                        <ul class="ibox float-e-margins">
                            <a href="add" class="btn btn-outline btn-default">新增</a>
                            <a href="add" class="btn btn-outline btn-success">编辑</a>
                            <a href="add" class="btn btn-outline btn-info">发送</a>
                            <a href="add" class="btn btn-outline btn-warning">删除</a>
                            <a href="add" class="btn btn-outline btn-danger">导入</a>
                            <a href="add" class="btn btn-outline btn-link">导出</a>
                        </ul>
                    </div>
                    <!-- //End 操作条 -->
                    <div class="table-module03">
                        <table id="datagrid"></table>
                        <div id="pagination"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="${ctx}/js/jq.Slide.js"></script>

<script type="text/javascript">

    // S data grid
    var $grid = $('#datagrid').jqGrid({
        url: 'list'
        , datatype: 'json'
        , colNames: [
            '操作', 'ID', '广告名称', '投放范围', '广告图片', '排序', '起始时间', '结束时间', '是否发布'
        ]
        , colModel: [
            {template: 'actions2'}
            , {name: 'id', index: 'id', hidden: true}
            , {name: 'name', index: 'name', template: "text"}
            , {name: 'adBlock.name', index: 'adBlock.name', template: "text"}
            , {name: 'path', index: 'path', template: "img"}
            , {name: 'position', index: 'position', template: "text"}
            , {name: 'startTime', index: 'startTime', template: "date", search: false}
            , {name: 'endTime', index: 'endTime', template: "date", search: false}
            , {name: 'enabled', index: 'enabled', template: "bool"}
        ]
        , pager: '#pagination'
        , filterbar: true
        , altRows: false
        , multiselect: true
        , autowidth: true
        , height: 'auto'
        , sortname: 'createdDate'
        , sortorder: 'desc'
        , batchDelBtn: '.operateBar .operate-delete'
    });
    // E data grid

    $('.operateBar .operate-delete').click(function () {
        var keys = $g.jqGrid('getGridParam', 'selarrrow');
        1 > keys.length ? Glanway.Messager.alert("提示", "您至少应该选择一行记录") : Glanway.Messager.confirm("警告", "您确定要删除选择的" + keys.length + "行记录吗？", function (r) {
            r && $.ajax({
                url: 'delete',
                type: 'post',
                traditional: true,
                data: {"id": keys}
            }).done(function (data) {
                var removed;
                if (data.success) {
                    removed = data.success || [];
                    $g.trigger("reloadGrid");
                    $.messager.show({
                        title: '提示',
                        text: '操作成功',
                        sticky: false,
                        time: 3000
                    });
                }
            }).fail(function () {
                $.messager.show({
                    title: '提示',
                    text: '操作失败',
                    sticky: false,
                    time: 3000
                });
            });
        });
    });

    $('.operateBar .operate-all').click(function () {
        $g.jqGrid('resetSelection');
        var ids = $g.getDataIDs();
        for (var i = 0, il = ids.length; i < il; i++) {
            $g.jqGrid('setSelection', ids[i], true);
        }
    });

    $('.operateBar .operate-cancel').click(function () {
        $g.jqGrid('resetSelection');
    });

</script>

</body>
</html>
