<%@ page pageEncoding="UTF-8" isELIgnored="false" %>
<%@ include file="/WEB-INF/view/include/taglibs.jspf" %>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>H+ 后台主题UI框架 - 基本表单</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="${ctx}/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="${ctx}/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="${ctx}/css/animate.min.css" rel="stylesheet">
    <link href="${ctx}/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="${ctx}/css/animate.min.css" rel="stylesheet">
    <link href="${ctx}/css/plugins/chosen/chosen.css" rel="stylesheet">

    <%--S Glanway  --%>
    <%-- chosen 未生效--%>
    <link href="http://cdn.bootcss.com/chosen/1.4.2/chosen.css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="chosen"
            src="http://cdn.bootcss.com/chosen/1.4.2/chosen.jquery.min.js"></script>

    <%-- uploader2 上傳--%>
    <link href="${ctx}/js/adminUser/lib/pui/uploader2/uploader2.css" rel="stylesheet">
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="uploader2"
            src="/js/adminUser/lib/pui/uploader2/uploader2.js"></script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="plupload"
            src="/js/adminUser/lib/plupload-2.1.2/plupload.full.min.js"></script>

    <link href="${ctx}/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="${ctx}/css/style.min.css?v=4.1.0" rel="stylesheet">
    <link href="${ctx}/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="${ctx}/css/adminUser/admin.css" rel="stylesheet">




    <%--E Glanway--%>
</head>

<body class="gray-bg">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <small>
                            <ul class="page-breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="${actx}/" title="首页">首页</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="${actx}/ad/index" title="广告管理">广告管理</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <span>添加广告</span>
                                </li>
                            </ul>
                        </small>
                    </h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="form_basic.html#">
                            <i class="fa fa-refresh"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="form_basic.html#">选项1</a>
                            </li>
                            <li><a href="form_basic.html#">选项2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <sf:form class="form-horizontal" id="ad-form" name="form" action="save" method="post"
                             commandName="ad">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">投放范围</label>
                            <div class="col-sm-10">
                                <sf:select path="adBlock" items="${adBlocks}"
                                           itemLabel="name" itemValue="id" class="select chosen-select ni form-control"
                                           cssErrorClass="select chosen-select ni error"
                                           data-placeholder="请选择" id="dl_chose">
                                </sf:select> <sf:label path="adBlock" cssClass="error">
                                <sf:errors path="adBlock"/>
                            </sf:label>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">投放范围</label>
                            <div class="col-sm-10">
                                <select data-placeholder="选择省份..." class="cselect chosen-select ni form-control" tabindex="2">
                                    <option value="">请选择省份</option>
                                    <option value="110000" hassubinfo="true">北京</option>
                                    <option value="120000" hassubinfo="true">天津</option>
                                    <option value="130000" hassubinfo="true">河北省</option>
                                    <option value="140000" hassubinfo="true">山西省</option>
                                    <option value="150000" hassubinfo="true">内蒙古自治区</option>
                                    <option value="210000" hassubinfo="true">辽宁省</option>
                                    <option value="220000" hassubinfo="true">吉林省</option>
                                    <option value="230000" hassubinfo="true">黑龙江省</option>
                                    <option value="310000" hassubinfo="true">上海</option>
                                    <option value="320000" hassubinfo="true">江苏省</option>
                                    <option value="330000" hassubinfo="true">浙江省</option>
                                    <option value="340000" hassubinfo="true">安徽省</option>
                                    <option value="350000" hassubinfo="true">福建省</option>
                                    <option value="360000" hassubinfo="true">江西省</option>
                                    <option value="370000" hassubinfo="true">山东省</option>
                                    <option value="410000" hassubinfo="true">河南省</option>
                                    <option value="420000" hassubinfo="true">湖北省</option>
                                    <option value="430000" hassubinfo="true">湖南省</option>
                                    <option value="440000" hassubinfo="true">广东省</option>
                                    <option value="450000" hassubinfo="true">广西壮族自治区</option>
                                    <option value="460000" hassubinfo="true">海南省</option>
                                    <option value="500000" hassubinfo="true">重庆</option>
                                    <option value="510000" hassubinfo="true">四川省</option>
                                    <option value="520000" hassubinfo="true">贵州省</option>
                                    <option value="530000" hassubinfo="true">云南省</option>
                                    <option value="540000" hassubinfo="true">西藏自治区</option>
                                    <option value="610000" hassubinfo="true">陕西省</option>
                                    <option value="620000" hassubinfo="true">甘肃省</option>
                                    <option value="630000" hassubinfo="true">青海省</option>
                                    <option value="640000" hassubinfo="true">宁夏回族自治区</option>
                                    <option value="650000" hassubinfo="true">新疆维吾尔自治区</option>
                                    <option value="710000" hassubinfo="true">台湾省</option>
                                    <option value="810000" hassubinfo="true">香港特别行政区</option>
                                    <option value="820000" hassubinfo="true">澳门特别行政区</option>
                                    <option value="990000" hassubinfo="true">海外</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">广告名称</label>

                            <div class="col-sm-10">
                                <input type="text" name="" placeholder="广告名称" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">广告图片</label>
                            <div class="col-sm-10">
                                <div class="uploader" id="single-drop-zone">
                                    <div class="upload-single">
                                        <div class="actions">
                                            <div class="actions">
                                        <span id="path" name="path" class="input">
                                            <a href="javascript:void(0);">选择文件</a>
                                        </span>
                                                <script type="text/javascript">
                                                    require(['uploader2'], function (Uploader2) {
                                                        Uploader2({
                                                            file_data_name: 'file',
                                                            browse_button: 'path',
                                                            url: '${ctx}/img/preupload',
                                                            policy: true,
                                                            name: 'path',
                                                            list: 'upload-queue',
                                                            filters: {
                                                                mime_types: [{
                                                                    title: 'Allowd Files',
                                                                    extensions: 'jpg,png,gif'
                                                                }]
                                                            },
                                                            mode: 't',
                                                            max_file_count: '1',
                                                            max_file_size: '1m'
                                                        });
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="upload-input">请选择文件...</div>
                                        <ul class="upload-queue" id="upload-queue">
                                            <li>
                                                <c:if test="${not empty ad.path}">
                                                    <input type="hidden" name="path" value="${ad.path}"
                                                           data-value="${image_host}/${ad.path}"/>
                                                </c:if>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="help-block m-b-none">请上传固定像素的图片</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">广告链接</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">广告说明</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control">
                                <span class="help-block m-b-none">广告说明，可能会超过一行，以块级元素显示</span>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">排序</label>

                            <div class="col-sm-10">
                                <input type="text" name="" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">起始时间</label>
                            <div class="col-sm-10">
                                <input class="laydate-icon form-control layer-date" id="start"
                                       placeholder="YYYY-MM-DD hh:mm:ss"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">结束时间</label>
                            <div class="col-sm-10">
                                <input class="laydate-icon form-control layer-date" id="end"
                                       placeholder="YYYY-MM-DD hh:mm:ss"
                                       onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">发布开关</label>
                            <div class="col-sm-10">
                                <div class="switch" style="margin-top: 7px;">
                                    <div class="onoffswitch">
                                        <input type="checkbox" checked class="onoffswitch-checkbox" id="example1">
                                        <label class="onoffswitch-label" for="example1">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit" data-form-sbm="1482136821317.2554">保存内容</button>
                                <button class="btn btn-white" type="submit">取消</button>
                            </div>
                        </div>
                    </sf:form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${ctx}/js/jquery.min.js?v=2.1.4"></script>
<script src="${ctx}/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${ctx}/js/content.min.js?v=1.0.0"></script>
<script src="${ctx}/js/plugins/iCheck/icheck.min.js"></script>
<script src="${ctx}/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${ctx}/js/plugins/validate/messages_zh.min.js"></script>
<script src="${ctx}/js/demo/form-validate-demo.min.js"></script>
<script src="${ctx}/js/plugins/layer/laydate/laydate.js"></script>
<script src="${ctx}/js/plugins/chosen/chosen.jquery.js"></script>
<script src="${ctx}/js/plugins/switchery/switchery.js"></script>
<script src="${ctx}/js/demo/form-advanced-demo.min.js"></script>

<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>

<script>
    $(document).ready(function () {
        $(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",})
    });
</script>


</body>
</html>