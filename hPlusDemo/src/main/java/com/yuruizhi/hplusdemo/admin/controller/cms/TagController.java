package com.yuruizhi.hplusdemo.admin.controller.cms;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Tag;
import com.yuruizhi.hplusdemo.admin.service.cms.TagService;
import com.google.common.collect.ImmutableMap;

/**
 * 文章标签
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/tag")
public class TagController extends BaseController {
	@Autowired
	private TagService tagService;

	/**
	 * 跳转到首页
	 */
	@RequestMapping("index")
	public void index() {}

	/**
	 * 跳转到增加页面
	 */
	@RequestMapping("add")
	public void add() {}

	@ResponseBody
	@RequestMapping("list")
	public Page<Tag> list(Filters filetes, Pageable pageable) {
		Page<Tag> list = tagService.findPage(filetes, pageable);
		return list;
	}

	/**
	 * 删除多条数据
	 */

	@ResponseBody
	@RequestMapping("delete")
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		tagService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}

	/**
	 * 增加
	 */
	@ResponseBody
	@RequestMapping("save")
	public ModelAndView delete(Tag tag) {
		ModelAndView mav = new ModelAndView();
		tag.setCreatedDate(new Date());
		tagService.save(tag);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}

	/**
	 * 修改
	 */

	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		Tag tag = tagService.find(id);
		mav.addObject("tag", tag);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	@ResponseBody
	@RequestMapping("update")
	public ModelAndView update(Tag tag) {
		ModelAndView mav = new ModelAndView();
		tag.setLastModifiedDate(new Date());
		tagService.update(tag);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}
	
	
	
	
	@ResponseBody
	@RequestMapping("isUnique")
	public Boolean isUnique(String tagName,Long id) {
		return tagService.isUnique(tagName,id);
	}

}
