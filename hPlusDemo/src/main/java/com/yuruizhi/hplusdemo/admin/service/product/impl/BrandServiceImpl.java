/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.BrandDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Brand;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.BrandService;

/**
 * <p>名称: BrandServiceImpl</p>
 * <p>说明: 商品品牌服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:48:47   
 * @version: 1.0
 */
@Service
public class BrandServiceImpl extends BaseServiceImpl<Brand> implements BrandService {
    
	private BrandDao brandDao;
	
	@Autowired
	public void setBrandDao(BrandDao brandDao) {
		this.brandDao = brandDao;
		setCrudDao(brandDao);
	}

	@Override
	public List<Brand> findByName(String name) {
		return brandDao.findByName(name);
	}

}
