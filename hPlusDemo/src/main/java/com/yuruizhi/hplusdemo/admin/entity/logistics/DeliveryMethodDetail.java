/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.logistics;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: DeliveryMethodDetail</p>
 * <p>说明: 物流方式详情</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:47:35   
 * @version: 1.0
 */
public class DeliveryMethodDetail extends BaseEntity{
	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -5360925229257868199L;
	private Long deliveryMethodId;
    private DeliveryArea deliveryArea;
    private Boolean deleted;
    private String deliveryAreaId;

    public String getDeliveryAreaId() {
        return deliveryAreaId;
    }

    public void setDeliveryAreaId(String deliveryAreaId) {
        this.deliveryAreaId = deliveryAreaId;
    }

    public Long getDeliveryMethodId() {
        return deliveryMethodId;
    }

    public void setDeliveryMethodId(Long deliveryMethodId) {
        this.deliveryMethodId = deliveryMethodId;
    }

    public DeliveryArea getDeliveryArea() {
        return deliveryArea;
    }

    public void setDeliveryArea(DeliveryArea deliveryArea) {
        this.deliveryArea = deliveryArea;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
