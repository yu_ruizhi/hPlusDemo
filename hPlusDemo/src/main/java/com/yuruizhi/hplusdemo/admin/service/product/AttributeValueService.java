/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: AttributeValueService</p>
 * <p>说明: 产品属性值服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:59:36   
 * @version: 1.0
 */
public interface AttributeValueService extends BaseService<AttributeValue> {

    /**
     * 
     * <p>名称：根据value值及属性id获取属性</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    AttributeValue findByValueAndAttrId(String value,Long attributeId);
	
}
