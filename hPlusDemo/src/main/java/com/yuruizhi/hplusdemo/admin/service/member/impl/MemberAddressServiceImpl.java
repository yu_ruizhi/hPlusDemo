/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.member.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberAddressDao;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.member.MemberAddressService;

import java.util.Date;

/**
 * <p>名称: MemberAddressServiceImpl</p>
 * <p>说明: 会员 地址服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:23:15   
 * @version: 1.0
 */
@Service
public class MemberAddressServiceImpl extends BaseServiceImpl<MemberAddress> implements MemberAddressService {

	/**
	 * 
	 */
	@Autowired
	private MemberAddressDao memberAddressDao;
	

	@Autowired
    public void setUserAddressDao(MemberAddressDao memberAddressDao) {
        super.setCrudDao(memberAddressDao);
        this.memberAddressDao=memberAddressDao;
    }
	/**
	 * 新增
//	 * @see com.glanway.yihe.service.UserAddressService#insert(com.glanway.yihe.entity.UserAddress)
	 */
	@Override
	
	public void insert(MemberAddress memberAddress) {
        if(null != (memberAddress.getId())){
            memberAddress.setLastModifiedDate(new Date());
		    memberAddressDao.update(memberAddress);
        }else{
            memberAddress.setCreatedDate(new Date());
            memberAddress.setFrequence(0);
//            userAddress.setDefaulted(Constant.NOT_DELETED);
            memberAddress.setDeleted(Constant.NOT_DELETED);
            memberAddressDao.insert(memberAddress);
        }
	}

	/**
	 * 删除默认地址
//	 * @see com.glanway.yihe.service.UserAddressService#deleteDefault(com.glanway.yihe.entity.UserAddress)
	 */
	@Override
	
	public int deleteDefault(MemberAddress memberAddress) {
		return memberAddressDao.deleteDefault(memberAddress);
	}

	/**
	 * 修改默认地址
//	 * @see com.glanway.yihe.service.UserAddressService#updateDefault(com.glanway.yihe.entity.UserAddress)
	 */
	@Override
	
	public int updateDefault(MemberAddress memberAddress) {
		return memberAddressDao.updateDefault(memberAddress);
	}



}
