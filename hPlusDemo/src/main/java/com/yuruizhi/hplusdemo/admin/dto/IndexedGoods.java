package com.yuruizhi.hplusdemo.admin.dto;

import java.util.Date;
import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Label;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductImg;

/**
 * 索引类
 *
 * @author vacoor
 */
public class IndexedGoods {
    private Long id;
    private String code;
    private String barcode;
    private String title;
    private String image;
    private String price;
    private String marketPrice;
    private Long hits;          // 点击量
    private Long sales;         // 销量
    private String unit;
    private Date onSellDate;   // 上架时间
    private Date reserveDeadline;  //新增“预定截止时间”字段 20160909 yuruizhi
    private Date limitedDeadLine;  //新增“限购截止时间”
    private Long productId;
    private String productTitle;
    private Long brandId;
    private String brandName;
    private Integer shopGroup;
    private Long shopId;
    private String shopName;
    private String shopLogo;
    private List<Category> cats;     // 分类
    private String path;
    private String pathNames;

    private List<AttributeValue> propVals;
    private List<ProductImg> images;
    private List<Label> labels;

    private Long worksId;            //作品ID
    private String worksName;		 //作品名
    private Boolean reserved = false;		 //是否预定产品
    private Boolean limited = false; //是否限购
    private Boolean spot = false;    //是否现货
    private String appIntro;         //app商品简介
    private Integer trend;           //商品趋向   0：无 ，1：男，  2：女
    private Category category;       //分类

    public Date getLimitedDeadLine() {
        return limitedDeadLine;
    }

    public void setLimitedDeadLine(Date limitedDeadLine) {
        this.limitedDeadLine = limitedDeadLine;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public Date getOnSellDate() {
        return onSellDate;
    }

    public void setOnSellDate(Date onSellDate) {
        this.onSellDate = onSellDate;
    }

    public Date getReserveDeadline() {
        return reserveDeadline;
    }

    public void setReserveDeadline(Date reserveDeadline) {
        this.reserveDeadline = reserveDeadline;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getShopGroup() {
        return shopGroup;
    }

    public void setShopGroup(Integer shopGroup) {
        this.shopGroup = shopGroup;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLogo() {
        return shopLogo;
    }

    public void setShopLogo(String shopLogo) {
        this.shopLogo = shopLogo;
    }

    public List<Category> getCats() {
        return cats;
    }

    public void setCats(List<Category> cats) {
        this.cats = cats;
    }

    public List<AttributeValue> getPropVals() {
        return propVals;
    }

    public void setPropVals(List<AttributeValue> propVals) {
        this.propVals = propVals;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

    public List<ProductImg> getImages() {
        return images;
    }

    public void setImages(List<ProductImg> images) {
        this.images = images;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public Long getWorksId() {
        return worksId;
    }

    public void setWorksId(Long worksId) {
        this.worksId = worksId;
    }

    public String getWorksName() {
        return worksName;
    }

    public void setWorksName(String worksName) {
        this.worksName = worksName;
    }

    public Boolean getReserved() {
        return reserved;
    }

    public void setReserved(Boolean reserved) {
        this.reserved = reserved;
    }

    public Boolean getLimited() {
        return limited;
    }

    public void setLimited(Boolean limited) {
        this.limited = limited;
    }

    public Boolean getSpot() {
        return spot;
    }

    public void setSpot(Boolean spot) {
        this.spot = spot;
    }

    public String getAppIntro() {
        return appIntro;
    }

    public void setAppIntro(String appIntro) {
        this.appIntro = appIntro;
    }

    public Integer getTrend() {
        return trend;
    }

    public void setTrend(Integer trend) {
        this.trend = trend;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}