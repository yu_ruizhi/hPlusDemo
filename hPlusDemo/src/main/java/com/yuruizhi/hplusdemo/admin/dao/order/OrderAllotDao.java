/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.order;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;

import java.util.List;
import java.util.Map;

/**
 * Created on 2016-03-07 14:38:27
 *
 * @author 赵创
 */
public interface OrderAllotDao extends BaseDao<OrderAllot> {

    //根据相关条件进入仓库查询商品数量
    Integer goodsInWarehouseNum(Map<String,Object> map);

    void updateMap(Map<String,Object> map);

    List<OrderAllot> orderAllotInOrder(Map<String,Object> map);

    //查询商品中的预购库存
    Integer advanceStock(Map<String,Object> map);

    Long getOrderId(Long id);
}