package com.yuruizhi.hplusdemo.admin.dao.product;

import com.yuruizhi.hplusdemo.admin.entity.product.ProductAttributeValue;

public interface ProductAttributeValueDao  {

    void insert(ProductAttributeValue pav);
    ProductAttributeValue getAttributeValueByProductIdAndAttributeId(ProductAttributeValue pav);
}
