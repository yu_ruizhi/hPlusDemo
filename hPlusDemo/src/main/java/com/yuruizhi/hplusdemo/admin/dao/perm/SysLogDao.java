package com.yuruizhi.hplusdemo.admin.dao.perm;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.SysLog;


/**
 * @author 赵创 on 2016/4/23. 后台系统日志持久层接口
 */
public interface SysLogDao extends BaseDao<SysLog> {

}
