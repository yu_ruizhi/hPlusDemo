/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.details;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.details.Details;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;

import java.util.List;
import java.util.Map;

/**
 * Created on 2016-03-07 14:38:27
 *
 * @author 赵创
 */
public interface DetailsDao extends BaseDao<Details> {


}