/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.product.AttributeValueDao;
import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.AttributeValueService;
import com.google.common.collect.Maps;

/**
 * <p>名称: AttributeValueServiceImpl</p>
 * <p>说明: 产品属性值服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:48:12   
 * @version: 1.0
 */
@Service
public class AttributeValueServiceImpl extends BaseServiceImpl<AttributeValue> implements AttributeValueService {
	
	private AttributeValueDao attributeValueDao;
	
	@Autowired
	public void setAttributValueDao(AttributeValueDao attributeValueDao){
		this.attributeValueDao = attributeValueDao;
		setCrudDao(attributeValueDao);
	}

	@Override
	public AttributeValue findByValueAndAttrId(String value, Long attributeId) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("value", value);
		map.put("attributeId", attributeId);
		return attributeValueDao.findByValueAndAttrId(map);
	}


}
