package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.service.product.IndexingService;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * 索引控制器
 */
@Controller
@RequestMapping("indexing")
public class IndexingController {
    private static Logger LOG = LoggerFactory.getLogger(IndexingController.class);

    @Autowired
    private IndexingService indexingService;

    /**
     * 重建索引
     */
    @ResponseBody
    @RequestMapping("indexing")
    public Map<String, Object> indexing() {
        try {
            indexingService.rebuildIndex();
            return ImmutableMap.<String, Object>of(
                    "success", true,
                    "message", "indexed"
            );
        } catch (Exception ex) {
            LOG.warn("indexing error", ex);
            String msg = ex.getMessage();
            return ImmutableMap.<String, Object>of(
                "success", false,
                "message", null != msg ? msg : ""
            );
        }
    }

    /**
     * 清空索引
     */
    @ResponseBody
    @RequestMapping("clear")
    public Map<String, Object> clear() {
        try {
            indexingService.truncate();
            return ImmutableMap.<String, Object>of(
                    "success", true,
                    "message", ""
            );
        } catch (Exception ex) {
            LOG.warn("clear index error", ex);
            String msg = ex.getMessage();
            return ImmutableMap.<String, Object>of(
                    "success", false,
                    "message", null != msg ? msg : ""
            );
        }
    }
}
