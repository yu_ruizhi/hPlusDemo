package com.yuruizhi.hplusdemo.admin.controller.platform;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Article;
import com.yuruizhi.hplusdemo.admin.entity.cms.ArticleTag;
import com.yuruizhi.hplusdemo.admin.entity.cms.Channel;
import com.yuruizhi.hplusdemo.admin.entity.cms.Tag;
import com.yuruizhi.hplusdemo.admin.entity.perm.SysLog;
import com.yuruizhi.hplusdemo.admin.service.cms.ArticleService;
import com.yuruizhi.hplusdemo.admin.service.cms.ArticleTagService;
import com.yuruizhi.hplusdemo.admin.service.cms.ChannelService;
import com.yuruizhi.hplusdemo.admin.service.cms.TagService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.google.common.collect.ImmutableMap;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/6/1 0001.
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/website")
public class AgreementController extends AdminBaseController<Article> {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private ChannelService channelService;
    @Autowired
    private TagService tagService;

    @Autowired
    private ArticleTagService articleTagService;

    @Autowired
    private SysLogService sysLogService;


    /**
     * <p>名称：basicIndex</p>
     * <p>描述：跳转到类表页</p>
     * @author：ChenGuang
     * @param code
     * @param model
     * @return
     */
    @RequestMapping({"{code}/index", "{code}/"})
    public String basicIndex(@PathVariable("code") String code, Map<String, Object> model) {
        model.put("channel", channelService.findOne("code", code));
        if (code.equals("agreement")) {
            return getRelativeViewPath("agreement/index");
        }

        return getRelativeViewPath("articleBasic/index");
    }
    /**
     * <p>名称：list</p>
     * <p>描述：查询分页列表</p>
     * @author：ChenGuang
     * @param filters
     * @param pageable
     * @param code
     * @return
     */
    @ResponseBody
    @RequestMapping("{code}/list")
    public Page<Article> list(Filters filters, Pageable pageable, @PathVariable("code") String code) {
        Channel channel = channelService.findOne("code", code);
        if (null == channel) {
            return new SimplePage<Article>(Collections.<Article>emptyList());
        }

        filters.beginsWith("channel.path", channel.getPath());
        return articleService.findPage(filters, pageable);
    }

    /**
     * <p>名称：add</p>
     * <p>描述：跳到新增页面</p>
     * @author：ChenGuang
     * @param code
     * @return
     */
    @RequestMapping("{code}/add")
    public String add(Map<String, Object> model, @PathVariable("code") String code) {
        model.put("limit", channelService.findOne("code", code));
        model.put("code", code);
        if (code.equals("agreement")) {
            return getRelativeViewPath("agreement/add");
        }
        return getRelativeViewPath("articleBasic/add");

    }


    /**
     * <p>名称：save</p>
     * <p>描述：新增操作</p>
     * @author：ChenGuang
     * @param article
     * @param code
     * @return
     */

    @RequestMapping("{code}/save")
    public String save(Article article, @PathVariable("code") String code) {
        if (null == article.getChannel()) {
            article.setChannel(channelService.findOne("code", code));
        }
        article.setCreatedDate(new Date());
        if (true == article.getPublished()) {
            article.setPublishTime(new Date());
        }
        articleService.save(article);
        if (null != article.getArtTagName()&&!article.getArtTagName().trim().equals("")) {
            String[] tagArr = article.getArtTagName().split(",");
            for (String tarr : tagArr) {
                Tag newTag = tagService.findAndSave(tarr);//如存在返回对应标签，不存在保存后返回
                ArticleTag artTag = new ArticleTag();
                artTag.setArticleId(article.getId());
                artTag.setTagId(newTag.getId());
                artTag.setDeleted(false);
                articleTagService.save(artTag);
            }
        }
        //加入操作日志
        if(null != article){
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE,"网站协议-新增-ID:"+article.getId()+"标题:"+article.getTitle(), "网站协议管理");
        }
        return redirectViewPath(code + "/index");
    }


    /**
     * <p>名称：delete</p>
     * <p>描述：删除操作</p>
     * @author：ChenGuang
     * @param id
     * @param code
     * @return
     */
    @ResponseBody
    @RequestMapping("{code}/delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
        articleService.delete(id);
        return ImmutableMap.<String, Object>of("success", true);
    }


    /**
     * <p>名称：edit</p>
     * <p>描述：跳转到编辑页面</p>
     * @author：ChenGuang
     * @param id
     * @param code
     * @param model
     * @return
     */
    @RequestMapping("{code}/edit/{id}")
    public String edit(@PathVariable(value = "id") Long id, @PathVariable("code") String code, Map<String, Object> model) {
        model.put("limit", channelService.findOne("code", code));
//		ModelAndView mav = new ModelAndView();
        Article article = articleService.find(id);
        model.put("article", article);
        List<Tag> artTags = tagService.findByArtId(id);
        model.put("artTags", artTags);
        model.put("limit", channelService.findOne("code", code));
        model.put("code", code);
//		mav.setViewName(getRelativeViewPath(code+"/edit"));
        if (code.equals("agreement")) {
            return getRelativeViewPath("agreement/edit");
        }
        return getRelativeViewPath("articleBasic/edit");
    }

    /**
     * <p>名称：update</p>
     * <p>描述：修改操作</p>
     * @author：ChenGuang
     * @param article
     * @param code
     * @return
     */
    @RequestMapping("{code}/update")
    public String update(Article article, @PathVariable("code") String code) {
        Boolean published = article.getPublished();
        if (null != published && published) {
            article.setPublishTime(new Date());
        }
        if(article.getArtTagName() != null&&article.getArtTagName().trim().equals("")){
            articleTagService.delByArtId(article.getId());
        }

        if (null != article.getArtTagName()&&!article.getArtTagName().trim().equals("")) {
            articleTagService.delByArtId(article.getId());
            String[] tagArr = article.getArtTagName().split(",");
            for (String tarr : tagArr) {
                Tag newTag = tagService.findAndSave(tarr);//如存在返回对应标签，不存在保存后返回
                ArticleTag artTag = new ArticleTag();
                artTag.setArticleId(article.getId());
                artTag.setTagId(newTag.getId());
                artTag.setDeleted(false);
                articleTagService.save(artTag);
            }
        }
        articleService.update(article);
        //加入操作日志
        if(null != article){
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE,"网站协议-修改-ID:"+article.getId()+"标题:"+article.getTitle(), "网站协议管理");
        }
        return redirectViewPath(code + "/index");
    }
}
