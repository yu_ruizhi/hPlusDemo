/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.platform;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.ponly.config.Option;
import org.ponly.config.mgt.ConfigManager;
import org.ponly.spring.util.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.ControllerSupport;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 配置管理
 *
 * @author vacoor
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/config")
public class ConfigController extends ControllerSupport {
    @Autowired
    private ConfigManager configManager;

    @RequestMapping({"", "index"})
    public String __default() {
        return redirectViewPath("/");
    }

    @RequestMapping("/")
    public String index(Map<String, Object> model) {
        Map<String, List<Option>> grouped = Maps.newLinkedHashMap();
        for (Option option : configManager.getOptions()) {
            String name = option.getName();
            if (null == name || name.endsWith("$_$")) {
                continue;
            }

            String group = option.getCategory();
            group = null != group ? group : "";
            List<Option> vars = grouped.get(group);
            if (null == vars) {
                grouped.put(group, vars = Lists.newArrayList());
            }
            vars.add(option);
        }
        model.put("grouped", grouped);
        return getViewPath("config");
    }

    /**
     * 刷新系统配置
     */
    @ResponseBody
    @RequestMapping("refresh")
    public Map<String, Object> refresh() {
        SpringUtils.refresh();
        return ImmutableMap.<String, Object>of("success", true);
    }

    @RequestMapping("create")
    public String create(Option option) {
        if (StringUtils.hasText(option.getName())) {
            configManager.create(option);
        }
        return redirectViewPath("/");
    }

    @RequestMapping("save")
    public String save(@RequestParam Map<String, String> config) {
        Properties props = new Properties();
        for (Map.Entry<String, String> entry : config.entrySet()) {
            String key = entry.getKey();
            if (null != key && key.startsWith("cfg_")) {
                props.put(key.substring(4), entry.getValue());
            }
        }

        configManager.updateOptions(props);
        configManager.reload();
        return redirectViewPath("/");
    }

    @ResponseBody
    @RequestMapping("check")
    public Boolean check(String name) {
        return null == configManager.getOption(name);
    }

    @ResponseBody
    @RequestMapping("gc")
    public Map<String, Object> gc() {
        // garbage collector
        final long before = Runtime.getRuntime().freeMemory();
        Runtime.getRuntime().gc();
        final long after = Runtime.getRuntime().freeMemory();
        long size = (after - before) / 1024;
        return ImmutableMap.<String, Object>of("released", size + "KB");
    }
}
