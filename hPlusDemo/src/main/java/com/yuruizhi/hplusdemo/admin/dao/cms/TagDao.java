package com.yuruizhi.hplusdemo.admin.dao.cms;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.Tag;


public interface TagDao extends BaseDao<Tag> {

	/** 
	 * @Title: findByArtId 
	 * @Description: TODO
	 * @param id
	 * @return
	 * @return: List<Tag>
	 */
	List<Tag> findByArtId(Long id);

	int isUnique(@Param("tagName")String tagName,@Param("id")Long id);
	
}
