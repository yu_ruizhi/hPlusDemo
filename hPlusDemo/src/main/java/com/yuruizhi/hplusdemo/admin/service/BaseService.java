/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service;

import org.ponly.webbase.service.CrudService;

/**
 * <p>名称: BaseService</p>
 * <p>说明: 基础Service接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:09:53   
 * @version: 1.0
 */
public interface BaseService<E> extends CrudService<E, Long> {
}
