/*
 * Copyright (c) 2014, by Besture All right reserved. 
 *
 */
package com.yuruizhi.hplusdemo.admin.dao.product;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ParameterValue;

/**
 * Created on 2014-07-25 09:11:01
 *
 * @author crud generated
 */
public interface ParameterValueDao extends BaseDao<ParameterValue> {
    void deleteParameterValueByProductId(Long pid);
}