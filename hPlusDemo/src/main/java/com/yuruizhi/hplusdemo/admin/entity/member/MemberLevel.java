/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MemberLevel.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.member
 * Date: 2016年4月18日 上午11:36:45
 **/
package com.yuruizhi.hplusdemo.admin.entity.member;

import java.math.BigDecimal;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 会员等级实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月18日 上午11:36:45
 * @version: 1.0
 */
public class MemberLevel extends BaseEntity{

	private static final long serialVersionUID = 1L;

	/**@Fields name : 名称 */ 
	private String name;

	/**@Fields path : 图片 */ 
	private String path;

	/**@Fields levelDiscount : 等级折扣 */ 
	private Double levelDiscount;

	/**@Fields upgradeType : 升级方式 */ 
	private Integer upgradeType;

	/**@Fields monetary : 消费金额 */ 
	private BigDecimal monetary;

	/**@Fields volume : 交易量 */ 
	private Integer volume;

	/**@Fields describes : 描述 */ 
	private String describes;

	/**@Fields deleted : 删除标识 */ 
	private Boolean deleted = false;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Double getLevelDiscount() {
		return levelDiscount;
	}

	public void setLevelDiscount(Double levelDiscount) {
		this.levelDiscount = levelDiscount;
	}

	public Integer getUpgradeType() {
		return upgradeType;
	}

	public void setUpgradeType(Integer upgradeType) {
		this.upgradeType = upgradeType;
	}

	public BigDecimal getMonetary() {
		return monetary;
	}

	public void setMonetary(BigDecimal monetary) {
		this.monetary = monetary;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public String getDescribes() {
		return describes;
	}

	public void setDescribes(String describes) {
		this.describes = describes;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
}
