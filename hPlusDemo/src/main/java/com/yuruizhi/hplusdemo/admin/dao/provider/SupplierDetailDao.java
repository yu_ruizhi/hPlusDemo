/*
* Copyright (c) 2005, 2014 vacoor
*
* Licensed under the Apache License, Version 2.0 (the "License");
*/
package com.yuruizhi.hplusdemo.admin.dao.provider;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierDetail;

import java.util.List;

/**
* Created on 2014-10-30 17:10:16
*
* @author crud generated
*/
public interface SupplierDetailDao extends BaseDao<SupplierDetail> {
	public List<SupplierDetail> getBySupplierId(String supplierId);
	
}