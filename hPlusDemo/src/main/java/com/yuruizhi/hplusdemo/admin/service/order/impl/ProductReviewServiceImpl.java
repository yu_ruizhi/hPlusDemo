/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import org.springframework.stereotype.Service;


import org.springframework.beans.factory.annotation.Autowired;


import com.yuruizhi.hplusdemo.admin.dao.order.ProductReviewDao;
import com.yuruizhi.hplusdemo.admin.entity.order.ProductReview;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.order.ProductReviewService;

/**
 * <p>名称: ProductReviewServiceImpl</p>
 * <p>说明: 产品评价服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:31:01   
 * @version: 1.0
 */
@Service
public class ProductReviewServiceImpl extends BaseServiceImpl<ProductReview> implements ProductReviewService {
	
	@Autowired
	private ProductReviewDao productReviewDao;
	/* (non Javadoc) 
	 * @Title: findDetailById
	 * @Description: TODO
	 * @return 
	 * @see com.glanway.eshop.admin.service.order.ProductReviewService#findDetailById() 
	 */
	@Override
	public ProductReview findDetailById(Long id) {
		// TODO Auto-generated method stub
		return productReviewDao.findDetailById(id);
	}

}
