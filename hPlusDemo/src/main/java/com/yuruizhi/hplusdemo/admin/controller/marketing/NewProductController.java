/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: NewProductController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月20日 上午9:37:32
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.NewOrHot;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.marketing.NewOrHotService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 新品上架 Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（20160523 - zuoyang - 列表页数据过滤掉下架的单品）</p> 
 * 
 * @author：wentan
 * @date：2016年4月20日 上午9:37:32 
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "newProduct")
public class NewProductController extends AdminBaseController<NewOrHot> {

	@Autowired
	private NewOrHotService newOrHotProductService;
	
	@Autowired
	private GoodsService goodsService;

	/**
	 * 
	 * <p>名称：异步获取列表数据</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param newProductfilters
	 * @param goodsFilters
	 * @param categoryFilters
	 * @param pageable
	 * @return
	 */
	@RequestMapping("newProductList")
	@ResponseBody
	public Page<NewOrHot> list(@Qualifier("N.")Filters newProductfilters, 
								@Qualifier("G.")Filters goodsFilters,
								@Qualifier("C.")Filters categoryFilters,
								Pageable pageable) {
		newProductfilters.add("type", Filters.Operator.EQ, Constant.NEW_PRODUCT_TYPE);
		//update 20160523 zuoyang 列表页数据过滤掉下架的单品
		goodsFilters.add("on_sell", Filters.Operator.EQ, Constant.GOODS_STATUS_NORMAL);
		
		Page<NewOrHot> page = newOrHotProductService.findPage(newProductfilters, 
															  goodsFilters, 
															  categoryFilters, 
															  pageable);
		return page;
	}

	/**
	 * 
	 * <p>名称：异步请求-带分类信息的单品</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param goodsId 单品ID
	 * @return
	 */
	@RequestMapping({"getNewGoods","getAppHotGoods"})
	@ResponseBody
	public Map<String, Object> getNewGoods(Long goodsId) {
		Map<String, Object> model = Maps.newHashMap();
		if(null != goodsId){
			Goods goods = goodsService.findGoodsCategoryById(goodsId);
			model.put("goods", goods);
			model.put("success", true);
			return model;
		}
		model.put("success", false);
		return model;
	}
}
