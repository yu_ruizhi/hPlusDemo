/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.marketing.MemberCoupon;
import com.yuruizhi.hplusdemo.admin.service.marketing.MemberCouponService;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;

/**
 * <p>名称: MemberCouponServiceImpl</p>
 * <p>说明: 会员优惠券服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:22:12   
 * @version: 1.0
 */
@Service
public class MemberCouponServiceImpl extends BaseServiceImpl<MemberCoupon> implements MemberCouponService {
}
