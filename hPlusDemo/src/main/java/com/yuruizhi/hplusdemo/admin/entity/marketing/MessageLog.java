/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Message.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月20日上午10:50:45
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;

import java.util.List;

/**
 * 
 * <p>名称: 私信记录实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author： yuruizhi
 * @date：2016年8月19日上午14:01:17
 * @version: 1.0
 */
public class MessageLog extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	/**
	 * @Fields Long : 会员ID
	 */
	private Long memberId;

	/**
	 * @Fields membert : 会员实体
	 */
	private Member member;
	
	/**
	 * @Fields title : 标题
	 * @Column TITLE
	 */
	private String title;

	/**
	 * @Fields content : 内容
	 * @Column CONTENT
	 */
	private String content;
	
	/**
	 * @Fields state : 状态
	 * @Column STATE
	 * 0:未读  1；已读
	 */
	private Integer state;
	
	/**
	 * @Fields type : 类型
	 * 私信类型
	 * 1:社区  2:电商
	 */
	private Integer type;
	
	/**
	 * @Fields deleted : 删除标识
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;


	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


}
