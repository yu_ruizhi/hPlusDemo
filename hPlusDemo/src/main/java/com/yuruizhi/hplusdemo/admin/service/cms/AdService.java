/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms;

import com.yuruizhi.hplusdemo.admin.entity.cms.Ad;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: AdService</p>
 * <p>说明: 广告服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:04:13   
 * @version: 1.0
 */
public interface AdService extends BaseService<Ad> {
}
