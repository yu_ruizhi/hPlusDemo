/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Keyword.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.platform
 * Date: 2016年4月23日 下午1:30:16
 **/
package com.yuruizhi.hplusdemo.admin.entity.platform;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 搜索管理-关键字实体类</p>
 * <p>说明: 对应表T_KEYWORD</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月23日 下午1:30:16  
 * @version: 1.0
 */
public class Keyword extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields name : 关键词
	 * @Size 255
	 * @Column NAME
	 */
	private String name;
	
	/**
	 * @Fields sort : 排序
	 * @Column SORT
	 */
	private Integer sort;
	
	/**
	 * @Fields highlight : 是否高亮
	 * @Column HIGHLIGHT
	 */
	private Boolean highlight;
	
	/**
	 * @Fields deleted :　删除标示
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Boolean getHighlight() {
		return highlight;
	}

	public void setHighlight(Boolean highlight) {
		this.highlight = highlight;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
}
