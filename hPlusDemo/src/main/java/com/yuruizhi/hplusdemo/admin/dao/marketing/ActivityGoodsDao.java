/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ActivityGoodsDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月21日 下午2:14:53 
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ActivityGoods;

/**
 * 
 * <p>名称: 活动商品Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午2:14:53  
 * @version: 1.0
 */
public interface ActivityGoodsDao extends BaseDao<ActivityGoods> {

	/**
	 * 
	 * <p>名称：根据活动id获取活动商品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param activityId 活动Id
	 * @return 返回该id下所有活动商品
	 */
	List<ActivityGoods> findByActivityId(Long activityId);
	
	/**
	 * 
	 * <p>名称：根据商品id及活动id获取信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param map 传入商品id及活动id
	 * @return 返回活动商品信息
	 */
	ActivityGoods findByGoodsIdAndActivityId(Map<String,Object> map);
	
}
