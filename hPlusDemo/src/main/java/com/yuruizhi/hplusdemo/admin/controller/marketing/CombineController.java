/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Filters.Filter;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Combine;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CombineDetail;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.marketing.CombineService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;

import java.util.*;

/**
 * Created on 2016-03-18 09:50:39
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/combine")
public class CombineController extends AdminBaseController<Combine> {
	@Autowired
	private CombineService combineService;
	
	@Autowired
	private GoodsService goodsService;
	
	@ResponseBody
	@RequestMapping("goodsList")
	public Page<Goods> goodsList(Filters filters, Pageable pageable) {
		return goodsService.findPage(filters, pageable);
	}
	@ResponseBody
	@RequestMapping("attachGoodsList")
	public Page<Goods> attachGoodsList(Filters filters, Pageable pageable,Long pryGoodsId) {
		filters.add(Filter.ne("id", pryGoodsId));
 		return goodsService.findPage(filters, pageable);
	}
	
	@ResponseBody
	@RequestMapping("findGoods")
	public Goods findGoods(Long id){
		Goods goods = goodsService.find(id);
		return goods;
	}
	
	
	@ResponseBody
	@RequestMapping("findGoodsList")
	public List<Goods> findGoodsList(Long[] ids){
		List<Goods> goodsList = new ArrayList<Goods>();
		for (int i = 0; i < ids.length; i++) {
			goodsList.add(goodsService.find(ids[i]));
		}
		return goodsList;
	}
	
	@ResponseBody
	@RequestMapping("{channel}/combineSave")
	public String combineSave(@PathVariable("channel") String channel,Combine combine,String goodsIds,String productIds,String ids){
		if(combine!=null){
			if(combine.getId()==null&&ids==null){
				combineService.combineSave(combine,goodsIds,productIds,channel);
			}else if(combine.getId()!=null&&ids!=null){
				combineService.combineUpdate(combine,goodsIds,productIds,ids,channel);
			}
			
		}
		
		return null;
	}
	
	@RequestMapping("{channel}/edit/{id}")
	public String _compat(@PathVariable("id") Long id,@PathVariable("channel") String channel,Map<String,Object> model){
		Combine combine = combineService.find(id);
		if(combine!=null){
			List<CombineDetail> combineDetails = combine.getCombineDetails();
			String goodsIds = "";
			String productIds = "";
			String ids = "";
			int len = combineDetails.size();
			for (int i = 0; i < combineDetails.size(); i++) {
				if(!combineDetails.get(i).getIsPrimary()){
					goodsIds += combineDetails.get(i).getGoodsId();
					productIds += combineDetails.get(i).getProductId();
					ids += combineDetails.get(i).getId();
					if(i<len-1){
						goodsIds += ",";
						productIds += ",";
						ids += ",";
					}
				}
			}
			model.put("ids",ids);
			model.put("goodsIds", goodsIds);
			model.put("productIds", productIds);
			
		}
		model.put("combine", combine);
		model.put("channel", channel);
		return getRelativeViewPath(channel+"/edit");
	}
	@RequestMapping("{channel}/index")
	public String index(@PathVariable("channel")String channel,Map<String,Object> model){
		model.put("channel", channel);
		return getRelativeViewPath(channel+"/index");
	}
	
	@ResponseBody
    @RequestMapping("{channel}/list")
    public Page<Combine> list(@PathVariable("channel")String channel,Filters filters, Pageable pageable) {
		if(channel.equals("combo")){
			filters.add(Filter.eq("type",Combine.COMBO));
		}else if(channel.equals("accessory")){
			filters.add(Filter.eq("type", Combine.ACCESSORY));
		}
        return combineService.findPage(filters, pageable);
    }
	
	 @RequestMapping("{channel}/add")
	    public String input(@PathVariable("channel") String channel,Map<String,Object> model) {
		 	model.put("channel", channel);
	        return getRelativeViewPath(channel+"/add");
	 }
	
	@ResponseBody
	@RequestMapping("{channel}/delete")
	public Map<String,Object> combineDelete(@PathVariable("channel") String channel,Long[] id){
		Map<String,Object> model = new HashMap<String, Object>();
		if(null != id){
			combineService.combineDelete(id);
		}
		model.put("success", true);
		return model;
	}
}
