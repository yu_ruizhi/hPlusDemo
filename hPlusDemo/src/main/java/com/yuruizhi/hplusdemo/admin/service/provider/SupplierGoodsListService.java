/*
* Copyright (c) 2005, 2014 vacoor
*
* Licensed under the Apache License, Version 2.0 (the "License");
*/
package com.yuruizhi.hplusdemo.admin.service.provider;


import com.yuruizhi.hplusdemo.admin.dto.SupplierGoodsDTO;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierGoodsList;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.List;

/**
* Created on 2014-10-30 17:10:16
*
* @author crud generated
*/
public interface SupplierGoodsListService extends BaseService<SupplierGoodsList> {
	
	public List<SupplierGoodsList> getListBySupplierId(String id);
	
	public SupplierGoodsList getDetails(String goodsId, String supplierId);

    List<SupplierGoodsDTO> findSupplierGoods(String sid);

	void savaOrUpdate(SupplierGoodsList supplierGoodsList);
}
