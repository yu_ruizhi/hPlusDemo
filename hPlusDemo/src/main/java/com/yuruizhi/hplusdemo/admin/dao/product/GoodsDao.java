/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.product;


import org.ponly.webbase.dao.support.mbt.MyBatisMapper;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;

import java.util.List;
import java.util.Map;

/**
 * Created on 2015-06-26 15:11:37
 *
 * @author vacoor
 */
@MyBatisMapper
public interface GoodsDao extends BaseDao<Goods> {
   Goods findGoodsDetailById(String id);
   void deleteGoodsSpecValues(Long goodsId);
   void deleteProductGoods(Long pid);
    List<Goods> selectCategoryChildren(String cid);
    List<Goods>  selectLike(Map<String ,Object> map);
	Goods findGoodsCategoryById(Long goodsId);
void updateShoppingGoods(Goods goods);

	/**
	 * 
	 * <p>名称：根据商品id获取所以单品</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param productId
	 * @return
	 */
	List<Goods> findByProductId(Long productId);
	
	
	List<Map<String,Object>> getNewGoods(Map<String,Object> map);

	void updateReserveDeadline(Map<String,Object> map);

	Integer findGoodsCount(Map<String,Object> map);

	void updateproduct(Map<String,Object> map);

	void updategoods(Map<String,Object> map);

	Integer findGoodszCount(Map<String,Object> map);

}