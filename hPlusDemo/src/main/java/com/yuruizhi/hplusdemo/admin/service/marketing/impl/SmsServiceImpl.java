/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import com.yuruizhi.hplusdemo.admin.dao.marketing.SmsDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.SmsMemberDao;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Sms;
import com.yuruizhi.hplusdemo.admin.entity.marketing.SmsMember;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.SmsService;

/**
 * <p>名称: SmsServiceImpl</p>
 * <p>说明: 短信营销服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:25:47   
 * @version: 1.0
 */
@Service
public class SmsServiceImpl extends BaseServiceImpl<Sms> implements SmsService{
	
	@Autowired
	private SmsDao smsDao;
	
	@Autowired
	private SmsMemberDao smsMemberDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Override
	public void smsSave(Sms sms) {
		
		if(sms.getId()==null){//保存
			String memberIds = sms.getMemberIds();
			sms.setStatus(1);
			sms.setCreatedDate(new Date());
			sms.setLastModifiedDate(new Date());
			smsDao.save(sms);
			if(null != memberIds&&memberIds != ""){
				String[] ids = memberIds.split(",");
				for (int i = 0; i < ids.length; i++) {
					SmsMember smsMember = new SmsMember();
					smsMember.setSmsId(sms.getId());
					smsMember.setMemberId(Long.valueOf(ids[i]));
					smsMember.setCreatedDate(new Date());
					smsMember.setLastModifiedDate(new Date());
					smsMemberDao.save(smsMember);
				}
			}
		}else{//更新
			String memberIds = sms.getMemberIds();
			Sms oldSms = smsDao.find(sms.getId());
			oldSms.setSubject(sms.getSubject());
			oldSms.setContent(sms.getContent());
			oldSms.setScheduleTime(sms.getScheduleTime());
			oldSms.setLastModifiedDate(new Date());
			smsDao.update(oldSms);
			smsMemberDao.deleteBysmsId(sms.getId());
			if(null != memberIds&&memberIds != ""){
				String[] ids = memberIds.split(",");
				for (int i = 0; i < ids.length; i++) {
					SmsMember smsMember = new SmsMember();
					smsMember.setSmsId(sms.getId());
					smsMember.setMemberId(Long.valueOf(ids[i]));
					smsMember.setCreatedDate(new Date());
					smsMember.setLastModifiedDate(new Date());
					smsMemberDao.save(smsMember);
				}
			}
		}
		
		
	}

	@Override
	public void smsSend(Long[] ids) {
		for (Long id : ids) {
			@SuppressWarnings("unused")
			Sms sms = smsDao.find(id);
			List<Member> members = memberDao.getMembersBySmsId(id);
			List<String> phones = new ArrayList<String>();
			for (Member member : members) {
				if(null==member.getPhone()||member.getPhone().trim().equals("")){
					continue;
				}
				phones.add(member.getPhone());
			}
		}
		
	}
	
}
