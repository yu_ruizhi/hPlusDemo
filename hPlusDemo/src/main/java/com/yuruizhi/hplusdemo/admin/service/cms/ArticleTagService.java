/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.cms.ArticleTag;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ArticleTagService</p>
 * <p>说明: 文章标签服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:05:09   
 * @version: 1.0
 */
public interface ArticleTagService extends BaseService<ArticleTag> {

	List<ArticleTag> findByArtId(Long id);

	void delByArtId(Long id);

}
