package com.yuruizhi.hplusdemo.admin.util.sms;

import cn.emay.eucp.client.EmaySDKClient;
import cn.emay.eucp.webservice.SDKClient;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

/**
 * 亿美软通短信平台 - SDK 5.0 (WebService)(电子商务专版) hp2平台.
 *
 * @author vacoor
 */
public abstract class EmayEucpSmsSender extends AbstractSmsSender {
    private static final int PRIORITY_HIGH = 5;

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean doSendSms(String[] mobiles, String content, Date sendDate) {
        if (mobiles.length > 1000) {
            throw new IllegalArgumentException("to many mobiles, must be less than 1000");
        }

        final long sendTime = sendDate.getTime();
        final long now = new Date().getTime();
        final EmaySDKClient client = getClient();

        int ret;
        try {
            // 30s 以内直接发送
            if (TimeUnit.SECONDS.convert(30, TimeUnit.MILLISECONDS) > sendTime - now) {
                ret = client.sendSMS(mobiles, content, ""/*?*/, PRIORITY_HIGH);
                ret = registIfNecessary(client, ret) ? client.sendSMS(mobiles, content, ""/*?*/, PRIORITY_HIGH) : ret;
            } else {
                String format = new SimpleDateFormat("yyyyMMddHHmmss").format(sendDate);
                ret = client.sendScheduledSMSEx(mobiles, content, format, "UTF-8");
                ret = !registIfNecessary(client, ret) ? client.sendScheduledSMSEx(mobiles, content, format, "UTF-8") : ret;
            }

            handleReturn(ret);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }

        return true;
    }

    /**
     * 处理返回值, 如果需要注册, 则进行注册, 注册成功返回 true, 不需要注册返回 false
     *
     * @param ret 其他操作返回值
     * @return 注册成功返回 true, 不需要注册返回 false
     */
    private boolean registIfNecessary(final EmaySDKClient client, int ret) {
        final String serialPassword = getSerialPassword();
        // 如果没有注册, 第一次需要注册
        if (SERIAL_NO_UNREGISTERED == ret || SERIAL_NO_LOGOUT == ret) {
            try {
                ret = client.registEx(serialPassword);
            } catch (Exception ex) {
                throw new IllegalStateException(ex);
            }
            // throw new SmsAccountException("serial_number_unregistered");
            if (REGIST_NO_EX_S == ret) {
                throw new IllegalStateException("SERIAL_NUMBER_REGISTER_FAIL");
            }
            return true;
        }
        return false;
    }

    // @Override
    public long getOverage() {
        EmaySDKClient client = getClient();
        try {
            double balance = client.getBalance();
            if (0 > balance && registIfNecessary(client, (int) balance)) {
                balance = client.getBalance();
            }
            if (0 > balance) {
                handleReturn((int) balance);
            }
            double eachFee = client.getEachFee();
            return (int) Math.floor(balance / eachFee);
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    protected EmaySDKClient getClient() {
        final String endpoint = getWsUri();
        final String serialNumber = getSerialNumber();
        final String serialKey = getSerialPassword();
        if (null != endpoint && !"".equals(endpoint)) {
            try {
                final Constructor<EmaySDKClient> ctor = EmaySDKClient.class.getDeclaredConstructor();
                final Field sdkClientField = EmaySDKClient.class.getDeclaredField("_SDKClient");
                final Field emaySerialNoField = EmaySDKClient.class.getDeclaredField("softwareSerialNo");
                final Field emayKeyField = EmaySDKClient.class.getDeclaredField("key");

                ctor.setAccessible(true);
                sdkClientField.setAccessible(true);
                emaySerialNoField.setAccessible(true);
                emayKeyField.setAccessible(true);

                final EmaySDKClient emayClient = ctor.newInstance();
                final JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
                factory.setServiceClass(SDKClient.class);
                factory.setAddress(endpoint);

                sdkClientField.set(emayClient, factory.create());
                emaySerialNoField.set(emayClient, serialNumber);
                emayKeyField.set(emayClient, serialKey);

                return emayClient;
            } catch (Exception e) {
                // ignore
            }
        }
        return EmaySDKClient.getInstance(serialNumber, serialKey);
    }

    protected abstract String getWsUri();

    protected abstract String getSerialNumber();

    protected abstract String getSerialPassword();

    private static void handleReturn(int ret) {
        if (SUCCESS == ret) {
            return;
        }
        if (SERIAL_NO_ERROR == ret || SERIAL_KEY_ERROR == ret || SERIAL_PASSWORD_ERROR == ret
                || ILLEGAL_SERIAL_NO == ret || INCORRECT_PASSWORD == ret) {
            throw new IllegalStateException("INVALID_SERIAL_NUMBER_OR_KEY");
        }
        // 没有余额
        if (ZERO_BALANCE == ret || NOT_ENOUGH_BALANCE == ret) {
            throw new IllegalStateException("NOT_ENOUGH_BALANCE");
        }

        if (CLIENT_EXCEPTION == ret || ILLEGAL_FORMAT == ret
                || ILLEGAL_KEY_FORMAT == ret || ILLEGAL_FORWARD_FORMAT == ret
                || ILLEGAL_CO_ADDRESS_FORMAT == ret || ILLEGAL_CO_NAME_CN_FORMAT == ret
                || ILLEGAL_CO_SHORT_NAME_CN_FORMAT == ret || ILLEGAL_EMAIL_FORMAT == ret
                || ILLEGAL_CO_NAME_EN_FORMAT == ret || ILLEGAL_CO_SHORT_NAME_EN_FORMAT == ret
                || ILLEGAL_FAX_FORMAT == ret || ILLEGAL_CONTACT_FORMAT == ret
                || ILLEGAL_PHONE_FORMAT == ret || ILLEGAL_ZIP_CODE_FORMAT == ret
                || ILLEGAL_NEW_PASSWORD == ret || ILLEGAL_SMS_PACKET_SIZE == ret
                || ILLEGAL_CONTENT_FORMAT == ret || ILLEGAL_EXT_NO == ret
                || ILLEGAL_PROVIOR == ret || ILLEGAL_MOBILE == ret
                || ILLEGAL_TIME_FMT == ret || ILLEGAL_UNIQUE_NO == ret
                || ILLEGAL_CHARGE_SERIAL == ret || ILLEGAL_CHARGE_PASSWORD == ret) {
        }
        throw new IllegalStateException("UNKNOWN_ERROR: " + ret);
    }

    /*-
     * 客户端错误
     */
    private static final int CLIENT_EXCEPTION = -2;
    private static final int ILLEGAL_FORMAT = -9000;                // 非法的数据格式/超过数据库允许范围
    private static final int ILLEGAL_SERIAL_NO = -9001;             // 非法的序列号格式
    private static final int INCORRECT_PASSWORD = -9002;            // 无效密码
    private static final int ILLEGAL_KEY_FORMAT = -9003;            // 非法的客户端Key格式
    private static final int ILLEGAL_FORWARD_FORMAT = -9004;        // 非法的转发格式
    private static final int ILLEGAL_CO_ADDRESS_FORMAT = -9005;     // 非法的公司地址格式
    private static final int ILLEGAL_CO_NAME_CN_FORMAT = -9006;     // 非法的公司中文名格式
    private static final int ILLEGAL_CO_SHORT_NAME_CN_FORMAT = -9007;    // 非法的公司中文简称格式
    private static final int ILLEGAL_EMAIL_FORMAT = -9008;          // 非法的邮箱地址格式
    private static final int ILLEGAL_CO_NAME_EN_FORMAT = -9009;     // 非法的公司英文名格式
    private static final int ILLEGAL_CO_SHORT_NAME_EN_FORMAT = -9010;     // 非法的公司英文简称格式
    private static final int ILLEGAL_FAX_FORMAT = -9011;            // 非法的传真格式
    private static final int ILLEGAL_CONTACT_FORMAT = -9012;        // 非法的联系人格式
    private static final int ILLEGAL_PHONE_FORMAT = -9013;          // 非法的联系电话格式
    private static final int ILLEGAL_ZIP_CODE_FORMAT = -9014;       // 非法的邮编
    private static final int ILLEGAL_NEW_PASSWORD = -9015;          // 非法的新密码
    private static final int ILLEGAL_SMS_PACKET_SIZE = -9016;       // 非法的短信包大小超出范围
    private static final int ILLEGAL_CONTENT_FORMAT = -9017;        // 非法的短信内容格式
    private static final int ILLEGAL_EXT_NO = -9018;                // 非法的短信扩展号格式
    private static final int ILLEGAL_PROVIOR = -9019;               // 非法的短信优先级
    private static final int ILLEGAL_MOBILE = -9020;                // 非法的手机号码格式
    private static final int ILLEGAL_TIME_FMT = -9021;              // 短信定时时间格式错误
    private static final int ILLEGAL_UNIQUE_NO = -9022;             // 发送短信唯一序列值错误
    private static final int ILLEGAL_CHARGE_SERIAL = -9023;         // 无效的充值卡号
    private static final int ILLEGAL_CHARGE_PASSWORD = -9024;       // 无效的充值卡密码

    /**
     * 亿美软通服务器端异常
     */
    private static final int SUCCESS = 0;                   // 发送成功
    private static final int SYSTEM_EXCEPTION = -1;         // 系统异常
    private static final int UNSUPPORTED_COMMAND = -101;    // 命令不支持
    private static final int REGISTRY_REMOVE_FAIL = -102;   // RegistryTransInfo 删除信息失败
    private static final int REGISTRY_UPDATE_FAIL = -103;   // RegistryInfo 更新信息失败
    private static final int REQUEST_LIMIT = -104;          // 请求超过限制
    private static final int CO_REGIST_FAIL = -111;         // 企业注册失败
    private static final int SEND_FAIL = -117;              // 发送短信失败
    private static final int RECIVE_MO = -118;
    private static final int REV_REPORT = -119;

    private static final int CHANGE_PASSWORD_FAIL = -120;   // 修改密码失败
    private static final int NO_LOGOUT_FAIL = -122;         // 号码注销失败
    private static final int NO_REGIST_ACTIVE = -110;       // 号码注册失败
    private static final int QUERY_PRICE_UNIT = -123;       // 查询单价失败
    private static final int QUERY_BANLAN_FAIL = -124;      // 查询余额失败


    private static final int SET_FORWARD_MO = -125;         // 设置 MO 转发失败
    private static final int ROUTE_FAIL = -126;             // 路由信息失败
    private static final int ZERO_BALANCE = -127;           // 计费失败, 0余额
    private static final int NOT_ENOUGH_BALANCE = -128;     // 计费失败, 余额不足
    private static final int SERIAL_NO_ERROR = -1100;        // 序列号错误(不存在或尝试攻击的用户)
    private static final int SERIAL_KEY_ERROR = -1103;       // 序列号KEY错误
    private static final int SERIAL_PASSWORD_ERROR = -1102;  // 序列号密码错误

    private static final int ROUTE_ADMIN = -1104;               // 路由失败, 请联系管理员
    private static final int SERIAL_NO_UNREGISTERED = -1105;    // 注册号状态异常, 未用 1
    private static final int SERIAL_NO_LOGOUT = -1107;          // 注册号状态异常, 注销/停用 3
    private static final int REGIST_NO_EX_S = -1108;            // 注册号状态异常, 停止 5
    private static final int CHAGER_FAIL = -113;                // 充值失败
    private static final int CHARGE_NO_INVALID = -1131;     // 充值卡无效
    private static final int CHARGE_PASSWORD_INVALID = -1132; // 充值卡密码无效
    private static final int CHARGE_BIND = -1133; // 充值绑定异常
    private static final int CHARGE_STATUS = -1134; // 充值状态异常
    private static final int CHARGE_AMOUNT = -1135; // 充值金额异常

    private static final int DATA_FAIL = -190;
    private static final int DATA_INSERT = -1901;       // 数据库插入失败
    private static final int DATA_UPDATE = -1902;       // 数据库更新失败
    private static final int DATA_DELETE = -1903;       // 数据库删除失败
}
