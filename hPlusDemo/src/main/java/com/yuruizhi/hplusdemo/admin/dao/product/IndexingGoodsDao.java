package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dto.IndexedGoods;

/**
 */
public interface IndexingGoodsDao {
    String OFFSET = "offset";
    String MAX_RESULTS = "maxResults";

    IndexedGoods find(Long id);

    int count();

    List<IndexedGoods> findMany(Map<String, Object> paramsMap);

}
