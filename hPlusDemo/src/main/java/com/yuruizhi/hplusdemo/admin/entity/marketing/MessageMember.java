/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageMember.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月20日下午2:12:01
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 私信-会员中间实体</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日下午2:12:27
 * @version: 1.0
 */
public class MessageMember extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 *@Fields state : 消息状态
	 */
	private Integer state;
	
	/**
	 * @Fields memberId : 用户ID
	 * @Column MEMEBER_ID
	 */
	private Long memberId;

	/**
	 * @Fields messageId : 会员Id
	 * @Column MESSAGES_ID
	 */
	private Long messageId;
	
	/**
	 * @Fields deleted : 删除标识
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	

}
