/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity;

import org.ponly.webbase.entity.Auditable;
import org.ponly.webbase.entity.Persistable;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>名称: BaseEntity</p>
 * <p>说明: 基础实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:26:33   
 * @version: 1.0
 */
public abstract class BaseEntity implements Persistable<Long>, Auditable<String, Long>, Serializable {

    private static final long serialVersionUID = -1775772660674482854L;
    /**
     * @Id
     * @Column
     * @ViewField text=ID position=-9999
     */
    protected Long id;                  // ID
    /**
     * @Column
     * @ViewField text=创建人 position=996
     */
    protected String createdBy;        // 创建人
    /**
     * @Column
     * @ViewField text=创建时间 position=997
     */
    protected Date createdDate;        // 创建时间
    /**
     * @Column
     * @ViewField text=最后修改人 position=998
     */
    protected String lastModifiedBy;   // 最后修改人
    /**
     * @Column
     * @ViewField text=最后修改时间 position=999
     */
    protected Date lastModifiedDate;    // 最后修改时间

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCreatedBy() {
        return createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @Override
    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    @Override
    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        } else if (null != other && this.getClass() == other.getClass()) {
            BaseEntity that = (BaseEntity) other;
            Serializable id = this.getId();
            Serializable thatId = that.getId();
            return null != id && null != thatId && (id == thatId || id.equals(thatId));
        } else {
            return false;
        }
    }

    public int hashCode() {
        byte result = 1;
        Long id = this.getId();
        return 31 * result + (id == null ? 0 : id.hashCode());
    }

    protected Object clone() throws CloneNotSupportedException {
        BaseEntity entity = BaseEntity.class.cast(super.clone());
        entity.setId(null);
        return entity;
    }
}
