/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 * <p>
 * ProjectName: hPlusDemo
 * FileName: MessageController.java
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月20日 上午11:35:40
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Message;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageLog;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageLogService;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;

import org.ponly.common.json.Jacksons;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

import static com.yuruizhi.hplusdemo.admin.controller.ControllerSupport.SUCCESS_MSG_KEY;

/**
 *
 * <p>名称: MessageLogController</p>
 * <p>说明: 私信记录控制器</p>
 * <p>创建记录：（2016.8.19 14:13 - yuruizhi - 增加私信记录控制器）</p>
 *
 * @author yuruizhi
 * @date 2016年8月19日 下午14:13:06 <br/>
 * @change Log: 2016.8.19 15:00 - yuruizhi - 增加私信发送成功提示语 <br/>
 * @since JDK 1.7
 * @version: 1.0
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/messageLog")
public class MessageLogController extends AdminBaseController<MessageLog> {

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageLogService messageLogService;

    @Autowired
    private SysLogService sysLogService;

    /**
     *
     * <p>名称：发送私信</p>
     * <p>描述：</p>
     * @author yuruizhi
     * @param message 保存页面传入的数据，并封装为对象
     * @return 跳转路径
     */
    @RequestMapping("messageSend")
    public String messageSend(Message message, RedirectAttributes redirectAttributes) {

        List<Member> memberList = message.getMemberList();
        message = messageService.find(message.getId());
        MessageLog messageLog = new MessageLog();
        if(null != message) for (Member member : memberList) {
            messageLog.setMemberId(member.getId());

            //配置私信内容

            messageLog.setTitle(message.getTitle());
            messageLog.setContent(message.getContent());

            messageLog.setState(Constant.MESSAGE_STATE_UNREAD); //默认未读
            messageLog.setType(Constant.MESSAGE_TYPE_ESHOP); //电商私信

            messageLogService.save(messageLog);

            //TODD 私信管理 私信记录 - 系统发送给会员ID私信 - 私信记录ID：ID
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "私信-新增-向会员ID:" + member.getId() + "发送了私信ID:" + message.getId() + "的私信", "私信记录");

        }
        redirectAttributes.addFlashAttribute(SUCCESS_MSG_KEY, "发送成功");
        return redirectViewPath("index");
    }

    /**
     *
     * <p>名称：delete</p>
     * <p>描述：用户前台删除私信</p>
     * @author yuruizhi
     * @param  id 删除的私信记录ID
     * @return 跳转路径
     */
    @Override
    public Map<String, Object> delete(Long[] id) {
        String idStr = "";
        for (int i = 0; i < id.length; i++) {
            idStr += id[i] + ",";
        }
        idStr = idStr.substring(0, idStr.length() - 1);

        //TODD 日志为  会员 + 删除 + ID:
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "私信记录-删除-ID:" + idStr, "私信记录");
        return super.delete(id);
    }

    /**
     *
     * <p>名称：列表页-显示</p>
     * <p>描述：列表页默认展示电商私信</p>
     * @author yuruizhi
     * @param filters 过滤器
     * @param pageable 分页
     * @return
     */
    @ResponseBody
    @RequestMapping("getList")
    public Page<MessageLog> list(Filters filters,@Qualifier("M.") Filters memberFilters,Pageable pageable) {
        //return messageLogService.findPage(filters, memberFilters, pageable);
        return messageLogService.findPage( memberFilters, pageable);
    }
    

    
    
}
