/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: RefundImg</p>
 * <p>说明: 退换货图片实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:13:52   
 * @version: 1.0
 */
public class RefundImg extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = -7252970448946404592L;

	/**
     * 退换货ID
     * 1:退货退款, 2:仅退款, 3:换货
     * 
     * @ViewField editor=spinner 
     * @Column REFUND_ID
     */
    @NotNull
    private Integer refundId;

    /**
     * 图片路径
     * 
     * @ViewField editor=input 
     * @Column PATH
     */
    @Size(max = 255)
    private String path;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Integer getRefundId() {
        return this.refundId;
    }

    public void setRefundId(Integer refundId) {
        this.refundId = refundId;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
