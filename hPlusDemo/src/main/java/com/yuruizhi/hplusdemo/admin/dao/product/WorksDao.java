/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: WorksDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.product
 * Date: 2016年4月18日 下午5:34:20
 **/
package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;

/**
 * 
 * <p>名称: 作品DAO</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月18日 下午5:34:50   
 * @version: 1.0
 */
public interface WorksDao extends BaseDao<Works> {

	/**
	 * 
	 * <p>名称：获取符合条件的作品</p> 
	 * <p>描述：参数Map中的key与实体类中的属性名一致，多个条件之间是“AND”关系</p>
	 * @author：wentan
	 * @param paramMap
	 * @return
	 */
	List<Works> getBaseWorks(Map<String, Object> paramMap);

	/**
	 * 
	 * <p>名称：根据名称进行查找</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	List<Works> findByName(String name);
	
}
