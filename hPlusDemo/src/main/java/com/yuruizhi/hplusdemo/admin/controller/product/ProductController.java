package com.yuruizhi.hplusdemo.admin.controller.product;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yuruizhi.hplusdemo.admin.util.*;
import org.apache.james.mime4j.dom.datetime.DateTime;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.ponly.common.json.Jacksons;
import org.ponly.common.util.FileUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.aliyun.oss.OSSClient;
import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;
import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Brand;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsCheck;
import com.yuruizhi.hplusdemo.admin.entity.product.Label;
import com.yuruizhi.hplusdemo.admin.entity.product.Model;
import com.yuruizhi.hplusdemo.admin.entity.product.Parameter;
import com.yuruizhi.hplusdemo.admin.entity.product.ParameterValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductAttributeValue;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductImg;
import com.yuruizhi.hplusdemo.admin.entity.product.Spec;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;
import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.product.AttributeService;
import com.yuruizhi.hplusdemo.admin.service.product.AttributeValueService;
import com.yuruizhi.hplusdemo.admin.service.product.BrandService;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsCheckService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.yuruizhi.hplusdemo.admin.service.product.LabelService;
import com.yuruizhi.hplusdemo.admin.service.product.ModelService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductAttributeValueService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductImgService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductService;
import com.yuruizhi.hplusdemo.admin.service.product.SpecService;
import com.yuruizhi.hplusdemo.admin.service.product.SpecValueService;
import com.yuruizhi.hplusdemo.admin.service.product.WorksService;
import com.yuruizhi.hplusdemo.admin.service.provider.SupplierService;
import com.yuruizhi.hplusdemo.admin.validate.ProductValidate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/*import com.yuruizhi.hplusdemo.admin.entity.member.Provider;*/
/*import com.yuruizhi.hplusdemo.admin.service.member.ProviderService;*/

/**
 * Product Management Controller
 *
 * @author vacoor
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/product")
public class ProductController extends BaseController {
    public static String PRODUCT_IMPORT_ERROR="PRODUCT_IMPORT_ERROR_";
    public static String PRODUCT_CF_ERROR="PRODUCT_CF_ERROR_";
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductImgService productImgService;
    @Autowired
    private LabelService labelService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private WorksService worksService;
    @Autowired
    private SupplierService supplierService;
    /*
     * @Autowired private DetailTemplateService templateService;
     */
    @Autowired
    private ProductAttributeValueService pavService;
    @Autowired
    private SysLogService sysLogService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private SpecValueService speValueService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttributeService attributeService;
    @Autowired
    private AttributeValueService attrbuteValueService;
    @Autowired
    private SpecService specService;
    @Autowired
    private GoodsCheckService goodsCheckService;
    
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
        binder.registerCustomEditor(Category.class, "category", new CustomStringBeanPropertyEditor(Category.class));
        binder.registerCustomEditor(Model.class, "model", new CustomStringBeanPropertyEditor(Model.class));
        binder.registerCustomEditor(Brand.class, "brand", new CustomStringBeanPropertyEditor(Brand.class));
        binder.registerCustomEditor(Works.class, "works", new CustomStringBeanPropertyEditor(Works.class));
  /*      binder.registerCustomEditor(Provider.class, "provider", new CustomStringBeanPropertyEditor(Provider.class));*/
        binder.registerCustomEditor(List.class, "labels", new CustomStringCollectionBeanCollectionPropertyEditor(
                List.class, Label.class
        ));
    }
	
    @RequestMapping("index")
    public void index() {
    }

    @ResponseBody
    @RequestMapping("list")
    public Page<Product> list(@Qualifier("B.") Filters brandFilters, @Qualifier("M.") Filters modelFilters,
                              @Qualifier("C.") Filters categoryFilters, @Qualifier("W.") Filters worksFilters,
                              @Qualifier("PR.") Filters supplierFilters,@Qualifier("P.") Filters productFilters,
                              @Qualifier("R.") Filters rFilters,
                              @RequestParam(value = "goodsCode", required = false) String goodsCode,
                              Pageable pageable) {
    	
        return productService.findPage(productFilters, brandFilters, modelFilters,
        		                                      categoryFilters,worksFilters,supplierFilters, pageable,rFilters, goodsCode);
    }

    @RequestMapping("add")
    public void add(Map<String, Object> model) {
        // model.put("groupedBrands", getGroupedBrands());
        model.put("labels", labelService.findAll());
        model.put("baseModel", modelService.findBaseModel());
        model.put("providerList", supplierService.findAll());
        model.put("worksList", worksService.findAll());
        model.put("brandList", brandService.findMany("enabled", true));

        /* model.put("templates", templateService.findAll()); */
    }

	/*
	 * 保存商品
	 */
    @RequestMapping("save")
    public String save(Product product,
                       @RequestParam(value = "attributeValueId", required = false) String[] attrValIds,
                       @RequestParam(value = "value", required = false) String[] value, RedirectAttributes redirectAttr) {

        attrValIds = clean(attrValIds);

        clean(product);
        productService.saveOrUpdate(product);
        redirectAttr.addFlashAttribute("message", "保存成功");
        return redirectViewPath("index");
    }

    @RequestMapping("edit/{id}")
    public String edit(@PathVariable("id") Long id, Map<String, Object> model) {
        Product product = productService.find(id);
        if (null == product) {
            return null;
        }

        Category category = product.getCategory();
        if (category != null) {
            model.put("category", category);
            //获取自定义模型属性值处理
            Model m = category.getModel();
            if (m != null) {
                m = modelService.findDetail(m.getId());
                List<Attribute> attributes = m.getAttributes();
                attributes = null != attributes ? attributes : Collections.<Attribute>emptyList();
                for (int j = 0; j < attributes.size(); j++) {
                    Attribute attribute = attributes.get(j);
                    if (1 == attribute.getDisplayType()) {
                        ProductAttributeValue pav = new ProductAttributeValue();
                        pav.setAttribute(attribute);
                        pav.setProduct(product);
                        if (null != pavService.getAttributeValueByProductIdAndAttributeId(pav)) {
                            attribute.setAttributeValues(
                                    null == pavService.getAttributeValueByProductIdAndAttributeId(pav) ? null
                                            : pavService.getAttributeValueByProductIdAndAttributeId(pav)
                                            .getAttributeValueList());
                        } else {
                            attribute.setAttributeValues(null);
                        }

                    }
                }
                if(m.getParameters()!=null&&m.getParameters().size()>0){
                    List<Parameter> parameters=new ArrayList<Parameter>();
                    for(Parameter parameter:m.getParameters()){
                        if(null!=parameter.getChildren()&&parameter.getChildren().size()>0){
                            parameters.add(parameter);
                        }
                    }
                    m.setParameters(parameters);
                }
                model.put("model", m);
            }
        }
      //获取基本模型属性值处理
        Model base = modelService.findBaseModel();
        List<Attribute> attributes = base.getAttributes();
        for (int i = 0; i < attributes.size(); i++) {
            Attribute attribute = attributes.get(i);
            if (attribute.getDisplayType() == 1) {
                ProductAttributeValue pav = new ProductAttributeValue();
                pav.setAttribute(attribute);
                pav.setProduct(product);
                pav = pavService.getAttributeValueByProductIdAndAttributeId(pav);
                attribute.setAttributeValues(null == pav ? null : pav.getAttributeValueList());
            }
        }
        // 商品标签
        List<Label> labelList = labelService.findManyByProductId(product.getId());
        product.setLabels(labelList);
        model.put("product", product);
        // model.put("groupedBrands", getGroupedBrands());
        model.put("labels", labelService.findAll());
        // model.put("baseModel", modelService.findBaseModel());
        model.put("baseModel", base);
        /* model.put("templates", templateService.findAll()); */

        Filters filter = Filters.create().eq("product_id", product.getId());
        List<Goods> goods = goodsService.findMany(filter, (Pageable) null);

        //现在即使不启用规格，单品表中也是有一条数据的
        /*final int size = null != goods ? goods.size() : 0;
        boolean specEnabled = true;
        if (1 > size) {
            specEnabled = false;
        } else if (1 == size) {
            Boolean isDefault = goods.get(0).getIsDefault();
            if(null != isDefault && isDefault) {
                specEnabled = false;
            }
        }
        product.setEnableSpecs(specEnabled);*/

        model.put("goods", goods);

        StringBuilder buff = new StringBuilder();
        if (null != goods) {
            for (int i = 0; i < goods.size(); i++) {
                Goods g = goods.get(i);
                List<SpecValue> values = g.getSpecValues();
                if (null != values) {
                    for (SpecValue value : values) {
                        buff.append(value.getId()).append(",");
                    }
                }
            }
            if (buff.length() > 0) {
                buff.deleteCharAt(buff.length() - 1);
            }
        }
        model.put("selectedSpecValueIds", buff.toString());
        
        model.put("worksList", worksService.findAll());
        model.put("brandList", brandService.findMany("enabled", true));
       model.put("providerList", supplierService.findAll());

        return getRelativeViewPath("edit");
    }

    private String[] clean(String[] array) {
        List<String> values = Lists.newArrayList();
        for (String value : values) {
            if (StringUtils.hasText(value)) {
                values.add(value);
            }
        }
        return values.toArray(new String[values.size()]);
    }

    private void clean(Product product) {
        List<ParameterValue> pvals = product.getParameterValues();
        if (null != pvals) {
            Iterator<ParameterValue> it = pvals.iterator();
            while (it.hasNext()) {
                ParameterValue value = it.next();
                Parameter param = value.getParameter();
                if (null == param || null == param.getId()) {
                    it.remove();
                }
            }
        }
		/* 没有站点了 */
		/* cleanTransients(product.getWebsites()); */
        List<ProductImg> images = product.getProductImgs();
        if (!CollectionUtils.isEmpty(images)) {
            Iterator<ProductImg> it = images.iterator();
            while (it.hasNext()) {
                ProductImg image = it.next();
                if (null == image || (null == image.getId() && !StringUtils.hasText(image.getPath()))) {
                    it.remove();
                }
            }
        }
    }

    @ResponseBody
    @RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] ids) {
        productService.delete(ids);
        StringBuffer idStr = new StringBuffer();
        for(Long id : ids){
        	idStr.append(id + ",");
        }
        idStr.deleteCharAt(idStr.length()-1);
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "商品管理-删除-ID:" + idStr , "商品管理");
        return ImmutableMap.<String, Object>of("success", true);
    }
    
    /**
     * 
     * <p>名称：检测单品编码的唯一性</p> 
     * <p>描述：</p>
     * @author：wentan
     * @param goodsId
     * @param code
     * @return
     */
    @RequestMapping("checkIsGoodsCodeExist")
    @ResponseBody
    public Map<String, Object> checkIsGoodsCodeExist(Long goodsId, String code) {
    	Filters filters = Filters.create();
		if (null != goodsId) {
			filters.ne("id", goodsId);
		}
		if (StringUtils.hasText(code)) {
			filters.eq("code", code);
		}
		Boolean isExist = goodsService.count(filters) >= 1;
		Map<String, Object> model = Maps.newHashMap();
		model.put("isExist", isExist);
		return model;
    }
    
    /**
     * 
     * <p>名称：检测商品编码的唯一性</p> 
     * <p>描述：</p>
     * @author：wentan
     * @param code
     * @return
     */
    @RequestMapping("checkIsCodeExist")
    @ResponseBody
    public Boolean checkIsCodeExist(Long id, String code) {
    	Filters filters = Filters.create();
		if (null != id) {
			filters.ne("id", id);
		}
		if (StringUtils.hasText(code)) {
			filters.eq("code", code);
		}
		return productService.count(filters) < 1;
    }
    
    /**
     * 
     * <p>名称：批量修改再贩次数</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param ids
     * @param reSellingNum
     * @return
     */
    @ResponseBody
    @RequestMapping("updateReSellingNum")
    public Map<String,Object> updateReSellingNum(@RequestParam("id") Long[] ids,Integer reSellingNum,String registerDate,String reserveDeadline) {
    	Map<String,Object> map = Maps.newHashMap();
    	for (Long id : ids) {
    		Product product = productService.find(id);
    		int oldReSellingNum = (null ==  product.getReSellingNum() ? 0 :  product.getReSellingNum());
    		product.setReSellingNum(oldReSellingNum + reSellingNum);
    		product.setIsReSelling(true);
            if(null!=registerDate&&registerDate.trim().length()>0) {
                try {
                    product.setRegisterDate(new SimpleDateFormat("yyyy-MM-dd").parse(registerDate));
                } catch (Exception e) {
                    product.setRegisterDate(new Date());
                }
            }
    		productService.update(product);
    	}
        try {
            map.put("reserveDeadline",new SimpleDateFormat("yyyy-MM-dd").parse(reserveDeadline));
        } catch (Exception e) {
            map.put("reserveDeadline",new Date());
        }
        map.put("productIds",ids);
goodsService.updateReserveDeadline(map);
        map.remove("reserveDeadline");
        map.remove("productIds");
    	map.put("success", true);
    	return map;
    }
    
    /**
     *
     * <p>名称：商品导出</p>
     * <p>描述：TODO未完成</p>
     * @author：wuqi
     * @param ids
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("exportData")
    public void exportData(@RequestParam(value="idArr") Long[] ids,HttpServletRequest request,HttpServletResponse response) {
        String[] headers = {"商品编号","商品名称","商品状态","商品规格","商品参数","商品类别","商品品牌","所属作品","供应商名称","日元成本价","人民币销售价","设计师",
                "日语名称","再贩次数","库存","截止时间","发售时间","购买范围","预定数量","预定截止时间","重量(kg)","库存预警(:以下)","商品定位"};
        List<String> productList = new ArrayList<>();
        Product product = new Product();
        List<Goods> goodsList = null;
        String str = "";
        for (Long id : ids) {
            product = productService.find(id);
            goodsList = goodsService.findByProductId(id);
            searchStatus(productList, goodsList, product);
        }
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
        CSVUtil.exportCsv(headers, productList, response, fileName);;

    }

    /**
     *
     * <p>名称：根据商品截止日期商品状态导出</p>
     * <p>描述：</p>
     * @author yuruizhi
     * @param cutOffStartTime
     * @param cutOffEndTime
     * @param status
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("searchGoodsExport")
    public void searchGoodsExport(
            @RequestParam(required = false ,defaultValue = "") String cutOffStartTime,
            @RequestParam(required = false ,defaultValue = "") String cutOffEndTime,
            @RequestParam(value="status") int status,
            HttpServletRequest request,
            HttpServletResponse response) {
    	String[] headers = {"商品ID","商品编码","商品标题","商品类型","是否上架","商品类别","商品品牌","所属作品","日元成本价","人民币销售价","截止时间","发售时间","库存"};
    	//查询商品 status:1-现货 spot；2-预定 reserved；3-限购 limited
        Map<String,Object> parmsMap = new HashMap<String,Object>();
        if(0 == status){
        	epExcel(headers,null,response);
        	return;
        }else{
            if(1 == status){
                parmsMap.put("spot", 1);
                parmsMap.put("reserved", null);
                parmsMap.put("limited", null);
            }else if(2 == status){ //字符串null
            	if(!"".equals(cutOffStartTime) && !"null".equals(cutOffStartTime) ){
            		parmsMap.put("cutOffStartTime",cutOffStartTime);
            	}
            	if(!"".equals(cutOffEndTime) && !"null".equals(cutOffEndTime)){
            		parmsMap.put("cutOffEndTime",cutOffEndTime);
            	}
                parmsMap.put("spot", null);
                parmsMap.put("reserved", 1);
                parmsMap.put("limited", null);
            }else if(3 == status){
                parmsMap.put("spot", null);
                parmsMap.put("reserved", null);
                parmsMap.put("limited", 1);
            }
        }
        List<Map<String,Object>> goodsList = productService.searchGoodsExcel(parmsMap);
        
        epExcel(headers,goodsList,response);
    }

    private void searchStatus(List<String> productList, List<Goods> goodsList, Product product) {
        String str;//预定商品
        if (true == product.getReserved()) {
            for (Goods goods : goodsList) {
                str = goodsStr(goods, product, "预定");
                productList.add(str);
            }
        }
        //限购商品
        if (true == product.getLimited()) {
            for (Goods goods : goodsList) {
                str = goodsStr(goods, product, "限购");
                productList.add(str);
            }
        }
        //现货商品
        if (true == product.getSpot()) {
            for (Goods goods : goodsList) {
                str = goodsStr(goods, product, "现货");
                productList.add(str);
            }
        }
    }
    
    private void epExcel(String[] str,List<Map<String,Object>> goodsList,HttpServletResponse response){

    	HSSFWorkbook workbook = new HSSFWorkbook();
    	HSSFSheet sheet = null ;
    	HSSFRow row = null ;
    	
        if(null != goodsList){
        	int j=1;
        	int count = 30000;
        	for(int i=0;i<goodsList.size();i++){
        		if(i==0){
        			sheet = workbook.createSheet("sheet1");
           	        row = sheet.createRow(0);
    	       	    for(int k=0;k<str.length;k++){
    	       	    	row.createCell(k).setCellValue(str[k]);
    	            }
        		}else{
        			if(i%count==0){
            			j++;
            			sheet = workbook.createSheet("sheet"+ j);
            			row = sheet.createRow(0);
         	       	    for(int k=0;k<str.length;k++){
         	       	    	row.createCell(k).setCellValue(str[k]);
         	            }
            		}
        		}
            	row = sheet.createRow(i%count+1);
            	row.createCell(0).setCellValue(goodsList.get(i).get("GOODS_ID").toString()); 
            	row.createCell(1).setCellValue(goodsList.get(i).get("GOODS_CODE").toString()); 
            	row.createCell(2).setCellValue(goodsList.get(i).get("GOODS_TITLE").toString()); 
            	row.createCell(3).setCellValue(goodsList.get(i).get("GOODS_TYPE").toString()); 
            	row.createCell(4).setCellValue(goodsList.get(i).get("ON_SELL").toString()); 
            	row.createCell(5).setCellValue(goodsList.get(i).get("CATEGORY_NAME").toString()); 
            	row.createCell(6).setCellValue(goodsList.get(i).get("BRAND_NAME").toString()); 
            	row.createCell(7).setCellValue(goodsList.get(i).get("WORK_NAME").toString()); 
            	row.createCell(8).setCellValue(goodsList.get(i).get("GOODS_PRICE").toString()); 
            	row.createCell(9).setCellValue(goodsList.get(i).get("GOODS_MARKET_PRICE").toString()); 
            	row.createCell(10).setCellValue(goodsList.get(i).get("RESERVE_DEADLINE").toString()); 
            	row.createCell(11).setCellValue(goodsList.get(i).get("REGISTER_DATE").toString()); 
            	row.createCell(12).setCellValue(goodsList.get(i).get("QTY").toString()); 
            }
        }
        
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";

        OutputStream ouputStream = null;
 		try {
 			response.setContentType("application/vnd.ms-excel");   
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);   
 		    ouputStream = response.getOutputStream();   
 		    workbook.write(ouputStream);   
 		    ouputStream.flush();   
 		    ouputStream.close();  
 		    if(ouputStream!=null)ouputStream.flush();
 			if(ouputStream!=null)ouputStream.close();

 		}  catch (IOException e) {
         
        }finally {
        	try {
        		if(ouputStream!=null)ouputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         }
    }

    /**
     * 
     * <p>名称：根据商品状态分类</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param goods
     * @param product
     * @param spot
     * @return
     */
    private String goodsStr(Goods goods,Product product,String spot) {
    	 String str = goods.getCode() + "\t," +
					  goods.getTitle() + "\t," +
					  spot + "," +
					  getSvStr(goods.getSvStr()) + "," +
					  getParameter(product.getParameterValues()) + "," +
					  (null == product.getCategory() ? "" : product.getCategory().getName()) + "," +
					  (null == product.getBrand() ? "" : product.getBrand().getName()) + "," +
					  (null == product.getWorks() ? "" : product.getWorks().getName()) + "," +
					  (null == goods.getSuppliers() ? "" : goods.getSuppliers()) + "," +
					  (null == goods.getPrice() ? "" : goods.getPrice()) + "," +
					  (null == goods.getMarketPrice() ? "" : goods.getMarketPrice()) + "," +
					  product.getDesigner() + "," +
					  product.getNameJp() + "," +
					  (null == product.getBoxNumber() ? "" : product.getBoxNumber()) + "," +
					  (null == product.getReSellingNum() ? "" : product.getReSellingNum()) + "," +
					  (null == goods.getStock() ? "" : goods.getStock()) + "," +
					  (null ==  product.getSalesOffDate() ? "" : DateUtils.date2Str( product.getSalesOffDate(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
					  (null ==  product.getRegisterDate() ? "" : DateUtils.date2Str( product.getRegisterDate(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
					  getScope(product.getRetail(), product.getDealer()) + "," +
					  (null == goods.getReserveNum() ? "" : goods.getReserveNum()) + "," +
					  (null == goods.getReserveDeadline() ? "" : DateUtils.date2Str(goods.getReserveDeadline(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
					  goods.getWeight() + "," +
					  goods.getStockForewarn() + "," +
					  getTrend(goods.getTrend());
    	 return str;
    }
    
    /**
     * 
     * <p>名称：商品购买范围</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param retail
     * @param dealer
     * @return
     */
    private String getScope (Boolean retail,Boolean dealer) {
    	String str = "";
    	if (true == retail) {
    		str += "零售;";
    	}
    	if (true == dealer) {
    		str += "经销商;";
    	}
    	str = str.substring(0, str.length()-1);
    	return str;
    }
    
    /**
     * 
     * <p>名称：商品定位</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param trend
     * @return
     */
    private String getTrend (Integer trend) {
    	if (new Integer(0).equals(trend)) {
    		return "无差别";
    	} else if (new Integer(1).equals(trend)) {
    		return "趋向男性";
    	} else if (new Integer(2).equals(trend)) {
    		return "趋向女性";
    	} else {
    		return "";
    	}
    }
    
    /**
     * 
     * <p>名称：商品规格</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param svStr
     * @return
     */
    private String getSvStr(String svStr) {
    	String str = "";
    	if(null != svStr) {
    		String[] specList = svStr.split(";");
        	for (int i=0; i<specList.length;i++) {
        		String[] specValueList = specList[i].split(":");
        		String id = specValueList[1];
        		SpecValue specValue = speValueService.specValueBean(id);
        		str += (null == specValue.getSpec() ? "" : specValue.getSpec().getName()) + ":" +
        			   (null == specValue.getName() ? "" : specValue.getName()) + ";";
        	}
    	}
    	return str;
    }
    
    /**
     * 
     * <p>名称：商品参数</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param parameterValueList
     * @return
     */
    private String getParameter (List<ParameterValue> parameterValueList) {
    	String str = "";
    	for (ParameterValue parameterValue : parameterValueList) {
    		String name = (null == parameterValue.getParameter() ? "" : parameterValue.getParameter().getName());
    		String value = (null == parameterValue.getValue() ? "" : parameterValue.getValue());
    		str += name + ":" + value + ";";
    	}
    	return str;	
    }

    /**
     *
     * <p>名称：商品模型下载</p>
     * <p>描述：</p>
     * @author：wuqi
     * @param request
     * @param responseimportData
     */
    @RequestMapping("downLoadModel")
    public void downLoadModel (HttpServletRequest request,HttpServletResponse response) {
        String[] headers = {"商品名称","商品编号","商品状态","商品品牌","所属作品","商品标签","日元成本价","发售日期","是否再贩","再贩次数","设计师",
                "日语名称","购买范围","排序","所属分类","属性","图片","规格","商品标题","商品编号","供应商编号","日元成本价",
                "限购数量","预定截止时间","重量(kg)","库存预警(:以下)","商品定位","图片1","图片2","图片3","图片4","图片5"};
        List<String> dataList = new ArrayList<>();
        String data = "火影忍者衬衫,H10000001,限购|现货|预定,有妖气,火影忍者,现货,100,2016/1/22,是,2,岸本,火影忍者のシャツ,零售|经销商,1,手办模型,产地:东京大阪|是否艾漫出品:否,img.jpg,"
                + "颜色:红色|材质:塑料|高度:10CM,火影忍者衬衫,H0000001,gys123456,100,20,2016/12/22,0.2,2,无差别,img1.jpg,img2.jpg,img3.jpg,img4.jpg,img5.jpg";
        dataList.add(data);
        data = ",,,,,,,,,,,,,,,,,颜色:蓝色|材质:橡胶|高度:20CM,火影忍者衬衫,H0000001,gys123456,100,20,2016/12/22,0.2,2,男性化,img1.jpg,img2.jpg,img3.jpg,img4.jpg,img5.jpg";
        dataList.add(data);
        data = ",,,,,,,,,,,,,,,,,颜色:白色|材质:铝制|高度:10CM,火影忍者衬衫,H0000001,gys123456,100,20,2016/12/22,0.2,2,女性化,img1.jpg,img2.jpg,img3.jpg,img4.jpg,img5.jpg";
        dataList.add(data);
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
        ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }

    @RequestMapping("importcf")
    public void importcf (HttpServletRequest request,HttpServletResponse response) {
        String[] headers = {"商品名称","商品编号","商品状态","商品品牌","所属作品","商品标签","日元成本价","发售日期","是否再贩","再贩次数","设计师",
                "日语名称","购买范围","排序","所属分类","属性","图片","规格","商品标题","商品编号","供应商编号","日元成本价",
                "限购数量","预定截止时间","重量(kg)","库存预警(:以下)","商品定位","图片1","图片2","图片3","图片4","图片5"};
        List<String> dataList = new ArrayList<>();
        List<Product> products=   UserUtils.getCfProduct(PRODUCT_CF_ERROR+UserUtils.getCurrentUserId());
        if(null!=products&&products.size()>0){
            for(Product product:products){
                int num=0;
                StringBuffer stringBuffer=new StringBuffer();
                stringBuffer.append(product.getTitle()+",");
                stringBuffer.append(product.getCode()+",");
                StringBuffer xgy=new StringBuffer("");
                if(product.getLimited()){
                    xgy.append("限购");
                }
                if(product.getSpot()){
                    if(xgy.toString().trim().length()>1){
                        xgy.append("|");
                    }
                    xgy.append("现货");
                }
                if(product.getReserved()){
                    if(xgy.toString().trim().length()>1){
                        xgy.append("|");
                    }
                    xgy.append("预定");
                }
                stringBuffer.append(xgy.toString()+",");
                stringBuffer.append(product.getBrand().getName()+",");
                stringBuffer.append(product.getWorks().getName()+",");
                stringBuffer.append(product.getLabels().get(0).getName()+",");
                stringBuffer.append(product.getPrice().toString()+",");
                stringBuffer.append(new SimpleDateFormat("yyyy/MM/dd").format(product.getRegisterDate())+",");
                stringBuffer.append((product.getIsReSelling()?"是":"否")+",");
                stringBuffer.append((product.getReSellingNum()==null?"":product.getReSellingNum().toString())+",");
                stringBuffer.append(product.getDesigner()+",");
                stringBuffer.append(product.getNameJp()+",");
                StringBuffer lj=new StringBuffer("");
                if(product.getRetail()){
                    ;lj.append("零售");
                }
                if(product.getDealer()){
                    if(lj.toString().trim().length()>1){
                        lj.append("|");
                    }
                    ;lj.append("经销商");
                }
                stringBuffer.append(lj.toString()+",");
                stringBuffer.append((product.getSort()!=null?product.getSort().toString():"")+",");
                stringBuffer.append(product.getCategory().getName()+",");
                if(null!= product.getCategory().getModel().getAttributes()&&product.getCategory().getModel().getAttributes().size()>0){
                    StringBuffer attr=new StringBuffer("");
                    for(Attribute attribute:product.getCategory().getModel().getAttributes()){
                        if(attr.toString().trim().length()>1){
                            attr.append("|");
                        }
                        attr.append(attribute.getName()+":"+attribute.getAttributeValues().get(0).getValue());
                    }
                    stringBuffer.append(attr.toString()+",");
                }else{
                    stringBuffer.append(""+",");
                }
                stringBuffer.append(product.getImage()+",");
                for(Goods goods:product.getGoods()) {
                    StringBuffer g = new StringBuffer();
                    StringBuffer sv = new StringBuffer("");
                   for(SpecValue specValue:goods.getSpecValues()){
                       if(sv.toString().trim().length()>1){
                           sv.append("|");
                       }
                       sv.append(specValue.getSpec().getName()+":"+specValue.getName());
                   }
                    g.append(sv.toString()+",");
                    g.append(goods.getTitle()+",");
                    g.append(goods.getCode()+",");
                    g.append(supplierService.find(goods.getSupplierId()).getNumberCode()+",");
                    g.append(goods.getPrice()+",");
                    g.append(goods.getReserveNum().toString()+",");
                    g.append(new SimpleDateFormat("yyyy/MM/dd").format(goods.getReserveDeadline())+",");
                    g.append(goods.getWeight().toString()+",");
                    g.append(goods.getStockForewarn().toString()+",");
                    g.append(goods.getStockForewarn().toString()+",");
                    if(goods.getTrend()==0){
                        g.append("无差别"+",");
                    }
                    if(goods.getTrend()==1){
                        g.append("男性化"+",");
                    }
                    if(goods.getTrend()==2){
                        g.append("女性化"+",");
                    }
                    for(ProductImg productImg:goods.getProductImgs()){
                        g.append(productImg.getPath()+",");
                    }
                    if(num==0){
                        dataList.add(stringBuffer.toString()+g.toString());
                    }else{
                        dataList.add(",,,,,,,,,,,,,,,,,"+g.toString());
                    }
                    num++;
                }
            }
        }
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
        ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }



    @RequestMapping("importError")
    public void importError (HttpServletRequest request,HttpServletResponse response) {
        String[] headers = {"商品名称","商品编号","商品状态","商品品牌","所属作品","商品标签","日元成本价","发售日期","是否再贩","再贩次数","设计师",
                "日语名称","购买范围","排序","所属分类","属性","图片","规格","商品标题","商品编号","供应商编号","日元成本价",
                "限购数量","预定截止时间","重量(kg)","库存预警(:以下)","商品定位","图片1","图片2","图片3","图片4","图片5"};
        List<String> dataList = new ArrayList<>();
        List<String> datas=   UserUtils.getCacheProduct(PRODUCT_IMPORT_ERROR+UserUtils.getCurrentUserId());
        if(null!=datas &&datas.size()>0){
            for(int i=0;i<datas.size();i++){
                try {
                    String data[] = datas.get(i).split(",");
                    if (data[0].trim().length() > 1) {
                        if(data[2].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //验证品牌
                        if(data[3].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        List<Brand> brands = brandService.findByName(data[3].trim());
                        if (null != brands && 0 < brands.size()) {

                        } else {
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //验证作品
                        if(data[4].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        List<Works> works = worksService.findByName(data[4].trim());
                        if (null != works && 0 < works.size()) {

                        } else {
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //验证标签
                        if(data[5].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        List<Label> labels = labelService.findByName(data[5].trim());
                        if (null != labels && 0 < labels.size()) {

                        } else {
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //pruduct日元成本价
                        if(data[6].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //发售日期
                        if(data[7].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //是否再贩
                        if(data[8].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //设计师
                        if(data[10].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //日语名称
                        if(data[11].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //购买范围
                        if(data[12].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //验证分类
                        if(data[14].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        List<Category> categories = categoryService.findByName(data[14].trim());
                        if (null != categories && 0 < categories.size()) {
                        } else {
                            dataList.add(datas.get(i));
                            continue;
                        }
                        //属性 如果上边的分类没有查到关联模型就不用说属性了，直接给提示
                        if(null!=data[15]&&data[15].trim().length()>0) {
                            //基本模型
                            Model m = modelService.findBaseModel();
                            if (null ==categories.get(0).getModel()) {
                                dataList.add(datas.get(i));
                                continue;
                            }else{
                                String attributs[]=data[15].split("\\|");
                                //查询模型底下的属性 进行匹配
                                List<Attribute> attributeList=new ArrayList<Attribute>();
                                List<Attribute> attributes =  attributeService.findModelAttributes(categories.get(0).getModel().getId());
                                if((null==attributes||attributes.size()<1)&&(null==m.getAttributes()||m.getAttributes().size()<1)){
                                    dataList.add(datas.get(i));
                                    continue;
                                }else{
                                    if(null!=attributes&&attributes.size()>0){
                                        if(null!=m.getAttributes()&&m.getAttributes().size()>0)
                                            attributes.addAll(m.getAttributes())  ;
                                    }else{
                                        attributes = m.getAttributes();
                                    }
                                    for(String a:attributs) {
                                        String av[]=a.split(":");
                                        for(Attribute attribute:attributes){
                                            if(av[0].trim().equals(attribute.getName())){
                                                List<AttributeValue> attributeValues=new ArrayList<AttributeValue>();
                                                if(attribute.getDisplayType()==1){
                                                    //文本直接给值
                                                    AttributeValue attributeValue=new AttributeValue();
                                                    attributeValue.setValue(av[1].trim());
                                                    attributeValues.add(attributeValue);
                                                    attribute.setAttributeValues(attributeValues);
                                                    attributeList.add(attribute);
                                                }else{
                                                    if(null!=attribute.getAttributeValues()&&attribute.getAttributeValues().size()>0) {
                                                        for (AttributeValue attributeValue : attribute.getAttributeValues()){
                                                            if(attributeValue.getValue().equals(av[1].trim())){
                                                                attributeValues.add(attributeValue);
                                                                break;
                                                            }
                                                        }
                                                        if(attributeValues.size()>0){
                                                            attribute.setAttributeValues(attributeValues);
                                                            attributeList.add(attribute);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if(attributs.length!=attributeList.size()){
                                        dataList.add(datas.get(i));
                                        continue;
                                    }
                                }
                            }
                        }
                        //图片
                        if(data[16].trim().length()<1){
                            dataList.add(datas.get(i));
                            continue;
                        }
                        Goods goods=createGoods(data);
                        if(null!=goods&&null!=goods.getError()){
                            dataList.add(datas.get(i));
                        }
                    }else{
                        Goods goods=createGoods(data);
                        if(null!=goods&&null!=goods.getError()){
                            dataList.add(datas.get(i));
                            continue;
                        }
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    dataList.add(datas.get(i));
                }
            }
        }
        String fileName = "productImportError" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
        ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }

    /**
     *
     * <p>名称：再贩单品导入模型</p>
     * <p>描述：</p>
     * @author：wuqi
     * @param request
     * @param response
     */
    @RequestMapping("goodsZaifan")
    public void goodsZaifan (HttpServletRequest request,HttpServletResponse response) {
        String [] headers={"单品货号","供应商编号","截单日期","发售日期"};
        List<Goods> goodsList=new ArrayList<Goods>();
        for(int i=0;i<1000;i++) {
            Goods goods = new Goods();
            goods.setCode(null);
            goods.setTitle(null);//充个数
            goods.setCreatedBy(null);//充个数
            goods.setLastModifiedBy(null);//充个数
            goodsList.add(goods);
        }
        String [] methosName={"code","title","createdBy","lastModifiedBy"};
        try {
        ExcelUtil.outExcel("再贩单品导入模板", headers, goodsList, methosName, response);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @ResponseBody
    @RequestMapping(value = "/zaifandaoru",  method = RequestMethod.POST)
    public Map<String,Object> zaifandaoru(@RequestPart("file") MultipartFile multipart,
                                         HttpServletRequest request) throws IOException {
        Map<String,Object> map=new HashMap<String,Object>();
        InputStream inputStream = multipart.getInputStream();
        List<String> list = ExcelUtil.importExcel(inputStream, 1, 4,"zc");
        StringBuilder moreGoods=new StringBuilder("");
        StringBuilder notFindGoods=new StringBuilder("");
        StringBuilder notFindGoodsCode=new StringBuilder("");
        int  count=0;
        
        if(null == list || list.size() < 1){
        	map.put("error", "模板内容为空");
            return map;
        }
        String content[] = null;
        for(int i=1;i<list.size();i++){
        	
        	if(null == list.get(i)){
        		continue;
        	}
        	content = list.get(i).split(",");
        	if(content.length != 4 || content[0].trim().length()<=0){
        		continue;
        	}
        	
            map.put("goodsCode",content[0].trim());
            map.put("suppCode",content[1].trim());
            int retCount = goodsService.findGoodsCount(map);
            if(retCount <= 0){
            	//商品未找到
            	notFindGoods.append("<br/>商品编号"+content[0].trim()+",供应商编号"+content[1].trim()+"  未能找到对应商品");
            	notFindGoodsCode.append(content[0].trim()+",");
            }else if(retCount == 1){
            	
            	try {
					map.put("reserveDeadline",new SimpleDateFormat("yyyy-MM-dd").parse(content[2].trim()));
					goodsService.updategoods(map);
	                map.put("registerDate",new SimpleDateFormat("yyyy-MM-dd").parse(content[3].trim()));
	                map.put("reSellingNum",1);
	                goodsService.updateproduct(map);
	                goodsCheckService.updateChecked(map);
	                count++;
				} catch (ParseException e) {
					e.printStackTrace();
					map.put("error", "导入异常，可能是因为数据格式等不正确引起");
			        return map;
				}
                
            }else {
            	//商品重复
            	moreGoods.append("<br/>商品编号"+content[0].trim()+",供应商编号"+content[1].trim()+" 存在重复货号，无法再贩");
            }

        }
        map.put("error", "成功导入"+count+"个商品"+notFindGoods+moreGoods);
        map.put("notFindGoodsCode", notFindGoodsCode);
        
        return map;
    }
    
    
    @RequestMapping(value = "/exportNotFindGoods",  method = RequestMethod.POST)
    public void exportNotFindGoods(String notFindGoodsCode,HttpServletRequest request,HttpServletResponse response) {
    	HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("sheet1");
        HSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 创建一个居中格式
        HSSFRow row = sheet.createRow(0);
        row.setRowStyle(style);
          
        row.createCell(0).setCellValue("商品货号");
        String[] arrCode = notFindGoodsCode.split(",");
     	for(int i=0;i<arrCode.length;i++){
         	row = sheet.createRow(i+1);
         	row.createCell(0).setCellValue(arrCode[i]); 
        }

        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
        OutputStream ouputStream = null;
  		try {
  			response.setContentType("application/vnd.ms-excel");   
             response.setHeader("Content-disposition", "attachment;filename=" + fileName);   
  		    ouputStream = response.getOutputStream();   
  		    workbook.write(ouputStream);   
  		    ouputStream.flush();   
  		    ouputStream.close();  
  		    if(ouputStream!=null)ouputStream.flush();
  			if(ouputStream!=null)ouputStream.close();

  		}  catch (IOException e) {
          
        }finally {
         	try {
         		if(ouputStream!=null)ouputStream.close();
 			} catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
        }
    }
     
    
    @ResponseBody
    @RequestMapping(value = "/importData",  method = RequestMethod.POST)
    public Map<String,Object> importData(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	Map<String,Object> map = Maps.newHashMap();
    	InputStream inputStream = multipart.getInputStream();
    	List<String> list = ExcelUtil.importExcel(inputStream, 1, 42);
        StringBuffer buffer=new StringBuffer("以下商品有问题具体如下：<br>");
        List<Product> products=null;
        if(null!=list &&list.size()>0){
         List<Product> productList=new ArrayList<Product>();
          for(int i=0;i<list.size();i++){
              try {
                  String data[] = list.get(i).split(",");
                  if (data[0].trim().length() > 1) {
                      //是product的数据，接下来就要校验数据了
                      Product product = new Product();
                      List<Goods> goodsList=new ArrayList<Goods>();
                      product.setTitle(data[0]);
                      product.setCode(data[1]);
                      if(data[2].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有商品状态导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      String types[]=data[2].split("\\|");
                      for(String type:types){
                          if(type.trim().equals("限购")){
                              product.setLimited(true);
                          }else  if(type.trim().equals("现货")){
                              product.setSpot(true);
                          }else  if(type.trim().equals("预定")){
                              product.setReserved(true);
                          }
                      }
                      //验证品牌
                      if(data[3].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有商品品牌导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      List<Brand> brands = brandService.findByName(data[3].trim());
    					if (null != brands && 0 < brands.size()) {
    						product.setBrand(brands.get(0));
    					} else {
                            buffer.append("第"+(i+1)+"条数据找不到品牌导入失败!<br>");
                            Goods goods=new Goods();
                            goods.setError("error");
                            goodsList.add(goods);
                            product.setGoods(goodsList);
                            productList.add(product);
							continue;
    					}
                      //验证作品
                      if(data[4].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有商品作品导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
    					List<Works> works = worksService.findByName(data[4].trim());
    					if (null != works && 0 < works.size()) {
    						product.setWorks(works.get(0));
    					} else {
                            buffer.append("第"+(i+1)+"条数据找不到作品导入失败!<br>");
                            Goods goods=new Goods();
                            goods.setError("error");
                            goodsList.add(goods);
                            product.setGoods(goodsList);
                            productList.add(product);
                            continue;
    					}
                      //验证标签
                      if(data[5].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有商品标签导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      List<Label> labels = labelService.findByName(data[5].trim());
    					if (null != labels && 0 < labels.size()) {
    						product.setLabels(labels);
    					} else {
                            buffer.append("第"+(i+1)+"条数据找不到标签导入失败!<br>");
                            Goods goods=new Goods();
                            goods.setError("error");
                            goodsList.add(goods);
                            product.setGoods(goodsList);
                            productList.add(product);
                            continue;
    					}
                      //pruduct日元成本价
                      if(data[6].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有日元成本价导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      product.setPrice(new BigDecimal(data[6].trim()));
                      //发售日期
                      if(data[7].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有发售日期导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      product.setRegisterDate(new SimpleDateFormat("yyyy-MM-dd").parse(data[7].trim()));
                      //是否再贩
                      if(data[8].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有是否再贩导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      if(data[8].trim().equals("是")){
                          product.setIsReSelling(true);
                      }else{
                          product.setIsReSelling(false);
                      }
                      //再贩次数
                      if(null!=data[9]&&data[9].trim().length()>0){
                          product.setReSellingNum(Integer.parseInt(data[9].trim()));
                      }
                      //设计师
                      if(data[10].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有设计师导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      product.setDesigner(data[10]);
                      //日语名称
                      if(data[11].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有日语名称导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      product.setNameJp(data[11]);
                      //购买范围
                      if(data[12].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有购买范围导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      String fw[]=data[12].split("\\|");
                      for(String f:fw){
                          if(f.trim().equals("零售")){
                              product.setRetail(true);
                          }else  if(f.trim().equals("经销商")){
                              product.setDealer(true);
                          }
                      }
                    //排序
                      if(null!=data[13]&&data[13].trim().length()>0){
                          product.setSort(Integer.parseInt(data[13].trim()));
                      }else{
                          product.setSort(1);
                      }
                    //验证分类
                      if(data[14].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有商品分类导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      List<Category> categories = categoryService.findByName(data[14].trim());
                      if (null != categories && 0 < categories.size()) {
    						product.setCategory(categories.get(0));
    					} else {
                          buffer.append("第"+(i+1)+"条数据找不到分类导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
    					}
                      //属性 如果上边的分类没有查到关联模型就不用说属性了，直接给提示
                      if(null!=data[15]&&data[15].trim().length()>0) {
                          //基本模型
                          Model m = modelService.findBaseModel();
                          if (null == product.getCategory().getModel()) {
                              buffer.append("第" + (i + 1) + "条数据找不到分类关联模型导入失败!<br>");
                              Goods goods=new Goods();
                              goods.setError("error");
                              goodsList.add(goods);
                              product.setGoods(goodsList);
                              productList.add(product);
                              continue;
                          }else{
                              String attributs[]=data[15].split("\\|");
                              //查询模型底下的属性 进行匹配
                              List<Attribute> attributeList=new ArrayList<Attribute>();
                           List<Attribute> attributes =  attributeService.findModelAttributes(product.getCategory().getModel().getId());
                              if((null==attributes||attributes.size()<1)&&(null==m.getAttributes()||m.getAttributes().size()<1)){
                                  buffer.append("第" + (i + 1) + "条数据找不到分类关联模型下的属性导入失败!<br>");
                                  Goods goods=new Goods();
                                  goods.setError("error");
                                  goodsList.add(goods);
                                  product.setGoods(goodsList);
                                  productList.add(product);
                                  continue;
                              }else{
                                  if(null!=attributes&&attributes.size()>0){
                                      if(null!=m.getAttributes()&&m.getAttributes().size()>0)
                                      attributes.addAll(m.getAttributes())  ;
                                  }else{
                                      attributes = m.getAttributes();
                                  }
                                for(String a:attributs) {
                                    String av[]=a.split(":");
                                    for(Attribute attribute:attributes){
                                        if(av[0].trim().equals(attribute.getName())){
                                            List<AttributeValue> attributeValues=new ArrayList<AttributeValue>();
                                            if(attribute.getDisplayType()==1){
                                                //文本直接给值
                                                AttributeValue attributeValue=new AttributeValue();
                                                attributeValue.setValue(av[1].trim());
                                                attributeValues.add(attributeValue);
                                                attribute.setAttributeValues(attributeValues);
                                                attributeList.add(attribute);
                                            }else{
                                                if(null!=attribute.getAttributeValues()&&attribute.getAttributeValues().size()>0) {
                                                    for (AttributeValue attributeValue : attribute.getAttributeValues()){
                                                        if(attributeValue.getValue().equals(av[1].trim())){
                                                            attributeValues.add(attributeValue);
                                                            break;
                                                        }
                                                    }
                                                   if(attributeValues.size()>0){
                                                       attribute.setAttributeValues(attributeValues);
                                                       attributeList.add(attribute);
                                                   }
                                                }
                                            }
                                        }
                                    }
                                }
                                  if(attributs.length!=attributeList.size()){
                                      buffer.append("第" + (i + 1) + "条数据找不到分类关联模型下的属性导入失败!<br>");
                                      Goods goods=new Goods();
                                      goods.setError("error");
                                      goodsList.add(goods);
                                      product.setGoods(goodsList);
                                      productList.add(product);
                                      continue;
                                  }else{
                                      product.getCategory().getModel().setAttributes(attributeList);
                                  }
                              }
                          }
                      }
                      //图片
                      if(data[16].trim().length()<1){
                          buffer.append("第"+(i+1)+"条数据没有图片导入失败!<br>");
                          Goods goods=new Goods();
                          goods.setError("error");
                          goodsList.add(goods);
                          product.setGoods(goodsList);
                          productList.add(product);
                          continue;
                      }
                      product.setImage(data[16]);
                      Goods goods=createGoods(data);
                      if(null!=goods) {
                          product.setEnableSpecs(true);
                          goodsList.add(goods);
                      }
                      if(null!=goods&&null!=goods.getError()){
                          buffer.append("第" + (i + 1) + "条数据"+goods.getError()+"导入失败!<br>");
                      }
                      product.setGoods(goodsList);
                      productList.add(product);
                  }else{
                      Goods goods=createGoods(data);
                      if(null!=goods&&null!=goods.getError()){
                          buffer.append("第" + (i + 1) + "条数据"+goods.getError()+"导入失败!<br>");
                          continue;
                      }
                      if(null!=goods) {
                          productList.get(productList.size()-1).setEnableSpecs(true);
                          productList.get(productList.size()-1).getGoods().add(goods);
                      }
                  }
              }catch(Exception e){
                  e.printStackTrace();
                  buffer.append("第"+(i+1)+"条数据异常导入失败!<br>");
              }
          }
            products=createProducts(productList);
           productService.dr(productList,products);
        }
        if(buffer.toString().equals("以下商品有问题具体如下：<br>"))
    	map.put("message", "导入完成");
        else {
            //存入整个表格数据进入缓存，方便之后导出数据不行的商品
            UserUtils.setCacheProduct(PRODUCT_IMPORT_ERROR+UserUtils.getCurrentUserId(),list);
            map.put("message", buffer.toString());
        }
        if(null!=products&&products.size()>0){
            map.put("error", "重复");
            UserUtils.setCfProduct(PRODUCT_CF_ERROR+UserUtils.getCurrentUserId(),products);
        }
    	return map;
    }
    public List<Product> createProducts(List<Product> productList){
        //对商品下的 单品货号和供应商编号进行唯一确认
        List<Product> products=new ArrayList<Product>();
        List<Product> p=new ArrayList<Product>();
        if(null!=productList&&productList.size()>0){
            for(int r=0;r<productList.size();r++){
                productList.get(r).setId(Long.parseLong(r+""));
                if(null!= productList.get(r).getGoods()&& productList.get(r).getGoods().size()>0) {
                    int i = 0;
                    for (Goods goods : productList.get(r).getGoods()) {
                        if (null != goods.getError()) {
                            i++;
                        }
                    }
                    if (i == 0) {
                        products.add(productList.get(r));
                    }
                }
            }
            if(products.size()>0){
                //验证单品是否有导入重复的
                for(Product product:products){
                    for(Goods goods:product.getGoods()){
                        int num=0;
                        for(Product pp:products){
                            for(Goods g:product.getGoods()){
                             if(goods.getCode().equals(g.getCode())&&goods.getSupplierId()==g.getSupplierId()){
                                 num++;
                             }
                                if(num>1){
                                    p.add(pp);
                                }
                            }
                        }
                    }
                }
            }
        }
        //这个时候 p中存在的都是双份的，过滤掉一份
        List<Product> df=new ArrayList<Product>();
        if(p.size()>0){
            for(Product product:p){
                if(df.size()<1){
                    df.add(product);
                }else{
                   int o=0;
                    for(Product dp:df){
                        if(product.getId().equals(dp.getId())){
                            o++;
                        }
                    }
                    if(o==0){
                        df.add(product);
                    }
                }
            }
        }
        if(products.size()>0){
        for(Product pt:products){
            int u=0;
            if(df.size()>0){
                for(Product ppp:df){
                    if(pt.getId().equals(ppp.getId()))  {
                        u++;
                    }
                }
            }
            if(u==0){
                int gg=0;
                for(Goods g:pt.getGoods()){
                    Map<String,Object> map=new HashMap<String,Object>();
                    map.put("goodsCode",g.getCode());
                    map.put("supplierId",g.getSupplierId());
                  if(goodsService.findGoodszCount(map)>0){
                      gg++;
                      break;
                  }
                }
                if(gg!=0){
                    df.add(pt);
                }
            }
        }
        }
       return df;
    }

    
    public Goods createGoods( String data[])throws  Exception{
        if(data.length<19){
            return null;
        }
        if(null==data[18]||data[18].trim().length()<1){
            return null;
        }
        Goods goods=new Goods();
        if(null==data[17]||data[17].trim().length()<1){
            goods.setError("规格为空");
            return goods;
        }
        String specs[]=data[17].split("\\|");
        List<SpecValue> specValues=new ArrayList<SpecValue>();
        for(String spec:specs){
            String sv[]=spec.split(":");
            SpecValue specValue=new SpecValue();
            specValue.setName(sv[1]);
            Spec s=new Spec();
            s.setName(sv[0]);
            specValue.setSpec(s);
            specValues.add(specValue);
        }

        goods.setSpecValues(specValues);
        goods.setTitle(data[18]);
        if(null==data[19]||data[19].trim().length()<1){
            goods.setError("商品编码为空");
            return goods;
        }
        goods.setCode(data[19]);
        //供应商验证
        if(null==data[20]||data[20].trim().length()<1){
            goods.setError("供应商编号为空");
            return goods;
        }
        List<Supplier> suppliers = supplierService.findBySupplierCode(data[20]);
        					if (null != suppliers && suppliers.size()>0) {
        						goods.setSupplierId(suppliers.get(0).getId());
        					} else {
                                goods.setError("供应商不存在");
                                return goods;
        					}
        //日元成本价
        if(null==data[21]||data[21].trim().length()<1){
            goods.setError("日元成本价为空");
            return goods;
        }
        goods.setPrice(new BigDecimal(data[21].trim()));
        //限购数量
        if(null==data[22]||data[22].trim().length()<1){
            goods.setError("限购数量为空");
            return goods;
        }
        goods.setReserveNum(Integer.parseInt(data[22].trim()));
       //预定截止日期
        if(null==data[23]||data[23].trim().length()<1){
            goods.setError("预定截止日期为空");
            return goods;
        }
        goods.setReserveDeadline(new SimpleDateFormat("yyyy-MM-dd").parse(data[23].trim()));
        //重量
        if(null==data[24]||data[24].trim().length()<1){
            goods.setError("重量为空");
            return goods;
        }
        goods.setWeight(Float.parseFloat(data[24].trim()));
        //库存预警
        if(null==data[25]||data[25].trim().length()<1){
            goods.setError("库存预警为空");
            return goods;
        }
        goods.setStockForewarn(Long.parseLong(data[25].trim()));
        //商品定位
        if(null==data[26]||data[26].trim().length()<1){
            goods.setError("商品定位为空");
            return goods;
        }
        if(data[26].trim().equals("无差别")){
            goods.setTrend(0);
        }else if(data[26].trim().equals("男性化")){
            goods.setTrend(1);
        }else if(data[26].trim().equals("女性化")){
            goods.setTrend(2);
        }else{
            goods.setTrend(0);
        }
        //图片
        if(null==data[27]||data[27].trim().length()<1){
            goods.setError("图片1商品主图为空");
            return goods;
        }
        goods.setImage(data[27].trim());
        List<ProductImg> productImgs=new ArrayList<>();
        ProductImg productImg1=new ProductImg();
        productImg1.setPath(data[27].trim());
        productImgs.add(productImg1);
        if(data.length>28) {
            for(int i=0;i<data.length-28;i++) {
                if(null!=data[i+28]&&data[i+28].trim().length()>0) {
                    ProductImg productImg = new ProductImg();
                    productImg.setPath(data[i + 28].trim());
                    productImgs.add(productImg);
                }
            }
        }
        goods.setProductImgs(productImgs);
        return goods;
    }

    
    /**
	 * <p>名称：批量更新商品图片</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @return
	 */
    @RequestMapping("updateImage")
    public String updateImage(){
    	
        return "product/updateImage";
    }
    
    /**
     * <p>名称：处理上传的zip图片压缩包并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param multipart
     * @param request
     * @throws IOException
     */
    @RequestMapping(value = "/uploadImage",  method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> uploadImage(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	
    	InputStream inputStream = multipart.getInputStream();
    	
    	String savePath = request.getSession().getServletContext().getRealPath("/WEB-INF/import/iamgeZip");
    	
    	String zipFilePath = ZipUtil.copyZipFileStreamToServer(inputStream, savePath);
    	//随机解压文件名
    	String unzipFolderName = DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
    	List<String> imagePathList = ZipUtil.handleZip(zipFilePath, savePath+"/"+unzipFolderName);
    	
    	Map<String,Object> resultMap = Maps.newHashMap();
    	//处理图片
    	if(null != imagePathList && imagePathList.size() > 0){
    		List<String> resultList = handleImageList(imagePathList);
    		resultMap.put("resultList", resultList);
    	}
    	
    	return resultMap;
    }
    
    /**
     * <p>名称：处理批量图片并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param imagePathList
     * @return
     */
    private  List<String> handleImageList(List<String> imagePathList){
    	String filePath = null;
   	    String productIdStr = null;
   	    String type = null;
   	    String imgSort = null;
   	    Product product = null;
   	    Goods goods = null;
   	    List<String> resultList = new ArrayList<String>();
   	    StringBuilder sb = new StringBuilder();
    	for(String path : imagePathList){
    		  productIdStr = FileUtils.getNameWithoutExtension(path);
	  		  //先判断是否符合数字加下划线加1/2的格式
	  		  if(productIdStr.matches("[12]_[0-9]{1,11}_[1-6]")){
	  			type = productIdStr.split("_")[0];
	  			imgSort = productIdStr.split("_")[2];
	  			productIdStr = productIdStr.split("_")[1];
	  		  }else{
	  			  sb.delete(0, sb.length());
	  			  sb.append(FileUtils.getName(path) + "命名格式不正确,请检查");
	  			  resultList.add(sb.toString());
	  			  continue ;
	  		  }
	  		  //将图片放到服务器对应位置并更新数据
  			  filePath = CSVUtil.CopyFileToServer(path);
  			  if("1".equals(type)){
  				 product = productService.find(Long.valueOf(productIdStr));
  			  }else if("2".equals(type)){
  				 goods = goodsService.find(Long.valueOf(productIdStr));
  			  }
  			  if(null != product 
  					  && "1".equals(type)
  					  && "1".equals(imgSort)){
  				  product.setImage(filePath);
  				  productService.onlyUpdate(product);
  			  }else if(null != product 
  					  && "1".equals(type)
  					  && !"1".equals(imgSort)){
  				  //更新商品图片
  				  ProductImg  productImg  = productImgService.findImgByProIdAndSort(Long.valueOf(productIdStr), Integer.valueOf(imgSort)-2);
  				  if(null != productImg){
  					productImg.setPath(filePath);
  					productImgService.update(productImg);
  				  }else{
  					productImg = new ProductImg();
  					productImg.setPath(filePath);
  					productImg.setProduct(product);
  					productImg.setSort(Integer.valueOf(imgSort)-2);
  					productImgService.save(productImg);
  				  }
  			    
  			  }else if(null != goods 
  					  && "2".equals(type)){
  				  //更新产品图片
  				 //更新商品图片
  				  ProductImg  productImg  = productImgService.findImgByGoodsIdAndSort(Long.valueOf(productIdStr), Integer.valueOf(imgSort)-1);
  				  if(null != productImg){
  					productImg.setPath(filePath);
  					productImgService.update(productImg);
  				  }else{
  					productImg = new ProductImg();
  					productImg.setPath(filePath);
  					productImg.setGoods(goods);
  					productImg.setSort(Integer.valueOf(imgSort)-1);
  					productImgService.save(productImg);
  				  }
  			  }else{
  				  sb.delete(0, sb.length());
	  			  sb.append(FileUtils.getName(path) + "图片对应的商品/产品找不到,请检查此商品/产品ID是否存在");
	  			  resultList.add(sb.toString());
  			  }
  	    }	
    	
    	return resultList ;
    }
    
    /**
     * <p>名称：上传压缩包，解压后，上传到oss</p> 
     * <p>描述：TODO</p>
     * @throws IOException 
     * @author：wangchen
     */					

    @RequestMapping(value = "/uploadPackgeImg",  method = RequestMethod.POST)
    @ResponseBody
	public String uploadPackgeImg(@RequestParam("file") MultipartFile file) throws IOException  {
    	
    	OSSClient ossClient = getOssClient();
    	ZipInputStream zipIn = new ZipInputStream(file.getInputStream()); 
    	ZipEntry zipEntry = null;
    	String name =null;
    	StringBuilder sb = new StringBuilder(); 
    	int successCount = 0;
    	int errorCount = 0;
    	while(null != (zipEntry = zipIn.getNextEntry())){
    		name = zipEntry.getName().toLowerCase();
    		if(name.indexOf("jpg")!=-1 || 
    		   name.indexOf("jpeg")!=-1 || 
    		   name.indexOf("gif")!=-1 || 
    		   name.indexOf("png")!=-1||
    		   name.indexOf("bmp")!=-1){
    			
    			try{
    				ossClient.putObject("aimon", name.split("/")[1], new ByteArrayInputStream(InputStreamTOByte(zipIn)));
    				sb.append(name.split("/")[1]+"成功上传<br/>");
    				successCount++;
    			}catch(Exception e){
    				sb.append(name.split("/")[1]+"上传失败<br/>");
    				errorCount++;
    			}
    		}		
    	}
    	ossClient.shutdown();
    	zipIn.close();
    	return "共成功上传："+successCount+"张<br/>共失败上传："+errorCount+"张<br/>"+sb.toString();
    }
    
    private OSSClient getOssClient(){
    	String endpoint = "http://oss-cn-shanghai.aliyuncs.com";
		String accessKeyId = "KudDSWR2yHbMYrBR";
		String accessKeySecret = "chHHYo2Q6HLPH7CkrfjqjWYq5jnT6S"; 
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		return ossClient;
    }
    
    public static byte[] InputStreamTOByte(InputStream in) throws IOException{
        
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] data = new byte[4096];
        int count = -1;
        while((count = in.read(data,0,4096)) != -1)
            outStream.write(data, 0, count);
         
        data = null;
        return outStream.toByteArray();
        
    }
    
}
