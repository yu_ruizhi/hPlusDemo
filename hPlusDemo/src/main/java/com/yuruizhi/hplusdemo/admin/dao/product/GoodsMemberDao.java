/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsMemberDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.product
 * Date: 2016年6月16日下午2:32:40
 **/
package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.List;
import java.util.Map;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsMember;

/**
 * <p>名称: 预售商品截单权限Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年6月16日下午2:32:47   
 * @version: 1.0
 */
@MyBatisMapper
public interface GoodsMemberDao extends BaseDao<GoodsMember> {
	
	/**
	 * <p>名称：通过单品id查找记录</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param paramMap
	 * @return
	 */
	List<GoodsMember> findByGoodsId(Map<String, Object> paramMap);
	
	/**
	 * <p>名称：通过单品id、会员id查找记录</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param paramMap
	 * @return
	 */
	List<GoodsMember> findByGoodsIdMemberId(Map<String, Object> paramMap);
}

