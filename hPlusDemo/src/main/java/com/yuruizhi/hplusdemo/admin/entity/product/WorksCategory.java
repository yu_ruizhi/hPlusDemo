/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: WorksCategory.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.product
 * Date: 2016年4月25日上午11:18:24
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 作品-分类实体</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月25日上午11:18:58
 * @version: 1.0
 */
public class WorksCategory extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 分类Id
	 * 
	 * @Column CATEGORY_ID
	 */
	private Long categoryId;

	/**
	 * 作品Id
	 * 
	 * @Column WORKS_ID
	 */
	private Long worksId;

	/**
	 * 删除标识
	 * 
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Long getWorksId() {
		return worksId;
	}

	public void setWorksId(Long worksId) {
		this.worksId = worksId;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
