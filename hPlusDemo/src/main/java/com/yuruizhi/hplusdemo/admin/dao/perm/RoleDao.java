package com.yuruizhi.hplusdemo.admin.dao.perm;

import java.util.List;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.Role;

/**
 * Created by ASUS on 2014/12/22.
 */
public interface RoleDao extends BaseDao<Role> {
    void saveRoleToPage(Map<String, Object> roleToPage);
    void deleteRoleToPageByRoleId(Long roleId);
    List<Role> getBaseRole(Map<String, Object> paramMap);
}
