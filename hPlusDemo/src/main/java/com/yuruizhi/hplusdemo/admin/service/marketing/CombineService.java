/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.marketing.Combine;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: CombineService</p>
 * <p>说明: 商品组合详情服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:12:43   
 * @version: 1.0
 */
public interface CombineService extends BaseService<Combine> {

	Map<String,Object> combineSave(Combine combine, String goodsIds, String productIds,String channel);

	Map<String,Object> combineUpdate(Combine combine, String goodsIds, String productIds, String ids,String channel);

	Map<String, Object> combineDelete(Long[] id);
}
