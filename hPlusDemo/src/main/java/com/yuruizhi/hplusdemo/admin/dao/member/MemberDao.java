package com.yuruizhi.hplusdemo.admin.dao.member;



import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import org.apache.ibatis.annotations.Param;
import org.ponly.webbase.domain.Filters;

import java.util.List;

public interface MemberDao extends BaseDao<Member> {
	
	String MEMBER_FILTERS_PROP = "_member_filters";
	String MEMBER_LEVEL_FILTERS_PROP = "_member_level_filters";
	String MEMBER_USER_FILTERS_PROP = "_member_user_filters";
	
	/**
	 * 
	 * <p>名称：根据用户名查找会员</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	List<Member> findByName(String name);
	
	/**
	 * 
	 * <p>名称：根据邮箱查找会员</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param email
	 * @return
	 */
	List<Member> findByEmail(String email);
	
	/**
	 * 
	 * <p>名称：根据手机查找会员</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param phone
	 * @return
	 */
	List<Member> findByPhone(String phone);
	
	/**
	 * 获取用户信息
	 * @param members
	 * @return
	 */
	Member getUserByUserEmail(Member members);
	 /**
     * 根据手机号码查询用户
     * @param phone 手机号
     * @return 用户
     */
    Member getUserByPhone(String phone);

	/**
	 * 新增
	 * @param members
	 * @return
	 */
	int insert(Member members);
	  /**
     * 忘记密码
     */
	Member forgetPwd(String email);
	/**
	 * 查询个人信息
	 * @param member
	 * @return
	 */
	Member selectById(Member member);
	List<Member> findMemberList(@Param("ids")Long[] ids);
	List<Member> getMembersBySmsId(@Param("smsId")Long smsId);
	int count(Filters filters, Long[] memberIds);
	List<Member> getMembersByEmailId(@Param("emailId")Long emailId);
	
	/**
	 * 根据会员编号查询会员
	 */
	Member findByCode(String code);

    void updateMember(Member member);

	/**
	 *
	 * <p>名称：查询所有的前台用户</p>
	 * <p>描述：</p>
	 * @author：yuruizhi
	 * @return: List<Member>
	 */
	List<Member> findAll();
	
}
