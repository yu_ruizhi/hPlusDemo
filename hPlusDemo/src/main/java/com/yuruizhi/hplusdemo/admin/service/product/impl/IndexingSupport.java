/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * TODO 最后更新时间,更新状态
 */
public abstract class IndexingSupport {
    private static final Logger LOG = LoggerFactory.getLogger(IndexingSupport.class);
    private SolrClient solrClient;

    /**
     * 更新 给定 id 的索引
     *
     * @param id     需要更新的 Document id
     * @param delete 是否是删除
     */
    protected void doUpdate(String id, boolean delete) {
        if (!StringUtils.hasText(id)) {
            return;
        }
        final SolrClient solrClient = getRequiredSolrClient();
        try {
            LOG.info("start update index...");

            Object bean = null;
            if (delete || null == (bean = getIndexedBean(id))) {
                LOG.info("delete document id: {}", id);
                solrClient.deleteById(id);
            } else {
                SolrInputDocument doc = solrClient.getBinder().toSolrInputDocument(bean);
                LOG.info("update document: {}", doc);
                solrClient.add(doc);
            }

            solrClient.commit();
        } catch (SolrServerException e) {
            LOG.error("update index failed.. {}", e);
            quietRollback(solrClient);
        } catch (IOException e) {
            LOG.error("update index failed.. {}", e);
            quietRollback(solrClient);
        }
    }

    /**
     * 重建索引
     */
    protected void doReindex() {
        final SolrClient solrClient = getRequiredSolrClient();
        LOG.info("start rebuild index...");
        try {
            solrClient.deleteByQuery("*:*");

            long count = countIndexedBeans();
            if (1 > count) {
                return;
            }

            int size = 1000;
            for (int offset = 0; offset < count; offset += size) {
                LOG.info("update {} to {}", offset, offset + size);
                List<?> beans = getPagedIndexedBeans(offset, size);
                if (null != beans) {
                    LOG.info("wait update size: " + beans.size());
                    solrClient.addBeans(beans);
                }
            }
            solrClient.commit();
            LOG.info("rebuild index complete, total: {}", count);
        } catch (IOException e) {
            LOG.error("rebuild index failed", e);
            quietRollback(solrClient);
        } catch (SolrServerException e) {
            LOG.error("rebuild index failed: {}", e);
            quietRollback(solrClient);
        }
    }

    protected void doTruncate() {
        final SolrClient solrClient = getRequiredSolrClient();
        try {
            solrClient.deleteByQuery("*:*");
            solrClient.commit();
        } catch (IOException e) {
            LOG.error("truncate index failed", e);
            quietRollback(this.solrClient);
        } catch (SolrServerException e) {
            LOG.error("truncate index failed: {}", e);
            quietRollback(this.solrClient);
        }
    }

    private void quietRollback(SolrClient solrClient) {
        try {
            solrClient.rollback();
        } catch (SolrServerException ignore) {
        } catch (IOException ignore) {
        }
    }

    private SolrClient getRequiredSolrClient() {
        SolrClient solrClient = getSolrClient();
        if (null == solrClient) {
            throw new IllegalStateException("No SolrClient found: no SolrClient configure?");
        }
        return solrClient;
    }

    protected abstract Object getIndexedBean(String id);

    protected abstract long countIndexedBeans();

    protected abstract List<?> getPagedIndexedBeans(int offset, int size);

    public SolrClient getSolrClient() {
        return solrClient;
    }

    public void setSolrClient(SolrClient solrClient) {
    	this.solrClient = solrClient;
    }
}
