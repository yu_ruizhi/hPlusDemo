package com.yuruizhi.hplusdemo.admin.controller.logistics;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryArea;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryAreaService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryMethodDetailService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryMethodService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;


@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/deliveryMethod")
public class DeliveryMethodController extends BaseController {
    @Autowired
    private DeliveryMethodService deliveryMethodService;
    @Autowired
    private DeliveryMethodDetailService deliveryMethodDetailService;
    @Autowired
    private DeliveryAreaService deliveryAreaService;

    @Autowired
    private UserService userService;

    @RequestMapping("index")
    public void index() { }

    @RequestMapping("list")
    @ResponseBody
    public Page<DeliveryMethod> list1(Filters filters, Pageable pageable) {
        Page<DeliveryMethod> mlist = deliveryMethodService.findPage(filters, pageable);

        return mlist;
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, HttpServletResponse response) {
        List<DeliveryArea> deliveryAreas = deliveryAreaService.selectAll();
        request.setAttribute("deliveryAreas", deliveryAreas);
        return getViewPath("add");
    }

    @RequestMapping(value = "save")
    public String save1(DeliveryMethod deliveryMethod, HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        Long currentUser = UserUtils.getCurrentUserId();
        User user = userService.find(currentUser);
        if (null == deliveryMethod.getId()) {
            deliveryMethod.setCreatedBy(user.getLoginName());
            deliveryMethod.setCreatedDate(new Date());
            deliveryMethod.setDeleted(false);
            if (deliveryMethod.getVisible() == null) {
                deliveryMethod.setVisible(false);
            }
            deliveryMethodService.addmethod(deliveryMethod);
        } else {
            deliveryMethod.setCreatedBy(user.getLoginName());
            deliveryMethod.setCreatedDate(new Date());
            if(null==deliveryMethod.getVisible())
                deliveryMethod.setVisible(false);
            deliveryMethodService.bianji(deliveryMethod);
        }
        return redirectViewPath("index");
    }

    @RequestMapping("delete")
    @ResponseBody
    public Map<String, Object> delete1(@RequestParam("id") Long[] id) {
        deliveryMethodService.delete(id);
        for (Long iid : id) {
            deliveryMethodDetailService.deletedByMethodId(iid);
        }
        return ImmutableMap.<String, Object>of("success", true);
    }

    @RequestMapping("edit/{id}")
    public ModelAndView edit1(@PathVariable(value = "id") Long id) {
        ModelAndView mav = new ModelAndView();
        DeliveryMethod deliveryMethod = deliveryMethodService.find(id);
        mav.addObject("deliveryMethod", deliveryMethod);
        List<DeliveryArea> deliveryAreas = deliveryAreaService.findAll();
        mav.addObject("deliveryAreas", deliveryAreas);
        mav.setViewName(getViewPath("edit"));
        return mav;
    }

    /**
	 * 检测用户名
	 * @param name
	 * @return
	 */
	@RequestMapping("checkIsNameExists")
	@ResponseBody
	public Map<String, Boolean> checkIsNameExists(String name){
		return deliveryMethodService.checkIsNameExists(name);
	}
	
	@RequestMapping("hasAnnotherName")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherName(String[] idAndName){
		Map<String, Boolean> map = deliveryMethodService.hasAnnotherName(idAndName);
		return map;
	}

}
