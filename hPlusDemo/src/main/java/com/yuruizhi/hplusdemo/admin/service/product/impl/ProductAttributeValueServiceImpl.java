/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.ProductAttributeValueDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductAttributeValue;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.ProductAttributeValueService;

/**
 * <p>名称: ProductAttributeValueServiceImpl</p>
 * <p>说明: 产品属性值服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:56:14   
 * @version: 1.0
 */
@Service
public class ProductAttributeValueServiceImpl extends BaseServiceImpl<ProductAttributeValue> implements ProductAttributeValueService {
    @Autowired
    private ProductAttributeValueDao productAttributeValueDao;


    public void insert(ProductAttributeValue pav){
        productAttributeValueDao.insert(pav);
    }

    @Override
    public ProductAttributeValue getAttributeValueByProductIdAndAttributeId(ProductAttributeValue pav) {
        return productAttributeValueDao.getAttributeValueByProductIdAndAttributeId(pav);
    }

}
