/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.details.impl;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.details.DetailsDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.NewOrHotDao;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberDao;
import com.yuruizhi.hplusdemo.admin.entity.details.Details;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.details.DetailsService;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.util.CipherUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.MailSender;
import com.yuruizhi.hplusdemo.admin.util.PassWordSaltUtil;
import com.yuruizhi.hplusdemo.admin.util.sms.SmsSender;
import com.google.common.collect.Maps;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * <p>名称: DetailsServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：zhaochuang
 * @date：2016年5月31日上午9:23:51   
 * @version: 1.0
 */
@Service
public class DetailsServiceImpl extends BaseServiceImpl<Details> implements
		DetailsService {
	@Autowired
	private DetailsDao detailsDao;

	@Autowired
	public void setOrdersReceiptDao(DetailsDao detailsDao) {
		super.setBaseDao(detailsDao);
		this.detailsDao = detailsDao;
	}


}
