/*
* Copyright (c) 2005, 2014 vacoor
*
* Licensed under the Apache License, Version 2.0 (the "License");
*/
package com.yuruizhi.hplusdemo.admin.dao.provider;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.dto.SupplierGoodsDTO;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierGoodsList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* Created on 2014-10-30 17:10:16
*
* @author crud generated
*/
public interface SupplierGoodsListDao extends BaseDao<SupplierGoodsList> {
	
	List<SupplierGoodsList> getListBySupplierId(@Param("supplierId") String supplierId);
	
	List<SupplierGoodsList> getListByGoodsId(String goodsCode);
	
	SupplierGoodsList getDetails(String goodsCode, String supplierId);

    List<SupplierGoodsDTO> findSupplierGoods(String sid);

	void updateGoods(SupplierGoodsList supplierGoodsList);
}