/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: NewOrHotDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月20日 上午9:28:38
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.NewOrHot;

/**
 * 
 * <p>名称: 新品上架\APP首页艾曼热卖 DAO</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月20日 上午9:28:38 
 * @version: 1.0
 */
public interface NewOrHotDao extends BaseDao<NewOrHot> {

	String BASE_FILTERS_PROP = "_new_or_hot_filters";
	String PRODUCT_FILTERS_PROP = "_product_filters";
	String CATEGORY_FILTERS_PROP = "_category_filters";
	String GOODS_FILTERS_PROP = "_goods_filters";
}
