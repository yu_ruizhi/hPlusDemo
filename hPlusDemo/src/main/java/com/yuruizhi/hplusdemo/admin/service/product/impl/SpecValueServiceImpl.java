/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.SpecValueDao;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.SpecValueService;
import com.google.common.collect.Maps;

/**
 * <p>名称: SpecValueServiceImpl</p>
 * <p>说明: 产品规格值服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:58:32   
 * @version: 1.0
 */
@Service
public class SpecValueServiceImpl extends BaseServiceImpl<SpecValue> implements SpecValueService {

    private SpecValueDao specValueDao;

    @Autowired
    public void setShelfDao(SpecValueDao specValueDao) {
        this.specValueDao = specValueDao;
        super.setBaseDao(specValueDao);
    }

    @Override
    public SpecValue specValueBean(String id) {
        return specValueDao.specValueBean(id);
    }

	@Override
	public Map<String, Boolean> checkIsCodeExist(String code) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<SpecValue> specList = specValueDao.findByCode(code);
		if(specList.size() > 0) {
			model.put("isExist", true);
		} else {
			model.put("isExist", false);
		}
		return model;
	}

	@Override
	public Map<String, Boolean> editCheckIsCodeExist(Long id, String code) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<SpecValue> specList = specValueDao.findByCode(code);
		if (specList.size() > 0) {
			// 已经存在记录（自身也会被认为存在），还需判断已存在的记录是否就是要修改的记录
			for (SpecValue spec : specList) {
				if (spec.getId().equals(id)) {
					model.put("isExist", false);
					break;
				} else {
					model.put("isExist", true);
					break;
				}
			}
		} else {
			model.put("isExist", false);
		}
		return model;
	}

	@Override
	public SpecValue findBySpecIdAndName(Long specId,String name) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("specId", specId);
		map.put("name", name);
		return specValueDao.findBySpecIdAndName(map);
	}
}
