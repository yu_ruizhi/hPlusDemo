/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ActivityServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月21日 下午2:19:15  
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.marketing.ActivityDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Activity;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.ActivityService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 活动管理ServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午2:19:15   
 * @version: 1.0
 */
@Service
public class ActivityServiceImpl extends BaseServiceImpl<Activity> implements ActivityService{

	private ActivityDao activityDao;
	
	@Autowired
	public void setActivityDao(ActivityDao activityDao){
		this.activityDao = activityDao;
		setCrudDao(activityDao);
	}
	
	@Override
	public Map<String, Boolean> checkIsNameExists(String name) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<Activity> activityList = activityDao.findByName(name);
		if (null != activityList && activityList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

	@Override
	public Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<Activity> activityList = activityDao.findByName(hasAnnotherName[1]);
		if (null != activityList && activityList.size() > 0) {
			Iterator<Activity> it = activityList.iterator();
			Activity activity = it.hasNext() ? it.next() : null;
			result.put("isExists", null != activity && !activity.getId().equals(Long.valueOf(hasAnnotherName[0])));
		}
		return result;
	}
	
}
