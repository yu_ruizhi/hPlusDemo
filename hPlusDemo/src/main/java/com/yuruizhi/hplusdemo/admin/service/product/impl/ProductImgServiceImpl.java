/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.product.ProductImg;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.ProductImgService;
import com.google.common.collect.Maps;

/**
 * <p>名称: ProductImgServiceImpl</p>
 * <p>说明: 产品图片服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:56:47   
 * @version: 1.0
 */
@Service
public class ProductImgServiceImpl extends BaseServiceImpl<ProductImg> implements ProductImgService {

	@Override
	public ProductImg findImgByProIdAndSort(Long productId, Integer sort) {
		Map<String,Object> paramMap = Maps.newHashMap();
		paramMap.put("product.id", productId);
		paramMap.put("sort", sort);
		paramMap.put("deleted", false);
		List<ProductImg> list = findMany(paramMap);
		
		if(null != list && list.size() > 0){
			return list.get(0);
		}
		
		return null;
	}

	@Override
	public ProductImg findImgByGoodsIdAndSort(Long goodsId, Integer sort) {
		Map<String,Object> paramMap = Maps.newHashMap();
		paramMap.put("goods.id", goodsId);
		paramMap.put("sort", sort);
		paramMap.put("deleted", false);
		List<ProductImg> list = findMany(paramMap);
		
		if(null != list && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

}
