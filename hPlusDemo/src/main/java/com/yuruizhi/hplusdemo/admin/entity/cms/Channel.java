/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.cms;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.TreeNodeBaseEntity;

/**
 * <p>名称: Channel</p>
 * <p>说明: 栏目实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:42:38   
 * @version: 1.0
 */
public class Channel extends TreeNodeBaseEntity<Channel> {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -7823199980969437872L;

	/**
     * 上级栏目
     * 
     * @ViewField editor=treepicker internal="id:name" 
     * @ManyToOne joinColumn=PARENT_ID optional=true
     */
    private Channel parent;

    /**
     * 栏目编码
     * 
     * @ViewField editor=input 
     * @Column CODE
     */
    @Size(max = 255)
    private String code;

    /**
     * 栏目名称
     * 
     * @ViewField editor=input 
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 栏目图标/缩略图
     * 
     * @ViewField editor=input 
     * @Column IMAGE
     */
    @Size(max = 255)
    private String image;

    /**
     * 栏目链接
     * 如果存在刚点击栏目时直接跳转到该地址
     * 
     * @ViewField editor=input 
     * @Column HREF
     */
    @Size(max = 255)
    private String href;

    /**
     * 栏目模型(预留,考虑参考商品模型)
     * 
     * @ViewField editor=spinner 
     * @Column MODEL_ID
     */
    private Integer modelId;

    /**
     * 每页记录数
     * 
     * @ViewField editor=spinner 
     * @Column PAGE_SIZE
     */
    private Integer pageSize;

    /**
     * 栏目排序
     * 
     * @ViewField editor=spinner 
     * @Column POSITION
     */
    private Integer position;

    /**
     * 最大层级
     * 
     * @ViewField editor=spinner 
     * @Column MAX_DEPTH
     */
    private Integer maxDepth;

    /**
     * 列表页模板(预留）
     * 
     * @ViewField editor=input 
     * @Column LIST_TEMPLATE
     */
    @Size(max = 255)
    private String listTemplate;

    /**
     * 详情页模板(预留)
     * 
     * @ViewField editor=input 
     * @Column DETAIL_TEMPLATE
     */
    @Size(max = 255)
    private String detailTemplate;

    /**
     * 是否显示
     * 
     * @ViewField editor=input 
     * @Column VISIBLE
     */
    private Boolean visible = Boolean.FALSE; ;

    /**
     * 是否预定义
     * 
     * @ViewField editor=input 
     * @Column PREDEFINED
     */
    private Boolean predefined = Boolean.FALSE; ;

    /**
     * 文章数
     * 
     * @ViewField editor=spinner 
     * @Column TERMS
     */
    private Integer terms;

    /**
     * 文章缩略图宽
     * 
     * @ViewField editor=spinner 
     * @Column WIDTH
     */
    private Integer width;

    /**
     * 文章缩略图高
     * 
     * @ViewField editor=spinner 
     * @Column HEIGHT
     */
    private Integer height;

    /**
     * 是否允许评论
     * 
     * @ViewField editor=input 
     * @Column ALLOW_COMMENT
     */
    private Boolean allowComment = Boolean.FALSE; ;

    /**
     * 评论是否需要审核
     * 
     * @ViewField editor=input 
     * @Column ENABLE_REVIEW
     */
    private Boolean enableReview = Boolean.FALSE; ;

    /**
     * 备注
     * 
     * @ViewField editor=input 
     * @Column NOTE
     */
    @Size(max = 255)
    private String note;

    /**
     * SEO_TITLE
     * 
     * @ViewField editor=input 
     * @Column SEO_TITLE
     */
    @Size(max = 255)
    private String seoTitle;

    /**
     * SEO_KEYWORDS
     * 
     * @ViewField editor=input 
     * @Column SEO_KEYWORDS
     */
    @Size(max = 255)
    private String seoKeywords;

    /**
     * SEO_DESCRIPTION
     * 
     * @ViewField editor=input 
     * @Column SEO_DESCRIPTION
     */
    @Size(max = 255)
    private String seoDescription;

    /**
     * 层级/深度
     * 
     * @ViewField editor=input 
     * @Column DEPTH
     */
    private Integer depth;

    /**
     * 路径
     * 
     * @ViewField editor=input 
     * @Column PATH
     */
    @Size(max = 255)
    private String path;

    /**
     * 是否叶子节点
     * 
     * @ViewField editor=input 
     * @Column IS_LEAF
     */
    private Boolean isLeaf;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Channel getParent() {
        return this.parent;
    }

    public void setParent(Channel parent) {
        this.parent = parent;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Integer getModelId() {
        return this.modelId;
    }

    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }

    public Integer getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getMaxDepth() {
        return this.maxDepth;
    }

    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    public String getListTemplate() {
        return this.listTemplate;
    }

    public void setListTemplate(String listTemplate) {
        this.listTemplate = listTemplate;
    }

    public String getDetailTemplate() {
        return this.detailTemplate;
    }

    public void setDetailTemplate(String detailTemplate) {
        this.detailTemplate = detailTemplate;
    }

    public Boolean getVisible() {
        return this.visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getPredefined() {
        return this.predefined;
    }

    public void setPredefined(Boolean predefined) {
        this.predefined = predefined;
    }

    public Integer getTerms() {
        return this.terms;
    }

    public void setTerms(Integer terms) {
        this.terms = terms;
    }

    public Integer getWidth() {
        return this.width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return this.height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Boolean getAllowComment() {
        return this.allowComment;
    }

    public void setAllowComment(Boolean allowComment) {
        this.allowComment = allowComment;
    }

    public Boolean getEnableReview() {
        return this.enableReview;
    }

    public void setEnableReview(Boolean enableReview) {
        this.enableReview = enableReview;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSeoTitle() {
        return this.seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoKeywords() {
        return this.seoKeywords;
    }

    public void setSeoKeywords(String seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    public String getSeoDescription() {
        return this.seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }


    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public Boolean getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(Boolean isLeaf) {
		this.isLeaf = isLeaf;
	}

	public Integer getDepth() {
		return depth;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}


}
