package com.yuruizhi.hplusdemo.admin.service.logistics.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.logistics.DeliveryMethodDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethodDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryMethodDetailService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryMethodService;
import com.google.common.collect.Maps;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by ASUS on 2014/12/9.
 */
@Service
public class DeliveryMethodServiceImpl extends BaseServiceImpl<DeliveryMethod> implements DeliveryMethodService{


    private DeliveryMethodDao deliveryMethodDao;
@Autowired
    private DeliveryMethodDetailService deliveryMethodDetailService;
    @Autowired
    public void setAdminUserDao(DeliveryMethodDao deliveryMethodDao){
        this.deliveryMethodDao = deliveryMethodDao;
        setBaseDao(deliveryMethodDao);
    }



    @Override
    public List<DeliveryMethod> selectMethodGoCityId(String areaId) {
        return deliveryMethodDao.selectMethodGoCityId(areaId);
    }

    @Override
    public void addmethod(DeliveryMethod dd) {
        save(dd);
        if(dd.getAreaIsa()!=null&&dd.getAreaIsa().length>0){
            for(String areaId:dd.getAreaIsa()){
            	DeliveryMethodDetail deliveryMethodDetail=new DeliveryMethodDetail();
                deliveryMethodDetail.setDeliveryMethodId(dd.getId());
                deliveryMethodDetail.setDeliveryAreaId(areaId);
                deliveryMethodDetail.setDeleted(false);
                deliveryMethodDetail.setCreatedBy(dd.getCreatedBy());
                deliveryMethodDetail.setCreatedDate(new Date());
                deliveryMethodDetailService.save(deliveryMethodDetail);
            }
        }
    }

    @Override
    public void bianji(DeliveryMethod dm) {
        update(dm);
        deliveryMethodDetailService.deletedMethodId(dm.getId());
        if(dm.getAreaIsa()!=null&&dm.getAreaIsa().length>0){
            for(String areaId:dm.getAreaIsa()){
            	DeliveryMethodDetail deliveryMethodDetail=new DeliveryMethodDetail();
                deliveryMethodDetail.setDeliveryMethodId(dm.getId());
                deliveryMethodDetail.setDeliveryAreaId(areaId);
                deliveryMethodDetail.setDeleted(false);
                deliveryMethodDetail.setCreatedBy(dm.getCreatedBy());
                deliveryMethodDetail.setCreatedDate(new Date());
                deliveryMethodDetailService.save(deliveryMethodDetail);
            }
        }
    }
    
    /**
     * 保存验证重名
     */
    @Override
	public Map<String, Boolean> checkIsNameExists(String name) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<DeliveryMethod> deliveryMethodList = deliveryMethodDao.findByName(name);
		if (deliveryMethodList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

    /**
	 * 更新验证重名
	 */
	@Override
	public Map<String, Boolean> hasAnnotherName(String[] hasAnnothername) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<DeliveryMethod> deliveryMethodList = deliveryMethodDao.findByName(hasAnnothername[1]);
		Iterator<DeliveryMethod> it = deliveryMethodList.iterator();
		DeliveryMethod deliveryMethod = it.hasNext() ? it.next() : null;
		result.put("isExists", null != deliveryMethod && !deliveryMethod.getId().equals(Long.valueOf(hasAnnothername[0])));
		return result;
	}
}
