/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.order.ProblemOrderDao;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import com.yuruizhi.hplusdemo.admin.entity.order.ProblemOrder;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.order.ProblemOrderService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import com.google.common.collect.Maps;


/**
 * <p>名称: ProblemOrderServiceImpl</p>
 * <p>说明: 问题订单服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:30:13   
 * @version: 1.0
 */
@Service
public class ProblemOrderServiceImpl extends BaseServiceImpl<ProblemOrder> implements ProblemOrderService {
    @Autowired
    private ProblemOrderDao problemOrderDao;
    @Autowired
    private UserService userService;
    @Autowired
    public void setProblemOrderDao(ProblemOrderDao problemOrderDao) {
        super.setBaseDao(problemOrderDao);
        this.problemOrderDao = problemOrderDao;
    }

    @Override
    public void updateStatus(Long id) {
        ProblemOrder problemOrder=new ProblemOrder();
        problemOrder.setId(id);
        Long adminUserId= UserUtils.getCurrentUserId();
        User user= userService.find(adminUserId);
        String adminName=user.getLoginName();
        problemOrder.setLastModifiedBy(adminName);
        problemOrder.setCutStatus(OrderAllot.CUT_REFUND);
        problemOrder.setLastModifiedDate(new Date());
         problemOrderDao.update(problemOrder);
    }

	@Override
	public Long[] findIdByTime(String beginDate, String endDate) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		return problemOrderDao.findIdByTime(map);
	}
}


