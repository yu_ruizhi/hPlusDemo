package com.yuruizhi.hplusdemo.admin.dao.logistics;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModuleDetail;

/**
 * Created by ASUS on 2015/1/13.
 */
public interface DeliveryModuleDetailDao extends BaseDao<DeliveryModuleDetail> {

    void deleteByModuleId(Long moduleId);

}
