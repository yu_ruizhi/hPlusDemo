/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import com.yuruizhi.hplusdemo.admin.dao.product.FavoritesDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Favorites;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.FavoritesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>名称: FavoritesServiceImpl</p>
 * <p>说明: 收藏服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:51:33   
 * @version: 1.0
 */
@Service
public class FavoritesServiceImpl extends BaseServiceImpl<Favorites> implements FavoritesService {

	@Autowired
	public void setFavoriterDao(FavoritesDao favoriterDao) {
		super.setBaseDao(favoriterDao);
	}


}
