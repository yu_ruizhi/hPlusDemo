/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: RecommendWorksService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月21日 下午1:32:04 
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.entity.marketing.RecommendWorks;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 推荐作品 Servcie</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月21日 下午1:32:04 
 * @version: 1.0
 */
public interface RecommendWorksService extends BaseService<RecommendWorks> {

	/**
	 * 
	 * <p>名称：异步获取列表数据</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param recommendWorksFilters 推荐作品对象过滤器
	 * @param worksFilters 作品对象过滤器
	 * @param pageable
	 * @return
	 */
	Page<RecommendWorks> findPage(Filters recommendWorksFilters, 
			Filters worksFilters, Pageable pageable);

}
