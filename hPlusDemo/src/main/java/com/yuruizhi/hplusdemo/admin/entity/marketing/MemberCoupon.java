/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: MemberCoupon</p>
 * <p>说明: 会员优惠券实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:05:32   
 * @version: 1.0
 */
public class MemberCoupon extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7984282999028690469L;

	/**
     * 会员ID
     * 
     * @ViewField editor=spinner 
     * @Column MEMBER_ID
     */
    @NotNull
    private Long memberId;

    /**
     * 优惠券ID
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=COUPON_ID optional=true
     */
    @NotNull
    private Coupon coupon;
    
    /**
     * 自动发放优惠券是的订单ID
     */
    private Long orderId;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Coupon getCoupon() {
        return this.coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	/**
	 * @return the memberId
	 */
	public Long getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

}
