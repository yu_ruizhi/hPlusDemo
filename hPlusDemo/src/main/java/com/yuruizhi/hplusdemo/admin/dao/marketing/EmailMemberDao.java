/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.apache.ibatis.annotations.Param;
import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailMember;

/**
 * Created on 2016-04-14 15:43:33
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface EmailMemberDao extends BaseDao<EmailMember> {

	void deleteByEmailId(@Param("emailId")Long emailId);
}