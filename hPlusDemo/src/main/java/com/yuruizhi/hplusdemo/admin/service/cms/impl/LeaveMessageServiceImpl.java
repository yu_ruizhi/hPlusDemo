/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.cms.LeaveMessageDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.LeaveMessage;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.LeaveMessageService;

/**
 * <p>名称: LeaveMessageServiceImpl</p>
 * <p>说明: 留言服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:01:34   
 * @version: 1.0
 */
@Service
public class LeaveMessageServiceImpl extends BaseServiceImpl<LeaveMessage> implements LeaveMessageService {

    @Autowired
    public void setHatAreaDao(LeaveMessageDao leaveMessageDao) {
        super.setBaseDao(leaveMessageDao);
    }

}
