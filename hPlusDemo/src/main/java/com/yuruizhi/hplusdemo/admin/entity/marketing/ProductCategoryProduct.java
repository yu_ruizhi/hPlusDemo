/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryProduct.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月20日 上午10:23:44 
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;

/**
 * 
 * <p>名称: 分类商品-商品实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:23:44   
 * @version: 1.0
 */
public class ProductCategoryProduct extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**@Fields goodsId : 单品ID */ 
	private Long goodsId;

	/**@Fields goods : 单品 */ 
	private Goods goods;

	/**@Fields categoryId : 分类商品ID */ 
	private Long categoryId;

	/**@Fields sort : 排序 */ 
	private Integer sort;

	/**@Fields deleted : 删除标识 */ 
	private Boolean deleted = false;

	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}
	
}
