package com.yuruizhi.hplusdemo.admin.controller.product;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Label;
import com.yuruizhi.hplusdemo.admin.service.product.LabelService;
import com.google.common.collect.ImmutableMap;

@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/label")
public class LabelController extends BaseController {

	@Autowired
	private LabelService labelService;
	/*
	 * 跳到增加页面
	 */

	@RequestMapping("add")
	private void add() {}

	/*
	 * 跳到增加首页
	 */
	@RequestMapping("index")
	private void index() {}

	@ResponseBody
	@RequestMapping("list")
	public Page<Label> list(Filters filetes, Pageable pageable) {
		Page<Label> list = labelService.findPage(filetes, pageable);
		return list;
	}

	/**
	 * 增加
	 */
	@ResponseBody
	@RequestMapping("save")
	public ModelAndView save(Label label) {
		ModelAndView mav = new ModelAndView();
		label.setCreatedDate(new Date());
		labelService.save(label);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}

	/**
	 * 删除
	 */

	@ResponseBody
	@RequestMapping("delete")
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		labelService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}

	/**
	 * 修改
	 */
	@RequestMapping("edit/{id}")
	public String edit(@PathVariable(value = "id") Long id, Map<String, Object> model) {
		Label label = labelService.find(id);
		model.put("label", label);
		return getRelativeViewPath("edit");
	}

	@ResponseBody
	@RequestMapping("update")
	public ModelAndView update(Label label) {
		ModelAndView mav = new ModelAndView();
		label.setLastModifiedDate(new Date());
		labelService.update(label);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class,
				new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), false));
	}
}
