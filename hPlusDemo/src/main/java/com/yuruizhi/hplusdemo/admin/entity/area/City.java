/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: City.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.area
 * Date: 2016年4月18日下午6:01:05
 **/
package com.yuruizhi.hplusdemo.admin.entity.area;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 城市实体</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午6:01:39
 * @version: 1.0
 */
public class City extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**@Fields cityId : 市编号*/
	private String cityId;

	/**@Fields cityName : 城市名字*/
	private String cityName;

	/**@Fields pid : 省编号*/
	private String pid;
	
	/**@Fields areaList : 区列表*/
	private List<Area> areaList;

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public List<Area> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}

}
