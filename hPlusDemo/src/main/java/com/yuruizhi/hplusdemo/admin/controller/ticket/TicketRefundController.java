package com.yuruizhi.hplusdemo.admin.controller.ticket;

import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketRefund;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.yuruizhi.hplusdemo.admin.service.ticket.TicketRefundService;
import com.yuruizhi.hplusdemo.admin.util.MatrixToImageWriter;
import com.google.common.collect.Maps;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import org.apache.commons.lang3.StringUtils;
import org.ponly.webbase.dao.CrudDao;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;

import javax.imageio.ImageIO;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * 单品控制器
 */
@Controller
@RequestMapping("ticketRefund")
public class TicketRefundController {

	@Autowired
	private TicketRefundService ticketRefundService;

	@RequestMapping("index")
	public String index(){
		return "ticketRefund/index";
	}
	
	@ResponseBody
	@RequestMapping("list")
	public Page<TicketRefund> list(Filters filters,Pageable pageable){
		Page<TicketRefund> ticketRefundPage=ticketRefundService.findPage(filters,pageable);
		return ticketRefundPage;
	}

	@RequestMapping("edit/{id}")
	public String edit(@PathVariable Long id, Model model){
      TicketRefund ticketRefund=ticketRefundService.find(id);
		model.addAttribute("ticketRefund",ticketRefund);
		return "ticketRefund/detail";
	}

	@RequestMapping("jujue")
	public String jujue(Long [] ids,Map<String,Object> map){
		  if(ids!=null){
			  map.put("type",4);
         map.put("ids",ids);
			  ticketRefundService.updateType(map);
		  }
		return "redirect:/ticketRefund/index";
	}

	@RequestMapping("tongyi")
	public String tongyi(Long [] ids,Map<String,Object> map){
		if(ids!=null){
			map.put("type",2);
			map.put("ids",ids);
			ticketRefundService.updateType(map);
		}
		return "redirect:/ticketRefund/index";
	}

	@RequestMapping("wancheng")
	public String wancheng(Long [] ids,Map<String,Object> map){
		if(ids!=null){
			map.put("type",3);
			map.put("ids",ids);
			ticketRefundService.updateType(map);
		}
		return "redirect:/ticketRefund/index";
	}

	@RequestMapping("createImage")
	public void createImage(HttpServletRequest request, HttpServletResponse response, String code)throws Exception {
		int width = 100;
		int height = 100;
		Hashtable hints= new Hashtable();
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		BitMatrix bitMatrix = new MultiFormatWriter().encode(code, BarcodeFormat.QR_CODE, width, height,hints);
		BufferedImage bi =  MatrixToImageWriter.toBufferedImage(bitMatrix);
		if (null != bi) {
			ImageIO.write(bi, "JPG", response.getOutputStream());
		}
	}

}
