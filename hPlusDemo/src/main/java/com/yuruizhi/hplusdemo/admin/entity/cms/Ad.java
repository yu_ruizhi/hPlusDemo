/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.cms;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.*;

/**
 * 广告
 * 
 * @author Administrator
 * @Table T_AD
 */
public class Ad extends BaseEntity {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -1702079258237506395L;

	/**
     * 投放范围(广告位)
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=AD_BLOCK_ID
     */
    private AdBlock adBlock;
    
    /**
     * 广告名称
     * 
     * @ViewField editor=input 
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 广告类型(图片,flash)
     * 广告类型: 图片/视频/flash
     * 
     * @ViewField editor=input 
     * @Column TYPE
     */
    @Size(max = 255)
    private String type;

    /**
     * 资源路径
     * 
     * @ViewField editor=input 
     * @Column PATH
     */
    @Size(max = 255)
    private String path;

    /**
     * 跳转链接
     * 
     * @ViewField editor=input 
     * @Column HREF
     */
    @Size(max = 255)
    private String href;

    /**
     * 广告说明
     * 
     * @ViewField editor=input 
     * @Column NOTE
     */
    @Size(max = 255)
    private String note;

    /**
     * 排序
     * 
     * @ViewField editor=spinner 
     * @Column POSITION
     */
    private Integer position;

    /**
     * 起始时间
     * 
     * @ViewField editor=datepicker 
     * @Column START_TIME
     */
    private Date startTime;

    /**
     * 结束时间
     * 
     * @ViewField editor=datepicker 
     * @Column END_TIME
     */
    private Date endTime;

    private String startTimez;

    /**
     * 结束时间
     *
     * @ViewField editor=datepicker
     * @Column END_TIME
     */
    private String endTimez;

    /**
     * 是否发布
     * 
     * @ViewField editor=input 
     * @Column ENABLED
     */
    private Boolean enabled = Boolean.FALSE; ;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE;

    public String getStartTimez() {
        return startTimez;
    }

    public void setStartTimez(String startTimez) {
        this.startTimez = startTimez;
    }

    public String getEndTimez() {
        return endTimez;
    }

    public void setEndTimez(String endTimez) {
        this.endTimez = endTimez;
    }

    public AdBlock getAdBlock() {
        return this.adBlock;
    }

    public void setAdBlock(AdBlock adBlock) {
        this.adBlock = adBlock;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
