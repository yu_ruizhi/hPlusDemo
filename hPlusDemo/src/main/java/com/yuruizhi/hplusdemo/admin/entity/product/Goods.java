/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.provider.*;
/**
 * <p>名称: Goods</p>
 * <p>说明: 单品实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:20:57   
 * @version: 1.0
 */
public class Goods extends BaseEntity {
    /**
     * @fieldName: serialVersionUID
     * @fieldType: long
     * @Description: TODO
     */
    private static final long serialVersionUID = 6598910957522289552L;
    /**
     * 商品标题
     * @Column
     */
    private String title;
    /**
     * 商品简介/卖点
     * @Column
     */
    private String intro;
    /**
     * 商品编码
     * @Column
     */
    private Long productId;

    @JsonUnwrapped(prefix = "code")
    private String code;
    /**
     * 商品条形码
     * @Column
     */
    private String barcode;
    /**
     * 价格
     * @Column
     */
    private BigDecimal price;
    /**
     * 市场价
     * @Column
     */
    private BigDecimal marketPrice;


    /**
     * 库存
     * @Column
     */
    private Long stock;

    /**
     * 锁定库存
     * @Column
     */
    private Long lockedStock;
    /**
     * 损坏库存
     * @Column
     */
    private Long damagedStock;
    /**
     * 其他库存
     * @Column
     */
    private Long otherStock;
    /**
     * 主图
     * @Column
     */
    private String image;
    /**
     * 产品大图
     * @Column
     */
    private String bigImage;

    public String getBigImage() {
        return bigImage;
    }

    public void setBigImage(String bigImage) {
        this.bigImage = bigImage;
    }
    /**
     * 供应商 暂时未用到
     */
    private String suppliers;
    public String getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(String suppliers) {
        this.suppliers = suppliers;
    }


    /**
     * 供应商实体
     */
    @JsonUnwrapped(prefix = "supplier.")
    private Supplier supplier;

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    /**
     * 供应商ID 注意区别其上的字符串类型的供应商，目前不知道其作用，所以没进行删除
     */
    private Long supplierId;

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * 产地
     */
    private String production;

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }
    /**
     * 图片
     * @OneToMany fetch=eager optional=true
     */
    private List<ProductImg> productImgs;

    /**
     * 点击量
     * @Column
     */
    private Long hits;
    /**
     * 销量
     * @Column
     */
    private Long saleNum;

    private Integer deleted=0;
    /*购买数量  自己添加 表里没有这一字段*/
    private String num;

    private String error; //表里没有  记录导入数据错误信息

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * @ManyToMany joinTable=TB_GOODS_SPEC_VALUE optional=true
     */
    private List<SpecValue> specValues;

    private List<Spec> specList;

    public List<Spec> getSpecList() {
        return specList;
    }

    public void setSpecList(List<Spec> specList) {
        this.specList = specList;
    }

    /**
     * 规格:规格值字符串, 用于 SKU 选择  这里我作为具体规格值的id使用，在加入购物车中
     * @Column
     */
    private String svStr;
    /**
     * 所属商品
     * @ManyToOne
     */
//    @JsonUnwrapped(prefix = "product.")
    private Product product;

    /**
     * 是否默认
     */
    private Boolean isDefault;

    /**
     * 内码
     */
    private String innerCode;

    /**
     * 库存预警
     */
    private Long stockForewarn;

    /**
     * 是否上架
     */
    private Boolean onSell = false;

    /**
     * 上架时间
     * @Column ON_SELL_TIME
     */
    private Date onSellTime;       //新增"上架时间"字段  20160602 zuoyang

    /**
     * 重量
     */
    private Float weight;

    /**
     * 预定数量
     * @Column RESERVE_NUM
     */
    private Integer reserveNum;		//新增“预定数量”字段 20160429 wentan

    /**
     * 预定截止时间
     * @Column RESERVE_DEADLINE
     */
    private Date reserveDeadline;	//新增“预定截止时间”字段 20160429 wentan

    /**
     * 趋向
     * @Column TREND
     * 	0 : 无pr
     * 	1 ： 趋向男性
     * 	2 ： 趋向女性
     */
    private Integer trend;			//新增“趋向”字段 20160620 wentan

    //限购截至时间

    private Date limitedDeadLine;

    public Date getLimitedDeadLine() {
        return limitedDeadLine;
    }

    public void setLimitedDeadLine(Date limitedDeadLine) {
        this.limitedDeadLine = limitedDeadLine;
    }

    public Date getOnSellTime() {
        return onSellTime;
    }

    public void setOnSellTime(Date onSellTime) {
        this.onSellTime = onSellTime;
    }

    public Boolean getOnSell() {
        return onSell;
    }

    public void setOnSell(Boolean onSell) {
        this.onSell = onSell;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getInnerCode() {
        return innerCode;
    }

    public void setInnerCode(String innerCode) {
        this.innerCode = innerCode;
    }

    public Long getStockForewarn() {
        return stockForewarn;
    }

    public void setStockForewarn(Long stockForewarn) {
        this.stockForewarn = stockForewarn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Long getLockedStock() {
        return lockedStock;
    }

    public void setLockedStock(Long lockedStock) {
        this.lockedStock = lockedStock;
    }

    public Long getDamagedStock() {
        return damagedStock;
    }   //品论数

    public void setDamagedStock(Long damagedStock) {
        this.damagedStock = damagedStock;
    }

    public Long getOtherStock() {
        return otherStock;
    }

    public void setOtherStock(Long otherStock) {
        this.otherStock = otherStock;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductImg> getProductImgs() {
        return productImgs;
    }

    public void setProductImgs(List<ProductImg> productImgs) {
        this.productImgs = productImgs;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Long getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Long saleNum) {
        this.saleNum = saleNum;
    }

    public List<SpecValue> getSpecValues() {
        return specValues;
    }

    public void setSpecValues(List<SpecValue> specValues) {
        this.specValues = specValues;
    }

    public String getSvStr() {
        return svStr;
    }

    public void setSvStr(String svStr) {
        this.svStr = svStr;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getReserveNum() {
        return reserveNum;
    }

    public void setReserveNum(Integer reserveNum) {
        this.reserveNum = reserveNum;
    }

    public Date getReserveDeadline() {
        return reserveDeadline;
    }

    public void setReserveDeadline(Date reserveDeadline) {
        this.reserveDeadline = reserveDeadline;
    }

    public Integer getTrend() {
        return trend;
    }

    public void setTrend(Integer trend) {
        this.trend = trend;
    }

}
