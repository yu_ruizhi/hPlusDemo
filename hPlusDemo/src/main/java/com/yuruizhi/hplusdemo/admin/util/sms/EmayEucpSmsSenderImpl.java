package com.yuruizhi.hplusdemo.admin.util.sms;

/**
 * 亿美软通短信平台 - SDK 5.0 (WebService)(电子商务专版) hp2平台.
 *
 * @author vacoor
 */
public class EmayEucpSmsSenderImpl extends EmayEucpSmsSender {
    private String wsUri = "http://hprpt2.eucp.b2m.cn:8080/sdk/SDKService?wsdl";
    private String serialNumber;
    private String serialPassword;
    private String signature;

    @Override
    public String getWsUri() {
        return wsUri;
    }

    public void setWsUri(String wsUri) {
        this.wsUri = wsUri;
    }

    @Override
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public String getSerialPassword() {
        return serialPassword;
    }

    public void setSerialPassword(String serialPassword) {
        this.serialPassword = serialPassword;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
