/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;

import java.util.Date;

/**
 * <p>名称:ProductComment </p>
 * <p>说明: 商品咨询</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 *
 * @author：lushanshan
 * @date：2016年7月5日上午10:28:13
 * @version: 1.0
 */
public class ProductComment extends BaseEntity {

    /**
     * @fieldName: serialVersionUID
     * @fieldType: long
     * @Description: TODO
     */
    private static final long serialVersionUID = 5029669228941954179L;

    /**
     * @Fields memberId : 会员ID
     */
    private long memberId;
    /**
     * @Fields goodsId : 单品ID
     */
    private long goodsId;
    /**
     * @Fields productId : 商品ID
     */
    private long productId;

    /**
     * @Fields content : 咨询内容
     */
    private String content;
    /**
     * @Fields contentTime : 咨询时间
     */
    private Date contentTime;
    /**
     * @Fields reply : 回复内容
     */
    private String reply;
    /**
     * @Fields replyTime : 回复时间
     */
    private Date replyTime;

    /**
     * @Fields deleted : 是否删除
     */
    private boolean deleted;
    /**
     * @Fields goods : 单品实体
     */
    private Goods goods;
    /**
     * @Fields Member : 会员实体
     */
    private Member member;
    /**
     * @Fields Product : 商品实体
     */
    private Product product;
    /**
     * @Fields Product : 是否前台显示
     */
    private Boolean visible=false;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(long goodsId) {
        this.goodsId = goodsId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getContentTime() {
        return contentTime;
    }

    public void setContentTime(Date contentTime) {
        this.contentTime = contentTime;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Date getReplyTime() {
        return replyTime;
    }

    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


}
