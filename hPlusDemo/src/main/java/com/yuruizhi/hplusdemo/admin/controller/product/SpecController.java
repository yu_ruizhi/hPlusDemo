package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Spec;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.product.SpecService;
import com.yuruizhi.hplusdemo.admin.service.product.SpecValueService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.MathUtil;
import com.yuruizhi.hplusdemo.admin.util.ZipUtil;
import com.yuruizhi.hplusdemo.admin.validate.SpecValidate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import org.ponly.common.util.FileUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/spec")
public class SpecController extends BaseController {

    @Autowired
    private SpecService specService;
    @Autowired
    private SpecValueService specValueService;
    @Autowired
    private SysLogService sysLogService;

    @RequestMapping("index")
    public void index() {
    }

    @ResponseBody
    @RequestMapping("check")
    public Boolean check(Long id, String name) {
        Filters filters = Filters.create();

        if (null != id) {
            filters.ne("id", id);
        }
        if (StringUtils.hasText(name)) {
            filters.eq("name", name);
        }
        return specService.count(filters) < 1;
    }

    @ResponseBody
    @RequestMapping("list")
    public Page<Spec> list(Filters filters, Pageable pageable) {
        return specService.findPage(filters, pageable);
    }

    @RequestMapping("add")
    public void add() {
    }

    @RequestMapping("save")
    public String save(Spec spec, RedirectAttributes redirectAttrs) {
        clean(spec);
        boolean ok = specService.saveSpecAndSpecVal(spec);
        if (ok) {
            redirectAttrs.addFlashAttribute("msg", "保存成功");
        }

        // redirectAttrs.addFlashAttribute(SUCCESS_MSG_KEY, "保存成功");
        //系统日志
        if (null != spec) {
            String operation = "规格管理-新增-:" + spec.getName();
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE, operation, "规格管理");
        }

        return redirectViewPath("index");


    }

    @RequestMapping("edit/{id}")
    public String edit(@PathVariable("id") Long id, Map<String, Object> model) {
        Spec spec = specService.find(id);

        List<SpecValue> specValue = specService.findSpecValById(id);
        if (null == spec) {
            return null;
        }
        spec.setSpecValues(specValue);
        model.put("m", spec);
        return getViewPath("edit");
    }

    @RequestMapping("update")
    public String update(Spec spec, RedirectAttributes redirectAttrs) {
        clean(spec);
        Map<String, Object> map = new HashMap<String, Object>();
        if (null != spec.getAlias() && spec.getAlias().trim().length() < 1)
            spec.setAlias(null);
        specService.update(spec);
        List<SpecValue> spcSpecValues = spec.getSpecValues();
        if (null != spcSpecValues) {
            for (SpecValue spv : spcSpecValues) {
                if (spv.getId() == null) {
                    map.put("id", spec.getId());
                    Spec spe = specService.findOne(map);
                    spv.setSpec(spe);
                    specService.saveSpecValue(spv);
                } else {
                    specService.updateSpecValue(spv);
                }
                if (null != spec) {
                    String operation = "规格管理-修改-:" + spec.getName();
                    sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "规格管理");
                }
            }
        }
        return redirectViewPath("index");
    }

    @ResponseBody
    @RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] ids) {

        Map<String, Object> m = Maps.newHashMap();
        m.put("ids", ids);
        String info = specService.getIsExistsSpec(m);
        if (null != info) {
            m.put("error", true);
            m.put("info", info + " 已关联商品，无法删除");
            return m;
        }

        specService.delete(ids);
        String ids1 = "";
        for (int i = 0; i < ids.length; i++) {
            ids1 += ids[i] + ",";
        }
        //系统日志
        ids1 = ids1.substring(0, ids1.length() - 1);
        String operation = "规格管理-删除-id:" + ids1;
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "规格管理");
        return ImmutableMap.<String, Object>of("success", true);
    }

    @ResponseBody
    @RequestMapping("deleteSpecValue/{id}")
    public Map<String, Object> deleteSpecValue(@PathVariable("id") Long id) {
        specValueService.delete(id);
        return ImmutableMap.<String, Object>of("success", true);
    }

    /**
     * 20160429添加， 解决规格编辑页类型切换问题
     * <p>
     * 根据规格ID删除其下的所有规格值
     *
     * @author wentan
     */
    @ResponseBody
    @RequestMapping("deleteSpecValueBySpecId")
    public Map<String, Object> deleteSpecValueBySpecId(Long specId) {
        Map<String, Object> param = Maps.newHashMap();
        Map<String, Object> model = Maps.newHashMap();
        if (null != specId) {
            param.put("specId", specId);
            specValueService.delete(param);
        }
        model.put("success", true);
        return model;
    }

    private void clean(Spec spec) {
        List<SpecValue> values;
        if (spec == null || (values = spec.getSpecValues()) == null) {
            return;
        }
        Iterator<SpecValue> it = values.iterator();
        while (it.hasNext()) {
            SpecValue value = it.next();
            if (value == null || StringUtils.isEmpty(value.getName())) {
                it.remove();
            }
        }
    }

    /**
     * <p>名称：判断规格值编码是否存在</p>
     * <p>描述：</p>
     *
     * @param code
     * @return
     * @author：qinzhongliang
     */
    @ResponseBody
    @RequestMapping("checkIsCodeExist")
    public Map<String, Boolean> checkIsCodeExist(String code) {
        return specValueService.checkIsCodeExist(code);
    }

    /**
     * <p>名称：编辑页-判断规格值编码是否存在</p>
     * <p>描述：编辑页时需要判断已存在的是否就是自身</p>
     *
     * @param id
     * @param code
     * @return
     * @author：qinzhongliang
     */
    @ResponseBody
    @RequestMapping("editCheckIsCodeExist")
    public Map<String, Boolean> editCheckIsCodeExist(Long id, String code) {
        return specValueService.editCheckIsCodeExist(id, code);
    }

    /**
     * <p>名称：下载规格值导入模版</p>
     * <p>描述：</p>
     *
     * @param request
     * @param response
     * @author：wuqi
     */
    @RequestMapping("downLoadModel")
    public void doenLoadModel(HttpServletRequest request, HttpServletResponse response) {
        String[] headers = {"规格名称", "规格别名", "排序", "是否是图片规格", "规格值名称", "规格值编码", "规格值排序"};
        List<String> dataList = new ArrayList<>();
        String data = "颜色(鞋子),颜色(鞋子),1,否,红色,100001,1";
        dataList.add(data);
        data = ",,,,黑色,100002,2";
        dataList.add(data);
        data = ",,,,黑色,100002,3";
        dataList.add(data);
        String fileName = "spec" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
        ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }

    /**
     * <p>名称：商品规格导入</p>
     * <p>描述：根据名称判断，有更新，没有保存，规格值同理</p>
     *
     * @param multipart
     * @param request
     * @return
     * @throws IOException
     * @author：wuqi
     */
    @ResponseBody
    @RequestMapping(value = "/importData", method = RequestMethod.POST)
    public Map<String, Object> importData(@RequestPart("file") MultipartFile multipart,
                                          HttpServletRequest request) throws IOException {
        Map<String, Object> param = Maps.newHashMap();
        InputStream inputStream = multipart.getInputStream();
        List<String> list = ExcelUtil.importExcel(inputStream, 1, 7);
        String error = "";
        if (null != list && 0 < list.size()) {
            Spec spec = new Spec();
            List<SpecValue> specValueList = new ArrayList<>();
            Boolean save = true;
            for (int i = 0; i < list.size(); i++) {
                String data[] = null;
                Boolean success = true;
                SpecValue specValue = new SpecValue();
                Map<String, Object> specValueMap = Maps.newHashMap();
                if (null != list.get(i)) {
                    data = list.get(i).split(",");
                    //名称
                    if (null != data[0] && !"".equals(data[0])) {
                        //规格名称有值时，保存上一条数据，并初始化
                        if (null != spec.getName()) {
                            saveAndUpdateSpec(spec, specValueList, save);
                            specValueList = new ArrayList<>();
                            spec = new Spec();
                            save = true;
                        }
                        //根据名称获取数据
                        List<Spec> specList = specService.findByName(data[0]);
                        if (null != specList && 0 < specList.size()) {
                            spec = specList.get(0);
                            save = false;
                        } else {
                            if (250 < data[0].length()) {
                                error += "第" + (i + 1) + "行数据导入失败，原因为名称超过250个字符<br>";
                                continue;
                            }
                            spec.setName(data[0]);
                        }
                    }

                    //对部分值进行验证
                    Map<String, Object> specMap = SpecValidate.specValidate(spec, data, error, i, success);
                    spec = (Spec) specMap.get("spec");
                    error = (String) specMap.get("error");
                    success = (Boolean) specMap.get("success");
                    //根据是否有id来判断是否需要保存规格值
                    if (null != spec.getId()) {
                        //根据规格值名称，id及code进行判断是否需要保存
                        specValue = specValueService.findBySpecIdAndName(spec.getId(), data[4]);
                        //对规格值进行验证
                        specValueMap = SpecValidate.specValueValidate(specValue, data, error, i, success);
                        specValue = (SpecValue) specValueMap.get("specValue");
                        error = (String) specValueMap.get("error");
                        success = (Boolean) specValueMap.get("success");
                        specValueList.add(specValue);
                    } else {
                        specValueMap = SpecValidate.specValueValidate(specValue, data, error, i, success);
                        specValue = (SpecValue) specValueMap.get("specValue");
                        error = (String) specValueMap.get("error");
                        success = (Boolean) specValueMap.get("success");
                        specValueList.add(specValue);
                    }

                }
            }
            if (null != spec.getName() || null != spec.getSort() || null != spec.getDisplayType()) {
                //当数据为最后一行时，保存更新执行
                saveAndUpdateSpec(spec, specValueList, save);
                specValueList = new ArrayList<>();
                spec = new Spec();
                save = true;
            }
        }
        param.put("error", error);
        return param;
    }

    /**
     * <p>名称：对导入的规格进行保存或者更新</p>
     * <p>描述：</p>
     *
     * @param spec
     * @param specValueList
     * @param save
     * @author：wuqi
     */
    public void saveAndUpdateSpec(Spec spec, List<SpecValue> specValueList, Boolean save) {
        spec.setSpecValues(specValueList);
        //判断保存或者更新
        if (save) {
            specService.saveSpecAndSpecVal(spec);
        } else {
            specService.update(spec);
            List<SpecValue> spcSpecValues = spec.getSpecValues();
            //保存，更新规格值
            if (null != spcSpecValues) {
                for (SpecValue spv : spcSpecValues) {
                    if (null == spv.getId()) {
                        Map<String, Object> map = Maps.newHashMap();
                        map.put("id", spec.getId());
                        Spec spe = specService.findOne(map);
                        spv.setSpec(spe);
                        specService.saveSpecValue(spv);
                    } else {
                        specService.updateSpecValue(spv);
                    }
                }
            }
        }
    }

    /**
     * <p>名称：批量更新品牌图片</p>
     * <p>描述：TODO</p>
     *
     * @return
     * @author：zuoyang
     */
    @RequestMapping("updateImage")
    public String updateImage() {

        return "spec/updateImage";
    }

    /**
     * <p>名称：处理上传的zip图片压缩包并更新数据</p>
     * <p>描述：TODO</p>
     *
     * @param multipart
     * @param request
     * @throws IOException
     * @author：zuoyang
     */
    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> uploadImage(@RequestPart("file") MultipartFile multipart,
                                           HttpServletRequest request) throws IOException {

        InputStream inputStream = multipart.getInputStream();

        String savePath = request.getSession().getServletContext().getRealPath("/WEB-INF/import/iamgeZip");

        String zipFilePath = ZipUtil.copyZipFileStreamToServer(inputStream, savePath);
        //随机解压文件名
        String unzipFolderName = DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
        List<String> imagePathList = ZipUtil.handleZip(zipFilePath, savePath + "/" + unzipFolderName);

        List<String> resultList = new ArrayList<String>();
        Map<String, Object> resultMap = Maps.newHashMap();

        if (null != imagePathList && imagePathList.size() > 0) {
            String filePath = null;
            String specValueIdStr = null;
            SpecValue specValue = null;
            Spec spec = null;
            for (String path : imagePathList) {
                specValueIdStr = FileUtils.getNameWithoutExtension(path);
                if (MathUtil.isLong(specValueIdStr)) {
                    filePath = CSVUtil.CopyFileToServer(path);
                    specValue = specValueService.find(Long.valueOf(specValueIdStr));
                    if (null != specValue) {
                        spec = specService.find(specValue.getSpec().getId());
                    }
                    if (null != specValue && null != spec
                            && new Integer("1").equals(spec.getDisplayType())) {
                        specValue.setImage(filePath);
                        specValue.setLastModifiedDate(new Date());
                        specValueService.update(specValue);
                    } else if (null != specValue && null != spec
                            && new Integer("0").equals(spec.getDisplayType())) {
                        //文字规格不需要上传图片
                        resultList.add(FileUtils.getName(path) + "对应的规格不是图片类型,不需要更新规格值图片");
                    } else {
                        resultList.add(FileUtils.getName(path) + "对应的规格值找不到,请检查此规格值ID是否存在");
                    }
                } else {
                    resultList.add(FileUtils.getName(path) + "命名格式不正确(非数字),请检查");
                }
            }
            resultMap.put("resultList", resultList);
        }

        return resultMap;
    }

}