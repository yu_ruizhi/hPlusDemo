/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsCheck.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.product
 * Date: 2016年4月26日 上午11:33:56 
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.math.BigDecimal;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 商品核价实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月26日 上午11:33:56  
 * @version: 1.0
 */
public class GoodsCheck extends BaseEntity{

	private static final long serialVersionUID = 1L;

	/**@Fields goodsId : 单品ID */ 
	private Long goodsId;
	
	
	private String goodsCode;
	private String goodsTitle;
	private String goodsOnSell;
	private String goodsStatus;
	private String goodsBrand;
	
	
	/**@Fields goods : 单品 */ 
	private Goods goods;

	/**@Fields rate : 汇率 */ 
	private BigDecimal rate;

	/**@Fields rmbCostPrice : RMB成本价 */ 
	private BigDecimal rmbCostPrice;

	/**@Fields logisticPrice : 物流单价 */ 
	private BigDecimal logisticPrice;

	/**@Fields logisticFee : 物流费 */ 
	private BigDecimal logisticFee;

	/**@Fields landedGoodsCost : 到岸成本价 */ 
	private BigDecimal landedGoodsCost;

	/**@Fields sellGrossMargin : 经销毛利率 */ 
	private BigDecimal sellGrossMargin;

	/**@Fields boxPoint : 未成箱加点 */ 
	private BigDecimal boxPoint;

	/**@Fields boxPrice : 成箱价 */ 
	private BigDecimal boxPrice;

	/**@Fields sellPrice : 经销商价 */ 
	private BigDecimal sellPrice;

	/**@Fields retailGrossMargin : 零售毛利率 */ 
	private BigDecimal retailGrossMargin;

	/**@Fields retailPrice : 零售销售价格 */ 
	private BigDecimal retailPrice;

	/**@Fields taobaoGrossMargin : 淘宝毛利率 */ 
	private BigDecimal taobaoGrossMargin;

	/**@Fields taobaoPrice : 淘宝价格 */ 
	private BigDecimal taobaoPrice;

	/**@Fields checked : 是否核价 */ 
	private Boolean checked = false;

	/**@Fields deleted : 是否删除 */ 
	private Boolean deleted = false;
	
	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getGoodsTitle() {
		return goodsTitle;
	}

	public void setGoodsTitle(String goodsTitle) {
		this.goodsTitle = goodsTitle;
	}
	
	public String getGoodsOnSell() {
		return goodsOnSell;
	}

	public void setGoodsOnSell(String goodsOnSell) {
		this.goodsOnSell = goodsOnSell;
	}
	
	public String getGoodsStatus() {
		return goodsStatus;
	}

	public void setGoodsStatus(String goodsStatus) {
		this.goodsStatus = goodsStatus;
	}

	public String getGoodsBrand() {
		return goodsBrand;
	}

	public void setGoodsBrand(String goodsBrand) {
		this.goodsBrand = goodsBrand;
	}

	
	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	public BigDecimal getRmbCostPrice() {
		return rmbCostPrice;
	}

	public void setRmbCostPrice(BigDecimal rmbCostPrice) {
		this.rmbCostPrice = rmbCostPrice;
	}

	public BigDecimal getLogisticFee() {
		return logisticFee;
	}

	public void setLogisticFee(BigDecimal logisticFee) {
		this.logisticFee = logisticFee;
	}

	public BigDecimal getLandedGoodsCost() {
		return landedGoodsCost;
	}

	public void setLandedGoodsCost(BigDecimal landedGoodsCost) {
		this.landedGoodsCost = landedGoodsCost;
	}

	public BigDecimal getSellGrossMargin() {
		return sellGrossMargin;
	}

	public void setSellGrossMargin(BigDecimal sellGrossMargin) {
		this.sellGrossMargin = sellGrossMargin;
	}

	public BigDecimal getBoxPoint() {
		return boxPoint;
	}

	public void setBoxPoint(BigDecimal boxPoint) {
		this.boxPoint = boxPoint;
	}

	public BigDecimal getBoxPrice() {
		return boxPrice;
	}

	public void setBoxPrice(BigDecimal boxPrice) {
		this.boxPrice = boxPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public BigDecimal getRetailGrossMargin() {
		return retailGrossMargin;
	}

	public void setRetailGrossMargin(BigDecimal retailGrossMargin) {
		this.retailGrossMargin = retailGrossMargin;
	}

	public BigDecimal getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(BigDecimal retailPrice) {
		this.retailPrice = retailPrice;
	}

	public BigDecimal getTaobaoGrossMargin() {
		return taobaoGrossMargin;
	}

	public void setTaobaoGrossMargin(BigDecimal taobaoGrossMargin) {
		this.taobaoGrossMargin = taobaoGrossMargin;
	}

	public BigDecimal getTaobaoPrice() {
		return taobaoPrice;
	}

	public void setTaobaoPrice(BigDecimal taobaoPrice) {
		this.taobaoPrice = taobaoPrice;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public BigDecimal getLogisticPrice() {
		return logisticPrice;
	}

	public void setLogisticPrice(BigDecimal logisticPrice) {
		this.logisticPrice = logisticPrice;
	}
	
}
