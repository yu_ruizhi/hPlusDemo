/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MathUtil.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.util
 * Date: 2016年8月11日下午4:28:22
 **/
package com.yuruizhi.hplusdemo.admin.util;

/**
 * <p>名称: 数字处理,判断工具类</p>
 * <p>说明: TODO</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：zuoyang
 * @date：2016年8月12日下午1:44:43   
 * @version: 1.0
 */
public class MathUtil {

	/**
	 * <p>名称：判断字符串是否是Integer整数</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param value
	 * @return
	 */
	 public static boolean isInteger(String value) {
		 if(null == value){
			 return false; 
		 }
		  try {
			  Integer.parseInt(value);
		  } catch (NumberFormatException e) {
			  return false;
		  }
		  
		  return true;
	 }
	 
	 /**
	 * <p>名称：判断字符串是否是Long整数</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param value
	 * @return
	 */
	 public static boolean isLong(String value) {
		 if(null == value){
			 return false; 
		 }
		 
		  try {
			  Long.parseLong(value);
		  } catch (NumberFormatException e) {
			  return false;
		  }
		  
		  return true;
	 }

	 /**
	  * <p>名称：判断字符串是否是浮点数</p> 
	  * <p>描述：TODO</p>
	  * @author：zuoyang
	  * @param value
	  * @return
	  */
	 public static boolean isDouble(String value) {
		 if(null == value){
			 return false; 
		 }
		  try {
			   Double.parseDouble(value);
			   if (value.contains(".")){
				   return true;
			   }
		  } catch (NumberFormatException e) {
		   return false;
		  }
		  
		  return false;
	 }

	 /**
	  * 判断字符串是否是数字
	  */
	 public static boolean isNumber(String value) {
	  return isInteger(value) || isDouble(value);
	 }
}
