package com.yuruizhi.hplusdemo.admin.controller;

import com.google.common.collect.Maps;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.login.AdminLoginService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.CipherUtil;
import com.yuruizhi.hplusdemo.admin.util.GeetestUtil;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

import static com.yuruizhi.hplusdemo.admin.common.Constant.ADMIN_PREFIX;

/**
 * 登录控制器
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(ADMIN_PREFIX + "/login")
public class LoginController extends BaseController {

    @Autowired
    private AdminLoginService adminLoginService;
    @Autowired
    private UserService userService;


    /**
     * 生成极验滑动验证
     */
    @ResponseBody
    @RequestMapping("generateGeetest")
    public void generateGeetest(String t,
                                HttpServletRequest request,
                                HttpServletResponse response) throws IOException {
        GeetestUtil.generateGeetest(request,response);
    }

    /**
     * 执行登录操作
     */
    @RequestMapping("loginSubmit")
    @ResponseBody
    public Map<String,Object> login(String username,
                                     String password,
                                     String geetest_challenge,
                                     String geetest_validate,
                                     String geetest_seccode,
                              HttpServletRequest request,
                              HttpServletResponse response,
                              HttpSession session) throws IOException {
        Map<String, Object> resultMap = new HashedMap();
        if (null == username || null == password || username.trim().length() < 1 || password.trim().length() < 1) {
            resultMap.put("errorMsg", "用户名密码不能为空");
            resultMap.put("result", false);
            return resultMap;
        }
        // FIXME 登录操作放到 这里, 不要返回 Map 会造成接口外调用的不确定性
        Map<String, Object> result = adminLoginService.loginAndTimeUpdate(username,password,geetest_challenge,geetest_validate,geetest_seccode,request,response,session);
        if (!(Boolean) result.get("result")) {
            resultMap.put("errorMsg", result.get("errorMsg"));
            resultMap.put("result", false);
            return resultMap;
        }
        resultMap.put("result", true);
        return resultMap;
    }

    /**
     * 登出控制器
     */
    @RequestMapping("logout")
    public String logout() {
        adminLoginService.logout();
        return "redirect:/" + ADMIN_PREFIX;
    }

    // FIXME url 不要用 getCurrentUser 这样的动词作为 url
    @ResponseBody
    @RequestMapping("getCurrentUserId")
    public User getCurrentUser() {
        return adminLoginService.getCurrentUser();
    }

    @ResponseBody
    @RequestMapping("changePassword")
    public Map<String, Object> changePassword(String origPassword, String newPassword, HttpServletRequest request) {
        Map<String, Object> model = Maps.newHashMap();
        Long userId = UserUtils.getCurrentUserId();
        if (null != userId) {
            Map<String, Object> map = Maps.newHashMap();
            map.put("id", userId);
            User user = userService.find(userId);
            if (!CipherUtil.validatePassword(user.getPassword(), origPassword + user.getCreatedDate().getTime())) {
                model.put("errorMsg", "原始密码不正确！");
                model.put("result", false);
            } else {
                user.setPassword(CipherUtil.generatePassword(newPassword + user.getCreatedDate().getTime()));
                userService.update(user);
                model.put("result", true);
            }
            return model;
        } else {
            model.put("errorMsg", "当前没有用户登录，请登录后尝试");
            return model;
        }
    }

    private String forward(String path) {
        path = path.startsWith("/") ? path : "/" + path;
        return "forward:" + path;
    }

}
