/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.*;

/**
 * <p>名称: SmsLog</p>
 * <p>说明: 短信发送日志实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:07:38   
 * @version: 1.0
 */
public class SmsLog extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 2721235960394483925L;

	/**
     * 短信营销ID
     * 收件人
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=SMS_ID optional=true
     */
    private Sms sms;

    /**
     * 接收人
     * 发送人
     * 
     * @ViewField editor=input 
     * @Column RECEIVER
     */
    @NotNull
    @Size(max = 2000)
    private String receiver;

    /**
     * 短信主题
     * 邮件主题
     * 
     * @ViewField editor=input 
     * @Column SUBJECT
     */
    @NotNull
    @Size(max = 255)
    private String subject;

    /**
     * 短信内容
     * 邮件内容
     * 
     * @ViewField editor=input 
     * @Column CONTENT
     */
    @NotNull
    @Size(max = 500)
    private String content;

    /**
     * 发送时间
     * 
     * @ViewField editor=datepicker 
     * @Column SEND_TIME
     */
    @NotNull
    private Date sendTime;

    /**
     * 发送状态
     * 
     * @ViewField editor=spinner 
     * @Column STATUS
     */
    @NotNull
    private Integer status;

    /**
     * 发送总数
     * 发送批次
     * 
     * @ViewField editor=input 
     * @Column SEND_COUNT
     */
    private Integer sendCount;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Sms getSms() {
        return this.sms;
    }

    public void setSms(Sms sms) {
        this.sms = sms;
    }

    public String getReceiver() {
        return this.receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getSendTime() {
        return this.sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public Integer getSendCount() {
		return sendCount;
	}

	public void setSendCount(Integer sendCount) {
		this.sendCount = sendCount;
	}

}
