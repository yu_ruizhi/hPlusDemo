/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: fengqi-admin 
 * FileName: CSVUtil.java 
 * PackageName: com.glanway.fengqi.admin.util 
 * Date: 2016年8月2日下午2:33:55
 **/
package com.yuruizhi.hplusdemo.admin.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.ponly.common.codec.Hex;
import org.ponly.common.util.Bytes;
import org.ponly.common.util.FileUtils;
import org.ponly.common.util.Randoms;
import org.ponly.fs.FileSystem;
import org.springframework.util.StringUtils;
/**
 * <p>名称: csv 工具类</p>
 * <p>说明: TODO</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：zuoyang
 * @date：2016年8月2日下午2:35:06   
 * @version: 1.0
 */
public class CSVUtil {

	private static final Logger logger = Logger.getLogger(CSVUtil.class);
	private static final byte COMMON_CSV_HEAD[] = { (byte) 0xEF, (byte) 0xBB,(byte) 0xBF };
	private static final String DATE_DIR_FMT = "%1$tY/%1$tm/%1$td/%2$04d";
	private static final int DEFAULT_FOLDERS_OF_DAY = 9;
	
	private static FileSystem fileSystem;
	
	public FileSystem getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(FileSystem fileSystem) {
		CSVUtil.fileSystem = fileSystem;
	}

	/**
	 * <p>名称：导出</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
     * @param file csv文件(路径+文件名)，csv文件不存在会自动创建
     * @param dataList 数据
	 * @return
	 */
    public static boolean exportCsv(File file, List<String> dataList){
        boolean isSucess=false;
        
        FileOutputStream out=null;
        OutputStreamWriter osw=null;
        BufferedWriter bw=null;
        try {
            out = new FileOutputStream(file);
            osw = new OutputStreamWriter(out);
            bw =new BufferedWriter(osw);
            if(dataList!=null && !dataList.isEmpty()){
                for(String data : dataList){
                    bw.append(data).append("\r");
                }
            }
            isSucess=true;
        } catch (Exception e) {
            isSucess=false;
        }finally{
            if(bw != null){
                try {
                    bw.close();
                    bw = null;
                } catch (IOException e) {
                	logger.debug("ExportReport IOException:"+e);
                } 
            }
            if(osw != null){
                try {
                    osw.close();
                    osw = null;
                } catch (IOException e) {
                	logger.debug("ExportReport IOException:"+e);
                } 
            }
            if(out != null){
                try {
                    out.close();
                    out = null;
                } catch (IOException e) {
                	logger.debug("ExportReport IOException:"+e);
                } 
            }
        }
        
        return isSucess;
    }
    
    /**
	 * <p>名称：调用浏览器下载器导出csv文件</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
     * @param headers 
     * @param dataList 
     * @param response 
     * @param fileName
	 */
    public static void exportCsv(String[] headers, List<String> dataList,HttpServletResponse response,String fileName){
        File file = null ;
        FileOutputStream out=null;
        OutputStreamWriter osw=null;
        BufferedWriter bw=null;
        try {
        	file = File.createTempFile("csv-temp", ".temp");
            out = new FileOutputStream(file);
            osw = new OutputStreamWriter(out,"GBK");

            logger.debug("OutputStream writer Encoding:" + osw.getEncoding());
            bw =new BufferedWriter(osw);
            
            //写入表头部分
            if(null != headers){
            	for(String headerStr : headers){
                    bw.append(headerStr).append(",");
                }
            	bw.append("\r");
            }
            //写入数据部分
            if(null != dataList
            		&& !dataList.isEmpty()){
                for(String data : dataList){
                	data = StringUtils.replace(data, ",null,", ",,");//替换null为空
                    bw.append(data).append("\t\r");
                }
            }
            
        } catch (Exception e) {
        	logger.debug("ExportReport IOException:"+e);
        }finally{
            if(bw != null){
                try {
                    bw.close();
                    bw = null;
                } catch (IOException e) {
                	logger.debug("ExportReport IOException:"+e);
                } 
            }
            if(osw != null){
                try {
                    osw.close();
                    osw = null;
                } catch (IOException e) {
                	logger.debug("ExportReport IOException:"+e);
                } 
            }
            if(out != null){
                try {
                    out.close();
                    out = null;
                } catch (IOException e) {
                	logger.debug("ExportReport IOException:"+e);
                } 
            }
        }
        
        OutputStream toClient = null;
        BufferedReader fis = null;
		try {
			response.reset();     
			response.setContentType("application/octet-stream;charset=UTF-8");
			response.setCharacterEncoding("GBK");
			response.setHeader("Content-Disposition", "attachment; filename="+fileName);
			
			toClient = new BufferedOutputStream(response.getOutputStream());
			FileInputStream is=new FileInputStream(file);
			InputStreamReader isr=new InputStreamReader(is,"GBK");
			logger.debug("ExportReport reader Encoding:"+isr.getEncoding());
            fis = new BufferedReader(isr);
            //给将要输出的内容加上BOM标识(UTF-8),这样的话用excel打开csv时就不会出现中文乱码了
            toClient.write(COMMON_CSV_HEAD);
	   		IOUtils.copy(fis, toClient,"GBK");
	   		toClient.flush();
		} catch (FileNotFoundException e) {
			logger.debug("ExportReport FileNotFoundException:"+e);
		} catch (IOException e) {
        	logger.debug("ExportReport IOException:"+e);
        }finally {
            //关闭流
            IOUtils.closeQuietly(fis);
            IOUtils.closeQuietly(toClient);
            if(null != file && file.exists()){
            	file.delete();
            }
        }
        
    }
    
    /**
	 * <p>名称：读取csv,返回string list</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param input 
	 */
   public static List<String> importCsv(InputStream input){
	   List<String> dataList=new ArrayList<String>();
       
       BufferedReader br=null;
       try { 
    	   InputStreamReader isr = new InputStreamReader(input);
           br = new BufferedReader(isr);
           String line = ""; 
           while ((line = br.readLine()) != null) { 
               dataList.add(line);
           }
       }catch (Exception e) {
    	   logger.debug("importCsv Exception:"+e);
       }finally{
           if(br!=null){
               try {
                   br.close();
                   br=null;
               } catch (IOException e) {
            	   logger.debug("importCsv IOException:"+e);
               }
           }
       }

       return dataList;
   }
   
   /**
    * <p>名称：将读取的文件上传到服务器</p> 
    * <p>描述：TODO</p>
    * @author：zuoyang
    * @param filePath
    * @return
    */
   public static String CopyFileToServer(String filePath){
	   File file = new File(filePath);
	   InputStream input = null ;
	   try {
			 input = null != file && file.exists()? new FileInputStream(file) : null;
		} catch (FileNotFoundException e) {
			 logger.debug("CopyFileToServer FileNotFoundException:"+e);
			return null;
		}
	   //文件不存在时返回null
	   if(null == input){
		    
		   logger.debug("input stream is null");
			return null;  
	   }
	   
   
	   String fileName = "img/"+randomFolder()+"/"+generateFilename()+FileUtils.getExtension(filePath,true);
	
	   OutputStream out = null != fileSystem ? fileSystem.create(fileName, true) : null;
	   try {
			IOUtils.copy(input, out);
			out.flush();
		} catch (IOException e) {
			logger.debug("CopyFileToServer IOException:"+e);
			return null;
		}finally {
            //关闭流
            IOUtils.closeQuietly(input);
            IOUtils.closeQuietly(out);
        }
	   
	   return fileName;
   
   }
   
   private static String randomFolder() {
       Date date = new Date();
       int random = (int) (date.getTime() % DEFAULT_FOLDERS_OF_DAY);
       return String.format(DATE_DIR_FMT, date, random);
   }
   
   private static String generateFilename(){
       return Hex.encode(Bytes.toBytes((new Date()).getTime() + Randoms.next(0L, 9223372036854775807L)));
   }
    
}
