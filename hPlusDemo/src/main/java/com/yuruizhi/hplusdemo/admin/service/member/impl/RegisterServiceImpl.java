/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.member.impl;

import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.member.RegisterService;

/**
 * <p>名称: RegisterServiceImpl</p>
 * <p>说明: 后台注册服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:24:53   
 * @version: 1.0
 */
@Service
public class RegisterServiceImpl extends BaseServiceImpl<Member> implements RegisterService {

}
