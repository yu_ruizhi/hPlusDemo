/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import java.util.Map;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.cms.Channel;
import com.yuruizhi.hplusdemo.admin.service.TreeNodeBaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.ChannelService;

/**
 * <p>名称: ChannelServiceImpl</p>
 * <p>说明: 栏目服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:00:50   
 * @version: 1.0
 */
@Service
public class ChannelServiceImpl extends TreeNodeBaseServiceImpl<Channel> implements ChannelService {
	
	private static final int DEFAULT_MAX_DEPTH = 999;
	
	protected ChannelServiceImpl() {
		super(null);
	}
	
	
	@Override
	protected Channel doMountNode(Channel node) {
		// 执行子节点挂到父节点操作时, 设置当前节点允许的最大层级是父节点 - 1
		node = super.doMountNode(node);
		Channel parent = node.getParent();
		Integer depth = null != parent ? parent.getMaxDepth() : DEFAULT_MAX_DEPTH;
		node.setMaxDepth(depth - 1);
		return node;
	}

	@Override
	protected Channel createDefaultRootNode() {
		Channel root = super.createDefaultRootNode();
		// 设置默认节点最大层级
		root.setMaxDepth(9999);
		return root;
	}

	@Override
	protected Map<String, Object> buildDescendantUpdateParams(Channel snapshot, Channel current) {
		Map<String, Object> params = super.buildDescendantUpdateParams(snapshot, current);

		Integer oldMaxDepth = snapshot.getMaxDepth();
		Integer newMaxDepth = current.getMaxDepth();
		oldMaxDepth = null != oldMaxDepth ? oldMaxDepth : DEFAULT_MAX_DEPTH;
		newMaxDepth = null != newMaxDepth ? newMaxDepth : DEFAULT_MAX_DEPTH;

		params.put("_maxDepthIncrement", newMaxDepth - oldMaxDepth);

		return params;
	}
	
	
	
	/*@Autowired
	private ChannelDao channelDao;
	
	@Override
	public void update(Channel channel) {
		User user = getCurrentAdminUserIfHas(); //获取当前登录管理员用户
		channel.setLastModifiedBy(user.getLoginName());
		channel.setLastModifiedDate(new Date());
		
		if(null!=channel.getParent()){
			Channel newParent  = channelDao.find(channel.getParent().getId());
			Channel oldChannel = channelDao.find(channel.getId());
			Channel oldParent = oldChannel.getParent();
			String oldChannelPath = oldChannel.getPath();
			String newChannelPath = newParent.getPath()+","+channel.getId();
			
			channelDao.update(channel);
			newParent.setIsLeaf(false);
			channelDao.update(newParent);
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("oldBegin", oldChannelPath);
			params.put("newBegin", newChannelPath);
			channelDao.updatePath(params);
			
			if(oldParent!=null){  //当父节点不为空是更新父节点isleaf字段；为空时就不需要更新了
				params.clear();
				params.put("path",oldParent.getPath()+",");
				int childCount = channelDao.childCount(params);
				if(childCount<1){
					oldChannel.setIsLeaf(true);
					channelDao.update(oldParent);
				}
			}
			
			
			
			int depthChange = newParent.getDepth()-channel.getDepth();
			params.clear();
			params.put("depthChange", depthChange);
			params.put("newBegin", newChannelPath);
			channelDao.updateDepth(params);
			
		}else{
			super.update(channel);
		}
	}
	
	
	@Override
	public void save(Channel channel) {
		User user = getCurrentAdminUserIfHas();
		channel.setCreatedBy(user.getLoginName());
		channel.setCreatedDate(new Date());
		channel.setIsLeaf(true);
		
		Channel parent = channel.getParent();
		
		if(null!=parent){    //存在父栏目
			parent = channelDao.find(parent.getId());
			channelDao.save(channel);
			channel.setPath(parent.getPath()+","+channel.getId());
			channel.setDepth(parent.getDepth()+1);
			parent.setIsLeaf(false);
			channelDao.update(channel);
			channelDao.update(parent);
		}else{   //不存在父栏目
			
			channel.setPath(","+channel.getId());
			channel.setDepth(0);
			channelDao.save(channel);
			channel.setPath(","+channel.getId());
			channelDao.update(channel);
		}
		
	}*/
}
