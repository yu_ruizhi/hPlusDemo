package com.yuruizhi.hplusdemo.admin.service.product;

import com.yuruizhi.hplusdemo.admin.entity.product.ProductComment;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by Administrator on 2016/7/5 0005.
 */
public interface ProductCommentService extends BaseService<ProductComment> {

}
