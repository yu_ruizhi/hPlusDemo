/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.ticket;


import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 票务表
 */
public class TicketCount extends BaseEntity {

    private String qrcode;

    private Integer type;//1未使用，2正在使用，3使用完毕',

    private Ticket ticket;

    private TicketOrder ticketOrder;

    private TicketOrderDetail ticketOrderDetail;

    private Boolean deleted;

    private TicketRefund ticketRefund;

    public TicketRefund getTicketRefund() {
        return ticketRefund;
    }

    public void setTicketRefund(TicketRefund ticketRefund) {
        this.ticketRefund = ticketRefund;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public TicketOrder getTicketOrder() {
        return ticketOrder;
    }

    public void setTicketOrder(TicketOrder ticketOrder) {
        this.ticketOrder = ticketOrder;
    }

    public TicketOrderDetail getTicketOrderDetail() {
        return ticketOrderDetail;
    }

    public void setTicketOrderDetail(TicketOrderDetail ticketOrderDetail) {
        this.ticketOrderDetail = ticketOrderDetail;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
