package com.yuruizhi.hplusdemo.admin.service.product.impl;

import com.yuruizhi.hplusdemo.admin.dao.product.ProductCommentDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductComment;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.ProductCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2016/7/5 0005.
 */
@Service
public class ProductCommentServiceImpl extends BaseServiceImpl<ProductComment> implements ProductCommentService {
	@Autowired
    private ProductCommentDao productCommentDao;
    @Autowired
    public void setProductCommentDao(ProductCommentDao productCommentDao) {
        super.setBaseDao(productCommentDao);
        this.productCommentDao = productCommentDao;
    }
	


}
