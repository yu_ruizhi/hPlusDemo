package com.yuruizhi.hplusdemo.admin.dto;

import java.math.BigDecimal;

/**
 */
public class SupplierGoodsDTO {
    private String id;  // 供应商-货品 关系id
    private String goodsId; // 货品id
    private String goodsCode; //isbn编号S
    private String goodsName;  //商品名称
    private String supplierId;          // 供应商id
    private String supplierName;          // 供应商名称
    private String publisher;          // 出版社
    private BigDecimal unitPrice;  //单价
    private BigDecimal discount;   //折扣

    private String categoryId;
    private String categoryName;
    private String supplierGoodsCode;   // 供应商货品code
    private Double supplierGoodsPrice;          // 供应商建议价格

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSupplierGoodsCode() {
        return supplierGoodsCode;
    }

    public void setSupplierGoodsCode(String supplierGoodsCode) {
        this.supplierGoodsCode = supplierGoodsCode;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public Double getSupplierGoodsPrice() {
        return supplierGoodsPrice;
    }

    public void setSupplierGoodsPrice(Double supplierGoodsPrice) {
        this.supplierGoodsPrice = supplierGoodsPrice;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }
}
