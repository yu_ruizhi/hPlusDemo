/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageMemberServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月20日下午2:28:07
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.marketing.MessageMemberDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageMember;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageMemberService;

/**
 * 
 * <p>名称: 私信-会员中间Service实现</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日下午2:28:38
 * @version: 1.0
 */
@Service

public class MessageMemberServiceImpl extends BaseServiceImpl<MessageMember> implements MessageMemberService{
	@Autowired
	private MessageMemberDao messageMemberDao;

	@Override
	public MessageMember findByMemberIdMessageId(MessageMember messageMember) {
		return messageMemberDao.findByMemberIddMessageId(messageMember);
	}
	

}
