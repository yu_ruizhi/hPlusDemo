/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: RecommendWorksController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月21日 下午1:37:32 
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.RecommendWorks;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;
import com.yuruizhi.hplusdemo.admin.service.marketing.RecommendWorksService;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import com.yuruizhi.hplusdemo.admin.service.product.WorksService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 推荐作品 Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月21日 下午1:37:32 
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "recommendWorks")
public class RecommendWorksController extends AdminBaseController<RecommendWorks> {

	@Autowired
	private RecommendWorksService recommendWorksService;
	
	@Autowired
	private WorksService worksService;
	
	@Autowired
	private CategoryService categoryService;
	
	/**
	 * 列表页异步获取数据
	 */
	/**
	 * 
	 * <p>名称：列表页异步获取数据</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param recommendWorksFilters
	 * @param worksFilters
	 * @param pageable
	 * @return
	 */
	@ResponseBody
	@RequestMapping("list2")
	public Page<RecommendWorks> list2(@Qualifier("R.")Filters recommendWorksFilters,
										@Qualifier("W.")Filters worksFilters,
										Pageable pageable) {
		return recommendWorksService.findPage(recommendWorksFilters, worksFilters, pageable);
	}

	/**
	 * 
	 * <p>名称：编辑页异步获取关联的作品</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param worksId
	 * @return
	 */
	@RequestMapping("findWorksById")
	@ResponseBody
	public Map<String, Object> findWorksById(Long worksId) {
		Map<String, Object> model = Maps.newHashMap();
		if(null != worksId){
			Works works = worksService.find(worksId);
			model.put("works", works);
			model.put("success", true);
			return model;
		}
		model.put("success", false);
		return model;
	}
	
	/**
	 * 重写edit方法
	 */
	@Override
	@RequestMapping({"edit/{id}"})
	public String _compat(@PathVariable("id")Long id, Map<String, Object> model) {
		RecommendWorks recommendWorks = recommendWorksService.find(id);
		List<Category> categoryList = categoryService.findAll();
		model.put("recommendWorks", recommendWorks);
		model.put("categoryList", categoryList);
		return getRelativeViewPath("edit");
	}
	
	/**
	 * 重写add方法
	 */
	@Override
	public void input(RecommendWorks e, Map<String, Object> model) {
		List<Category> categoryList = categoryService.findAll();
		model.put("categoryList", categoryList);
	}
}
