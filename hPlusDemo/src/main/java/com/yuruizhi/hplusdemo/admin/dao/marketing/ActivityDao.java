/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ActivityDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月21日 下午2:14:53 
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Activity;

/**
 * 
 * <p>名称: 活动管理Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午2:14:53   
 * @version: 1.0
 */
public interface ActivityDao extends BaseDao<Activity> {
	
	/**
	 * 
	 * <p>名称：根据活动名称获取活动信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name 传入活动名称
	 * @return 活动信息数据
	 */
	List<Activity> findByName(String name);
	
}
