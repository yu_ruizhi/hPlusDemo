/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月20日 上午11:35:40
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Message;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageMember;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageMemberService;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;

import static com.yuruizhi.hplusdemo.admin.controller.ControllerSupport.ERROR_MSG_KEY;
import static com.yuruizhi.hplusdemo.admin.controller.ControllerSupport.SUCCESS_MSG_KEY;

/**
 * 
 * <p>名称: 私信Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * <p>修改记录：（20160705 - yuruizhi - 增加私信发送成功提示语）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日 上午11:36:06
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/message")
public class MessageController extends AdminBaseController<Message> {
	
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private MessageMemberService messageMemberService;
	
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * 
	 * <p>名称：跳转到发送私信页面</p> 
	 * <p>描述：跳转时传入私信列表</p>
	 * @author：qinzhongliang
	 * @param model 传到页面的数据
	 * @return 跳转路径
	 */
	@RequestMapping("send")
	public String send(Map<String, Object> model) {
		List<Message> messageList = messageService.findAll();
		model.put("messageList", messageList);
		return getRelativeViewPath("send");
	}
	
	/**
	 * 
	 * <p>名称：新增页-保存私信</p> 
	 * <p>描述：默认为电商私信，且为未读状态</p>
	 * @author：qinzhongliang
	 * @param message 保存页面传入的数据，并封装为对象
	 * @return 跳转路径
	 */
	@RequestMapping("messageSave")
	public String messageSave(Message message) {
		//默认为未读状态
		message.setState(Constant.MESSAGE_STATE_UNREAD);
		//电商私信
		message.setType(Constant.MESSAGE_TYPE_ESHOP);
		messageService.save(message);
		if(null != message){
			sysLogService.saveSysLog(Constant.SYSLOG_SAVE, 
					"私信-新增-ID:" + message.getId() + ",标题:" 
					+ message.getTitle() + "的私信记录", "私信管理");
		}
		return redirectViewPath("index");
	}
	
	/**
	 * <p>名称：私信更新</p> 
	 * <p>描述：记录到日志</p>
	 * @author：qinzhongliang
	 */
	@Override
	public String update(Message message, BindingResult binding, RedirectAttributes redirectAttrs) {
		if(null != message) {
			sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, 
					"私信-修改-ID:" + message.getId() + "标题:" + message.getTitle(), "私信管理");
		}
		return super.update(message, binding, redirectAttrs);
	}
	
	/**
	 * 
	 * <p>名称：发送私信</p> 
	 * <p>描述：需要判断是否已经发送过，若已发送则不保存</p>
	 * @author：qinzhongliang
	 * @param message 保存页面传入的数据，并封装为对象
	 * @return 跳转路径
	 */
	/*@RequestMapping("messageSend")
	public String messageSend(Message message,RedirectAttributes redirectAttributes) {
		List<Member> memberList = message.getMemberList();
		MessageMember messageMember = new MessageMember();
		for(Member member : memberList) {
			messageMember.setMessageId(message.getId());
			messageMember.setMemberId(member.getId());
			//判断是否已经发送过，若发过则不保存
			//if(null == messageMemberService.findByMemberIdMessageId(messageMember)) {
				messageMember.setState(Constant.MESSAGE_STATE_UNREAD);
				messageMemberService.save(messageMember);
				sysLogService.saveSysLog(Constant.SYSLOG_SAVE, 
						"私信-新增-向会员ID:" + member.getId() + "发送了私信ID:"
						+ message.getId() + "的私信", "私信管理");
			//}
		}
		redirectAttributes.addFlashAttribute(SUCCESS_MSG_KEY, "发送成功");
		return redirectViewPath("index");
	}*/
	
	@Override
	public Map<String, Object> delete(Long[] id) {
		String idStr = "";
		for(int i=0;i<id.length;i++) {
			idStr += id[i] + ",";
		}
		idStr = idStr.substring(0, idStr.length()-1);
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "私信-删除-ID:"+idStr, "私信管理");
		return super.delete(id);
	}
	
	/**
	 * 
	 * <p>名称：列表页-显示</p> 
	 * <p>描述：列表页默认展示电商私信</p>
	 * @author：qinzhongliang
	 * @param filters 过滤器
	 * @param pageable 分页
	 * @return
	 */
	@RequestMapping("eshopList")
	@ResponseBody
	public Page<Message> eshopList(Filters filters, Pageable pageable) {
		filters.add("type", Filters.Operator.EQ, Constant.MESSAGE_TYPE_ESHOP);
		return messageService.findPage(filters, pageable);
	}
}
