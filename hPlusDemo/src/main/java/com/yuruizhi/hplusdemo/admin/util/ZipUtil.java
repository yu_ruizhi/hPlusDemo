/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ZipUtil.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.util
 * Date: 2016年8月11日下午4:28:22
 **/
package com.yuruizhi.hplusdemo.admin.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipEntry;

/**
 * <p>名称: zip 工具类</p>
 * <p>说明: TODO</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：zuoyang
 * @date：2016年8月11日下午2:35:06   
 * @version: 1.0
 */
public class ZipUtil {

	private static final Logger logger = Logger.getLogger(ZipUtil.class);
	
	/**
	 * <p>名称：递归压缩文件夹 </p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param srcRootDir 压缩文件夹根目录的子路径 
	 * @param file   当前递归压缩的文件或目录对象 
	 * @param zos    压缩文件存储对象 
	 * @throws Exception
	 */
    private static void zip(String srcRootDir, File file, ZipOutputStream zos) throws Exception{  
        if (file == null){  
            return;  
        }                 
          
        //如果是文件，则直接压缩该文件  
        if (file.isFile()) {             
            int count, bufferLen = 1024;  
            byte data[] = new byte[bufferLen];  
              
            //获取文件相对于压缩文件夹根目录的子路径  
            String subPath = file.getAbsolutePath();  
            int index = subPath.indexOf(srcRootDir);  
            if (index != -1){  
                subPath = subPath.substring(srcRootDir.length() + File.separator.length());  
            }  
            ZipEntry entry = new ZipEntry(subPath);  
            zos.putNextEntry(entry);  
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));  
            while ((count = bis.read(data, 0, bufferLen)) != -1){  
                zos.write(data, 0, count);  
            }  
            bis.close();  
            zos.closeEntry();  
        }else {   //如果是目录，则压缩整个目录  
            //压缩目录中的文件或子目录  
            File[] childFileList = file.listFiles();  
            for (int n=0; n<childFileList.length; n++) {  
                childFileList[n].getAbsolutePath().indexOf(file.getAbsolutePath());  
                zip(srcRootDir, childFileList[n], zos);  
            }  
        }  
    }  
      
    /**
     * <p>名称：对文件或文件目录进行压缩 </p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param srcPath 要压缩的源文件路径。如果压缩一个文件，则为该文件的全路径；如果压缩一个目录，则为该目录的顶层目录路径 
     * @param zipPath 压缩文件保存的路径。注意：zipPath不能是srcPath路径下的子文件夹 
     * @param zipFileName 压缩文件名 
     * @return
     * @throws Exception
     */
    public static boolean zip(String srcPath, String zipPath, String zipFileName) throws Exception{  
        if (StringUtils.isEmpty(srcPath) 
        		|| StringUtils.isEmpty(zipPath) 
        		|| StringUtils.isEmpty(zipFileName)){  
        	logger.debug("srcPath or zipPath or zipFileName not empty"); 
        	return false; 
        }  
        CheckedOutputStream cos = null;  
        ZipOutputStream zos = null;                       
        try {  
            File srcFile = new File(srcPath);  
              
            //判断压缩文件保存的路径是否为源文件路径的子文件夹，如果是，则抛出异常（防止无限递归压缩的发生）  
            if (srcFile.isDirectory() && zipPath.indexOf(srcPath)!=-1){  
            	logger.debug("zipPath must not be the child directory of srcPath.");
                return false; 
            }  
            //判断压缩文件保存的路径是否存在，如果不存在，则创建目录  
            File zipDir = new File(zipPath);  
            if (!zipDir.exists() || !zipDir.isDirectory()){  
                zipDir.mkdirs();  
            }  
              
            //创建压缩文件保存的文件对象  
            String zipFilePath = zipPath + File.separator + zipFileName;  
            File zipFile = new File(zipFilePath);             
            if (zipFile.exists()){  
                //检测文件是否允许删除，如果不允许删除，将会抛出SecurityException  
                SecurityManager securityManager = new SecurityManager();  
                securityManager.checkDelete(zipFilePath);  
                //删除已存在的目标文件  
                zipFile.delete();                 
            }  
              
            cos = new CheckedOutputStream(new FileOutputStream(zipFile), new CRC32());  
            zos = new ZipOutputStream(cos);  
              
            //如果只是压缩一个文件，则需要截取该文件的父目录  
            String srcRootDir = srcPath;  
            if (srcFile.isFile()){  
                int index = srcPath.lastIndexOf(File.separator);  
                if (index != -1){  
                    srcRootDir = srcPath.substring(0, index);  
                }  
            }  
            //调用递归压缩方法进行目录或文件压缩  
            zip(srcRootDir, srcFile, zos);  
            zos.flush();  
        }catch (Exception e) {  
        	logger.debug("zip Exception:"+e); 
        	 return false; 
        }finally {             
            try{  
                if (zos != null){  
                    zos.close();  
                }                 
            } catch (Exception e){  
            	logger.debug("zip close Exception:"+e); 
            }             
        } 
        
        return true; 
    }  
      
    /**
     * <p>名称：解压缩zip包 </p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param zipFilePath  zip文件的全路径
     * @param unzipFilePath 解压后的文件保存的路径 
     * @return
     * @throws Exception
     */
    @SuppressWarnings({"rawtypes" })
	public static boolean unzip(String zipFilePath, String unzipFilePath) {  
        if (StringUtils.isEmpty(zipFilePath) 
        		|| StringUtils.isEmpty(unzipFilePath)){  
        	logger.debug("zipFilePath or unzipFilePath not empty"); 
        	return false;           
        }
        File pathFile = new File(unzipFilePath);  
        if(!pathFile.exists()){  
            pathFile.mkdirs();  
        }  
        ZipFile zip = null;
        ZipEntry entry = null ;
		String zipEntryName = null;
		InputStream in = null;
		String outPath = null;
		OutputStream out = null;
		try {
			zip = new ZipFile(zipFilePath);
			for(Enumeration entries = zip.getEntries();entries.hasMoreElements();){  
	        	entry = (ZipEntry)entries.nextElement();  
	            zipEntryName = entry.getName();  
	            in = zip.getInputStream(entry);  
	            outPath = (unzipFilePath+"/"+ zipEntryName).replaceAll("\\*", "/");;  
	            //判断路径是否存在,不存在则创建文件路径  
	            File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));  
	            if(!file.exists()){  
	                file.mkdirs();  
	            }  
	            //判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压  
	            if(new File(outPath).isDirectory()){  
	                continue;  
	            }  
	            //输出文件路径信息  
	            logger.debug(outPath);  
	              
	            out = new FileOutputStream(outPath);  
	            byte[] buf1 = new byte[1024];  
	            int len;  
	            while((len=in.read(buf1))>0){  
	                out.write(buf1,0,len);  
	            }  
            } 
		} catch (IOException e) {
			logger.debug("unzip IOException:"+e); 
		}finally{
			try {
				if(null != in){
					in.close();	
				}
			} catch (IOException e) {
				logger.debug("unzip close stream IOException:"+e); 
			}  
			try {
				if(null != out){
					out.close();	
				}
			} catch (IOException e) {
				logger.debug("unzip close stream IOException:"+e); 
			}  
			//解压后删除压缩包
			new File(zipFilePath).delete();
		}  
         
        
        return true;       
    }  
    
    /**
     * <p>名称：获取文件夹里的所有文件路径</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param filePath
     * @return
     * @throws Exception
     */
    public static List<String> getImagePath(String filePath,List<String> imgPathList) throws Exception{

    	File unzipFolder = new File(filePath);
    	File[] imageFileList = unzipFolder.listFiles();
    	for (File file: imageFileList) {
    		if(file.isFile()){
    			imgPathList.add(file.getAbsolutePath());
    		}else if(file.isDirectory()){
    			getImagePath(file.getAbsolutePath(),imgPathList);
    		}
    	}
    	
    	return imgPathList;
    }
    
    /**
     * <p>名称：将上传的zip文件流保存到服务器当前项目下的目录</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param input
     * @return
     * @throws Exception
     */
    public static String copyZipFileStreamToServer(InputStream  input,String targetFolder){
    	
    	 File path = new File(targetFolder);
         if (!path.exists()) {
             path.mkdirs();
         }
         //随机解压文件名
     	 String zipFileName = DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS)+".zip";
         File file = new File(path,zipFileName);
         OutputStream  out = null;
         try {
        	 out = new FileOutputStream(file);
             IOUtils.copy(input,out);
         } catch (IOException ioe) {
             file.delete();
             logger.debug("copyZipFileStreamToServer IOException: "+ioe);
             return null;
         }finally{
        	 IOUtils.closeQuietly(out);
        	 IOUtils.closeQuietly(input);
         }
    	
    	return null != file ? file.getAbsolutePath() : null;
    }
    
    /**
     * <p>名称：将上传的zip文件解压,并返回所有图片路径</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param input
     * @return
     * @throws Exception
     */
    public static List<String> handleZip(String zipFilePath,String unzipFilePath){
    	List<String> imgPathList = new ArrayList<String>();
    	try {
			unzip(zipFilePath, unzipFilePath);
			
			return getImagePath(unzipFilePath,imgPathList);
		} catch (Exception e) {
			logger.debug("handleZip Exception: "+e);
			return null ;
		}

    }
      
}
