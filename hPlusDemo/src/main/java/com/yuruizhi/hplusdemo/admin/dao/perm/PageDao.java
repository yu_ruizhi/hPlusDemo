package com.yuruizhi.hplusdemo.admin.dao.perm;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;

/**
 * Created by ASUS on 2014/12/22.
 */
public interface PageDao extends BaseDao<AdminPage> {
    String PAGE_FILTERS_PROP = "_page_filters";
    String MODULE_FILTERS_PROP = "_module_filters";
    List<AdminPage> getBasePage(Map<String, Object> paramMap);
}
