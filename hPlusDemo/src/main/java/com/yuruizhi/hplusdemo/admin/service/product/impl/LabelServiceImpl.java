/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.LabelDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Label;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.LabelService;

/**
 * <p>名称: LabelServiceImpl</p>
 * <p>说明: 标签服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:53:47   
 * @version: 1.0
 */
@Service
public class LabelServiceImpl extends BaseServiceImpl<Label> implements LabelService{
	@Autowired
	private LabelDao labelDao;
	@Autowired
	public void setLabelDao(LabelDao labelDao) {
		super.setBaseDao(labelDao);
		this.labelDao = labelDao;
	}

	@Override
	public List<Label> findManyByProductId(Long id) {
		return labelDao.findManyByProductId(id);
	}

	@Override
	public List<Label> findByName(String name) {
		return labelDao.findByName(name);
	}

}
