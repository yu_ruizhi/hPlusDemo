/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms;

import com.yuruizhi.hplusdemo.admin.entity.cms.AdBlock;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: AdBlockService</p>
 * <p>说明: 广告位服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:03:37   
 * @version: 1.0
 */
public interface AdBlockService extends BaseService<AdBlock> {
}
