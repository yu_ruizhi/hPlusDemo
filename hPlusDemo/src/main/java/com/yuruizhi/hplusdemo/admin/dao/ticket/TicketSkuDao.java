/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProvinceService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area
 * Date: 2016年4月18日下午7:26:58
 **/
package com.yuruizhi.hplusdemo.admin.dao.ticket;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketSku;



/**
 * 
 * <p>名称: 票务</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wangchen
 * @date：2016年12月12日下午14:07:00
 * @version: 1.0
 * @param <E>
 */
public interface TicketSkuDao extends BaseDao<TicketSku>{
	

}