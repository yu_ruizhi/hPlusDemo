/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Sms;
import com.yuruizhi.hplusdemo.admin.entity.marketing.SmsLog;
import com.yuruizhi.hplusdemo.admin.entity.marketing.SmsMember;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.marketing.SmsLogService;
import com.yuruizhi.hplusdemo.admin.service.marketing.SmsMemberService;
import com.yuruizhi.hplusdemo.admin.service.marketing.SmsService;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.util.sms.SmsSender;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created on 2016-03-28 14:18:17
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/sms")
public class SmsController extends AdminBaseController<Sms> {

	@Autowired
	private SmsService smsService;
	
	@Autowired
	private SmsLogService smsLogService;
	
	@Autowired
	private SmsMemberService smsMemberService;
	
	@Autowired
	private MemberService memberService;
	
	@Autowired
	private SmsSender smsSender;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }
	
	@RequestMapping("smsSave")
	public String smsSave(Sms sms){
		if(null == sms){
			redirectViewPath("index");
		}else{
			smsService.smsSave(sms);
		}
		return redirectViewPath("index");
	}
	
	@Override
	@RequestMapping("edit/{id}")
	public String _compat(@PathVariable("id") Long id,Map<String, Object> model){
		Sms sms = smsService.find(id);
		model.put("sms", sms);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("smsId", id);
		List<SmsMember> smsMembers = smsMemberService.findMany(params);
		model.put("smsMembers", smsMembers);
		List<Member> members = memberService.getMembersBySmsId(id);
		String memberIds = "";
		for (int i = 0; i < members.size(); i++) {
			if(i==members.size()-1){
				memberIds += members.get(i).getId();
			}else{
				memberIds += members.get(i).getId()+",";
			}
		}
		sms.setMemberIds(memberIds);
		model.put("sms", sms);
		model.put("members", members);
		
		return getRelativeViewPath("edit");
	}
	
	@ResponseBody
	@RequestMapping("smsSend")
	public Map<String,Object> smsSend(Long[] ids,Map<String, Object> model){
		
		if(null != ids&&ids.length != 0){
			smsService.smsSend(ids);
			for (Long id : ids) {
				Sms sms = smsService.find(id);
				List<Member> members = memberService.getMembersBySmsId(id);
				List<String> phones = new ArrayList<String>();
				for (Member member : members) {
					if(null==member.getPhone()||member.getPhone().trim().equals("")){
						continue;
					}
					phones.add(member.getPhone());
				}
				String phoneStr = "";
				String phone[] = new String[phones.size()];
				for (int i = 0; i < phones.size(); i++) {
					phone[i] = phones.get(i);
					if(i==phones.size()-1){
						phoneStr +=phone[i];
					}else{
						phoneStr = phone[i]+",";
					}
					
				}
				@SuppressWarnings("unused")
				boolean flag = false;
				try {
					flag = smsSender.sendSms(phone,sms.getContent());
				} catch (Exception e) {
					flag = false;
				}finally {
					SmsLog smsLog = new SmsLog();
					smsLog.setSms(sms);
					smsLog.setContent(sms.getContent());
					smsLog.setReceiver(phoneStr);
					smsLog.setSendCount(phones.size());
					smsLog.setSendTime(new Date());
					smsLog.setCreatedDate(new Date());
					smsLog.setLastModifiedDate(new Date());
					smsLog.setSubject(sms.getSubject());
					smsLog.setStatus(2);
					smsLogService.save(smsLog);
					sms.setStatus(2);
					smsService.update(sms);
				}
				
				
			}
		}
		
		return null;
	}
	
}
