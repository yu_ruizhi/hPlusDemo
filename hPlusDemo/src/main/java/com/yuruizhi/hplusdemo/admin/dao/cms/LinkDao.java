/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.cms;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.TreeNodeBaseDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.Link;

/**
 * Created on 2016-02-18 15:12:54
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface LinkDao extends TreeNodeBaseDao<Link> {
}