/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.order.ProblemOrder;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ProblemOrderService</p>
 * <p>说明: 问题订单服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:35:45   
 * @version: 1.0
 */
public interface ProblemOrderService extends BaseService<ProblemOrder> {


    void updateStatus(Long id);
    
    /**
     * 
     * <p>名称：根据创建时间来查找id</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    Long[] findIdByTime(String beginDate,String endDate);
}
