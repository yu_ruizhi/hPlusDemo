/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: BrandValidate.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.validate
 * Date: 2016年8月17日下午4:10:33
 **/
package com.yuruizhi.hplusdemo.admin.validate;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Brand;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 品牌导入验证</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年8月18日上午10:41:44   
 * @version: 1.0
 */
public class BrandValidate {
	
	public static Map<String,Object> brandValidate (String list,String error,int i,Boolean success) {
		Map<String,Object> map = Maps.newHashMap();
		Brand brand = new Brand();
		String data[] = list.split(",");
		if ( 7 != data.length) {
			error += "第"+(i+1)+"行数据导入失败，原因为字段数量不符合<br>";
			success = false;
			map.put("brand", brand);
			map.put("success", success);
			map.put("error", error);
			return map;
		}
		//名称
		if (null != data[0] && !"".equals(data[0])) {
			if (250 < data[0].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为名称超过250个字符<br>";
				success = false;
			}
			brand.setName(data[0]);
		}
		//别名
		if (null != data[1] && !"".equals(data[1])) {
			if (250 < data[1].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为别名超过250个字符<br>";
				success = false;
			}
			brand.setAlias(data[1]);
		}
		//网址
		if (null != data[2] && !"".equals(data[2])) {
			if (250 < data[2].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为网址超过250个字符<br>";
				success = false;
			}
			brand.setUrl(data[2]);
		}
		//是否启用
		if (null != data[3] && !"".equals(data[3])) {
			if ("是".equals(data[3])) {
				brand.setEnabled(true);
			} else {
				brand.setEnabled(false);
			}
		}
		//是否推荐
		if (null != data[4] && !"".equals(data[4])) {
			if ("是".equals(data[4])) {
				brand.setRecommended(true);
			} else {
				brand.setRecommended(false);
			}
		}
		//排序
		if (null != data[5] && !"".equals(data[5])) {
			try{
				if (10 < data[5].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为排序超过10个字符<br>";
					success = false;
				}
				brand.setPosition(Integer.valueOf(data[5]));
			} catch (Exception e) {
				error += "第"+(i+1)+"行数据导入失败，原因为排序格式错误<br>";
				success = false;
			}
		}
		//介绍
		if (null != data[6] && !"".equals(data[6])) {
			brand.setDetail(data[6]);					
		}
		map.put("brand", brand);
		map.put("success", success);
		map.put("error", error);
		return map;
	}
	
}
