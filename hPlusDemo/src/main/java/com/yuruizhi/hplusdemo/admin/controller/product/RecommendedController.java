package com.yuruizhi.hplusdemo.admin.controller.product;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.CategoryRecommend;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryRecommendService;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

/**
 * Created by Administrator on 2015/6/25.
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/recommended")
public class RecommendedController extends BaseController {
	@Autowired
	private GoodsService goodsService;
	@Autowired
	private CategoryRecommendService categoryRecommendService;
	@Autowired
	private CategoryService categoryService;

	@RequestMapping("index")
	public void index() {
	}

	@RequestMapping("list")
	@ResponseBody
	public Page<CategoryRecommend> list(Filters filters, Pageable pageable) {
		Page<CategoryRecommend> clist = categoryRecommendService.findPage(filters, pageable);
		return clist;
	}

	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(Model model) {
		List<Category> clist = categoryService.findManyTopmost();
		model.addAttribute("clist", clist);
		return getViewPath("add");
	}

	@RequestMapping("selectCategoryChildren")
	@ResponseBody
	public List<Goods> selectChildren(String cid) {
		/* 根据分类id查询相关单品 */
		List<Goods> glist = goodsService.selectCategoryChildren(cid);
		return glist;
	}

	@RequestMapping("save")
	public String save(CategoryRecommend categoryRecommend, Long[] goodsId, String[] locations,
			HttpServletRequest request, HttpServletResponse response) {
		if (null != goodsId && goodsId.length > 0) {
			
			for (int i = 0; i < goodsId.length; i++) {
				CategoryRecommend categoryRecommend1 = new CategoryRecommend();
				categoryRecommend1.setGoods(new Goods());
				categoryRecommend1.setDeleted(false);
				categoryRecommend1.setCreatedDate(new Date());
				categoryRecommend1.getGoods().setId(goodsId[i]);
				categoryRecommend1.setLocation(Integer.parseInt(locations[i]));
				categoryRecommendService.forSave(categoryRecommend1);
			}
		}
		return redirectViewPath("index");
	}

	@RequestMapping("update")
	public String update(CategoryRecommend categoryRecommend, Long[] goodsId, Long[] crId, String[] locations,
			HttpServletRequest request, HttpServletResponse response) {
		categoryRecommendService.updateForSave(categoryRecommend, goodsId, crId, locations);
		return redirectViewPath("index");
	}

	@RequestMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, Map<String, Object> model) {
		CategoryRecommend categoryRecommend = categoryRecommendService.find(id);
		List<CategoryRecommend> clist = categoryRecommendService.cselectAll(categoryRecommend.getCategory().getId());
		Category c = clist.get(0).getCategory();
		Integer paixu = clist.get(0).getSort();
		Boolean visible = clist.get(0).getVisible();
		for (int i = 0; i < clist.size(); i++) {
			model.put("goods" + clist.get(i).getLocation(), clist.get(i).getGoods());
		}
		model.put("clist", clist);
		model.put("c", c);
		model.put("paixu", paixu);
		model.put("visible", visible);
		return getViewPath("edit");
	}

	@ResponseBody
	@RequestMapping("delete")
	public Map<String, Object> delete(@RequestParam("id") Long[] ids) {
		categoryRecommendService.delete(ids);
		return ImmutableMap.<String, Object>of("success", true);
	}

	@ResponseBody
	@RequestMapping("likez")
	public List<Goods> likez(String nm, String cid) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("nm", "%" + nm + "%");
		map.put("cid", cid);
		List<Goods> glist = goodsService.selectLike(map);
		return glist;
	}

	@ResponseBody
	@RequestMapping("isYou")
	public Boolean isYou(Long cid) {
		Integer count = categoryRecommendService.isYou(cid);
		if (null == count || count == 0) {
			return true;
		}
		return false;
	}

}
