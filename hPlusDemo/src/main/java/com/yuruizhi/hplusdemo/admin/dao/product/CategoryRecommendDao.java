/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.product;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.CategoryRecommend;

import java.util.List;

/**
 * Created on 2015-07-09 13:41:59
 *
 * @author vacoor
 */
@MyBatisMapper
public interface CategoryRecommendDao extends BaseDao<CategoryRecommend> {
    List<CategoryRecommend> cselectAll(Long cid);

    void deletec(Long cid);

    Integer isYou(Long cid);
}