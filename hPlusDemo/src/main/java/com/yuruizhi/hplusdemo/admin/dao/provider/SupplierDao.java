/*
* Copyright (c) 2005, 2014 vacoor
*
* Licensed under the Apache License, Version 2.0 (the "License");
*/
package com.yuruizhi.hplusdemo.admin.dao.provider;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;

import java.util.List;

/**
* Created on 2014-10-30 17:10:16
*
* @author crud generated
*/
public interface SupplierDao extends BaseDao<Supplier> {
	
	Supplier getById(String id);

    List<Supplier> findManyByGoodsId(String gid);
    
    public String getIdByName(String supplierName);
    
    /**
     * 
     * <p>名称：根据供应商名称获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param supplierName
     * @return
     */
    List<Supplier> findBySupplierName(String supplierName);

    List<Supplier> getSupplierExistsBycode(String code);
}