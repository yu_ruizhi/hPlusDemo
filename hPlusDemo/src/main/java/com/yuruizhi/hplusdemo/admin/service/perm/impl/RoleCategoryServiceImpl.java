/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import com.yuruizhi.hplusdemo.admin.dao.perm.RoleCategoryDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.RoleCategory;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.RoleCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <p>名称: RoleServiceImpl</p>
 * <p>说明: 后台角色管理服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:40:26   
 * @version: 1.0
 */
@Service
public class RoleCategoryServiceImpl extends BaseServiceImpl<RoleCategory> implements RoleCategoryService {

    private RoleCategoryDao roleCategoryDao;

    @Autowired
    private void setRoleDao(RoleCategoryDao roleCategoryDao){
        this.roleCategoryDao = roleCategoryDao;
        setBaseDao(roleCategoryDao);
    }

    @Override
    public List<RoleCategory> findInRole(Long roleId) {
        return roleCategoryDao.findInRole(roleId);
    }

    @Override
    public void deleteInRole(Long roleId) {
        roleCategoryDao.deleteInRole(roleId);
    }
}
