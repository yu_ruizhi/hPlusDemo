/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月20日 上午10:37:52  
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategory;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 分类商品Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:37:52  
 * @version: 1.0
 */
public interface ProductCategoryService extends BaseService<ProductCategory> {

	/**
	 * 
	 * <p>名称：根据名称检测分类推荐是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name 分类推荐名称
	 * @return 返回true，false
	 */
	Map<String, Boolean> checkIsNameExists(String name);
	
	/**
	 * 
	 * <p>名称：根据分类推荐id及分类推荐名称是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param hasAnnotherName 分类推荐id,分类推荐名称数组
	 * @return 返回true,false
	 */
	Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName);
	
}
