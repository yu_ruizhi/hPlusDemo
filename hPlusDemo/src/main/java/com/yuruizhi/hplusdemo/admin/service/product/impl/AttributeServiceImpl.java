/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.product.AttributeDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.AttributeService;
import com.google.common.collect.Maps;

/**
 * <p>名称: AttributeServiceImpl</p>
 * <p>说明: 产品属性服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:46:59   
 * @version: 1.0
 */
@Service
public class AttributeServiceImpl extends BaseServiceImpl<Attribute> implements AttributeService {
    
	private AttributeDao attributeDao;
	
	@Autowired
	public void setAttributeDao(AttributeDao attributeDao){
    	this.attributeDao = attributeDao;
    	setCrudDao(attributeDao);
    }

	@Override
	public Attribute findByNameAndModelId(String name, Long modelId) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("name", name);
		map.put("modelId", modelId);
		return attributeDao.findByNameAndModelId(map);
	}

	@Override
	public Attribute findByNameAndIsBase(String name) {
		return attributeDao.findByNameAndIsBase(name);
	}

	@Override
	public List<Attribute> findModelAttributes(Long modelId) {
		return attributeDao.findModelAttributes(modelId);
	}
}
