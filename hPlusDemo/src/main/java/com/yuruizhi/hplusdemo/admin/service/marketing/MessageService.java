/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月20日上午11:25:01
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.entity.marketing.Message;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.List;

/**
 * 
 * <p>名称: 私信Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日上午11:25:33
 * @version: 1.0
 */
public interface MessageService extends BaseService<Message> {

    //订单状态为尾款订单后,站内信通知
    void OrderAllotNotice(Long[] ids);
}
