/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms;

import com.yuruizhi.hplusdemo.admin.entity.cms.LeaveMessage;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: LeaveMessageService</p>
 * <p>说明: 留言服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:06:05   
 * @version: 1.0
 */
public interface LeaveMessageService extends BaseService<LeaveMessage> {

}
