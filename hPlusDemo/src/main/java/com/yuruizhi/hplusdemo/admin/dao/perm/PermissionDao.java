package com.yuruizhi.hplusdemo.admin.dao.perm;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.perm.Module;

/**
 * Created by ASUS on 2014/12/25.
 */
public interface PermissionDao {
    List<Module> findPermissionByUserId(Long userId);
}
