/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: HotProductController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月20日 上午10:00:30
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.NewOrHot;
import com.yuruizhi.hplusdemo.admin.service.marketing.NewOrHotService;

/**
 * 
 * <p>名称: APP首页艾曼热卖 Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（20160523 - zuoyang - 列表页数据过滤掉下架的单品）</p>  
 * 
 * @author：wentan
 * @date：2016年4月20日 上午10:00:30  
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "appHotProduct")
public class HotProductController extends AdminBaseController<NewOrHot> {
	
	@Autowired
	private NewOrHotService newOrHotProductService;

	/**
	 * 
	 * <p>名称：异步获取列表数据</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param newProductfilters
	 * @param goodsFilters
	 * @param categoryFilters
	 * @param pageable
	 * @return
	 */
	@RequestMapping("appHotProductList")
	@ResponseBody
	public Page<NewOrHot> list(@Qualifier("N.")Filters newProductfilters, 
								@Qualifier("G.")Filters goodsFilters, 
								@Qualifier("C.")Filters categoryFilters, 
								Pageable pageable) {
		
		newProductfilters.add("type", Filters.Operator.EQ, Constant.APP_HOT_PRODUCT_TYPE);
		//update 20160523 zuoyang 列表页数据过滤掉下架的单品
		goodsFilters.add("on_sell", Filters.Operator.EQ, Constant.GOODS_STATUS_NORMAL);
		
		Page<NewOrHot> page = newOrHotProductService.findPage(newProductfilters, 
				goodsFilters, categoryFilters, pageable);
		return page;
	}
	
}
