/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: SeoServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.platform.impl
 * Date: 2016年4月23日上午11:15:15
 **/
package com.yuruizhi.hplusdemo.admin.service.platform.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.platform.Seo;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.platform.SeoService;

/**
 * <p>名称: SEO Service实现类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月23日上午11:15:50
 * @version: 1.0
 */
@Service
public class SeoServiceImpl extends BaseServiceImpl<Seo> implements SeoService {
}
