/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月20日 上午11:59:59  
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategory;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategoryProduct;
import com.yuruizhi.hplusdemo.admin.service.marketing.ProductCategoryProductService;
import com.yuruizhi.hplusdemo.admin.service.marketing.ProductCategoryService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 分类商品Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午11:59:59
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX+"productCategory")
public class ProductCategoryController extends AdminBaseController<ProductCategory>{

	@Autowired
	private ProductCategoryService productCategoryService;
	
	@Autowired
	private ProductCategoryProductService productCategoryProductService;
    
    @Autowired
    private SysLogService sysLogService;
	
	/**
	 * 
	 * <p>名称：获取数据</p> 
	 * <p>描述：异步获取分类推荐首页需要显示的数据</p>
	 * @author：wuqi
	 * @param filters 过滤器
	 * @param pageable 初始页数
	 * @return
	 */
	@ResponseBody
	@RequestMapping("list")
	public Page<ProductCategory> list(Filters filters, Pageable pageable){
		return productCategoryService.findPage(filters, pageable);
	}
	
	/**
	 * 
	 * <p>名称：分类推荐保存</p> 
	 * <p>描述：保存新增页面中的分类推荐数据</p>
	 * @author：wuqi
	 * @param productCategory 分类推荐
	 * @return
	 */
	@RequestMapping("saveCategory")
	public String save(ProductCategory productCategory){
		productCategory.setCreatedDate(new Date());
		productCategoryService.save(productCategory);
		
		List<ProductCategoryProduct> productCategoryProductList = productCategory.getProductCategoryProductList();
		String goodsOperation = "分类商品推荐-新增-id:"+productCategory.getId()+",单品id:";
		for(ProductCategoryProduct productCategoryProduct : productCategoryProductList) {
			productCategoryProduct.setCategoryId(productCategory.getId());
			productCategoryProduct.setCreatedDate(new Date());
			productCategoryProductService.save(productCategoryProduct);
			goodsOperation += productCategoryProduct.getId()+",";
		}
		//系统日志
		goodsOperation = goodsOperation.substring(0, goodsOperation.length()-1);
		String operation = "分类商品推荐-新增-id:"+productCategory.getId()+",名称:"+productCategory.getName();
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, goodsOperation, "分类商品推荐");
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, operation, "分类商品推荐");
		return redirectViewPath("index");
	}
	
	/**
	 * 
	 * <p>名称：编辑分类推荐</p> 
	 * <p>描述：从分类推荐列表页进行编辑页面</p>
	 * @author：wuqi
	 * @param id 分类推荐id
	 * @return
	 */
	@Override
	@RequestMapping("edit/{id}")
	public String _compat(@PathVariable(value="id") Long id,Map<String, Object> model){
		ProductCategory productCategory = productCategoryService.find(id);
		List<ProductCategoryProduct> productCategoryProductList = productCategoryProductService.findByCategoryId(productCategory.getId());
		productCategory.setProductCategoryProductList(productCategoryProductList);
		
		model.put("productCategory", productCategory);
		return getRelativeViewPath("edit");
	}
	
	/**
	 * 
	 * <p>名称：更新分类推荐</p> 
	 * <p>描述：对在编辑页面进行修改的分类推荐数据进行更新</p>
	 * @author：wuqi
	 * @param productCategory 分类推荐数据
	 * @return
	 */
	@Override
	@RequestMapping("update")
	public String update(ProductCategory productCategory, BindingResult binding, RedirectAttributes redirectAttrs){
		productCategory.setLastModifiedDate(new Date());
		productCategoryService.update(productCategory);
		
		//查找选择的商品是否为保存
		List<ProductCategoryProduct> productCategoryProductList = productCategory.getProductCategoryProductList();
		for(ProductCategoryProduct productCategoryProduct : productCategoryProductList) {
			//根据分类id及商品id进行查找，为null插入
			ProductCategoryProduct productCategoryProductInfo = productCategoryProductService.findByIdAndCategoryId(productCategoryProduct.getGoodsId(),
																													productCategory.getId());
			if (null == productCategoryProductInfo) {
				productCategoryProduct.setCategoryId(productCategory.getId());
				productCategoryProduct.setCreatedDate(new Date());
				productCategoryProductService.save(productCategoryProduct);
			} else {
				//比较排序是否被更改
				int oldProductSort = productCategoryProductInfo.getSort() == null ? 0 : productCategoryProductInfo.getSort();
				int productSort = productCategoryProduct.getSort() == null ? 0 : productCategoryProduct.getSort();
				if (oldProductSort != productSort){
					productCategoryProductService.update(productCategoryProduct);
				}
			}
		}
		
		//系统日志
		String operation = "分类商品推荐-修改-id:"+productCategory.getId();
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "分类商品推荐");
		return redirectViewPath("index");
	}
	
	/**
	 * 
	 * <p>名称：删除分类推荐</p> 
	 * <p>描述：删除分类推荐数据</p>
	 * @author：wuqi
	 * @param id 分类推荐id
	 * @return
	 */
	@Override
	@ResponseBody
	@RequestMapping("delete")
	public Map<String,Object> delete(@RequestParam("id") Long[] id){
		productCategoryService.delete(id);
		String ids = "";
		for (int i = 0; i < id.length; i++) {
			ids += id[i]+",";
		}
		
		//系统日志
		ids = ids.substring(0, ids.length()-1);
		String operation = "分类商品推荐-删除-id:"+ids;
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "分类商品推荐");
		return ImmutableMap.<String, Object> of("success", true);
	}
	
	/**
	 * 
	 * <p>名称：删除分类商品-商品</p> 
	 * <p>描述：编辑页面删除分类推荐中的单品数据</p>
	 * @author：wuqi
	 * @param id 单品id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("deleteProduct")
	public Map<String,Object> deleteProduct(Long id){
		Map<String,Object> map = Maps.newHashMap();
		ProductCategoryProduct productCategoryProduct = productCategoryProductService.find(id);
		productCategoryProductService.delete(id);
		if (null != productCategoryProduct) {
			String operation = "分类商品推荐-删除-分类推荐id:"+productCategoryProduct.getCategoryId()+",单品id:"+id;
			sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "分类商品推荐");
		}
		
		map.put("success",true);
		return map;
	}

	/**
	 * 
	 * <p>名称：检测是否重名</p> 
	 * <p>描述：新增页面根据分类名称检测是否重名</p>
	 * @author：wuqi
	 * @param name 分类名称
	 * @return
	 */
	@RequestMapping("checkIsNameExists")
	@ResponseBody
	public Map<String, Boolean> checkIsNameExists(String name){
		return productCategoryService.checkIsNameExists(name);
	}
	
	/**
	 * 
	 * <p>名称：检测是否重名</p> 
	 * <p>描述：编辑页面根据分类id及分类名称检测是否重名</p>
	 * @author：wuqi
	 * @param idAndName 分类id及分类名称
	 * @return
	 */
	@RequestMapping("hasAnnotherName")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherName(String[] idAndName){
		Map<String, Boolean> map = productCategoryService.hasAnnotherName(idAndName);
		return map;
	}
	
}
