/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProvinceService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area
 * Date: 2016年4月18日下午7:26:58
 **/
package com.yuruizhi.hplusdemo.admin.service.area;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.area.Province;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 省份Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午7:27:30
 * @version: 1.0
 */
public interface ProvinceService extends BaseService<Province>{
	
	/**
	 * 
	 * <p>名称：通过省份编号查找省份</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param provinceId 省份编号
	 * @return 省份
	 */
	Province findByProvinceId(String provinceId);
	
	List<Province> listPvcToCity();
	
	/**
	 * 
	 * <p>名称：列出直辖市</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @return 直辖市列表
	 */
	List<Province> listMunicipalities();
}
