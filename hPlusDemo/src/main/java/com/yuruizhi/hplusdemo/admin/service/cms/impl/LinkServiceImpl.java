/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.cms.Link;
import com.yuruizhi.hplusdemo.admin.service.TreeNodeBaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.LinkService;

import java.util.Map;

/**
 * <p>名称: LinkServiceImpl</p>
 * <p>说明: 链接服务实现接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:02:12   
 * @version: 1.0
 */
@Service
public class LinkServiceImpl extends TreeNodeBaseServiceImpl<Link> implements LinkService {
    private static final int DEFAULT_MAX_DEPTH = 999;

    protected LinkServiceImpl() {
        super(null);
    }

    @Override
    protected Link doMountNode(Link node) {
        // 执行子节点挂到父节点操作时, 设置当前节点允许的最大层级是父节点 - 1
        node = super.doMountNode(node);
        Link parent = node.getParent();
        Integer depth = null != parent ? parent.getMaxDepth() : DEFAULT_MAX_DEPTH;
        node.setMaxDepth(depth - 1);
        return node;
    }

    @Override
    protected Link createDefaultRootNode() {
        Link root = super.createDefaultRootNode();
        // 设置默认节点最大层级
        root.setMaxDepth(DEFAULT_MAX_DEPTH);
        return root;
    }

    @Override
    protected Map<String, Object> buildDescendantUpdateParams(Link snapshot, Link current) {
        Map<String, Object> params = super.buildDescendantUpdateParams(snapshot, current);

        Integer oldMaxDepth = snapshot.getMaxDepth();
        Integer newMaxDepth = current.getMaxDepth();
        oldMaxDepth = null != oldMaxDepth ? oldMaxDepth : DEFAULT_MAX_DEPTH;
        newMaxDepth = null != newMaxDepth ? newMaxDepth : DEFAULT_MAX_DEPTH;

        params.put("_maxDepthIncrement", newMaxDepth - oldMaxDepth);

        return params;
    }
}
