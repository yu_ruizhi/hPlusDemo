/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: SeoController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.platform
 * Date: 2016年4月23日上午11:18:16
 **/
package com.yuruizhi.hplusdemo.admin.controller.platform;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.platform.Seo;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;

/**
 * 
 * <p>名称: SEO Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月23日上午11:18:43
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/seo")
public class SeoController extends AdminBaseController<Seo> {
	@Autowired
	private SysLogService sysLogService;
	
	/**
	 * <p>名称：重写父类的方法，新增时添加日志</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 */
	@Override
	public String create(Seo seo, BindingResult binding, RedirectAttributes redirectAttrs) {
		String result = super.create(seo, binding, redirectAttrs);
		if(null != seo){
			sysLogService.saveSysLog(Constant.SYSLOG_SAVE, 
					"Seo-新增-ID:"+seo.getId()+"标题:"+seo.getTitle(), "Seo管理");
		}
		return result;
	}
	
	/**
	 * <p>名称：重写父类的方法，更新时添加日志</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 */
	@Override
	public String update(Seo seo, BindingResult binding, RedirectAttributes redirectAttrs) {
		if(null != seo){
			sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, 
					"Seo-修改-ID:"+seo.getId()+"标题:"+seo.getTitle(), "Seo管理");
		}
		return super.update(seo, binding, redirectAttrs);
	}
	
	/**
	 * <p>名称：重写父类的方法，删除时添加日志</p>
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 */
	@Override
	public Map<String, Object> delete(Long[] id) {
		String idStr = "";
		for(int i=0;i<id.length;i++){
			idStr += id[i] + ",";
		}
		idStr = idStr.substring(0, idStr.length()-1);
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "Seo-删除-ID:"+idStr, "Seo管理");
		return super.delete(id);
	}
	
}
