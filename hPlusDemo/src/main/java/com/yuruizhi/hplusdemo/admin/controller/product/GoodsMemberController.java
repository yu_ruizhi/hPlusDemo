/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsMemberController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.product
 * Date: 2016年6月16日下午3:34:21
 **/
package com.yuruizhi.hplusdemo.admin.controller.product;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ponly.common.json.Jacksons;
import org.ponly.webbase.dao.CrudDao;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsMember;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsMemberService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.google.common.collect.Maps;

/**
 * <p>名称:  预售商品截单权限Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年6月16日下午3:34:40   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "goodsMember")
public class GoodsMemberController extends AdminBaseController<GoodsMember> {
	
	@Autowired
	private GoodsService goodsService;
	
	@Autowired
	private GoodsMemberService goodsMemberService;
	
	/**
	 * <p>名称：自定义数据展示</p> 
	 * <p>描述：获取单品中未删除、已上架、预定截止时间小于当前时间的记录</p>
	 * @author：qinzhongliang
	 * @param filters
	 * @param pageable
	 * @return
	 */
	@ResponseBody
	@RequestMapping("customList")
	public Page<Goods> customList(Filters filters, Pageable pageable) {	
		
		filters.add("ON_SELL", Filters.Operator.EQ, new Boolean(true));
		filters.add("RESERVE_DEADLINE", Filters.Operator.LT, new Date());
		
		Map<String, Object> params = Maps.newHashMap();
		params.put(CrudDao.FILTERS_PROP, filters);
		params.put("reserved","1");
		
		return goodsService.findPage(params, pageable);
	}
	
	/**
	 * <p>名称：跳转到编辑页</p> 
	 * <p>描述：因列表页是从单品表中获取的，故此处id实际为单品id</p>
	 * @author：qinzhongliang
	 * @param id	单品id
	 * @param model	数据模型
	 * @return
	 */
	@RequestMapping("customEdit")
	public String customEdit(@RequestParam("goodsId")Long goodsId, Map<String, Object> model) {
		List<GoodsMember> goodsMemberList = goodsMemberService.findByGoodsId(goodsId);
		model.put("goodsMemberList", goodsMemberList);
		model.put("goodsId", goodsId);
		return getRelativeViewPath("edit");
	}
	
	/**
	 * <p>名称：自定义保存方法</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param goodsMember
	 */
	@RequestMapping("customSave")
	public String customSave(GoodsMember goodsMember) {
		GoodsMember tempObj = new GoodsMember();
		List<Member> memberList = goodsMember.getMemberList();
		if(null != memberList) {
			for(Member member : memberList) {
				tempObj.setGoodsId(goodsMember.getGoodsId());
				tempObj.setMemberId(member.getId());
				goodsMemberService.save(tempObj);
			}
		}
		return redirectViewPath("/");
	}
}
