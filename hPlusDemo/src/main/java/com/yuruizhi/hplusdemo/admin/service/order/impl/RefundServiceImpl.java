/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import com.yuruizhi.hplusdemo.admin.dao.order.OrdersReceiptDao;
import com.yuruizhi.hplusdemo.admin.dao.order.RefundDao;
import com.yuruizhi.hplusdemo.admin.entity.order.Refund;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.order.RefundService;

/**
 * <p>名称: RefundServiceImpl</p>
 * <p>说明: 退换货服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:32:16   
 * @version: 1.0
 */
@Service
public class RefundServiceImpl extends BaseServiceImpl<Refund> implements RefundService {

	@Autowired
	private RefundDao refundDao;
	
	@Autowired
	private OrdersReceiptDao orderDao;
	
	@Override
	public Refund findRefundById(Long id) {
		
		return refundDao.findRefundById(id);
	}

	@Override
	public void updateStatus(Long id, int status) {
		
		refundDao.updateStatus(id,status);
		
	}

	@Override
	public void batchReject(String ids, String rejectReason) {
		int length = ids.length();
		if(length<1){
		}else{
			String[] strIdArr = ids.split(",");
			for (String strId : strIdArr) {
				Long id = Long.parseLong(strId);
				Refund refund = refundDao.findRefundById(id);
				if(refund.getStatus()==1){
					refund.setRejectReason(rejectReason);
					refund.setStatus(3);
					refundDao.update(refund);
					orderDao.updateOrdersReceipt(refund.getOrderId(), 14);
				}
				
			}
			
		}
		
	}

	@Override
	public void batchAgree(Long[] ids) {
		for (Long id : ids) {
			Refund refund = refundDao.findRefundById(id);
			if(refund.getStatus()==1){
				refundDao.updateStatus(id, 2);
			}
			//orderDao.updateOrdersReceipt(refund.getOrderId(), 15);
		}
		
	}
}
