/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.marketing.FlashSaleDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.FlashSaleDetailService;

/**
 * <p>名称: FlashSaleDetailServiceImpl</p>
 * <p>说明: 打折优惠等服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:19:58   
 * @version: 1.0
 */
@Service
public class FlashSaleDetailServiceImpl extends BaseServiceImpl<FlashSaleDetail> implements FlashSaleDetailService {
}
