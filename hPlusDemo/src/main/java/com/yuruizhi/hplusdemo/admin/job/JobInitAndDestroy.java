/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.job;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Pageable;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.yuruizhi.hplusdemo.admin.entity.marketing.Email;
import com.yuruizhi.hplusdemo.admin.service.marketing.EmailService;
import com.yuruizhi.hplusdemo.admin.util.ScheduleUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Date;
import java.util.List;

/**
 * <p>名称: JobInitAndDestroy</p>
 * <p>说明: 定时任务加载和关闭</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:51:37   
 * @version: 1.0
 */
@Component
public class JobInitAndDestroy {
    /** 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(JobInitAndDestroy.class);
    @Autowired
    private EmailService emailService;
    @Autowired
    @Qualifier("scheduler")
    private Scheduler scheduler;
    /**
     * web容器启动时运行此方法
     * @throws Exception
     */
    @PostConstruct
    public void init() {
        initScheduleJob();
        try {
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
            LOG.error("初始化失败",e);
        }
    }

    /**
     * web容器关闭时，调用此方法，不是直接停止哦！
     *  附：jetty停止命令[ jetty:stop ]
     */
    @PreDestroy
    public void close(){
        try {
            scheduler.shutdown();
            System.out.println("关闭");
        } catch (SchedulerException e) {
            e.printStackTrace();
            System.out.println("关闭异常");
            LOG.error("关闭异常",e);

        }
    }

    /**
     * 初始化任务
     */
    public void initScheduleJob() {
        //创建任务//或者数据库中读取需要调度的任务
        Filters filters = Filters.create();
        filters.eq("status",1).gt("scheduleTime",new Date());
        List<Email> emailList = emailService.findMany(filters, (Pageable) null);
        for(Email email:emailList){
            ScheduleUtils.createScheduleJob(scheduler, email.getId(), "email"+email.getId(), email.getScheduleTime());
        }

    }

}
