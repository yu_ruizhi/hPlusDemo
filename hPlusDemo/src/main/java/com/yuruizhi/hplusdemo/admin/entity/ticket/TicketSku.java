/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.ticket;




import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.List;

/**
 * 票务规格表
 */
public class TicketSku extends BaseEntity {

	private String name;

	private Integer type;

	private Integer timeType;
	private Integer timeTypeDays;
  
	public Integer getTimeTypeDays() {
		return timeTypeDays;
	}

	public void setTimeTypeDays(Integer timeTypeDays) {
		this.timeTypeDays = timeTypeDays;
	}

	private Boolean deleted;

    private Ticket ticket;

    private  List<TicketSkuValue> ticketSkuValues;

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public List<TicketSkuValue> getTicketSkuValues() {
        return ticketSkuValues;
    }

    public void setTicketSkuValues(List<TicketSkuValue> ticketSkuValues) {
        this.ticketSkuValues = ticketSkuValues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
