/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.*;

/**
 * <p>名称: Refund</p>
 * <p>说明: 退换货实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:13:27   
 * @version: 1.0
 */
public class Refund extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1268280765126022643L;

	/**
     * 退换货类型
     * 1:退货退款, 2:仅退款, 3:换货
     * 
     * @ViewField editor=spinner 
     * @Column TYPE   1:退货退款;2:仅退款;3:换货
     */
    @NotNull
    private Integer type;

    /**
     * 退款金额
     * 
     * @ViewField editor=input 
     * @Column PRICE
     */
    @Size(max = 8)
    @Digits(integer = 8, fraction = 3)
    private Double price;

    /**
     * 是否收到货
     * 
     * @ViewField editor=input 
     * @Column IS_RECEIPT_GOODS
     */
    @Size(max = 1)
    private Boolean isReceiptGoods = Boolean.FALSE; ;

    /**
     * 退换货原因
     * 
     * @ViewField editor=spinner 
     * @Column REASON
     */
    @NotNull
    private Integer reason;

    /**
     * NOTE
     * 
     * @ViewField editor=input 
     * @Column NOTE
     */
    @Size(max = 255)
    private String note;

    /**
     * 附件
     * 
     * @ViewField editor=input 
     * @Column ATTACHMENTS
     */
    @Size(max = 255)
    private String attachments;

    /**
     * 状态
     * 
     * @ViewField editor=spinner 
     * @Column STATUS   1:买家发出退货申请;2:卖家同意退货处理;3:卖家拒绝;4:已完成
     */
    @Size(max = 6)
    private Integer status;

    /**
     * 订单明细ID
     * 
     * @ViewField editor=spinner 
     * @Column ORDER_DETAIL_ID
     */
    private Long orderDetailId;

    /**
     * 订单ID
     * 
     * @ViewField editor=spinner 
     * @Column ORDER_ID
     */
    private Long orderId;
    
    private OrdersReceipt order;
    
    

	private List<RefundImg> refundImgs;
  
    private String rejectReason;//卖家拒绝退还原因
    
    /**
     * 退换数量
     */
    private int qty;
    public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}
    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getIsReceiptGoods() {
        return this.isReceiptGoods;
    }

    public void setIsReceiptGoods(Boolean isReceiptGoods) {
        this.isReceiptGoods = isReceiptGoods;
    }

    public Integer getReason() {
        return this.reason;
    }

    public void setReason(Integer reason) {
        this.reason = reason;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAttachments() {
        return this.attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public List<RefundImg> getRefundImgs() {
		return refundImgs;
	}

	public void setRefundImgs(List<RefundImg> refundImgs) {
		this.refundImgs = refundImgs;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getOrderDetailId() {
		return orderDetailId;
	}

	public void setOrderDetailId(Long orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

	public OrdersReceipt getOrder() {
		return order;
	}

	public void setOrder(OrdersReceipt order) {
		this.order = order;
	}


	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

}
