/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.cms.Article;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ArticleService</p>
 * <p>说明: 文章服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:04:39   
 * @version: 1.0
 */
public interface ArticleService extends BaseService<Article> {
	/**
	 * <p>名称：getInfo</p> 
	 * <p>描述：获取新闻信息</p>
	 * @author：ChenGuang
	 * @param params
	 * @return
	 */
	List<Article> getInfo(Map<String, Object> params);

	/**
	 * <p>名称：getInfoCount</p> 
	 * <p>描述：获取新闻信息数量</p>
	 * @author：ChenGuang
	 * @param params
	 * @return
	 */
	int getInfoCount(Map<String, Object> params);
}
