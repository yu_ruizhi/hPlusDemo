/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 * <p>
 * ProjectName: hPlusDemo
 * FileName: MessageMemberServiceImpl.java
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月20日下午2:28:07
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import com.yuruizhi.hplusdemo.admin.dao.marketing.MessageLogDao;

import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageLog;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsCheck;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;

import com.yuruizhi.hplusdemo.admin.service.marketing.MessageLogService;


import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * <p>名称: MessageLogServiceImpl</p>
 * <p>说明: 私信记录Service实现</p>
 * <p>创建记录：（2016.8.19 14:13 - yuruizhi - 增加私信记录Service实现）</p>
 *
 * @author yuruizhi
 * @date 2016年8月19日 下午14:44:06
 * @version: 1.0
 */
@Service
public class MessageLogServiceImpl extends BaseServiceImpl<MessageLog> implements MessageLogService {

    @Autowired
    private MessageLogDao messageLogDao;

    /**
     *
     * <p>名称：根据会员ID查询记录</p>
     * <p>描述：</p>
     * @author yuruizhi
     * @param memberId 会员ID
     * @return
     */
    @Override
    public List<MessageLog> findByMemberId(String memberId) {
        return messageLogDao.findByMemberId(memberId);
    }

	@Override
	public Page<MessageLog> findPage(Filters memberFilters, Pageable pageable) {

        Map<String, Object> paramsMap = createParamsMap();

        if (null != memberFilters&&memberFilters.size()>0) {
            paramsMap.put("_member_filters", memberFilters);
        }
       

        if (null != pageable) {
            paramsMap.put(MessageLogDao.OFFSET_PROP, pageable.getOffset());
            paramsMap.put(MessageLogDao.MAX_RESULTS_PROP, pageable.getPageSize());

            Sort sort = pageable.getSort();
            if (null != sort) {
                paramsMap.put(MessageLogDao.SORT_PROP, sort);
            }
        }
        
        int total = count(paramsMap);
        List<MessageLog> data = total > 0 ? findMany(paramsMap) : Collections.<MessageLog>emptyList();
        
        return new SimplePage<MessageLog>(pageable, data, total);
        
	}


}
