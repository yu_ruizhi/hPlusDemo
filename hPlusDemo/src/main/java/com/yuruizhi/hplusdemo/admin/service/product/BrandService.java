/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.product.Brand;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: BrandService</p>
 * <p>说明: 产品品牌服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:00:12   
 * @version: 1.0
 */
public interface BrandService extends BaseService<Brand> {

	/**
	 * 
	 * <p>名称：根据品牌名获取数据</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	List<Brand> findByName(String name);
	
}
