package com.yuruizhi.hplusdemo.admin.service.logistics.impl;


import com.yuruizhi.hplusdemo.admin.dao.logistics.DeliveryModuleDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModuleDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryModuleDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ASUS on 2015/1/13.
 */
@Service
public class DeliveryModuleDetailServiceImpl extends BaseServiceImpl<DeliveryModuleDetail> implements DeliveryModuleDetailService {

    private DeliveryModuleDetailDao deliveryModuleDetailDao;

    @Autowired
    public void setDeliveryModuleDetailDao(DeliveryModuleDetailDao deliveryModuleDetailDao){
        this.deliveryModuleDetailDao = deliveryModuleDetailDao;
        super.setBaseDao(deliveryModuleDetailDao);
    }

    @Override
    public void deleteByModuleId(Long moduleId) {
        deliveryModuleDetailDao.deleteByModuleId(moduleId);
    }
}
