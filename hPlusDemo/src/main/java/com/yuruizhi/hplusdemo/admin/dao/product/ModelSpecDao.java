/*
 * Copyright (c) 2005, 2014 besture
 */
package com.yuruizhi.hplusdemo.admin.dao.product;


import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ModelSpec;

public interface ModelSpecDao extends BaseDao<ModelSpec> {

    void deleteModelSpecs(Long mid);

    List<ModelSpec> findModelSpecs(Long mid);

    Boolean isReferenced(Long msId);
}