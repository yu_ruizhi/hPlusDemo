/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.cms;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: ArticleTag</p>
 * <p>说明: 文章标签实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:42:06   
 * @version: 1.0
 */
public class ArticleTag extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 文章Id
     */
    private Long articleId;
    /**
     * 文章标签Id
     */
    private Long tagId;

    private boolean deleted = false;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
}
