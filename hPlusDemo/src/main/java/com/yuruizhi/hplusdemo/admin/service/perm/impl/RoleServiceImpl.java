/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import com.yuruizhi.hplusdemo.admin.dao.perm.RoleCategoryDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.RoleCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.perm.RoleDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.Role;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.RoleService;
import com.yuruizhi.hplusdemo.admin.util.ArrayUtil;

import java.util.*;

/**
 * <p>名称: RoleServiceImpl</p>
 * <p>说明: 后台角色管理服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:40:26   
 * @version: 1.0
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService {

    private RoleDao roleDao;

    @Autowired
    private void setRoleDao(RoleDao roleDao){
        this.roleDao = roleDao;
        setBaseDao(roleDao);
    }

    @Autowired
    private  RoleCategoryDao roleCategoryDao;

    @Override
    
    public void saveRole(Role role, String pages) {
        role.setDeleted(Constant.NOT_DELETED);
        save(role);
        if(!StringUtils.hasText(pages)){
            return;
        }

        Long roleId = role.getId();
        String[] pageIds = ArrayUtil.stringToArray(pages);
        for(String pageId : pageIds){
        	Map<String, Object> roleToPage = new HashMap<String, Object>();	
            roleToPage.put("roleId", roleId);
            roleToPage.put("pageId", pageId);
            roleToPage.put("deleted", Constant.NOT_DELETED);
            roleToPage.put("createdDate", new Date());
            roleToPage.put("lastModifiedDate", new Date());
            roleDao.saveRoleToPage(roleToPage);
        }
        if(null!=role.getRoleCategories()&&role.getRoleCategories().size()>0){
            for(RoleCategory roleCategory:role.getRoleCategories()){
                if(null!=roleCategory.getCategoryId()){
                    roleCategory.setRoleId(roleId);
                    roleCategory.setDeleted(false);
                    roleCategory.setCreatedDate(new Date());
                    roleCategoryDao.save(roleCategory);
                }
            }
        }
    }

    @Override
    
    public boolean deleteRole(Long id) {
        if(null == id){
            return false;
        }
        Role role = new Role();
        role.setId(id);
        delete(role);
        roleDao.deleteRoleToPageByRoleId(id);
        return true;
    }

    @Override
    
    public boolean deleteRoleByIds(Long[] ids){
        if(null == ids){
            return false;
        }
        for(Long id: ids){
            deleteRole(id);
        }

        return true;
    }

    @Override
    
    public void updateRole(Role role, String pages) {
        if(StringUtils.isEmpty(role)){
            return;
        }

        update(role);

        if(!StringUtils.hasText(pages)){
            return;
        }

        Long roleId = role.getId();
        roleDao.deleteRoleToPageByRoleId(roleId);

        String[] pageIds = ArrayUtil.stringToArray(pages);
        
        for(String pageId : pageIds){
        	Map<String, Object> roleToPage = new HashMap<String, Object>();
            roleToPage.put("roleId", roleId);
            roleToPage.put("pageId", pageId);
            roleToPage.put("deleted", Constant.NOT_DELETED);
            roleToPage.put("createdDate", new Date());
            roleToPage.put("lastModifiedDate", new Date());
            roleDao.saveRoleToPage(roleToPage);
        }
roleCategoryDao.deleteInRole(roleId);
        if(null!=role.getRoleCategories()&&role.getRoleCategories().size()>0){
            for(RoleCategory roleCategory:role.getRoleCategories()){
                if(null!=roleCategory.getCategoryId()){
                    roleCategory.setRoleId(roleId);
                    roleCategory.setDeleted(false);
                    roleCategory.setCreatedDate(new Date());
                    roleCategoryDao.save(roleCategory);
                }
            }
        }
    }

    @Override
    public List<Map<String, Object>> findAllRoleTree(){
        List<Role> roles = findAll();
        return buildTree(roles);
    }

    @Override
    public List<Role> getBaseRole(Map<String, Object> paramMap) {
        return roleDao.getBaseRole(paramMap);
    }

    @Override
    public Map<String, Boolean> checkIsRoleExists(String name){
        Map<String, Boolean> result = new HashMap<String, Boolean>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name", name);
        List<Role> roles = getBaseRole(paramMap);
        if(roles.size()>0){
            result.put("isExists", true);
        }else {
            result.put("isExists", false);
        }
        return result;
    }

    public List<Map<String, Object>> buildTree(List<Role> roles){
        List<Map<String, Object>> roleTree = new ArrayList<Map<String, Object>>();

        for(int i=0; i<roles.size(); i++){
            Map<String, Object> roleNode = new HashMap<String, Object>();
            Role role = roles.get(i);
            roleNode.put("id", role.getId());
            roleNode.put("text", role.getName());
            roleNode.put("type", "role");
            roleNode.put("state", "open");
            roleTree.add(roleNode);
        }
        return roleTree;
    }
}
