/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-inoutstock 
 * FileName: SupplierService.java 
 * PackageName: com.glanway.inoutstock.service
 * Date: 2016年5月9日下午3:56:41    
 **/
package com.yuruizhi.hplusdemo.admin.service.provider;

import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.List;
import java.util.Map;

/**      
 * <p>名称:SupplierService </p>
 * <p>说明: 供应商管理的Service</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：lushanshan
 * @date：2016年5月9日下午3:56:54   
 * @version: 1.0
 *
 */
public interface SupplierService extends BaseService<Supplier> {
	
	public List<Supplier> getListByGoodsId(String goodsId);

    public List<Supplier> findManyByGoodsId(String gid);
	
	public List<SupplierDetail> getBySupplierId(String supplierId);
	
	public void saveSupplierGoodsList(Supplier supplier);

	public String getIdByName(String supplierName);

    /**
     * 
     * <p>名称：根据供应商名称获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param supplierName
     * @return
     */
	List<Supplier> findBySupplierName(String supplierName);

	List<Supplier> findBySupplierCode(String supplierCode);

	Map<String, Boolean> checkCodeIsExits(String code);
	
}
