/*
* Copyright (c) 2005, 2014 vacoor
*
* Licensed under the Apache License, Version 2.0 (the "License");
*/
package com.yuruizhi.hplusdemo.admin.service.provider.impI;

import com.yuruizhi.hplusdemo.admin.dao.provider.SupplierGoodsListDao;
import com.yuruizhi.hplusdemo.admin.dto.SupplierGoodsDTO;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierGoodsList;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.provider.SupplierGoodsListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created on 2014-10-30 17:10:16
 *
 * @author crud generated
 */
@Service
@Transactional
public class SupplierGoodsListServiceImpl extends BaseServiceImpl<SupplierGoodsList> implements SupplierGoodsListService {

	@Autowired
	private SupplierGoodsListDao supplierGoodsListDao;
	@Autowired
	public void setSupplierGoodsListDao(SupplierGoodsListDao supplierGoodsListDao){
		super.setBaseDao(supplierGoodsListDao);
	}
	
	@Override
	public void save(SupplierGoodsList supplierGoodsList){
		Map<String, Object> param =new HashMap<String, Object>();
		param.put("goodsCode", supplierGoodsList.getGoodsCode());
		param.put("supplierId", supplierGoodsList.getSupplierId());
		int result =  supplierGoodsListDao.count(param);
		if(result < 1){
			supplierGoodsListDao.save(supplierGoodsList);
		}else {
			supplierGoodsListDao.update(supplierGoodsList);
		}
	}

	@Override
	public List<SupplierGoodsList> getListBySupplierId(String supplierId) {
		return supplierGoodsListDao.getListBySupplierId(supplierId);
	}


	@Override
	public SupplierGoodsList getDetails(String goodsId, String supplierId) {
		return supplierGoodsListDao.getDetails(goodsId, supplierId);
	}

    @Override
    public List<SupplierGoodsDTO> findSupplierGoods(String sid) {
        return supplierGoodsListDao.findSupplierGoods(sid);
    }
	public void savaOrUpdate(SupplierGoodsList supplierGoodsList){
		if(StringUtils.hasLength(String.valueOf(supplierGoodsList.getId()))){
			supplierGoodsListDao.updateGoods(supplierGoodsList);
		}else{
			supplierGoodsListDao.save(supplierGoodsList);
		}
	}

}
