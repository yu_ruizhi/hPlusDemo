/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Parameter</p>
 * <p>说明: 产品参数实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:23:21   
 * @version: 1.0
 */
public class Parameter extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 1283301445251610839L;
	/**
     * @Column
     */
    private String name;
    /**
     * @Column
     */
    private Integer sort;
    /**
     * @Column
     */
    private Boolean deleted = false;

    /**
     * 上级参数
     * @ManyToOne optional=true
     */
    private Parameter parent;
    /**
     * 子参数
     */
    private List<Parameter> children;

    /**
     * 所属模型
     * @ManyToOne
     */
    private Model model;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Parameter getParent() {
        return parent;
    }

    public void setParent(Parameter parent) {
        this.parent = parent;
    }

    public List<Parameter> getChildren() {
        return children;
    }

    public void setChildren(List<Parameter> children) {
        this.children = children;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
