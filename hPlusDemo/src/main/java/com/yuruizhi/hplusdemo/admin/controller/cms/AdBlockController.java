package com.yuruizhi.hplusdemo.admin.controller.cms;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.AdBlock;



/**
 * <p>名称:AdBlockController </p>
 * <p>说明: 广告位管理Controller</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月4日下午2:52:19   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/adBlock")
public class AdBlockController extends AdminBaseController<AdBlock> {

}
