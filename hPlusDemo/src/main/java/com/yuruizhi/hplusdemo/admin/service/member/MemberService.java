/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.member;

import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseService;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * <p>名称: MemberService</p>
 * <p>说明: 会员服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:26:02   
 * @version: 1.0
 */
public interface MemberService extends BaseService<Member> {
	Map<String, Object> login(String mobileOrEmail, String password);

	Page<Member> findPage(Filters filters,  Filters memberLevelFilters,Filters memberUserFilters, Pageable pageable);
	
	/**
	 * 
	 * <p>名称：新增时根据用户名检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	Map<String, Boolean> checkIsNameExists(String name);
	
	/**
	 * 
	 * <p>名称：编辑时根据会员id及用户名检测重名</p> 
	 * <p>描述：TODO</p>
	 * @author：wuqi
	 * @param hasAnnotherName
	 * @return
	 */
	Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName);
	
	/**
	 * 
	 * <p>名称：新增时根据邮箱检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param email
	 * @return
	 */
	Map<String, Boolean> checkIsEmailExists(String email);
	
	/**
	 * 
	 * <p>名称：编辑时根据会员id及邮箱检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param hasAnnotherEmail
	 * @return
	 */
	Map<String, Boolean> hasAnnotherEmail(String[] hasAnnotherEmail);
	
	/**
	 * 
	 * <p>名称：新增时根据手机号检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param phone
	 * @return
	 */
	Map<String, Boolean> checkIsPhoneExists(String phone);
	
	/**
	 * 
	 * <p>名称：编辑时根据会员id及手机检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param hasAnnotherPhone
	 * @return
	 */
	Map<String, Boolean> hasAnnotherPhone(String[] hasAnnotherPhone);
	
	/**
	 * @param member
	 *            用户
	 * @return
	 * @description 验证注册时，填写的邮箱是否已存在
	 */
	Map<String, Boolean> validateEmailExists(Member member);


	Map<String, Object> signUpWithVerifyCode(Member member);

	/**
	 * 通过手机注册账号
	 * 
	 * @param member
	 * @return
	 */
	Map<String, Object> signUpByPhone(Member member);


	/**
	 * 根据手机号码查询用户
	 * 
	 * @param phone
	 *            手机号
	 * @return 用户
	 */
	Member getUserByPhone(String phone);

	Map<String, Boolean> validatePhoneExists(Member member);

	int insert(Member members);

	Member getUserByUserEmail(Member members);
    /**
     * 忘记密码
     * @description (这里用一句话描述这个方法的作用). <br/>
     */
    Member forgetPwd(String email);

	Member selectById(Member member);

	List<Member> findMemberList(Long[] ids);

	List<Member> getMembersBySmsId(Long smsId);

	Page<Member> findMemberPage(Filters filters, Pageable pageable, Long[] memberIds);

	List<Member> getMembersByEmailId(Long emailId);

	/**
	 * 根据会员编号查询会员
	 */
	Member findByCode(String code);

    void updateMember(Member member);

	/**
	 * <p>名称：查询所有的前台用户</p>
	 * <p>描述：</p>
	 * @author：yuruizhi
	 * @return: List<Member>
	 */
	List<Member> findAll();
}
