/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;

/**
 * <p>名称: Favorites</p>
 * <p>说明: 收藏实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:20:29   
 * @version: 1.0
 */
public class Favorites extends BaseEntity {
    private static final long serialVersionUID = 1L;
    
	/**
	 * member:用户对象
	 */
	private Member member;
	/**
	 * goods:货品对象
	 */
	private Goods goods=new Goods();

    /**
     * product:商品对象
     */
    private Product product;

	/**
	 * deleted:是否删除
	 */
	private Boolean deleted;

    public Member getUser() {
        return member;
    }

    public void setUser(Member member) {
        this.member = member;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
