package com.yuruizhi.hplusdemo.admin.dao.ticket;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketRefund;

import java.util.Map;


/**
 * Project Name：
 * Description：
 * Created By：      Argevolle
 * Created Date：    2014/12/3 10:44
 * Modified By：
 * Modified Date：
 * Modified Remark：
 * Version：         v 0.1
 */
public interface TicketRefundDao extends BaseDao<TicketRefund> {

   void updateType(Map<String,Object> map);
   
}