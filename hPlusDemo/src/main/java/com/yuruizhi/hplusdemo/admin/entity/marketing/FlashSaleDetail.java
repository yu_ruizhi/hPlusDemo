/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;

import java.math.BigDecimal;

/**
 * <p>名称: FlashSaleDetail</p>
 * <p>说明: 限时抢购明细</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:05:02   
 * @version: 1.0
 */
public class FlashSaleDetail extends BaseEntity {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = -4933992107349133342L;

	/**
     * 活动ID
     *
     * @ViewField editor=select internal="flashSale:name"
     * @ManyToOne joinColumn=PROMOTION_ID optional=true
     */
    @NotNull
    private FlashSale flashSale;

    /**
     * 商品ID
     *
     * @ViewField editor=spinner
     * @Column GOODS_ID
     */
    @NotNull
    private Goods goods;

    /**
     * 产品ID
     *
     * @ViewField editor=spinner
     * @Column PRODUCT_ID
     */
    @NotNull
    private Product product;

    /**
     * 折扣
     *
     * @ViewField editor=input
     * @Column DISCOUNT
     */
    @Size(max = 8)
    @Digits(integer = 8, fraction = 2)
    private BigDecimal discount;

    /**
     * 减价
     *
     * @ViewField editor=input
     * @Column DESCEND
     */
    @Size(max = 8)
    @Digits(integer = 8, fraction = 2)
    private BigDecimal descend;

    /**
     * 排序
     *
     * @ViewField editor=spinner
     * @Column POSITION
     */
    @NotNull
    private Integer position;

    /**
     * 是否删除
     *
     * @ViewField editor=input
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted;

    public FlashSale getFlashSale() {
        return this.flashSale;
    }

    public void setFlashSale(FlashSale flashSale) {
        this.flashSale = flashSale;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getDiscount() {
        return this.discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDescend() {
        return this.descend;
    }

    public void setDescend(BigDecimal descend) {
        this.descend = descend;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
