/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces
 * FileName: Activity.java
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.ticket;



import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 票务表
 */
public class Ticket extends BaseEntity {

	private String tkName;

	private String tkDesc;

	private Boolean isRefund;

	private Boolean isPcShow;

	private Boolean isAppShow;

	private BigDecimal price;

	private Date startTime;

	private Date endTime;

	private Integer qty;

	private String img1;

	private String img2;

	private String img3;

	private String img4;

	private String img5;

	private String  unionCode;

	private Boolean deleted;

	private List<TicketSku> ticketSkus;

	private Boolean isstale;//是否过期，表里没有，用于验证商品是否可以购买

	public Boolean getIsstale() {
		return isstale;
	}

	public void setIsstale(Boolean isstale) {
		this.isstale = isstale;
	}

	public List<TicketSku> getTicketSkus() {
		return ticketSkus;
	}

	public void setTicketSkus(List<TicketSku> ticketSkus) {
		this.ticketSkus = ticketSkus;
	}

	public String getTkName() {
		return tkName;
	}

	public void setTkName(String tkName) {
		this.tkName = tkName;
	}

	public String getTkDesc() {
		return tkDesc;
	}

	public void setTkDesc(String tkDesc) {
		this.tkDesc = tkDesc;
	}

	public Boolean getRefund() {
		return isRefund;
	}

	public void setRefund(Boolean refund) {
		isRefund = refund;
	}

	public Boolean getPcShow() {
		return isPcShow;
	}

	public void setPcShow(Boolean pcShow) {
		isPcShow = pcShow;
	}

	public Boolean getAppShow() {
		return isAppShow;
	}

	public void setAppShow(Boolean appShow) {
		isAppShow = appShow;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}

	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}

	public String getImg3() {
		return img3;
	}

	public void setImg3(String img3) {
		this.img3 = img3;
	}

	public String getImg4() {
		return img4;
	}

	public void setImg4(String img4) {
		this.img4 = img4;
	}

	public String getImg5() {
		return img5;
	}

	public void setImg5(String img5) {
		this.img5 = img5;
	}

	public String getUnionCode() {
		return unionCode;
	}

	public void setUnionCode(String unionCode) {
		this.unionCode = unionCode;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
}
