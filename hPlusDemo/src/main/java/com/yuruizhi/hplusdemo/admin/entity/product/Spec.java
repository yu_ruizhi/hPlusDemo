/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Spec</p>
 * <p>说明: 产品规格实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:25:26   
 * @version: 1.0
 */
public class Spec extends BaseEntity {
	/**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -1282244090112240998L;


	/**
     * @Column
     */
    private String name;
    /**
     * @Column
     */
    private String alias;
    /**
     * @Column
     */
    private Integer sort;
    /**
     * @OneToMany fetch=true optional=true
     */
    private List<SpecValue> specValues;
    /**
     * @Column
     */
    private Boolean deleted = false;
    
    /**
     * 该规格下的规格值的类型
     */
    private Integer displayType;

    private Boolean istemporary;//是否临时规格

    public Boolean getIstemporary() {
        return istemporary;
    }

    public void setIstemporary(Boolean istemporary) {
        this.istemporary = istemporary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<SpecValue> getSpecValues() {
        return specValues;
    }

    public void setSpecValues(List<SpecValue> specValues) {
        this.specValues = specValues;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public Integer getDisplayType() {
		return displayType;
	}

	public void setDisplayType(Integer displayType) {
		this.displayType = displayType;
	}
    
}
