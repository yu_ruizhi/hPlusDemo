/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import com.yuruizhi.hplusdemo.admin.entity.product.ProductAttributeValue;

/**
 * <p>名称: ProductAttributeValueService</p>
 * <p>说明: 产品属性值服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:07:06   
 * @version: 1.0
 */
public interface ProductAttributeValueService {
    void insert(ProductAttributeValue pav);
    ProductAttributeValue getAttributeValueByProductIdAndAttributeId(ProductAttributeValue pav);
}
