/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.cms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.yuruizhi.hplusdemo.admin.entity.TreeNodeBaseEntity;

/**
 * <p>名称: Link</p>
 * <p>说明: 网站链接，导航等实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:43:51   
 * @version: 1.0
 */
public class Link extends TreeNodeBaseEntity<Link> {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 2572597991726966108L;

	/**
     * 导航编码
     *
     * @ViewField editor=input
     * @Column CODE
     */
    @Size(max = 255)
    private String code;

    /**
     * 导航图标
     *
     * @ViewField editor=input
     * @Column ICON
     */
    @Size(max = 255)
    private String icon;

    /**
     * 名称
     *
     * @ViewField editor=input
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 跳转链接
     *
     * @ViewField editor=input
     * @Column HREF
     */
    @Size(max = 255)
    private String href;

    /**
     * 打开方式
     *
     * @ViewField editor=input
     * @Column TARGET
     */
    @Size(max = 255)
    private String target;

    /**
     * 导航描述
     *
     * @ViewField editor=input
     * @Column NOTE
     */
    @Size(max = 255)
    private String note;

    /**
     * 排序
     *
     * @ViewField editor=spinner
     * @Column POSITION
     */
    @NotNull
    private Integer position;

    /**
     * 是否可见
     *
     * @ViewField editor=input
     * @Column VISIBLE
     */
    private Boolean visible = Boolean.TRUE;

    /**
     * 最大层级
     *
     * @ViewField editor=spinner
     * @Column MAX_DEPTH
     */
    private Integer maxDepth;

    /**
     * 是否预定义
     *
     * @ViewField editor=input
     * @Column PREDEFINED
     */
    private Boolean predefined = Boolean.FALSE;

    /**
     * 是否删除
     *
     * @ViewField editor=input
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE;


    /**
     * 上级导航
     *
     * @ViewField editor=treepicker internal="id:name"
     * @ManyToOne joinColumn=PARENT_ID optional=true
     */
    @Override
    public Link getParent() {
        return super.getParent();
    }

    @Override
    public void setParent(Link parent) {
        super.setParent(parent);
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTarget() {
        return this.target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getVisible() {
        return this.visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Integer getMaxDepth() {
        return this.maxDepth;
    }

    public void setMaxDepth(Integer maxDepth) {
        this.maxDepth = maxDepth;
    }

    public Boolean getPredefined() {
        return this.predefined;
    }

    public void setPredefined(Boolean predefined) {
        this.predefined = predefined;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
