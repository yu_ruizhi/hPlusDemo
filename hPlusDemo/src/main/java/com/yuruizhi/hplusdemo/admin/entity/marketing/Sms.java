/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.*;

/**
 * <p>名称: Sms</p>
 * <p>说明: 短信营销实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:07:16   
 * @version: 1.0
 */
public class Sms extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5349561446148227155L;

	/**
     * 短信主题
     * 邮件主题
     * 
     * @ViewField editor=input 
     * @Column SUBJECT
     */
    @NotNull
    @Size(max = 255)
    private String subject;

    /**
     * 短信内容
     * 邮件内容
     * 
     * @ViewField editor=input 
     * @Column CONTENT
     */
    @NotNull
    @Size(max = 500)
    private String content;

    /**
     * 定时发送时间
     * 
     * @ViewField editor=datepicker 
     * @Column SCHEDULE_TIME
     */
    private Date scheduleTime;

    /**
     * 批次大小
     * 
     * @ViewField editor=spinner 
     * @Column BATCH_SIZE
     */
    private Integer batchSize;

    /**
     * 发送状态
     * 
     * @ViewField editor=spinner 
     * @Column STATUS
     */
    @NotNull
    private Integer status;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;
    
    
    //memberIds会员id
    private String memberIds;

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getScheduleTime() {
        return this.scheduleTime;
    }

    public void setScheduleTime(Date scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public Integer getBatchSize() {
        return this.batchSize;
    }

    public void setBatchSize(Integer batchSize) {
        this.batchSize = batchSize;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public String getMemberIds() {
		return memberIds;
	}

	public void setMemberIds(String memberIds) {
		this.memberIds = memberIds;
	}

}
