
package com.yuruizhi.hplusdemo.admin.dao.ticket;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;

import com.yuruizhi.hplusdemo.admin.entity.ticket.Ticket;


public interface TicketDao extends BaseDao<Ticket>  {
	
   
}