/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MemberLevelController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.member
 * Date: 2016年4月26日 上午11:49:25 
 **/
package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsCheck;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsCheckService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductService;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 
 * <p>名称: 商品核价Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月26日 上午11:49:25 
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/goodsCheck")
public class GoodsCheckController extends AdminBaseController<GoodsCheck>{

	private static final Logger logger = Logger.getLogger(GoodsCheckController.class);
	
	@Autowired
	private GoodsCheckService goodsCheckService;

	@Autowired
	private GoodsService goodsService;
    
    @Autowired
    private SysLogService sysLogService;

    @Autowired
    private ProductService productService;
	
	/**
	 * 
	 * <p>名称：删除数据</p> 
	 * <p>描述：根据id删除选择的核价商品数据</p>
	 * @author：wuqi
	 * @param ids 核价商品id
	 * @return
	 */
    @Override
	@ResponseBody
	@RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		goodsCheckService.delete(id);
		
		String ids = "";
		for (int i = 0; i < id.length; i++) {
			ids = id[i]+",";
		}
		//系统日志
		ids = ids.substring(0, ids.length()-1);
		String operation = "核价管理-删除-id:"+ids;
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "核价管理");
		return ImmutableMap.<String, Object>of("success", true);
	}
	
	/**
	 * 
	 * <p>名称：异步获取核价单品数据</p> 
	 * <p>描述：异步获取需要在核价商品列表页面显示的数据</p>
	 * @author：wuqi
	 * @param filters 核价商品
	 * @param goodsFilters 单品
	 * @param productFilters 商品
	 * @param categoryFilters 分类
	 * @param brandFilters 品牌
	 * @param worksFilters 作品
	 * @param pageable 初始页面
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getList")
	public Page<GoodsCheck> getList(@Qualifier("TGC.")Filters filters,
								@Qualifier("TG.")Filters goodsFilters,
								@Qualifier("TP.")Filters productFilters,
								@Qualifier("TC.")Filters categoryFilters,
								@Qualifier("TB.")Filters brandFilters,
								@Qualifier("TW.")Filters worksFilters,Pageable pageable,
								@RequestParam(required = false) String spotSell){

		if(!filters.iterator().hasNext() && !goodsFilters.iterator().hasNext() && !productFilters.iterator().hasNext()
			&& !categoryFilters.iterator().hasNext() && !brandFilters.iterator().hasNext() && !worksFilters.iterator().hasNext()
			&& null == spotSell ){
			filters.eq("checked", 0);
		}
		return goodsCheckService.findPage(filters, goodsFilters, productFilters, categoryFilters, brandFilters, worksFilters, pageable,spotSell);
	}
	
	/**
	 * 
	 * <p>名称：修改所有待核价商品的数据</p> 
	 * <p>描述：根据修改类型修改所有待核价商品的数据</p>
	 * @author：wuqi
	 * @param goodsCheck 核价单品数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping("updateInfo")
	public Map<String,Object> updateLogisticFee(GoodsCheck goodsCheck) {
		Map<String,Object> param = Maps.newHashMap();
		param.put("goodsChecked", 0);
		//获取待核价数据
 		List<GoodsCheck> goodsCheckList = goodsCheckService.findMany(param);
 		
 		//消息提示
 		String remind = "";
 		
		for (GoodsCheck goodsChecks : goodsCheckList) {
			String operation = "核价管理-修改-id:"+goodsChecks.getId();
			//新物流单价
			BigDecimal newLogisticPrice = goodsCheck.getLogisticPrice();
			//旧物流单价
			BigDecimal oldLogisticPrice = goodsChecks.getLogisticPrice();
			//旧重量
			BigDecimal oldWeight = BigDecimal.valueOf(goodsChecks.getGoods().getWeight() == null ? 0 : goodsChecks.getGoods().getWeight());
			//旧RMB成本价
			BigDecimal oldRmbCostPrice = goodsChecks.getRmbCostPrice();
			//旧经销商毛利率
			BigDecimal oldSellGrossMargin = goodsChecks.getSellGrossMargin();
			//旧经销商价格
			BigDecimal oldSellPrice = goodsChecks.getSellPrice();
			//新经销商毛利率
			BigDecimal newSellGrossMargin = goodsCheck.getSellGrossMargin();
			//旧零售毛利率
			BigDecimal oldRetailGrossMargin = goodsChecks.getRetailGrossMargin();
			//旧零售价格
			BigDecimal oldRetailPrice = goodsChecks.getRetailPrice();
			//新零售毛利率
			BigDecimal newRetailGrossMargin = goodsCheck.getRetailGrossMargin();
			//旧淘宝毛利率
			BigDecimal oldTaobaoGrossMargin = goodsChecks.getTaobaoGrossMargin();
			//旧淘宝价格
			BigDecimal oldTaobaoPrice = goodsChecks.getTaobaoPrice();
			//新淘宝毛利率
			BigDecimal newTaobaoGrossMargin = goodsCheck.getTaobaoGrossMargin();
			//旧未成箱加点
			BigDecimal oldBoxPoint = new BigDecimal(0);    //BigDecimal oldBoxPoint = goodsChecks.getBoxPoint();
			//旧成箱价格
			BigDecimal oldBoxPrice = goodsChecks.getBoxPrice();
			
			//旧到岸价格
			BigDecimal oldLandedGoodsCost = goodsChecks.getLandedGoodsCost();
			//旧物流费
			BigDecimal oldLogisticFee = goodsChecks.getLogisticFee();
			//新汇率
			BigDecimal newRate = goodsCheck.getRate();
			//旧汇率
			BigDecimal oldRate = goodsChecks.getRate();
			//旧日元成本价
			BigDecimal oldProductPrice = goodsChecks.getGoods().getPrice();
			
			//修改物流费及到岸价
			if (null != newLogisticPrice && !(newLogisticPrice.equals(oldLogisticPrice))) {
				//物流费 = 单价 * 重量
				BigDecimal logisticFee = goodsCheckService.getLogisticFee(newLogisticPrice, oldWeight);
				operation += ",物流费:由"+goodsCheckService.isNull(oldLogisticFee)+"修改为"+Decimal(logisticFee);
				goodsChecks.setLogisticPrice(newLogisticPrice);
				goodsChecks.setLogisticFee(logisticFee);
				//因物流费修改导致其他价格的修改
				BigDecimal sellPrice = goodsCheckService.getSellPrice(oldRmbCostPrice, oldSellGrossMargin, oldBoxPoint, logisticFee);
				BigDecimal boxPrice = goodsCheckService.getBoxPrice(oldRmbCostPrice, oldSellGrossMargin, logisticFee);
				BigDecimal retailPrice = goodsCheckService.getRetailPrice(oldRmbCostPrice, oldRetailGrossMargin, logisticFee);
				BigDecimal taobaoPrice = goodsCheckService.getTaobaoPrice(oldRmbCostPrice, oldTaobaoGrossMargin, logisticFee);
				operation += ",经销商价格:由"+goodsCheckService.isNull(oldSellPrice)+"修改为"+Decimal(sellPrice);
				operation += ",成箱价格:由"+goodsCheckService.isNull(oldBoxPrice)+"修改为"+Decimal(boxPrice);
				operation += ",零售销售价格:由"+goodsCheckService.isNull(oldRetailGrossMargin)+"修改为"+Decimal(retailPrice);
				operation += ",淘宝销售价格:由"+goodsCheckService.isNull(oldTaobaoPrice)+"修改为"+Decimal(taobaoPrice);
				goodsChecks.setSellPrice(sellPrice);
				goodsChecks.setBoxPrice(boxPrice);
				goodsChecks.setRetailPrice(retailPrice);
				goodsChecks.setTaobaoPrice(taobaoPrice);
				//判断是否有人民币成本价计算到岸价
				if (null != oldRmbCostPrice){
					BigDecimal landedGoodsCost = goodsCheckService.getLandedGoodsCost(logisticFee, oldRmbCostPrice);
					operation += ",到岸价:由"+goodsCheckService.isNull(oldLandedGoodsCost)+"修改为"+Decimal(landedGoodsCost);
					goodsChecks.setLandedGoodsCost(landedGoodsCost);
				}
				remind += "商品编号"+goodsChecks.getGoods().getCode()+",修改成功。<br/>";
			}
			
			//修改RMB成本价及到岸价
			if (null != newRate && !(newRate.equals(oldRate))) {
				//RMB成本价 = 日元成本价 / 汇率
				BigDecimal rmbCostPrice = goodsCheckService.getRmbCostPrice(oldProductPrice, newRate);
				operation += ",RMB成本价:由"+goodsCheckService.isNull(oldRmbCostPrice)+"修改为"+Decimal(rmbCostPrice);
				goodsChecks.setRate(newRate);
				goodsChecks.setRmbCostPrice(rmbCostPrice);
				//因RMB成本价导致其他价格的修改
				BigDecimal sellPrice = goodsCheckService.getSellPrice(rmbCostPrice, oldSellGrossMargin, oldBoxPoint, oldLogisticFee);
				BigDecimal boxPrice = goodsCheckService.getBoxPrice(rmbCostPrice, oldSellGrossMargin, oldLogisticFee);
				BigDecimal retailPrice = goodsCheckService.getRetailPrice(rmbCostPrice, oldRetailGrossMargin, oldLogisticFee);
				BigDecimal taobaoPrice = goodsCheckService.getTaobaoPrice(rmbCostPrice, oldTaobaoGrossMargin, oldLogisticFee);
				operation += ",经销商价格:由"+goodsCheckService.isNull(oldSellPrice)+"修改为"+Decimal(sellPrice);
				operation += ",成箱价格:由"+goodsCheckService.isNull(oldBoxPrice)+"修改为"+Decimal(boxPrice);
				operation += ",零售销售价格:由"+goodsCheckService.isNull(oldRetailPrice)+"修改为"+Decimal(retailPrice);
				operation += ",淘宝销售价格:由"+goodsCheckService.isNull(oldTaobaoPrice)+"修改为"+Decimal(taobaoPrice);
				goodsChecks.setSellPrice(sellPrice);
				goodsChecks.setBoxPrice(boxPrice);
				goodsChecks.setRetailPrice(retailPrice);
				goodsChecks.setTaobaoPrice(taobaoPrice);
				//判断是否有物流价计算到岸价
				if (null != oldLogisticFee) {
					BigDecimal landedGoodsCost = goodsCheckService.getLandedGoodsCost(oldLogisticFee, rmbCostPrice);
					operation += ",到岸价:由"+goodsCheckService.isNull(oldLandedGoodsCost)+"修改为"+Decimal(landedGoodsCost);
					goodsChecks.setLandedGoodsCost(landedGoodsCost);
				}
				remind += "商品编号"+goodsChecks.getGoods().getCode()+",修改成功。<br/>";
			}
			
			//修改经销商价格及成箱价
			if (null != newSellGrossMargin && !(newSellGrossMargin.equals(oldSellGrossMargin))) {
				//经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用
				//成箱价 = RMB 成本价 / (1 - 经销毛利率) + 物流费用
				if (1 != BigDecimal.valueOf(1).compareTo(newSellGrossMargin.add(goodsCheckService.isNull(oldBoxPoint)))) {
					remind += "商品编号"+goodsChecks.getGoods().getCode()+",修改经销商价格计算失败。<br/>";
				} else {
					BigDecimal sellPrice = goodsCheckService.getSellPrice(oldRmbCostPrice, newSellGrossMargin, oldBoxPoint, oldLogisticFee);
					sellPrice = sellPrice.setScale(1,BigDecimal.ROUND_HALF_UP);
					BigDecimal boxPrice = goodsCheckService.getBoxPrice(oldRmbCostPrice, newSellGrossMargin, oldLogisticFee);
					operation += ",经销商价格:由"+goodsCheckService.isNull(oldSellPrice)+"修改为"+Decimal(sellPrice);
					operation += ",成箱价:由"+goodsCheckService.isNull(oldBoxPrice)+"修改为"+Decimal(boxPrice);
					goodsChecks.setSellGrossMargin(newSellGrossMargin);
					goodsChecks.setBoxPrice(boxPrice);
					goodsChecks.setSellPrice(sellPrice);
					remind += "商品编号"+goodsChecks.getGoods().getCode()+",修改成功。<br/>";
				}
			}
			
			//修改零售销售价格
			if (null != newRetailGrossMargin && !(newRetailGrossMargin.equals(oldRetailGrossMargin))) {
				//零售销售价格 = RMB 成本价 / (1-零售毛利率) + 物流费用
				BigDecimal retailPrice = goodsCheckService.getRetailPrice(oldRmbCostPrice, newRetailGrossMargin, oldLogisticFee);
				retailPrice = retailPrice.setScale(0, BigDecimal.ROUND_HALF_UP);
				operation += ",零售销售价格:由"+goodsCheckService.isNull(oldRetailPrice)+"修改为"+Decimal(retailPrice);
				goodsChecks.setRetailGrossMargin(newRetailGrossMargin);
				goodsChecks.setRetailPrice(retailPrice);
				remind += "商品编号"+goodsChecks.getGoods().getCode()+",修改成功。<br/>";
			}
			
			//修改淘宝销售价格
			if (null != newTaobaoGrossMargin && !(newTaobaoGrossMargin.equals(oldTaobaoGrossMargin))) {
				//淘宝销售价格 = RMB 成本价 / (1-淘宝毛利率) + 物流费用
				BigDecimal taobaoPrice = goodsCheckService.getTaobaoPrice(oldRmbCostPrice,newTaobaoGrossMargin, oldLogisticFee);
				operation += ",淘宝销售价格:由"+goodsCheckService.isNull(oldTaobaoPrice)+"修改为"+Decimal(taobaoPrice);
				goodsChecks.setTaobaoGrossMargin(newTaobaoGrossMargin);
				goodsChecks.setTaobaoPrice(taobaoPrice);	
				remind += "商品编号"+goodsChecks.getGoods().getCode()+",修改成功。<br/>";
			}
			
			Boolean checked = isGoodsCheck(goodsChecks);
			
			if (true == checked) {
				operation += ",核价已通过";
			}
			goodsChecks.setChecked(checked);
			goodsCheckService.update(goodsChecks);
			sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "核价管理");
			
		}
		if (("").equals(remind)) {
			remind += "本次没有商品进行修改";
		}
		Map<String,Object> map = Maps.newHashMap();
		map.put("success", true);
		map.put("remind", remind);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：核价商品上下架操作</p> 
	 * <p>描述：根据传入状态判断上下架操作</p>
	 * @author：wuqi
	 * @param ids 单品id
	 * @param status 上下架状态
	 * @return
	 */
	@ResponseBody
	@RequestMapping("updateOnSell")
	public Map<String, Object> updateOnSell(@RequestParam("id") Long[] ids,Boolean status){
		Map<String,Object> map = Maps.newHashMap();
		String reminder = "";
		String operation = "";
		int goodsCount = 1;
		Date today = new Date();
		for (Long id : ids) {
			GoodsCheck goodsCheck = goodsCheckService.find(id);
			//是否上下架
			if (null != goodsCheck) {
				if(true == status) {
					if (true == goodsCheck.getChecked()){
						Goods goods = goodsService.find(goodsCheck.getGoodsId());
						//update 20160602 zuoyang 更新为上架时更新上架时间
						goods.setOnSellTime(today);
						goods.setOnSell(status);
                        //update 20160628 lushanshan 判断商品是否有品牌。分类及作品
                        map.put("id",goods.getProductId());
                        Product product=productService.findOne(map);
                        
                        if(product !=null){
                        	
                            if(null == product.getWorks() || null == product.getWorks().getId()){
                            	reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+"上架失败-作品未填写<br/>";
                                goodsCount ++ ;
                            } else if(product.getBrand() == null || product.getBrand().getId()==null){
                                reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+"上架失败-品牌未填写<br/>";
                                goodsCount ++ ;
                            } else if(product.getCategory() == null || product.getCategory().getId()==null){
                            	//update 20160602 zuoyang 添加上架失败提示信息
                                reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+"上架失败-类目未填写<br/>";
                                goodsCount ++ ;
                            } else{
                            	goodsService.update(goods);
                            	reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+"上架成功<br/>";
                                operation += "核价管理-编辑-id:"+goodsCheck.getId()+"修改为上架";
                                goodsCount ++ ;
                            }
                        } else {
                        	//update 20160602 zuoyang 添加上架失败提示信息
                            reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+"上架失败-商品的品牌、分类、作品未填写<br/>";
                            goodsCount ++ ;
                        }
					} else {
						 //update 20160602 zuoyang 添加上架失败提示信息
						reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+
								"的商品因为未核价完成导致上架失败,请按照下面的信息重新核价-"+getUncheckReason(goodsCheck)+"<br/>";         
						goodsCount ++ ;
                }
				} else {
					Goods goods = goodsService.find(goodsCheck.getGoodsId());
					goods.setOnSell(status);
					goodsService.update(goods);
					operation += "核价管理-编辑-id:"+goodsCheck.getId()+"修改为下架";
					reminder += goodsCount + ". 商品编号:"+goodsCheck.getGoods().getCode()+"下架成功<br/>";
					goodsCount ++ ;
				}
			}
		}

		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "核价管理");
		map.put("reminder", reminder);
		map.put("success",true);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：重新核价</p> 
	 * <p>描述：对选择核价数据进行重新核价</p>
	 * @author：wuqi
	 * @param goodsCheck 选择中的核价数据
	 */
	@ResponseBody
	@RequestMapping("savePrice")
	public void savePrice(GoodsCheck goodsCheck){
		
        GoodsCheck oldGoodsCheck = goodsCheckService.find(goodsCheck.getId());
		
		String operation = "核价管理-修改-id:"+goodsCheck.getId();
		//旧RMB成本价
		BigDecimal oldRmbCostPrice = oldGoodsCheck.getRmbCostPrice();
		//旧经销商毛利率
		BigDecimal oldSellGrossMargin = goodsCheckService.isNull(oldGoodsCheck.getSellGrossMargin());
		
		//新经销商毛利率
		BigDecimal newSellGrossMargin = goodsCheck.getSellGrossMargin();
		//旧零售毛利率
		BigDecimal oldRetailGrossMargin = goodsCheckService.isNull(oldGoodsCheck.getRetailGrossMargin());
		//新零售毛利率
		BigDecimal newRetailGrossMargin = goodsCheck.getRetailGrossMargin();
		//旧淘宝毛利率
		BigDecimal oldTaobaoGrossMargin = goodsCheckService.isNull(oldGoodsCheck.getTaobaoGrossMargin());
		//新淘宝毛利率
		BigDecimal newTaobaoGrossMargin = goodsCheck.getTaobaoGrossMargin();
		//旧未成箱加点
		BigDecimal oldBoxPoint = goodsCheckService.isNull(oldGoodsCheck.getBoxPoint()); 
		//旧经销商价格
		BigDecimal oldSellPrice = oldGoodsCheck.getSellPrice();
		//旧零售价格
		BigDecimal oldRetailPrice = oldGoodsCheck.getRetailPrice();
		//旧淘宝价格
		BigDecimal oldTaobaoPrice = oldGoodsCheck.getTaobaoPrice();
		//旧成箱价格
		BigDecimal oldBoxPrice = goodsCheckService.isNull(oldGoodsCheck.getBoxPrice());
		//新成箱价格
		BigDecimal newBoxPoint = new BigDecimal(0);	//BigDecimal newBoxPoint = goodsCheck.getBoxPoint();
		//旧物流费
		BigDecimal oldLogisticFee = oldGoodsCheck.getLogisticFee();
		
		//修改经销毛利率
		if (null != newSellGrossMargin && !(newSellGrossMargin.setScale(2, BigDecimal.ROUND_HALF_UP)).equals(oldSellGrossMargin.setScale(2, BigDecimal.ROUND_HALF_UP))){
			//经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用
			//成箱价 = RMB 成本价 / (1 - 经销毛利率) + 物流费用
			BigDecimal sellPrice = goodsCheckService.getSellPrice(oldRmbCostPrice, newSellGrossMargin, oldBoxPoint, oldLogisticFee);
			sellPrice = sellPrice.setScale(1, BigDecimal.ROUND_HALF_UP);
			BigDecimal boxPrice = goodsCheckService.getBoxPrice(oldRmbCostPrice, newSellGrossMargin, oldLogisticFee);
			operation += ",经销商价格:由"+goodsCheckService.isNull(oldSellPrice)+"修改为"+Decimal(sellPrice);
			operation += ",成箱价格:由"+goodsCheckService.isNull(oldBoxPrice)+"修改为"+Decimal(boxPrice);
			oldGoodsCheck.setSellGrossMargin(newSellGrossMargin);
			oldGoodsCheck.setBoxPrice(boxPrice);
			oldGoodsCheck.setSellPrice(sellPrice);
		} 
		
		//修改未成箱加点  （现在未成箱价格为0）
		if (newBoxPoint.equals(new BigDecimal(0)) || (null != newBoxPoint && !(newBoxPoint.setScale(2, BigDecimal.ROUND_HALF_UP).equals(oldBoxPoint.setScale(2, BigDecimal.ROUND_HALF_UP))))) {
			//经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用
			BigDecimal sellPrice = null;
			if (null != newSellGrossMargin && !(newSellGrossMargin.setScale(2, BigDecimal.ROUND_HALF_UP)).equals(oldSellGrossMargin.setScale(2, BigDecimal.ROUND_HALF_UP))){
				sellPrice = goodsCheckService.getSellPrice(oldRmbCostPrice, newSellGrossMargin, newBoxPoint, oldLogisticFee);
			}else{
				sellPrice = goodsCheckService.getSellPrice(oldRmbCostPrice, oldSellGrossMargin, newBoxPoint, oldLogisticFee);
			}
			sellPrice = sellPrice.setScale(1, BigDecimal.ROUND_HALF_UP);
			operation += ",经销商价格:由"+goodsCheckService.isNull(oldSellPrice)+"修改为"+Decimal(sellPrice);
			oldGoodsCheck.setSellPrice(sellPrice);
			oldGoodsCheck.setBoxPoint(newBoxPoint);
		}
		
		//修改零售毛利率
		if (null != newRetailGrossMargin && !(newRetailGrossMargin.equals(oldRetailGrossMargin))) {
			//零售销售价格 = RMB 成本价 / (1-零售毛利率) + 物流费用
			BigDecimal retailPrice = goodsCheckService.getRetailPrice(oldRmbCostPrice, newRetailGrossMargin, oldLogisticFee);
			retailPrice = retailPrice.setScale(0, BigDecimal.ROUND_HALF_UP);
			operation += ",零售销售价格:由"+goodsCheckService.isNull(oldRetailPrice)+"修改为"+Decimal(retailPrice);
			oldGoodsCheck.setRetailGrossMargin(newRetailGrossMargin);
			oldGoodsCheck.setRetailPrice(retailPrice);
		}
		
		//修改淘宝毛利率
		if (null != newTaobaoGrossMargin && !(newTaobaoGrossMargin.equals(oldTaobaoGrossMargin))) {
			//淘宝销售价格 = RMB 成本价 / (1-淘宝毛利率) + 物流费用
			BigDecimal taobaoPrice = goodsCheckService.getTaobaoPrice(oldRmbCostPrice, newTaobaoGrossMargin, oldLogisticFee);
			operation += ",淘宝销售价格:由"+goodsCheckService.isNull(oldTaobaoPrice)+"修改为"+Decimal(taobaoPrice);
			oldGoodsCheck.setTaobaoGrossMargin(newTaobaoGrossMargin);
			oldGoodsCheck.setTaobaoPrice(taobaoPrice);		
		}
		
		//Boolean checked = isGoodsCheck(oldGoodsCheck);
		Boolean checked = true;
		if (true == checked) {
			operation += ",核价已通过";
		}
		oldGoodsCheck.setChecked(checked);
		goodsCheckService.update(oldGoodsCheck);
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "核价管理");
	}
	
	/**
	 * 
	 * <p>名称：批量核价</p> 
	 * <p>描述：对选择核价数据进行批量核价</p>
	 * @author：zuoyang
	 * @param ids 选择中的核价数据
	 * @param goodsCheck 计算所需要的参数
	 */
	@ResponseBody
	@RequestMapping("saveMoreGoodsCheckPrice")
	public String saveMoreGoodsCheckPrice(Long[] ids,GoodsCheck goodsCheck){
		
		if(null != ids && ids.length > 0){
			for(Long id : ids){
				saveOneGoodsCheckPrice(id,goodsCheck);
			}
		}
		
		return "success";
	}
	
	/**
	 * 
	 * <p>名称：更新单个核价单品信息</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param goodsCheckId
	 * @param goodsCheck
	 */
	private String saveOneGoodsCheckPrice(Long goodsCheckId ,GoodsCheck goodsCheck ){
		String remind = "" ;
		GoodsCheck oldGoodsCheck = goodsCheckService.find(goodsCheckId);
		
		String operation = "核价管理-修改-id:"+oldGoodsCheck.getId();
		//新物流单价
		BigDecimal newLogisticPrice = goodsCheck.getLogisticPrice();
		//旧物流单价
		BigDecimal oldLogisticPrice = oldGoodsCheck.getLogisticPrice();
		//旧重量
		BigDecimal oldWeight = BigDecimal.valueOf(oldGoodsCheck.getGoods().getWeight() == null ? 0 : oldGoodsCheck.getGoods().getWeight());
		//旧RMB成本价
		BigDecimal oldRmbCostPrice = oldGoodsCheck.getRmbCostPrice();
		//新RMB成本价
		BigDecimal newRmbCostPrice = null;
		//旧经销商毛利率
		BigDecimal oldSellGrossMargin = oldGoodsCheck.getSellGrossMargin();
		//旧经销商价格
		BigDecimal oldSellPrice = oldGoodsCheck.getSellPrice();
		//新经销商价格
		BigDecimal newSellPrice = null;
		//新经销商毛利率
		BigDecimal newSellGrossMargin = goodsCheck.getSellGrossMargin();
		//旧零售毛利率
		BigDecimal oldRetailGrossMargin = oldGoodsCheck.getRetailGrossMargin();
		//旧零售价格
		BigDecimal oldRetailPrice = oldGoodsCheck.getRetailPrice();
		//新零售价格
		BigDecimal newRetailPrice = null;
		//新零售毛利率
		BigDecimal newRetailGrossMargin = goodsCheck.getRetailGrossMargin();
		//旧淘宝毛利率
		BigDecimal oldTaobaoGrossMargin = oldGoodsCheck.getTaobaoGrossMargin();
		//旧淘宝价格
		BigDecimal oldTaobaoPrice = oldGoodsCheck.getTaobaoPrice();
		//旧淘宝价格
		BigDecimal newTaobaoPrice = null;
		//新淘宝毛利率
		BigDecimal newTaobaoGrossMargin = goodsCheck.getTaobaoGrossMargin();
		//旧未成箱加点
		BigDecimal oldBoxPoint = oldGoodsCheck.getBoxPoint(); //BigDecimal oldBoxPoint = oldGoodsCheck.getBoxPoint();
		//新未成箱加点
		BigDecimal newBoxPoint = new BigDecimal(0);	//BigDecimal newBoxPoint = goodsCheck.getBoxPoint();
		//旧成箱价格
		BigDecimal oldBoxPrice = goodsCheck.getBoxPrice();
		//新成箱价格
		BigDecimal newBoxPrice = null;
		//旧物流费
		BigDecimal oldLogisticFee = oldGoodsCheck.getLogisticFee();
		//新物流费
		BigDecimal newLogisticFee = null;
		//新汇率
		BigDecimal newRate = goodsCheck.getRate();
		//旧汇率
		BigDecimal oldRate = oldGoodsCheck.getRate();
		//旧日元成本价
		BigDecimal oldProductPrice = oldGoodsCheck.getGoods().getPrice();
		boolean isUpdatePrice = false;
		
		//修改物流费及到岸价
		if (null != newLogisticPrice && !(newLogisticPrice.equals(oldLogisticPrice))) {
			//物流费 = 单价 * 重量
			newLogisticFee = goodsCheckService.getLogisticFee(newLogisticPrice, oldWeight);
			operation += ",物流费:由"+goodsCheckService.isNull(oldLogisticFee)+"修改为"+Decimal(newLogisticFee);
			oldGoodsCheck.setLogisticPrice(newLogisticPrice);
			oldGoodsCheck.setLogisticFee(newLogisticFee);
			//到岸成本价 = RMB 成本价 + 物流费用 (如果重量大于0)
			BigDecimal landedGoodsCost = goodsCheckService.getLandedGoodsCost(newLogisticFee, oldGoodsCheck.getRmbCostPrice());
			oldGoodsCheck.setLandedGoodsCost(landedGoodsCost);
			
			isUpdatePrice = true;
		}
		
		//修改RMB成本价及到岸价
		if (null != newRate && !(newRate.equals(oldRate))) {
			//RMB成本价 = 日元成本价 / 汇率
			newRmbCostPrice = goodsCheckService.getRmbCostPrice(oldProductPrice, newRate);
			operation += ",RMB成本价:由"+goodsCheckService.isNull(oldRmbCostPrice)+"修改为"+Decimal(newRmbCostPrice);
			oldGoodsCheck.setRate(newRate);
			oldGoodsCheck.setRmbCostPrice(newRmbCostPrice);
			//到岸成本价 = RMB 成本价 + 物流费用 (如果重量大于0)
			BigDecimal landedGoodsCost = goodsCheckService.getLandedGoodsCost(oldGoodsCheck.getLogisticFee(), newRmbCostPrice);
			oldGoodsCheck.setLandedGoodsCost(landedGoodsCost);
			
			isUpdatePrice = true;
		}
		
		if(null != newBoxPoint){
			oldGoodsCheck.setBoxPoint(newBoxPoint);
		}
		//修改经销商价格及成箱价
		if (null != newSellGrossMargin && !(newSellGrossMargin.equals(oldSellGrossMargin))
				||null != newBoxPoint && !(newBoxPoint.equals(oldBoxPoint))) {
			
			oldGoodsCheck.setSellGrossMargin(newSellGrossMargin);
			//经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用
			if (1 != BigDecimal.valueOf(1).compareTo(newSellGrossMargin.add(goodsCheckService.isNull(oldGoodsCheck.getBoxPoint())))) {
				remind += "商品编号"+oldGoodsCheck.getGoods().getCode()+",修改经销商价格计算失败。<br/>";
			} else {
				newSellPrice = goodsCheckService.getSellPrice(oldGoodsCheck.getRmbCostPrice(), 
						                                      newSellGrossMargin, 
						                                      oldGoodsCheck.getBoxPoint(),
						                                      oldGoodsCheck.getLogisticFee());
				newSellPrice = newSellPrice.setScale(1, BigDecimal.ROUND_HALF_UP);
				operation += ",经销商价格:由"+goodsCheckService.isNull(oldSellPrice)+"修改为"+Decimal(newSellPrice);
				oldGoodsCheck.setSellPrice(newSellPrice);
			}
			//成箱价格 = RMB 成本价 / (1 - 经销毛利率) + 物流费用
			newBoxPrice = goodsCheckService.getBoxPrice(oldGoodsCheck.getRmbCostPrice(),
					                                    newSellGrossMargin, 
					                                    oldGoodsCheck.getLogisticFee());
			operation += ",成箱价格:由"+goodsCheckService.isNull(oldBoxPrice)+"修改为"+Decimal(newBoxPrice);
			oldGoodsCheck.setBoxPrice(newBoxPrice);
			
			isUpdatePrice = true;
		}
		
		//修改零售销售价格
		if (null != newRetailGrossMargin && !(newRetailGrossMargin.equals(oldRetailGrossMargin))) {
			//零售销售价格 = RMB 成本价 / (1-零售毛利率) + 物流费用
			newRetailPrice = goodsCheckService.getRetailPrice(oldGoodsCheck.getRmbCostPrice(),
					                                          newRetailGrossMargin,
					                                          oldGoodsCheck.getLogisticFee());
			newRetailPrice = newRetailPrice.setScale(0, BigDecimal.ROUND_HALF_UP);
			operation += ",零售销售价格:由"+goodsCheckService.isNull(oldRetailPrice)+"修改为"+Decimal(newRetailPrice);
			oldGoodsCheck.setRetailGrossMargin(newRetailGrossMargin);
			oldGoodsCheck.setRetailPrice(newRetailPrice);
			
			isUpdatePrice = true;
		}
		
		//修改淘宝销售价格
		if (null != newTaobaoGrossMargin && !(newTaobaoGrossMargin.equals(oldTaobaoGrossMargin))) {
			//淘宝销售价格 = RMB 成本价 / (1-淘宝毛利率) + 物流费用
			newTaobaoPrice = goodsCheckService.getTaobaoPrice(oldGoodsCheck.getRmbCostPrice(),
					                                                  newTaobaoGrossMargin, 
					                                                  oldGoodsCheck.getLogisticFee());
			operation += ",淘宝销售价格:由"+goodsCheckService.isNull(oldTaobaoPrice)+"修改为"+Decimal(newTaobaoPrice);
			oldGoodsCheck.setTaobaoGrossMargin(newTaobaoGrossMargin);
			oldGoodsCheck.setTaobaoPrice(newTaobaoPrice);	
			
			isUpdatePrice = true;
		}
		
		Boolean checked = isGoodsCheck(oldGoodsCheck);
		
		if (isUpdatePrice) {
			remind += "商品编号"+oldGoodsCheck.getGoods().getCode()+",修改成功。<br/>";
		}
		
		if (true == checked) {
			operation += ",核价已通过";
		}
		
		oldGoodsCheck.setChecked(checked);
		goodsCheckService.update(oldGoodsCheck);
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "核价管理");
		
		return remind;
	}
	
	/**
	 * 
	 * <p>名称：是否核价通过</p> 
	 * <p>描述：每次进行修改价格后，根据所有金额判断是否已核价</p>
	 * @author：wuqi
	 * @param goodsCheck 修改后的核价数据
	 * @return
	 */
	public Boolean isGoodsCheck(GoodsCheck goodsCheck){
		if (null == goodsCheck.getRate() || null == goodsCheck.getRmbCostPrice() || null == goodsCheck.getLogisticPrice() || 
			null == goodsCheck.getLogisticFee() || null == goodsCheck.getLandedGoodsCost() || null == goodsCheck.getSellGrossMargin() || 
			null == goodsCheck.getSellPrice() || null == goodsCheck.getBoxPoint() || null == goodsCheck.getBoxPrice() || 
			null == goodsCheck.getRetailGrossMargin() || null == goodsCheck.getRetailPrice() ||
			null == goodsCheck.getTaobaoGrossMargin() || null == goodsCheck.getTaobaoPrice()){
			return false;
		}
		return true;
	}
	
	/**
	 * 
	 * <p>名称：返回核价不成功的提示信息</p> 
	 * <p>描述：返回核价不成功的提示信息</p>
	 * @author：zuoyang
	 * @param goodsCheck 修改后的核价数据
	 * @return
	 */
	private String getUncheckReason(GoodsCheck goodsCheck){
		String reasonStr = "";
		int count = 1 ;
        //update lushanshan 2016-06-29 上架失败的原因 start
        if(null==goodsCheck.getGoods().getProduct().getCategory().getId()){
            reasonStr = reasonStr + "&nbsp;&nbsp;&nbsp;"+count+")."+"分类未填写;<br>";
            count++;
        }
        if(null==goodsCheck.getGoods().getProduct().getWorks().getId()){
            reasonStr = reasonStr + "&nbsp;&nbsp;&nbsp;"+count+")."+"作品未填写;<br>";
            count++;
        }
        if(null==goodsCheck.getGoods().getProduct().getBrand().getId()){
            reasonStr = reasonStr + "&nbsp;&nbsp;&nbsp;"+count+")."+"品牌未填写;<br>";
            count++;
        }
        //update lushanshan 2016-06-29 上架失败的原因 end
		if(null == goodsCheck.getRate()){
			reasonStr = reasonStr + "&nbsp;&nbsp;&nbsp;"+count+")."+"汇率未填写;<br>";
			count++;
		}
		if(null == goodsCheck.getBoxPoint()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"未成箱加点未填写更改;<br>";
			count++;
		}
		if(null == goodsCheck.getRetailGrossMargin()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"下面的批量零售毛利率未填写更改;<br>";
			count++;
		}
		if(null == goodsCheck.getTaobaoGrossMargin()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"下面的批量淘宝毛利率未填写更改;<br>";
			count++;
		}
		if(null == goodsCheck.getLogisticPrice()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"下面的每公斤物流费用未填写更改;<br>";
			count++;
		}
		if(null == goodsCheck.getSellGrossMargin()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"下面的批量经销毛利率未填写更改;<br>";
			count++;
		}
		if(null == goodsCheck.getRmbCostPrice()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"RMB成本价未计算出来,可能是汇率或商品添加或编辑时日元成本价未填写,RMB 成本价 = 日元成本价/汇率;<br>";
			count++;
		}
		if(null == goodsCheck.getLogisticFee()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"物流费用未计算出来，可能是商品添加或编辑时生成单品的重量或下面的每公斤物流费用未填写,物流费用 = 重量 * 每公斤物流费;<br>";
			count++;
		}
		if(null == goodsCheck.getLandedGoodsCost()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"到岸成本价未计算出来，可能是RMB成本价和物流费用未计算出来,到岸成本价 = RMB 成本价 + 物流费用 (如果重量大于0);<br>";
			count++;
		}
		if(null == goodsCheck.getSellPrice()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"经销商价未计算出来,请检查RMB成本价及物流费用是否已计算,经销毛利率,未成箱加点是否填写,经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用;<br>";
			count++;
		}
		if(null == goodsCheck.getBoxPrice()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"成箱价未计算出来，请检查RMB成本价及物流费用是否已计算,下面的经销毛利率是否已填写更改,成箱价格 = RMB 成本价 / (1 - 经销毛利率) + 物流费用;<br>";
			count++;
		}
		if(null == goodsCheck.getRetailPrice()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"零售销售价格未计算出来,请检查RMB成本价及物流费用是否已计算,下面的零售毛利率是否未填写更改,经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用;<br>";
			count++;
		}
		if(null == goodsCheck.getTaobaoPrice()){
			reasonStr = reasonStr+"&nbsp;&nbsp;&nbsp;"+count+")." +"淘宝价格未计算出来,请检查RMB成本价及物流费用是否已计算,下面的淘宝毛利率是否未填写更改,淘宝销售价格 = RMB 成本价 / (1-淘宝毛利率) + 物流费用;<br>";
			count++;
		}
		
		return reasonStr;
	}
	
	/**
	 * 
	 * <p>名称：导出核价数据</p> 
	 * <p>描述：对选择的核价数据进行excel导出</p>
	 * @author：wuqi
	 * @param ids 选择的核价id
	 * @param request
	 * @param response
	 */
	@RequestMapping("exportData")
	public void exportData(@RequestParam(value="ids") Long[] ids,HttpServletRequest request,HttpServletResponse response){
		//默认下载标题
		String code = "G"+DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
		String[] headers = {"单品ID","商品货号","商品名称","是否上架","商品状态","商品品牌","汇率","RMB成本价","物流单价","物流费","到岸成本价","经销毛利率",/*"未成箱加点","成箱价",*/"经销商价","零售毛利率","零售销售价格","淘宝毛利率","淘宝价格"};
															
										
		
		List<GoodsCheck> dataset = new ArrayList<GoodsCheck>();  
		GoodsCheck goodsCheck = new GoodsCheck();
		for(Long id :ids){
			goodsCheck = goodsCheckService.find(id);
			goodsCheck.setGoodsCode(goodsCheck.getGoods().getCode());
			goodsCheck.setGoodsTitle(goodsCheck.getGoods().getTitle());
			goodsCheck.setGoodsOnSell(goodsCheck.getGoods().getOnSell()?"是":"否");
			goodsCheck.setGoodsStatus(getProductStatus(goodsCheck.getGoods().getProduct()));
			goodsCheck.setGoodsBrand(goodsCheck.getGoods().getProduct().getBrand().getName());
			dataset.add(goodsCheck);
		}
		//商品货号，商品名称，商品品牌，商品状态
        Map<String,Object> map = Maps.newHashMap();
        map.put("goodsId", "goodsId");
        map.put("rate", "rate");
        map.put("rmbCostPrice", "rmbCostPrice");
        map.put("logisticPrice", "logisticPrice");
        map.put("logisticFee", "logisticFee");
        map.put("landedGoodsCost", "landedGoodsCost");
        map.put("sellGrossMargin", "sellGrossMargin");
/*        map.put("boxPoint", "boxPoint");
        map.put("boxPrice", "boxPrice");*/
        map.put("sellPrice", "sellPrice");
        map.put("retailGrossMargin", "retailGrossMargin");
        map.put("retailPrice", "retailPrice");
        map.put("taobaoGrossMargin", "taobaoGrossMargin");
        map.put("taobaoPrice", "taobaoPrice");
        map.put("goodsCode", "goodsCode");
        map.put("goodsTitle", "goodsTitle");
        map.put("goodsOnSell", "goodsOnSell");
        map.put("goodsStatus", "goodsStatus");
        map.put("goodsBrand", "goodsBrand");
        try {
            ExcelUtil.exportExcel("核价属性", headers, dataset, map, code, response);
        } catch (Exception e) {
        	logger.info("Exception:"+e);
        }
	}
	
	/**
	 * 
	 * <p>名称：保留2位小数</p> 
	 * <p>描述：将传入的bigDeimal保存两位小数</p>
	 * @author：wuqi
	 * @param decimal 
	 * @return
	 */
	public String Decimal(BigDecimal decimal){
		return String.format("%.2f" ,decimal);
	}
	
	private String getProductStatus(Product p)
	{
		String ret = "";
		if(null != p.getSpot() && p.getSpot()){
			ret+=" 现货";
		}
		if(null != p.getReserved() && p.getReserved() ){
			ret+=" 预定";
		}
		if(null != p.getLimited() && p.getLimited() ){
			ret+=" 限购";
		}
		return ret;
	}
}
