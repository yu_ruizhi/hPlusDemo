package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Label;

public interface LabelDao extends BaseDao<Label>{
	List<Label> findManyByProductId(Long id);
	
	/**
	 * 
	 * <p>名称：根据名称获取数据</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	List<Label> findByName(String name);
}
