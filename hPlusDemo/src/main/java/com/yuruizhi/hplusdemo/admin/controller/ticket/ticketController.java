package com.yuruizhi.hplusdemo.admin.controller.ticket;


import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.ticket.Ticket;
import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketSku;
import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketSkuValue;
import com.yuruizhi.hplusdemo.admin.service.ticket.TicketService;
import com.yuruizhi.hplusdemo.admin.service.ticket.TicketSkuService;
import com.yuruizhi.hplusdemo.admin.service.ticket.TicketSkuValueService;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;


/**
 * Created by ASUS on 2016/7/14.
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/ticket")

public class ticketController extends AdminBaseController<Ticket>  {
	@Autowired
	private TicketService ticketService;
	@Autowired
	private TicketSkuService ticketSkuService;
	@Autowired
	private TicketSkuValueService ticketSkuValueService;
	
	
    @ResponseBody
    @RequestMapping("getTicketList")
    public Page<Ticket> getTicketList(Filters filters, Pageable pageable){
        return ticketService.findPage(filters, pageable);
    }
    
    @Transactional
    @RequestMapping("saveTicketSku")
    public void saveTicketSku(
    		String selectType,String hTimeType,String dateTimeName,String playName,String timeDays,
    		@RequestParam(value = "inputTimeBegin[]", required = false) String[] inputTimeBegin,
    		@RequestParam(value = "inputTimeEnd[]", required = false) String[] inputTimeEnd,
    		@RequestParam(value = "inputDateBegin[]", required = false) String[] inputDateBegin,
    		@RequestParam(value = "inputDateEnd[]", required = false) String[] inputDateEnd,
    		@RequestParam(value = "inputPlay[]", required = false) String[] inputPlay){
    	
   
		TicketSku t = new TicketSku();
    	TicketSkuValue v = null;   
        if(selectType.equals("1")){//时间规格
        	t.setType(Integer.parseInt(selectType));
			t.setTimeType(Integer.parseInt(hTimeType));
			t.setName(dateTimeName);
			
			if(null==timeDays || "".equals(timeDays)){
				timeDays="0";
			}
			t.setTimeTypeDays(Integer.parseInt(timeDays));
			
			ticketSkuService.save(t);//添加sku
			
        	SimpleDateFormat  s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        	if(hTimeType.equals("1")){//固定时间
    				
    			for(int i =0;i<inputDateBegin.length;i++){
    				v = new TicketSkuValue();
    				try {
						v.setStartTime(s.parse(inputDateBegin[i]));
						v.setEndTime(s.parse(inputDateBegin[i]));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				
    				ticketSkuValueService.save(v);//添加skuvalue
    			}
        		
        	}else if(hTimeType.equals("2")){//展会时间
        		for(int i =0;i<inputTimeBegin.length;i++){
    				v = new TicketSkuValue();
    				try {
						v.setStartTime(s.parse(inputTimeBegin[i]));
						v.setEndTime(s.parse(inputTimeEnd[i]));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    				ticketSkuValueService.save(v);//添加skuvalue
    			}
        	}
        }else if(selectType.equals("2")){//场馆规格
        	t.setType(Integer.parseInt(selectType));
			t.setTimeType(Integer.parseInt(hTimeType));
			t.setName(dateTimeName);
			t.setTimeTypeDays(0);
			ticketSkuService.save(t);//添加sku

        	for(int i =0;i<inputPlay.length;i++){
				v = new TicketSkuValue();
				v.setAreaName(inputPlay[i]);
				ticketSkuValueService.save(v);//添加skuvalue
			}
        	
        }
    	
    }
    
}
