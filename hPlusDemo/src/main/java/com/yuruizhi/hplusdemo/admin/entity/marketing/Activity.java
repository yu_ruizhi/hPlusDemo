/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月21日 下午2:09:25 
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 
 * <p>名称: 活动管理实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午2:09:25  
 * @version: 1.0
 */
public class Activity extends BaseEntity{

	private static final long serialVersionUID = 1L;

	/**@Fields name : 活动名称 */ 
	private String name;

	/**@Fields image : 活动图片 */ 
	private String image;

	/**@Fields discount : 折扣 */ 
	private BigDecimal discount;

	/**@Fields startTime : 开始时间 */ 
	private Date startTime;
	
	/**@Fields endTime : 结束时间 */ 
	private Date endTime;

	/**@Fields sort : 排序 */ 
	private Integer sort;

	/**@Fields isShow : 是否显示 */ 
	private Boolean isShow;

	/**@Fields deleted : 删除标识 */ 
	private Boolean deleted = false;

	/**@Fields activityGoodsList : 活动商品 */ 
	private List<ActivityGoods> activityGoodsList;
    /**@Fields content : 资讯内容 */
    private String content;
    /**@Fields type : 活动的类别 */
    private Integer type;

    public Integer  getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Boolean getIsShow() {
		return isShow;
	}

	public void setIsShow(Boolean isShow) {
		this.isShow = isShow;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public List<ActivityGoods> getActivityGoodsList() {
		return activityGoodsList;
	}

	public void setActivityGoodsList(List<ActivityGoods> activityGoodsList) {
		this.activityGoodsList = activityGoodsList;
	}
	
}
