package com.yuruizhi.hplusdemo.admin.dao.logistics;



import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethodDetail;

/**
 * Created by ASUS on 2014/12/9.
 */
public interface DeliveryMethodDetailDao extends BaseDao<DeliveryMethodDetail> {
   void deletedByMethodId(Long deliveryMethodId);
  void  deletedMethodId(Long deliveryMethodId);
}
