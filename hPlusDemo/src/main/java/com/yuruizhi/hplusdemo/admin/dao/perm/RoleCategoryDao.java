package com.yuruizhi.hplusdemo.admin.dao.perm;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.RoleCategory;

import java.util.List;


/**
 * Created by ASUS on 2014/12/22.
 */
public interface RoleCategoryDao extends BaseDao<RoleCategory> {

    List<RoleCategory> findInRole(Long roleId);

    void deleteInRole(Long roleId);

}
