/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.entity.marketing.FlashSaleDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: FlashSaleDetailService</p>
 * <p>说明: 限时抢购详情服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:17:35   
 * @version: 1.0
 */
public interface FlashSaleDetailService extends BaseService<FlashSaleDetail> {
}
