/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import java.math.BigDecimal;
import java.util.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Coupon</p>
 * <p>说明: 优惠券实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:53:37   
 * @version: 1.0
 */
public class Coupon extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5263334353730805675L;

	/**
     * 优惠券名称
     * 
     * @ViewField editor=input 
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 优惠券编码
     * 
     * @ViewField editor=input 
     * @Column CODE
     */
    @NotNull
    @Size(max = 255)
    private String code;

    /**
     * 券面值
     * 
     * @ViewField editor=input 
     * @Column AMOUNT
     */
    @NotNull
    private BigDecimal amount;

    /**
     * 最低使用金额
     * 
     * @ViewField editor=input 
     * @Column MIN_USAGE_AMOUNT
     */
    @NotNull
    private BigDecimal minUsageAmount;

    /**
     * 生成批次号
     * @ViewField editor=input 
     * @Column BATCH
     */
    @Size(max = 255)
    private String batch;

    /**
     * 状态
     * @ViewField editor=spinner 
     * @Column STATUS
     */
    @NotNull
    private Integer status;

    /**
     * 发放时间
     * 
     * @ViewField editor=datepicker 
     * @Column SEND_TIME
     */
    private Date sendTime;

    /**
     * 有效截止时间
     * 
     * @ViewField editor=datepicker 
     * @Column DEAD_TIME
     */
    private Date deadTime;

    /**
     * 优惠券类型
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=COUPON_TYPE_ID optional=true
     */
    private CouponType couponType;
    
    private Long couponTypeId;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBatch() {
        return this.batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getSendTime() {
        return this.sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getDeadTime() {
        return this.deadTime;
    }

    public void setDeadTime(Date deadTime) {
        this.deadTime = deadTime;
    }

    public CouponType getCouponType() {
        return this.couponType;
    }

    public void setCouponType(CouponType couponType) {
        this.couponType = couponType;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the minUsageAmount
	 */
	public BigDecimal getMinUsageAmount() {
		return minUsageAmount;
	}

	/**
	 * @param minUsageAmount the minUsageAmount to set
	 */
	public void setMinUsageAmount(BigDecimal minUsageAmount) {
		this.minUsageAmount = minUsageAmount;
	}

	public Long getCouponTypeId() {
		return couponTypeId;
	}

	public void setCouponTypeId(Long couponTypeId) {
		this.couponTypeId = couponTypeId;
	}

}
