/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.ticket;


import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.List;

/**
 * 票务规格表
 */
public class TicketOrderDetail extends BaseEntity {

    private Long orderId;

    private Ticket ticket;

    private BigDecimal price;

    private Integer qty;

    private Boolean deleted;

    private TicketOrder ticketOrder;

    private Long ticketCountId;

    private List<TicketCount> ticketCountList;

    public List<TicketCount> getTicketCountList() {
        return ticketCountList;
    }

    public void setTicketCountList(List<TicketCount> ticketCountList) {
        this.ticketCountList = ticketCountList;
    }

    public Long getTicketCountId() {
        return ticketCountId;
    }

    public void setTicketCountId(Long ticketCountId) {
        this.ticketCountId = ticketCountId;
    }

    public TicketOrder getTicketOrder() {
        return ticketOrder;
    }

    public void setTicketOrder(TicketOrder ticketOrder) {
        this.ticketOrder = ticketOrder;
    }

    private List<TicketSku> ticketSkuList;

    public List<TicketSku> getTicketSkuList() {
        return ticketSkuList;
    }

    public void setTicketSkuList(List<TicketSku> ticketSkuList) {
        this.ticketSkuList = ticketSkuList;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
