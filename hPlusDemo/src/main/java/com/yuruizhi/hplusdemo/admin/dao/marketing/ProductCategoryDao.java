/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月20日 上午10:33:42 
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategory;

/**
 * 
 * <p>名称: 分类商品Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:33:42    
 * @version: 1.0
 */
public interface ProductCategoryDao extends BaseDao<ProductCategory>{

	/**
	 * 
	 * <p>名称：根据分类名称获取分类信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name 传入分类名称
	 * @return 返回该分类名称的所有信息
	 */
	List<ProductCategory> findByName(String name);
	
}
