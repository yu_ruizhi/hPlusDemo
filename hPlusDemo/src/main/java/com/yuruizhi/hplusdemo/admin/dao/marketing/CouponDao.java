/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.entity.marketing.Coupon;

/**
 * Created on 2016-03-30 11:47:01
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface CouponDao extends BaseDao<Coupon> {
}