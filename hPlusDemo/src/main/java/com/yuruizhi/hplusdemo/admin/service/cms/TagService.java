/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.cms.Tag;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: TagService</p>
 * <p>说明: 文章标签服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:07:01   
 * @version: 1.0
 */
public interface TagService extends BaseService<Tag>{

	/** 
	 * @Title: findAndSave 
	 * @Description: TODO
	 * @param tarr
	 * @return
	 * @return: Tag
	 */
	Tag findAndSave(String tarr);

	/** 
	 * @Title: findByArtId 
	 * @Description: TODO
	 * @param id
	 * @return
	 * @return: List<Tag>
	 */
	List<Tag> findByArtId(Long id);

	Boolean isUnique(String tagName,Long id);

}
