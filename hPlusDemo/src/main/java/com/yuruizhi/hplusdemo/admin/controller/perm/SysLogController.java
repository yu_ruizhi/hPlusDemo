package com.yuruizhi.hplusdemo.admin.controller.perm;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.perm.SysLog;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 赵创 on 2016/4/23. 后台系统日志
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/syslog")
public class SysLogController extends BaseController {

     @Autowired
     private SysLogService sysLogService;


	@RequestMapping("index")
	public String index() {
        return "syslog/index";
	}



	@RequestMapping("list")
	@ResponseBody
	public Page<SysLog> list(Filters filters, Pageable pageable
            ,@RequestParam(value = "goodsCode", required = false) String goodsCode) {
        if(null!=goodsCode&&goodsCode.trim().length()>0){
            filters.like("operation", '%' + goodsCode + '%');
        }

        Page<SysLog> sysLogs=sysLogService.findPage(filters,pageable);
		return sysLogs;
	}



}
