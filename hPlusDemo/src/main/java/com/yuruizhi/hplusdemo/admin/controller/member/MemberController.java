package com.yuruizhi.hplusdemo.admin.controller.member;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberLevel;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.member.MemberLevelService;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.PassWordSaltUtil;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * 前台会员用户管理
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/member")
public class MemberController extends BaseController {
	
	private static final Logger logger = Logger.getLogger(DateUtils.class);
	
	@Autowired
	private MemberService memberService;

	@Autowired
	private MemberLevelService memberLevelService;
    
    @Autowired
    private SysLogService sysLogService;

    @Autowired
    private PassWordSaltUtil passWordSaltUtil;
    
    @Autowired
    private UserService userService;
    
    /**
     * 
     * <p>名称：新增</p> 
     * <p>描述：从列表页面进入新增</p>
     * update 2016.4.19 wuqi: 进入新增时显示会员等级
     * @author：
     * @param model
     */
	@RequestMapping("add")
	public void add(ModelMap model) {
//		List<MemberLevel> memberLevelList = memberLevelService.findAll();
		List<User> salesUserList = userService.findByRoleName(Constant.ADMIN_USER_NAME.SALESMAN);
//		model.put("memberLevelList", memberLevelList);
		model.put("salesUserList", salesUserList);
	}

	@RequestMapping("index")
	public void index(ModelMap model) {
		List<User> salesUserList = userService.findByRoleName(Constant.ADMIN_USER_NAME.SALESMAN);
		model.put("salesUserList", salesUserList);
	}

	@RequestMapping("list")
	@ResponseBody
	public Page<Member> list(@Qualifier("TM.")Filters filters,@Qualifier("TML.")Filters memberLevelFilters, 
							@Qualifier("TMU.")Filters memberUserFilters, Pageable pageable) {
		return memberService.findPage(filters, memberLevelFilters,memberUserFilters, pageable);
	}
	
	@RequestMapping("memberList")
	@ResponseBody
	public Page<Member> memberList(Filters filters, Pageable pageable,Long[] memberIds) {
		if(memberIds!=null&&memberIds.length==0){
			memberIds = null;
		}
		return memberService.findMemberPage(filters, pageable,memberIds);
	}
	
	/**
	 * 
	 * <p>名称：编辑</p> 
	 * <p>描述：</p>
	 * update 2016.4.19 wuqi: 进入编辑时显示会员等级
	 * @author：
	 * @param id
	 * @return
	 * 
	 */
	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		Member member = memberService.find(id);
//		List<MemberLevel> memberLevelList = memberLevelService.findAll();
		List<User> salesUserList = userService.findByRoleName(Constant.ADMIN_USER_NAME.SALESMAN);
		mav.addObject("user", member);
//		mav.addObject("memberLevelList", memberLevelList);
		mav.addObject("salesUserList",salesUserList);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	/**
	 * 
	 * <p>名称：更新</p> 
	 * <p>描述：</p>
	 * update 2016.5.4 wuqi: 更新时保存系统日志
	 * @author：
	 * @param member
	 * @return
	 */
	@RequestMapping("update")
	public String update(Member member) {
		Member oldMember = memberService.find(member.getId());
		member.setLastModifiedDate(new Date());
		if ((Constant.USER_TYPE_NORMAL).equals(member.getUserType())) {
			member.setSalesmanId(0L);
			member.setIsAudit(false);
		}
		memberService.update(member);
		//系统操作日志
		if (null != member && null != oldMember) {
			String operation = "会员管理-修改-id:"+member.getId();
			if (oldMember.getStatus() != member.getStatus()){
				operation += ",会员状态被修改";
			}
			if (oldMember.getMemberLevelId() != member.getMemberLevelId()) {
				operation += ",会员等级被修改";
			}
			if (oldMember.getUserType() != member.getUserType() ) {
				operation += ",会员类型被修改";
			}
			sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "会员管理");
		}
		return redirectViewPath("index");
	}

	/**
	 * 
	 * <p>名称：保存</p> 
	 * <p>描述：</p>
	 * update 2016.5.4 wuqi: 保存时保存系统日志
	 * @author：
	 * @param member
	 * @return
	 */
	@RequestMapping("save")
	public String save(Member member) {
		member.setRegisterTerraceType(Constant.MEMBER_REGISTER_TERRACE_TYPE.PC);
		member.setIsActive(true);
		memberService.insert(member);
		//系统日志
		if (null != member) {
			String operation = "会员管理-新增-id:"+member.getId();
			sysLogService.saveSysLog(Constant.SYSLOG_SAVE, operation, "会员管理");
		}
		return redirectViewPath("index");
	}

	/**
	 * 
	 * <p>名称：删除</p> 
	 * <p>描述：</p>
	 * update 2016.5.4 wuqi: 删除时保存系统日志
	 * @author：wuqi
	 * @param id
	 * @return
	 */
	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		memberService.delete(id);
		String ids = "";
		for (int i = 0; i < id.length; i++) {
			ids += id[i]+",";
		}
		//系统日志
		ids = ids.substring(0, ids.length()-1);
		String operation = "会员管理-删除-id:"+ids;
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "会员管理");
		return ImmutableMap.<String, Object> of("success", true);
	}
	
	@ResponseBody
	@RequestMapping("findMemberList")
	public List<Member> findMemberList(Long[] ids){
		return memberService.findMemberList(ids);
	}
	
	/**
	 * 
	 * <p>名称：保存检测用户名是否重复</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	@RequestMapping("checkIsNameExists")
	@ResponseBody
	public Map<String, Boolean> checkIsNameExists(String name){
		return memberService.checkIsNameExists(name);
	}
	
	/**
	 * 
	 * <p>名称：编辑检测用户名是否重复</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param idAndName
	 * @return
	 */
	@RequestMapping("hasAnnotherName")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherName(String[] idAndName){
		Map<String, Boolean> map = memberService.hasAnnotherName(idAndName);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：新增时检测邮箱是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param email
	 * @return
	 */
	@RequestMapping("checkIsEmailExists")
	@ResponseBody
	public Map<String, Boolean> checkIsEmailExists(String email){
		return memberService.checkIsEmailExists(email);
	}
	
	/**
	 * 
	 * <p>名称：编辑时检测邮箱是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param idAndEmail
	 * @return
	 */
	@RequestMapping("hasAnnotherEmail")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherEmail(String[] idAndEmail){
		Map<String, Boolean> map = memberService.hasAnnotherEmail(idAndEmail);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：新增时检测手机是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param phone
	 * @return
	 */
	@RequestMapping("checkIsPhoneExists")
	@ResponseBody
	public Map<String, Boolean> checkIsPhoneExists(String phone){
		return memberService.checkIsPhoneExists(phone);
	}
	
	/**
	 * 
	 * <p>名称：编辑时检测手机是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param idAndPhone
	 * @return
	 */
	@RequestMapping("hasAnnotherPhone")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherPhone(String[] idAndPhone){
		Map<String, Boolean> map = memberService.hasAnnotherPhone(idAndPhone);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：导出数据</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ids
	 * @param request
	 * @param response
	 */
	@RequestMapping("exportData")
	public void exportData(@RequestParam(value="ids") Long[] ids,HttpServletRequest request,HttpServletResponse response){
		String code = "M"+DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
		String[] headers = { "用户编号","用户名","昵称","会员类型","邮箱","手机号","注册时间","注册的平台","保证金经销商"};
		List<Member> dataset = new ArrayList<Member>();  
		Member member = new Member();
		if (null == ids || 0 >= ids.length) {
			List<Member> memberList = memberService.findAll();
			for (Member members : memberList) {
				members = getMember(members);
				dataset.add(members);
			}
		} else {
			for(Long id :ids){
				member = memberService.find(id);
	            member = getMember(member);
				dataset.add(member);
			}
		}
        Map<String,Object> map = Maps.newHashMap();
        map.put("code", "code");
        map.put("lastName", "lastName");
        map.put("nickName", "nickName");
        map.put("email", "email");
        map.put("phone", "phone");
        map.put("userType","userType");
        map.put("register", "register");
        map.put("registerTerraceType","registerTerraceType");
        map.put("isMargins","isMargins");
        //调用下载
        try{
        	ExcelUtil.exportExcel("会员属性", headers, dataset, map, code, response);
        } catch (Exception e) {
        	logger.info("Exception:"+e);
        	e.printStackTrace();
        }
	}
	
	private Member getMember (Member member) {
		if(null == member.getUserType() || 0==member.getUserType().compareTo(String.valueOf(0))){
            member.setUserType("普通会员");
        }else {
            member.setUserType("经销商");
        }
        if(null == member.getRegisterTerraceType() || 1==member.getRegisterTerraceType()){
            member.setRegisterTerraceType(1);
        }else {
            member.setRegisterTerraceType(2);
        }
        if(null!=member.getIsMargin() && member.getIsMargin()){
            member.setIsMargins("是");
        }else {
            member.setIsMargins("否");
        }
        return member;
	}
	
	/**
	 * 
	 * <p>名称：审核经销商</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ids
	 * @return
	 */
	@RequestMapping("audit")
	@ResponseBody
	public Map<String, Object> audit(@RequestParam("id") Long[] ids) {
		for (Long id : ids) {
			Member member = memberService.find(id);
			if (Constant.USER_TYPE_MERCHANT.equals(member.getUserType())) {
				member.setIsAudit(true);
				memberService.updateMember(member);
				String operation = "会员管理-修改-id:"+member.getId()+",经销商审核通过";
				sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "会员管理");
			}
		}
		return ImmutableMap.<String, Object> of("success", true);
	}
	
	/**
	 * 
	 * <p>名称：普通用户升级为经销商</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @return
	 */
	@RequestMapping("up")
	@ResponseBody
	public Map<String, Object> up(@RequestParam("id") Long[] ids,Long salesmanId) {
		for (Long id : ids) {
			Member member = memberService.find(id);
			if (Constant.USER_TYPE_NORMAL.equals(member.getUserType())) {
				//update 2016-8-12 wuqi 当普通会员没有昵称时，将手机号赋值给经销商用户名
				if (null == member.getNickName() || "".equals(member.getNickName())) {
					member.setLastName(member.getPhone());
				} else {
					member.setLastName(member.getNickName());
				}
				member.setIsAudit(true);
				member.setUserType(Constant.USER_TYPE_MERCHANT);
				member.setSalesmanId(salesmanId);
				memberService.updateMember(member);
				String operation = "会员管理-修改-id:"+member.getId()+",升级为经销商";
				sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "会员管理");
			}
		}
		return ImmutableMap.<String, Object> of("success", true);
	}

}
