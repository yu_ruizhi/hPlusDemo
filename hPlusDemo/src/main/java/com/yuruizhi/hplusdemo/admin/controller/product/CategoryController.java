/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.product;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ponly.common.util.FileUtils;
import org.ponly.common.util.StringUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.multipart.MultipartFile;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Brand;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Model;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import com.yuruizhi.hplusdemo.admin.service.product.ModelService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.MathUtil;
import com.yuruizhi.hplusdemo.admin.util.ZipUtil;
import com.yuruizhi.hplusdemo.admin.validate.CategoryValidate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * Created on 2016-03-10 19:08:53
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/category")
public class CategoryController extends AdminBaseController<Category> {
    @Autowired
    private ModelService modelService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ProductService productService;
    
    /**
     * 校验分类名称合法性
     */
    @ResponseBody
    @RequestMapping("check")
    public Boolean check(Long id, String name) {
        Filters filters = Filters.create();

        if (null != id) {
            filters.ne("id", id);
        }
        if (StringUtils.hasText(name)) {
            filters.eq("name", name);
        }
        return categoryService.count(filters) < 1;
    }

    @ResponseBody
    @RequestMapping("detail")
    public Category detail(Long id, Category category) {
        return category;
    }

    @ResponseBody
    @RequestMapping("categoryList")
    public Page<Category> categoryList(Filters filters, Pageable pageable,
                               @RequestParam(value = "nolist", required = false) String nolist
                               ) {
        ServletRequest request = (ServletRequest) RequestContextHolder.currentRequestAttributes().resolveReference("request");
        String nid = null != request ? request.getParameter("nodeid") : null;
        if (StringUtils.hasText(nid)) {
            Long pid = -9999L;  // 无效值
            try {
                pid = Long.valueOf(nid);
            } catch (NumberFormatException ignore) {
            }
            filters.eq("parent.id", pid);
        }
        if(null!=nolist&&nolist.equals("nolist")){
            pageable=null;
        }
        Page<Category> categoryPage= super.list(filters, pageable);
        
        
  
        
//        List<Category> newCate = orderCate(categoryPage.getData(),new ArrayList<Category>() ,1, 0);
//        
//        String id = "";
//        for(Category ca : newCate){
//        	if(ca.getParent()==null){
//        		id="0";
//        	}else{
//        		id = ca.getParent().getId().toString();
//        	}
//        	System.out.println(ca.getDepth()+"-"+ca.getId()+"-"+id+"-"+ca.getName());
//        }
        
        return categoryPage;
    }
    
    private List<Category> orderCate(List<Category> cate,List<Category> newCate,int depth,long parentId){
    	for(Category cat : cate){
    		if(depth == 1){
    			if(cat.getDepth() == depth){
        			newCate.add(cat);
        			orderCate(cate,newCate,depth+1,cat.getId());
        		}
    		}else{
    			if(null !=cat.getParent()){
    				if(cat.getDepth() == depth && cat.getDepth() > 1 && parentId == cat.getParent().getId() ){
            			newCate.add(cat);
            			orderCate(cate,newCate,depth+1,cat.getId());
            		}
    			}
    		}
    	}
    	return newCate;
    }

    @ResponseBody
    @RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
          if(null==id||id.length<1)
        return ImmutableMap.<String, Object>of("success", true);
        categoryService.delete(id);
        return ImmutableMap.<String, Object>of("success", true);
    }

    @Override
    public void input(Category category, Map<String, Object> model) {
        super.input(category, model);
        model.put("models", modelService.findAll());
//        model.put("categorys", categoryService.findAll());
        
//        model.put("brands", brandService.findAll());
        //updated by qinzhongliang 20160620 获取已启用品牌
        Map<String, Object> paramMap = Maps.newHashMap();
        paramMap.put("enabled", true);
        /*List<Brand> brands = brandService.findMany(paramMap);
        model.put("brands", brands);
        
        // update 2016.4.25 qinzhongliang 查询所有作品
        model.put("worksList", worksService.findAll());*/
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Model.class, "model", new CustomStringBeanPropertyEditor(Model.class));
        binder.registerCustomEditor(Category.class, "parent", new CustomStringBeanPropertyEditor(Category.class));
        binder.registerCustomEditor(List.class, "brands", new CustomStringCollectionBeanCollectionPropertyEditor(
                List.class, true, Brand.class, "id", new String[]{}
        ));
        binder.registerCustomEditor(List.class, "works",new CustomStringCollectionBeanCollectionPropertyEditor(
        		List.class, true,Works.class,"id",new String[]{}
        ));
    }

    @Override
    protected Category prepareModel(@RequestParam(value="id", required = false) Long id) {
        Category cat = super.prepareModel(id);
        // 如果是新建对象, 设置默认值
        if (null == cat.getId()) {
            cat.setVisible(true);
        }
        return cat;
    }
    
    /**
     * <p>名称：自定义保存方法</p> 
     * <p>描述：默认插入所有的品牌、作品</p>
     * @author：qinzhongliang	2016/6/17
     * @param category
     * @return
     */
    @RequestMapping("customSave")
    public String customSave(Category category) {
    	Map<String, Object> paramMap = Maps.newHashMap();
    	paramMap.put("enabled", true);
    	categoryService.save(category);
    	return redirectViewPath("/");
    }
    
    /**
     * 
     * <p>名称：</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param request
     * @param response
     */
    @RequestMapping("downLoadModel")
    public void downLoadModel(HttpServletRequest request,HttpServletResponse response) {
    	String[] headers = {"分类名称","所属上级分类","使用模型","排序","是否显示"};
    	List<String> dataList = new ArrayList<>();
    	String data = "手办模型,,手办模型,1,是";
    	dataList.add(data);
    	data = "手办测试,手办模型,手办模型,1,是";
    	dataList.add(data);
    	String fileName = "category" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
    	ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }
    
    @ResponseBody
    @RequestMapping("importData")
    public Map<String,Object> importData(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	Map<String,Object> map = Maps.newHashMap();
    	InputStream inputStream = multipart.getInputStream();
    	//将xls数据转成String数组
    	List<String> list = ExcelUtil.importExcel(inputStream, 1, 5);
    	String error = "";
    	if (null != list && 0 < list.size()) {
			for (int i=0;i<list.size();i++) {
				String data[] = null;
				Boolean success = true;
				if (null != list.get(i)) {
					data = list.get(i).split(",");
					//对分类的部分值进行验证
					Map<String,Object> categoryMap = CategoryValidate.categoryValidate(data, error, i, success);
					Category category = (Category) categoryMap.get("category");
					success = (Boolean) categoryMap.get("success");
					error = (String) categoryMap.get("error");
					//是否存在上级分类
					if (null != data[1] && !"".equals(data[1])) {
						List<Category> parentCategory = categoryService.findByName(data[1]);
						if (null != parentCategory && 0 < parentCategory.size()) {
							category.setParent(parentCategory.get(0));
						} else {
							error = "第"+(i+1)+"行数据导入失败，原因为上级分类不存在<br>";
							success = false;
						}
					}
					//是否存在模型
					if (null != data[2] && !"".equals(data[2])) {
						List<Model> modelList = modelService.findByName(data[2]);
						if (null != modelList && 0 < modelList.size()) {
							category.setModel(modelList.get(0));
						} else {
							error = "第"+(i+1)+"行数据导入失败，原因为模型不存在<br>";
							success = false;
						}
					}
					//success为true时，保存
					if (success) {
						//根据名称进行查找保存还是更新
						List<Category> categoryList = categoryService.findByName(category.getName());
						if (null != categoryList && 0 < categoryList.size()) {
							categoryService.update(category);
						} else {
							categoryService.save(category);
						}
					}
				}
			}
    	}
    	map.put("error", error);
    	return map;
    }
    
    @ResponseBody
    @RequestMapping("merge")
    public Map<String,Object> merge(Long id,Long mergeId) {
    	List<Product> products = productService.findByCategoryId(mergeId);
    	for (Product product : products) {
    		Category category = categoryService.find(id);
    		product.setCategory(category);
    		productService.onlyUpdate(product);
    	}
    	return ImmutableMap.<String, Object>of("success",true);
    }

    /**
	 * <p>名称：批量更新分类图片</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @return
	 */
    @RequestMapping("updateImage")
    public String updateImage(){
    	
        return "category/updateImage";
    }
    
    /**
     * <p>名称：处理上传的zip图片压缩包并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param multipart
     * @param request
     * @throws IOException
     */
    @RequestMapping(value = "/uploadImage",  method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> uploadImage(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	
    	InputStream inputStream = multipart.getInputStream();
    	
    	String savePath = request.getSession().getServletContext().getRealPath("/WEB-INF/import/iamgeZip");
    	
    	String zipFilePath = ZipUtil.copyZipFileStreamToServer(inputStream, savePath);
    	//随机解压文件名
    	String unzipFolderName = DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
    	List<String> imagePathList = ZipUtil.handleZip(zipFilePath, savePath+"/"+unzipFolderName);
    	
    	Map<String,Object> resultMap = Maps.newHashMap();
    	//处理图片
    	if(null != imagePathList && imagePathList.size() > 0){
    		List<String> resultList = handleImageList(imagePathList);
    		resultMap.put("resultList", resultList);
    	}
    	
    	return resultMap;
    }
    
    /**
     * <p>名称：处理批量图片并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param imagePathList
     * @return
     */
    private  List<String> handleImageList(List<String> imagePathList){
    	String filePath = null;
   	    String categoryIdStr = null;
   	    String iconType = null;
   	    Category category = null;
   	    List<String> resultList = new ArrayList<String>();
   	    StringBuilder sb = new StringBuilder();
    	for(String path : imagePathList){
	  		  categoryIdStr = FileUtils.getNameWithoutExtension(path);
	  		  //先判断是否符合数字加下划线加1/2的格式
	  		  if(null != categoryIdStr 
	  				  && categoryIdStr.matches("[0-9]{1,11}_[1-2]")){
	  			iconType = categoryIdStr.split("_")[1];
	    		categoryIdStr = categoryIdStr.split("_")[0];
	  		  }else{
	  			  sb.delete(0, sb.length());
	  			  sb.append(FileUtils.getName(path) + "命名格式不正确,请检查");
	  			  resultList.add(sb.toString());
	  			  continue ;
	  		  }
	  		  //将图片放到服务器对应位置并更新数据
  			  filePath = CSVUtil.CopyFileToServer(path);
  			  category = categoryService.find(Long.valueOf(categoryIdStr));
  			  if(null != category && "1".equals(iconType)){
  				  category.setImage(filePath);
  				  categoryService.update(category);
  			  }else if(null != category && "2".equals(iconType)){
  				  category.setHighlightImage(filePath);
  				  categoryService.update(category);
  			  }else{
  				  sb.delete(0, sb.length());
	  			  sb.append(FileUtils.getName(path) + "对应的分类找不到,请检查此分类ID是否存在");
	  			  resultList.add(sb.toString());
  			  }
  	    }	
    	
    	return resultList ;
    }

}
