/*
 * Copyright (c) 2014, by Besture All right reserved. 
 *
 */
package com.yuruizhi.hplusdemo.admin.dao.product;


import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductImg;

/**
 * Created on 2014-07-25 09:11:01
 *
 * @author crud generated
 */
public interface ProductImgDao extends BaseDao<ProductImg> {

	/**
	 *
	 * @author baiyin
	 * @param productID
	 * @return
	 * @since JDK 1.7
	 * @change Log:
	 */
	List<ProductImg> listByProduct(String productID);


	void deleteProductImage(Long pid);

	void deleteGoodsImage(Long gid);
}