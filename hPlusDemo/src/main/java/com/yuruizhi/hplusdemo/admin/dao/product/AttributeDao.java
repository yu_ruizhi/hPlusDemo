
package com.yuruizhi.hplusdemo.admin.dao.product;
import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;


public interface AttributeDao extends BaseDao<Attribute> {
    List<Attribute> findModelAttributes(Long mid);

    void deleteModelAttributes(Long mid);
    
    /**
     * 
     * <p>名称：根据名称及模型id获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    Attribute findByNameAndModelId (Map<String,Object> map);
    
    /**
     * 
     * <p>名称：根据名称获取基础数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    Attribute findByNameAndIsBase(String name);
}