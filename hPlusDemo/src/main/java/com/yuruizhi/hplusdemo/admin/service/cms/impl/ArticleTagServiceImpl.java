/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.cms.ArticleTagDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.ArticleTag;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.ArticleTagService;

/**
 * <p>名称: ArticleTagServiceImpl</p>
 * <p>说明: 文章标签服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:59:54   
 * @version: 1.0
 */
@Service
public class ArticleTagServiceImpl extends BaseServiceImpl<ArticleTag> implements ArticleTagService {
	
	@Autowired
	private ArticleTagDao articleTagDao;
	@Autowired
	public void setArticleTagDao(ArticleTagDao articleTagDao) {
		super.setBaseDao(articleTagDao);
		this.articleTagDao = articleTagDao;
	}
	@Override
	public void delByArtId(Long id) {
		articleTagDao.delByArtId(id);
	}
	@Override
	public List<ArticleTag> findByArtId(Long id) {
		return articleTagDao.findByArtId(id);
	}

}
