package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.ponly.webbase.dao.CrudDao;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.ServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 单品控制器
 */
@Controller
@RequestMapping("goods")
public class GoodsController extends AdminBaseController<Goods> {
	
	@Autowired
	private GoodsService goodsService;
	
	@ResponseBody
	@RequestMapping("list")
	public Page<Goods> list(Filters filters,Pageable pageable){
		ServletRequest request = (ServletRequest) RequestContextHolder.currentRequestAttributes().resolveReference("request");
		String code = null != request ? request.getParameter("goodsCode") : null;
		if (StringUtils.isNotEmpty(code)) {
			filters.like("code", '%'+code+'%' );
		}
		return goodsService.findPage(filters, pageable);
	}

	@ResponseBody
	@RequestMapping("listInProductId")
	public Page<Goods> listInProductId(Filters filters,Pageable pageable,@RequestParam(value = "productId", required = false) Long productId){
		if(null!=productId){
			filters.eq("product.id",productId );
		}
		return goodsService.findPage(filters, pageable);
	}
	
	/**
	 * 
	 * <p>名称：获取已上架的单品</p> 
	 * <p>描述：update20160511 wentan 解决可供选择的商品有“未上架”的bug</p>
	 * @author：wentan
	 * @param filters
	 * @param pageable
	 * @return
	 */
	@RequestMapping("onSellGoods")
	@ResponseBody
	public Page<Goods> onSellGoodsList(Filters filters, Pageable pageable){
		filters.add("onSell", Filters.Operator.EQ, "1");

		return goodsService.findPage(filters, pageable);
	}
	
	
	@RequestMapping("getNewGoods")
	@ResponseBody
	public List<Map<String, Object>> getNewGoods(Filters filters,Pageable pageable){
		Map<String,Object> map =Maps.newHashMap();
		int offset = (pageable.getPage() - 1) * pageable.getPageSize();
		map.put("page", offset);
		map.put("pageSize", pageable.getPageSize());

		if(null != filters.getFilterFor("code")){	
			map.put("code",filters.getFilterFor("code").iterator().next());
		}
		if(null != filters.getFilterFor("title")){
			map.put("title",filters.getFilterFor("title").iterator().next());
		}
		if(null != filters.getFilterFor("name")){
			map.put("name",filters.getFilterFor("name").iterator().next());
		}
		
		return goodsService.getNewGoods(map);
	}
	
	
    /**
     *
     * <p>名称：获取已上架的现货单品</p>
     * <p>描述：update20160511 wentan 解决可供选择的商品有“未上架”的bug</p>
     * @author：wentan
     * @param filters
     * @param pageable
     * @return
     */
    @RequestMapping("onSellAndSpot")
    @ResponseBody
    public Page<Goods> onSellAndSpot(Filters filters, Pageable pageable){
        filters.add("ON_SEll", Filters.Operator.EQ, "1");
       
        
        Map<String, Object> params = Maps.newHashMap();
		params.put(CrudDao.FILTERS_PROP, filters);
		params.put("isCheck",true);
		params.put("spot", 1);
        return goodsService.findPage(params, pageable);
    }
}
