/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Email;
import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailMember;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.marketing.EmailMemberService;
import com.yuruizhi.hplusdemo.admin.service.marketing.EmailService;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created on 2016-04-14 15:43:33
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/email")
public class EmailController extends AdminBaseController<Email> {
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private EmailMemberService emailMemberService;
	
	@Autowired
	private MemberService memberService;

	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }
	
	@RequestMapping("emailSave")
	public String emailSave(Email email){
		if(email==null){
			return getRelativeViewPath("index");
		}else{
			emailService.emailSave(email);
			return getRelativeViewPath("index");
		}
	}
	
	@Override
	@RequestMapping("edit/{id}")
	public String _compat(@PathVariable("id") Long id,Map<String, Object> model){
		Email email = emailService.find(id);
		model.put("email", email);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("emailId", id);
		List<EmailMember> emailMembers = emailMemberService.findMany(params);
		model.put("emailMembers", emailMembers);
		List<Member> members = memberService.getMembersByEmailId(id);
		String memberIds = "";
		if(null != members){
			for (int i = 0; i < members.size(); i++) {
				if(i==members.size()-1){
					memberIds += members.get(i).getId();
				}else{
					memberIds += members.get(i).getId()+",";
				}
			}
		}
		
		email.setMemberIds(memberIds);
		model.put("email", email);
		model.put("members", members);
		
		return getRelativeViewPath("edit");
	}
	
	@ResponseBody
	@RequestMapping("emailSend")
	public Map<String,Object> emailSend(Long[] ids,Map<String, Object> model){
		if(null != ids&&ids.length != 0){
			emailService.emailSend(ids);
		}
		return null;
	}
	
	
}
