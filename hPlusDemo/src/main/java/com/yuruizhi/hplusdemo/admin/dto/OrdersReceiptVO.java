/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: OrdersReceiptDTO.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dto
 * Date: 2016年5月3日下午5:50:08
 **/
package com.yuruizhi.hplusdemo.admin.dto;

import org.apache.commons.lang3.StringUtils;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;

/**      
 * <p>名称: 订单内容展现类</p>
 * <p>说明: 将订单内容查询出之后，部分字段在该类中转换，以实现方便的前端展示</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：Sun.Fan
 * @date：2016年5月3日下午5:50:11   
 * @version: 1.0 
 */
public class OrdersReceiptVO extends OrdersReceipt{
	private static final long serialVersionUID = 5665343370960305100L;
	
	/**@Fields orderTypeContext : 订单类型内容展示*/ 
	private String orderTypeContext;
	/**@Fields statusContext : 订单状态内容展示 */ 
	private String statusContext;
	/**@Fields goodTypeContext : 商品类型内容展示 */ 
	private String goodTypeContext;
	/**@Fields originateContext : 订单来源内容展示 */ 
	private String originateContext;
	/**@Fields address : 收货人地址 */ 
	private String address;
	/**@Fields recvUserName : 收货人姓名 */ 
	private String recvUserName;
	/**@Fields recvUserPhone : 收货人电话 */ 
	private String recvUserPhone;
	
	/**@Fields createdDate : 创建时间 */ 
	private String createdTime;
	
	public String getOrderTypeContext() {
		return orderTypeContext;
	}
	public String getStatusContext() {
		return statusContext;
	}
	public String getGoodTypeContext() {
		return goodTypeContext;
	}
	public String getOriginateContext() {
		return originateContext;
	}
	public String getAddress() {
		return address;
	}
	public String getRecvUserName() {
		return recvUserName;
	}
	public String getRecvUserPhone() {
		return recvUserPhone;
	}
	public void setOrderTypeContext(String orderTypeContext) {
		if(StringUtils.isEmpty(orderTypeContext)){
			Integer orderType = super.getOrderType();
			if(null == orderType){
				orderTypeContext = "";
			}else if(ORDERTYPE_COMMON == orderType){
				orderTypeContext = "普通订单";
			}else if(ORDERTYPE_RESERVE == orderType){
				orderTypeContext = "预售订单";
			}else{
				orderTypeContext = "";
			}
		}
		this.orderTypeContext = orderTypeContext;

	}
	
	public void setStatusContext(String statusContext) {
		if(StringUtils.isEmpty(statusContext)){
			Integer status = super.getStatus();
			if(null == status){
				statusContext = "";
			}else if(Constant.ORDERS_STATUS_SUBMIT == status){
				statusContext = "待支付";
			}else if(Constant.ORDERS_STATUS_COMPLETE_PACKAGE == status){
				statusContext = "待收货";
			}else if(Constant.ORDERS_STATUS_SHIPPED == status){
				statusContext = "已完成";
			}else if(Constant.ORDERS_STATUS_CANCELLED == status){
				statusContext = "已取消";
			}else if(Constant.ORDERS_STATUS_COMMENTED == status){
				statusContext = "已评价";
			}else if(Constant.ORDERS_STATUS_RETURN == status){
				statusContext = "订单退换货";
			}else if(Constant.ORDERS_STATUS_ISSUE_DELIVERY == status || Constant.ORDERS_STATUS_ISSUE == status){
				statusContext = "问题/缺货";
			}else{
				statusContext = "待发货";
			}
		}
		
		this.statusContext = statusContext;
	}
	
	public void setGoodTypeContext(String goodTypeContext) {
		if(StringUtils.isEmpty(goodTypeContext)){
			Integer goodType = super.getGoodType();
			if(null == goodType){
				goodTypeContext = "";
			}else if(GOODTYPE_RESERVE == goodType){
				goodTypeContext = "预订";
			}else if(GOODTYPE_EXISTING == goodType){
				goodTypeContext = "现货";
			}else if(GOODTYPE_LIMITED == goodType){
				goodTypeContext = "限购";
			}else{
				goodTypeContext = "";
			}
		}
		this.goodTypeContext = goodTypeContext;
	}
	
	public void setOriginateContext(String originateContext) {
		if(StringUtils.isEmpty(originateContext)){
			Integer originate = super.getOriginate();
			if(null == originate){
				originateContext = "";
			}else if(ORIGINATE_PC == originate){
				originateContext = "PC";
			}else if(ORIGINATE_APP == originate){
				originateContext = "APP";
			}else{
				originateContext = "";
			}
		}
		this.originateContext = originateContext;
	}
	
	public void setAddress(String address) {
		if(StringUtils.isEmpty(address)){
			String json = super.getShippingAddress();
			String provinceName = findByJson(json, "provinceName");
			String cityName = findByJson(json, "cityName");
			String areaName = findByJson(json, "areaName");
			String addressLine = findByJson(json, "addressLine");
			StringBuilder sb = new StringBuilder("");
			sb.append(provinceName).append(" ")
			.append(cityName).append(" ")
			.append(areaName).append(" ")
			.append(addressLine);
			address = convert(sb.toString());
			
		}
		this.address = address;
	}
	
	public void setRecvUserName(String recvUserName) {
		if(StringUtils.isEmpty(recvUserName)){
			String json = super.getShippingAddress();
			String value = findByJson(json, "firstName");
			recvUserName = convert(value);
			
		}
		this.recvUserName = recvUserName;
	}
	
	public void setRecvUserPhone(String recvUserPhone) {
		if(StringUtils.isEmpty(recvUserPhone)){
			String json = super.getShippingAddress();
			String value = findByJson(json, "phone1");
			recvUserPhone = value;
		}
		this.recvUserPhone = recvUserPhone;
		
	}
	
	/**
	 * <p>名称：从json字符串中取出响应字段的值</p> 
	 * <p>描述：仅为了方便的取订单信息中的收货人信息，该方法请不要复用</p>
	 * @author：Sun.Fan
	 * @param json
	 * @param name
	 * @return
	 */
	private static String findByJson(String json, String name){
		String result = "";
		if(StringUtils.isEmpty(json) || StringUtils.isEmpty(name) ){
			return result;
		}
		try {
			String[] array = json.split(name+"\" : ");
			if(array != null && array.length == 2){
				String value = array[1].split(",")[0];
				result = value.replace("\"", "").replace("}", "").trim();
			}
		} catch (Exception e) {
			return result;
		}
		return result;
	}
	
	/**
	 * <p>名称：转换字符串unicode转字符串</p> 
	 * <p>描述：转换字符串unicode转字符串</p>
	 * @author：Sun.Fan
	 * @param utfString
	 * @return
	 */
	public static String convert(String utfString){  
	    StringBuilder sb = new StringBuilder();  
	    int i = -1;  
	    int pos = 0;  
	      
	    while((i=utfString.indexOf("\\u", pos)) != -1){  
	        sb.append(utfString.substring(pos, i));  
	        if(i+5 < utfString.length()){  
	            pos = i+6;  
	            sb.append((char)Integer.parseInt(utfString.substring(i+2, i+6), 16));  
	        }  
	    }  
	      
	    return sb.toString();  
	}  
	
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

}
