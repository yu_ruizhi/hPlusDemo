/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.ticket.impl;


import com.yuruizhi.hplusdemo.admin.dao.ticket.TicketRefundDao;
import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketRefund;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;

import com.yuruizhi.hplusdemo.admin.service.ticket.TicketRefundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;

/**
 * <p>名称: ProductServiceImpl</p>
 * <p>说明: 产品服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:57:20   
 * @version: 1.0
 */
@Service
public class TicketRefundServiceImpl extends BaseServiceImpl<TicketRefund> implements TicketRefundService{

    private TicketRefundDao ticketRefundDao;

    @Autowired
    public void setAttributeDao(TicketRefundDao ticketRefundDao){
        this.ticketRefundDao = ticketRefundDao;
        setCrudDao(ticketRefundDao);
    }

    @Override
    public void updateType(Map<String, Object> map) {
        ticketRefundDao.updateType(map);
    }
}
