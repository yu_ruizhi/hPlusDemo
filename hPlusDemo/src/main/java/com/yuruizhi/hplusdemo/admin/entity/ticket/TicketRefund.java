/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.ticket;




import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;

import java.math.BigDecimal;
import java.util.List;

/**
 * 票务退换货
 */
public class TicketRefund extends BaseEntity {

    private Long memberId;

    private Long ticketId;

    private Long ticketCountId;

    private Long ticketOrderId;

    private Long ticketOrderDetailId;

    private String aliName;

    private String aliNameNo;

    private String phone;

    private BigDecimal price;

    private Integer qty;

    private Integer refundType;//1待审核 2已审核 3已完成 4已拒绝',

    private Boolean deleted;

    private TicketOrder ticketOrder;

    private List<TicketCount> ticketCountList;

    private Member member;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public List<TicketCount> getTicketCountList() {
        return ticketCountList;
    }

    public void setTicketCountList(List<TicketCount> ticketCountList) {
        this.ticketCountList = ticketCountList;
    }

    public TicketOrder getTicketOrder() {
        return ticketOrder;
    }

    public void setTicketOrder(TicketOrder ticketOrder) {
        this.ticketOrder = ticketOrder;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getTicketCountId() {
        return ticketCountId;
    }

    public void setTicketCountId(Long ticketCountId) {
        this.ticketCountId = ticketCountId;
    }

    public Long getTicketOrderId() {
        return ticketOrderId;
    }

    public void setTicketOrderId(Long ticketOrderId) {
        this.ticketOrderId = ticketOrderId;
    }

    public Long getTicketOrderDetailId() {
        return ticketOrderDetailId;
    }

    public void setTicketOrderDetailId(Long ticketOrderDetailId) {
        this.ticketOrderDetailId = ticketOrderDetailId;
    }

    public String getAliName() {
        return aliName;
    }

    public void setAliName(String aliName) {
        this.aliName = aliName;
    }

    public String getAliNameNo() {
        return aliNameNo;
    }

    public void setAliNameNo(String aliNameNo) {
        this.aliNameNo = aliNameNo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getRefundType() {
        return refundType;
    }

    public void setRefundType(Integer refundType) {
        this.refundType = refundType;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
