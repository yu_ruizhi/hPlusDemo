package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.beans.Field;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.dao.product.IndexingGoodsDao;
import com.yuruizhi.hplusdemo.admin.dto.IndexedGoods;
import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;
import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Label;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductImg;
import com.yuruizhi.hplusdemo.admin.service.product.IndexingService;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * @author vacoor
 */
@Service
public class IndexingServiceImpl extends IndexingSupport implements IndexingService {
    private static final Logger LOG = LoggerFactory.getLogger(IndexingServiceImpl.class);

    public static final String MIX_VAL_DELIMITER = "::";        // 混合值分隔符  123::中文::345::语言
    public static final String CAT_FIELD = "cat";               // 分类字段
    public static final String SHOP_FIELD = "shop";               // 分类字段
    public static final String SHOP_GROUP_FIELD = "group";               // 分类字段
    public static final String CAT_VAL_PREFIX = CAT_FIELD + "_";
    public static final String BRAND_FIELD = "brand";           // 品牌字段
    public static final String BRAND_VAL_FIELD = "brandName";   // 品牌字段
    public static final String PRICE_FIELD = "price";           // 价格字段
    public static final String DYN_PROPS_FIELD = "props";       // 动态属性列表字段
    public static final String DYN_PROP_PREFIX = "prop_";       // 动态属性字段前缀
    public static final String DYN_PROP_VAL_PREFIX = "prop_v_"; // 动态属性值字段前缀
    public static final String SORT_DELIMITER = "-";
    public static final String SORT_DESC = "desc";
    public static final String PROP_VALUE_PATH_DELIMITER = ":";
    public static final String CAT_PATH_PREFIX = "cat_path_";


    private static class GoodsIndexedBean {
        @Field
        private Long id;      // 货品id
        @Field
        private String code;   // 货品编码
        @Field
        private String title;   // 货品标题
        @Field
        private String image;   // 货品图片
        @Field
        private String price;   // 货品价格
        @Field
        private String marketPrice; // 货品市场价
        @Field
        private String unit;          // 点击量
        @Field("onSellDate")
        private Date onSellDate;
        @Field
        private Long hits;          // 点击量
        @Field("saleNum")
        private Long saleNum;         // 销量
        @Field("brand")
        private Long brandId;
        // 品牌id
        @Field("brandName")
        private String brandName;   // 品牌名称
        @Field("group")
        private Integer shopGroup;       // 商品归属，热销，特价等
        @Field("shop")
        private Long shopId;
        // 店铺id
        @Field("shopName")
        private String shopName;   // 店铺名称
        @Field("shopLogo")
        private String shopLogo;   // 店铺 LOGO
        
        @Field("works")
        private Long works;            //作品ID
        @Field("worksName")
        private String worksName;		 //作品名
        @Field("reserved")
        private Boolean reserved;		 //是否预定产品
        @Field("limited")
        private Boolean limited; //是否限购
        @Field("spot")
        private Boolean spot;    //是否现货
        @Field("reserveDeadline")
        private Date reserveDeadline;    //新增“预定截止时间”字段 20160909 yuruizhi
        @Field("limitedDeadLine")
        private Date limitedDeadLine;    //新增“限购截止时间”
        @Field("appIntro")
        private String appIntro;         //app商品简介
        @Field("parentCat")
        private Long parentCat; // 分类id
        //@Field("cat")
        //private List<String> catIds = Lists.newArrayList(); // 分类id集合
        @Field("cat")
        private Long cat; // 分类id
        @Field("cat_*")
        private Map<String, String> catsAndParent = Maps.newHashMap();  // 分类名称及父分类
        @Field("props")
        private List<Long> propKeys = Lists.newArrayList();   // 所有的属性
        @Field("prop_*")
        private Map<String, Long> propAndValueMap = Maps.newHashMap();    // 所有的属性:属性值
        @Field("prop_v_*")
        private Map<String, String> propAndValueNames = Maps.newHashMap();  // 所有的属性和属性值名称
        @Field("images")
        public List<String> images;
        @Field("labels")
        private List<String> labels = Lists.newArrayList();                //标签
        @Field("trend")
        private Integer trend;
        @Field("trendName")
        private String trendName;
    }

    @Autowired
    private IndexingGoodsDao indexingGoodsDao;

    @Override
    protected Object getIndexedBean(String id) {
        Long lid = null;
        try {
            lid = Long.valueOf(id);
        } catch (NumberFormatException nfe) {
            // ignore
        }
        return null != lid ? toIndexedBean(indexingGoodsDao.find(lid)) : null;
    }

    @Override
    protected long countIndexedBeans() {
        return indexingGoodsDao.count();
    }

    @Override
    protected List<?> getPagedIndexedBeans(int offset, int size) {
        Map<String, Object> paramsMap = Maps.newHashMap();
        paramsMap.put(IndexingGoodsDao.OFFSET, offset);
        paramsMap.put(IndexingGoodsDao.MAX_RESULTS, size);
        return Lists.transform(indexingGoodsDao.findMany(paramsMap), new Function<IndexedGoods, Object>() {
            @Override
            public Object apply(IndexedGoods indexedGoods) {
                return toIndexedBean(indexedGoods);
            }
        });
    }

    protected Object toIndexedBean(IndexedGoods g) {//********************
        if (null == g) {
            return null;
        }
        GoodsIndexedBean bean = new GoodsIndexedBean();
        bean.id = g.getId();
        bean.code = g.getCode();
        bean.title = g.getTitle();
        if (!StringUtils.hasText(bean.title)) {
            bean.title = g.getProductTitle();
        }
        bean.image = g.getImage();
        bean.price = g.getPrice();
        bean.marketPrice = g.getMarketPrice();
        bean.unit = g.getUnit();
        bean.hits = g.getHits();
        bean.reserveDeadline = g.getReserveDeadline();
        bean.limitedDeadLine=g.getLimitedDeadLine();
        bean.saleNum = g.getSales();
        bean.brandId = g.getBrandId();
        bean.brandName = g.getBrandName();
        bean.shopGroup = g.getShopGroup();
        bean.shopId = g.getShopId();
        bean.shopName = g.getShopName();
        bean.shopLogo = g.getShopLogo();
        
        bean.works = g.getWorksId();
        bean.worksName = g.getWorksName();
        bean.reserved = g.getReserved();
        bean.limited = g.getLimited();
        bean.spot = g.getSpot();
        bean.appIntro = g.getAppIntro();
        bean.trend = g.getTrend();
        bean.onSellDate=g.getOnSellDate();
        
        if(null != g.getTrend()){
        	if(g.getTrend() == 0){
        		bean.trendName = "无差别";
        	}else if(g.getTrend() == 1){
        		bean.trendName = "男性向";
        	}else{
        		bean.trendName = "女性向";
        	}
        }
        bean.labels = Lists.transform(g.getLabels(), new Function<Label, String>() {

			@Override
			public String apply(Label input) {
				String reslut = input.getName();
				return reslut;
			}
			
		});
        
        bean.images = Lists.transform(g.getImages(), new Function<ProductImg, String>() {
            @Override
            public String apply(ProductImg productImg) {
                return productImg.getPath();
            }
        });
        
        //添加分类
        Category categroy = g.getCategory();
        if(null != categroy){
        	bean.cat = categroy.getId();
        	bean.catsAndParent.put(CAT_VAL_PREFIX + categroy.getId(), categroy.getName());
        	if(null != categroy.getParentId()){
        		bean.catsAndParent.put(CAT_PATH_PREFIX + categroy.getParentId(),categroy.getId()+"");
        	}
        }
        
        bean.parentCat = categroy.getParentId()==null?categroy.getId():categroy.getParentId();
        
        // 添加所有分类
       /* List<Category> cats = g.getCats();
        if (null != cats) {
            for (Category cat : cats) {
                
                for (; null != cat; cat = cat.getParent()) {
                    Long catId = cat.getId();
                    String catName = cat.getName();
                    Category p = cat.getParent();
                    String value = null == p ? catName : catName + MIX_VAL_DELIMITER + p.getId() + MIX_VAL_DELIMITER + p.getName();

                    bean.catIds.add(catId + "");
                    bean.catsAndParent.put(CAT_VAL_PREFIX + catId, value);
                }
                
                String path = cat.getPath();
                String pathNames = cat.getPathNames();
                if (null == path || null == pathNames) {
                    continue;
                }
                String[] ids = path.split(",");
                String[] names = pathNames.split("/\\$");
                int len = ids.length;

                // 不合法数据
                if (ids.length != names.length) {
                    len = Math.min(ids.length, names.length);
                }

                for (int i = 0; i < len; i++) {
                    String catId = ids[i];

                    if (!StringUtils.hasText(catId)) {
                        continue;
                    }

                    String catName = names[i];
                    Category p = cat.getParent();
                    String value = null == p ? catName : catName + MIX_VAL_DELIMITER + p.getId() + MIX_VAL_DELIMITER + p.getName();

                    bean.catIds.add(catId + "");
                    bean.catsAndParent.put(CAT_VAL_PREFIX + catId, value);
                    bean.catsAndParent.put(CAT_PATH_PREFIX + catId,cat.getParentId()+"");
                    if(null != cat.getParentId()){
                    	 bean.catsAndParent.put(CAT_PATH_PREFIX + catId,cat.getParentId()+"");
                    }
                    
                }
            }
        }*/
        List<AttributeValue> values = g.getPropVals();
        if (!CollectionUtils.isEmpty(values)) {
            for (AttributeValue value : values) {
                Attribute prop = value.getAttribute();
                if (null == prop) {
                    continue;
                }

                Long sid = prop.getId();
                String sname = prop.getName();
                Long vid = value.getId();
                String firstValue = value.getValue();

                bean.propKeys.add(sid);
                bean.propAndValueMap.put(DYN_PROP_PREFIX + sid, vid);
                bean.propAndValueNames.put(DYN_PROP_VAL_PREFIX + vid, sname + MIX_VAL_DELIMITER + firstValue);
            }
        }
        return bean;
    }


    @Override
    public void updateIndex(String id, boolean delete) {
        doUpdate(id, delete);
    }

    //@Scheduled(cron = "0 0/30 * * * ?")
    @Scheduled(cron = "0 0 0/1 * * ?")
    @Override
    public void rebuildIndex() {
        LOG.info("rebuild index....");
        doReindex();
    }

    @Override
    public void truncate() {
        doTruncate();
    }

    @Autowired
    @Override
    public void setSolrClient(SolrClient solrClient) {
        super.setSolrClient(solrClient);
    }
}
