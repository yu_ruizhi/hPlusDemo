/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ApplicationUtil.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.util
 * Date: 2016年8月10日上午11:53:45
 **/
package com.yuruizhi.hplusdemo.admin.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * <p>名称: 实现spring 的ApplicationContextAware接口，获取spring 配置中的bean</p>
 * <p>说明: TODO</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：Administrator
 * @date：2016年8月10日上午11:55:10   
 * @version: 1.0
 */
public class ApplicationUtil implements ApplicationContextAware{

	private static ApplicationContext applicationContext;  
    
	@Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationUtil.applicationContext = applicationContext;
    }
    public static Object getBean(String name){
    	 if(null == ApplicationUtil.applicationContext){
         	ApplicationUtil.applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
         }
        return applicationContext.getBean(name);
    }
	
}
