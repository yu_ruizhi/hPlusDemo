package com.yuruizhi.hplusdemo.admin.service.logistics;


import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModule;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by ASUS on 2015/1/13.
 */
public interface DeliveryModuleService extends BaseService<DeliveryModule> {

    void saveModule(DeliveryModule deliveryModule);

    void updateModule(DeliveryModule deliveryModule);
}
