/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import java.math.BigDecimal;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: CouponType</p>
 * <p>说明: 优惠券模板实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:53:59   
 * @version: 1.0
 */
public class CouponType extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1612239351056821779L;

	/**
     * 优惠券类型名称
     * 优惠券名称
     * 
     * @ViewField editor=input 
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 券面值
     * 
     * @ViewField editor=input 
     * @Column AMOUNT
     */
    @NotNull
    private BigDecimal amount;

    /**
     * 最低使用金额
     * 
     * @ViewField editor=input 
     * @Column MIN_USAGE_AMOUNT
     */
    @NotNull
    private BigDecimal minUsageAmount;

    /**
     * 是否自动发放
     * 
     * @ViewField editor=input 
     * @Column AUTO_SEND
     */
    @NotNull
    private Boolean autoSend = Boolean.FALSE; ;

    /**
     * 自动发放最低金额
     * 
     * @ViewField editor=input 
     * @Column SEND_MIN_AMOUNT
     */
    private BigDecimal sendMinAmount;

    /**
     * 自动发放最大金额
     * 
     * @ViewField editor=input 
     * @Column SEND_MAX_AMOUNT
     */
    private BigDecimal sendMaxAmount;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    /**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the minUsageAmount
	 */
	public BigDecimal getMinUsageAmount() {
		return minUsageAmount;
	}

	/**
	 * @param minUsageAmount the minUsageAmount to set
	 */
	public void setMinUsageAmount(BigDecimal minUsageAmount) {
		this.minUsageAmount = minUsageAmount;
	}

	/**
	 * @return the sendMinAmount
	 */
	public BigDecimal getSendMinAmount() {
		return sendMinAmount;
	}

	/**
	 * @param sendMinAmount the sendMinAmount to set
	 */
	public void setSendMinAmount(BigDecimal sendMinAmount) {
		this.sendMinAmount = sendMinAmount;
	}

	/**
	 * @return the sendMaxAmount
	 */
	public BigDecimal getSendMaxAmount() {
		return sendMaxAmount;
	}

	/**
	 * @param sendMaxAmount the sendMaxAmount to set
	 */
	public void setSendMaxAmount(BigDecimal sendMaxAmount) {
		this.sendMaxAmount = sendMaxAmount;
	}

	public Boolean getAutoSend() {
        return this.autoSend;
    }

    public void setAutoSend(Boolean autoSend) {
        this.autoSend = autoSend;
    }


    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
