/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity;

import org.ponly.webbase.entity.PathTreeNode;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * <p>名称: TreeNodeBaseEntity</p>
 * <p>说明: 属性接口基础实体</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:27:06   
 * @version: 1.0
 */
public abstract class TreeNodeBaseEntity<N extends TreeNodeBaseEntity<N>> extends BaseEntity implements PathTreeNode<N, Long> {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 3765552127812287862L;
	protected N parent;
    protected List<N> children;
    /**
     * 层级/深度
     *
     * @Column DEPTH
     */
    private Integer depth;

    /**
     * 路径
     *
     * @Column PATH
     */
    @Size(max = 255)
    private String path;

    /**
     * 是否叶子节点
     *
     * @Column IS_LEAF
     */
    private Boolean isLeaf = Boolean.FALSE;


    @Override
    public Long getNid() {
        return this.getId();
    }

    @Override
    public void setNid(Long id) {
        this.setId(id);
    }

    /*
    @ManyToOne(optional = true)
    @JoinColumn(name = "PARENT_ID")
    @JsonManagedReference
    */
    @Override
    public N getParent() {
        return parent;
    }

    @Override
    public void setParent(N parent) {
        this.parent = parent;
    }

    /*
    @JsonBackReference
    @OneToMany(mappedBy = "parent")
    */
    @Override
    public List<N> getChildren() {
        return children;
    }

    @Override
    public void setChildren(List<N> children) {
        this.children = children;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public Integer getDepth() {
        return depth;
    }

    @Override
    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    @Override
    public Boolean getIsLeaf() {
        return isLeaf;
    }

    @Override
    public void setIsLeaf(Boolean isLeaf) {
        this.isLeaf = isLeaf;
    }
}
