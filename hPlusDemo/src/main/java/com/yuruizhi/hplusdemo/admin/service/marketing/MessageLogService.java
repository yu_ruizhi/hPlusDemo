/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageMemberService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月20日下午2:24:44
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageLog;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.List;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

/**
 * 
 * <p>名称: MessageLogService</p>
 * <p>说明: 私信记录Service</p>
 * <p>创建记录：（2016.8.19 14:13 - yuruizhi - 增加私信记录Service）</p>
 * <p>修改记录：（2016.8.19 15:00 - yuruizhi - 增加私信发送Service）</p>
 *
 * @author yuruizhi
 * @date 2016年8月19日 下午14:17:06
 * @version: 1.0
 */
public interface MessageLogService extends BaseService<MessageLog>{
	
	/**
	 * 
	 * <p>名称：根据会员ID查询记录</p>
	 * <p>描述：</p>
	 * @author yuruizhi
	 * @param memberId 会员ID
	 * @return
	 */
	List<MessageLog> findByMemberId(String memberId);
	
	Page<MessageLog> findPage(
			Filters memberFilters,
			Pageable pageable 
    );
}
