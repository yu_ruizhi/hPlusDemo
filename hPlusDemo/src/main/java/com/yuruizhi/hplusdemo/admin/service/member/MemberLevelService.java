/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MemberLevelService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.member
 * Date: 2016年4月18日 上午11:48:20  
 **/
package com.yuruizhi.hplusdemo.admin.service.member;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.member.MemberLevel;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 会员等级Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月18日 上午11:48:20  
 * @version: 1.0
 */
public interface MemberLevelService extends BaseService<MemberLevel>{

	/**
	 * 
	 * <p>名称：根据会员等级名称检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name 会员等级名称
	 * @return 返回true,false
	 */
	Map<String, Boolean> checkIsNameExists(String name);
	
	/**
	 * 
	 * <p>名称：根据会员等级id及会员等级名称检测重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param hasAnnotherName 会员等级id，会员等级名称数组
	 * @return 返回true,false
	 */
	Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName);
	
}
