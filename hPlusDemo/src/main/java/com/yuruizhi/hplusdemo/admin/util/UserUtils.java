/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import com.yuruizhi.hplusdemo.admin.controller.product.ProductController;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import org.springframework.web.context.request.RequestContextHolder;

import com.yuruizhi.hplusdemo.admin.entity.perm.Module;

import static com.yuruizhi.hplusdemo.admin.util.CacheUtils.*;

import java.util.List;
import java.util.Set;

/**
 * <p>名称: UserUtils</p>
 * <p>说明: 后台用户工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:34:41   
 * @version: 1.0
 */
public abstract class UserUtils {
    private static final String AUTHORIZE_PREFIX_CURRENTUSER = "currentUser_";
    private static final String AUTHORIZE_PREFIX_PERMISSION = "permission_";

    public static final String AUTHENTICATION_CACHE_NAME = "authz.cache";

    public static final String ROLE_CATEGORY_IDS = "roleCategoryIds";

    /**
     * 获取当前用户ID, 如果不存在返回 null
     *
     * @return 当前用户 id
     */
    public static Long getCurrentUserId() {
        Object value = get(AUTHENTICATION_CACHE_NAME, getCurrentUserAuthenticationCacheKey());
        if (value instanceof String) {
            return Long.valueOf((String) value);
        }
        return (Long) value;
    }
    
    /**
     * 将用户ID放入缓存
     *
     * @param uid 用户 id
     */
    public static void setCurrentUserId(Long uid) {
        put(AUTHENTICATION_CACHE_NAME, getCurrentUserAuthenticationCacheKey(), uid);
    }

    //将用户数据权限分类集合放入缓存中
    public static void setRoleCategoryIds(Set<Long> roleCategoryIds) {
        String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
        put(AUTHENTICATION_CACHE_NAME, ROLE_CATEGORY_IDS+sessionId, roleCategoryIds);
    }
    //取出用户数据分类权限
    public static Set<Long> getRoleCategoryIds(){
        String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
        Object value = get(AUTHENTICATION_CACHE_NAME, ROLE_CATEGORY_IDS+sessionId);
        if(null==value){
            return null;
        }else{
            return (Set<Long>)value;
        }
    }

    /**
     * 将当前用户信息从缓存中清空
     */
    public static void cleanup() {
        Long uid = getCurrentUserId();
        evictFromAuthenticationCache(getCurrentUserAuthenticationCacheKey());
        evictFromAuthenticationCache(getPermissionsCacheKey(uid));
        evictFromAuthenticationCache(ROLE_CATEGORY_IDS+RequestContextHolder.getRequestAttributes().getSessionId());
        evictFromAuthenticationCache(ProductController.PRODUCT_IMPORT_ERROR+uid);
        evictFromAuthenticationCache(ProductController.PRODUCT_CF_ERROR+uid);
    }

    /**
     * 获取当前用户缓存的权限信息
     *
     * @return 权限信息，如果不存在则返回 null
     */
    public static List<Module> getCurrentUserCachedPermissions() {
        return getCachedPermissions(getCurrentUserId());
    }

    /**
     * 缓存给定用户的权限信息
     *
     * @param uid     用户 id
     * @param modules 权限信息
     */
    public static void cachePermissions(Long uid, List<Module> modules) {
        String key = getPermissionsCacheKey(uid);
        putToAuthenticationCache(key, modules);
    }

    /**
     * 获取缓存的用户权限信息
     *
     * @param uid 用户 id
     * @return 缓存的权限信息
     */
    public static List<Module> getCachedPermissions(Long uid) {
        String key = getPermissionsCacheKey(uid);
        return getFromAuthenticationCache(key);
    }


    /**
     * 缓存导入商品信息
     *
     */
    public static void setCacheProduct(String key, List<String> data) {
        putToAuthenticationCache(key, data);
    }

    /**
     * 缓存导入重复商品信息
     *
     */
    public static void setCfProduct(String key, List<Product> data) {
        putToAuthenticationCache(key, data);
    }

    /**
     * 获取缓存导入商品信息
     *
     * @param uid 用户 id
     * @return 缓存的权限信息
     */
    public static List<String> getCacheProduct(String key) {
        return getFromAuthenticationCache(key);
    }

    /**
     * 获取缓存重复商品信息
     *
     * @param uid 用户 id
     * @return 缓存的权限信息
     */
    public static List<Product> getCfProduct(String key) {
        return getFromAuthenticationCache(key);
    }

    /**
     * 清楚给定用户缓存的权限信息
     *
     * @param uid 用户id
     */
    public static void evictCachedPermissions(Long uid) {
        String key = getPermissionsCacheKey(uid);
        evictFromAuthenticationCache(key);
    }

    /**
     * 从认证信息缓存中获取给定 key 对应的值
     *
     * @param key Key
     * @param <V> 值类型
     * @return 缓存的值
     */
    public static <V> V getFromAuthenticationCache(String key) {
        return get(AUTHENTICATION_CACHE_NAME, key);
    }

    /**
     * 将给定的值 缓存在认证信息缓存中
     *
     * @param key   key
     * @param value value
     */
    public static void putToAuthenticationCache(String key, Object value) {
        put(AUTHENTICATION_CACHE_NAME, key, value);
    }

    /**
     * 将给定 key 对应的缓存值从认证缓存中移除
     *
     * @param key Key
     */
    public static void evictFromAuthenticationCache(String key) {
        evict(AUTHENTICATION_CACHE_NAME, key);
    }

    /**
     * 清空认证缓存
     */
    public static void clearAuthenticationCache() {
        clear(AUTHENTICATION_CACHE_NAME);
    }

    protected static String getCurrentUserAuthenticationCacheKey() {
        String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
        return AUTHORIZE_PREFIX_CURRENTUSER + sessionId;
    }

    protected static String getPermissionsCacheKey(Long uid) {
        return AUTHORIZE_PREFIX_PERMISSION + uid;
    }

    private UserUtils() {
    }
}
