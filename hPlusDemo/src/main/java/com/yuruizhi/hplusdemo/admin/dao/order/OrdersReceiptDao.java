package com.yuruizhi.hplusdemo.admin.dao.order;




import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;

/**
 * Created on 2014-08-12 14:17:17
 *
 * @author crud generated
 */
public interface OrdersReceiptDao extends BaseDao<OrdersReceipt> {
    String USER_FILTERS_PROP = "_user_filters"; // 前台用户
    String ORDER_FILTERS_PROP = "_order_filters";
    String SEARCH_FILTERS_PROP = "_filters";

	
	/**
	 * @param paramMap userId 用户ID， orderStatus订单状态， orderType 订单类型
	 * @return 特定条件下的订单总数
	 * @description 根据用户ID，订单状态，订单类型查询订单的总数
	 */

	int getCountByParams(Map<String, Object> paramMap);

    
	OrdersReceipt findDetailById(Long id);
    List<OrdersReceipt>  selectAll();
    Integer booleanLogisticsNo(String logisticsNo);
    void addLogisticsNoz(OrdersReceipt ordersReceipt);
    void updateStatus(Long id);
	
	/**
	 * @description 通用状态修改. <br/>
	 * @author 侯澎帅
	 * @param ordersId 订单id
	 * @param status 需要修改的状态
	 * @return
	 * @since JDK 1.7
	 * @change Log:
	 */
	int updateOrdersReceipt(Long ordersId, int status);

    //根据订单id集合修改预售订单的状态
    void updatePresaleStatus(Map<String,Object> map);

    OrdersReceipt findOrdersReceipt(Long id);

    void updateActualAmount(OrdersReceipt ordersReceipt);
    
    void cancelOrder(OrdersReceipt ordersReceipt);
    
    void updateDiscountAmount(OrdersReceipt ordersReceipt);

    void updateExpiredTime(Map<String,Object> map);
    
    /**
     * 
     * <p>名称：根据时间获取订单id</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    Long[] findIdByTime(Map<String,Object> map);
}