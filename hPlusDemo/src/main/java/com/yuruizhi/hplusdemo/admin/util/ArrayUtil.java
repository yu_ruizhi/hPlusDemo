/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import org.springframework.util.StringUtils;

/**
 * <p>名称: ArrayUtil</p>
 * <p>说明: 数组工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:30:17   
 * @version: 1.0
 */
public class ArrayUtil {
	
	public static String arrayToString(String[] args){
		StringBuffer result = new StringBuffer();
		for (int i=0; i<args.length; i++) {
			result.append(args[i]);
			if(i != args.length-1){
				result.append(",");
			}
		}
		return result.toString();
	}
	
	public static String[] stringToArray(String arg){
		if (!StringUtils.hasText(arg)){
			return new String[0];
		}
		String[] result = arg.split(",");
		return result;
	}

    public static boolean hasNotEmpty(String[] array) {
        if (null == array) {
            return false;
        }
        for (String s : array) {
            if (!StringUtils.isEmpty(s)) {
                return true;
            }
        }
        
        return false;
    }
}
