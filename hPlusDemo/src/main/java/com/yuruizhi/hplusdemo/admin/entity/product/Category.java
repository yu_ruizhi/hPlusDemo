/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.util.List;
import java.util.Set;

import com.yuruizhi.hplusdemo.admin.entity.TreeNodeBaseEntity;

/**
 * 产品分类
 *
 * @author yangchanghe
 * @Table TB_CATEGORY
 */
public class Category extends TreeNodeBaseEntity<Category> {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = -692833446023631460L;
	/**
     * 分类名称
     *
     * @Column
     */
    private String name;
    
    
    private Integer depth;


	public Integer getDepth() {
		return depth;
	}

	public void setDepth(Integer depth) {
		this.depth = depth;
	}

	/**
     * 名称高亮色
     *
     * @Column
     */
    private String color;
    /**
     * 是否可见/显示
     *
     * @Column
     */
    private Boolean visible;
    /**
     * 排序
     *
     * @Column
     */
    private Integer sort;

    /**
     * @OneToOne
     */
    private Model model;
    /**
     * 是否删除
     *
     * @Column
     */
    private Boolean deleted = false;
    /**
     * 关联品牌集合
     *
     * @OneToMany optional=true
     */
    private List<Brand> brands;

    private String pathNames;
    
    /**
     * 父ID
     */
    private Long parentId;

    private String image;

    private List<Category> children;

    private List<Works> works;

    /**@Fields highlightImage : 高亮ICON */
    private String highlightImage;

    public String getHighlightImage() {
        return highlightImage;
    }

    public void setHighlightImage(String highlightImage) {
        this.highlightImage = highlightImage;
    }

    public List<Works> getWorks() {
        return works;
    }

    public void setWorks(List<Works> works) {
        this.works = works;
    }

    private Set<Works> worksSet;

    public Set<Works> getWorksSet() {
        return worksSet;
    }

    public void setWorksSet(Set<Works> worksSet) {
        this.worksSet = worksSet;
    }

    @Override
    public List<Category> getChildren() {
        return children;
    }

    @Override
    public void setChildren(List<Category> children) {
        this.children = children;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Brand> getBrands() {
        return brands;
    }

    public void setBrands(List<Brand> brands) {
        this.brands = brands;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public Category getParent() {
        return parent;
    }

    @Override
    public void setParent(Category parent) {
        this.parent = parent;
    }

    // for easyui
    public String getState() {
        Boolean isLeaf = getIsLeaf();
        return (isLeaf == null || isLeaf) ? "open" : "closed";
    }

    public String getPathNames() {
        return pathNames;
    }

    public void setPathNames(String pathNames) {
        this.pathNames = pathNames;
    }

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
}
