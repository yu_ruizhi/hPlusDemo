/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.product.GoodsDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;

/**
 * <p>名称: GoodsServiceImpl</p>
 * <p>说明: 商品服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:52:14   
 * @version: 1.0
 */
@Service
public class GoodsServiceImpl extends BaseServiceImpl<Goods> implements GoodsService {


    private GoodsDao goodsDao;


    @Autowired
    public void setAdminUserDao(GoodsDao goodsDao){
        this.goodsDao = goodsDao;
        setBaseDao(goodsDao);
    }


    @Override
    public Goods getGoodsById(String id) {
        return goodsDao.findGoodsDetailById(id);
    }

    @Override
    public String checkStatusByIdAndNum(String goodsId, Integer goodsNum) {
        String result = "";
        Goods goods = goodsDao.findGoodsDetailById(goodsId);
        if (null == goods || null == goods.getProduct()) {
            // 不存在 || 已删除
            result = Constant.GOODS_STATUS_NO_EXISTS;
        } else {
            if (null == goods.getProduct().getOnSell() || !goods.getProduct().getOnSell()) {
                // 已下架
                result = Constant.GOODS_STATUS_NO_PUTAWAY;
            } else {
                /*根据单品goods库存判断库存状态*/
               if (goods.getStock()>=goodsNum) {
                    result = Constant.GOODS_STATUS_NORMAL;
               } else if(goods.getStock()<goodsNum){
                    result = Constant.GOODS_STATUS_STOCK_SHORTAGE;
               }
            }
        }
        return result;
    }

    @Override
    public Map<String, Object> getGoodsDetailAndStatus(String goodsId, int goodsNum) {
        Map<String, Object> result = new HashMap<String, Object>();
        // 获取单品基本信息
        Goods goods = goodsDao.findGoodsDetailById(goodsId);

        // 单品不存在 || 商品不存在 || 商品已删除 || 单品已删除
        if (null == goods || null == goods.getProduct()) {
            result.put("status", Constant.GOODS_STATUS_NO_EXISTS);
        } else {
            if (null == goods.getProduct().getOnSell() || !goods.getProduct().getOnSell()) {
                // 商品/单品已下架
                result.put("status", Constant.GOODS_STATUS_NO_PUTAWAY);
            } else {
                /*根据单品goods库存判断库存状态*/

                if (goods.getStock()>=goodsNum) {
                    result.put("status", Constant.GOODS_STATUS_NORMAL);
                } else if(goods.getStock()<goodsNum){
                    result.put("status", Constant.GOODS_STATUS_STOCK_SHORTAGE);
                }

            }
        }
        result.put("goods", goods);
        return result;
    }

    @Override
    public List<Goods> selectCategoryChildren(String cid) {
        return goodsDao.selectCategoryChildren(cid);
    }

    @Override
    public List<Goods> selectLike(Map<String ,Object> map) {
        return goodsDao.selectLike(map);
    }


	@Override
	public Goods findGoodsCategoryById(Long goodsId) {
		return goodsDao.findGoodsCategoryById(goodsId);
	}

    @Override
    public void updateShoppingGoods(Goods goods) {
        goodsDao.updateShoppingGoods(goods);
    }


	@Override
	public List<Goods> findByProductId(Long productId) {
		return goodsDao.findByProductId(productId);
	}


	@Override
	public List<Map<String, Object>> getNewGoods(Map<String,Object> map) {
		// TODO Auto-generated method stub
		return goodsDao.getNewGoods(map);
	}

    @Override
    public void updateReserveDeadline(Map<String, Object> map) {
        goodsDao.updateReserveDeadline(map);
    }

    @Override
    public Integer findGoodsCount(Map<String, Object> map) {
        Integer count=goodsDao.findGoodsCount(map);
        if(null == count){
        	return 0;
        }
        return count ;
    }

    @Override
    public void updateproduct(Map<String, Object> map) {
        goodsDao.updateproduct(map);
    }

    @Override
    public void updategoods(Map<String, Object> map) {
        goodsDao.updategoods(map);
    }

    @Override
    public Integer findGoodszCount(Map<String, Object> map) {
        return goodsDao.findGoodszCount(map);
    }
}
