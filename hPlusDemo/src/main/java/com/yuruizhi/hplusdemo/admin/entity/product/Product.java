/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>名称: Product</p>
 * <p>说明: 产品信息实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:24:09   
 * @version: 1.0
 */
public class Product extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -4700747718158706458L;
	/**
     * 产品标题
     * @Column
     */
    private String title;
    /**
     * 产品简介/卖点
     * @Column
     */
    private String intro;
    /**
     * 产品编码
     * @Column
     */
    private String code;
    /**
     * 产品条形码
     * @Column
     */
    private String barcode;
    /**
     * 价格
     * @Column
     */
    private BigDecimal price;
    /**
     * 市场价
     * @Column
     */
    private BigDecimal marketPrice;
    /**
     * 库存
     * @Column
     */
    private Long stock;
    /**
     * 锁定库存
     * @Column
     */
    private Long lockedStock;
    /**
     * 损坏库存
     * @Column
     */
    private Long damagedStock;
    /**
     * 其他库存
     * @Column
     */
    private Long otherStock;
    /**
     * 供应商
     */
    private String suppliers;

	public String getSuppliers() {
		return suppliers;
	}

	public void setSuppliers(String suppliers) {
		this.suppliers = suppliers;
	}
	 /**
     * 产地
     */
    private String production;

	public String getProduction() {
		return production;
	}

	public void setProduction(String production) {
		this.production = production;
	}
	// 长宽高重
    /**
     * @Column
     */
    private Float length;
    /**
     * @Column
     */
    private Float width;
    /**
     * @Column
     */
    private Float height;
    /**
     * @Column
     */
    private Float weight;
    /**
     * 产品主图
     * @Column
     */
    private String image;
    /**
     * 产品大图
     * @Column
     */
    private String bigImage;
    /**
     * 供应商
     * @Column
     */
    private Supplier supplier;

	public String getBigImage() {
		return bigImage;
	}

	public void setBigImage(String bigImage) {
		this.bigImage = bigImage;
	}

	/**
     * 产品图片
     * @OneToMany fetch=eager optional=true
     */
    private List<ProductImg> productImgs;
    /**
     * 属性值实体集合
     */
    private List<AttributeValue> attributeValues;
    /**
     * 参数值实体集合
    */
    private List<ParameterValue> parameterValues;

    /**
     * 产品详情
     * @Column
     */
    private String detail;

    /**
     * 产品点击量
     * @Column
     */
    private Long hits;
    /**
     * 产品销量
     * @Column
     */
    private Long saleNum;

    /**
     * 是否在售/上架
     * @Column
     */
    private Boolean onSell = false;
    /**
     * 上架时间
     */
    private Date registerDate;

    /**
     * 下架时间
     */
    private Date salesOffDate;
    /**
     * 标签实体集合
     */
    private List<Label> labels;

    /**
     * @Column
     */
    private Boolean deleted = false;

    /**
     * 产品分类
     * @ManyToOne optional=true
     */
    private Category category;
    /**
     * 产品品牌
     * @ManyToOne optional=true
     */
    private Brand brand;
    
    /**
     * 产品所属作品
     * @ManyToOne optional=true
     */
    private Works works;

    /**
     * 是否开启规格
     */
    private Boolean enableSpecs = false;
    /**
     * 单品实体集合
     */
    @JsonBackReference
    private List<Goods> goods;

    /**
     * 商品状态-预定
     */
    private Boolean reserved = false;
    
    /**
     * 商品状态-限购
     */
    private Boolean limited = false;
    
    /**
     * 商品状态-现货
     */
    private Boolean spot = false;
    
    /**
     * APP 商品简介
     */
    private String appIntro;
    
    /**
     * 入箱数
     */
    private Integer boxNumber;
    
    /**
     * 是否再贩
     */
    private Boolean isReSelling = false;
    
    /**
     * 再贩次数
     */
    private Integer reSellingNum;
    
    /**
     * 设计师
     */
    private String designer;
    
    /**
     * 日语名称
     */
    private String nameJp;
    
    /**
     * 购买范围-零售
     */
    private Boolean retail = false;
    
    /**
     * 购买范围-经销商
     */
    private Boolean dealer = false;
    
    /**
     * 排序
     */
    private Integer sort;
    
    /**
     * SEO标题
     */
    private String seoTitle;
    
    /**
     * SEO关键字
     */
    private String seoKeywords;
    
    /**
     * SEO描述
     */
    private String seoDescribe;

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Boolean getReserved() {
		return reserved;
	}

	public void setReserved(Boolean reserved) {
		this.reserved = reserved;
	}

	public Boolean getLimited() {
		return limited;
	}

	public void setLimited(Boolean limited) {
		this.limited = limited;
	}

	public Boolean getSpot() {
		return spot;
	}

	public void setSpot(Boolean spot) {
		this.spot = spot;
	}

	public String getAppIntro() {
		return appIntro;
	}

	public void setAppIntro(String appIntro) {
		this.appIntro = appIntro;
	}

	public Integer getBoxNumber() {
		return boxNumber;
	}

	public void setBoxNumber(Integer boxNumber) {
		this.boxNumber = boxNumber;
	}

	public Boolean getIsReSelling() {
		return isReSelling;
	}

	public void setIsReSelling(Boolean isReSelling) {
		this.isReSelling = isReSelling;
	}

	public Integer getReSellingNum() {
		return reSellingNum;
	}

	public void setReSellingNum(Integer reSellingNum) {
		this.reSellingNum = reSellingNum;
	}

	public String getDesigner() {
		return designer;
	}

	public void setDesigner(String designer) {
		this.designer = designer;
	}

	public String getNameJp() {
		return nameJp;
	}

	public void setNameJp(String nameJp) {
		this.nameJp = nameJp;
	}

	public Boolean getRetail() {
		return retail;
	}

	public void setRetail(Boolean retail) {
		this.retail = retail;
	}

	public Boolean getDealer() {
		return dealer;
	}

	public void setDealer(Boolean dealer) {
		this.dealer = dealer;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getSeoTitle() {
		return seoTitle;
	}

	public void setSeoTitle(String seoTitle) {
		this.seoTitle = seoTitle;
	}

	public String getSeoKeywords() {
		return seoKeywords;
	}

	public void setSeoKeywords(String seoKeywords) {
		this.seoKeywords = seoKeywords;
	}

	public String getSeoDescribe() {
		return seoDescribe;
	}

	public void setSeoDescribe(String seoDescribe) {
		this.seoDescribe = seoDescribe;
	}

	public Works getWorks() {
		return works;
	}

	public void setWorks(Works works) {
		this.works = works;
	}

	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Long getLockedStock() {
        return lockedStock;
    }

    public void setLockedStock(Long lockedStock) {
        this.lockedStock = lockedStock;
    }

    public Long getDamagedStock() {
        return damagedStock;
    }

    public void setDamagedStock(Long damagedStock) {
        this.damagedStock = damagedStock;
    }

    public Long getOtherStock() {
        return otherStock;
    }

    public void setOtherStock(Long otherStock) {
        this.otherStock = otherStock;
    }

    public Float getLength() {
        return length;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getHeight() {
        return height;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ProductImg> getProductImgs() {
        return productImgs;
    }

    public void setProductImgs(List<ProductImg> productImgs) {
        this.productImgs = productImgs;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Long getSaleNum() {
        return saleNum;
    }

    public void setSaleNum(Long saleNum) {
        this.saleNum = saleNum;
    }

    public Boolean getOnSell() {
        return onSell;
    }

    public void setOnSell(Boolean onSell) {
        this.onSell = onSell;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public List<AttributeValue> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<AttributeValue> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public List<ParameterValue> getParameterValues() {
        return parameterValues;
    }

    public void setParameterValues(List<ParameterValue> parameterValues) {
        this.parameterValues = parameterValues;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public Date getSalesOffDate() {
        return salesOffDate;
    }

    public void setSalesOffDate(Date salesOffDate) {
        this.salesOffDate = salesOffDate;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }

    public Boolean getEnableSpecs() {
        return enableSpecs;
    }

    public void setEnableSpecs(Boolean enableSpecs) {
        this.enableSpecs = enableSpecs;
    }

    public List<Goods> getGoods() {
        return goods;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }
}
