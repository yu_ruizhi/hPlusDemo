/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;


import com.yuruizhi.hplusdemo.admin.entity.product.ParameterValue;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ParameterValueService</p>
 * <p>说明: 产品参数值服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:06:46   
 * @version: 1.0
 */
public interface ParameterValueService extends BaseService<ParameterValue> {

  
}
