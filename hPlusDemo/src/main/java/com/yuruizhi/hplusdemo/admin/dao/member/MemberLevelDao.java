/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MemberLevelDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月18日 上午11:46:07 
 **/
package com.yuruizhi.hplusdemo.admin.dao.member;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberLevel;

/**
 * 
 * <p>名称: 会员等级Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月18日 上午11:46:07
 * @version: 1.0
 */
public interface MemberLevelDao extends BaseDao<MemberLevel> {

	/**
	 * 
	 * <p>名称：根据会员等级名称获取会员等级信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name 会员等级名称
	 * @return 返回会员等级信息
	 */
	List<MemberLevel> findByName(String name);
	
}
