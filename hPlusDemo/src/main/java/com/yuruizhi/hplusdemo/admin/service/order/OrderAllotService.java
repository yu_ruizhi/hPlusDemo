/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order;

import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.Map;

/**
 * <p>名称: OrderAllotService</p>
 * <p>说明: 订单分配服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:32:49   
 * @version: 1.0
 */
public interface OrderAllotService extends BaseService<OrderAllot> {

    //根据相关条件进入仓库查询商品数量
    Integer goodsInWarehouseNum(Map<String,Object> map);

    void update(OrderAllot orderAllot);

    void update(Map<String,Object> map);

    String checkedOrderIsPay(Map<String,Object> map);

    //查询商品中的预购库存
    Integer advanceStock(Map<String,Object> map);

    //根据配单id集合批量修改订单的预售订单状态
    public void updatePresaleStatus(Long [] orderAllotIds,Integer presaleStatus,String type);//预售订单状态：1-定金订单；2-已支付定金；3-已配单；4-尾款订单；5-已支付尾款

    Long getOrderId(Long id);

    void updateOrderActualAmount(Map<String,Object> map);
}
