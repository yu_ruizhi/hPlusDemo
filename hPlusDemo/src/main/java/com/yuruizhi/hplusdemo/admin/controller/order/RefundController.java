/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.order;

import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.order.Refund;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService;
import com.yuruizhi.hplusdemo.admin.service.order.RefundService;

import java.util.*;

/**
 * Created on 2016-03-07 14:38:27
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX+"/refund")
public class RefundController extends AdminBaseController<Refund> {

	@Autowired
	private RefundService refundService;
    @Autowired
    private OrdersReceiptService ordersReceiptService;
	@Autowired
	private OrdersReceiptService orderService;
	
	@RequestMapping("findRefundById")
	public String findRefundById(@RequestParam("id")Long id,Map<String,Object> model){
		Refund refund = refundService.findRefundById(id);
        OrdersReceipt order = ordersReceiptService.getOrdersReceiptById(refund.getOrder().getId());
		model.put("refund", refund);
        model.put("order", order);
		return getRelativeViewPath("detail");
	}
	
	@RequestMapping("updateStatus")
	public String updateStatus(Long orderId,Long id){
		
		refundService.updateStatus(id,2);
		return redirectViewPath("findRefundById?id=" + id);
	}
	
	@ResponseBody
	@RequestMapping("rejectRefund")
	public Map<String,Object> rejectRefund(Refund refund){
		if(null!=refund.getId()&&null!=refund.getRejectReason()){
			refund.setStatus(3);
			refundService.update(refund);
			orderService.updateOrdersStatus(refund.getOrderId(), 14);
		}
		Map<String,Object> model = new HashMap<>();
		model.put("message","200");
		return model;
	}
	
	@ResponseBody
	@RequestMapping("batchReject")
	public Map<String,Object> batchReject(String ids,String rejectReason){
		
		refundService.batchReject(ids,rejectReason);
		
		Map<String,Object> model = new HashMap<>();
		model.put("message","200");
		return model;
	}
	
	@ResponseBody
	@RequestMapping("batchAgree")
	public Map<String,Object> batchAgree(@RequestParam("ids") Long[] ids){
		System.out.println(ids);
		refundService.batchAgree(ids);
		
		Map<String,Object> model = new HashMap<>();
		model.put("message","200");
		return model;
	}
	
	@RequestMapping("refundClose")
	public String refundClose(Long orderId,Long id){
		
		refundService.updateStatus(id,4);
		orderService.updateOrdersStatus(orderId, 15);
		return redirectViewPath("findRefundById?id=" + id);
	}
}
