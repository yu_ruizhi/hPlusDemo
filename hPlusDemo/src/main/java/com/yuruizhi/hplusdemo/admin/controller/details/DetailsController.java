package com.yuruizhi.hplusdemo.admin.controller.details;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.dto.ExcelDTO;
import com.yuruizhi.hplusdemo.admin.entity.details.Details;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;
import com.yuruizhi.hplusdemo.admin.service.details.DetailsService;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageService;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.service.order.OrderAllotService;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptDetailService;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.ponly.common.json.Jacksons;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created on 2016-03-03 11:37:24
 *
 * @author 赵创
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/details")
public class DetailsController extends BaseController {

    @Autowired
    private DetailsService detailsService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public void index() {}

    @RequestMapping("list")
    @ResponseBody
    public Page<Details> list(Filters filters, Pageable pageable) {
        Page<Details> mlist = detailsService.findPage(filters, pageable);

        return mlist;
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public void add() { }

    @RequestMapping("edit/{id}")
    public ModelAndView edit(@PathVariable(value = "id") Long id) {
        ModelAndView mav = new ModelAndView();
        Details details = detailsService.find(id);
        mav.addObject("details", details);
        mav.setViewName(getViewPath("edit"));
        return mav;
    }
    @RequestMapping(value = "save")
    public String save(Details details) {
        details.setCreatedDate(new Date());
        details.setDeleted(false);
        details.setLastModifiedDate(new Date());
        detailsService.save(details);
        return redirectViewPath("index");
    }

    @RequestMapping(value = "update")
    public String update(Details details) {
        details.setLastModifiedDate(new Date());
        detailsService.update(details);
        return redirectViewPath("index");
    }

    @RequestMapping("delete")
    @ResponseBody
    public Map<String, Object> delete(@RequestParam("id") Long[] ids) {
        if(null!=ids&&ids.length>0)
          for(Long id:ids) {
              detailsService.delete(id);
          }
        return ImmutableMap.<String, Object> of("success", true);
    }

}
