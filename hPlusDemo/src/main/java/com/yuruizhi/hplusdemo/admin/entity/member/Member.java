/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.member;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;

/**
 * <p>名称: Member</p>
 * <p>说明: 会员实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:08:50   
 * @version: 1.0
 */
public class Member extends BaseEntity {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /**
     * 会员编号
     */
    private String code;
    
    private String firstName;
    /**
     * 真实姓名
     */
    @JsonUnwrapped(prefix = "lastName")
    private String lastName;
    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户类型   用户类型(0:普通 1：经销商 2:超级VIP 3:零售)
     */
    private String userType;
    /**
     * pwd:密码
     *
     * @since JDK 1.7
     */
    private String pwd;
    /**
     * age:年龄
     *
     * @since JDK 1.7
     */
    private int age;
    /**
     * sex:性别
     *
     * @since JDK 1.7
     */
    private String sex;

    /**
     * 头像
     */
    private String headImage;
    /**
     * email:邮箱
     *
     * @since JDK 1.7
     */
    private String email;

    /**
     * phone:联系电话
     *
     * @since JDK 1.7
     */
    @JsonUnwrapped(prefix = "phone")
    private String phone;
    
    /**
     * register:注册时间
     *
     * @since JDK 1.7
     */
    private Date register;
    
    /**
     * 固定电话
     */
    private String telephone;

    /**
     * country:国家
     *
     * @since JDK 1.7
     */
    private String countryId;
    
    /**
     * city:城市
     *
     * @since JDK 1.7
     */
    private String cityId;
    
    /**
     * state:州（省）
     *
     * @since JDK 1.7
     */
    private String provinceId;

    /**
     * 县
     */
    private String areaId;
    
    /**
     * address:地址
     *
     * @since JDK 1.7
     */
    private String address;

    /**
     * zip:邮编
     *
     * @since JDK 1.7
     */
    private int zip;
    
    /**
     * 重置密码密钥
     */
    private String resetPwdToken;

    /**
     * 密钥失效时间
     */
    private Date expirationDate;
    
    /**
     * createdBy:建单人
     *
     * @since JDK 1.7
     */
    private String createdBy;
    
    /**
     * createdDate:建单时间
     *
     * @since JDK 1.7
     */
    private Date createdDate;
    
    /**
     * lastModifiedBy:修改人
     *
     * @since JDK 1.7
     */
    private String lastModifiedBy;
    
    /**
     * lastModifiedDate:修改时间
     *
     * @since JDK 1.7
     */
    private Date lastModifiedDate;
    
    /**
     * isDeleted:是否删除
     * @since JDK 1.7
     */
    private Boolean Deleted;

    /**
     * 是否激活
     */
    private Boolean isActive;

    /**
     * 最后登录IP
     */
    private String loginIp;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * status:状态
     *
     * @since JDK 1.7
     */
    private int status;

    /**
     * remark:备注
     *
     * @since JDK 1.7
     */
    private String remark;

    /**
     * 积分
     */
    private Long integral;
    
    /**
     * 注册平台
     */
    private Integer registerTerraceType;
    
    /**
     * 会员等级Id
     */
    private Long memberLevelId;
    
    /**
     * 会员等级
     */
    private MemberLevel memberLevel;
    
    /**
     * 保证金标识
     */
    private Boolean isMargin;

    /**
     * 是否审核
     */
    private Boolean isAudit;
    
    /**
     * 生日
     */
    private String birthday;
    
    /**
     * 星座
     */
    private String constellation;
    
    /**
     * 血型
     */
    private String bloodType;
    
    /**
     * 签名
     */
    private String signature;
    private String isMargins;

    /**
     * 经销商绑定业务员id
     */
    private Long salesmanId;
    
    private User memberUser;
    
    public String getIsMargins() {
        return isMargins;
    }

    public void setIsMargins(String isMargins) {
        this.isMargins = isMargins;
    }

    public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getRegisterTerraceType() {
		return registerTerraceType;
	}

	public void setRegisterTerraceType(Integer registerTerraceType) {
		this.registerTerraceType = registerTerraceType;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getConstellation() {
		return constellation;
	}

	public void setConstellation(String constellation) {
		this.constellation = constellation;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public Long getMemberLevelId() {
		return memberLevelId;
	}

	public void setMemberLevelId(Long memberLevelId) {
		this.memberLevelId = memberLevelId;
	}

	public MemberLevel getMemberLevel() {
		return memberLevel;
	}

	public void setMemberLevel(MemberLevel memberLevel) {
		this.memberLevel = memberLevel;
	}

	public Boolean getIsMargin() {
		return isMargin;
	}

	public void setIsMargin(Boolean isMargin) {
		this.isMargin = isMargin;
	}

	public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public Boolean getDeleted() {
        return Deleted;
    }

    public void setDeleted(Boolean deleted) {
        Deleted = deleted;
    }

    public Boolean getIsDeleted() {
        return Deleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.Deleted = isDeleted;
    }
    
	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegister() {
        return register;
    }

    public void setRegister(Date register) {
        this.register = register;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getResetPwdToken() {
        return resetPwdToken;
    }

    public void setResetPwdToken(String digitalSignature) {
        this.resetPwdToken = digitalSignature;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }


    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getIntegral() {
        return integral;
    }

    public void setIntegral(Long integral) {
        this.integral = integral;
    }

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public Boolean getIsAudit() {
		return isAudit;
	}

	public void setIsAudit(Boolean isAudit) {
		this.isAudit = isAudit;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public Long getSalesmanId() {
		return salesmanId;
	}

	public void setSalesmanId(Long salesmanId) {
		this.salesmanId = salesmanId;
	}

	public User getMemberUser() {
		return memberUser;
	}

	public void setMemberUser(User memberUser) {
		this.memberUser = memberUser;
	}

}
