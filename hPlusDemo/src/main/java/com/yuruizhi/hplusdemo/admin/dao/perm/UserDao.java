package com.yuruizhi.hplusdemo.admin.dao.perm;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;

import java.util.List;
import java.util.Map;

public interface UserDao extends BaseDao<User> {

    void saveUserToRole(Map<String, Object> userToRole);

    void deleteUserToRoleByUserId(Long userId);

    List<User> getBaseUser(Map<String, Object> paramMap);

    void insert(User user);

    /**
     * <p>名称：根据角色名称获取用户信息</p>
     * <p>描述：</p>
     *
     * @param name
     * @return
     * @author：wuqi
     */
    List<User> findByRoleName(String name);
}
