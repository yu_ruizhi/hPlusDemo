package com.yuruizhi.hplusdemo.admin.dao.logistics;


import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryArea;

/**
 * Created by ASUS on 2014/12/8.
 */
public interface DeliveryAreaDao extends BaseDao<DeliveryArea> {
   List<DeliveryArea> selectAll();
   
	List<DeliveryArea> findByName(String name);
}
