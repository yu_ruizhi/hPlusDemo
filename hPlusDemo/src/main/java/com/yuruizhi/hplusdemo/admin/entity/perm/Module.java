/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.perm;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Module</p>
 * <p>说明: 后台模块实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:15:08   
 * @version: 1.0
 */
public class Module extends BaseEntity {



    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -2510831918123490048L;

	private String name;

    private Integer sortId;

    private List<AdminPage> pages;

    private String remark;

    private Boolean deleted;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<AdminPage> getPages() {
        return pages;
    }

    public void setPages(List<AdminPage> pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "Module{" +
                "sortId=" + sortId +
                ", pages=" + pages +
                ", remark='" + remark + '\'' +
                ", deleted=" + deleted +
                ", name='" + name + '\'' +
                '}';
    }
}
