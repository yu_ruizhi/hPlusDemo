/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsMemberServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.product.impl
 * Date: 2016年6月16日下午3:32:48
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yuruizhi.hplusdemo.admin.dao.product.GoodsMemberDao;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsMember;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsMemberService;
import com.google.common.collect.Maps;

/**
 * <p>名称: 预售商品截单权限Service实现</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年6月16日下午3:32:56   
 * @version: 1.0
 */
@Transactional
@Service
public class GoodsMemberServiceImpl extends BaseServiceImpl<GoodsMember> implements GoodsMemberService {
	
	@Autowired
	private GoodsMemberDao goodsMemberDao;
	
	@Override
	public List<GoodsMember> findByGoodsId(Long goodsId) {
		Map<String, Object> paramMap = Maps.newHashMap();
		paramMap.put("goodsId", goodsId);
		return goodsMemberDao.findByGoodsId(paramMap);
	}

	@Override
	public List<GoodsMember> findByGoodsIdMemberId(Long goodsId, Long memberId) {
		Map<String, Object> paramMap = Maps.newHashMap();
		paramMap.put("goodsId", goodsId);
		paramMap.put("memberId", memberId);
		return goodsMemberDao.findByGoodsIdMemberId(paramMap);
	}

}

