/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: KeywordServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.platform.impl
 * Date: 2016年4月23日 下午1:38:11
 **/
package com.yuruizhi.hplusdemo.admin.service.platform.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.platform.KeywordDao;
import com.yuruizhi.hplusdemo.admin.entity.platform.Keyword;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.platform.KeywordService;

/**
 * 
 * <p>名称: 搜索管理 关键字Service实现类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月23日 下午1:38:11  
 * @version: 1.0
 */
@Service
public class KeywordServiceImpl extends BaseServiceImpl<Keyword>implements KeywordService {

	private KeywordDao keywordDao;
	
	@Autowired
	private SysLogService sysLogService;
	
	@Autowired
	public void setKeywordDao(KeywordDao keywordDao) {
		this.keywordDao = keywordDao;
		super.setCrudDao(keywordDao);
	}
	
	/*(non-Javadoc) 
	 * <p>名称: save</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl#save(java.lang.Object)
	 */ 
	@Override
	public void save(Keyword e) {
		super.save(e);
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "搜索管理-新增-ID：" + e.getId(), "平台管理");
	}

	/*(non-Javadoc) 
	 * <p>名称: update</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl#update(java.lang.Object)
	 */ 
	@Override
	public void update(Keyword e) {
		super.update(e);
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "搜索管理-修改-ID:" + e.getId(), "平台管理");
	}

	/*(non-Javadoc) 
	 * <p>名称: delete</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see org.ponly.webbase.service.support.CrudServiceImpl#delete(java.lang.Object) 
	 */ 
	@Override
	public void delete(Keyword e) {
		super.delete(e);
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "搜索管理-删除-ID:" + e.getId(), "平台管理");
	}



	@Override
	public Map<String, Boolean> checkIsKeywordExists(String name) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name", name);
        List<Keyword> keywordList = getBaseWorks(paramMap);
        if (keywordList.size() > 0) {
            result.put("isExists", true);
        } else {
            result.put("isExists", false);
        }
        return result;
    }

	@Override
	public Boolean checkIsAnotherKeywordExists(Long id, String name) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("name", name);
		List<Keyword> keywordList = getBaseWorks(paramMap);
		
		//新增时已做过唯一性验证，通过名字正常情况下只能找到一个Works对象
		Keyword keyword = (null != keywordList && 0 != keywordList.size()) ? keywordList.get(0) : null;
		
		//如果对象不存在，或者相同则合法
		return keyword == null || keyword.getId().equals(id);
	}
	
	private List<Keyword> getBaseWorks(Map<String, Object> paramMap) {
		return keywordDao.getBaseWorks(paramMap);
	}
}
