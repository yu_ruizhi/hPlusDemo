/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.perm.ModuleDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.ModuleService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>名称: ModuleServiceImpl</p>
 * <p>说明: 后台模块服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:39:26   
 * @version: 1.0
 */
@Service("moduleService")
public class ModuleServiceImpl extends BaseServiceImpl<Module> implements ModuleService {
    @Autowired
    private ModuleDao moduleDao;

    @Autowired
    public void setModuleDao(ModuleDao moduleDao){
        this.moduleDao = moduleDao;
        setBaseDao(moduleDao);
    }

    @Override
    public void saveModule(Module module) {
        module.setDeleted(Constant.NOT_DELETED);
        save(module);
    }

    @Override
    public boolean deleteModule(Long id) {
        if(null == id){
            return false;
        }
        Module module = new Module();
        module.setId(id);
        delete(module);
        return true;
    }

    @Override
    
    public boolean deleteModuleByIds(Long[] moduleIds) {
        if(StringUtils.isEmpty(moduleIds)){
            return false;
        }

        for(Long id: moduleIds){
            deleteModule(id);
        }

        return true;
    }

    @Override
    public List<Map<String, Object>> findAllPage(){
        List<Module> modules = findAll();
        return buildTree(modules);
    }

    @Override
    public List<Module> getBaseModule(Map<String, Object> paramMap) {
        return moduleDao.getBaseModule(paramMap);
    }

    @Override
    public Map<String, Boolean> checkIsModuleExists(String name,Long id){
        Map<String, Boolean> result = new HashMap<String, Boolean>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name", name);
        paramMap.put("id",id);
        List<Module> modules = checkIsModuleExists(paramMap);
        if(modules.size()>0){
            result.put("isExists", true);
        }else {
            result.put("isExists", false);
        }
        return result;
    }

    private List<Module> checkIsModuleExists(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
    	return moduleDao.checkIsModuleExists(paramMap);
	}

	public List<Map<String, Object>> buildTree(List<Module> modules){
        List<Map<String, Object>> moduleTree = new ArrayList<Map<String, Object>>();

        for(int i=0; i<modules.size(); i++){
            Map<String, Object> moduleNode = new HashMap<String, Object>();
            Module module = modules.get(i);
            moduleNode.put("id", "m"+module.getId());
            moduleNode.put("text", module.getName());
            moduleNode.put("type", "module");
            moduleNode.put("state", "open");
            List<AdminPage> pages = module.getPages();
            if(pages.size()>0){
                List<Map<String, String>> pageNodes = new ArrayList<Map<String, String>>();
                for(int j=0; j<pages.size(); j++){
                    AdminPage page = pages.get(j);
                    Map<String, String> pageNode = new HashMap<String, String>();
                    pageNode.put("id", page.getId() + "");
                    pageNode.put("text", page.getName());
                    pageNode.put("type", "page");
                    pageNode.put("state", "open");
                    pageNodes.add(pageNode);
                }
                moduleNode.put("children", pageNodes);
            }
            moduleTree.add(moduleNode);
        }
        return moduleTree;
    }
}
