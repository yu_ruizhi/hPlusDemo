/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: SeoDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.platform
 * Date: 2016年4月23日上午11:14:00
 **/
package com.yuruizhi.hplusdemo.admin.dao.platform;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.platform.Seo;

/**
 * 
 * <p>名称: SEO Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月23日上午11:14:23
 * @version: 1.0
 */
@MyBatisMapper
public interface SeoDao extends BaseDao<Seo> {

}
