package com.yuruizhi.hplusdemo.admin.entity.provider;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created on 2014-12-03 11:26:22
 * 供应商评分详情
 *
 * @author crud generated
 */
public class SupplierDetail extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 供应商id
     */
    private String supplierId;

    /**
     * 分数值
     */
    private BigDecimal ratingValue;

    /**
     * 评分类型
     */
    private String type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 删除标志
     */
    private Boolean deleted = Boolean.FALSE;

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public BigDecimal getRatingValue() {
        return ratingValue;
    }

    public void setRatingValue(BigDecimal ratingValue) {
        this.ratingValue = ratingValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }


}
