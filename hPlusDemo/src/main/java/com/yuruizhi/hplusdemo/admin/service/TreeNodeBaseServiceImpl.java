/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service;

import java.util.Map;

import org.ponly.webbase.service.support.PathTreeNodeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.yuruizhi.hplusdemo.admin.dao.TreeNodeBaseDao;
import com.yuruizhi.hplusdemo.admin.entity.TreeNodeBaseEntity;

/**
 * <p>名称: TreeNodeBaseServiceImpl</p>
 * <p>说明: 树形结构基础服务实现抽象类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:12:00   
 * @version: 1.0
 */
public abstract class TreeNodeBaseServiceImpl<N extends TreeNodeBaseEntity<N>> extends PathTreeNodeServiceImpl<N, Long> implements BaseService<N> {
    protected TreeNodeBaseDao<N> treeNodeBaseDao;

    @Autowired
    public void setTreeNodeBaseDao(TreeNodeBaseDao<N> baseDao) {
        this.treeNodeBaseDao = baseDao;
        super.setCrudDao(baseDao);
    }

    protected TreeNodeBaseServiceImpl(Long rootId) {
        super(rootId);
    }

    protected TreeNodeBaseServiceImpl(Long rootId, String pathSeparator) {
        super(rootId, pathSeparator);
    }

    @Override
    protected void deleteDescendantFor(N node) {
        // TODO 删除后代元素
        // super.deleteDescendantFor(node);
    }

    @Override
	protected void doInternalUpdateDescendantFor(N snapshot, N current) {
    	Map<String, Object> params = buildDescendantUpdateParams(snapshot, current);
        treeNodeBaseDao.updateDescendantPathAndDepthFor(params);
	}

    protected Map<String, Object> buildDescendantUpdateParams(N snapshot, N current) {
    	String oldPath = snapshot.getPath();
    	String newPath = current.getPath();
    	Integer oldLevel = snapshot.getDepth();
    	Integer newLevel = current.getDepth();

    	oldLevel = null != oldLevel ? oldLevel : 0;
    	newLevel = null != newLevel ? newLevel : 0;

        ParamsMap paramsMap = createParamsMap();
        paramsMap.put(TreeNodeBaseDao.OLD_PATH_PROP, oldPath + pathSeparator);
        paramsMap.put(TreeNodeBaseDao.NEW_PATH_PROP, newPath + pathSeparator);
        paramsMap.put(TreeNodeBaseDao.LEVEL_ICREMENT_PROP, newLevel - oldLevel);
        return paramsMap;
    }
}
