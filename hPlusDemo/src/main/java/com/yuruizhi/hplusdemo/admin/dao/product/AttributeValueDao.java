
package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;

public interface AttributeValueDao extends BaseDao<AttributeValue> {
    void deleteByAttributeId(Long aid);
    void deleteProductAttributeValue(Long pid);
    void saveProductAttributeValue(Map<String, Object> params);

    void deleteProductCustomAttributeValueByProductId(Long id);
    
    /**
     * 
     * <p>名称：根据value值及属性id获取属性</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    AttributeValue findByValueAndAttrId(Map<String,Object> map);
}