/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: RecommendWorksServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月21日 下午1:33:40 
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.marketing.RecommendWorksDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.RecommendWorks;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.RecommendWorksService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;

/**
 * 
 * <p>名称: 推荐作品 Service实现类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月21日 下午1:33:40 
 * @version: 1.0
 */
@Service
public class RecommendWorksServiceImpl extends BaseServiceImpl<RecommendWorks>implements RecommendWorksService {

	@SuppressWarnings("unused")
	private RecommendWorksDao recommendWorksDao;
	
	@Autowired
	private SysLogService sysLogService;
	
	@Autowired
	public void setRecommendWorksDao(RecommendWorksDao recommendWorksDao) {
		this.recommendWorksDao = recommendWorksDao;
		super.setCrudDao(recommendWorksDao);
	}
	
	/*(non-Javadoc) 
	 * <p>名称: delete</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param arg0 
	 * @see org.ponly.webbase.service.support.CrudServiceImpl#delete(java.io.Serializable[]) 
	 */ 
	@Override
	public void delete(Long[] arg0) {
		super.delete(arg0);
		StringBuffer idStr = new StringBuffer();
		for(Long id : arg0){
			idStr.append(id + ",");
		}
		idStr.deleteCharAt(idStr.length()-1);
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "推荐作品管理-新增-ID:" + idStr, "运营管理");
	}


	/*(non-Javadoc) 
	 * <p>名称: save</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl#save(java.lang.Object)
	 */ 
	@Override
	public void save(RecommendWorks e) {
		super.save(e);
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "推荐作品管理-新增-ID:"+e.getId(), "运营管理");
	}

	/*(non-Javadoc) 
	 * <p>名称: update</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl#update(java.lang.Object)
	 */ 
	@Override
	public void update(RecommendWorks e) {
		super.update(e);
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "推荐作品管理-修改-ID:"+e.getId(), "运营管理");
	}

	@Override
	public Page<RecommendWorks> findPage(Filters recommendWorksFilters, 
											Filters worksFilters, Pageable pageable) {
		
		Map<String, Object> paramsMap = createParamsMap();
        if (null != recommendWorksFilters) {
            paramsMap.put(RecommendWorksDao.BASE_FILTERS_PROP, recommendWorksFilters);
        }
        if (null != worksFilters) {
            paramsMap.put(RecommendWorksDao.WORKS_FILTERS_PROP, worksFilters);
        }
		if (null != pageable) {
			paramsMap.put(RecommendWorksDao.OFFSET_PROP, pageable.getOffset());

			paramsMap.put(RecommendWorksDao.MAX_RESULTS_PROP, pageable.getPageSize());

			Sort sort = pageable.getSort();
			if (null != sort) {
				paramsMap.put(RecommendWorksDao.SORT_PROP, sort);
			}
		}
        int total = count(paramsMap);
        List<RecommendWorks> data = total > 0 ? findMany(paramsMap) : Collections.<RecommendWorks>emptyList();
        return new SimplePage<RecommendWorks>(pageable, data, total);
	}
}
