/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsMember.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.product
 * Date: 2016年6月16日下午2:29:16
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;

/**
 * <p>名称: 预售商品截单权限</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年6月16日下午2:29:20   
 * @version: 1.0
 */
public class GoodsMember extends BaseEntity {

	/**@Fields serialVersionUID :  */ 
	private static final long serialVersionUID = 1L;
	
	/**@Fields goodsId : 单品ID */ 
	private Long goodsId;
	
	/**@Fields memberId : 会员ID */ 
	private Long memberId;
	
	/**@Fields Boolean : 是否删除 */ 
	private Boolean deleted = Boolean.FALSE;
	
	/**@Fields member : 会员 */ 
	private Member member;
	
	/**@Fields memberList : 会员list（用于接收页面传递的数据） */ 
	private List<Member> memberList;
	
	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public List<Member> getMemberList() {
		return memberList;
	}

	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
}

