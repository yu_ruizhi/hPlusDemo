/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order;

import com.yuruizhi.hplusdemo.admin.entity.order.ProductReview;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ProductReviewService</p>
 * <p>说明: 产品评论服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:36:15   
 * @version: 1.0
 */
public interface ProductReviewService extends BaseService<ProductReview> {

	/** 
	 * @Title: findDetailById 
	 * @Description: TODO
	 * @return
	 * @return: ProductReview
	 */
	ProductReview findDetailById(Long id);
}
