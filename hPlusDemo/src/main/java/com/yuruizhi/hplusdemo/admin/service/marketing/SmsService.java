/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.entity.marketing.Sms;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: SmsService</p>
 * <p>说明: 短信服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:22:21   
 * @version: 1.0
 */
public interface SmsService extends BaseService<Sms> {

	void smsSave(Sms sms);

	void smsSend(Long[] ids);
}
