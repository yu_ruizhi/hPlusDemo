/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;

/**
 * <p>名称: CombineDetail</p>
 * <p>说明: 组合商品详情</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:53:13   
 * @version: 1.0
 */
public class CombineDetail extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5263457532086075372L;

	/**
     * 组合ID
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=COMBINE_ID
     */
    @NotNull
    private Combine combine;

    /**
     * 商品ID
     * 
     * @ViewField editor=spinner 
     * @Column GOODS_ID
     */
    @NotNull
    private Long goodsId;

    /**
     * 产品ID
     * 
     * @ViewField editor=spinner 
     * @Column PRODUCT_ID
     */
    @NotNull
    private Long productId;

    /**
     * 排序
     * 
     * @ViewField editor=spinner 
     * @Column POSITION
     */
    private Integer position;

    /**
     * 是否主商品
     * 
     * @ViewField editor=input 
     * @Column IS_PRIMARY
     */
    @NotNull
    private Boolean isPrimary = Boolean.FALSE; ;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    private Goods goods;
    
    public Combine getCombine() {
        return this.combine;
    }

    public void setCombine(Combine combine) {
        this.combine = combine;
    }

    public Long getGoodsId() {
        return this.goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return this.productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getIsPrimary() {
        return this.isPrimary;
    }

    public void setIsPrimary(Boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

}
