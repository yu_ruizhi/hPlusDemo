/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.ParameterDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Parameter;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.ParameterService;

/**
 * <p>名称: ParameterServiceImpl</p>
 * <p>说明: 产品参数服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:55:10   
 * @version: 1.0
 */
@Service
public class ParameterServiceImpl extends BaseServiceImpl<Parameter> implements ParameterService {

    @Autowired
	public void setParameterDao(ParameterDao parameterDao) {
    	super.setBaseDao(parameterDao);
	}


}
