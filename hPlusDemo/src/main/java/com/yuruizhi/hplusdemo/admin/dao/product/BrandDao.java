/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.List;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Brand;


@MyBatisMapper
public interface BrandDao extends BaseDao<Brand> {
	
	/**
	 * 
	 * <p>名称：根据品牌名获取数据</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	List<Brand> findByName(String name);
	
}