package com.yuruizhi.hplusdemo.admin.controller.order;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;
import com.yuruizhi.hplusdemo.admin.entity.order.ProblemOrder;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService;
import com.yuruizhi.hplusdemo.admin.service.order.ProblemOrderService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.google.common.collect.ImmutableMap;

import net.sf.json.JSONObject;

/**
 * Created by 路姗姗 on 2016/4/27 0027.
 * 问题配单订单管理控制器
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/problemOrder")
public class ProblemOrderController extends BaseController {

    @Autowired
    private ProblemOrderService problemOrderService;

    @Autowired
    private OrdersReceiptService ordersReceiptService;


    /**
     *问题配单订单的列表页
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(HttpServletRequest request, HttpServletResponse response) {
        return getViewPath("index");
    }

    /**
     * 问题配单订单的列表页分页
     * @param filters
     * @param pageable
     * @return
     */
    @RequestMapping("list")
    @ResponseBody
    public Page<ProblemOrder> list(Filters filters, Pageable pageable) {

        Page<ProblemOrder> mlist = problemOrderService.findPage(filters, pageable);
        return mlist;
    }

    /**
     * 更新问题砍单的处理状态
     * @param id
     * @return
     */
    @RequestMapping("update")
    @ResponseBody
    public Map<String ,Object> update(Long id) {
        problemOrderService.updateStatus(id);
       return ImmutableMap.<String ,Object>of("success", true);
    }

    /**
	 * 
	 * <p>名称：导出csv格式文件</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ids
	 * @param request
	 * @param response
	 */
    @RequestMapping("exportData")
    public void exportData(@RequestParam(value="idArr") Long[] ids,HttpServletRequest request,HttpServletResponse response) {
    	exportCsv(ids, request, response);
    }
    
    /**
	 * 
	 * <p>名称：导出csv格式文件</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ids
	 * @param request
	 * @param response
	 */
    @RequestMapping("exportDataByTime")
    public void exportDataByTime(String beginDate,String endDate,HttpServletRequest request,HttpServletResponse response) {
    	Long [] ids = problemOrderService.findIdByTime(beginDate, endDate);
    	exportCsv(ids, request, response);
    }
    
    private void exportCsv (Long[] ids,HttpServletRequest request,HttpServletResponse response) {
    	String[] headers = {"订单编号","订单类型","订单状态","订单来源","砍单处理状态","退款金额","操作人","砍单备注","用户名","收货人","收货电话","收货地址","订单金额","现货金额",
				"展会总金额","下单时间","商品编号","商品名称","商品规格","数量","单价","展会价","首付","小计","展会小计","预定截止日期","预定到货日期"};
		List<String> problemOrderList = new ArrayList<>();
		ProblemOrder problemOrder = new ProblemOrder();
		OrdersReceipt order = new OrdersReceipt();
		String str = "";
		String newStr = ",,,,,,,,,,,,,,";
		for (Long id : ids) {
		problemOrder = problemOrderService.find(id);
		if (null != problemOrder) {
			order = ordersReceiptService.getOrdersReceiptById(problemOrder.getOrderId());
		}
		if (null != problemOrder && null != order) {
			int count = 0;
			str = order.getOrderCode() + "\t," +
				  getOrderType(order.getOrderType()) + "," +
				  getStatus(order.getStatus()) + "," +
				  getOriginate(order.getOriginate()) + "," +
				  getCutStatus(problemOrder.getCutStatus()) + "," +
				  (null == problemOrder.getPrice() ? "" : problemOrder.getPrice()) + "," +
				  problemOrder.getLastModifiedBy() + "," +
				  problemOrder.getCutRemark() + "," +
				  order.getUserName() + "\t," +
				  getShippingAddress(order.getShippingAddress(), "firstName") + "," +
				  getShippingAddress(order.getShippingAddress(), "phone1") + "\t," +
				  getShippingAddress(order.getShippingAddress(), "addressLine") + "," +
				  (null == order.getTotalPrice() ? "" : order.getTotalPrice()) + "," +
				  (null == order.getTotalPrice() ? "" : order.getTotalPrice()) + "," +
				  (null == order.getExhibitionAmount() ? "" : order.getExhibitionAmount()) + ',' +
				  (null == order.getCreatedDate() ? "" : DateUtils.date2Str(order.getCreatedDate(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS));
				  if (null != order.getOrdersReceiptDetails()) {
					  for (OrdersReceiptDetail ordersReceiptDetail : order.getOrdersReceiptDetails()) {
						  if (1 > count) {
							  str += getOrdersReceiptDetail(ordersReceiptDetail, order);
							  problemOrderList.add(str);
							  count ++;
						  } else {
							  newStr += getOrdersReceiptDetail(ordersReceiptDetail, order);
							  problemOrderList.add(newStr);
						  }
					  }
				  }
		}
		}
		String fileName = "problemOrder" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
		CSVUtil.exportCsv(headers, problemOrderList, response, fileName);
    }
    
    /**
	 * 
	 * <p>名称：获取订单下单品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ordersReceiptDetail
	 * @param order
	 * @return
	 */
	private String getOrdersReceiptDetail(OrdersReceiptDetail ordersReceiptDetail,OrdersReceipt order) {
		String str = "," + ordersReceiptDetail.getGoodsCode() + "," +
					ordersReceiptDetail.getProductName() + "," +
					ordersReceiptDetail.getSpecValue().replaceAll(",", ";") + "," +
					ordersReceiptDetail.getGoodsNum() + "," +
					(null == ordersReceiptDetail.getGoodsPrice() ? "" : ordersReceiptDetail.getGoodsPrice()) + ",,," +
					//订单商品展会价及首付暂无显示
//					(null == order.getExhibitionAmount() ? "" : order.getExhibitionAmount()) + "," +
//					(null == order.getDepositAmount() ? "" : order.getDepositAmount()) + "," +
					(null == ordersReceiptDetail.getGtotalPrice() ? "" : ordersReceiptDetail.getGtotalPrice()) + ",," +
//					(null == order.getExhibitionAmount() || null == ordersReceiptDetail.getGoodsNum() ? "" : order.getExhibitionAmount().multiply(BigDecimal.valueOf(ordersReceiptDetail.getGoodsNum()))) + "," +
					(null == ordersReceiptDetail.getGoods().getReserveDeadline() ? "" : DateUtils.date2Str(ordersReceiptDetail.getGoods().getReserveDeadline(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS));
		return str;
	}
    
    /**
	 * 
	 * <p>名称：根据订单类型获取</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param orderType
	 * @return
	 */
	private String getOrderType(Integer orderType) {
		if (new Integer(1).equals(orderType)) {
			return "普通订单";
		} else if (new Integer(2).equals(orderType)) {
			return "预售订单";
		} else if (new Integer(4).equals(orderType)) {
			return "汇总订单";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：获取订单信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param status
	 * @return
	 */
	private String getStatus(Integer status) {
		if (new Integer(1).equals(status)) {
			return "待支付";
		} else if (new Integer(2).equals(status) || new Integer(3).equals(status) || new Integer(4).equals(status) ||
				new Integer(5).equals(status) || new Integer(6).equals(status)) {
			return "待发货";
		} else if (new Integer(7).equals(status)) {
			return "待收货";
		} else if (new Integer(8).equals(status)) {
			return "已完成";
		} else if (new Integer(10).equals(status)) {
			return "已取消";
		} else if (new Integer(11).equals(status)) {
			return "已评论";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：获取下单方式</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param originate
	 * @return
	 */
	private String getOriginate(Integer originate) {
		if (new Integer(1).equals(originate)) {
			return "PC";
		} else if (new Integer(2).equals(originate)) {
			return "APP";
		} else if (new Integer(3).equals(originate)) {
			return "阅文";
		} else {
			return "";
		}
	}
    
	/**
	 * 
	 * <p>名称：砍单处理状态</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param cutStatus
	 * @return
	 */
	private String getCutStatus(Integer cutStatus) {
		if (new Integer(1).equals(cutStatus)) {
			return "未退款";
		} else if (new Integer(2).equals(cutStatus)) {
			return "已退款";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：解析收货方式</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param shippingAddress
	 * @param keyName
	 * @return
	 */
	private String getShippingAddress(String shippingAddress,String keyName) {
		JSONObject jsonObject=JSONObject.fromObject(shippingAddress);
		return jsonObject.getString(keyName);
	}
	
}

