package com.yuruizhi.hplusdemo.admin.controller.order;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.dto.ExcelDTO;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageService;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.service.order.OrderAllotService;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptDetailService;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.service.perm.impl.AdminUserServiceImpl;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import org.apache.commons.lang.StringUtils;
import org.ponly.common.json.Jacksons;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created on 2016-03-03 11:37:24
 *
 * @author 赵创
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/orderallot")
public class OrderAllotController extends BaseController {

    @Autowired
    private OrderAllotService orderAllotService;
    @Autowired
    private OrdersReceiptService ordersReceiptService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private OrdersReceiptDetailService ordersReceiptDetailService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;


    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index() {
        return getViewPath("index");
    }

    @RequestMapping("list")
    @ResponseBody
    public Page<OrderAllot> list(Filters filters, Pageable pageable
    ,@RequestParam(value = "goodsCode", required = false) String goodsCode,
     @RequestParam(value = "beginDate", required = false) String beginDate,
     @RequestParam(value = "endDate", required = false)  String endDate
    ) {
        if(null!=goodsCode&&goodsCode.trim().length()>0){
            filters.eq("O_D_G_CODE",goodsCode);
        }
        if (StringUtils.isNotEmpty(beginDate)) {
            filters.ge("O_D_G_RESERVE_DEADLINE", beginDate);
        }
        if (StringUtils.isNotEmpty(endDate)) {
            filters.le("O_D_G_RESERVE_DEADLINE", endDate);
        }
        Page<OrderAllot> orderAllots = orderAllotService.findPage(filters, pageable);
        //对订单中的收货地址进行遍历
        if(null!=orderAllots&&orderAllots.getData()!=null){
            for(OrderAllot orderAllot:orderAllots.getData()){
                if(null!=goodsCode&&goodsCode.trim().length()>0&&orderAllot.getAllotStatus()==OrderAllot.ALLOT_NOT_ARRIVING){
                    int num=orderAllot.getOrdersReceipt().getOrdersReceiptDetail().getGoodsNum();
                    orderAllot.setAllotNum(num);
                }
                MemberAddress memberAddress = Jacksons.deserialize(orderAllot.getOrdersReceipt().getShippingAddress(), MemberAddress.class);
                orderAllot.getOrdersReceipt().setMemberAddress(memberAddress);

            }
        }

        return orderAllots;
    }
  //查询仓库的商品预售库存
    @RequestMapping("goodsInWarehouseNum")
    @ResponseBody
    public Integer goodsInWarehouseNum(
           String goodsCode,Map<String,Object> map
    ) {
       if(null==goodsCode||goodsCode.trim().length()<1){
           return 0;
       }
        map.put("goodsCode",goodsCode) ;
      Integer num=orderAllotService.goodsInWarehouseNum(map);
        return null!=num&&num>0?num:0;
    }




    @RequestMapping("distribution")
    @ResponseBody
    public boolean distribution (
            @RequestParam(value = "ids[]", required = true)  Long[] ids,
            @RequestParam(value = "allotNums[]", required = true)  Integer[] allotNums,
            @RequestParam(value = "ydNums[]", required = true)  Integer[] ydNums,
             String wdate
    ) {
        Long adminUserId= UserUtils.getCurrentUserId();
        if((ids==null||ids.length<1)||(allotNums==null||allotNums.length<1)||null==wdate)
            return false;
        for(int i=0;i<ids.length;i++){
            OrderAllot orderAllot=new OrderAllot();
         if(allotNums[i]==ydNums[i]){
             orderAllot.setId(ids[i]);
             orderAllot.setAllotNum(allotNums[i]);
             orderAllot.setAllotStatus(OrderAllot.ALLOT_NOT_REMIND);
             try {
                 orderAllot.setArrivalTime(new SimpleDateFormat("yyyy-MM-dd").parse(wdate));
             }catch (Exception e){
                 e.printStackTrace();
             }
         }else if (allotNums[i] < ydNums[i]){
             orderAllot.setId(ids[i]);
             orderAllot.setAllotNum(allotNums[i]);
             orderAllot.setAllotStatus(OrderAllot.ALLOT_NOT_REMIND);
             try {
                 orderAllot.setArrivalTime(new SimpleDateFormat("yyyy-MM-dd").parse(wdate));
             }catch (Exception e){
                 e.printStackTrace();
             }
             orderAllot.setIsCut(true);
             orderAllot.setCutNum(ydNums[i]-allotNums[i]);
             orderAllot.setCutRemark("砍掉"+(ydNums[i]-allotNums[i])+"个");
             orderAllot.setCutStatus(OrderAllot.CUT_NOT_REFUND);
             //TODO 查询订单，算出砍单商品的总金额，更改实际付款金额
             Long orderId=orderAllotService.getOrderId(ids[i]);

             OrdersReceipt ordersReceipt=ordersReceiptService.findOrdersReceipt(orderId);
                 OrdersReceiptDetail ordersReceiptDetail=ordersReceiptDetailService.getOrdersReceiptDetailByOrderId(orderId);
             // TODO 保证金经销商是没有缴纳定金的，所以砍单后计算价格不应该在加上定金
             Member member=memberService.find(ordersReceipt.getUserId());
             if(member.getUserType().equals("0")||(member.getUserType().equals("1")&&(null==member.getIsAudit()|| !member.getIsAudit()))||(member.getUserType().equals("1")&&(null == member.getIsMargin() || !member.getIsMargin()))) {
                 //零售商或者没有审核的经销商，或者不是保证金经销商
                 ordersReceipt.setActualAmount(ordersReceipt.getActualAmount().subtract(ordersReceiptDetail.getGoodsPrice().multiply(new BigDecimal(((ydNums[i] - allotNums[i]) + "").trim()))).add(ordersReceiptDetail.getGoodsPrice().multiply(new BigDecimal(((ydNums[i] - allotNums[i]) + "").trim())).multiply(new BigDecimal(Constant.DEPOSIT))).setScale(2, BigDecimal.ROUND_HALF_UP));
             }else{
                 ordersReceipt.setActualAmount(ordersReceipt.getActualAmount().subtract(ordersReceiptDetail.getGoodsPrice().multiply(new BigDecimal(((ydNums[i] - allotNums[i]) + "").trim()))).setScale(2, BigDecimal.ROUND_HALF_UP));
             }
                 ordersReceiptService.updateActualAmount(ordersReceipt);
             ordersReceiptDetail.setCutNum(allotNums[i]);
             ordersReceiptDetailService.update(ordersReceiptDetail);
         }
            orderAllot.setLastModifiedBy(userService.find(adminUserId).getLoginName());
            orderAllot.setLastModifiedDate(new Date());
            orderAllot.setAllotTime(new Date());
            orderAllotService.update(orderAllot);
        }
        orderAllotService.updatePresaleStatus(ids, OrdersReceipt.PRESALE_STATUS_SINGLE,"分配");
        return true;
    }

    /**
     *
     * <p>名称：updateAllotStatus</p>
     * <p>描述：配单到货提醒</p>
     * @author：zhaochuang
     * @param ids
     * @param map
     * @return boolean
     * update by yuruizhi on 20160706 订单状态为尾款订单后,站内信通知
     */
    @RequestMapping("updateAllotStatus")
    @ResponseBody
    public boolean updateAllotStatus(
            @RequestParam(value = "ids[]", required = true)  Long[] ids,Map<String,Object> map
    ) {
        if(ids==null||ids.length<1){
            return false;
        }
        map.put("allotStatus",OrderAllot.ALLOT_OTHER);
        map.put("ids",ids);
        orderAllotService.update(map);
        orderAllotService.updatePresaleStatus(ids, OrdersReceipt.PRESALE_STATUS_RETAINAGE,"提醒");

        messageService.OrderAllotNotice(ids); //订单状态为尾款订单后,站内信通知

        return true;
    }

    @RequestMapping("revoke")
    @ResponseBody
    public boolean revoke(
            @RequestParam(value = "ids[]", required = true)  Long[] ids
            ,String cancelCutRemark
            ,Map<String,Object> map
    ) {
        if(ids==null||ids.length<1){
            return false;
        }
        map.put("ids",ids);
        //如果是配单，还原价格
  orderAllotService.updateOrderActualAmount(map);
        map.put("allotStatus",OrderAllot.ALLOT_NOT_ARRIVING);
        map.put("revoke","revoke");
        map.put("cancelCutRemark",cancelCutRemark);
        orderAllotService.update(map);
        orderAllotService.updatePresaleStatus(ids, OrdersReceipt.PRESALE_STATUS_PAYDEPOSIT,"撤销");
        return true;
    }

    @RequestMapping("checkedOrderIsPay")
    @ResponseBody
    public String checkedOrderIsPay(
            @RequestParam(value = "ids[]", required = true)  Long[] ids
            ,Map<String,Object> map
    ) {
        if(ids==null||ids.length<1){
            return "数据异常";
        }
        map.put("ids",ids);
     String mess=   orderAllotService.checkedOrderIsPay(map);
        return mess;
    }



//表格导出
@RequestMapping("excelAllotStock")
public void excelAllotStock(HttpServletResponse response,Filters filters,String bigTime,String endTime){
    filters.lt("createdDate",endTime);
    filters.gt("createdDate",bigTime);
Page<OrderAllot> orderAllots=orderAllotService.findPage(filters,null);

    List<ExcelDTO> excelDTOList=new ArrayList<>();

    if(null!=orderAllots&&orderAllots.getData().size()>0){
        for(OrderAllot orderAllot:orderAllots.getData()){
            ExcelDTO excelDTO=new ExcelDTO();
            excelDTO.setGoodsCode(orderAllot.getOrdersReceipt().getOrdersReceiptDetail().getGoods().getCode());
            excelDTO.setAllotNum(orderAllot.getAllotNum());
            excelDTO.setGoodsTitle(orderAllot.getOrdersReceipt().getOrdersReceiptDetail().getGoods().getTitle());
            excelDTO.setArrivalTime(null!=orderAllot.getArrivalTime()? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(orderAllot.getArrivalTime()):null);
            excelDTO.setPandianTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
            excelDTOList.add(excelDTO) ;
        }
    }

    String [] titles={"商品编号","商品名称","分配数量","到货时间","盘点时间"};
    String [] methosName={"goodsCode","goodsTitle","allotNum","arrivalTime","pandianTime"};
    try {
        ExcelUtil.outExcel("配单库存盘点明细", titles, excelDTOList, methosName, response);
    }catch(Exception e){
        e.printStackTrace();
    }

}


    @RequestMapping("moniter")
    public String moniter(){
    	return getViewPath("allotMoniter");
    }

    @RequestMapping("moniterList")
    @ResponseBody
    public Page<OrderAllot> moniterList(Filters filters, Pageable pageable) {
        Page<OrderAllot> orderAllots = orderAllotService.findPage(filters, pageable);
        return orderAllots;
    }

    @RequestMapping("exportData")
    public void exportData (@RequestParam(value="idArr") Long[] ids,HttpServletResponse response,HttpServletRequest request) {
    	String[] headers = {"商品编号","商品名称","用户名","到货数量","到货时间","配单时间"};
    	List<String> orderAllotList = new ArrayList<>();
    	String str = "";
    	for (Long id :ids) {
    		OrderAllot orderAllot = orderAllotService.find(id);
    		if (null != orderAllot) {
    			str = getGoods(str, orderAllot.getOrdersReceipt()) +
    				  (null == orderAllot.getOrdersReceipt() ? "" : 
    				  (null == orderAllot.getOrdersReceipt().getMember()) ? "" : 
    				  orderAllot.getOrdersReceipt().getMember().getPhone()) + "\t," +
    				  (null == orderAllot.getAllotNum() ? "" :orderAllot.getAllotNum()) + "," +
    				  (null == orderAllot.getArrivalTime() ? "" : DateUtils.date2Str(orderAllot.getArrivalTime(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
    				  (null == orderAllot.getAllotTime() ? "" : DateUtils.date2Str(orderAllot.getAllotTime(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS));
    		}
    		orderAllotList.add(str);
    	}
    	String fileName = "orderAllot" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
    	CSVUtil.exportCsv(headers, orderAllotList, response, fileName);
    }

    public String getGoods(String str,OrdersReceipt ordersReceipt) {
    	if (null != ordersReceipt) {
    		if (null != ordersReceipt.getOrdersReceiptDetail()) {
    			if (null != ordersReceipt.getOrdersReceiptDetail().getGoods()) {
    				str = ordersReceipt.getOrdersReceiptDetail().getGoods().getCode() + "\t," +
    					  ordersReceipt.getOrdersReceiptDetail().getGoods().getTitle() + ",";
    			} else {
    				str = ",,";
    			}
    		} else {
    			str = ",,";
			}
    	} else {
    		str = ",,";
		}
    	return str;
    }
    
}
