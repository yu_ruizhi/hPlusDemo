package com.yuruizhi.hplusdemo.admin.dao.perm;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;

/**
 * Created by ASUS on 2014/12/22.
 */
public interface ModuleDao extends BaseDao<Module> {
    List<Module> getBaseModule(Map<String, Object> paramMap);

	List<Module> checkIsModuleExists(Map<String, Object> paramMap);
}
