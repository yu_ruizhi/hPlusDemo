/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Spec;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: SpecService</p>
 * <p>说明: 产品规格服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:08:13   
 * @version: 1.0
 */
public interface SpecService extends BaseService<Spec> {

	void saveSpecValue(SpecValue spv);

	void updateSpecValue(SpecValue spv);

	List<SpecValue> findSpecValById(Long specId);

    boolean saveSpecAndSpecVal(Spec spec);
    
    /**
     * 
     * <p>名称：根据名称获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    List<Spec> findByName(String name);
    String getIsExistsSpec(Map<String, Object> m);
}
