/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: RecommendWorks.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月21日 上午11:42:44
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;

/**
 * 
 * <p>名称: 作品推荐实体类</p>
 * <p>说明: 对应表T_RECOMMEND_WORKS</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月21日 上午11:42:44 
 * @version: 1.0
 */
public class RecommendWorks extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields worksId :　作品ID
	 * @Column WORKS_ID
	 */
	private Long worksId;
	
	//updated by qinzhongliang 20160617
	/**@Fields categoryId : 分类ID */ 
	private Long categoryId;
	
	/**
	 * @Fields iamge : 图片
	 * @Column IMAGE
	 */
	private String image;
	
	/**
	 * @Fields platform : 平台
	 * @size 20
	 * @Column PLATFORM
	 */
	private String platform;
	
	/**
	 * @Fields sort : 排序
	 * @Column SORT
	 */
	private Integer sort;
	
	/**
	 * @Fields deleted : 删除标示
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	/**
	 * @Fields works : 作品对象
	 */
	private Works works;
	
	/**
	 * @Fields category : 分类对象
	 */
	private Category category;
	
	public Long getWorksId() {
		return worksId;
	}

	public void setWorksId(Long worksId) {
		this.worksId = worksId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Works getWorks() {
		return works;
	}

	public void setWorks(Works works) {
		this.works = works;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
}
