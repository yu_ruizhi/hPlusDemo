/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: GoodsService</p>
 * <p>说明: 商品服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:03:39   
 * @version: 1.0
 */
public interface GoodsService extends BaseService<Goods> {
    /**
     * 获取单品详情（无数据过滤）
     *
     * @param id
     * @return
     */
    Goods getGoodsById(String id);

    /**
     * 检查单品状态
     *
     * @param goodsId
     * @param goodsNum
     * @return
     */
    String checkStatusByIdAndNum(String goodsId, Integer goodsNum);

    public Map<String, Object> getGoodsDetailAndStatus(String goodsId, int goodsNum);
    List<Goods> selectCategoryChildren(String cid);
    List<Goods>  selectLike(Map<String ,Object> map);

    /**
     * 根据单品的ID获取该商品的分类信息
     */
	Goods findGoodsCategoryById(Long goodsId);

    void updateShoppingGoods(Goods goods);

	/**
	 * 
	 * <p>名称：根据商品id获取所以单品</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param productId
	 * @return
	 */
	List<Goods> findByProductId(Long productId);
	
	List<Map<String,Object>> getNewGoods(Map<String,Object> map);

	void updateReserveDeadline(Map<String,Object> map);

	Integer findGoodsCount(Map<String,Object> map);

	void updateproduct(Map<String,Object> map);

	void updategoods(Map<String,Object> map);

	Integer findGoodszCount(Map<String,Object> map);
}
