/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.details;

import com.yuruizhi.hplusdemo.admin.entity.details.Details;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseService;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * <p>名称: DetailsService</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：zhaochaung
 * @date：2016年5月31日上午9:26:02   
 * @version: 1.0
 */
public interface DetailsService extends BaseService<Details> {

}
