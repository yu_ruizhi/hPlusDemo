/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.order;

import org.apache.ibatis.annotations.Param;
import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.order.Refund;

/**
 * Created on 2016-03-07 14:38:27
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface RefundDao extends BaseDao<Refund> {

	Refund findRefundById(@Param("id")Long id);

	void updateStatus(@Param("id")Long id, @Param("status")int status);
}