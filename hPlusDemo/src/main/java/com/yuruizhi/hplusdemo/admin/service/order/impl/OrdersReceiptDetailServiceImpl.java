/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.order.OrdersReceiptDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptDetailService;

/**
 * <p>名称: OrdersReceiptDetailServiceImpl</p>
 * <p>说明: 订单详情服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:28:54   
 * @version: 1.0
 */
@Service
public class OrdersReceiptDetailServiceImpl extends BaseServiceImpl<OrdersReceiptDetail> implements
        OrdersReceiptDetailService {

    private OrdersReceiptDetailDao ordersReceiptDetailDao;

    @Autowired
    public void setOrdersReceiptDetailDao(OrdersReceiptDetailDao ordersReceiptDetailDao) {
        super.setBaseDao(ordersReceiptDetailDao);
        this.ordersReceiptDetailDao=ordersReceiptDetailDao;
    }

    @Override
    public OrdersReceiptDetail getOrdersReceiptDetailByOrderId(Long orderId) {
        return ordersReceiptDetailDao.getOrdersReceiptDetailByOrderId(orderId);
    }
}
