/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;

import java.util.*;

/**
 * <p>名称: ProductReview</p>
 * <p>说明: 商品评价实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:13:02   
 * @version: 1.0
 */
public class ProductReview extends BaseEntity {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 5649671613963423011L;

	/**
     * 评论人
     * 
     * @ViewField editor=spinner 
     * @Column MEMBER_ID
     */
    @NotNull
    private Integer memberId;

    /**
     * 评分
     * 
     * @ViewField editor=spinner 
     * @Column SCORE
     */
    @NotNull
    private Integer score;

    /**
     * 评级
     * 评级,(好评/中评/差评), 根据评分计算
     * 
     * @ViewField editor=spinner 
     * @Column RATE
     */
    @NotNull
    private Integer rate;

    /**
     * 评论内容
     * 
     * @ViewField editor=input 
     * @Column CONTENT
     */
    @Size(max = 500)
    private String content;

    /**
     * 是否匿名评价
     * 
     * @ViewField editor=input 
     * @Column IS_ANONY
     */
    @NotNull
    private Boolean isAnony = Boolean.FALSE; ;

    /**
     * 是否有照片
     * 
     * @ViewField editor=input 
     * @Column HAS_PHOTOS
     */
    @NotNull
    private Boolean hasPhotos = Boolean.FALSE; ;

    /**
     * 单品ID
     * 
     * @ViewField editor=spinner 
     * @Column GOODS_ID
     */
    private Integer goodsId;

    /**
     * 评论产品ID
     * 
     * @ViewField editor=spinner 
     * @Column PRODUCT_ID
     */
    private Integer productId;

    /**
     * 所属订单明细
     * 相关订单
     * 
     * @ViewField editor=spinner 
     * @Column ORDER_DETAIL_ID
     */
    private Integer orderDetailId;

    /**
     * 所属订单
     * 
     * @ViewField editor=spinner 
     * @Column ORDER_ID
     */
    private Integer orderId;

    /**
     * 评论时间
     * 
     * @ViewField editor=datepicker 
     * @Column REVIEW_DATE
     */
    private Date reviewDate;

    /**
     * 回复内容
     * 
     * @ViewField editor=input 
     * @Column REPLY
     */
    @Size(max = 500)
    private String reply;


	

	/**
     * 回复时间
     * 
     * @ViewField editor=datepicker 
     * @Column REPLY_DATE
     */
    private Date replyDate;
    
    private Member member;

    /**
     * 商品实体对象
     */
    private Product product;

    /**
     * 单品
     */
    private Goods goods;
    
    /**
     * 相关订单
     */
    private OrdersReceipt order;
    
    /**
     * 评价晒图
     */
    private List<Photo> photos;
    
    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;
    private Boolean isVisible;

    public Integer getMemberId() {
        return this.memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getScore() {
        return this.score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getRate() {
        return this.rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getIsAnony() {
        return this.isAnony;
    }

    public void setIsAnony(Boolean isAnony) {
        this.isAnony = isAnony;
    }

    public Boolean getHasPhotos() {
        return this.hasPhotos;
    }

    public void setHasPhotos(Boolean hasPhotos) {
        this.hasPhotos = hasPhotos;
    }

    public Integer getGoodsId() {
        return this.goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getProductId() {
        return this.productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getOrderDetailId() {
        return this.orderDetailId;
    }

    public void setOrderDetailId(Integer orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public Integer getOrderId() {
        return this.orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Date getReviewDate() {
        return this.reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public Date getReplyDate() {
        return this.replyDate;
    }

    public void setReplyDate(Date replyDate) {
        this.replyDate = replyDate;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    /**
	 * @return the reply
	 */
	public String getReply() {
		return reply;
	}

	/**
	 * @param reply the reply to set
	 */
	public void setReply(String reply) {
		this.reply = reply;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}


	public List<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
	}


	public OrdersReceipt getOrder() {
		return order;
	}

	public void setOrder(OrdersReceipt order) {
		this.order = order;
	}

	public Boolean getIsVisible() {
		return isVisible;
	}

	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}

}
