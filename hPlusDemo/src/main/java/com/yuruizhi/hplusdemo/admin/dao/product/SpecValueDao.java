/*
 * Copyright (c) 2014, by Besture All right reserved. 
 *
 */
package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;

/**
 * Created on 2014-07-25 09:11:01
 *
 * @author crud generated
 */
public interface SpecValueDao extends BaseDao<SpecValue> {
    void saveGoodsSpecValue(Map<String, Object> paramsMap);
    SpecValue specValueBean(String id);
    List<SpecValue> findSpecValByspId(Long specId);
    
    /**
     * <p>名称：通过规格值编码进行查找</p> 
     * <p>描述：</p>
     * @author：qinzhongliang
     * @param code
     * @return
     */
    List<SpecValue> findByCode(String code);
    
    /**
     * 
     * <p>名称：根据规格id及名称获取数量</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    SpecValue findBySpecIdAndName(Map<String,Object> map);
}