/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.order.Photo;
import com.yuruizhi.hplusdemo.admin.entity.order.ProductReview;
import com.yuruizhi.hplusdemo.admin.service.order.ProductReviewService;

import java.util.*;

/**
 * Created on 2016-03-03 11:37:24
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/photo")
public class PhotoController extends AdminBaseController<Photo> {
    @Autowired
    private ProductReviewService productReviewService;

    @Override
    public void input(Photo photo, Map<String, Object> model) {
        super.input(photo, model);
        model.put("productReviews", productReviewService.findAll());
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(ProductReview.class, "productReview", new CustomStringBeanPropertyEditor(ProductReview.class));
    }
}
