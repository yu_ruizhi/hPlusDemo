/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ModuleService</p>
 * <p>说明: 后台模块管理服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:41:57   
 * @version: 1.0
 */
public interface ModuleService extends BaseService<Module> {
    void saveModule(Module module);

    boolean deleteModule(Long id);

    boolean deleteModuleByIds(Long[] moduleIds);

    List<Map<String, Object>> findAllPage();

    List<Module> getBaseModule(Map<String, Object> paramMap);

    Map<String, Boolean> checkIsModuleExists(String name,Long id);
}
