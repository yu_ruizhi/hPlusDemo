/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.perm.Module;

/**
 * <p>名称: PermissionService</p>
 * <p>说明: 权限管理服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:43:07   
 * @version: 1.0
 */
public interface PermissionService {
    List<Module> findPermissionByUserId(Long userId);

	List<Module> getPermissionFromCache();

    List<Integer> getCurrentAuditPerm();
}
