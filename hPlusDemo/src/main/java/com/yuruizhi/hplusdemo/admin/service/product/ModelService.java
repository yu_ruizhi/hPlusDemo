/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.product.Model;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ModelService</p>
 * <p>说明: 产品型号服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:05:46   
 * @version: 1.0
 */
public interface ModelService extends BaseService<Model> {
    Model findDetail(Long id);
    boolean isReferenced(Long mid);
    Model findBaseModel();
    void saveBaseModel(Model model);
    void deleteModelSpec(Long msId);
    
    /**
     * 
     * <p>名称：根据名称获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    List<Model> findByName(String name);
}
