/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;

import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.entity.product.CategoryRecommend;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: CategoryRecommendService</p>
 * <p>说明: 分类推荐服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:00:44   
 * @version: 1.0
 */
@Service
public interface CategoryRecommendService extends BaseService<CategoryRecommend> {

    void forSave(CategoryRecommend categoryRecommend);
    void updateForSave(CategoryRecommend categoryRecommend,Long[] goodsId,Long[] crId,String[] locations);
    List<CategoryRecommend> cselectAll(Long cid);
    void deletec(Long cid);
    Integer isYou(Long cid);
}
