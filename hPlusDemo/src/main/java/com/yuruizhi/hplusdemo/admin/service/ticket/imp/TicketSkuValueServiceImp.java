/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: AreaService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area
 * Date: 2016年4月18日下午7:40:44
 **/
package com.yuruizhi.hplusdemo.admin.service.ticket.imp;


import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketSkuValue;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;

import com.yuruizhi.hplusdemo.admin.service.ticket.TicketSkuValueService;


@Service
public class TicketSkuValueServiceImp extends BaseServiceImpl<TicketSkuValue> implements TicketSkuValueService{


	
}
