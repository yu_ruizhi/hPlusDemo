package com.yuruizhi.hplusdemo.admin.service.logistics;



import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by ASUS on 2014/12/9.
 */
public interface DeliveryMethodService extends BaseService<DeliveryMethod> {
    List<DeliveryMethod> selectMethodGoCityId(String cityId);
    void addmethod(DeliveryMethod dd);
    void bianji(DeliveryMethod dm);
    
    /**
     * 保存验证重名
     */
	Map<String, Boolean> checkIsNameExists(String name);
	
	/**
	 * 更新验证重名
	 */
	Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName);
}
