/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-inoutstock
 * FileName: SupplierController.java
 * PackageName: com.glanway.inoutstock.controller.admin
 * Date: 2016年5月9日下午3:20:41
 **/
package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;


import com.yuruizhi.hplusdemo.admin.service.provider.SupplierService;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.support.PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.yuruizhi.hplusdemo.admin.controller.ControllerSupport.SUCCESS_MSG_KEY;

/**
 * <p>名称:SupplierController </p>
 * <p>说明: 供应商管理</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 *
 * @author：lushanshan
 * @date：2016年5月9日下午3:20:41
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/supplier")
public class SupplierController extends BaseController {

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private SysLogService sysLogService;

    /**
     * <p>名称：redirectToIndex</p>
     * <p>描述：跳转至供应商列表页</p>
     * @author：lushanshan
     * @return
     */
    @RequestMapping()
    public String redirectToIndex() {
        return redirectViewPath("index");
    }

    /**
     * <p>名称：index</p>
     * <p>描述：供应商列表页</p>
     * @author：lushanshan
     * @param
     * @return
     */
    @RequestMapping("index")
    public void index() {
    }

    /**
     * <p>名称：list</p>
     * <p>描述：供应商管理列表页分页</p>
     * @author：lushanshan
     * @param filters
     * @param pageable
     * @return
     */
    @ResponseBody
    @RequestMapping("list")
    public Page<Supplier> list(Filters filters, Pageable pageable) {

        Filters tmpFilters = Filters.create();
		Iterator<Filters.Filter> it = filters.iterator();
		while(it.hasNext()){
			Filters.Filter f = it.next();
			if(!f.getProperty().equals("createdDate")){
				tmpFilters.add(f);
			}
		}
		Filters.Filter createdDate = filters.getFilterFor("createdDate");
		if(null != createdDate){
			SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd");
			tmpFilters.like("createdDate", time.format(createdDate.iterator().next()));
		}

        return supplierService.findPage(tmpFilters, pageable);
    }

    /**
     * <p>名称：supplierList</p>
     * <p>描述：供应商列表页</p>
     * @author：lushanshan
     * @return
     */
    @ResponseBody
    @RequestMapping("supplierList")
    public List<Supplier> supplierList() {
        return supplierService.findAll();
    }

    /**
     * <p>名称：goSupplierGoodsAll</p>
     * <p>描述： 供应商商品列表页</p>
     * @author：lushanshan
     * @param model
     * @return
     */
    @RequestMapping("goSupplierGoodsAll")
    public String goSupplierGoodsAll(Map<String, Object> model) {
        return "/supplier/supplierGoods";
    }


    /**
     * <p>名称：edit</p>
     * <p>描述：供应商管理的编辑页面</p>
     * @author：lushanshan
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("edit/{id}")
    public String edit(@PathVariable("id") Long id, Map<String, Object> model) {
        Supplier supplier = supplierService.find(id);
        model.put("supplier", supplier);
        return getRelativeViewPath("edit");
    }

    /**
     * <p>名称：add</p>
     * <p>描述：供应商新增操作</p>
     * @author：lushanshan
     */
    @RequestMapping("add")
    public void add() {
    }

    /**
     * <p>名称：delete</p>
     * <p>描述：供应商删除操作</p>
     * @author：lushanshan
     * @param ids
     * @return
     */
    @ResponseBody
    @RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] ids) {

        try {
            //添加日志
            for (Long id :ids){
                Supplier supplier = supplierService.find(id);
                supplierService.delete(id);
                String content = "供应商管理-删除-供应商为"+ supplier.getSupplierName() + "已被删除";
                sysLogService.saveSysLog(Constant.SYSLOG_DELETE,content,"供应商管理");
            }
        } catch (Exception e) {
            return ImmutableMap.<String, Object>of("false", true);
        }

        return ImmutableMap.<String, Object>of("success", true);
    }

    /**
     * <p>名称：check</p>
     * <p>描述：查看供应商的名字是否存在</p>
     * @author：lushanshan
     * @param id
     * @param supplierName
     * @return
     */
    @ResponseBody
    @RequestMapping("check")
    public Boolean check(String id, String supplierName) {
        Filters filters = Filters.create();

        if (StringUtils.hasText(id)) {
            filters.ne("id", id);
        }
        if (StringUtils.hasText(supplierName)) {
            filters.eq("supplierName", supplierName);
        }
        return supplierService.count(filters) < 1;
    }

    /**
     * <p>名称：save</p>
     * <p>描述：新增保存</p>
     * @author：lushanshan
     * @param supplier
     * @param redirectAttributes
     * @return
     */
    @RequestMapping("save")
    public String save(Supplier supplier, RedirectAttributes redirectAttributes) {
        supplierService.save(supplier);
        sysLogService.saveSysLog(Constant.SYSLOG_SAVE,"供应商管理-新增"+supplier.getSupplierName()+"的供应商信息","供应商管理");
        redirectAttributes.addFlashAttribute(SUCCESS_MSG_KEY, "添加成功");
        return "redirect:/supplier/index";
    }

    /**
     * <p>名称：checkCodeIsExits</p>
     * <p>描述：查看供应商编号是否存在</p>
     * @author：lushanshan
     * @param code
     * @return
     */
    @ResponseBody
    @RequestMapping("checkCodeIsExits")
    public Map<String, Boolean>  checkCodeIsExits(String code) {
        return supplierService.checkCodeIsExits(code);
    }

    /**
     * <p>名称：update</p>
     * <p>描述：更新操作</p>
     * @author：lushanshan
     * @param supplier
     * @param redirectAttributes
     * @return
     */
    @RequestMapping("update")
    public String update(Supplier supplier,
                         RedirectAttributes redirectAttributes) {

        supplierService.update(supplier);
        sysLogService.saveSysLog(Constant.SYSLOG_UPDATE,"供应商管理-修改"+supplier.getSupplierName()+"的供应商信息","供应商管理");
        redirectAttributes.addFlashAttribute(SUCCESS_MSG_KEY, "更新成功");
        return "redirect:/supplier/index";
    }

    /**
     * <p>名称：combobox</p>
     * <p>描述：供应商列表下拉框</p>
     * @author：lushanshan
     * @param goodsId
     * @return
     */
    @ResponseBody
    @RequestMapping("combo")
    public List<Supplier> combobox(@RequestParam("goodsId") String goodsId) {
        List<Supplier> result = new ArrayList<Supplier>();

        if (StringUtils.hasText(goodsId)) {
            result = supplierService.getListByGoodsId(goodsId);
        }
        return result;
    }

    /**
     * <p>名称：gotoRating</p>
     * <p>描述：供应商追加评分页面</p>
     * @author：lushanshan
     * @param supplierId
     * @return
     */
    @RequestMapping("gotoRating")
    public ModelAndView gotoRating(Long supplierId) {
        if (null == supplierId) {
            return null;
        }
        Supplier supplier = supplierService.find(supplierId);
        if (supplier == null) {
            return null;
        }
        Map<String, Object> map = Maps.newHashMap();
        map.put("model", supplier);
        return new ModelAndView("/supplier/gotoRating", map);
    }

    /**
     * <p>名称：search</p>
     * <p>描述：搜索供应商</p>
     * @author：lushanshan
     * @param keyword
     * @param size
     * @return
     */
    @ResponseBody
    @RequestMapping("search-supplier")
    public List<Map<String, Object>> search(
            String keyword,
            String size) {
        Filters filters = Filters.create();
        filters.like("legalName", "%" + keyword + "%");
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(1);
        pageRequest.setPageSize(30);

        List<Supplier> many = supplierService.findMany(filters, pageRequest);
        return Lists.transform(many, new Function<Supplier, Map<String, Object>>() {

            @Override
            public Map<String, Object> apply(Supplier input) {
                Map<String, Object> map = Maps.newHashMap();
                Long id = input.getId();
                Long code = input.getNumberCode();
                String name = input.getSupplierName();
                String label = name + "【" + code + "】";
                map.put("text", label);
                map.put("id", id);
                map.put("supplierName", name);
                return map;
            }
        });
    }


    /**
     * <p>名称：searchCustomer</p>
     * <p>描述：查询所有供应商</p>
     * @author：lushanshan
     * @param keyword
     * @param size
     * @return
     */
    /*@ResponseBody
    @RequestMapping("searchCustomer")
    public List<Map<String, Object>> searchCustomer(String keyword, String size) {
        Filters filters = new Filters();
        filters.like("supplierName", "%" + keyword + "%");
        int maxResult = 100;
        if (org.springframework.util.StringUtils.hasText(size)) {
            try {
                maxResult = Integer.parseInt(size);
            } catch (NumberFormatException ignore) { *//* ignore *//*
            }
        }
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(0);
        pageRequest.setPageSize(maxResult);

        List<Supplier> many = supplierService.findMany(filters, pageRequest);

        return Lists.transform(many, new Function<Supplier, Map<String, Object>>() {

            @Override
            public Map<String, Object> apply(Supplier input) {
                Map<String, Object> map = Maps.newHashMap();
                map.put("id", input.getId());

                String text = input.getSupplierName();
                map.put("text", text);
                return map;
            }
        });

    }*/


}