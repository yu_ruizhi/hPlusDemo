/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: NewOrHot.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月20日 上午9:26:00
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;

/**
 * 
 * <p>名称: 新品上架\APP首页艾曼热卖</p>
 * <p>说明: 对应表为T_NEW_OR_HOT</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月20日 上午9:26:28 
 * @version: 1.0
 */
public class NewOrHot extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields goodsId : 单品ID
	 * @Column GOODS_ID
	 */
	private Long goodsId;
	
	/**
	 * @Fields  sort : 排序
	 * @Column SORT
	 */
	private Integer sort;
	
	/**
	 * @Fields type : 类型
	 * @Column TYPE
	 */
	private Integer type;
	
	/**
	 * @Fields deleted : 删除标示
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	/**
	 * @Fields goods : 单品对象
	 */
	private Goods goods;
	
	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}

}
