/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategory.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.marketing
 * Date: 2016年4月20日 上午10:22:42 
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 分类商品实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:22:42   
 * @version: 1.0
 */
public class ProductCategory extends BaseEntity{

	private static final long serialVersionUID = 1L;

	/**@Fields name : 分类名称 */ 
	private String name;

	/**@Fields image : 图片 */ 
	private String image;

	/**@Fields sort : 排序 */ 
	private Integer sort;

	/**@Fields enabled : 是否启用 */ 
	private Boolean enabled = false;

	/**@Fields deleted : 删除标识 */ 
	private Boolean deleted = false;

	private List<ProductCategoryProduct> productCategoryProductList;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public List<ProductCategoryProduct> getProductCategoryProductList() {
		return productCategoryProductList;
	}

	public void setProductCategoryProductList(List<ProductCategoryProduct> productCategoryProductList) {
		this.productCategoryProductList = productCategoryProductList;
	}
	
}
