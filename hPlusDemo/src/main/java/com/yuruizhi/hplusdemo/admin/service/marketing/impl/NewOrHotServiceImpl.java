/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: NewOrHotServiceImlp.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月20日 上午9:32:34
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.marketing.NewOrHotDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.NewOrHot;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.NewOrHotService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;

/**
 * 
 * <p>名称: 新品上架\APP首页艾曼热卖  Service实现类</p>
 * <p>说明: TODO</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月20日 上午9:32:34  
 * @version: 1.0
 */
@Service
public class NewOrHotServiceImpl extends BaseServiceImpl<NewOrHot>implements NewOrHotService {

	private NewOrHotDao newOrHotDao;
	
	@Autowired
	private SysLogService sysLogService;
	
	@Autowired
	public void setNewOrHotProductDao(NewOrHotDao newOrHotDao) {
		this.newOrHotDao = newOrHotDao;
		super.setBaseDao(newOrHotDao);
	}

	/*(non-Javadoc) 
	 * <p>名称: 新品上架save</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl#save(java.lang.Object)
	 */ 
	@Override
	public void save(NewOrHot e) {
		super.save(e);
		if (Constant.NEW_PRODUCT_TYPE.equals(e.getType())) {
			sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "新品上架管理-新增-ID:"+e.getId(), "运营管理");			
		} else {
			sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "艾曼热卖管理-新增-ID:"+e.getId(), "运营管理");
		}
	}

	/*(non-Javadoc) 
	 * <p>名称: update</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param e 
	 * @see com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl#update(java.lang.Object)
	 */ 
	@Override
	public void update(NewOrHot e) {
		super.update(e);
		if (Constant.NEW_PRODUCT_TYPE.equals(e.getType())) {
			sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "新品上架管理-修改-ID:"+e.getId(), "运营管理");			
		} else {
			sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "艾曼热卖管理-修改-ID:"+e.getId(), "运营管理");						
		}
	}

	/*(non-Javadoc) 
	 * <p>名称: delete</p> 
	 * <p>描述: </p> 
	 * <p>author：wentan</p> 
	 * @param id 
	 * @see org.ponly.webbase.service.support.CrudServiceImpl#delete(java.io.Serializable) 
	 */ 
	@Override
	public void delete(Long id) {
		NewOrHot newOrHot = newOrHotDao.find(id);
		super.delete(id);
		if (Constant.NEW_PRODUCT_TYPE.equals(newOrHot.getType())) {
			sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "新品上架管理-删除-ID:"+id, "运营管理");			
		} else {
			sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "艾曼热卖管理-删除-ID:"+id, "运营管理");
		}
	}

	@Override
	public Page<NewOrHot> findPage(Filters filters, Filters goodsFilters, Filters categoryFilters,
			Pageable pageable) {

        Map<String, Object> paramsMap = createParamsMap();
        if (null != filters) {
            paramsMap.put(NewOrHotDao.BASE_FILTERS_PROP, filters);
        }
        if (null != goodsFilters) {
            paramsMap.put(NewOrHotDao.GOODS_FILTERS_PROP, goodsFilters);
        }
        if (null != categoryFilters) {
            paramsMap.put(NewOrHotDao.CATEGORY_FILTERS_PROP, categoryFilters);
        }
		if (null != pageable) {
			paramsMap.put(NewOrHotDao.OFFSET_PROP, pageable.getOffset());

			paramsMap.put(NewOrHotDao.MAX_RESULTS_PROP, pageable.getPageSize());

			Sort sort = pageable.getSort();
			if (null != sort) {
				paramsMap.put(NewOrHotDao.SORT_PROP, sort);
			}
		}
        int total = count(paramsMap);
        List<NewOrHot> data = total > 0 ? findMany(paramsMap) : Collections.<NewOrHot>emptyList();
        return new SimplePage<NewOrHot>(pageable, data, total);
	}

}
