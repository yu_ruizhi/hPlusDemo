/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.interceptor;

import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.perm.PermissionService;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>名称: PermissionInterceptor</p>
 * <p>说明: 权限控制拦截器</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:48:19   
 * @version: 1.0
 */
@Repository
public class PermissionInterceptor extends HandlerInterceptorAdapter {

    //    @Value("${permission.interceptor.whiteList}")
//    private String whiteListString;
//
//    @Autowired
//    private CacheUtils cacheUtil;
//
    @Autowired
    private PermissionService permissionService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestUri = WebUtils.getPathWithinApplication(request);
        
        Long currentUser = UserUtils.getCurrentUserId();
        String prefix = Constant.ADMIN_PREFIX;
        prefix = prefix.startsWith("/") ? prefix.substring(1) : prefix;
        prefix = prefix.endsWith("/") ? prefix.substring(0, prefix.length() - 1) : prefix;
        
        //放过验证码拦截
        if(requestUri.startsWith("/captcha")){
        	return true;
        }
        //放过app接口
        if(requestUri.startsWith("/api")){
            return true;
        }

        //放过home update by yuruizhi on 2016.12.08
        if(requestUri.startsWith("/homePage")){
            return true;
        }
        
        /* *******************************
         *       代码优化 [[
         * ******************************/

        /*-
         * 获取模块之后内容的最后一个"/"的索引, 以便获取模块uri
         * eg: /admin/module/index --> module/index 之间"/" 的索引
         *      /admin/module --> -1
         */
        int endIndex = requestUri.lastIndexOf("/");

        // 处理特殊情况 /admin/module/edit/1
        if (4 < endIndex && endIndex - 5 == requestUri.indexOf("/edit/")) {
            // endIndex = requestUri.lastIndexOf('/', endIndex - 1);
            endIndex -= 5;
        }

        // 获取模块路径 eg: /admin/module/index -- admin/module/
        String moduleUri = endIndex == 0 ? "/" : (-1 < endIndex ? requestUri.substring(1, endIndex) : requestUri.substring(1));

        /* *******************************
         *       代码优化 ]]
         * ******************************/

        /*-
         * 优化不下去了, 谁告诉我 uuu 是干嘛的, uu 又是干嘛的
         */
        moduleUri += "/";
        request.setAttribute("moduleUri", moduleUri);
        request.setAttribute("requestUri", requestUri);

        // 如果是登录请求
        if (requestUri.endsWith("login/loginSubmit")) {
            return true;
        }

        // 如果不是登录请求且没有登录
        if (null == currentUser) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/admin/login.jsp");
            if (null == dispatcher) {
                throw new IllegalStateException();
            }
            dispatcher.forward(request, response);
            return false;
        }

        // 已经登录
        List<Module> modules = permissionService.getPermissionFromCache();
        for (Module module : modules) {
            for (AdminPage page : module.getPages()) {
                String pageUrl = page.getPageUrl();
                if (null != pageUrl && pageUrl.contains(moduleUri)) {
                    request.setAttribute("mid", module.getId());
                    break;
                }
            }
        }
        request.setAttribute("modules", modules);
        return true;
    }
}
