/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.entity.cms.AdBlock;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.AdBlockService;

/**
 * <p>名称: AdBlockServiceImpl</p>
 * <p>说明: 广告位服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:57:58   
 * @version: 1.0
 */
@Service
public class AdBlockServiceImpl extends BaseServiceImpl<AdBlock> implements AdBlockService {
		
}
