/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.apache.ibatis.annotations.Param;
import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Combine;

/**
 * Created on 2016-03-18 09:50:39
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface CombineDao extends BaseDao<Combine> {

	void deleteById(@Param("id")Long id);

}