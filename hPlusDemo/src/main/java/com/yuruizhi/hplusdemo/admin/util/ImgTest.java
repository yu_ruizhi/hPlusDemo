package com.yuruizhi.hplusdemo.admin.util;

import org.ponly.common.util.IOUtils;
import org.ponly.fs.FileSystem;
import org.ponly.fs.FileSystems;
import org.ponly.fs.FileView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;

/**
 * Created by 于瑞智 on 2016/7/20.
 */
public class ImgTest {
    public static void main(String[] args) {
        try {

            String fsUri = "local:///d:/fs/";
            FileSystem fs = FileSystems.getFileSystem(fsUri);  // 创建一个基于本地文件系统, 以 d:/fs 为根目录的文件系统

            System.out.println("本地地址是否存在1.jpg：" + fs.exists("1.jpg"));

            InputStream in = fs.open("1.jpg"); // 读取 1.jpg 文件内容

            String fsUriNew = "aliyun://KudDSWR2yHbMYrBR:chHHYo2Q6HLPH7CkrfjqjWYq5jnT6S@aimon.oss-cn-shanghai.aliyuncs.com";  //新：艾漫阿里云OSS
            FileSystem fsNew = FileSystems.getFileSystem(fsUriNew);


            /*FileView[] fileViews = fsNew.ls("");
            for (FileView fv : fileViews) {
                System.out.println(fv.getPath());
            }*/

            OutputStream opt = fsNew.create("fs/4.jpg", true);  //在该文件系统中创建一个 fileName, 如果存在则覆盖

            try {
                int val = -1;
                while ((val = in.read()) != -1) {
                    opt.write(val);
                }
                opt.flush();
                opt.close();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("OSS地址3.jpg是否创建成功：" + fsNew.exists("fs/4.jpg"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
