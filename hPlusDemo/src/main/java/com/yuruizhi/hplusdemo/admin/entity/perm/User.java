/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.perm;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.Date;

/**
 * <p>名称: User</p>
 * <p>说明: 后台用户类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:16:13   
 * @version: 1.0
 */
public class User extends BaseEntity {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 2180815742145516183L;

	private String loginName;

    private String password;

    private Boolean isFreeze;

    private List<Role> role;

    private String remark;

    private Date lastLoginTime;

    private Boolean deleted;

    private Boolean islookAllGoods;

    public Boolean getIslookAllGoods() {
        return islookAllGoods;
    }

    public void setIslookAllGoods(Boolean islookAllGoods) {
        this.islookAllGoods = islookAllGoods;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getIsFreeze() {
        return isFreeze;
    }

    public void setIsFreeze(Boolean isFreeze) {
        this.isFreeze = isFreeze;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<Role> getRole() {
        return role;
    }

    public void setRole(List<Role> role) {
        this.role = role;
    }
}
