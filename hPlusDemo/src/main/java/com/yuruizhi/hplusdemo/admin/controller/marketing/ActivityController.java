/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 * <p>
 * ProjectName: hPlusDemo
 * FileName: ActivityController.java
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.marketing
 * Date: 2016年4月21日 下午2:28:39
 **/
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.*;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.marketing.*;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * <p>名称: 活动管理Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 *
 * @author：wuqi
 * @date：2016年4月21日 下午2:28:39   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "activity")
public class ActivityController extends AdminBaseController<Activity> {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private ActivityGoodsService activityGoodsService;

    @Autowired
    private SysLogService sysLogService;

    @Autowired
    private MemberService memberService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private MessageLogService messageLogService;

    /**
     *
     * <p>名称：日期转换器</p>
     * <p>描述：将前台传回来的String时间转换格式</p>
     * @author：wuqi
     * @param binder
     */
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(
                new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"), false));
    }

    /**
     *
     * <p>名称：异步获取数据</p>
     * <p>描述：获取活动管理列表页显示的数据</p>
     * @author：wuqi
     * @param filters 过滤器
     * @param pageable 初始页数
     * @return
     *
     */
    @RequestMapping("list")
    @ResponseBody
    public Page<Activity> list(Filters filters, Pageable pageable) {
        return activityService.findPage(filters, pageable);
    }

    /**
     *
     * <p>名称：保存数据</p>
     * <p>描述：对新增的数据进行保存</p>
     * @author：wuqi
     * @param activity 传入的新增的数据
     * @return
     * <p>  update by yuruizhi on 20160706 发布商品活动给会员发送站内信 </p>
     */
    @RequestMapping("saveActivity")
    public String saveActivity(Activity activity) {
        activityService.save(activity);

        //获取活动单品信息进行保存
        if (0 == activity.getType()) {
            List<ActivityGoods> activityGoodsList = activity.getActivityGoodsList();

            String goodsOperation = "活动管理-新增-id:" + activity.getId() + ",单品id:";   //系统日志

            if (!activityGoodsList.isEmpty()) {
                for (ActivityGoods activityGoods : activityGoodsList) {
                    if (null != activityGoods) {
                        activityGoods.setActivityId(activity.getId());
                        activityGoodsService.save(activityGoods);
                        goodsOperation += activityGoods.getId() + ",";
                    }
                }

            }

            Date startDate = activity.getStartTime();
            Date endDate = activity.getEndTime();
            Date now = new Date();

            /**
             * 如果是发布到前台的商品活动，站内信通知用户
             */
            if (now.after(startDate) && now.before(endDate)) {
                SendMessageOfActivity(activity);
            }


            //系统日志
            goodsOperation = goodsOperation.substring(0, goodsOperation.length() - 1);
            String activityOperation = "活动管理-新增-id:" + activity.getId() + ",名称:" + activity.getName();
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE, goodsOperation, "活动管理");
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE, activityOperation, "活动管理");
        }

        return redirectViewPath("index");
    }

    private void SendMessageOfActivity(Activity activity) {
        // 发送创建好的站内信给所有会员
        String title = null == activity.getName() ? "商品促销活动来啦！" : activity.getName();
        MessageLog messageLog = new MessageLog();
        messageLog.setTitle(title);
        messageLog.setContent("商品促销活动来啦！童鞋们赶紧围观了~~");
        messageLog.setState(Constant.MESSAGE_STATE_UNREAD); //默认未读
        messageLog.setType(Constant.MESSAGE_TYPE_ESHOP); //电商私信

        List<Member> memberList = memberService.findAll(); //所有前台会员
        for (Member member : memberList) {
            Long memberId = member.getId();
            messageLog.setMemberId(memberId);
            messageLogService.save(messageLog);   //注册成功发送站内信
        }
    }

    /**
     *
     * <p>名称：编辑</p>
     * <p>描述：从列表页面进行编辑页面</p>
     * @author：wuqi
     * @param id 传入id
     * @return
     */
    @Override
    @RequestMapping("edit/{id}")
    public String _compat(@PathVariable(value = "id") Long id, Map<String, Object> model) {
        Activity activity = activityService.find(id);
        List<ActivityGoods> activityGoods = activityGoodsService.findByActivityId(activity.getId());
        activity.setActivityGoodsList(activityGoods);
        model.put("activity", activity);
        return getRelativeViewPath("edit");
    }

    /**
     *
     * <p>名称：更新</p>
     * <p>描述：对编辑页面修改的数据进行更新</p>
     * @author：wuqi
     * @param activity 修改后的活动数据
     * @return
     * update by yuruizhi on 20160706 方法增加发布到前台的商品促销活动给用户发送站内信
     */
    @Override
    @RequestMapping("update")
    public String update(Activity activity, BindingResult binding, RedirectAttributes redirectAttrs) {
        activityService.update(activity);
        //对活动商品进行操作
        if (0 == activity.getType()) {
            List<ActivityGoods> activityGoodsList = activity.getActivityGoodsList();
            for (ActivityGoods activityGoods : activityGoodsList) {
                if (activityGoods != null) {
                    ActivityGoods oldActivityGoods = activityGoodsService.findByGoodsIdAndActivityId(activityGoods.getGoodsId(), activity.getId());
                    if (null == oldActivityGoods) {
                        activityGoods.setActivityId(activity.getId());
                        activityGoodsService.save(activityGoods);
                    } else {
                        if ((oldActivityGoods.getSort()) != (activityGoods.getSort())) {
                            activityGoodsService.update(activityGoods);
                        }
                    }
                }
            }

            /**
             * 如果是发布到前台的商品活动，站内信通知用户
             */
            Date startDate = activity.getStartTime();
            Date endDate = activity.getEndTime();
            Date now = new Date();

            if (now.after(startDate) && now.before(endDate)) {
                SendMessageOfActivity(activity);
            }

        }

        //系统日志
        String operation = "活动管理-修改-id:" + activity.getId();
        sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "活动管理");
        return redirectViewPath("index");
    }

    @Override
    @ResponseBody
    @RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
        activityService.delete(id);
        String ids = "";
        for (int i = 0; i < id.length; i++) {
            ids = id[i] + ",";
        }
        ids = ids.substring(0, ids.length() - 1);

        //系统日志
        String operation = "活动管理-删除-id:" + ids;
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "活动管理");
        return ImmutableMap.<String, Object>of("success", true);
    }

    /**
     *
     * <p>名称：根据id删除活动中的单品</p>
     * <p>描述：在编辑页面异步根据单品id删除活动的单品</p>
     * @author：wuqi
     * @param id 单品id
     * @return
     */
    @ResponseBody
    @RequestMapping("deleteGoods")
    public Map<String, Object> deleteGoods(Long id) {
        Map<String, Object> map = Maps.newHashMap();
        ActivityGoods activityGoods = activityGoodsService.find(id);
        activityGoodsService.delete(id);
        if (null != activityGoods) {
            String operation = "活动管理-删除-活动id:" + activityGoods.getId() + ",单品id:" + id;
            sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "活动管理");
        }

        map.put("success", true);
        return map;
    }

    /**
     *
     * <p>名称：检测是否重名</p>
     * <p>描述：新增页面根据活动名称检测是否重名</p>
     * @author：wuqi
     * @param name 活动名称
     * @return
     */
    @RequestMapping("checkIsNameExists")
    @ResponseBody
    public Map<String, Boolean> checkIsNameExists(String name) {
        return activityService.checkIsNameExists(name);
    }

    /**
     *
     * <p>名称：检测是否重名</p>
     * <p>描述：编辑页面根据活动id及活动名称检测是否重名</p>
     * @author：wuqi
     * @param idAndName 活动id及活动名称
     * @return
     */
    @RequestMapping("hasAnnotherName")
    @ResponseBody
    public Map<String, Boolean> hasAnnotherName(String[] idAndName) {
        Map<String, Boolean> map = activityService.hasAnnotherName(idAndName);
        return map;
    }
}
