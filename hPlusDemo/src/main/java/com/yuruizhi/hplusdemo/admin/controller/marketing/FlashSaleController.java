/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.FlashSale;
import com.yuruizhi.hplusdemo.admin.service.marketing.FlashSaleDetailService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created on 2016-03-17 20:29:03
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "flashSale")
public class FlashSaleController extends AdminBaseController<FlashSale> {
    @Autowired
    private FlashSaleDetailService flashSaleDetailService;


    @Override
    public void input(FlashSale flashSale, Map<String, Object> model) {
        super.input(flashSale, model);
        if (null != flashSale.getId()) {
            flashSale.setFlashSaleDetails(flashSaleDetailService.findMany("promotionId", flashSale.getId()));
        }
    }

    @Override
    protected FlashSale prepareModel(@RequestParam(value = "id", required = false) Long id) {
        FlashSale flashSale = super.prepareModel(id);

        // 新创建对象
        if (null == flashSale.getId()) {
            flashSale.setDeleted(false);
            flashSale.setLimitQty(1);
            flashSale.setAllowDeduction(true);
        }
        return flashSale;
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, "startTime", new CustomDateEditor(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), false
        ));
        binder.registerCustomEditor(Date.class, "endTime", new CustomDateEditor(
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), false
        ));
    }
}
