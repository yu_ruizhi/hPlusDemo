/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.service.BaseService;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Coupon;

/**
 * <p>名称: CouponService</p>
 * <p>说明: 优惠券服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:13:08   
 * @version: 1.0
 */
public interface CouponService extends BaseService<Coupon> {

	void couponSend(Long[] memberIds, Coupon cooupon);

	void batchSave(Coupon coupon, int createCount);
}
