/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: KeywordServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.platform
 * Date: 2016年4月23日 下午1:49:25
 **/
package com.yuruizhi.hplusdemo.admin.controller.platform;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.platform.Keyword;
import com.yuruizhi.hplusdemo.admin.service.platform.KeywordService;

/**
 * 
 * <p>名称: 搜索管理 关键字Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月23日 下午1:49:25   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "keyword")
public class KeywordController extends AdminBaseController<Keyword> {

	@Autowired
	private KeywordService keywordService;
	
	/**
	 * 
	 * <p>名称：验证关键词唯一性</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param name 需验证的关键词
	 * @return
	 */
	@RequestMapping("checkIsKeywordExists")
	@ResponseBody
	public Map<String, Boolean> checkIsWorksExists(String name) {
		return keywordService.checkIsKeywordExists(name);
	}
	
	/**
	 * 
	 * <p>名称：验证除当前关键词外是否还有同名关键词</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param id 当前关键词ID
	 * @param name 要验证的关键词
	 * @return
	 */
	@RequestMapping("checkIsAnotherKeywordExists")
	@ResponseBody
	public Boolean checkIsAnotherWorksExists(Long id, String name) {
		return keywordService.checkIsAnotherKeywordExists(id, name);
	}
	
	/**
	 *
	 * <p>名称：搜索管理-编辑页</p>
	 * <p>描述：</p>
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("edit/{id}")
	@Override
    public String _compat(@PathVariable("id") Long id, Map<String, Object> model) {

		Keyword keyword = keywordService.find(id);
		
        model.put("keyword", keyword);
        
        return getRelativeViewPath("edit");
    }
}
