/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.cms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Ad;
import com.yuruizhi.hplusdemo.admin.entity.cms.AdBlock;
import com.yuruizhi.hplusdemo.admin.service.cms.AdBlockService;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>名称: 广告管理Contrller</p>
 * <p>说明: 广告管理控制器类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 *
 * @author：ChenGuang
 * @date：2016年5月5日下午3:46:17
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/ad")
public class AdController extends AdminBaseController<Ad> {
    @Autowired
    private AdBlockService adBlockService;

    @Override
    public void input(Ad ad, Map<String, Object> model) {
        super.input(ad, model);
        model.put("adBlocks", adBlockService.findAll());
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(AdBlock.class, "adBlock", new CustomStringBeanPropertyEditor(AdBlock.class));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }

    @Override
    protected Ad prepareModel(Long id) {
        Ad ad = super.prepareModel(id);
        // 对新增的数据, 设置默认属性
        if (null == ad.getId()) {
            ad.setEnabled(true);
        }
        return ad;
    }
}
