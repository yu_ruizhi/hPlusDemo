/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.order.RefundImg;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.order.RefundImgService;

/**
 * <p>名称: RefundImgServiceImpl</p>
 * <p>说明: 退换货图片服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:31:45   
 * @version: 1.0
 */
@Service
public class RefundImgServiceImpl extends BaseServiceImpl<RefundImg> implements RefundImgService {
}
