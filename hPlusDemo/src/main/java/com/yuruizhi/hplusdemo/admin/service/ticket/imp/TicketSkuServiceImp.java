/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: AreaService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area
 * Date: 2016年4月18日下午7:40:44
 **/
package com.yuruizhi.hplusdemo.admin.service.ticket.imp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;




import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketSku;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;


import com.yuruizhi.hplusdemo.admin.service.ticket.TicketSkuService;


@Service
public class TicketSkuServiceImp extends BaseServiceImpl<TicketSku> implements TicketSkuService{


	
}
