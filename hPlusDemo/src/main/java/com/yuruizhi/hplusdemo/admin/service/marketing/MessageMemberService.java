/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageMemberService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月20日下午2:24:44
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageMember;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 私信-会员Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日下午2:25:04
 * @version: 1.0
 */
public interface MessageMemberService extends BaseService<MessageMember>{
	
	/**
	 * 
	 * <p>名称：根据会员id、私信id查询记录</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param messageMember 私信-会员实体
	 * @return
	 */
	MessageMember findByMemberIdMessageId(MessageMember messageMember );
}
