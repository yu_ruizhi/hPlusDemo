/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.service.BaseService;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

/**
 * <p>名称: OrdersReceiptService</p>
 * <p>说明: 订单服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:35:04   
 * @version: 1.0
 */
public interface OrdersReceiptService extends BaseService<OrdersReceipt> {

    Integer booleanLogisticsNo(String logisticsNo);
    
    void addLogisticsNoz(OrdersReceipt ordersReceipt);
    
    void updateStatus(Long id);
    
    void goCargo(OrdersReceipt ordersReceipt);
    /**
	 * @param paramMap
	 *            userId 用户ID， orderStatus订单状态， orderType 订单类型
	 * @return 特定条件下的订单总数
	 * @description 根据用户ID，订单状态，订单类型查询订单的总数
	 */
	int getCountByParams(Map<String, Object> paramMap);

	List<OrdersReceipt> checkExpirationDate(List<OrdersReceipt> list);

	OrdersReceipt getOrdersReceiptById(Long orderId);
	
	int updateDetailStatus(String id, String status);
	
    List<OrdersReceipt>  selectAll();
    
     void updateOrdersStatus(Long orderId, Integer status);

    OrdersReceipt findOrdersReceipt(Long id);

    void updateActualAmount(OrdersReceipt ordersReceipt);
    
    void cancelOrder(OrdersReceipt or);
    
    void updateDiscountAmount(OrdersReceipt or);

    Page<OrdersReceipt> findPage(Filters filters, Pageable pageable);
    
    /**
     * 
     * <p>名称：根据时间获取订单id</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    Long[] findIdByTime(String beginDate,String endDate);
}
