/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.cms;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.*;

/**
 * <p>名称: Article</p>
 * <p>说明: 文章百科问答实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:41:16   
 * @version: 1.0
 */
public class Article extends BaseEntity {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -2046093520013230858L;

	/**
     * 所属栏目
     * 
     * @ViewField editor=treepicker internal="id:name" 
     * @ManyToOne joinColumn=CHANNEL_ID optional=true
     */
    private Channel channel;

    /**
     * 文章标题
     * 
     * @ViewField editor=input 
     * @Column TITLE
     */
    @NotNull
    @Size(max = 255)
    private String title;
    
    
    private String artTagName;

    /**
     * 文章类型
     * 
     * @ViewField editor=spinner 
     * @Column TYPE
     */
    private Integer type;

    /**
     * 文章来源
     * SOURCE, FROM 都是关键字
     * 
     * @ViewField editor=input 
     * @Column REF
     */
    @Size(max = 255)
    private String ref;

    /**
     * 作者
     * 
     * @ViewField editor=input 
     * @Column AUTHOR
     */
    @Size(max = 255)
    private String author;

    /**
     * 缩略图
     * 
     * @ViewField editor=input 
     * @Column IMAGE
     */
    @Size(max = 255)
    private String image;

    /**
     * 简述
     * 
     * @ViewField editor=input 
     * @Column EXCERPT
     */
    @Size(max = 500)
    private String excerpt;

    /**
     * 跳转链接
     * 
     * @ViewField editor=input 
     * @Column HREF
     */
    @Size(max = 255)
    private String href;

    /**
     * 内容
     * 
     * @ViewField editor=input 
     * @Column CONTENT
     */
    private String content;

    /**
     * 点击量
     * 
     * @ViewField editor=spinner 
     * @Column HITS
     */
    private Integer hits;

    /**
     * 点赞数
     * 
     * @ViewField editor=spinner 
     * @Column LIKES
     */
    private Integer likes;

    /**
     * 评论数
     * 
     * @ViewField editor=spinner 
     * @Column REVIEWS
     */
    private Integer reviews;

    /**
     * 是否置顶
     * 
     * @ViewField editor=input 
     * @Column STICKY
     */
    private Boolean sticky = Boolean.FALSE; ;

    /**
     * 排序
     * 
     * @ViewField editor=spinner 
     * @Column POSITION
     */
    private Integer position;

    /**
     * 是否允许评论
     * 
     * @ViewField editor=input 
     * @Column ALLOW_COMMENT
     */
    private Boolean allowComment = Boolean.FALSE; ;

    /**
     * 是否已经审核
     * 
     * @ViewField editor=input 
     * @Column REVIEWED
     */
    private Boolean reviewed = Boolean.FALSE; ;

    /**
     * 是否发布
     * 
     * @ViewField editor=input 
     * @Column PUBLISHED
     */
    private Boolean published = Boolean.FALSE; ;

    /**
     * 发布时间
     * 
     * @ViewField editor=datepicker 
     * @Column PUBLISH_TIME
     */
    private Date publishTime;

    /**
     * SEO_TITLE
     * 
     * @ViewField editor=input 
     * @Column SEO_TITLE
     */
    @Size(max = 255)
    private String seoTitle;

    /**
     * SEO_KEYWORDS
     * 
     * @ViewField editor=input 
     * @Column SEO_KEYWORDS
     */
    @Size(max = 255)
    private String seoKeywords;

    /**
     * SEO_DESCRIPTION
     * 
     * @ViewField editor=input 
     * @Column SEO_DESCRIPTION
     */
    @Size(max = 255)
    private String seoDescription;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Channel getChannel() {
        return this.channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRef() {
        return this.ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getExcerpt() {
        return this.excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getHits() {
        return this.hits;
    }

    public void setHits(Integer hits) {
        this.hits = hits;
    }

    public Integer getLikes() {
        return this.likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getReviews() {
        return this.reviews;
    }

    public void setReviews(Integer reviews) {
        this.reviews = reviews;
    }

    public Boolean getSticky() {
        return this.sticky;
    }

    public void setSticky(Boolean sticky) {
        this.sticky = sticky;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getAllowComment() {
        return this.allowComment;
    }

    public void setAllowComment(Boolean allowComment) {
        this.allowComment = allowComment;
    }

    public Boolean getReviewed() {
        return this.reviewed;
    }

    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    public Boolean getPublished() {
        return this.published;
    }

    public void setPublished(Boolean published) {
        this.published = published;
    }

    public Date getPublishTime() {
        return this.publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getSeoTitle() {
        return this.seoTitle;
    }

    public void setSeoTitle(String seoTitle) {
        this.seoTitle = seoTitle;
    }

    public String getSeoKeywords() {
        return this.seoKeywords;
    }

    public void setSeoKeywords(String seoKeywords) {
        this.seoKeywords = seoKeywords;
    }

    public String getSeoDescription() {
        return this.seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public String getArtTagName() {
		return artTagName;
	}

	public void setArtTagName(String artTagName) {
		this.artTagName = artTagName;
	}

}
