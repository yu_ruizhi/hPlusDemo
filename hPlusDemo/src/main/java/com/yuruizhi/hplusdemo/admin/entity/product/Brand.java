/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <p>名称: Brand</p>
 * <p>说明: 产品品牌实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:17:42   
 * @version: 1.0
 */
public class Brand extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 3881814132759463933L;

	/**
     * 名称
     *
     * @ViewField editor=input
     * @Column NAME
     */
    @Size(max = 255)
    private String name;

    /**
     * 别名
     *
     * @ViewField editor=input
     * @Column ALIAS
     */
    @Size(max = 255)
    private String alias;

    /**
     * 网址
     *
     * @ViewField editor=input
     * @Column URL
     */
    @Size(max = 255)
    private String url;

    /**
     * LOGO图片路径
     *
     * @ViewField editor=input
     * @Column LOGO
     */
    @Size(max = 500)
    private String logo;

    /**
     * 详情介绍
     *
     * @ViewField editor=input
     * @Column DETAIL
     */
    private String detail;

    /**
     * 是否启用
     *
     * @ViewField editor=input
     * @Column ENABLED
     */
    @NotNull
    private Boolean enabled=Boolean.FALSE;

    /**
     * 排序
     *
     * @ViewField editor=spinner
     * @Column POSITION
     */
    private Integer position;

    /**
     * 是否删除
     * 删除标记,1删除，0未删除
     *
     * @ViewField editor=input
     * @Column DELETED
     */
    private Boolean deleted;
    
    /**
     * 是否推荐
     * 1 推荐，0不推荐
     *
     * @ViewField editor=input
     * @Column RECOMMENDED
     */
    private Boolean recommended = Boolean.FALSE;

    public Boolean getRecommended() {
		return recommended;
	}

	public void setRecommended(Boolean recommended) {
		this.recommended = recommended;
	}

	public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return this.logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getDetail() {
        return this.detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
