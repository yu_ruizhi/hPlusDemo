/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.Date;

/**
 * <p>名称: Promotion</p>
 * <p>说明: 促销活动实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:06:26   
 * @version: 1.0
 */
public class Promotion extends BaseEntity {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 5036463761287420206L;

	/**
     * 活动模式, 限时抢购
     */
    public static final Integer PROMOTION_MODE_FLASH_SALE = 1;

    /**
     * 活动名称
     *
     * @ViewField editor=input
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 活动开始时间
     *
     * @ViewField editor=datepicker
     * @Column START_TIME
     */
    @NotNull
    private Date startTime;

    /**
     * 活动结束时间
     *
     * @ViewField editor=datepicker
     * @Column END_TIME
     */
    @NotNull
    private Date endTime;

    /**
     * 活动类型（线上,线下,栏目等）
     *
     * @ViewField editor=spinner
     * @Column TYPE
     */
    private Integer type;

    /**
     * 活动模式（折扣,满减）
     *
     * @ViewField editor=spinner
     * @Column MODE
     */
//    @NotNull
    private Integer mode;

    /**
     * 活动状态
     *
     * @ViewField editor=spinner
     * @Column STATUS
     */
//    @NotNull
    private Integer status;

    /**
     * 活动备注
     *
     * @ViewField editor=input
     * @Column NOTE
     */
    @Size(max = 500)
    private String note;

    /**
     * 是否删除
     *
     * @ViewField editor=input
     * @Column DELETED
     */
//    @NotNull
    private Boolean deleted;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartTime() {
        return this.startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return this.endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getMode() {
        return this.mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
