/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: BrandCategory</p>
 * <p>说明: 产品品牌分类实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:18:07   
 * @version: 1.0
 */
public class BrandCategory extends BaseEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String brandId;
	private String categoryId;
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	   @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        BrandCategory bc = (BrandCategory) o;

	        if (brandId != null ? !brandId.equals(bc.brandId) : bc.brandId != null) return false;

	        return true;
	    }
	    @Override
	    public int hashCode() {
	        return brandId != null ? brandId.hashCode() : 0;
	    }
	

}
