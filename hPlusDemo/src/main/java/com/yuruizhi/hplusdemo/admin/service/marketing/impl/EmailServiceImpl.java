/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;


import com.yuruizhi.hplusdemo.admin.dao.marketing.EmailDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.EmailLogDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.EmailMemberDao;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Email;
import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailLog;
import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailMember;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.EmailService;
import com.yuruizhi.hplusdemo.admin.util.MailSender;
import com.yuruizhi.hplusdemo.admin.util.ScheduleUtils;
import com.google.common.collect.Lists;

/**
 * <p>名称: EmailServiceImpl</p>
 * <p>说明: 邮件服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:19:11   
 * @version: 1.0
 */
@Service
public class EmailServiceImpl extends BaseServiceImpl<Email> implements EmailService {
	
	@Autowired
    @Qualifier("scheduler")
    private Scheduler scheduler;
	@Autowired
	private EmailDao emailDao;
	
	@Autowired
	private EmailMemberDao emailMemberDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private EmailLogDao emailLogDao;
	
	@Autowired
	private MailSender mailSender;
	
	@Override
	public void emailSave(Email email) {
		
		if(email.getId()==null){//保存
			String memberIds = email.getMemberIds();
			email.setStatus(1);
			email.setCreatedDate(new Date());
			email.setLastModifiedDate(new Date());
			emailDao.save(email);
			if(email.getMemberIds() != null&&email.getMemberIds() != ""&&email.getScheduleTime()!=null){
				//创建新的定时任务
				ScheduleUtils.createScheduleJob(scheduler, email.getId(), "email"+email.getId(), email.getScheduleTime());
				
			}
			if(null != memberIds&&memberIds != ""){
				String[] ids = memberIds.split(",");
				for (int i = 0; i < ids.length; i++) {
					EmailMember emailMember = new EmailMember();
					emailMember.setEmailId(email.getId());
					emailMember.setMemberId(Long.valueOf(ids[i]));
					emailMember.setCreatedDate(new Date());
					emailMember.setLastModifiedDate(new Date());
					emailMemberDao.save(emailMember);
				}
			}
		}else{//更新
			String memberIds = email.getMemberIds();
			Email oldemail = emailDao.find(email.getId());
			oldemail.setSubject(email.getSubject());
			oldemail.setContent(email.getContent());
			oldemail.setScheduleTime(email.getScheduleTime());
			oldemail.setLastModifiedDate(new Date());
			oldemail.setBatchSize(email.getBatchSize());
			emailDao.update(oldemail);
			if(email.getMemberIds() != null&&email.getMemberIds() != ""&&email.getScheduleTime()!=null){
				//更新定时发送任务
				ScheduleUtils.updateScheduleJob(scheduler, email.getId(), "email"+email.getId(), email.getScheduleTime());
			}
			emailMemberDao.deleteByEmailId(email.getId());
			if(null != memberIds&&memberIds != ""){
				String[] ids = memberIds.split(",");
				for (int i = 0; i < ids.length; i++) {
					EmailMember emailMember = new EmailMember();
					emailMember.setEmailId(email.getId());
					emailMember.setMemberId(Long.valueOf(ids[i]));
					emailMember.setCreatedDate(new Date());
					emailMember.setLastModifiedDate(new Date());
					emailMemberDao.save(emailMember);
				}
			}
		}
		
		
		
		
	}

	@Override
	public void emailSend(Long[] ids) {
		for (Long id : ids) {
			Email email = emailDao.find(id);
			List<Member> members = memberDao.getMembersByEmailId(id);
			List<String> emails = Lists.newArrayList();
			List<String> contents = Lists.newArrayList();
			for (Member member : members) {
				if(null==member.getEmail()||member.getEmail().trim().equals("")){
					continue;
				}
				String content = email.getContent();
				content = content.replace("${memberName}", member.getNickName());
				content = content.replace("${sendTime}",new Date()+"");
				content = content.replace("${websitName}","艾漫");
				emails.add(member.getEmail());
				contents.add(content);
			}
			String emailStr = "";
			String emailNos[] = new String[emails.size()];
			String newContents[] = new String[emails.size()];
			
			for (int i = 0; i < emails.size(); i++) {
				emailNos[i] = emails.get(i);
				newContents[i] = contents.get(i);
				if(i==emails.size()-1){
					emailStr +=emailNos[i];
				}else{
					emailStr = emailNos[i]+",";
				}
				
			}
			@SuppressWarnings("unused")
			boolean flag = false;
			try {
				mailSender.sendEmails(email.getSubject(), newContents, emailNos);
			} catch (Exception e) {
				e.printStackTrace();
				flag = false;
			}finally {
				EmailLog emailLog = new EmailLog();
				emailLog.setEmail(email);
				emailLog.setContent(email.getContent());
				emailLog.setReceiver(emailStr);
				//emailLog.setSendCount(emails.size());
				emailLog.setSendTime(new Date());
				emailLog.setCreatedDate(new Date());
				emailLog.setLastModifiedDate(new Date());
				emailLog.setSubject(email.getSubject());
				emailLog.setStatus(2);
				emailLogDao.save(emailLog);
				email.setStatus(2);
				emailDao.update(email);
			}
			
			
		}
		
		
		
		
		
	}
}
