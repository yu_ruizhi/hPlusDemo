/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.order;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.order.ProductReview;

/**
 * Created on 2016-03-03 11:37:24
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface ProductReviewDao extends BaseDao<ProductReview> {

	/** 
	 * @Title: findDetailById 
	 * @Description: TODO
	 * @param id
	 * @return
	 * @return: ProductReview
	 */
	ProductReview findDetailById(Long id);
}