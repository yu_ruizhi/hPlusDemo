/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.member.impl;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.marketing.NewOrHotDao;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberDao;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.util.CipherUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.MailSender;
import com.yuruizhi.hplusdemo.admin.util.PassWordSaltUtil;
import com.yuruizhi.hplusdemo.admin.util.sms.SmsSender;
import com.google.common.collect.Maps;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * <p>名称: MemberServiceImpl</p>
 * <p>说明: 会员服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:23:51   
 * @version: 1.0
 */
@Service
public class MemberServiceImpl extends BaseServiceImpl<Member> implements
		MemberService {
	@Autowired
	public MailSender mailSender;
	@Autowired
	public SmsSender smsSender;
	@Autowired
	private MemberDao memberDao;
    @Autowired
    private PassWordSaltUtil passWordSaltUtil;

	@Override
	public Map<String, Object> login(String mobileOrEmail, String password) {
		Map<String, Object> result = new HashMap<String, Object>();

		if (StringUtils.isEmpty(mobileOrEmail) || StringUtils.isEmpty(password)) {
			return null;
		}
		int index = mobileOrEmail.indexOf('@');
		Member member = new Member();
		if (-1 < index) {
			// email
			member.setEmail(mobileOrEmail);
			member = memberDao.getUserByUserEmail(member);
		} else {
			// mobile
			member = memberDao.getUserByPhone(mobileOrEmail);
		}

		if (member == null) {
			result.put("result", "failure");
			result.put("errorMsg", "账号未注册");
			return result;
		}

		if (!member.getIsActive()) {
			result.put("result", "failure");
			result.put("errorMsg", "账号未激活，请激活账号");
			return result;
		}

		// match password
		if (!CipherUtil.validatePassword(member.getPwd(), password)) {
			result.put("result", "failure");
			result.put("errorMsg", "密码错误");
			return result;
		}

		result.put("result", "success");
		result.put("member", member);
		return result;
	}

	@Override
	public Map<String, Object> signUpWithVerifyCode(Member member) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Boolean> existResult = validateEmailExists(member);
		if (existResult.get("isExists")) {
			result.put("result", false);
			result.put("reasonCode", "RC_2");
			result.put("reason", "该邮箱已经注册");
		} else {
			member.setIsActive(Constant.NOT_ACTIVE);
			member.setUserType(Constant.USER_TYPE_NORMAL);
			insert(member);
			result.put("result", true);
			result.put("member", member);
		}
		return result;
	}

	/**
	 * 新增
	 *
	 * @see com.glanway.MemberService.service.UserService#insert(com.glanway.Member.entity.User)
	 */
	@Override
	public int insert(Member members) {
     Date date = new Date();
     members.setCode(memberCode(date));
     members.setRegister(date); // 注册时间
     members.setIsDeleted(Constant.NOT_DELETED); // 未删除
     members.setCreatedDate(date);
   /*  String pwd = passWordSaltUtil.passWordSalt(members.getPwd(), members.getCreatedDate()); */// 加密 初始密码
    String pwd=CipherUtil.generatePassword(members.getPwd());
     members.setPwd(pwd);
     members.setLastModifiedDate(date); // 注册日期
     return memberDao.insert(members);
     }

    /**
     * 生成会员编码
     * @param date
     * @return
     */
    public String memberCode(Date date){
        Member member = new Member();
        String memberCode = "";
        int x = 0;
        do{
            Random r = new Random();
            x = r.nextInt(9000)+1000;//为变量赋随机值1000-9999
            memberCode = DateUtils.date2Str(date, DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
            memberCode = memberCode+String.valueOf(x);
            member = findByCode(memberCode);
        } while(null != member);
        return memberCode+String.valueOf(x);
    }

    /**
     * 根据会员编号查询会员
     */
    @Override
    public Member findByCode(String code) {
        return memberDao.findByCode(code);
    }

    @Override
    public void updateMember(Member member) {
        memberDao.updateMember(member);
    }

    /**
     * 更新
	 */
	@Override
	public void update(Member members){
		Member oldMember = memberDao.find(members.getId());
		if (("").equals(members.getPwd())) {
			members.setPwd(oldMember.getPwd());
		} else {
			/*String pwd =  passWordSaltUtil.passWordSalt(members.getPwd(), oldMember.getCreatedDate());*/
            String pwd=CipherUtil.generatePassword(members.getPwd());
			members.setPwd(pwd);
		}
		memberDao.update(members);
	}

	@Override
	public Map<String, Object> signUpByPhone(Member member) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Boolean> phoneExists = validatePhoneExists(member);
		if (phoneExists.get("isExists")) {
			result.put("result", false);
			result.put("reasonCode", "RC_2");
			result.put("reason", "该手机已注册");
		} else {
			member.setIsActive(Constant.ACTIVE);
			member.setIsDeleted(Constant.NOT_DELETED);
			member.setUserType(Constant.USER_TYPE_NORMAL);
			insert(member);
			result.put("result", true);
			result.put("member", member);
		}
		return result;
	}



	/**
	 * 验证手机号是否已存在
	 *
	 * @param member
	 * @return
	 */
	@Override
	public Map<String, Boolean> validatePhoneExists(Member member) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		Member cUser = memberDao.getUserByPhone(member.getPhone());
		if (cUser == null || cUser.getId().equals(member.getId())) {
			result.put("isExists", false);
		} else {
			result.put("isExists", true);
		}
		return result;
	}

	/**
	 * 验证邮箱是否已存在
	 *
	 * @param member
	 *            用户
	 * @return
	 */
	@Override
	public Map<String, Boolean> validateEmailExists(Member member) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		Member cUser = memberDao.getUserByUserEmail(member);
		if (cUser == null || cUser.getId().equals(member.getId())) {
			result.put("isExists", false);
		} else {
			result.put("isExists", true);
		}
		return result;
	}

	@Override
	public Member getUserByPhone(String phone) {
		return memberDao.getUserByPhone(phone);
	}


	/**
	 * 获取用户信息
	 *
	 * @see
	 */
	@Override
	public Member getUserByUserEmail(Member members) {
		return memberDao.getUserByUserEmail(members);
	}

	/**
	 * 忘记密码
	 *
	 * @see
	 */
	@Override
	public Member forgetPwd(String email) {
		return memberDao.forgetPwd(email);
	}

	/**
	 * 查询个人信息
	 */
	@Override
	public Member selectById(Member member) {
		return memberDao.selectById(member);
	}


	@Override
	public List<Member> findMemberList(Long[] ids) {
		// TODO Auto-generated method stub
		return memberDao.findMemberList(ids);
	}

	@Override
	public List<Member> getMembersBySmsId(Long smsId) {
		return memberDao.getMembersBySmsId(smsId);
	}

	@Override
	public Page<Member> findMemberPage(Filters filters, Pageable pageable, Long[] memberIds) {
		int count = count(filters,memberIds);
        List<Member> data = count > 0 ? findMany(filters, pageable,memberIds) : Collections.<Member>emptyList();
        return new SimplePage<Member>(pageable, data, count);
	}
	public int count(Filters filters,Long[] memberIds) {
        Map<String, Object> paramsMap = createParamsMap();
        paramsMap = resolveFiltersToParamsMap(filters, paramsMap);
        paramsMap.put("memberIds", memberIds);
        return count(paramsMap);
    }
	
	public List<Member> findMany(Filters filters, Pageable pageable,Long[] memberIds) {
        Map<String, Object> paramsMap = createParamsMap();
        paramsMap = resolveFiltersToParamsMap(filters, paramsMap);
        paramsMap.put("memberIds", memberIds);
        paramsMap = resolvePageableToParamsMap(pageable, paramsMap);

        return findMany(paramsMap);
    }

	@Override
	public List<Member> getMembersByEmailId(Long emailId) {
		// TODO Auto-generated method stub
		return memberDao.getMembersByEmailId(emailId);
	}

	@Override
	public Page<Member> findPage(Filters filters, Filters memberLevelFilters,Filters memberUserFilters, Pageable pageable) {
		Map<String, Object> paramsMap = createParamsMap();
        if (null != filters) {
            paramsMap.put(MemberDao.MEMBER_FILTERS_PROP, filters);
        }
        if (null != memberLevelFilters) {
            paramsMap.put(MemberDao.MEMBER_LEVEL_FILTERS_PROP, memberLevelFilters);
        }
        if (null != memberUserFilters) {
            paramsMap.put(MemberDao.MEMBER_USER_FILTERS_PROP, memberUserFilters);
        }
		if (null != pageable) {
			paramsMap.put(MemberDao.OFFSET_PROP, pageable.getOffset());

			paramsMap.put(MemberDao.MAX_RESULTS_PROP, pageable.getPageSize());

			Sort sort = pageable.getSort();
			if (null != sort) {
				paramsMap.put(NewOrHotDao.SORT_PROP, sort);
			}
		}

        int total = count(paramsMap);
        List<Member> data = total > 0 ? findMany(paramsMap) : Collections.<Member>emptyList();

        return new SimplePage<Member>(pageable, data, total);
	}
	
	/**
	 * 新增检测重名
	 */
	@Override
	public Map<String, Boolean> checkIsNameExists(String name) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<Member> memberList = memberDao.findByName(name);
		if (memberList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

	/**
	 * 更新检测重名
	 */
	@Override
	public Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<Member> memberList = memberDao.findByName(hasAnnotherName[1]);
		Iterator<Member> it = memberList.iterator();
		Member member = it.hasNext() ? it.next() : null;
		result.put("isExists", null != member && !member.getId().equals(Long.valueOf(hasAnnotherName[0])));
		return result;
	}
	
	/**
	 * 新增检测邮箱
	 */
	@Override
	public Map<String, Boolean> checkIsEmailExists(String email) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<Member> memberList = memberDao.findByEmail(email);
		if (memberList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

	/**
	 * 更新检测邮箱
	 */
	@Override
	public Map<String, Boolean> hasAnnotherEmail(String[] hasAnnotherEmail) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<Member> memberList = memberDao.findByEmail(hasAnnotherEmail[1]);
		Iterator<Member> it = memberList.iterator();
		Member member = it.hasNext() ? it.next() : null;
		result.put("isExists", null != member && !member.getId().equals(Long.valueOf(hasAnnotherEmail[0])));
		return result;
	}
	
	/**
	 * 新增检测手机号
	 */
	@Override
	public Map<String, Boolean> checkIsPhoneExists(String phone) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<Member> memberList = memberDao.findByPhone(phone);
		if (memberList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

	/**
	 * 更新检测手机号
	 */
	@Override
	public Map<String, Boolean> hasAnnotherPhone(String[] hasAnnotherPhone) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<Member> memberList = memberDao.findByPhone(hasAnnotherPhone[1]);
		Iterator<Member> it = memberList.iterator();
		Member member = it.hasNext() ? it.next() : null;
		result.put("isExists", null != member && !member.getId().equals(Long.valueOf(hasAnnotherPhone[0])));
		return result;
	}

	/**
	 * <p>名称：查询所有的前台用户</p>
	 * <p>描述：</p>
	 * @author：yuruizhi
	 * @return: List<Member>
	 */
	@Override
	public List<Member> findAll(){ return memberDao.findAll();}

}
