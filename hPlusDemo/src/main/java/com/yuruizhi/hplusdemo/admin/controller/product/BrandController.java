/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.product;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ponly.common.util.FileUtils;
import org.ponly.webbase.domain.Filters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Brand;
import com.yuruizhi.hplusdemo.admin.service.product.BrandService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.MathUtil;
import com.yuruizhi.hplusdemo.admin.util.ZipUtil;
import com.yuruizhi.hplusdemo.admin.validate.BrandValidate;
import com.google.common.collect.Maps;

/**
 * 商品品牌维护控制器
 * <p/>
 * Created on 2016-03-10 16:55:37
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/brand")
public class BrandController extends AdminBaseController<Brand> {
    @Autowired
    private BrandService brandService;
    
    
    /**
     * 校验名称是否有效
     */
    @ResponseBody
    @RequestMapping("check")
    public Boolean check(Long id, String name) {
        Filters filters = Filters.create();

        if (null != id) {
            filters.ne("id", id);
        }
        if (StringUtils.hasText(name)) {
            filters.eq("name", name);
        }
        return brandService.count(filters) < 1;
    }

    /**
     * 复写预处理模型方法，设置默认值
     *
     * @param id ID
     */
    @Override
    protected Brand prepareModel(@RequestParam(value = "id", required = false) Long id) {
        Brand brand = super.prepareModel(id);
        if (null == brand.getId()) {
            brand.setEnabled(true);
        }
        return brand;
    }
    
    /**
     * 
     * <p>名称：下载品牌导入模版</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param request
     * @param response
     */
    @RequestMapping("downLoadModel")
    public void downLoadModel(HttpServletRequest request,HttpServletResponse response) {
    	String[] headers = {"名称","别名","网址","是否启用","是否推荐","排序","介绍"};
    	List<String> dataList = new ArrayList<>();
    	String data = "有妖气,有妖气网,http://www.u17.com,是,是,1,有妖气动画";
    	dataList.add(data);
    	String fileName = "brand" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
    	ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }
    
    /**
     * 
     * <p>名称：批量导入</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param multipart
     * @param request
     * @return
     * @throws IOException
     */
	@RequestMapping(value = "/importData",  method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> imporyData(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
		Map<String,Object> map = Maps.newHashMap();
		InputStream inputStream = multipart.getInputStream();
		List<String> list = ExcelUtil.importExcel(inputStream, 1, 7);

		String error = "";
		if (null != list && 0 < list.size()) {
			for (int i=0;i<list.size();i++) {
				Boolean success = true;
				if (null != list.get(i)) {
					Map<String,Object> brandMap = Maps.newHashMap();
					//将值进行验证
					brandMap = BrandValidate.brandValidate(list.get(i), error, i, success);
					Brand brand = (Brand) brandMap.get("brand");
					success = (Boolean) brandMap.get("success");
					error = (String)brandMap.get("error");
					if (success) {
						List<Brand> brands = brandService.findByName(brand.getName());
						if (null != brands && 0 < brands.size()) {
							brand.setId(brands.get(0).getId());
							brandService.update(brand);
						} else {
							brandService.save(brand);
						}
					}
				}
				
			}
		}
		map.put("error", error);
		return map;
	}
	
	/**
	 * <p>名称：批量更新品牌图片</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @return
	 */
    @RequestMapping("updateImage")
    public String updateImage(){
    	
        return "brand/updateImage";
    }
    
    /**
     * <p>名称：处理上传的zip图片压缩包并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param multipart
     * @param request
     * @throws IOException
     */
    @RequestMapping(value = "/uploadImage",  method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> uploadImage(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	
    	InputStream inputStream = multipart.getInputStream();
    	
    	String savePath = request.getSession().getServletContext().getRealPath("/WEB-INF/import/iamgeZip");
    	
    	String zipFilePath = ZipUtil.copyZipFileStreamToServer(inputStream, savePath);
    	//随机解压文件名
    	String unzipFolderName = DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
    	List<String> imagePathList = ZipUtil.handleZip(zipFilePath, savePath+"/"+unzipFolderName);
    	
    	List<String> resultList = new ArrayList<String>();
    	Map<String,Object> resultMap = Maps.newHashMap();
    	
    	if(null != imagePathList && imagePathList.size() > 0){
    	  String filePath = null;
    	  String brandIdStr = null;
    	  Brand brand = null;
    	  for(String path : imagePathList){
    		  brandIdStr = FileUtils.getNameWithoutExtension(path);
    		  if(MathUtil.isLong(brandIdStr)){
    			  filePath = CSVUtil.CopyFileToServer(path);
    			  brand = brandService.find(Long.valueOf(brandIdStr));
    			  if(null != brand){
    				  brand.setLogo(filePath);
    				  brandService.update(brand);
    			  }else{
    				  resultList.add(FileUtils.getName(path) + "对应的品牌找不到,请检查此品牌ID是否存在");
    			  }
    		  }else{
    			  resultList.add(FileUtils.getName(path) + "命名格式不正确(非数字),请检查");
    		  }
    		 
    	  }
    	  resultMap.put("resultList", resultList);
    	}
    	
    	return resultMap;
    }
}
