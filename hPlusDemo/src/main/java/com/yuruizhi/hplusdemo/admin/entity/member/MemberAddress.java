/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.member;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.area.Area;
import com.yuruizhi.hplusdemo.admin.entity.area.City;
import com.yuruizhi.hplusdemo.admin.entity.area.Province;

/**
 * <p>名称: MemberAddress</p>
 * <p>说明: 会员地址实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:09:30   
 * @version: 1.0
 */
public class MemberAddress extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * memberid:用户
     */
    private Long memberId;

    /**
     * memberid:用户
     */
    private Member member;
    
    /**
     * country:国家
     */
    private String countryId;
    
    /**
     * 省、州编码 
     */
    private String provinceId;
    
    /*城市id*/
    private String cityId;
    /**
     * 县/区编码
     */
    private String areaId;

    
    /**
     * firstName:姓名
     */
    private String firstName;

    /**
     * lastName:姓氏
     */
    private String lastName;

    /**
     * addressLine:地址
     */
    private String addressLine;

    /**
     * phone1:联系电话1
     */
    private String phone1;

    /**
     * phone2:联系电话2
     */
    private String phone2;

    

    /**
     * city:城市
     */
    private City city = new City();

    /**
     * :州/省
     */
    private Province province = new Province();

    /**
     * 县、区
     */
    private Area area = new Area();

    /**
     * zip:邮编
     */
    private String zip;

    /**
     * defaulted:默认收货地址
     */
    private Boolean defaulted;

    /**
     * email:邮箱
     */
    private String email;
    
    /**
     * createdBy:建单人
     */
    private String createdBy;

    /**
     * createdDate:建单时间
     */
    private Date createdDate;

    /**
     * lastModifiedBy:修改人
     */
    private String lastModifiedBy;

    /**
     * lastModifiedDate:修改时间
     */
    private Date lastModifiedDate;

    /*自己添加属性*/
    private String[] ids;

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public String[] getNums() {
        return nums;
    }

    public void setNums(String[] nums) {
        this.nums = nums;
    }

    private String[] nums;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * isDeleted:是否删除CITY
     */
    private Boolean deleted;

    /*新增地址后，判断是否有库存*/
    private Boolean isKc = true;

    public Boolean getIsKc() {
        return isKc;
    }

    public void setIsKc(Boolean isKc) {
        this.isKc = isKc;
    }

    public int getFrequence() {
        return frequence;
    }

    public void setFrequence(int frequence) {
        this.frequence = frequence;
    }

    /**
     * 使用频率
     */
    private int frequence;


    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getPhone1() {
        return phone1;
    }


    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }


    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Boolean getDefaulted() {
        return defaulted;
    }

    public void setDefaulted(Boolean defaulted) {
        this.defaulted = defaulted;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        } else {
            if (this.getClass() == obj.getClass()) {
                MemberAddress memberAddress = (MemberAddress) obj;
                if (this.getId().equals(memberAddress.getId())) {
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        }
    }

    @Override
    public String toString() {
        return "MemberAddress{" +
                "id='" + id + '\'' +
                ", memberId='" + memberId + '\'' +
                ", member=" + member +
                ", cityId='" + cityId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", addressLine='" + addressLine + '\'' +
                ", phone1='" + phone1 + '\'' +
                ", phone2='" + phone2 + '\'' +
                ", country='" + countryId + '\'' +
                ", city=" + city +
                ", province=" + provinceId +
                ", area=" + areaId +
                ", zip='" + zip + '\'' +
                ", defaulted=" + defaulted +
                ", email='" + email + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                ", ids=" + Arrays.toString(ids) +
                ", nums=" + Arrays.toString(nums) +
                ", deleted=" + deleted +
                ", isKc=" + isKc +
                ", frequence=" + frequence +
                '}';
    }

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
}
