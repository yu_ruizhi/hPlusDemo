/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;

import java.util.*;

/**
 * 商品组合(套餐,配件)
 * 
 * @author Administrator
 * @Table T_COMBINE
 */
public class Combine extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -4495542477080785678L;
	public static final int COMBO = 1; //套餐
	public static final int ACCESSORY = 2;//配件
	/**
     * 组合类型
     * combo:套餐, accessory: 配件
     * 
     * @ViewField editor=spinner 
     * @Column TYPE
     */
    @NotNull
    private Integer type;

    /**
     * 组合名称
     * 
     * @ViewField editor=input 
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 组合主商品
     * 
     * @ViewField editor=spinner 
     * @Column PRIMARY_GOODS_ID
     */
    @NotNull
    private Long primaryGoodsId;

    private Goods goods;
    

	/**
     * 组合主产品
     * 
     * @ViewField editor=spinner 
     * @Column PRIMARY_PRODUCT_ID
     */
    @NotNull
    private Long primaryProductId;

    /**
     * 组合价格
     * 只有当类型为套餐时才填写此字段
     * 
     * @ViewField editor=input 
     * @Column PRICE
     */
    @NotNull
    @Size(max = 8)
    @Digits(integer = 8, fraction = 2)
    private Double price;

    /**
     * 组合描述
     * 
     * @ViewField editor=input 
     * @Column NOTE
     */
    @Size(max = 500)
    private String note;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    private ArrayList<CombineDetail> combineDetails;
    
    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    /**
	 * @return the primaryGoodsId
	 */
	public Long getPrimaryGoodsId() {
		return primaryGoodsId;
	}

	/**
	 * @param primaryGoodsId the primaryGoodsId to set
	 */
	public void setPrimaryGoodsId(Long primaryGoodsId) {
		this.primaryGoodsId = primaryGoodsId;
	}

	/**
	 * @return the primaryProductId
	 */
	public Long getPrimaryProductId() {
		return primaryProductId;
	}

	/**
	 * @param primaryProductId the primaryProductId to set
	 */
	public void setPrimaryProductId(Long primaryProductId) {
		this.primaryProductId = primaryProductId;
	}

	public ArrayList<CombineDetail> getCombineDetails() {
		return combineDetails;
	}

	public void setCombineDetails(ArrayList<CombineDetail> combineDetails) {
		this.combineDetails = combineDetails;
	}

	public Goods getGoods() {
		return goods;
	}

	public void setGoods(Goods goods) {
		this.goods = goods;
	}
}
