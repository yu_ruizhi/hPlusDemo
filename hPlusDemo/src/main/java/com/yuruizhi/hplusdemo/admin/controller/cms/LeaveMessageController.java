package com.yuruizhi.hplusdemo.admin.controller.cms;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.LeaveMessage;
import com.yuruizhi.hplusdemo.admin.service.cms.LeaveMessageService;
import com.google.common.collect.ImmutableMap;

/**
 * 留言
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/leavemessage")
public class LeaveMessageController extends BaseController {
	@Autowired
	private LeaveMessageService leaveMessageService;

	@RequestMapping("index")
	public void index() {}

	@RequestMapping("list")
	@ResponseBody
	public Page<LeaveMessage> list(Filters filters, Pageable pageable) {
		Page<LeaveMessage> plist = leaveMessageService.findPage(filters, pageable);
		for (LeaveMessage lm : plist.getData()) {
			if (lm.getContent().length() > 10) {
				lm.setContent(lm.getContent().substring(0, 10) + "... ...");
			}
		}
		return plist;
	}

	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		LeaveMessage leaveMessage = leaveMessageService.find(id);
		mav.addObject("leaveMessage", leaveMessage);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		leaveMessageService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}
}
