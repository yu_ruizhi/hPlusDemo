/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.perm.UserDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.ArrayUtil;
import com.yuruizhi.hplusdemo.admin.util.CipherUtil;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>名称: AdminUserServiceImpl</p>
 * <p>说明: 后台用户管理服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:39:00   
 * @version: 1.0
 */
@Service
public class AdminUserServiceImpl extends BaseServiceImpl<User> implements UserService {

    private UserDao userDao;

    @Autowired
    public void setAdminUserDao(UserDao userDao) {
        this.userDao = userDao;
        setBaseDao(userDao);
    }

    @Override
    
    public void saveAdminUser(User user, String roleIds) {
        user.setDeleted(Constant.NOT_DELETED);
//        Date curDate = new Date();
//        System.out.println(curDate.getTime());
//        System.out.println(CipherUtil.generatePassword(user.getPassword()+curDate.getTime()));
//        user.setCreatedDate(curDate);
        
//        user.setPassword(CipherUtil.generatePassword(user.getPassword()+curDate.getTime()));//密码加盐处理
        save(user);
        User newUser = userDao.find(user.getId());
        //System.out.println(user.getPassword()+newUser.getCreatedDate().getTime());
        newUser.setPassword(CipherUtil.generatePassword(user.getPassword()+newUser.getCreatedDate().getTime()));
        update(newUser);
        Long userId = user.getId();
        saveUserToRole(userId, roleIds);
    }

    @Override
    
    public void updateAdminUser(User user, String roleIds) {
        if (StringUtils.isEmpty(user)) {
            return;
        }

        @SuppressWarnings("unused")
		User adminUser1 = find(user.getId());
        //System.out.println(CipherUtil.generatePassword(user.getPassword()+adminUser1.getCreatedDate().getTime()));
        //System.out.println(user.getPassword());
        //System.out.println(adminUser1.getPassword());
        /*if (!user.getPassword().equals(adminUser1.getPassword())) {
            user.setPassword(CipherUtil.generatePassword(user.getPassword()+adminUser1.getCreatedDate().getTime()));//密码加盐处理
        }*/
        user.setLastModifiedBy(UserUtils.getCurrentUserId() + "");
        update(user);

        if (!StringUtils.hasText(roleIds)) {
            return;
        }
        Long userId = user.getId();
        userDao.deleteUserToRoleByUserId(userId);
        saveUserToRole(userId, roleIds);

    }

    @Override
    
    public boolean deleteAdminUser(Long id) {
        if (null == id) {
            return false;
        }

        User user = new User();
        user.setId(id);
        delete(user);
        userDao.deleteUserToRoleByUserId(id);
        return true;
    }

    @Override
    
    public boolean deleteAminUserByIds(Long[] ids) {
        if (StringUtils.isEmpty(ids)) {
            return false;
        }

        for (Long id : ids) {
            deleteAdminUser(id);
        }

        return true;

    }

    @Override
    public void saveLastLoginTime(User user) {
        if (StringUtils.isEmpty(user)) {
            return;
        }
        userDao.update(user);
    }

    @Override
    public Map<String, Object> changePassword(String origPassword, String newPassword) {
        Map<String, Object> result = new HashMap<String, Object>();

        if (!StringUtils.hasText(origPassword) || !StringUtils.hasText(newPassword)) {
            result.put("result", false);
            result.put("errorMsg", "密码为空");
            return result;
        }
        Long currentUserId = UserUtils.getCurrentUserId();

        if (null == currentUserId) {
            result.put("result", false);
            result.put("errorMsg", "未登录");
        }

        User currentUser = find(currentUserId);
        if (!CipherUtil.validatePassword(currentUser.getPassword(), origPassword)) {
            result.put("result", false);
            result.put("errorMsg", "原始密码错误");
            return result;
        }

        currentUser.setPassword(CipherUtil.generatePassword(newPassword));
        currentUser.setLastModifiedBy(currentUserId + "");
        update(currentUser);

        result.put("result", true);
        return result;
    }

    private void saveUserToRole(Long userId, String roles) {
        if (!StringUtils.hasText(roles)) {
            return;
        }
        String[] roleIds = ArrayUtil.stringToArray(roles);
        
        for (String roleId : roleIds) {
        	Map<String, Object> userToRole = new HashMap<String, Object>();
            userToRole.put("userId", userId);
            userToRole.put("roleId", roleId);
            userToRole.put("deleted", Constant.NOT_DELETED);
            userToRole.put("createdDate", new Date());
            userToRole.put("lastModifiedDate", new Date());
            userDao.saveUserToRole(userToRole);
        }
    }

    @Override
    public List<User> getBaseUser(Map<String, Object> paramMap) {
        return userDao.getBaseUser(paramMap);
    }

    @Override
    public Map<String, Boolean> checkIsUserExists(String name) {
        Map<String, Boolean> result = new HashMap<String, Boolean>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("loginName", name);
        List<User> users = getBaseUser(paramMap);
        if (users.size() > 0) {
            result.put("isExists", true);
        } else {
            result.put("isExists", false);
        }
        return result;
    }

	@Override
	public List<User> findByRoleName(String name) {
		return userDao.findByRoleName(name);
	}
}
