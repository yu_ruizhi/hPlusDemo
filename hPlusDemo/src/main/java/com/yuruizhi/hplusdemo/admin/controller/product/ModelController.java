package com.yuruizhi.hplusdemo.admin.controller.product;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yuruizhi.hplusdemo.admin.service.product.*;
import org.ponly.common.util.StringUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;
import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Model;
import com.yuruizhi.hplusdemo.admin.entity.product.ModelSpec;
import com.yuruizhi.hplusdemo.admin.entity.product.Parameter;
import com.yuruizhi.hplusdemo.admin.entity.product.Spec;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.validate.ModelValidate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * 商品模型 Controller. <br/>
 * date: 2014年7月29日 上午11:28:05 <br/>
 *
 * @author vacoor
 * @change Log: ************************************************
 * @since JDK 1.7
 *
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/model")
public class ModelController extends BaseController {
	@Autowired
	private ModelService modelService;
	@Autowired
	private AttributeService attributeService;
	@Autowired
	private AttributeValueService attributeValueService;
	@Autowired
	private SysLogService sysLogService;
	@Autowired
	private SpecService specService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProductService productService;
	@RequestMapping("index")
	public void index() {
	}

	@ResponseBody
	@RequestMapping("models")
	public List<Model> models() {
		return modelService.findAll();
	}

	/**
	 * 校验名称是否有效
	 */
	@ResponseBody
	@RequestMapping("check")
	public Boolean check(Long id, String name) {
		Filters filters = Filters.create();

		if (null != id) {
			filters.ne("id", id);
		}
		if (StringUtils.hasText(name)) {
			filters.eq("name", name);
		}
		return modelService.count(filters) < 1;
	}

	@ResponseBody
	@RequestMapping("list")
	public Page<Model> list(Filters filters, Pageable pageable) {
		return modelService.findPage(filters, pageable);
	}

	@RequestMapping("add")
	public void add() {
	}

	@RequestMapping("save")
	public String save(Model model, RedirectAttributes redirectAttr) {
		cleanModel(model);
		modelService.save(model);
		redirectAttr.addFlashAttribute("message", "保存成功");
		//系统操作日志
		String operation = "模型管理-新增-id:"+model.getId();
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, operation, "模型管理");
		return redirectViewPath("index");
	}

	@RequestMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, Map<String, Object> model) {
		Model m = modelService.findDetail(id);
		if (m == null) {
			return null;
		}
		model.put("model", m);
		model.put("referenced", modelService.isReferenced(id));
		return getViewPath("edit");
	}

	@RequestMapping("update")
	public String update(Model model, RedirectAttributes redirectAttr) {
		cleanModel(model);
		modelService.update(model);
		redirectAttr.addFlashAttribute("message", "更新成功");
		//系统操作日志
		String operation = "模型管理-修改-id:"+model.getId();
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "模型管理");
		return redirectViewPath("index");
	}

	@RequestMapping("base")
	public String basicModel(Map<String, Object> model) {
		Model m = modelService.findBaseModel();
		model.put("m", m);
		return getViewPath("base");
	}

	@ResponseBody
	@RequestMapping("base/save")
	public Map<String, Object> saveBaseModel(Model model) {
		modelService.saveBaseModel(model);
		return ImmutableMap.<String, Object>of("success", true);
	}

	/*
	 * *****************************************************
	 * 其实傻了，只要当前模型被引用任何属性都不应该被删除, 下面方法有空删掉
	 ******************************************************/
	@ResponseBody
	@RequestMapping("delAttr")
	@Deprecated
	public Map<String, Object> delAttr(@RequestParam("id") Long id) {
		attributeService.delete(id);

		return ImmutableMap.<String, Object>of("success", true);
	}

	@ResponseBody
	@RequestMapping("delAttrVal")
	@Deprecated
	public Map<String, Object> delAttrVal(@RequestParam("id") Long id) {
		attributeValueService.delete(id);
		return ImmutableMap.<String, Object>of("success", true);
	}

	@ResponseBody
	@RequestMapping("delModelSpec")
	@Deprecated
	public Map<String, Object> delModelSpec(@RequestParam("id") Long id) {
		//20160429 wentan 处理当规格被使用时不能删除但是前台没有提示的问题
		try {
			modelService.deleteModelSpec(id);			
		} catch (Exception e) {
			return ImmutableMap.<String, Object>of("success", false);
		}
		return ImmutableMap.<String, Object>of("success", true);
	}

	@ResponseBody
	@RequestMapping("delete")
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		modelService.delete(id);
		String ids = "";
		for (int i = 0; i < id.length; i++) {
			ids += id[i]+",";
		}
		//系统日志
		ids = ids.substring(0, ids.length()-1);
		String operation = "会员管理-删除-id:"+ids;
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "会员管理");
		return ImmutableMap.<String, Object>of("success", true);
	}

	@ResponseBody
	@RequestMapping("detail/{id}")
	public Map<String, Object> detail(@PathVariable("id") Long id) {
		Model data = modelService.findDetail(id);
		if(data.getParameters()!=null&&data.getParameters().size()>0){
			List<Parameter> parameters=new ArrayList<Parameter>();
            for(Parameter parameter:data.getParameters()){
				if(null!=parameter.getChildren()&&parameter.getChildren().size()>0){
					parameters.add(parameter);
				}
			}
			data.setParameters(parameters);
		}
		return ImmutableMap.<String, Object>of("success", true, "data", data);
	}

	/**
	 * clean empty attributes, parameters and specifications for model transfer
	 * data object
	 */
	private void cleanModel(Model model) {
		if (model == null) {
			return;
		}

		// clean empty attributes
		List<Attribute> attrs = Lists.newArrayList();
		if (model.getUseAttribute() && (attrs = model.getAttributes()) != null) {
			Iterator<Attribute> it = attrs.iterator();
			while (it.hasNext()) {
				Attribute attr = it.next();
				if (attr == null || !StringUtils.hasLength(attr.getName())) {
					it.remove();
				}
				List<AttributeValue> values = attr.getAttributeValues();
				if (null == values) {
					continue;
				}
				Iterator<AttributeValue> vit = values.iterator();
				if (vit == null) {
					continue;
				}
				while (vit.hasNext()) {
					AttributeValue value = vit.next();
					if (value == null || !StringUtils.hasLength(value.getValue())) {
						vit.remove();
					}
				}
			}
		} else {
			model.setAttributes(attrs);
		}

		// clean empty parameters
		List<Parameter> params = Lists.newArrayList();
		if (model.getUseParameter() && (params = model.getParameters()) != null) {
			// parameter group
			Iterator<Parameter> it = params.iterator();
			while (it.hasNext()) {
				Parameter g = it.next();
				if (g == null || !StringUtils.hasLength(g.getName())) {
					it.remove();
				}
				// parameter
				List<Parameter> children = g.getChildren();
				if (children != null) {
					Iterator<Parameter> cit = children.iterator();
					while (cit.hasNext()) {
						Parameter c = cit.next();
						if (c == null || !StringUtils.hasLength(c.getName())) {
							cit.remove();
						}
					}
				}
			}
		} else {
			model.setParameters(params);
		}

		// clean empty specifications
		List<ModelSpec> modelSpecs = Lists.newArrayList();
		if (model.getUseSpec() && (modelSpecs = model.getModelSpecs()) != null) {
			Iterator<ModelSpec> it = modelSpecs.iterator();
			while (it.hasNext()) {
				ModelSpec modelSpec = it.next();
				if (modelSpec == null || modelSpec.getSpec() == null) {
					it.remove();
				}
			}
		} else {
			model.setModelSpecs(modelSpecs);
		}

		// clean categories
		// cleanTransients(model.getCategories());
	}
	
	/**
	 * 
	 * <p>名称：下载模型模版</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param request
	 * @param response
	 */
	@RequestMapping("downLoadModel")
	public void downLoadModel(HttpServletRequest request,HttpServletResponse response) {
		String[] headers = {"模型名称","是否启用属性","属性名称","属性别名","展示类型","属性值","排序","是否显示","是否启用规格","规格名称",
							"排序","是否启用参数","参数组名称","参数名称"};
		List<String> dataList = new ArrayList<>();
		String data = "手办模型1,是,产地,产地,文本,,1,是,是,材质,1,是,精致度,一般";
		dataList.add(data);
		data = ",,高度,高度,区间,100mm-200mm|200mm-300mm|300mm-400mm,2,是,,颜色,2,是,,高级";
		dataList.add(data);
		data = ",,适合人群,适合人群,选择,老人|小孩|青年,3,是,,,,,,";
		dataList.add(data);
		String fileName = "model" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
		ExcelUtil.exportExcel(headers, dataList, response, fileName);
	}
	
	/**
	 * 
	 * <p>名称：导入商品模型</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param multipart
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping("importData")
	public Map<String,Object> importData (@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
		Map<String,Object> map = Maps.newHashMap();
		InputStream inputStream = multipart.getInputStream();
		List<String> list = ExcelUtil.importExcel(inputStream, 1, 15);
		//错误提示
		String error = "";
		if (null != list && 0 < list.size()) {
				Model model = new Model();
				//商品属性
				List<Attribute> attributeList = new ArrayList<>();
				//商品规格
				List<ModelSpec> modelSpecsList = new ArrayList<>();
				//商品参数
				List<Parameter> parametersList = new ArrayList<>();
				List<Parameter> childrens = new ArrayList<>();
				Parameter parameter = new Parameter();
				Boolean save = true;
				for (int i=0;i<list.size();i++) {
					String data[] = null;
					Attribute attribute = new Attribute();
					ModelSpec modelSpec = new ModelSpec();
					Boolean success = true;
					if (null != list.get(i)) {
						data = list.get(i).split(",");
						try {
							//模型名称
							if (null != data[0] && !"".equals(data[0])) {
								//根据名称查找是否有重名，有进行更新，没有则保存
								List<Model> modelList = modelService.findByName(data[0]);
								if (null != modelList && 0 < modelList.size()) {
									model = modelList.get(0);
									save = false;
								} else {
									if (250 < data[0].length()) {
										error += "第"+(i+1)+"行数据导入失败，原因为名称超过250个字符<br>";
										continue;
									}
									model.setName(data[0]);
									save = true;
								}
							}
							Map<String,Object> modelMap = ModelValidate.modelValidate(model, attribute, attributeList, data, error, i, success);
							model = (Model) modelMap.get("model");
							attribute = (Attribute) modelMap.get("attribute");
							error = (String) modelMap.get("error");
							success = (Boolean) modelMap.get("success");
							//规格名称
							if (null != data[9] && !"".equals(data[9])) {
								List<Spec> specList = specService.findByName(data[9]);
								if (null != specList && 0 < specList.size()) {
									modelSpec.setSpec(specList.get(0));
								} else {
									error += "第"+(i+1)+"行数据导入失败，原因为没有找到规格名称<br>";
									continue;
								}
							}
							
							//排序
							if (null != data[10] && !"".equals(data[10])) {
								try {
									modelSpec.setSort(Integer.valueOf(data[10]));
									modelSpec.setIsAffectGoods(true);
									modelSpecsList.add(modelSpec);
								} catch (Exception e) {
									error += "第"+(i+1)+"行数据导入失败，原因为排序类型错误<br>";
									continue;
								}
							}
							//是否启用参数
							if (null != data[11] && !"".equals(data[11])) {
								//当启用参数时，才对以下数据进行保存
								if ("是".equals(data[11])) {
									model.setUseParameter(true);
								} else {
									model.setUseParameter(false);
								}
							}
							//参数组名称
							if (null != data[12] && !"".equals(data[12])) {
								if (null != childrens && 0 < childrens.size()) {
									parameter.setChildren(childrens);
									parametersList.add(parameter);
									childrens = new ArrayList<>();
									parameter = new Parameter();
								}
								if (250 < data[12].length()) {
									error += "第"+(i+1)+"行数据导入失败，原因为参数组名称超过250个字符<br>";
									continue;
								}
								parameter.setName(data[12]);
							}
							//参数名称
							if (null != data[13] && !"".equals(data[13].trim())) {
								if (250 < data[13].length()) {
									error += "第"+(i+1)+"行数据导入失败，原因为参数名称超过250个字符<br>";
									continue;
								}
								Parameter children = new Parameter();
								children.setName(data[13]);
								childrens.add(children);
							}
						} catch (Exception e) {
						}
						
					}
				}
				parameter.setChildren(childrens);
				parametersList.add(parameter);
				model.setAttributes(attributeList);
				model.setModelSpecs(modelSpecsList);
				model.setParameters(parametersList);
				if (save) {
					modelService.save(model);
				} else {
					modelService.update(model);
				}
		}
		map.put("error", error);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：根据id获取信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getModel")
	public Map<String,Object> getModel(Long id) {
		Map<String,Object> map = Maps.newHashMap();
		Model model = modelService.findDetail(id);
		map.put("model", model);
		Integer count=productService.findProductCount(model.getId());
		if(null!=count&&count>0){
			map.put("error", "模型["+model.getName()+"]所在的分类已经关联商品，请慎重合并.");
		}
		return map;
	}
	
	/**
	 * 
	 * <p>名称：获取模型及合并模型</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param modelId
	 * @param mergeModelId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getAllModel")
	public Map<String,Object> getAllModel(Long modelId,Long mergeModelId){
		Map<String,Object> map = Maps.newHashMap();
		Model model = modelService.findDetail(modelId);
		Model mergeModel = modelService.findDetail(mergeModelId);
		map.put("model", model);
		map.put("mergeModel", mergeModel);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：合并新模型</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param modelId
	 * @param mergeModelId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getMergeModel")
	public Map<String,Object> getMergeModel(Long modelId,Long mergeModelId){
		Map<String,Object> map = Maps.newHashMap();
		Model newModel = getNewModel(modelId, mergeModelId);
		map.put("newModel", newModel);

		return map;
	}

	/**
	 * 
	 * <p>名称：合并新模型</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param modelId
	 * @param mergeModelId
	 * @return
	 */
	public Model getNewModel(Long modelId,Long mergeModelId) {
		Model model = modelService.findDetail(modelId);
		Model mergeModel = modelService.findDetail(mergeModelId);
		Model newModel = new Model();
		//判断属性是否新增
		if (!mergeModel.getUseAttribute()) {
			if (model.getUseAttribute()) {
				newModel.setUseAttribute(true);
				newModel.setAttributes(model.getAttributes());
			}
		} else {
			if (model.getUseAttribute()) {
				List<Attribute> attributeList = model.getAttributes();
				List<Attribute> mergeAttributeList = mergeModel.getAttributes();
				List<Attribute> newAttributeList = getMergeAttribute(attributeList, mergeAttributeList);
				newModel.setAttributes(newAttributeList);
				newModel.setUseAttribute(true);
			}
		}
		//判断规格是否新增
		if (!mergeModel.getUseSpec()) {
			if (model.getUseSpec()) {
				newModel.setUseSpec(true);
				newModel.setModelSpecs(model.getModelSpecs());
			}
		} else {
			if (model.getUseSpec()) {
				List<ModelSpec> modelSpecList = model.getModelSpecs();
				List<ModelSpec> mergeModelSpecList = mergeModel.getModelSpecs();
				List<ModelSpec> newModelSpecList = getMergeModelSpec(modelSpecList, mergeModelSpecList);
				newModel.setModelSpecs(newModelSpecList);
				newModel.setUseSpec(true);
			}
		}
		//判断参数是否新增
		if (!mergeModel.getUseParameter()) {
			if (model.getUseParameter()) {
				newModel.setUseParameter(true);
				newModel.setParameters(model.getParameters());
			}
		} else {
			if (model.getUseParameter()) {
				List<Parameter> parameterList = model.getParameters();
				List<Parameter> mergeParameterList = mergeModel.getParameters();
				List<Parameter> newParameterList = getMergeParameter(parameterList, mergeParameterList);
				newModel.setParameters(newParameterList);
				newModel.setUseParameter(true);
			}
		}
		
		return newModel;
	}
	
	/**
	 * 
	 * <p>名称：合并属性</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param attributeList
	 * @param mergeAttributeList
	 * @return
	 */
	public List<Attribute> getMergeAttribute(List<Attribute> attributeList,List<Attribute> mergeAttributeList) {
		List<Attribute> newAttribute = new ArrayList<>();
		if (null != attributeList) {
			newAttribute = attributeList;
		}
		//将原有的属性与需合并的比较，不存在插入
		if (null != mergeAttributeList) {
			for (Attribute mergeAttribute: mergeAttributeList) {
				String mergeName = mergeAttribute.getName();
				Boolean add = true;
				if (null != attributeList) {
					for (Attribute attribute : attributeList) {
						String name = attribute.getName();
						if (mergeName.equals(name)) {
							add = false;
						}
					}
				}
				if (add) {
					newAttribute.add(mergeAttribute);
				}
			}
		}
		return newAttribute;
	}
	
	/**
	 * 
	 * <p>名称：合并规格</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param modelSpecList
	 * @param mergeModelSpecList
	 * @return
	 */
	public List<ModelSpec> getMergeModelSpec(List<ModelSpec> modelSpecList,List<ModelSpec> mergeModelSpecList){
		List<ModelSpec> newModelSpec = new ArrayList<>();
		if (null != modelSpecList) {
			newModelSpec = modelSpecList;
		}
		//将原有的规格与需合并的比较，不存在插入
		if (null != mergeModelSpecList) {
			for (ModelSpec mergeModelSpec : mergeModelSpecList) {
				Boolean add = true;
				Long mergeSpecId = mergeModelSpec.getSpec().getId();
				if (null != modelSpecList) {
					for (ModelSpec modelSpec : modelSpecList) {
						Long specId = modelSpec.getSpec().getId();
						if (mergeSpecId.equals(specId)) {
							add = false;
						}
					}
				}
				if (add) {
					newModelSpec.add(mergeModelSpec);
				}
			}
		}
		return newModelSpec;
	}
	
	/**
	 * 
	 * <p>名称：合并参数</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param parameterList
	 * @param mergeParameterList
	 * @return
	 */
	public List<Parameter> getMergeParameter(List<Parameter> parameterList,List<Parameter> mergeParameterList){
		List<Parameter> newParameter = new ArrayList<>();
		if (null != parameterList) {
			newParameter = parameterList;
		}
		//将原有的参数与需合并的比较，不存在插入
		if (null != mergeParameterList) {
			for (Parameter mergeParameter : mergeParameterList) {
				Boolean add = true;
				String mergeParameterName = mergeParameter.getName();
				if (null != mergeParameterList) {
					for (Parameter parameter : parameterList) {
						String parameterName = parameter.getName();
						if (mergeParameterName.equals(parameterName)) {
							add = false;
							List<Parameter> childrenMergeParameterList = getChildrenParameter(parameter, mergeParameter);
							mergeParameter.setChildren(childrenMergeParameterList);
						}
					}
				}
				if (add) {
					newParameter.add(mergeParameter);
				}
			}
		}
		return newParameter;
	}
	
	/**
	 * 
	 * <p>名称：合并子参数</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param parameter
	 * @param mergeParameter
	 * @return
	 */
	public List<Parameter> getChildrenParameter(Parameter parameter,Parameter mergeParameter) {
		List<Parameter> childrenParameterList = new ArrayList<>();
		if (null != parameter) {
			childrenParameterList = parameter.getChildren();
		}
		//比较参数组中的参数
		if (null != mergeParameter && null != mergeParameter.getChildren()) {
			for (Parameter childrenMergeParameter : mergeParameter.getChildren()) {
				String childrenMergeName = childrenMergeParameter.getName();
				Boolean add = true;
				if (null != parameter && null != parameter.getChildren()) {
					for (Parameter childrenParameter : parameter.getChildren()) {
						String childrenName = childrenParameter.getName();
						if (childrenMergeName.equals(childrenName)) {
							add = false;
						}
					}
				}
				if (add) {
					childrenParameterList.add(childrenMergeParameter);
				}
			}
		}
		return childrenParameterList;
	}
	
	/**
	 * 
	 * <p>名称：保存新模型</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param title
	 * @param modelId
	 * @param mergeModelId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveNewModel")
	public Map<String,Object> saveNewModel(String title,Long modelId,Long mergeModelId){
		Model model = getNewModel(modelId, mergeModelId);
		List<Category> categoryList = categoryService.findByModelId(modelId);
		List<Category> mergecategoryList = categoryService.findByModelId(mergeModelId);
		Map<String,Object> map = Maps.newHashMap();
		Map<String,Object> param = Maps.newHashMap();
		param.put("name", title);
		String error = "";
		int count = modelService.count(param);
		if (1 > count) {
			cleanModel(model);
			cancelId(model);
			model.setName(title);
			modelService.save(model);
			for (Category category : categoryList) {
				category.setModel(model);
				categoryService.update(category);
			}
			for (Category mergeCategory : mergecategoryList) {
				mergeCategory.setModel(model);
				categoryService.update(mergeCategory);
			}
			modelService.delete(modelId);
			modelService.delete(mergeModelId);
		} else {
			error = "模型名称重复";
		}
		map.put("error", error);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：去除新模型参数，规格，属性id</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param model
	 */
	public void cancelId(Model model) {
		List<Attribute> attributeList = model.getAttributes();
		List<ModelSpec> modelSpecList = model.getModelSpecs();
		List<Parameter> parameterList = model.getParameters();
		for (Attribute attribute : attributeList) {
			attribute.setId(null);
			if (null != attribute.getAttributeValues()) {
				for (AttributeValue attributeValue : attribute.getAttributeValues()) {
					attributeValue.setId(null);
				}
			}
		}
		for (ModelSpec modelSpec : modelSpecList) {
			modelSpec.setId(null);
		}
		for (Parameter parameter : parameterList) {
			parameter.setId(null);
			if (null != parameter.getChildren()) {
				for (Parameter children : parameter.getChildren()) {
					children.setId(null);
				}
			}
		}
	}
}