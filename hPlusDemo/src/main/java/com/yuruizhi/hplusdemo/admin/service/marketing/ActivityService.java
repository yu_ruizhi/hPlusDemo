/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ActivityService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月21日 下午2:18:25 
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.marketing.Activity;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 活动管理Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午2:18:25 
 * @version: 1.0
 */
public interface ActivityService extends BaseService<Activity> {
	
	/**
	 * 
	 * <p>名称：根据活动名称检测是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name 活动名称
	 * @return 返回true,false
	 */
	Map<String, Boolean> checkIsNameExists(String name);
	
	/**
	 * 
	 * <p>名称：根据活动id及活动名称检测是否重名</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param hasAnnotherName 活动id，活动名称数组
	 * @return 返回true,false
	 */
	Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName);
	
}
