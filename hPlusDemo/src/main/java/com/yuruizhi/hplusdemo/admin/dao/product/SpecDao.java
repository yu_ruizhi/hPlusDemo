/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.product;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Spec;

import java.util.List;
import java.util.Map;


/**
 * Created on 2015-07-08 18:19:06
 *
 * @author vacoor
 */
@MyBatisMapper
public interface SpecDao extends BaseDao<Spec> {

    List<Spec> findSpecAndValuesByProductId(String pid);

    /**
     * 
     * <p>名称：根据名称获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    List<Spec> findByName(String name);
    
    String getIsExistsSpec(Map<String,Object> m);
    
}