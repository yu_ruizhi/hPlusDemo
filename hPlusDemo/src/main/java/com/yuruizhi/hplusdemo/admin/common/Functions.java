/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.common;


import org.ponly.common.json.Jacksons;

import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;

import java.util.*;

/**
 * @author vacoor
 */
public abstract class Functions {

    public static boolean contains(Iterable<?> it, Object target) {
        if (null == it) {
            return false;
        }
        for (Object e : it) {
            if (e == target || (e != null && e.equals(target))) {
                return true;
            }
        }
        return false;
    }

    public static String concat(String a, String b) {
        a = null != a ? a : "";
        b = null != b ? b : "";
        return a + b;
    }

    public static String toJson(Object obj) {
        return Jacksons.serialize(obj);
    }

    public static boolean hasSpecValue(Iterable<Goods> goodsList, Long id) {
        if (null == goodsList) {
            return false;
        }

        for (Goods goods : goodsList) {
            List<SpecValue> values = goods.getSpecValues();
            if (null == values) {
                continue;
            }
            for (SpecValue value : values) {
                Long vid = null;
                if (null == value || null == (vid = value.getId())) {
                    continue;
                }
                if (vid.equals(id)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String appendParam(Map<String, String[]> params, String p, Object v) {
        v = null == v ? "" : v;
        Map<String, String[]> clone = new HashMap<String, String[]>(params);

        String[] newValues = clone.get(p);
        newValues = null == newValues ? new String[1] : Arrays.copyOf(newValues, newValues.length + 1);
        newValues[newValues.length - 1] = String.valueOf(v);

        clone.put(p, newValues);
        return toQs(clone);
    }

    public static String removeParam(Map<String, String[]> params, String p) {
        Map<String, String[]> clone = new HashMap<String, String[]>(params);
        clone.remove(p);
        return toQs(clone);
    }

    public static String removeParamValue(Map<String, String[]> params, String p, String v) {
        Map<String, String[]> clone = new HashMap<String, String[]>(params);
        String[] values = clone.get(p);
        List<String> newValues = new ArrayList<String>();

        if (null != values) {
            for (String value : values) {
                if (v == value || (null != v && v.equals(value))) {
                    continue;
                }
                newValues.add(value);
            }
            clone.put(p, newValues.toArray(new String[newValues.size()]));
        }
        return toQs(clone);
    }

    public static String replaceParam(Map<String, String[]> params, String p, String value) {
        Map<String, String[]> clone = new HashMap<String, String[]>(params);
        clone.put(p, new String[]{value});
        return toQs(clone);
    }

    public static String toQs(Map<String, String[]> params) {
        StringBuilder buff = new StringBuilder();
        for (Map.Entry<String, String[]> param : params.entrySet()) {
            String key = param.getKey();
            // ....
            if ("p".equals(key)) {
                continue;
            }
            String[] values = param.getValue();

            for (String value : values) {
                buff.append("&").append(key).append("=").append(value);
            }
        }
        return buff.replace(0, 1, "?").toString();
    }
    public static boolean hasEqPropObj(@SuppressWarnings("rawtypes") java.lang.Iterable it,java.lang.Object target,java.lang.String prop) {
        return org.ponly.web.servlet.jsp.func.Functions.containsEqPropInstance(it, target, prop);
    }
    public static Object find(@SuppressWarnings("rawtypes") java.lang.Iterable it,java.lang.String path,java.lang.Object value) {
        return org.ponly.web.servlet.jsp.func.Functions.find(it, path, value);
    }
}
