/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.ParameterValueDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ParameterValue;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.ParameterValueService;

/**
 * <p>名称: ParameterValueServiceImpl</p>
 * <p>说明: 产品参数值服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:55:48   
 * @version: 1.0
 */
@Service
public class ParameterValueServiceImpl extends BaseServiceImpl<ParameterValue> implements ParameterValueService {

    @Autowired
	public void setParameterValueDao(ParameterValueDao parameterValueDao) {
		super.setBaseDao(parameterValueDao);
	}

   

}
