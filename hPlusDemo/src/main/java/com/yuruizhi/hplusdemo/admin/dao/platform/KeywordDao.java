/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: KeywordDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.platform
 * Date: 2016年4月23日 下午1:35:30
 **/
package com.yuruizhi.hplusdemo.admin.dao.platform;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.platform.Keyword;

/**
 * 
 * <p>名称: 搜索管理 关键字DAO</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月23日 下午1:35:30   
 * @version: 1.0
 */
public interface KeywordDao extends BaseDao<Keyword> {

	/**
	 * 
	 * <p>名称：获取符合条件的关键字</p> 
	 * <p>描述：参数Map中的key与实体类中的属性名一致，多个条件之间是“AND”关系</p>
	 * @author：wentan
	 * @param paramMap
	 * @return
	 */
	List<Keyword> getBaseWorks(Map<String, Object> paramMap);
}
