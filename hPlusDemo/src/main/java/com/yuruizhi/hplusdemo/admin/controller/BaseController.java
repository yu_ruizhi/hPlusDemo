package com.yuruizhi.hplusdemo.admin.controller;


/**
 * 每个项目都有的 Base Controller, 项目中 Controller 都应该继承, 方便后续增加逻辑
 *
 * @author yangchanghe
 * @deprecated
 * @see com.yuruizhi.hplusdemo.admin.controller.AdminBaseController
 */
@Deprecated
public abstract class BaseController extends org.ponly.webbase.controller.ControllerSupport {

    protected String getViewPath(String path) {
        return getRelativeViewPath(path);
    }
}
