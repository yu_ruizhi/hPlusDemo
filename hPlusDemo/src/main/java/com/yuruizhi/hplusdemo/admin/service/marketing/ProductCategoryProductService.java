/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryProductService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月20日 上午10:39:17  
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategoryProduct;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 分类商品-商品Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:39:17   
 * @version: 1.0
 */
public interface ProductCategoryProductService extends BaseService<ProductCategoryProduct> {

	/**
	 * 
	 * <p>名称：根据分类推荐id获取单品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param categoryId 分类推荐id
	 * @return 返回单品信息
	 */
	List<ProductCategoryProduct> findByCategoryId(Long categoryId);
	
	/**
	 * 
	 * <p>名称：根据单品id及分类推荐id获取单品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param goodsId 单品id
	 * @param categoryId 分类推荐id
	 * @return 返回单品信息
	 */
	ProductCategoryProduct findByIdAndCategoryId(Long goodsId,Long categoryId);
}
