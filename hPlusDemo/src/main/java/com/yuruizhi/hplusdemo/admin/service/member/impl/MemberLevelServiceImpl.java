/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MemberLevelServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.member.impl
 * Date: 2016年4月18日 上午11:49:18   
 **/
package com.yuruizhi.hplusdemo.admin.service.member.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.member.MemberLevelDao;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberLevel;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.member.MemberLevelService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 会员等级ServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月18日 上午11:49:18  
 * @version: 1.0
 */
@Service

public class MemberLevelServiceImpl extends BaseServiceImpl<MemberLevel> implements MemberLevelService{

	private MemberLevelDao memberLevelDao;
	
	@Autowired
	public void setMemberLevelDao(MemberLevelDao memberLevelDao){
		this.memberLevelDao = memberLevelDao;
		setCrudDao(memberLevelDao);
	}
	
	@Override
	public Map<String, Boolean> checkIsNameExists(String name) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<MemberLevel> memberLevelList = memberLevelDao.findByName(name);
		if (null != memberLevelList && memberLevelList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

	@Override
	public Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<MemberLevel> memberLevelList = memberLevelDao.findByName(hasAnnotherName[1]);
		if (null != memberLevelList && memberLevelList.size() > 0) {
			Iterator<MemberLevel> it = memberLevelList.iterator();
			MemberLevel memberLevel = it.hasNext() ? it.next() : null;
			result.put("isExists", null != memberLevel && !memberLevel.getId().equals(Long.valueOf(hasAnnotherName[0])));
		}
		return result;
	}
	
}
