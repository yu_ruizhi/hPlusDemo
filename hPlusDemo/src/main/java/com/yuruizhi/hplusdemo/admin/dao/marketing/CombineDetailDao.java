/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.apache.ibatis.annotations.Param;
import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CombineDetail;

/**
 * Created on 2016-03-18 09:50:38
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface CombineDetailDao extends BaseDao<CombineDetail> {

	void deleteByCombineId(@Param("id")Long id);

	void deleteAllByCombineId(@Param("id")Long id);
}