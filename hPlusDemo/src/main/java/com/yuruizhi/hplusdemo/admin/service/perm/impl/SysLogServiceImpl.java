/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import com.yuruizhi.hplusdemo.admin.dao.perm.SysLogDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.SysLog;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.net.InetAddress;
import java.util.Date;

/**
 * <p>名称: SysLogServiceImpl</p>
 * <p>说明: 后台日志管理服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:41:07   
 * @version: 1.0
 */
@Service
public class SysLogServiceImpl extends BaseServiceImpl<SysLog> implements SysLogService {

    @SuppressWarnings("unused")
	private SysLogDao sysLogDao;

    @Autowired
    private void setRoleDao(SysLogDao sysLogDao){
        this.sysLogDao = sysLogDao;
        setBaseDao(sysLogDao);
    }

    /**
     * 记录日志及是否记录成功
     * @param type          操作类型 1：新增  2：删除   3:修改  建议传值用 Constant  中的常量 SYSLOG_SAVE  SYSLOG_DELETE  SYSLOG_UPDATE ，便于维护
     * @param operation    日志内容
     * @param moduleName    模块名称
     * @return map
     *
     * 新增：模块 - 新增 - 内容（title）
     *编辑 ：模块 - 内容（字段） - 由 -“”修改为-“”
     *删除  : 模块 - 删除 - 内容（title）
     */
    @Override
    public boolean saveSysLog(Integer type, String operation,String moduleName) {
       try{
           SysLog sysLog=new SysLog();
           sysLog.setDeleted(false);
           sysLog.setCreatedDate(new Date());
           sysLog.setOperation(operation);
           sysLog.setType(type);
           sysLog.setModuleName(moduleName);
           sysLog.setUserIp(this.getIpAddr());
           super.save(sysLog);
           return true;
       }catch (Exception e){
           return false;
       }
    }

//获取用户ip
    public String getIpAddr() {
        try{
            InetAddress addr = InetAddress.getLocalHost();
           return addr.getHostAddress().toString(); //获取本机ip
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }


}
