/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

/**
 * <p>名称: Deduction</p>
 * <p>说明: 满减促销（包邮、打折、送积分）</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:54:30   
 * @version: 1.0
 */
public class Deduction extends Promotion {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 5506003542409288467L;

	/**
     * 最低使用金额
     *
     * @ViewField editor=input
     * @Column MIN_USAGE_AMOUNT
     */
    @NotNull
    @Size(max = 8)
    @Digits(integer = 8, fraction = 2)
    private BigDecimal minUsageAmount;

    /**
     * 折扣
     *
     * @ViewField editor=input
     * @Column DISCOUNT
     */
    @Size(max = 8)
    @Digits(integer = 8, fraction = 2)
    private BigDecimal discount;

    /**
     * 减价
     *
     * @ViewField editor=input
     * @Column DESCEND
     */
    @Size(max = 8)
    @Digits(integer = 8, fraction = 2)
    private BigDecimal descend;

    /**
     * 包邮
     *
     * @ViewField editor=input
     * @Column FREE_SHIPPING
     */
    @NotNull
    private Boolean freeShipping;

    /**
     * 是否删除
     *
     * @ViewField editor=input
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted;

    public BigDecimal getMinUsageAmount() {
        return this.minUsageAmount;
    }

    public void setMinUsageAmount(BigDecimal minUsageAmount) {
        this.minUsageAmount = minUsageAmount;
    }

    public BigDecimal getDiscount() {
        return this.discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDescend() {
        return this.descend;
    }

    public void setDescend(BigDecimal descend) {
        this.descend = descend;
    }

    public Boolean getFreeShipping() {
        return this.freeShipping;
    }

    public void setFreeShipping(Boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
