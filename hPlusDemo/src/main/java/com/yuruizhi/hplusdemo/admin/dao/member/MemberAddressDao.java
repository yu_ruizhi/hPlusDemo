package com.yuruizhi.hplusdemo.admin.dao.member;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;

public interface MemberAddressDao extends BaseDao<MemberAddress> {
	
	/**
	 * 新增
	 * @param users
	 * @return
	 */
    void insert(MemberAddress memberAddress);
	
	/**
	 * 设置默认地址
	 * @param users
	 * @return
	 */
	int updateDefault(MemberAddress memberAddress);
	
	
	/**
	 * 删除默认地址
	 * @param users
	 * @return
	 */
	int deleteDefault(MemberAddress memberAddress);
	
    
}
