/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import com.yuruizhi.hplusdemo.admin.util.geetest.sdk.java.GeetestLib;
import com.yuruizhi.hplusdemo.admin.util.geetest.sdk.java.web.demo.GeetestConfig;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * <p>名称: GeetestUtil</p>
 * <p>说明: 极验滑动验证工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：YuRuizhi
 * @date：2016年12月11日下午8:30:17
 * @version: 1.0
 */
public abstract class GeetestUtil {

	private static final long serialVersionUID = 244554953219893949L;

	public static void generateGeetest(HttpServletRequest request,
						 			   HttpServletResponse response) throws IOException {

		GeetestLib gtSdk = new GeetestLib(GeetestConfig.getGeetest_id(), GeetestConfig.getGeetest_key());

		String resStr = "{}";

		//自定义userid
		String userid = "test";

		//进行验证预处理
		int gtServerStatus = gtSdk.preProcess(userid);

		//将服务器状态设置到session中
		request.getSession().setAttribute(gtSdk.gtServerStatusSessionKey, gtServerStatus);
		//将userid设置到session中
		request.getSession().setAttribute("userid", userid);

		/*resStr = gtSdk.getResponseStr();

		PrintWriter out = response.getWriter();
		out.println(resStr);*/

	}

	public static Boolean VerifyGeetest( String challenge,
										 String validate,
										 String seccode,
										 HttpServletRequest request,
										 HttpServletResponse response,
										 HttpSession session) throws IOException {

		GeetestLib gtSdk = new GeetestLib(GeetestConfig.getGeetest_id(), GeetestConfig.getGeetest_key());


		//从session中获取gt-server状态
		int gt_server_status_code = (Integer) request.getSession().getAttribute(gtSdk.gtServerStatusSessionKey);

		//从session中获取userid
		String userid = (String)request.getSession().getAttribute("userid");

		int gtResult = 0;

		if (gt_server_status_code == 1) {
			//gt-server正常，向gt-server进行二次验证

			gtResult = gtSdk.enhencedValidateRequest(challenge, validate, seccode, userid);
			System.out.println(gtResult);
		} else {
			// gt-server非正常情况下，进行failback模式验证

			System.out.println("failback:use your own server captcha validate");
			gtResult = gtSdk.failbackValidateRequest(challenge, validate, seccode);
			System.out.println(gtResult);
		}


		if (gtResult == 1) {
			// 验证成功
			/*PrintWriter out = response.getWriter();
			JSONObject data = new JSONObject();
			try {
				data.put("status", "success");
				data.put("version", gtSdk.getVersionInfo());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			out.println(data.toString());*/
			return true;
		}
		else {
			// 验证失败
			/*JSONObject data = new JSONObject();
			try {
				data.put("status", "fail");
				data.put("version", gtSdk.getVersionInfo());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			PrintWriter out = response.getWriter();
			out.println(data.toString());*/
			return false;
		}
	}
	

}
