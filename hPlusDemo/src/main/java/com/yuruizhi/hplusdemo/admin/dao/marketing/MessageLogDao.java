/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageMemberDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月20日上午10:55:16
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageLog;
import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import java.util.List;

/**
 * 
 * <p>名称: MessageLogDao </p>
 * <p>说明: 私信记录Dao </p>
 * <p>创建记录：（2016.8.19 14:24 - yuruizhi - 增加私信记录DAO）</p>
 *
 * @author yuruizhi
 * @date 2016年8月19日 下午14:24:06
 * @version: 1.0
 */
@MyBatisMapper
public interface MessageLogDao extends BaseDao<MessageLog> {

	/**
	 *
	 * <p>名称：根据会员ID查询记录</p>
	 * <p>描述：</p>
	 * @author yuruizhi
	 * @param memberId 会员ID
	 * @return
	 */
	List<MessageLog> findByMemberId(String memberId);
}
