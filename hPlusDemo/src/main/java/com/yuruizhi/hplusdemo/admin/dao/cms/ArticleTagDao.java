package com.yuruizhi.hplusdemo.admin.dao.cms;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.ArticleTag;

public interface ArticleTagDao extends BaseDao<ArticleTag> {
	List<ArticleTag> findByArtId(Long id);
	void delByArtId(Long id);
}
