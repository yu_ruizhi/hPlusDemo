/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import static org.ponly.common.util.Throwables.*;

/**
 * <p>名称: HttpUtils</p>
 * <p>说明: 简单的Http 请求工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:32:59   
 * @version: 1.0
 */
public abstract class HttpUtils {
    private static final Logger LOG = LoggerFactory.getLogger(HttpUtils.class);
    private static final String POST_METHOD = "POST";
    private static final String ENCODING = "UTF-8";

    public static boolean postOnly(String serverUrl, Map<String, String> data, int timeout) {
        InputStream is = null;
        try {
            is = post(serverUrl, data, timeout);
            return true;
        } catch (Throwable ignore) {
            // ignore
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
        return false;
    }

    public static InputStream post(String serverUrl, Map<String, String> data, int timeout) throws IOException {
        OutputStreamWriter writer = null;
        URL url;

        try {
            url = new URL(serverUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            if ("https".equals(url.getProtocol())) {
                LOG.debug("init ssl context for {}", serverUrl);
                // 初始化 SSL 上下文
                SSLContext sslContext = SSLContext.getInstance("SSL");
                // 类似12306 会使用证书, 这里初始化为不是用密钥库, 信任任何证书
                sslContext.init(new KeyManager[]{}, new TrustManager[]{new TrustAnyTrustManager()}, new SecureRandom());
                SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

                ((HttpsURLConnection) conn).setSSLSocketFactory(sslSocketFactory);
            }

            conn.setRequestMethod(POST_METHOD);
            conn.setDoOutput(true);
            conn.setConnectTimeout(timeout);
            writer = new OutputStreamWriter(conn.getOutputStream());

            if (null != data) {
                boolean first = true;
                for (Map.Entry<String, String> entry : data.entrySet()) {
                    if (!first) {
                        writer.write('&');
                    }
                    first = false;
                    writer.write(URLEncoder.encode(entry.getKey(), ENCODING));
                    writer.write('=');
                    writer.write(URLEncoder.encode(entry.getValue(), ENCODING));
                }
            }
            writer.flush();

            return conn.getInputStream();
        } catch (NoSuchAlgorithmException e) {
            LOG.warn("", e);

            return rethrowRuntimeException(e);
        } catch (KeyManagementException e) {
            LOG.warn("", e);
            return rethrowRuntimeException(e);
        } finally {
            if (null != writer) {
                try {
                    writer.close();
                } catch (IOException ignore) {/* ignore */}
            }
        }
    }


    /**
     * 信任任何证书的信任库管理器
     */
    private static class TrustAnyTrustManager implements X509TrustManager {

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    private HttpUtils() {
    }
}
