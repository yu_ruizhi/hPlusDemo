/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.NotNull;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: SmsMember</p>
 * <p>说明: 短信会员关系实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:08:10   
 * @version: 1.0
 */
public class SmsMember extends BaseEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7853466531441695647L;

	//短消息ID
	private Long smsId;
	
	/**
     * 会员ID
     * 邮件内容
     * @ViewField editor=input 
     * @Column MEMBER_ID
     */
    @NotNull
    private Long memberId;

    

	/**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

	public Long getSmsId() {
		return smsId;
	}

	public void setSmsId(Long smsId) {
		this.smsId = smsId;
	}

	/**
	 * @return the memberId
	 */
	public Long getMemberId() {
		return memberId;
	}

	/**
	 * @param memberId the memberId to set
	 */
	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}
}
