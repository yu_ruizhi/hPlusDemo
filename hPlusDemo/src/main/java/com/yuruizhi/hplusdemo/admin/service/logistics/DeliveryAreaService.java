package com.yuruizhi.hplusdemo.admin.service.logistics;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryArea;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by ASUS on 2014/12/8.
 */
public interface DeliveryAreaService extends BaseService<DeliveryArea> {
public BigDecimal getPostage(DeliveryMethod deliveryMethod,String[] ids,String[] nums);

    void addArea(DeliveryArea deliveryArea);
    void bianji(DeliveryArea deliveryArea);
    List<DeliveryArea> selectAll();

    /**
     * 保存验证重名
     */
	Map<String, Boolean> checkIsNameExists(String name);
	
	/**
	 * 更新验证重名
	 */
	Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName);
    
}
