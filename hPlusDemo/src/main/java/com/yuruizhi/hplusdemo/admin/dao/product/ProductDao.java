package com.yuruizhi.hplusdemo.admin.dao.product;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;

import java.util.List;
import java.util.Map;

/**
 * Project Name：
 * Description：
 * Created By：      Argevolle
 * Created Date：    2014/12/3 10:44
 * Modified By：
 * Modified Date：
 * Modified Remark：
 * Version：         v 0.1
 */
public interface ProductDao extends BaseDao<Product> {
   String PRODUCT_FILTERS_PROP = "_product_filters";
   String BRAND_FILTERS_PROP = "_brand_filters";
   String MODEL_FILTERS_PROP = "_model_filters";
   String CATEGORY_FILTERS_PROP = "_category_filters";
   String WORKS_FILTERS_PROP = "_works_filters";
   String SUPPLIER_FILTERS_PROP = "_supplier_filters";
   
   Float getWeight(String goodsId);
   void deleteProductLabelByProductId(Long pid);
   void saveProductLabel(Map<String, Object> params);

   int countForIndexList(Map<String, Object> paramsMap);

   List<Product> findManyForIndexList(Map<String, Object> paramsMap);

   /**
    * 
    * <p>名称：根据分类id获取商品信息</p> 
    * <p>描述：</p>
    * @author：wuqi
    * @param categoryId
    * @return
    */
   List<Product> findByCategoryId(Long categoryId);

   /**
    *
    * <p>名称：根据商品截止日期商品状态查询商品</p>
    * <p>描述：</p>
    * @author yuruizhi
    * @param parmsMap
    * @return
    */
   List<Product> searchProductByTimeAndStatus(Map<String,Object> parmsMap);
   
   List<Map<String,Object>> searchGoodsExcel(Map<String,Object> parmsMap);
   
   Integer findProductCount(Long modelId);
}