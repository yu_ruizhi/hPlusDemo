/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.cms.ArticleDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.Article;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.ArticleService;

/**
 * <p>名称: ArticleService实现类</p>
 * <p>说明: ArticleService实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：ChenGuang
 * @date：2016年5月5日下午3:42:57   
 * @version: 1.0
 */
@Service

public class ArticleServiceImpl extends BaseServiceImpl<Article> implements ArticleService {
	
	@Autowired
	private ArticleDao articleDao;
	@Override
	public List<Article> getInfo(Map<String, Object> params) {
		// TODO Auto-generated method stub
		return articleDao.getInfo(params);
	}
	@Override
	public int getInfoCount(Map<String, Object> params) {
		
		return articleDao.getInfoCount(params);
	}
}
