/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.logistics;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: DeliveryMethod</p>
 * <p>说明: 物流方式实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:47:11   
 * @version: 1.0
 */
public class DeliveryMethod extends BaseEntity{

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -550839901271128825L;

	//送货方式名称
    private String name;

    /**
     * 物流公司
     */
    private String logistics;

    //排序
    private Integer sortId;

    //备注
    private String remark;

    //前台是否显示
    private Boolean visible;

    //邮寄区域
    private List<DeliveryMethodDetail> deliveryMethodDetails;

    //是否删除
    private Boolean deleted;

    /*邮费*/
    private BigDecimal postage;

    /*自己添加属性方便维护后台邮寄方式和邮寄区域*/

    private String[] areaIsa;

    public String[] getAreaIsa() {
        return areaIsa;
    }

    public void setAreaIsa(String[] areaIsa) {
        this.areaIsa = areaIsa;
    }

    public BigDecimal getPostage() {
        return postage;
    }

    public void setPostage(BigDecimal postage) {
        this.postage = postage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public List<DeliveryMethodDetail> getDeliveryMethodDetails() {
        return deliveryMethodDetails;
    }

    public void setDeliveryMethodDetails(List<DeliveryMethodDetail> deliveryMethodDetails) {
        this.deliveryMethodDetails = deliveryMethodDetails;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "DeliveryMethod{" +
                "name='" + name + '\'' +
                ", logistics='" + logistics + '\'' +
                ", sortId=" + sortId +
                ", remark='" + remark + '\'' +
                ", visible=" + visible +
                ", deliveryMethodDetails=" + deliveryMethodDetails +
                ", deleted=" + deleted +
                ", postage=" + postage +
                ", areaIsa=" + Arrays.toString(areaIsa) +
                '}';
    }
}
