/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: WorksServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.product.impl
 * Date: 2016年4月18日 下午5:34:30 
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.product.WorksDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.product.WorksService;

/**
 * 
 * <p>名称: 作品Service实现类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月18日下午5:35:11   
 * @version: 1.0
 */
@Service
public class WorksServiceImpl extends BaseServiceImpl<Works>implements WorksService {

	private WorksDao worksDao;
	
	@Autowired
	private SysLogService sysLogService;
	
	@Autowired
	public void setWorksDao(WorksDao worksDao) {
		this.worksDao = worksDao;
		super.setBaseDao(worksDao);
	}
	
	@Override
	public void update(Works e) {
		e.setLastModifiedDate(new Date());
		User user = getCurrentAdminUserIfHas();
		if(null != user){
			e.setLastModifiedBy(user.getLoginName());			
		}
		worksDao.update(e);
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "作品管理-修改-ID:"+e.getId(), "商品管理");
	}

	@Override
	public void save(Works works) {
		Date date = new Date();
		User user = getCurrentAdminUserIfHas();
		works.setCreatedDate(date);
		works.setLastModifiedDate(date);
		if(null != user){
			works.setCreatedBy(user.getLoginName());
			works.setLastModifiedBy(user.getLoginName());
		}
		worksDao.save(works);	
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "作品管理-新增-ID:"+works.getId(), "商品管理");
	}

	@Override
	public void delete(Long[] argu) {
		StringBuffer ids = new StringBuffer();
		for(Long id : argu){
			ids.append(id + ",");
		}
		ids.deleteCharAt(ids.length()-1);
        super.delete(argu);
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "作品管理-删除-ID:"+ids, "商品管理");		
	}

	@Override
	public Map<String, Boolean> checkIsWorksExists(String name) {
		Map<String, Boolean> result = new HashMap<String, Boolean>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("name", name);
        List<Works> worksList = getBaseWorks(paramMap);
        if (null != worksList && worksList.size() > 0) {
            result.put("isExists", true);
        } else {
            result.put("isExists", false);
        }
        return result;
    }

	@Override
	public Boolean checkIsAnotherWorksExists(Long id, String name) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("name", name);
		List<Works> worksList = getBaseWorks(paramMap);
		Works works = (null != worksList && 0 != worksList.size()) ? worksList.get(0) : null;
		return works == null || works.getId().equals(id);
	}

	private List<Works> getBaseWorks(Map<String, Object> paramMap) {
		return worksDao.getBaseWorks(paramMap);
	}

	@Override
	public List<Works> findByName(String name) {
		return worksDao.findByName(name);
	}

}
