/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>名称: OrdersReceipt</p>
 * <p>说明: 订单实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:10:45   
 * @version: 1.0
 */
public class OrdersReceipt extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/** @Fields GOODTYPE_RESERVE : 商品类型 - 预订 */
	public static final int GOODTYPE_RESERVE = 1;
	/** @Fields GOODTYPE_EXISTING :商品类型 - 现货 */
	public static final int GOODTYPE_EXISTING = 2;
	/** @Fields GOODTYPE_LIMITED : 商品类型 - 限购 */
	public static final int GOODTYPE_LIMITED = 3;

	/** @Fields ORIGINATE_PC : 订单来源 - 计算机 */
	public static final int ORIGINATE_PC = 1;
	/** @Fields ORIGINATE_APP :订单来源 -移动端 */
	public static final int ORIGINATE_APP = 2;
	
	/**@Fields ORDERTYPE_COMMON : 订单类型 - 普通订单 */ 
	public static final int ORDERTYPE_COMMON = 1;
	/**@Fields ORDERTYPE_RESERVE : 订单类型 - 预售订单*/ 
	public static final int ORDERTYPE_RESERVE = 2;
    /**@Fields ORDERTYPE_RESERVE : 订单类型 - 汇总订单*/
    public static final int ORDERTYPE_HUIZONG = 4;

	/** @Fields PRESALE_STATUS_DEPOSIT :定金订单 */
	public static final int PRESALE_STATUS_DEPOSIT = 1;
	/** @Fields PRESALE_STATUS_PAYDEPOSIT :已支付定金定金 */
	public static final int PRESALE_STATUS_PAYDEPOSIT = 2;
	/** @Fields PRESALE_STATUS_SINGLE :已配单 */
	public static final int PRESALE_STATUS_SINGLE = 3;
	/** @Fields PRESALE_STATUS_RETAINAGE :尾款订单 */
	public static final int PRESALE_STATUS_RETAINAGE = 4;
	/** @Fields PRESALE_STATUS_PAYRETAINAGE :已支付尾款 */
	public static final int PRESALE_STATUS_PAYRETAINAGE = 5;
	/** @Fields PRESALE_STATUS_PAYRETAINAGE :新尾款订单 */
	public static final int PRESALE_STATUS_NEWWEIKUAN = 6;
	/**
	 * member:用户.
	 */
	@JsonUnwrapped(prefix = "member.")
	private Member member;

	/**
	 * transactionNo:交易号.
	 */
	private String transactionNo;

	/**
	 * 订单号.
	 */
	private String orderCode;

	/**
	 * shippingAddress:收货地址，json 数据.
	 */
	private String shippingAddress;

	/**
	 * actualAmount:实际付款.
	 */
	private BigDecimal actualAmount;

	/**
	 * totalPrice:总金额.
	 */
	private BigDecimal totalPrice;

	/**
	 * couponId:优惠券id.
	 */
	private String couponId;

	/**
	 * 退换货实体集合
	 */
	// private List<Refund> refunds;

	/**
	 * paymentId:支付方式.
	 */
	private String paymentId;

	/**
	 * shippingMethodId:物流方式.
	 */
	private DeliveryMethod deliveryMethod;
	
	/**
	 * cancelReason
	 */
	private String cancelReason;
	


	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public DeliveryMethod getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	/**
	 * shippingFee:物流费用
	 */
	private BigDecimal deliveryFee;

	/**
	 * status:订单状态.
	 */
	private Integer status;

	/**
	 * pickersId:拣货号.
	 */
	private String pickersId;

	/**
	 * packagerId:打包号.
	 */
	private String packagerId;

	/**
	 * remark:备注.
	 */
	private String remark;

	/**
	 * 订单截止日期
	 */
	private Date expirationTime;

	/**
	 * 物流编号
	 */
	private String logisticsNo;

	/**
	 * 订单包裹重量
	 */
	private Float packageWeight;

	/**
	 * 支付时间
	 */
	private Date paidTime;

	/**
	 * 优惠信息
	 */
	private String promotionInfo;

	private Boolean deleted;

	private Long combineId; // 套餐/配件ID

	private String userName;


    private Long userId;


	private List<OrdersReceiptDetail> ordersReceiptDetails;

	// todo 新增字段
	/**
	 * 定金订单详情实体
	 */
	@JsonUnwrapped(prefix = "ordersReceiptDetail.")
	private OrdersReceiptDetail ordersReceiptDetail;

	public OrdersReceiptDetail getOrdersReceiptDetail() {
		return ordersReceiptDetail;
	}

	public void setOrdersReceiptDetail(OrdersReceiptDetail ordersReceiptDetail) {
		this.ordersReceiptDetail = ordersReceiptDetail;
	}

	/**
	 * 订单范围（经销商，淘宝，零售，会展）
	 */
	private Integer range;

	/**
	 * 商品类型（预定，现货，限购）
	 */
	private Integer goodType;

	/**
	 * 订单来源 （pc || app）
	 */
	private Integer originate;

	/**
	 * 定金金额
	 */
	private BigDecimal depositAmount;

	/**
	 * 优惠金额
	 */
	private BigDecimal discountAmount;

	/**
	 * 展会金额
	 */
	private BigDecimal exhibitionAmount;

	/**
	 * 多订单支付唯一标识
	 */
	private String uniqueCode;

	/**
	 * 订单类型，区别普通订单和预售订单
	 */
	private Integer orderType;

	/**
	 * （预售订单状态：1-定金订单；2-已支付定金；3-已配单；4-尾款订单；5-已支付尾款）
	 */
	private Integer presaleStatus;

	/**
	 * 收货地址对象
	 */
	@JsonUnwrapped(prefix = "memberAddress.")
	private MemberAddress memberAddress;
    /**
     * 派送时间    1  工作日、双休日与假日均可送
     *
     *            2 只双休日、假日送货（工作日不用送）
     *
     *            3 请在工作日送货（双休日、假日无法接收）
     *
     *            4 学校地址，白天没人，请尽量安排其他时间送货
     */
    private Long deliveryTimeId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeliveryTimeId() {
        return deliveryTimeId;
    }

    public void setDeliveryTimeId(Long deliveryTimeId) {
        this.deliveryTimeId = deliveryTimeId;
    }

    public MemberAddress getMemberAddress() {
		return memberAddress;
	}

	public void setMemberAddress(MemberAddress memberAddress) {
		this.memberAddress = memberAddress;
	}

	public Integer getRange() {
		return range;
	}

	public void setRange(Integer range) {
		this.range = range;
	}

	public Integer getGoodType() {
		return goodType;
	}

	public void setGoodType(Integer goodType) {
		this.goodType = goodType;
	}

	public Integer getOriginate() {
		return originate;
	}

	public void setOriginate(Integer originate) {
		this.originate = originate;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getExhibitionAmount() {
		return exhibitionAmount;
	}

	public void setExhibitionAmount(BigDecimal exhibitionAmount) {
		this.exhibitionAmount = exhibitionAmount;
	}

	public String getUniqueCode() {
		return uniqueCode;
	}

	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Integer getPresaleStatus() {
		return presaleStatus;
	}

	public void setPresaleStatus(Integer presaleStatus) {
		this.presaleStatus = presaleStatus;
	}

	/**
	 * 拣货单
	 */
	// private PickingReceipt pickingReceipt;

	public List<OrdersReceiptDetail> getOrdersReceiptDetails() {
		return ordersReceiptDetails;
	}

	public void setOrdersReceiptDetails(List<OrdersReceiptDetail> ordersReceiptDetails) {
		this.ordersReceiptDetails = ordersReceiptDetails;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getTransactionNo() {
		return transactionNo;
	}

	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public BigDecimal getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public BigDecimal getDeliveryFee() {
		return deliveryFee;
	}

	public void setDeliveryFee(BigDecimal deliveryFee) {
		this.deliveryFee = deliveryFee;
	}

	public Float getPackageWeight() {
		return packageWeight;
	}

	public void setPackageWeight(Float packageWeight) {
		this.packageWeight = packageWeight;
	}

	public Date getPaidTime() {
		return paidTime;
	}

	public void setPaidTime(Date paidTime) {
		this.paidTime = paidTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPickersId() {
		return pickersId;
	}

	public void setPickersId(String pickersId) {
		this.pickersId = pickersId;
	}

	public String getPackagerId() {
		return packagerId;
	}

	public void setPackagerId(String packagerId) {
		this.packagerId = packagerId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getLogisticsNo() {
		return logisticsNo;
	}

	public void setLogisticsNo(String logisticsNo) {
		this.logisticsNo = logisticsNo;
	}

	public String getPromotionInfo() {
		return promotionInfo;
	}

	public void setPromotionInfo(String promotionInfo) {
		this.promotionInfo = promotionInfo;
	}

	@Override
	public String toString() {
		return "OrdersReceipt{" + "id='" + id + '\'' + ", member=" + member + ", transactionNo='" + transactionNo + '\''
				+ ", orderType='" + orderType + '\'' + ", orderCode='" + orderCode + '\'' + ", shippingAddress='"
				+ shippingAddress + '\'' + ", actualAmount=" + actualAmount + ", totalPrice=" + totalPrice
				+ ", couponId='" + couponId + '\'' + ", paymentId='" + paymentId + '\'' + ", deliveryMethod="
				+ deliveryMethod + ", deliveryFee=" + deliveryFee + ", status=" + status + ", pickersId='" + pickersId
				+ '\'' + ", packagerId='" + packagerId + '\'' + ", remark='" + remark + '\'' + ", expirationTime="
				+ expirationTime + ", logisticsNo='" + logisticsNo + '\'' + ", packageWeight=" + packageWeight
				+ ", paidTime=" + paidTime + ", promotionInfo='" + promotionInfo + '\'' + ", deleted=" + deleted
				+ ", ordersReceiptDetails=" + ordersReceiptDetails + '}';
	}

	public Long getCombineId() {
		return combineId;
	}

	public void setCombineId(Long combineId) {
		this.combineId = combineId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
