/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.cms;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Link;
import com.yuruizhi.hplusdemo.admin.service.cms.LinkService;
import com.google.common.collect.ImmutableMap;
import org.ponly.common.validation.Groups;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;


/**
 * Created on 2016-02-22 16:16:32
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/link")
public class LinkController extends AdminBaseController<Link> {
    @Autowired
    private LinkService linkService;

    @ResponseBody
    @RequestMapping({"check", "{channel}/check"})
    public Boolean check(String code, String id) {
        Filters filters = Filters.create().eq("code", code).ne("id", id);
        return 1 > linkService.count(filters);
    }

    @Override
    public void input(Link link, Map<String, Object> model) {
        super.input(link, model);
         // model.put("links", linkService.findAll());
    }
    
    /*
    @RequestMapping("{channel}")
    public String _subindex(@PathVariable("channel") String code) {
    	return redirectViewPath(code + "/");
    }
    */

    @RequestMapping({"{channel}/", "{channel}/index"})
    public String subindex(@PathVariable("channel") String code, Map<String, Object> model) {
        Link limit = linkService.findOne("code", code);
        model.put("limit", limit);
    	return getRelativeViewPath("index");
    }
    
    @ResponseBody
    @RequestMapping("{channel}/list")
    public Page<Link> sublist(@PathVariable("channel") String code, Filters filters, Pageable pageable, Map<String, Object> model)  {
    	Link limit = linkService.findOne("code", code);
    	if (null == limit) {
    		return new SimplePage<Link>(Collections.<Link>emptyList());
    	}
    	/*
    	// 如果没有传递父 id 则为顶级
    	Filter filter = filters.getFilterFor("parent.id");
    	if (null == filter || Filters.Operator.NU.equals(filter.getOperator())) {
    		filters.remove("parent.id");
    		filters.eq("parent.id", limit.getId());
    	}
    	*/
    	// 考虑这两个条件是否也放入  前台
    /*	filters.beginsWith("path", limit.getPath() + ",");*/
    	//filters.lt("maxDepth", limit.getMaxDepth());
    	return super.list(filters, pageable);
    }
    
    @RequestMapping("{channel}/edit/{id}")
    public String _subedit(@PathVariable("channel") String code, @PathVariable("id") Long id, Map<String, Object> model) {
    	Link link = prepareModel(id);
    	model.put("link", link);
    	subinput(code, link, model);
    	return getRelativeViewPath("edit");
    }
    
    @RequestMapping({"{channel}/add"})
    public String subadd(@PathVariable("channel") String code, Link link, Map<String, Object> model) {
    	subinput(code, link, model);
    	return getRelativeViewPath("add");
    }

    @RequestMapping({"{channel}/edit"})
    public String subedit(@PathVariable("channel") String code, Link link, Map<String, Object> model) {
    	subinput(code, link, model);
    	return getRelativeViewPath("edit");
    }

    @ResponseBody
    @RequestMapping({"{channel}/delete"})
    public Map<String, Object> subdelete(@RequestParam("id") Long[] ids) {
        linkService.delete(ids);
        return ImmutableMap.<String, Object>of("success", true);
    }

    protected void subinput(String code, Link link, Map<String, Object> model) {
    	Link limit = linkService.findOne("code", code);
    	model.put("limit", limit);
    	Link parent = null != link ? link.getParent() : null;
    	if (null != parent && parent.equals(limit)) {
    		link.setParent(null);
    	}
    }
    
    @RequestMapping("{channel}/save")
    public String subsave(@PathVariable("channel") String code, @Validated(Groups.C.class) Link link, BindingResult binding, RedirectAttributes redirectAttrs) {
    	Link limit = linkService.findOne("code", code);
    	Link parent = link.getParent();
    	if (null == parent) {
    		link.setParent(limit);
    	}
        if (binding.hasErrors()) {
            /*
            if (binding.hasFieldErrors()) {
                return getRelativeViewPath("edit");
            }
            */
            return "forward:add";
        }
        crudService.save(link);
        return redirectViewPath(code + "/");
    }
    
    @RequestMapping("{channel}/update")
    public String subupdate(@PathVariable("channel") String code, @Validated(Groups.U.class) Link link, BindingResult binding, RedirectAttributes redirectAttrs) {
    	Link limit = linkService.findOne("code", code);
    	Link parent = link.getParent();
    	if (null == parent) {
    		link.setParent(limit);
    	}
        if (binding.hasErrors()) {
            // binding.getGlobalErrors().
            return "forward:edit";
        }
        crudService.update(link);
        return redirectViewPath(code + "/");
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Link.class, "parent", new CustomStringBeanPropertyEditor(Link.class));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }
}
