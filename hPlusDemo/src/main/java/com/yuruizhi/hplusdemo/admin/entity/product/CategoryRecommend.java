/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: CategoryRecommend</p>
 * <p>说明: 产品分类推荐实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:19:36   
 * @version: 1.0
 */
public class CategoryRecommend extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 8991773239111752290L;

	/**
     * 推荐类型：首页商品推荐/首页品牌推荐/列表页热卖推荐/列表页推广商品
     * @Column
     */
    private Integer type;

    /**
     * 品类实体对象
     * @ManyToOne optional=true
     */
    private Category category;

    /**
     * 推荐的品牌实体对象
     * @ManyToOne optional=true
     */
    private Brand brand;

    /**
     * 单品实体对象
     * @ManyToOne optional=true
     */
    @JsonUnwrapped(prefix = "goods.")
    private Goods goods;

    /**
     * 删除标识符
     * @Column
     */
    /**
     * 是否可见/显示
     * @Column
     */
    private Boolean visible = true;
    /**
     * 排序
     * @Column
     */
    private Integer sort;
    /*位置*/
    private Integer location;

    /*类别外键*/
    /*单品外键*/

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getLocation() {
        return location;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    private Boolean deleted = Boolean.FALSE;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
