/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.dto;

import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * <p>名称: ProductDTO</p>
 * <p>说明: 产品DTO</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:56:32   
 * @version: 1.0
 */
public class ProductDTO extends Product {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -2052829339706908588L;
	private List<Compose> composes = Lists.newArrayList();
    private List<Goods> goodsList = Lists.newArrayList();

    public List<Goods> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<Goods> goodsList) {
        this.goodsList = goodsList;
    }


    public List<Compose> getComposes() {
        return composes;
    }

    public void setComposes(List<Compose> composes) {
        this.composes = composes;
    }

    /**
     * 组合 一个商品和另外N个配件
     */
    public static class Compose {
        private String name;
        private String productId;
        private List<Accessory> accessories;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public List<Accessory> getAccessories() {
            return accessories;
        }

        public void setAccessories(List<Accessory> accessories) {
            this.accessories = accessories;
        }
    }

    // 配件
    public static class Accessory {
        private String productId;
        private String goodsId;

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(String goodsId) {
            this.goodsId = goodsId;
        }
    }
}