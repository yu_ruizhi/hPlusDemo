/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: SpecValidate.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.validate
 * Date: 2016年8月18日下午1:09:28
 **/
package com.yuruizhi.hplusdemo.admin.validate;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Spec;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.google.common.collect.Maps;

public class SpecValidate {

	public static Map<String,Object> specValidate(Spec spec,String[] data,String error,int i,Boolean success){
		Map<String,Object> map = Maps.newHashMap();
		if ( 7 != data.length) {
			error += "第"+(i+1)+"行数据导入失败，原因为字段数量不符合<br>";
			success = false;
			map.put("spec", spec);
			map.put("success", success);
			map.put("error", error);
			return map;
		}
		//别名
		if (null != data[1] && !"".equals(data[1])) {
			if (250 < data[1].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为别名超过250个字符<br>";
				success = false;
			}
			spec.setAlias(data[1]);
		}
		//排序
		if (null != data[2] && !"".equals(data[2])) {
			try{
				if (250 < data[2].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为排序超过250个字符<br>";
					success = false;
				}
				spec.setSort(Integer.valueOf(data[2]));
			} catch(Exception e) {
				error += "第"+(i+1)+"行数据导入失败，原因为排序格式错误<br>";
				success = false;
			}
		}
		//是否图片
		if (null != data[3] && !"".equals(data[3])) {
			if ("是".equals(data[3])) {
				spec.setDisplayType(1);
			} else {
				spec.setDisplayType(0);
			}
		}
		map.put("spec", spec);
		map.put("error", error);
		map.put("success", success);
		return map;
	}
	
	public static Map<String,Object> specValueValidate(SpecValue specValue,String[] data,String error,int i,Boolean success) {
		Map<String,Object> map = Maps.newHashMap();
		if (null == specValue) {
			specValue = new SpecValue();
		}
			//规格名称
		if (null != data[4] && !"".equals(data[4])) {
			if (250 < data[4].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为规格名称超过250个字符<br>";
				success = false;
			}
			specValue.setName(data[4]);
		}
		//规格编码
		if (null != data[5] && !"".equals(data[5])) {
			if (250 < data[5].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为规格编码超过250个字符<br>";
				success = false;
			}
			specValue.setCode(data[5]);
		}
		//规格排序
		if (null != data[6] && !"".equals(data[6])) {
			try{
				if (250 < data[6].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为规格排序超过250个字符<br>";
					success = false;
				}
				specValue.setSort(Integer.valueOf(data[6]));
			} catch (Exception e) {
				error += "第"+(i+1)+"行数据导入失败，原因为规格排序超过250个字符<br>";
				success = false;
			}
		}
		map.put("specValue", specValue);
		map.put("error", error);
		map.put("success", success);
		return map;
	}
	
}
