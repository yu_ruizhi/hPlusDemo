package com.yuruizhi.hplusdemo.admin.controller.logistics;


import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModule;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryModuleDetailService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryModuleService;
import com.google.common.collect.ImmutableMap;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

import static com.yuruizhi.hplusdemo.admin.common.Constant.ADMIN_PREFIX;

/**
 * Created by ASUS on 2016/7/14.
 */
@Controller
@RequestMapping(ADMIN_PREFIX + "/deliveryModule")
public class AdminDeliveryModuleController {

    @Autowired
    private DeliveryModuleService deliveryModuleService;
    @Autowired
    private DeliveryModuleDetailService deliveryModuleDetailService;
    @RequestMapping("add")
    public String add(){
        return "deliveryModule/add";
    }

    @RequestMapping("index")
    public String index(){
        return "deliveryModule/index";
    }

    @ResponseBody
    @RequestMapping("save")
    public boolean save(DeliveryModule deliveryModule){
        deliveryModuleService.saveModule(deliveryModule);
        return true;
    }

    @ResponseBody
    @RequestMapping("list")
    public Page<DeliveryModule> list(Filters filters, Pageable pageable){
        return deliveryModuleService.findPage(filters, pageable);
    }

    @RequestMapping("edit/{id}")
    public String edit(@PathVariable(value = "id") Long id, Map<String, Object> model){
        DeliveryModule deliveryModule = deliveryModuleService.find(id);
        model.put("deliveryModule", deliveryModule);
        return "deliveryModule/edit";
    }

    @ResponseBody
    @RequestMapping("update")
    public boolean update(DeliveryModule deliveryModule){
        deliveryModuleService.updateModule(deliveryModule);
        return true;
    }
    @RequestMapping("delete")
    @ResponseBody
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
        deliveryModuleService.delete(id);
        for (Long iid : id) {
            deliveryModuleDetailService.deleteByModuleId(iid);
        }
        return ImmutableMap.<String, Object>of("success", true);
    }
//
//    @ResponseBody
//    @RequestMapping("listAll")
//    public List<DeliveryModule> listAll(){
//        return deliveryModuleService.findAll();
//    }
}
