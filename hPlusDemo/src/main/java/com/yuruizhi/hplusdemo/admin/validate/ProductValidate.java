/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductValidate.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.validate
 * Date: 2016年8月18日下午1:09:59
 **/
package com.yuruizhi.hplusdemo.admin.validate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.google.common.collect.Maps;

public class ProductValidate {

		public static Map<String,Object> productValidate(Product product,String[] data,String error,int i,Boolean productSave) {
			Map<String,Object> map = Maps.newHashMap();
			
			//商品编号
			if (null != data[1] && !"".equals(data[1])) {
				if (250 < data[1].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为商品编号超过250个字符<br>";
					productSave = false;
				}
				product.setCode(data[1]);
			}
			//商品状态
			if (null != data[2] && !"".equals(data[2])) {
				setProductStatus(product, data[2]);
			}
			
			//入箱数
			if (null != data[6] && !"".equals(data[6])) {
				try {
					product.setBoxNumber(Integer.valueOf(data[6]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为入箱数格式错误<br>";
					productSave = false;
				}
			}
			//日元成本价
			if (null != data[7] && !"".equals(data[7])) {
				try {
					product.setPrice(BigDecimal.valueOf(Double.valueOf(data[7])));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为日元成本价格式错误<br>";
					productSave = false;
				}
			}
			//发售日期
			if (null != data[8] && !"".equals(data[8])) {
				try {
					Date date = DateUtils.str2Date(data[8], "yyyy/MM/dd");
					product.setRegisterDate(date);
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为发售日期格式错误<br>";
					productSave = false;
				}
			}
			//是否再贩
			if (null != data[9] && !"".equals(data[9])) {
				if ("是".equals(data[9])) {
					product.setIsReSelling(true);
				} else {
					product.setIsReSelling(false);
				}
			}
			//再贩次数
			if (null != data[10] && !"".equals(data[10])) {
				try {
					product.setReSellingNum(Integer.valueOf(data[10]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为再贩次数格式错误<br>";
					productSave = false;
				}
			}
			//设计师
			if (null != data[11] && !"".equals(data[11])) {
				if (250 < data[11].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为设计师超过250个字符<br>";
					productSave = false;
				}
				product.setDesigner(data[11]);
			}
			//日语名称
			if (null != data[12] && !"".equals(data[12])) {
				if (250 < data[12].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为日语名称超过250个字符<br>";
					productSave = false;
				}
				product.setNameJp(data[12]);
			}
			//购买范围
			if (null != data[13] && !"".equals(data[13])) {
				setRetailAndDealer(product, data[13]);
			}
			//排序
			if (null != data[14] && !"".equals(data[14])) {
				try {
					product.setSort(Integer.valueOf(data[14]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为排序格式错误<br>";
					productSave = false;
				}
			}
			//是否启用规格
			if (null != data[17] && !"".equals(data[17])) {
				if ("是".equals(data[17])) {
					product.setEnableSpecs(true);
				} else {
					product.setEnableSpecs(false);
				}
			}
			map.put("product", product);
			map.put("error", error);
			map.put("productSave", productSave);
			return map;
		}
	
		public static Map<String,Object> goodsValidate(String[] data,String error,Boolean goodsSave,Goods goods,int i) {
			Map<String,Object> map = Maps.newHashMap();
			//商品标题
			if (null != data[19] && !"".equals(data[19])) {
				if (250 < data[19].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为商品标题超过250个字符<br>";
					goodsSave = false;
				}
				goods.setTitle(data[19]);
			}
			//商品编号
			if (null != data[20] && !"".equals(data[20])) {
				if (250 < data[20].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为商品编号超过250个字符<br>";
					goodsSave = false;
				}
				goods.setCode(data[20]);
			}
			//日元成本价
			if (null != data[22] && !"".equals(data[22])) {
				try {
					goods.setPrice(BigDecimal.valueOf(Double.valueOf(data[22])));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为日元成本价格式错误<br>";
					goodsSave = false;
				}
			}
			//库存
			if (null != data[23] && !"".equals(data[23])) {
				try {
					goods.setStock(Long.valueOf(data[23]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为库存格式错误<br>";
					goodsSave = false;
				}
			}
			//限购数量
			if (null != data[24] && !"".equals(data[24])) {
				try {
					goods.setReserveNum(Integer.valueOf(data[24]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为限购数量格式错误<br>";
					goodsSave = false;
				}
			}
			//预定截止时间
			if (null != data[25] && !"".equals(data[25])) {
				try {
					Date date = DateUtils.str2Date(data[25], "yyyy/MM/dd");
					goods.setReserveDeadline(date);
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为预定截止时间格式错误<br>";
					goodsSave = false;
				}
			}
			//重量
			if (null != data[26] && !"".equals(data[26])) {
				try {
					goods.setWeight(Float.valueOf(data[26]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为重量格式错误<br>";
					goodsSave = false;
				}
			}
			//库存预警
			if (null != data[27] && !"".equals(data[27])) {
				try {
					goods.setStockForewarn(Long.valueOf(data[27]));
				} catch (Exception e) {
					error += "第"+(i+1)+"行数据导入失败，原因为库存预警格式错误<br>";
					goodsSave = false;
				}
			}
			//商品定位
			if (null != data[28] && !"".equals(data[28])) {
				setTrend(goods, data[28]);
			}
			map.put("goods", goods);
			map.put("error", error);
			map.put("goodsSave", goodsSave);
			return map;
		}
		
		/**
	     * 
	     * <p>名称：根据String字符串获取商品状态</p> 
	     * <p>描述：</p>
	     * @author：wuqi
	     * @param product
	     * @param status
	     * @return
	     */
	    private static Product setProductStatus (Product product,String status) {
	    	if (status.indexOf("预定") >= 0) {
	    		product.setReserved(true);
	    	}
	    	if (status.indexOf("限购") >= 0) {
	    		product.setLimited(true);
	    	}
	    	if (status.indexOf("现货") >= 0) {
	    		product.setSpot(true);
	    	}
	    	return product;
	    }
	    
	    private static Product setRetailAndDealer (Product product,String str) {
	    	if (str.indexOf("零售") >= 0) {
	    		product.setRetail(true);
	    	}
	    	if (str.indexOf("经销商") >= 0) {
	    		product.setDealer(true);
	    	}
	    	return product;
	    }
		
	    
	    private static Goods setTrend (Goods goods,String str) {
	    	if ("无差别".equals(str)) {
	    		goods.setTrend(0);
	    	}
	    	if ("趋向男性".equals(str)) {
	    		goods.setTrend(1);
	    	}
	    	if ("趋向女性".equals(str)) {
	    		goods.setTrend(2);
	    	}
	    	return goods;
	    }
}
