/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: KeywordService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.platform
 * Date: 2016年4月23日 下午1:37:02 
 **/
package com.yuruizhi.hplusdemo.admin.service.platform;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.platform.Keyword;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称:  搜索管理 关键字Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月23日 下午1:37:02   
 * @version: 1.0
 */
public interface KeywordService extends BaseService<Keyword> {
	
	/**
	 * 
	 * <p>名称：检查同名关键词是否存在</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param name 关键字
	 * @return
	 */
	Map<String, Boolean> checkIsKeywordExists(String name);

	/**
	 * 
	 * <p>名称：检查除此关键词外是否还存在同名的关键词</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param id 当前关键词的ID
	 * @param name 当前的关键词
	 * @return
	 */
	Boolean checkIsAnotherKeywordExists(Long id, String name);

}
