/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: AttributeValue</p>
 * <p>说明: 产品属性值实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:17:12   
 * @version: 1.0
 */
public class AttributeValue extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -6354348859108654998L;
	/**
     * 属性值
     * @Column
     */
    private String value;
    /**
     * 属性值编码
     * @Column
     */
    private String code;
    /**
     * 属性值排序
     * @Column
     */
    private Integer sort;
    /**
     * 是否删除
     * @Column
     */
    private Boolean deleted = false;
    /**
     * 所属属性
     * @ManyToOne
     */
    private Attribute attribute;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
}
