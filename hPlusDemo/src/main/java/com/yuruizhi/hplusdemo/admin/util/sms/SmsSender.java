/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util.sms;

/**
 * <p>名称: SmsSender</p>
 * <p>说明: 邮件发送</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:29:07   
 * @version: 1.0
 */
public interface SmsSender {


	/**
	 * <p>名称：sendSms</p> 
	 * <p>描述：发送短信</p>
	 * @author：ChenGuang
	 * @param mobile
	 * @param content
	 * @return
	 */
    boolean sendSms(String mobile, String content);

    /**
     * <p>名称：sendSms</p> 
     * <p>描述：发送短信</p>
     * @author：ChenGuang
     * @param mobiles
     * @param content
     * @return
     */
    boolean sendSms(String[] mobiles, String content);

}
