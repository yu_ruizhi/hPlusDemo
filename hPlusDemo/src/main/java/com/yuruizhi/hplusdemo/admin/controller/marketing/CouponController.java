/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Coupon;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CouponType;
import com.yuruizhi.hplusdemo.admin.service.marketing.CouponService;
import com.yuruizhi.hplusdemo.admin.service.marketing.CouponTypeService;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created on 2016-03-30 11:47:01
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/coupon")
public class CouponController extends AdminBaseController<Coupon> {
    @Autowired
    private CouponTypeService couponTypeService;

    @Autowired
    private CouponService couponService;
    
    @Override
    public void input(Coupon coupon, Map<String, Object> model) {
        super.input(coupon, model);
        model.put("couponTypes", couponTypeService.findAll());
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
    	binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
        binder.registerCustomEditor(CouponType.class, "couponType", new CustomStringBeanPropertyEditor(CouponType.class));
    }
    
    @RequestMapping("couponSend")
    public String couponSend(Map<String, Object> model){
    	model.put("couponTypes", couponTypeService.findAll());
    	return getRelativeViewPath("couponSend");
    }
    
    
    @RequestMapping("couponSendSave")
    public String couponSave(Long[] memberIds,Coupon coupon){
    	couponService.couponSend(memberIds,coupon);
    	
    	return redirectViewPath("index");
    	
    }
    
    @RequestMapping("batchSave")
    public String batchSave(Coupon coupon,int createCount){
    	couponService.batchSave(coupon,createCount);
    	
    	return redirectViewPath("index");
    	
    }
    @Override
    @RequestMapping("edit/{id}")
    public String _compat(@PathVariable("id") Long id, Map<String, Object> model) {
    	
    	List<CouponType> couponTypes = couponTypeService.findAll();
    	Coupon coupon = couponService.find(id);
    	model.put("couponTypes", couponTypes);
    	model.put("coupon", coupon);
        return getRelativeViewPath("edit");
    }
    
}
