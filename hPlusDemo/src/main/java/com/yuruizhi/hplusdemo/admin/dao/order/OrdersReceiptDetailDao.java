package com.yuruizhi.hplusdemo.admin.dao.order;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;

import java.util.Map;


public interface OrdersReceiptDetailDao extends BaseDao<OrdersReceiptDetail> {
    
    
    int updateDetailStatus(String id, String status);

    OrdersReceiptDetail getOrdersReceiptDetailByOrderId(Long orderId);

    void updateRetainageTime(Map<String,Object> map);

}
