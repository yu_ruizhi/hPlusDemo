/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.cms;

import org.ponly.common.validation.Groups;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Channel;
import com.yuruizhi.hplusdemo.admin.service.cms.ChannelService;
import com.google.common.collect.ImmutableMap;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>名称: HelpChannelController</p>
 * <p>说明: 帮助中心栏目管理Controller</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月4日下午3:32:42   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/helpChannel")
public class HelpChannelController extends AdminBaseController<Channel> {
    @Autowired
    private ChannelService channelService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Channel.class, "parent", new CustomStringBeanPropertyEditor(Channel.class));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }
    
    /**
     * <p>名称：subinput</p> 
     * <p>描述：TODO</p>
     * @author：ChenGuang
     * @param code
     * @param link
     * @param model
     */
    protected void subinput(String code, Channel link, Map<String, Object> model) {
    	Channel limit = channelService.findOne("code", code);
    	model.put("limit", limit);
    	Channel parent = null != link ? link.getParent() : null;
    	if (null != parent && parent.equals(limit)) {
    		link.setParent(null);
    	}
    }
    
    @Override
    public void input(Channel channel, Map<String, Object> model) {
        super.input(channel, model);
        //model.put("channels", channelService.findAll());
    }
    
    
    @RequestMapping({"{channel}/", "{channel}/index"})
    public String subindex(@PathVariable("channel") String code, Map<String, Object> model) {
        Channel limit = channelService.findOne("code", code);
        model.put("limit", limit);
    	return getRelativeViewPath("index");
    }
    
    /**
     * <p>名称：sublist</p> 
     * <p>描述：列表页数据展示</p>
     * @author：ChenGuang
     * @param code
     * @param filters
     * @param pageable
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping("{channel}/list")
    public Page<Channel> sublist(@PathVariable("channel") String code, Filters filters, Pageable pageable, Map<String, Object> model)  {
    	Channel limit = channelService.findOne("code", code);
    	if (null == limit) {
    		return new SimplePage<Channel>(Collections.<Channel>emptyList());
    	}
    	/*
    	// 如果没有传递父 id 则为顶级
    	Filter filter = filters.getFilterFor("parent.id");
    	if (null == filter || Filters.Operator.NU.equals(filter.getOperator())) {
    		filters.remove("parent.id");
    		filters.eq("parent.id", limit.getId());
    	}
    	*/
    	// 考虑这两个条件是否也放入  前台
    	filters.beginsWith("path", limit.getPath() + ",");
    	// filters.lt("maxDepth", limit.getMaxDepth());
    	return super.list(filters, pageable);
    }
    
    /**
     * <p>名称：_subedit</p> 
     * <p>描述：进入编辑页面</p>
     * @author：ChenGuang
     * @param code
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("{channel}/edit/{id}")
    public String _subedit(@PathVariable("channel") String code, @PathVariable("id") Long id, Map<String, Object> model) {
    	Channel channel = prepareModel(id);
    	model.put("channel", channel);
    	subinput(code, channel, model);
    	return getRelativeViewPath("edit");
    }
    
    @Override
    public String _compat(@PathVariable("id") Long id, Map<String, Object> model) {
    	
    	Channel channel = channelService.find(id);
    	@SuppressWarnings("unused")
		Channel limit = null;
    	if(null!=channel.getParent()){
    		limit = channelService.find(channel.getParent().getId());
    		//model.put("limit", limit);
    	}
    	
    	// TODO null
    	model.put("channel", channel);
    	
    	return getRelativeViewPath("edit");
    }
    
    /**
     * <p>名称：addSubchannel</p> 
     * <p>描述：进入栏目添加页面</p>
     * @author：ChenGuang
     * @param code
     * @param model
     * @return
     */
    @RequestMapping("{channel}/add")
    public String addSubchannel(@PathVariable("channel") String code, Map<String, Object> model) {
    	model.put("limit", channelService.findOne("code", code));
    	return getRelativeViewPath("add");
    }
    
    /**
     * <p>名称：subupdate</p> 
     * <p>描述：修改栏目内容</p>
     * @author：ChenGuang
     * @param code
     * @param channel
     * @param binding
     * @param redirectAttrs
     * @return
     */
    @RequestMapping("{channel}/update")
    public String subupdate(@PathVariable("channel") String code, @Validated(Groups.U.class) Channel channel, BindingResult binding, RedirectAttributes redirectAttrs) {
    	Channel limit = channelService.findOne("code", code);
    	Channel parent = channel.getParent();
    	if (null == parent) {
    		channel.setParent(limit);
    	}
        if (binding.hasErrors()) {
            // binding.getGlobalErrors().
            return "forward:edit";
        }
        channel.setLastModifiedDate(new Date());
        crudService.update(channel);
        return redirectViewPath(code + "/");
    }
    
    /**
     * <p>名称：subdelete</p> 
     * <p>描述：删除栏目信息</p>
     * @author：ChenGuang
     * @param code
     * @param ids
     * @return
     */
    @ResponseBody
    @RequestMapping({"{channel}/delete"})
    public Map<String, Object> subdelete(@RequestParam("id") Long[] ids) {
        channelService.delete(ids);
        return ImmutableMap.<String, Object>of("success", true);
    }
    
    
    /**
     * <p>名称：subsave</p> 
     * <p>描述: 栏目保存处理</p>
     * @author：ChenGuang
     * @param code
     * @param channel
     * @param binding
     * @param redirectAttrs
     * @return
     */
    @RequestMapping("{channel}/save")
    public String subsave(@PathVariable("channel") String code, @Validated(Groups.C.class) Channel channel, BindingResult binding, RedirectAttributes redirectAttrs) {
    	Channel limit = channelService.findOne("code", code);
    	Channel parent = channel.getParent();
    	if (null == parent) {
    		channel.setParent(limit);
    	}
        if (binding.hasErrors()) {
            /*
            if (binding.hasFieldErrors()) {
                return getRelativeViewPath("edit");
            }
            */
            return "forward:add";
        }
        channel.setCreatedDate(new Date());
        crudService.save(channel);
        return redirectViewPath(code + "/");
    }
    
}
