package com.yuruizhi.hplusdemo.admin.dao.cms;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.LeaveMessage;

/**
 * Created by ASUS on 2014/12/8.
 */
public interface LeaveMessageDao extends BaseDao<LeaveMessage> {

}
