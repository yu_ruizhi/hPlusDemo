package com.yuruizhi.hplusdemo.admin.util;

/**
 * Created by 于瑞智 on 2016/7/20.
 */
import org.ponly.fs.FileSystem;
import org.ponly.fs.FileSystems;
import org.ponly.fs.FileView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class transferImg {
    public static void main(String[] args) {

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String url = null;
        String user = null;
        String password = null;
        String sql = null;
        List<String> imgs = new ArrayList<String>();
        try {
            Class.forName("com.mysql.jdbc.Driver"); //加载mysq驱动
        } catch (ClassNotFoundException e) {
            System.out.println("驱动加载错误");
            e.printStackTrace();//打印出错详细信息
        }
        try {
            url = "jdbc:mysql://121.40.102.225:3306/AM_ESHOP?characterEncoding=UTF-8";
            user = "aimon";
            password = "aimon@glanway";
            conn = DriverManager.getConnection(url,user,password);
        } catch (SQLException e) {
            System.out.println("数据库链接错误");
            e.printStackTrace();
        }
        try {
            stmt = conn.createStatement();
            sql = "SELECT * FROM T_GOODS WHERE CREATED_BY = 'system' LIMIT 0  ,9";//dept这张表有deptno，deptname和age这三个字段
            rs = stmt.executeQuery(sql);//执行sql语句
            while(rs.next()) {
                System.out.println(rs.getString("image"));
                imgs.add(rs.getString("image"));
            }

            //imgs导入
            CopySingleFileTo(imgs);

        } catch (SQLException e) {
            System.out.println("数据操作错误");
            e.printStackTrace();
        }

        //关闭数据库
        try {
            if(rs != null) {
                rs.close();
                rs = null;
            }
            if(stmt != null) {
                stmt.close();
                stmt = null;
            }
            if(conn != null) {
                conn.close();
                conn = null;
            }
        } catch(Exception e) {
            System.out.println("数据库关闭错误");
            e.printStackTrace();
        }
    }

    public static void CopySingleFileTo(List<String> imgs) {
        try {

            String fsUri = "local:///d:/oku/";
            FileSystem fs = FileSystems.getFileSystem(fsUri);  // 创建一个基于本地文件系统, 以 d:/fs 为根目录的文件系统

            String fsUriNew = "aliyun://KudDSWR2yHbMYrBR:chHHYo2Q6HLPH7CkrfjqjWYq5jnT6S@aimon.oss-cn-shanghai.aliyuncs.com";  //新：艾漫阿里云OSS
            FileSystem fsNew = FileSystems.getFileSystem(fsUriNew);

            /*FileView[] fileViews = fsNew.ls(fsUriNew);
            for(FileView fv : fileViews){
                System.out.println(fv.getPath());
            }*/

            for(String img : imgs){

                if(fs.exists(img)){  //获取给定的文件是否存在，如果存在

                    try {

                        System.out.println("本地地址是否存在1.jpg：" + fs.exists(img));

                        InputStream in = fs.open("1.jpg"); // 读取 1.jpg 文件内容

                        OutputStream opt = fsNew.create("fs/4.jpg", true);  //在该文件系统中创建一个 fileName, 如果存在则覆盖

                        try {
                            int val = -1;
                            while ((val = in.read()) != -1) {
                                opt.write(val);
                            }
                            opt.flush();
                            opt.close();
                            in.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        System.out.println("OSS地址3.jpg是否创建成功：" + fsNew.exists("fs/4.jpg"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}