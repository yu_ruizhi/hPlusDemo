/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.cms;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Article;
import com.yuruizhi.hplusdemo.admin.entity.cms.ArticleTag;
import com.yuruizhi.hplusdemo.admin.entity.cms.Channel;
import com.yuruizhi.hplusdemo.admin.entity.cms.Tag;
import com.yuruizhi.hplusdemo.admin.service.cms.ArticleService;
import com.yuruizhi.hplusdemo.admin.service.cms.ArticleTagService;
import com.yuruizhi.hplusdemo.admin.service.cms.ChannelService;
import com.yuruizhi.hplusdemo.admin.service.cms.TagService;
import com.google.common.collect.ImmutableMap;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>名称: ArticleController</p>
 * <p>说明: cms管理控制器</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月4日下午3:29:43   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/article")
public class ArticleController extends AdminBaseController<Article> {
    @Autowired
    private ChannelService channelService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private TagService tagService;

    @Autowired
    private ArticleTagService articleTagService;

    @Override
    public void input(Article article, Map<String, Object> model) {
        super.input(article, model);
        model.put("channels", channelService.findAll());
    }

    /**
     * <p>名称：initBinder</p> 
     * <p>描述：</p>
     * @author：ChenGuang
     * @param binder
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Channel.class, "channel", new CustomStringBeanPropertyEditor(Channel.class));
    }
    
    /**
     * <p>名称：basicIndex</p> 
     * <p>描述：跳转到类表页</p>
     * @author：ChenGuang
     * @param code
     * @param model
     * @return
     */
    @RequestMapping({"{code}/index", "{code}/"})
    public String basicIndex(@PathVariable("code") String code, Map<String, Object> model) {
        model.put("channel", channelService.findOne("code", code));
        if (code.equals("problem")) {
            return getRelativeViewPath("problem/index");
        } else if (code.equals("help")) {
            return getRelativeViewPath("help/index");
        }else if(code.equals("notice")){
            return getRelativeViewPath("notice/index");
        }else if(code.equals("info")){
                return getRelativeViewPath("info/index");
        }else if(code.equals("foot")){
            return getRelativeViewPath("foot/index");
        }

        return getRelativeViewPath("articleBasic/index");
    }

    /**
     * <p>名称：list</p> 
     * <p>描述：查询分页列表</p>
     * @author：ChenGuang
     * @param filters
     * @param pageable
     * @param code
     * @return
     */
    @ResponseBody
    @RequestMapping("{code}/list")
    public Page<Article> list(Filters filters, Pageable pageable, @PathVariable("code") String code) {
        Channel channel = channelService.findOne("code", code);
        if (null == channel) {
            return new SimplePage<Article>(Collections.<Article>emptyList());
        }

        filters.beginsWith("channel.path", channel.getPath());
        return articleService.findPage(filters, pageable);
    }


    /**
     * 跳转到增加页面
     */
    @RequestMapping("{code}/add")
    public String add(Map<String, Object> model, @PathVariable("code") String code) {
        model.put("limit", channelService.findOne("code", code));
        model.put("code", code);
        if (code.equals("problem")) {
            return getRelativeViewPath("problem/add");
        } else if (code.equals("help")) {
            return getRelativeViewPath("help/add");
        }else if(code.equals("notice")){
            return getRelativeViewPath("notice/add");
        }else if(code.equals("info")){
            return getRelativeViewPath("info/add");
        }else if(code.equals("foot")){
            return getRelativeViewPath("foot/add");
        }
        return getRelativeViewPath("articleBasic/add");

    }


    /**
     * <p>名称：save</p> 
     * <p>描述：新增操作</p>
     * @author：ChenGuang
     * @param article
     * @param code
     * @return
     */
    
    @RequestMapping("{code}/save")
    public String save(Article article, @PathVariable("code") String code) {
        if (null == article.getChannel()) {
            article.setChannel(channelService.findOne("code", code));
        }
        article.setCreatedDate(new Date());
        if (true == article.getPublished()) {
            article.setPublishTime(new Date());
        }
        articleService.save(article);
        if (null != article.getArtTagName()&&!article.getArtTagName().trim().equals("")) {
            String[] tagArr = article.getArtTagName().split(",");
            for (String tarr : tagArr) {
                Tag newTag = tagService.findAndSave(tarr);//如存在返回对应标签，不存在保存后返回
                ArticleTag artTag = new ArticleTag();
                artTag.setArticleId(article.getId());
                artTag.setTagId(newTag.getId());
                artTag.setDeleted(false);
                articleTagService.save(artTag);
            }
        }
        return redirectViewPath(code + "/index");
    }


    /**
     * <p>名称：delete</p> 
     * <p>描述：删除操作</p>
     * @author：ChenGuang
     * @param id
     * @param code
     * @return
     */
    @ResponseBody
    @RequestMapping("{code}/delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
        articleService.delete(id);
        return ImmutableMap.<String, Object>of("success", true);
    }

    
    /**
     * <p>名称：edit</p> 
     * <p>描述：跳转到编辑页面</p>
     * @author：ChenGuang
     * @param id
     * @param code
     * @param model
     * @return
     */
    @RequestMapping("{code}/edit/{id}")
    public String edit(@PathVariable(value = "id") Long id, @PathVariable("code") String code, Map<String, Object> model) {
        model.put("limit", channelService.findOne("code", code));
//		ModelAndView mav = new ModelAndView();
        Article article = articleService.find(id);
        model.put("article", article);
        List<Tag> artTags = tagService.findByArtId(id);
        model.put("artTags", artTags);
        model.put("limit", channelService.findOne("code", code));
        model.put("code", code);
//		mav.setViewName(getRelativeViewPath(code+"/edit"));
        if (code.equals("problem")) {
            return getRelativeViewPath("problem/edit");
        } else if (code.equals("help")) {
            return getRelativeViewPath("help/edit");
        }else if(code.equals("notice")){
            return getRelativeViewPath("notice/edit");
        }else if(code.equals("info")){
            return getRelativeViewPath("info/edit");
        }else if(code.equals("foot")){
            return getRelativeViewPath("foot/edit");
        }
        return getRelativeViewPath("articleBasic/edit");
    }

    /**
     * <p>名称：update</p> 
     * <p>描述：修改操作</p>
     * @author：ChenGuang
     * @param article
     * @param code
     * @return
     */
    @RequestMapping("{code}/update")
    public String update(Article article, @PathVariable("code") String code) {
        Boolean published = article.getPublished();
        if (null != published && published) {
            article.setPublishTime(new Date());
        }
        if(article.getArtTagName() != null&&article.getArtTagName().trim().equals("")){
        	articleTagService.delByArtId(article.getId());
        }
        
        if (null != article.getArtTagName()&&!article.getArtTagName().trim().equals("")) {
            articleTagService.delByArtId(article.getId());
            String[] tagArr = article.getArtTagName().split(",");
            for (String tarr : tagArr) {
                Tag newTag = tagService.findAndSave(tarr);//如存在返回对应标签，不存在保存后返回
                ArticleTag artTag = new ArticleTag();
                artTag.setArticleId(article.getId());
                artTag.setTagId(newTag.getId());
                artTag.setDeleted(false);
                articleTagService.save(artTag);
            }
        }
        articleService.update(article);
        return redirectViewPath(code + "/index");
    }
}
