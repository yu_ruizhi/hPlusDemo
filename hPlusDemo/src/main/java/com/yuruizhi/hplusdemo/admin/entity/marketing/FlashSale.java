/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

/**
 * <p>名称: FlashSale</p>
 * <p>说明: 显示抢购(限时低价/打折)实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:03:49   
 * @version: 1.0
 */
public class FlashSale extends Promotion {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 4268897758554656899L;

	/**
     * 活动范围: 不同宝贝不同折扣
     */
    public static final Integer TARGET_DIFF_DISCOUNT = 1;

    /**
     * 活动标签
     * 例如: 新年特卖
     *
     * @ViewField editor=input
     * @Column TAG
     */
    @Size(max = 5)
    private String tag;

    /**
     * 优惠限制(限量)
     *
     * @ViewField editor=spinner
     * @Column LIMIT_QTY
     */
    private Integer limitQty;

    /**
     * 活动范围
     * 所有宝贝统一折扣,不同宝贝不同折扣
     * 当前只支持不同宝贝不同折扣
     *
     * @ViewField editor=spinner
     * @Column TARGET
     */
    @NotNull
    private Integer target = TARGET_DIFF_DISCOUNT;

    /**
     * 是否参与满减
     *
     * @ViewField editor=input
     * @Column ALLOW_DEDUCTION
     */
    @NotNull
    private Boolean allowDeduction;

    /**
     * 优惠方式, 当活动范围为统一折扣时有效
     * 折扣/减价
     *
     * @ViewField editor=spinner
     * @Column WAY
     */
//    @NotNull
    private Integer way;

    /**
     * 折扣, 当活动范围为统一折扣时有效
     *
     * @ViewField editor=input
     * @Column DISCOUNT
     */
//    @Size(max = 8)
//    @Digits(integer = 8, fraction = 2)
    private BigDecimal discount;

    /**
     * 减价, 当活动范围为统一折扣时有效
     *
     * @ViewField editor=input
     * @Column DESCEND
     */
//    @Size(max = 8)
//    @Digits(integer = 8, fraction = 2)
    private BigDecimal descend;

    private List<FlashSaleDetail> flashSaleDetails;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getLimitQty() {
        return limitQty;
    }

    public void setLimitQty(Integer limitQty) {
        this.limitQty = limitQty;
    }

    public Integer getTarget() {
        return target;
    }

    public void setTarget(Integer target) {
        this.target = target;
    }

    public Boolean getAllowDeduction() {
        return allowDeduction;
    }

    public void setAllowDeduction(Boolean allowDeduction) {
        this.allowDeduction = allowDeduction;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDescend() {
        return descend;
    }

    public void setDescend(BigDecimal descend) {
        this.descend = descend;
    }

    public List<FlashSaleDetail> getFlashSaleDetails() {
        return flashSaleDetails;
    }

    public void setFlashSaleDetails(List<FlashSaleDetail> flashSaleDetails) {
        this.flashSaleDetails = flashSaleDetails;
    }
}
