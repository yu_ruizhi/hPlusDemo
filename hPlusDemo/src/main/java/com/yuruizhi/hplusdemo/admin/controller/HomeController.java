package com.yuruizhi.hplusdemo.admin.controller;

import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.perm.PermissionService;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static com.yuruizhi.hplusdemo.admin.common.Constant.ADMIN_PREFIX;

/**
 * 后台首页控制器
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(ADMIN_PREFIX)
public class HomeController extends BaseController {

    @Autowired
    private PermissionService permissionService;

	/**
	 * 默认控制器, 跳转到 prefix/
	 * @return
	 */
    @RequestMapping
    public String _default() {
    	return redirectViewPath("/");
    }

    @RequestMapping("/")
    public String index(String t) {
    	// 已经登录
        if (null != (UserUtils.getCurrentUserId())) {
        	return getViewPath("index");
        }
        // 没有登录
        return getViewPath("login");
    }


    /**
     * 过时, 仅为兼
     * @deprecated
     */
    @Deprecated
    @RequestMapping("homePage")
    public ModelAndView homePage(HttpServletRequest request) {

        ModelAndView mav = new ModelAndView();

        // 已经登录
        List<Module> modules = permissionService.getPermissionFromCache();
        for (Module module : modules) {
            for (AdminPage page : module.getPages()) {
                String pageUrl = page.getPageUrl();
                if (null != pageUrl) {
                    request.setAttribute("mid", module.getId());
                    break;
                }
            }
        }
        request.setAttribute("modules", modules);

        mav.setViewName(getViewPath("homePage"));
    	return mav;
    }

    /**
     * <p>名称: urlLink </p>
     * <p>说明: 页面跳转方法/p>
     * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
     *
     * @author：YuRuizhi
     * @date：2016年12月9日下午10:52:19
     * @version: 1.0
     */
    @RequestMapping("urlLink")
    public String urlLink(String path) {
        if (!StringUtils.hasText(path)) {
            return "redirect:/" + ADMIN_PREFIX;
        }
        String url = getRelativeViewPath("template/" + path);
        return url;
    }
}
