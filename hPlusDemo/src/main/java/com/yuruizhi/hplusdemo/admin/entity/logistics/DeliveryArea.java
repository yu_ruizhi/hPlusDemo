/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.logistics;

import java.util.Arrays;
import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * <p>名称: DeliveryArea</p>
 * <p>说明: 物流区域实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:45:33   
 * @version: 1.0
 */
public class DeliveryArea extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 215429694725369590L;

	//邮寄区域名称
    private String name;

    //首重价格
    private BigDecimal firstPrice;

    //续重价格
    private BigDecimal additionalPrice;

    private List<DeliveryAreaDetail> deliveryAreaDetails;

    private Boolean deleted;

    /*自己添加 这个属性方便后台邮寄区域和城市之间取值的维护*/
    private String[] zaeraId;

    private Integer tijizhongliangbi;

    public Integer getTijizhongliangbi() {
        return tijizhongliangbi;
    }

    public void setTijizhongliangbi(Integer tijizhongliangbi) {
        this.tijizhongliangbi = tijizhongliangbi;
    }

    public String[] getZaeraId() {
		return zaeraId;
	}

	public void setZaeraId(String[] zaeraId) {
		this.zaeraId = zaeraId;
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getFirstPrice() {
        return firstPrice;
    }

    public void setFirstPrice(BigDecimal firstPrice) {
        this.firstPrice = firstPrice;
    }

    public BigDecimal getAdditionalPrice() {
        return additionalPrice;
    }

    public void setAdditionalPrice(BigDecimal additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public List<DeliveryAreaDetail> getDeliveryAreaDetails() {
        return deliveryAreaDetails;
    }

    public void setDeliveryAreaDetails(List<DeliveryAreaDetail> deliveryAreaDetails) {
        this.deliveryAreaDetails = deliveryAreaDetails;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public String toString() {
        return "DeliveryArea{" +
                "name='" + name + '\'' +
                ", firstPrice=" + firstPrice +
                ", additionalPrice=" + additionalPrice +
                ", deliveryAreaDetails=" + deliveryAreaDetails +
                ", deleted=" + deleted +
                ", zaeraId=" + Arrays.toString(zaeraId) +
                '}';
    }
}
