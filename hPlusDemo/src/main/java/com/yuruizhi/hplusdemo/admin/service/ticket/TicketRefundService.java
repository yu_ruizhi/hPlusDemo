/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.ticket;


import com.yuruizhi.hplusdemo.admin.entity.ticket.TicketRefund;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.Map;


/**
 * <p>名称: ProductService</p>
 * <p>说明: 产品服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:07:55   
 * @version: 1.0
 */
public interface TicketRefundService extends BaseService<TicketRefund> {

    void updateType(Map<String,Object> map);
    
}
