/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.dao.perm.PermissionDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.entity.perm.Role;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.perm.PermissionService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.ArrayUtil;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;

/**
 * <p>名称: PermissionServiceImpl</p>
 * <p>说明: 后台权限管理服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:39:51   
 * @version: 1.0
 */
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionDao permissionDao;

    @Autowired
    private UserService userService;

    @Override
    public List<Module> findPermissionByUserId(Long userId) {
        List<Module> modules = new ArrayList<Module>();
        if (null == userId) {
            return modules;
        }

        modules = permissionDao.findPermissionByUserId(userId);
        return modules;
    }

    @Override
    public List<Module> getPermissionFromCache() {
        return UserUtils.getCurrentUserCachedPermissions();
    }

    @Override
    public List<Integer> getCurrentAuditPerm() {
        List<Integer> auditPerm = new ArrayList<Integer>();
        Long userId = UserUtils.getCurrentUserId();
        User currentUser = userService.find(userId);
        if (StringUtils.isEmpty(currentUser)) {
            return auditPerm;
        }
        List<Role> roles = currentUser.getRole();
        for (Role role : roles) {
            String permissionsStr = role.getAuditPerm();
            String[] permissions = ArrayUtil.stringToArray(permissionsStr);
            for (String permission : permissions) {
                int permissionInt = Integer.parseInt(permission);
                if (!auditPerm.contains(permissionInt)) {
                    auditPerm.add(permissionInt);
                }
            }
        }
        return auditPerm;
    }
}
