package com.yuruizhi.hplusdemo.admin.controller.perm;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.perm.ModuleService;
import com.yuruizhi.hplusdemo.admin.service.perm.PageService;
import com.google.common.collect.ImmutableMap;

/**
 * Created by Administrator on 2015/6/25.
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/page")
public class PageController extends BaseController {
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private PageService adminPageService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public void index() {}

	@RequestMapping(value = "add", method = RequestMethod.GET)
	public String add(HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Module> mlist = moduleService.findPage((Map<String, ?>)null, null);
		model.addAttribute("mlist", mlist);
		return getViewPath("add");
	}

	@RequestMapping("save")
	public String save(AdminPage adminPage) {
		adminPageService.savePage(adminPage);
		return redirectViewPath("index");
	}

	@RequestMapping("list")
	@ResponseBody
	public Page<AdminPage> list(@Qualifier("page.") Filters filters, @Qualifier("module.") Filters moduleFilters,
			Pageable pageable) {
		for (Sort.Order order : pageable.getSort()) {
			if (pageable.toString().indexOf("page.name") != -1)
				order.setProperty("name");
			if (pageable.toString().indexOf("page.pageUrl") != -1)
				order.setProperty("pageUrl");
			if (pageable.toString().indexOf("page.sortId") != -1)
				order.setProperty("sort_id");
			if (pageable.toString().indexOf("module.name") != -1)
				order.setProperty("m_name");
			if (pageable.toString().indexOf("page.isShow") != -1)
				order.setProperty("is_show");
		}
		return adminPageService.findPage(filters, moduleFilters, pageable);
	}

	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		adminPageService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}

	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		AdminPage adminPage = adminPageService.find(id);
		mav.addObject("adminPage", adminPage);
		mav.setViewName(getViewPath("edit"));
		Page<Module> mlist = moduleService.findPage((Map<String,?>)null, null);
		mav.addObject("mlist", mlist);
		return mav;
	}

	@ResponseBody
	@RequestMapping("update")
	public ModelAndView update(AdminPage adminPage) {
		ModelAndView mav = new ModelAndView();
		adminPageService.update(adminPage);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}

	@ResponseBody
	@RequestMapping("checkIsPageExists")
	public Map<String, Boolean> checkIsPageExists(String name) {
		return adminPageService.checkIsPageExists(name);
	}

}
