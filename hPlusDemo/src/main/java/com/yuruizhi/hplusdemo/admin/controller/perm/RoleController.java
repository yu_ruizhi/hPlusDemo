package com.yuruizhi.hplusdemo.admin.controller.perm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.perm.RoleCategory;
import com.yuruizhi.hplusdemo.admin.service.perm.RoleCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.entity.perm.Role;
import com.yuruizhi.hplusdemo.admin.service.perm.RoleService;
import com.yuruizhi.hplusdemo.admin.util.ArrayUtil;
import com.google.common.collect.ImmutableMap;

/**
 * 后台角色
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/role")
public class RoleController extends BaseController {
	@Autowired
	private RoleService roleService;
	@Autowired
	private RoleCategoryService roleCategoryService;

	@RequestMapping("index")
	public void index() {}

	@RequestMapping("list")
	@ResponseBody
	public Page<Role> list(Filters filters, Pageable pageable) {
		return roleService.findPage(filters, pageable);
	}

	@RequestMapping("add")
	public void add() {
	}

	@RequestMapping("save")
	public String save(Role role, String pageIds) {
		roleService.saveRole(role, pageIds);
		return redirectViewPath("index");
	}

	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		Role role = roleService.find(id);
		String permissionStr = role.getAuditPerm();
		if (StringUtils.hasText(permissionStr)) {
			List<String> permissions = Arrays.asList(ArrayUtil.stringToArray(permissionStr));
			mav.addObject("permissions", permissions);
		}
		mav.addObject("role", role);
		List<RoleCategory> roleCategories=  roleCategoryService.findInRole(id);
		mav.addObject("roleCategories", roleCategories);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	@RequestMapping("update")
	public String update(Role role, String pageIds) {
		roleService.updateRole(role, pageIds);
		return redirectViewPath("index");
	}

	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		roleService.delete(id);
		if(null!=id&&id.length>0){
			for(Long l:id){
				roleCategoryService.deleteInRole(l);
			}
		}
		return ImmutableMap.<String, Object> of("success", true);
	}

	@ResponseBody
	@RequestMapping("getPagesByRoleId")
	public List<AdminPage> getPagesByRoleId(Long id) {
		List<AdminPage> pages = new ArrayList<AdminPage>();
		Role role = roleService.find(id);
		if (StringUtils.isEmpty(role)) {
			return pages;
		}
		pages = role.getPages();
		return pages;
	}

	@ResponseBody
	@RequestMapping("treeAllRoles")
	public List<Map<String, Object>> treeAllRoles() {
		return roleService.findAllRoleTree();
	}

	@ResponseBody
	@RequestMapping("checkIsRoleExists")
	public Map<String, Boolean> checkIsRoleExists(String name) {
		return roleService.checkIsRoleExists(name);
	}

}
