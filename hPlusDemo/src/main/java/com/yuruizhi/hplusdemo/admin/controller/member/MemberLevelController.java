/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MemberLevelController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.member
 * Date: 2016年4月18日 上午11:52:58  
 **/
package com.yuruizhi.hplusdemo.admin.controller.member;

import java.util.Date;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.member.MemberLevel;
import com.yuruizhi.hplusdemo.admin.service.member.MemberLevelService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.google.common.collect.ImmutableMap;

/**
 * 
 * <p>名称: 会员等级Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月18日 上午11:52:58 
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX +"/memberLevel")
public class MemberLevelController extends AdminBaseController<MemberLevel>{
	
	@Autowired
	private MemberLevelService memberLevelService;
	
    @Autowired
    private SysLogService sysLogService;
	
	/**
	 * 
	 * <p>名称：异步获取数据</p> 
	 * <p>描述：获取会员等级列表页面需要显示的数据</p>
	 * @author：wuqi
	 * @param filters 过滤器
	 * @param pageable 初始页数
	 * @return
	 */
	@ResponseBody
	@RequestMapping("list")
	public Page<MemberLevel> list(Filters filters, Pageable pageable){
		return memberLevelService.findPage(filters, pageable);
	}
	
	/**
	 * 
	 * <p>名称：保存会员等级数据</p> 
	 * <p>描述：保存在新增页面所填写的会员等级数据</p>
	 * @author：wuqi
	 * @param memberLevel 会员等级数据
	 * @return
	 */
	@ResponseBody
	@RequestMapping("saveMemberLevel")
	public ModelAndView save(MemberLevel memberLevel){
		ModelAndView mav = new ModelAndView();
		memberLevel.setCreatedDate(new Date());
		memberLevelService.save(memberLevel);
		
		//系统日志
		String operation = "会员等级-新增-id:"+memberLevel.getId()+",名称:"+memberLevel.getName();
		sysLogService.saveSysLog(Constant.SYSLOG_SAVE, operation, "会员等级管理");
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}

	
	/**
	 * 
	 * <p>名称：删除数据</p> 
	 * <p>描述：根据id删除选择的数据</p>
	 * @author：wuqi
	 * @param id 会员等级id
	 * @return
	 */
	@Override
	@ResponseBody
	@RequestMapping("delete")
	public Map<String,Object> delete(@RequestParam("id") Long[] id){
		memberLevelService.delete(id);
		
		String ids = "";
		for (int i = 0; i < id.length; i++) {
			ids += id[i]+",";
		}
		//系统日志
		ids = ids.substring(0, ids.length()-1);
		String operation = "会员等级-删除-id:"+ids;
		sysLogService.saveSysLog(Constant.SYSLOG_DELETE, operation, "会员等级管理");
		return ImmutableMap.<String, Object> of("success", true);
	}

	/**
	 * 
	 * <p>名称：编辑页面</p> 
	 * <p>描述：从列表页面进入编辑页面</p>
	 * @author：wuqi
	 * @param id 会员等级id
	 * @return
	 */
	@Override
	@RequestMapping("edit/{id}")
	public String _compat(@PathVariable(value="id") Long id, Map<String, Object> model){
		MemberLevel memberLevel = memberLevelService.find(id);
		model.put("memberLevel", memberLevel);
		return getRelativeViewPath("edit");
	}
	
	/**
	 * 
	 * <p>名称：更新会员等级数据</p> 
	 * <p>描述：对编辑页面的会员等级数据进行更新</p>
	 * @author：wuqi
	 * @param memberLevel 会员等级数据
	 * @return
	 */
	@Override
	@RequestMapping("update")
	public String update(MemberLevel memberLevel, BindingResult binding, RedirectAttributes redirectAttrs){
		memberLevel.setLastModifiedDate(new Date());
		memberLevelService.update(memberLevel);
		
		//系统日志
		String operation = "会员等级-修改-id:"+memberLevel.getId();
		sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, operation, "会员等级管理");
		return redirectViewPath("index");
	}
	
	/**
	 * 
	 * <p>名称：检测是否重名</p> 
	 * <p>描述：在新增页面对会员等级名称进行重名验证</p>
	 * @author：wuqi
	 * @param name 会员等级名称
	 * @return
	 */
	@RequestMapping("checkIsNameExists")
	@ResponseBody
	public Map<String, Boolean> checkIsNameExists(String name){
		return memberLevelService.checkIsNameExists(name);
	}
	
	/**
	 * 
	 * <p>名称：检测是否重名</p> 
	 * <p>描述：在编辑页面根据会员等级id及会员等级名称进行重名验证</p>
	 * @author：wuqi
	 * @param idAndName 会员等级id，会员等级名称
	 * @return 
	 */
	@RequestMapping("hasAnnotherName")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherName(String[] idAndName){
		Map<String, Boolean> map = memberLevelService.hasAnnotherName(idAndName);
		return map;
	}
}
