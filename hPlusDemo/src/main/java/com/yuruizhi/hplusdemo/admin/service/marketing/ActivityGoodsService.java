/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ActivityGoodsService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月21日 下午5:13:43 
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.marketing.ActivityGoods;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 活动商品Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午5:13:43    
 * @version: 1.0
 */
public interface ActivityGoodsService extends BaseService<ActivityGoods> {

	/**
	 * 
	 * <p>名称：根据活动id获取单品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param activityId 活动id
	 * @return 返回单品信息
	 */
	List<ActivityGoods> findByActivityId(Long activityId);
	
	/**
	 * 
	 * <p>名称：根据单品id及活动id获取单品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param goodsId 单品id
	 * @param activityId 活动id
	 * @return 返回单品信息
	 */
	ActivityGoods findByGoodsIdAndActivityId(Long goodsId,Long activityId);
	
}
