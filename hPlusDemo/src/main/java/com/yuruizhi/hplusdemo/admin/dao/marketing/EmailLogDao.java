/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailLog;

/**
 * Created on 2016-04-14 15:43:33
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface EmailLogDao extends BaseDao<EmailLog> {
}