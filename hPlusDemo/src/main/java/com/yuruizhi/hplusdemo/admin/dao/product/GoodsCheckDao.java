/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsCheckDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.product
 * Date: 2016年4月26日 上午11:45:20  
 **/
package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsCheck;

/**
 * 
 * <p>名称: 商品核价Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月26日 上午11:45:20   
 * @version: 1.0
 */
public interface GoodsCheckDao extends BaseDao<GoodsCheck> {

	String GOODS_CHECK_FILTERS_PROP = "_goods_check_filters";
	String GOODS_FILTERS_PROP = "_goods_filters";
	String PRODUCT_FILTERS_PROP = "_product_filters";
	String CATEGORY_FILTERS_PROP = "_category_filters";
	String BRAND_FILTERS_PROP = "_brand_filters";
	String WOEKS_FILTERS_PROP = "_works_filters";
	
	/**
	 * 
	 * <p>名称：根据单品id删除核价信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param id 单品id
	 */
	void deleteByGoodsId(Long id);
	
	
	/**
	 * 
	 * <p>名称：根据单品ID修改为未核价</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param id 单品id
	 */
	void updateChecked(Map<String,Object> map);
	
}
