package com.yuruizhi.hplusdemo.admin.util.sms;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 */
public abstract class AbstractSmsSender implements SmsSender {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean sendSms(String mobile, String content) {
        return sendSms(mobile.split("\\s*,\\s*"), content);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean sendSms(String[] mobiles, String content) {
        return sendSms(mobiles, content, 0);
    }

    /**
     * 在给定的时间发送短信到给定的手机号码
     *
     * @param mobiles 目标手机号码
     * @param content 短信内容
     * @param second  延迟发送秒
     * @return 是否发送成功
     */
    public boolean sendSms(String[] mobiles, String content, int second) {
        Date sendTime = new Date(new Date().getTime() + TimeUnit.SECONDS.toMillis(second));
        return sendSms(mobiles, content, sendTime);
    }

    /**
     * 在给定的时间发送短信到给定的手机号码
     *
     * @param mobiles  目标手机号码
     * @param content  短信内容
     * @param sendDate 短信发送时间
     * @return 剩余可用短信数
     */
    public boolean sendSms(String[] mobiles, String content, Date sendDate) {
        String signature = getSignature();
        // 追加签名
        if (null != signature) {
            signature = signature.startsWith("【") ? signature : "【" + signature;
            signature = signature.endsWith("】") ? signature : signature + "】";
        }
        if (null != signature) {
            content = isSignatureAtAfter() ? content + signature : signature + content;
        }
        return doSendSms(mobiles, content, sendDate);
    }

    protected abstract boolean doSendSms(String[] mobiles, String content, Date sendDate);

    // 签名
    protected abstract String getSignature();

    // 是否签名在后
    protected boolean isSignatureAtAfter() {
        return false;
    }
}
