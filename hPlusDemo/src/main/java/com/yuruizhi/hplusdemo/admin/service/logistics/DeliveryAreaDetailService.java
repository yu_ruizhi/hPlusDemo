package com.yuruizhi.hplusdemo.admin.service.logistics;


import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryAreaDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by ASUS on 2014/12/9.
 */
public interface DeliveryAreaDetailService extends BaseService<DeliveryAreaDetail> {

    void deletedByAreaId(Long fkid);
    void deletedByParent(Long deliveryAreaId);
}
