/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CouponType;

/**
 * Created on 2016-03-30 11:47:01
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/couponType")
public class CouponTypeController extends AdminBaseController<CouponType> {

}
