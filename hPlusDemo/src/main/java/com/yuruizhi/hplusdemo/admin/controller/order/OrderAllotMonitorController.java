/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: OrderAllotMonitorController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.order
 * Date: 2016年4月28日上午10:24:35
 **/
package com.yuruizhi.hplusdemo.admin.controller.order;

import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;
import com.yuruizhi.hplusdemo.admin.service.order.OrderAllotService;
import org.apache.commons.lang.StringUtils;
import org.ponly.common.json.Jacksons;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * <p>名称: 配单监控Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月28日上午10:25:04
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "orderAllotMonitor")
public class OrderAllotMonitorController extends AdminBaseController<OrderAllot> {

    @Autowired
    private OrderAllotService orderAllotService;
    @RequestMapping("listr")
    @ResponseBody
    public Page<OrderAllot> listr(Filters filters, Pageable pageable
            ,@RequestParam(value = "goodsCode", required = false) String goodsCode,
                                 @RequestParam(value = "beginDate", required = false) String beginDate,
                                 @RequestParam(value = "endDate", required = false)  String endDate
    ) {
        filters.ne("allotStatus",1);
        if(null!=goodsCode&&goodsCode.trim().length()>0){
            filters.eq("O_D_G_CODE",goodsCode);
        }
        if (StringUtils.isNotEmpty(beginDate)) {
            filters.ge("O_D_G_RESERVE_DEADLINE", beginDate);
        }
        if (StringUtils.isNotEmpty(endDate)) {
            filters.le("O_D_G_RESERVE_DEADLINE", endDate);
        }
        Page<OrderAllot> orderAllots = orderAllotService.findPage(filters, pageable);
        //对订单中的收货地址进行遍历
        if(null!=orderAllots&&orderAllots.getData()!=null){
            for(OrderAllot orderAllot:orderAllots.getData()){
                if(null!=goodsCode&&goodsCode.trim().length()>0&&orderAllot.getAllotStatus()==OrderAllot.ALLOT_NOT_ARRIVING){
                    int num=orderAllot.getOrdersReceipt().getOrdersReceiptDetail().getGoodsNum();
                    orderAllot.setAllotNum(num);
                }
                MemberAddress memberAddress = Jacksons.deserialize(orderAllot.getOrdersReceipt().getShippingAddress(), MemberAddress.class);
                orderAllot.getOrdersReceipt().setMemberAddress(memberAddress);

            }
        }

        return orderAllots;
    }

}
