package com.yuruizhi.hplusdemo.admin.dao.logistics;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModule;

/**
 * Created by ASUS on 2015/1/13.
 */
public interface DeliveryModuleDao extends BaseDao<DeliveryModule> {
}
