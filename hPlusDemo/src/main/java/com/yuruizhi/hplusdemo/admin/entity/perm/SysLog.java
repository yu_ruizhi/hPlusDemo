/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.perm;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;


/**
 * @author 赵创 on 2016/4/23. 后台系统日志实体
 */
public class SysLog extends BaseEntity {
    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 8409995381070512344L;
	/**
     *
     * @Column
     * @ViewField text=日志记录类型   1：新增  2：删除   3:修改
     */
	private Integer type;
    /**
     *
     * @Column
     * @ViewField text=记录内容
     */
    private String  operation;

    private Boolean deleted;

    private String moduleName;//模块名称

    private String userIp;//用户ip

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
