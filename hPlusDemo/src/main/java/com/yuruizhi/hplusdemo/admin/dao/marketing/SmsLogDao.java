/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.SmsLog;

/**
 * Created on 2016-03-28 14:18:17
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface SmsLogDao extends BaseDao<SmsLog> {
}