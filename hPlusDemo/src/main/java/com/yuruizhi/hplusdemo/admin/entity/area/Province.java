/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Province.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.area
 * Date: 2016年4月18日下午5:56:01
 **/
package com.yuruizhi.hplusdemo.admin.entity.area;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 省份实体</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午5:56:31
 * @version: 1.0
 */
public class Province extends BaseEntity {
	
	private static final long serialVersionUID = 1L;

	/**@Fields provinceId : 省编号*/
	private String provinceId;

	/**@Fields provinceName : 省名*/
	private String provinceName;
	
	/**@Fields cities : 城市列表*/
	private List<City> cities;

	public String getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(String provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}
}
