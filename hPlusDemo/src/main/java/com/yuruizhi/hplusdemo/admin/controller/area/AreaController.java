/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: AreaController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.area
 * Date: 2016年4月18日下午8:08:15
 **/
package com.yuruizhi.hplusdemo.admin.controller.area;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.entity.area.Area;
import com.yuruizhi.hplusdemo.admin.entity.area.City;
import com.yuruizhi.hplusdemo.admin.service.area.AreaService;
import com.yuruizhi.hplusdemo.admin.service.area.CityService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 地区Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午8:08:46
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/area")
public class AreaController {
	@Autowired
	private CityService cityService;
	@Autowired 
	private AreaService areaService;
	
	/**
	 * 
	 * <p>名称：根据省编号找城市列表</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param provinceId 省编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping("findByProvinceId")
	public Map<String,Object> findByContinentId(String provinceId){
		Map<String,Object> map = Maps.newHashMap();
		List<City> cityList = cityService.findByPid(provinceId);
		map.put("cityList", cityList);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：根据城市编号查找区列表</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param cityId 市编号
	 * @return
	 */
	@ResponseBody
	@RequestMapping("findByCityId")
	public Map<String,Object> findByCityId(String cityId){
		Map<String,Object> map = Maps.newHashMap();
		List<Area> areaList = areaService.findByCid(cityId);
		map.put("areaList", areaList);
		return map;
	}

}
