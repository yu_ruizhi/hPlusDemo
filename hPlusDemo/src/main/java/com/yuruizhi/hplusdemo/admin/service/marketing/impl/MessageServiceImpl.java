/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月20日 上午11:27:50
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.marketing.MessageDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.MessageLogDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.MessageMemberDao;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberDao;
import com.yuruizhi.hplusdemo.admin.dao.order.OrderAllotDao;
import com.yuruizhi.hplusdemo.admin.dao.order.ProblemOrderDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageLog;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageMember;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import com.yuruizhi.hplusdemo.admin.entity.order.ProblemOrder;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.marketing.Message;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.MessageService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.ponly.common.util.Dates.now;

/**
 * 
 * <p>名称: 私信Service实现</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日 上午11:28:12
 * @version: 1.0
 */
@Service

public class MessageServiceImpl extends BaseServiceImpl<Message> implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Autowired
    public void setProblemOrderDao(MessageDao messageDao) {
        super.setBaseDao(messageDao);
        this.messageDao = messageDao;
    }

    @Autowired
    private MessageLogDao messageLogDao;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private OrderAllotDao orderAllotDao;


    /**
     *
     * <p>名称：OrderAllotNotice</p>
     * <p>描述：站内信通知：订单状态为尾款订单后</p>
     * @author：yuruizhi
     * @param ids
     * @return
     */
    @Override
    public void OrderAllotNotice(Long[] ids) {

        Map<String,Object> parmsMap = new HashMap<String,Object>();
        parmsMap.put("ids",ids);
        List<OrderAllot> orderAllots = orderAllotDao.orderAllotInOrder(parmsMap);  //根据配单ID集合查询配单、订单、用户信息

        for(OrderAllot orderAllot:orderAllots){

            //配单后发送站内信站内信
            String orderCode = orderAllot.getOrdersReceipt().getOrderCode();   //订单号
            // 发送创建好的站内信给所有会员
            String title = "配单提醒";
            MessageLog messageLog = new MessageLog();
            messageLog.setTitle(title);
            messageLog.setContent("您的订单：" + orderCode + "已配单啦，您可以支付尾款了~~~");

            messageLog.setState(Constant.MESSAGE_STATE_UNREAD); //默认未读
            messageLog.setType(Constant.MESSAGE_TYPE_ESHOP); //电商私信

            List<Member> memberList = memberDao.findAll(); //所有前台会员
            for (Member member : memberList) {
                Long memberId = member.getId();
                messageLog.setMemberId(memberId);
                messageLogDao.save(messageLog);   //注册成功发送站内信
            }

        }
    }

}
