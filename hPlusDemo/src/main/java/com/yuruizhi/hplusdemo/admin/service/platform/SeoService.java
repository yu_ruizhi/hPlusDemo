/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: SeoService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.platform
 * Date: 2016年4月23日上午10:21:15
 **/
package com.yuruizhi.hplusdemo.admin.service.platform;

import com.yuruizhi.hplusdemo.admin.entity.platform.Seo;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: SEO Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月23日上午10:21:46
 * @version: 1.0
 */
public interface SeoService extends BaseService<Seo> {

}
