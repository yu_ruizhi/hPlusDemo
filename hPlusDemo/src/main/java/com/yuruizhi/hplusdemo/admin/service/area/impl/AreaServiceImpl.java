/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: AreaServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area.impl
 * Date: 2016年4月18日下午7:43:05
 **/
package com.yuruizhi.hplusdemo.admin.service.area.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.area.AreaDao;
import com.yuruizhi.hplusdemo.admin.entity.area.Area;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.area.AreaService;

/**
 * 
 * <p>名称: 区Service实现</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午7:43:33
 * @version: 1.0
 */
@Service
public class AreaServiceImpl extends BaseServiceImpl<Area> implements AreaService {
	@Autowired
	private AreaDao areaDao;

	@Override
	public Area findByAreaId(String areaId) {
		return areaDao.findByAreaId(areaId);
	}

	@Override
	public List<Area> findByCid(String pid) {
		List<Area> areaList = areaDao.findByCid(pid);
		return areaList;
	}

}
