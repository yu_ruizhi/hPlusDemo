package com.yuruizhi.hplusdemo.admin.service.logistics.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.logistics.DeliveryAreaDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryArea;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryAreaDetail;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryAreaDetailService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryAreaService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductService;
import com.google.common.collect.Maps;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by ASUS on 2014/12/8.
 */
@Service
public class DeliveryAreaServiceImpl extends BaseServiceImpl<DeliveryArea> implements DeliveryAreaService{
  @Autowired
   private ProductService productService;
    @Autowired
    private DeliveryAreaDetailService deliveryAreaDetailService;

    private DeliveryAreaDao deliveryAreaDao;
    @Autowired
    public void setAdminUserDao(DeliveryAreaDao deliveryAreaDao){
        this.deliveryAreaDao = deliveryAreaDao;
        setBaseDao(deliveryAreaDao);
    }



    @Override
    public BigDecimal getPostage(DeliveryMethod deliveryMethod,String[] ids,String[] nums) {
      /*计算所有商品重量*/
        BigDecimal zprice=null;
Float wigth=new Float(0);
       for(int i=0;i<ids.length;i++){
          Float w= productService.getWeight(ids[i]);
           wigth+=((Float)w*(Integer.parseInt(nums[i])));
       }
        BigDecimal b = new BigDecimal(wigth);
        wigth = b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
/*总总量转换为克*/
       Float k=(Float)wigth*1000;
        BigDecimal firstPrice=  deliveryMethod.getDeliveryMethodDetails().get(0).getDeliveryArea().getFirstPrice();
           if(k<=1000){
               zprice= firstPrice;
           }else if(k>1000){
               BigDecimal additionalPrice=  deliveryMethod.getDeliveryMethodDetails().get(0).getDeliveryArea().getAdditionalPrice();
               Float k2=(Float)k-1000;
               zprice= firstPrice;
               if(k2<1000) {
                   zprice= zprice.add(additionalPrice);
               }else{
                   zprice=zprice.add(additionalPrice.multiply(new BigDecimal(k2%1000+"")));
               }
           }
        return zprice;
    }

    @Override
    public void addArea(DeliveryArea deliveryArea) {
        save(deliveryArea);
        if(deliveryArea.getZaeraId()!=null&&deliveryArea.getZaeraId().length>0){
            for(String areaId:deliveryArea.getZaeraId()){
            	DeliveryAreaDetail deliveryAreaDetail=new DeliveryAreaDetail();
                deliveryAreaDetail.setDeliveryAreaId(deliveryArea.getId());
                deliveryAreaDetail.setAreaId(areaId);
                deliveryAreaDetail.setDeleted(false);
                deliveryAreaDetail.setCreatedBy(deliveryArea.getCreatedBy());
                deliveryAreaDetail.setCreatedDate(new Date());
                deliveryAreaDetailService.save(deliveryAreaDetail);
            }
        }
    }

    @Override
    public void bianji(DeliveryArea deliveryArea) {
    	update(deliveryArea);
        deliveryAreaDetailService.deletedByParent(deliveryArea.getId());
        if(deliveryArea.getZaeraId()!=null&&deliveryArea.getZaeraId().length>0){
            DeliveryAreaDetail deliveryAreaDetail=new DeliveryAreaDetail();
            for(String areaId:deliveryArea.getZaeraId()){
                deliveryAreaDetail.setDeliveryAreaId(deliveryArea.getId());
                deliveryAreaDetail.setAreaId(areaId);
                deliveryAreaDetail.setDeleted(false);
                deliveryAreaDetail.setCreatedBy(deliveryArea.getCreatedBy());
                deliveryAreaDetail.setCreatedDate(new Date());
                deliveryAreaDetailService.save(deliveryAreaDetail);
            }
        }
    }

    @Override
    public List<DeliveryArea> selectAll() {
        return deliveryAreaDao.selectAll();
    }

    /**
     * 保存验证重名
     */
    @Override
	public Map<String, Boolean> checkIsNameExists(String name) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<DeliveryArea> deliveryAreaList = deliveryAreaDao.findByName(name);
		if (deliveryAreaList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

    /**
	 * 更新验证重名
	 */
	@Override
	public Map<String, Boolean> hasAnnotherName(String[] hasAnnothername) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<DeliveryArea> deliveryAreaList = deliveryAreaDao.findByName(hasAnnothername[1]);
		Iterator<DeliveryArea> it = deliveryAreaList.iterator();
		DeliveryArea deliveryArea = it.hasNext() ? it.next() : null;
		result.put("isExists", null != deliveryArea && !deliveryArea.getId().equals(Long.valueOf(hasAnnothername[0])));
		return result;
	}

}
