/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月20日 上午10:40:03  
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.marketing.ProductCategoryDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategory;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.ProductCategoryService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 分类商品ServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:40:03    
 * @version: 1.0
 */
@Service
public class ProductCategoryServiceImpl extends BaseServiceImpl<ProductCategory> implements ProductCategoryService{

	private ProductCategoryDao productCategoryDao;
	
	@Autowired
	public void setProductCategoryDao(ProductCategoryDao productCategoryDao) {
		this.productCategoryDao = productCategoryDao;
		setCrudDao(productCategoryDao);
	}
	
	@Override
	public Map<String, Boolean> checkIsNameExists(String name) {
		Map<String, Boolean> model = Maps.newHashMap();
		List<ProductCategory> productCategoryList = productCategoryDao.findByName(name);
		if (null != productCategoryList && productCategoryList.size() > 0) {
			model.put("isExists", true);
		} else {
			model.put("isExists", false);
		}
		return model;
	}

	@Override
	public Map<String, Boolean> hasAnnotherName(String[] hasAnnotherName) {
		Map<String, Boolean> result = Maps.newHashMap();
		List<ProductCategory> productCategoryList = productCategoryDao.findByName(hasAnnotherName[1]);
		if (null != productCategoryList && productCategoryList.size() > 0) {
			Iterator<ProductCategory> it = productCategoryList.iterator();
			ProductCategory productCategory = it.hasNext() ? it.next() : null;
			result.put("isExists", null != productCategory && !productCategory.getId().equals(Long.valueOf(hasAnnotherName[0])));
		}
		return result;
	}
	
}
