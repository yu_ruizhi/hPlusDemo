/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Area.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.area
 * Date: 2016年4月18日下午6:08:16
 **/
package com.yuruizhi.hplusdemo.admin.entity.area;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 区实体</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午6:08:16
 * @version: 1.0
 */
public class Area extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**@Fields areaId : 区编号*/
	private String areaId;

	/**@Fields areaName : 区名字*/
	private String areaName;

	/**@Fields pid : 城市编号（因数据表中为pid，故与数据表命名一致）*/
	private String pid;

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}
}
