/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.*;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.*;

/**
 * <p>名称: EmailLog</p>
 * <p>说明: 邮件发送日志实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:55:55   
 * @version: 1.0
 */
public class EmailLog extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 2642019304637040916L;

	/**
     * 邮件ID
     * ID
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=EMAIL_ID optional=true
     */
    @NotNull
    private Email email;

    /**
     * 发送人
     * 
     * @ViewField editor=input 
     * @Column SENDER
     */
    @NotNull
    @Size(max = 500)
    private String sender;

    /**
     * 收件人
     * 
     * @ViewField editor=input 
     * @Column RECEIVER
     */
    @Size(max = 500)
    private String receiver;

    /**
     * 抄送
     * 
     * @ViewField editor=input 
     * @Column COPY_TO
     */
    @Size(max = 500)
    private String copyTo;

    /**
     * 密送
     * 
     * @ViewField editor=input 
     * @Column SECRET_TO
     */
    @Size(max = 500)
    private String secretTo;

    /**
     * 邮件主题
     * 
     * @ViewField editor=input 
     * @Column SUBJECT
     */
    @NotNull
    @Size(max = 255)
    private String subject;

    /**
     * 邮件内容
     * 
     * @ViewField editor=input 
     * @Column CONTENT
     */
    @NotNull
    @Size(max = 500)
    private String content;

    /**
     * 发送时间
     * 
     * @ViewField editor=datepicker 
     * @Column SEND_TIME
     */
    @NotNull
    private Date sendTime;

    /**
     * 发送状态
     * 
     * @ViewField editor=spinner 
     * @Column STATUS
     */
    @NotNull
    private Integer status;

    /**
     * 发送源
     * 
     * @ViewField editor=input 
     * @Column TYPE
     */
    @Size(max = 255)
    private String type;

    /**
     * 发送源引用
     * 
     * @ViewField editor=spinner 
     * @Column REF_ID
     */
    private Integer refId;

    /**
     * 发送批次
     * 
     * @ViewField editor=input 
     * @Column BATCH
     */
    @Size(max = 255)
    private String batch;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Email getEmail() {
        return this.email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getSender() {
        return this.sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return this.receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getCopyTo() {
        return this.copyTo;
    }

    public void setCopyTo(String copyTo) {
        this.copyTo = copyTo;
    }

    public String getSecretTo() {
        return this.secretTo;
    }

    public void setSecretTo(String secretTo) {
        this.secretTo = secretTo;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getSendTime() {
        return this.sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getRefId() {
        return this.refId;
    }

    public void setRefId(Integer refId) {
        this.refId = refId;
    }

    public String getBatch() {
        return this.batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
