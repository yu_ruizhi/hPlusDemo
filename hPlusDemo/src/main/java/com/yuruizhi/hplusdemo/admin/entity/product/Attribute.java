/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Attribute</p>
 * <p>说明: 产品属性实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:16:50   
 * @version: 1.0
 */
public class Attribute extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 5029669228941954179L;
	private String name;
    private String alias;
    /**
     * 显示类型
     * @Column
     */
    private Integer displayType;
    /**
     * 是否显示
     * @Column
     */
    private Boolean visible;
    /**
     * 是否基础属性
     * @Column
     */
    private Boolean isBase;
    /**
     * 排序
     * @Column
     */
    private Integer sort;
    /**
     * 是否删除
     * @Column
     */
    private Boolean deleted = false;
    /**
     * 所属模型
     * @ManyToOne
     */
    private Model model;
    /**
     * 属性值
     */
    private List<AttributeValue> attributeValues;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Integer getDisplayType() {
        return displayType;
    }

    public void setDisplayType(Integer displayType) {
        this.displayType = displayType;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getIsBase() {
        return isBase;
    }

    public void setIsBase(Boolean isBase) {
        this.isBase = isBase;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public List<AttributeValue> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<AttributeValue> attributeValues) {
        this.attributeValues = attributeValues;
    }
}
