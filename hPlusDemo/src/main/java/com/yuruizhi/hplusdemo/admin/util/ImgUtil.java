/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 * <p>
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import org.ponly.common.util.IOUtils;
import org.ponly.fs.FileSystem;
import org.ponly.fs.FileSystems;
import org.ponly.fs.FileView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;

import static com.yuruizhi.hplusdemo.admin.util.CacheUtils.get;
import static org.ponly.common.util.Throwables.rethrowRuntimeException;

/**
 * <p>名称: ImgUtil</p>
 * <p>说明: 图片处理 工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 *
 * @author：YuRuizhi
 * @date：2016年7月20日下午14:20:59
 * @version: 1.0
 */
public class ImgUtil {

    /**
     * 日志对象
     */
    private static final Logger LOG = LoggerFactory.getLogger(ImgUtil.class);
    private static final String POST_METHOD = "POST";
    private static final String ENCODING = "UTF-8";

    /**
     * 图片测试
     *
     * @return 当前用户 id
     */

    public static void test() {

        // 创建一个从 ftp 读取文件的文件系统 uri
        String fsUri = "ftp://user:password@host";

        // 创建一个基于OSS系统, 以 d:/fs 为根目录的文件系统
        String sftp = "stfp:amdmin:password//@180.153.88.55:///d:oku";


        FileSystem fs = FileSystems.getFileSystem(fsUri);

        // FileSystem.getFileSystem("sftp://127.0.0.1", "vacoor", "password")

        // 在该文件系统中创建一个 dir/hello.txt, 如果存在则覆盖
        OutputStream out = fs.create("dir/hello.txt", true);
        PrintStream printOut = new PrintStream(out);
        printOut.append("Hello FS !");
        printOut.flush();
        printOut.close();

        // 读取 dir/hello.txt 文件内容
        InputStream in = fs.open("dir/hello.txt");
        //String text = IOUtils.toString(in, Charset.forName("UTF-8"), true);
        //System.out.println(text);

        // 查看文件状态
        FileView stat = fs.stat("dir/hello.txt");
        System.out.println("路径:" + stat.getPath());
        System.out.println("所有人:" + stat.getOwner());
        System.out.println("大小:" + stat.getLength());
        System.out.println("最后修改时间:" + stat.getLastModifiedTime());
    }

    /**
     * 复制单个文件(原名复制)
     * @param fileName 准备复制的文件
     * 拷贝到新绝对路径带文件名(注：目录路径需带文件名)
     * @return
     */
    public static void CopySingleFileTo(String fileName) {
        try {

            int bytesum = 0;
            int byteread = 0;

            String fsUriOld = "ftp://administrator:P@ssw0rd--!@#456&*(@180.153.88.55/d/oku/700";  //老：艾漫window server
            FileSystem fsOld = FileSystems.getFileSystem(fsUriOld);

            String fsUriNew = "aliyun://KudDSWR2yHbMYrBR:chHHYo2Q6HLPH7CkrfjqjWYq5jnT6S@aimon.oss-cn-shanghai.aliyuncs.com";  //新：艾漫阿里云OSS
            FileSystem fsNew = FileSystems.getFileSystem(fsUriNew);

            FileView[] fileViews = fsNew.ls(fsUriNew);
            for(FileView fv : fileViews){
                System.out.println(fv.getPath());
            }

            if(fsOld.exists(fileName)){  //获取给定的文件是否存在，如果存在

                InputStream inStream = fsOld.open(fileName);  //读取原文件内容

                OutputStream opt = fsNew.create(fileName, true);  //在该文件系统中创建一个 fileName, 如果存在则覆盖

                byte[] buffer = new byte[1444];
                while ((byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread; //字节数 文件大小

                    opt.write(buffer, 0, byteread);
                }
                inStream.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
