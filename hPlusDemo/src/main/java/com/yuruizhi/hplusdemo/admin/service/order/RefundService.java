/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order;

import com.yuruizhi.hplusdemo.admin.entity.order.Refund;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: RefundService</p>
 * <p>说明: 退换货服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:37:26   
 * @version: 1.0
 */
public interface RefundService extends BaseService<Refund> {

	Refund findRefundById(Long id);

	void updateStatus(Long id, int status);

	void batchReject(String ids, String rejectReason);

	void batchAgree(Long[] ids);
}
