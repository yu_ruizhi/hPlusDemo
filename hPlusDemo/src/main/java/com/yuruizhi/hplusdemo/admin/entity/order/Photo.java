/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Photo</p>
 * <p>说明: 商品评论图片实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:11:46   
 * @version: 1.0
 */
public class Photo extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 6926846051575882227L;

	/**
     * 照片路径
     * 
     * @ViewField editor=input 
     * @Column PATH
     */
    @NotNull
    @Size(max = 255)
    private String path;

    /**
     * 照片排序
     * 
     * @ViewField editor=spinner 
     * @Column POSITION
     */
    @NotNull
    private Integer position;

    /**
     * 评论ID
     * 
     * @ViewField editor=select internal="id:name" 
     * @ManyToOne joinColumn=PRODUCT_REVIEW_ID optional=true
     */
    @NotNull
    private ProductReview productReview;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getPosition() {
        return this.position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public ProductReview getProductReview() {
        return this.productReview;
    }

    public void setProductReview(ProductReview productReview) {
        this.productReview = productReview;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
