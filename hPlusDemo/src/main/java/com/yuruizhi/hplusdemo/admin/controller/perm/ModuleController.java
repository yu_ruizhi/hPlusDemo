package com.yuruizhi.hplusdemo.admin.controller.perm;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.perm.ModuleService;
import com.google.common.collect.ImmutableMap;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 后台模块
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/module")
public class ModuleController extends BaseController {
	@Autowired
	private ModuleService moduleService;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	public void index() {}

	@RequestMapping(value = "add", method = RequestMethod.GET)
	public void add() { }

	@RequestMapping(value = "save")
	public String save(Module module) {
		module.setCreatedDate(new Date());
//		module.setCreatedBy();
		moduleService.save(module);
		return redirectViewPath("index");
	}

	@RequestMapping("list")
	@ResponseBody
	public Page<Module> list(Filters filters, Pageable pageable) {
		Page<Module> mlist = moduleService.findPage(filters, pageable);

		return mlist;
	}

	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		moduleService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}

	@RequestMapping("deleteModuleByIds")
	@ResponseBody
	public boolean deleteModuleByIds(Long[] ids) {
		return moduleService.deleteModuleByIds(ids);

	}

	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		Module module = moduleService.find(id);
		mav.addObject("module", module);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	@ResponseBody
	@RequestMapping("findAllPage")
	public List<Map<String, Object>> findAllPage() {
		return moduleService.findAllPage();
	}

	@ResponseBody
	@RequestMapping("checkIsModuleExists")
	public Map<String, Boolean> checkIsModuleExists(String name,Long id) {
		return moduleService.checkIsModuleExists(name,id);
	}

	@ResponseBody
	@RequestMapping("update")
	public ModelAndView update(Module module) {
		ModelAndView mav = new ModelAndView();
		moduleService.update(module);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}

}
