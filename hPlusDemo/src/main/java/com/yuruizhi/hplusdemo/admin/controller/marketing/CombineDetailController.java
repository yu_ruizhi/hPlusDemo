/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.marketing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Combine;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CombineDetail;
import com.yuruizhi.hplusdemo.admin.service.marketing.CombineService;

import java.util.*;

/**
 * Created on 2016-03-18 09:50:39
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX+ "/combineDetail")
public class CombineDetailController extends AdminBaseController<CombineDetail> {
    @Autowired
    private CombineService combineService;

    @Override
    public void input(CombineDetail combineDetail, Map<String, Object> model) {
        super.input(combineDetail, model);
        model.put("combines", combineService.findAll());
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Combine.class, "combine", new CustomStringBeanPropertyEditor(Combine.class));
    }
}
