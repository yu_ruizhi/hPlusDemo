/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.cms;

import java.util.Map;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.TreeNodeBaseDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.Channel;

/**
 * Created on 2016-02-23 11:01:34
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface ChannelDao extends TreeNodeBaseDao<Channel> {
	final String CHANNEL_FILTERS_PROP = "channelFilter";

	/** 
	 * @Title: updatePath 
	 * @Description: TODO
	 * @param params
	 * @return: void
	 */
	void updatePath(Map<String, Object> params);

	/** 
	 * @Title: childCount 
	 * @Description: TODO
	 * @param path
	 * @return
	 * @return: int
	 */
	int childCount(Map<String, Object> params);

	/** 
	 * @Title: updateDepth 
	 * @Description: TODO
	 * @param params
	 * @return: void
	 */
	void updateDepth(Map<String, Object> params);
}