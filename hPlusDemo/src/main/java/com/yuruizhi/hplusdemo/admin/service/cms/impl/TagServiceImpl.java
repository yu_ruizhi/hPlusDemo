/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.cms.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.cms.TagDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.Tag;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.cms.TagService;

/**
 * <p>名称: TagServiceImpl</p>
 * <p>说明: 标签服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:02:51   
 * @version: 1.0
 */
@Service
public class TagServiceImpl extends BaseServiceImpl<Tag> implements TagService {
	
	@Autowired
	private TagDao tagDao;
	
	
	
	@Override
	public Tag findAndSave(String tarr) {
		Map<String,Object> params = new HashMap<>();
		params.put("name", tarr);
		Tag tag = tagDao.findOne(params);
		if(null == tag){
			tag = new Tag();
			tag.setName(tarr);
			tag.setCreatedDate(new Date());
			tag.setLastModifiedDate(new Date());
			tagDao.save(tag);
			
		}
		return tag;
	}



	/**
	 * @Title: findByArtId
	 * @Description:获取文章标签列表
	 * @param id
	 */
	@Override
	public List<Tag> findByArtId(Long id) {
		
		return tagDao.findByArtId(id);
	}



	@Override
	public Boolean isUnique(String tagName,Long id) {
		int count = tagDao.isUnique(tagName.trim(),id);
		if(count>0){
			return false;
		}else{
			return true;
		}
	}

}
