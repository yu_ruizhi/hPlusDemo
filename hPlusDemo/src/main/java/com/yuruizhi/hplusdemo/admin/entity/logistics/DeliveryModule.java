package com.yuruizhi.hplusdemo.admin.entity.logistics;



import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ASUS on 2015/1/13.
 */
public class DeliveryModule extends BaseEntity implements Serializable{

    private String name;

    private String bgUrl;

    private String remark;

    private List<DeliveryModuleDetail> deliveryModuleDetails;

    private Boolean deleted;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBgUrl() {
        return bgUrl;
    }

    public void setBgUrl(String bgUrl) {
        this.bgUrl = bgUrl;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<DeliveryModuleDetail> getDeliveryModuleDetails() {
        return deliveryModuleDetails;
    }

    public void setDeliveryModuleDetails(List<DeliveryModuleDetail> deliveryModuleDetails) {
        this.deliveryModuleDetails = deliveryModuleDetails;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
