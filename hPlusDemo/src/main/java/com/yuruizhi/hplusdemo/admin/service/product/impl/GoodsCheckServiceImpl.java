/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsCheckServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.product.impl
 * Date: 2016年4月26日 上午11:46:38 
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.marketing.NewOrHotDao;
import com.yuruizhi.hplusdemo.admin.dao.member.MemberDao;
import com.yuruizhi.hplusdemo.admin.dao.product.GoodsCheckDao;
import com.yuruizhi.hplusdemo.admin.entity.product.GoodsCheck;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsCheckService;

/**
 * 
 * <p>名称: 商品核价ServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月26日 上午11:46:38  
 * @version: 1.0
 */
@Service
public class GoodsCheckServiceImpl extends BaseServiceImpl<GoodsCheck> implements GoodsCheckService{
	
	private GoodsCheckDao goodsCheckDao;

	@Autowired
	public void setGoodsCheckDao(GoodsCheckDao goodsCheckDao){
		this.goodsCheckDao = goodsCheckDao;
		setCrudDao(goodsCheckDao);
	}

	@Override
	public Page<GoodsCheck> findPage(Filters filters, Filters goodsFilters, Filters productFilters, Filters categoryFilters,
			Filters brandFilters, Filters worksFilters, Pageable pageable,String spotSell) {
		Map<String, Object> paramsMap = createParamsMap();
        if (null != filters) {
            paramsMap.put(GoodsCheckDao.GOODS_CHECK_FILTERS_PROP, filters);
        }
        if (null != goodsFilters) {
            paramsMap.put(GoodsCheckDao.GOODS_FILTERS_PROP, goodsFilters);
        }
        if (null != productFilters) {
            paramsMap.put(GoodsCheckDao.PRODUCT_FILTERS_PROP, productFilters);
        }
        if (null != categoryFilters) {
            paramsMap.put(GoodsCheckDao.CATEGORY_FILTERS_PROP, categoryFilters);
        }
        if (null != brandFilters) {
            paramsMap.put(GoodsCheckDao.BRAND_FILTERS_PROP, brandFilters);
        }
        if (null != worksFilters) {
            paramsMap.put(GoodsCheckDao.WOEKS_FILTERS_PROP, worksFilters);
        }
		if (null != pageable) {
			paramsMap.put(MemberDao.OFFSET_PROP, pageable.getOffset());

			paramsMap.put(MemberDao.MAX_RESULTS_PROP, pageable.getPageSize());

			Sort sort = pageable.getSort();
			if (null != sort) {
				paramsMap.put(NewOrHotDao.SORT_PROP, sort);
			}
		}
		paramsMap.put("spotSell", spotSell);
        int total = count(paramsMap);
        List<GoodsCheck> data = total > 0 ? findMany(paramsMap) : Collections.<GoodsCheck>emptyList();

        return new SimplePage<GoodsCheck>(pageable, data, total);
	}

	@Override
	public void deleteByGoodsId(Long id) {
		goodsCheckDao.deleteByGoodsId(id);
	}

	@Override
	public BigDecimal getLogisticFee(BigDecimal logisticPrice, BigDecimal weight) {
		return isNull(logisticPrice).multiply(isNull(weight));
	}
	
	@Override
	public BigDecimal getRmbCostPrice(BigDecimal productPrice, BigDecimal rate) {
		return isNull(productPrice).divide(isNull(rate),2, BigDecimal.ROUND_UP);
	}

	@Override
	public BigDecimal getLandedGoodsCost(BigDecimal logisticFee, BigDecimal rmbCostPrice) {
		return isNull(logisticFee).add(isNull(rmbCostPrice));
	}

	@Override
	public BigDecimal getSellPrice(BigDecimal rmbCostPrice, BigDecimal sellGrossMargin, BigDecimal boxPoint,
			BigDecimal logisticFee) {
		return isNull(rmbCostPrice).divide((new BigDecimal(Integer.toString(1)).subtract(isNull(sellGrossMargin))).subtract(isNull(boxPoint)),2, BigDecimal.ROUND_UP).add(isNull(logisticFee));
	}

	@Override
	public BigDecimal getRetailPrice(BigDecimal rmbCostPrice, BigDecimal retailGrossMargin, BigDecimal logisticFee) {
		return isNull(rmbCostPrice).divide((new BigDecimal(Integer.toString(1)).subtract(isNull(retailGrossMargin))),
				2, BigDecimal.ROUND_UP).add(isNull(logisticFee));
	}

	@Override
	public BigDecimal getTaobaoPrice(BigDecimal rmbCostPrice, BigDecimal taobaoGrossMargin, BigDecimal logisticFee) {
		return isNull(rmbCostPrice).divide((new BigDecimal(Integer.toString(1)).subtract(isNull(taobaoGrossMargin))),
				2, BigDecimal.ROUND_UP).add(isNull(logisticFee));
	}

	@Override
	public BigDecimal getBoxPrice(BigDecimal rmbCostPrice, BigDecimal sellGrossMargin, BigDecimal logisticFee) {
		return isNull(rmbCostPrice).divide((new BigDecimal(Integer.toString(1)).subtract(isNull(sellGrossMargin))),
				2, BigDecimal.ROUND_UP).add(isNull(logisticFee));
	}
	
	@Override
	public BigDecimal isNull(BigDecimal bigDecimal){
		return bigDecimal == null ? new BigDecimal(Integer.toString(0)) : bigDecimal;
	}

	@Override
	public void updateChecked(Map<String,Object> map) {
		goodsCheckDao.updateChecked(map);
	}
}
