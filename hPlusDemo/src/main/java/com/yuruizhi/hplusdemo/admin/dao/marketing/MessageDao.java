/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月20日下午2:15:30
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Message;

import java.util.List;

/**
 * 
 * <p>名称: 私信Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日下午2:16:00
 * @version: 1.0
 */
@MyBatisMapper
public interface MessageDao extends BaseDao<Message> {


}
