/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.order;


import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.util.StringUtils;
import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.order.ProductReview;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.order.ProductReviewService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

/**
 * Created on 2016-03-03 11:37:24
 *
 * @author glanway copyer
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/productReview")
public class ProductReviewController extends AdminBaseController<ProductReview> {
	@Autowired
	private UserService userService;
	@Autowired
	private ProductReviewService productReviewService;
	@Autowired
	private SysLogService sysLogService;
	
	@RequestMapping("findDetailById")
	private String findDetailById(@RequestParam("id") Long id,Map<String,Object> model){
		ProductReview productReview = productReviewService.findDetailById(id);
		model.put("productReview",productReview);
		Long userId = UserUtils.getCurrentUserId();
		User user = userService.find(userId);
		model.put("userName",user.getLoginName());
		return getRelativeViewPath("detail");
	}
	@ResponseBody
	@RequestMapping("reply")
	public String reply(Long id,String reply,Boolean isVisible){
		ProductReview review = productReviewService.find(id);
		if(id != null&&!StringUtils.isEmpty(reply)){
			Map<String,Object> params = Maps.newHashMap();
			params.put("reply", reply);
			params.put("isVisible", isVisible);
			params.put("replyDate", new Date());
			productReviewService.update(id, params);
			
			// update 2016.4.19 qinzhongliang 回复评论时添加日志
			if(null != review){
				sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, 
						"商品评论-修改-对会员ID:"+review.getMemberId()+"产品ID:"+review.getProductId()+"进行了回复", "商品评论管理");
			}
			/*ProductReview review = productReviewService.find(id);
			review.setReply(reply);
			review.setIsVisible(isVisible);
			review.setReplyDate(new Date());
			productReviewService.update(review);*/
			return "success";
		}else{
			return "error";
		}
	}
	
	/**
	 * 
	 * <p>名称：选中前台隐藏</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param id
	 * @return
	 */
	@RequestMapping("hide")
	@ResponseBody
	public Map<String, Object> hide(@RequestParam("id") Long[] id) {
		ProductReview review = null;
		for (int i = 0; i < id.length; i++) {
			review = productReviewService.find(id[i]);
			review.setIsVisible(false);
			productReviewService.update(review);
		}
		return ImmutableMap.<String, Object> of("success", true);
	}
}
