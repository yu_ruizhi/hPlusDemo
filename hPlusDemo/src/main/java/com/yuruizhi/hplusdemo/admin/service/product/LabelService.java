/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.product.Label;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: LabelService</p>
 * <p>说明: 商品标签服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:04:42   
 * @version: 1.0
 */
public interface LabelService extends BaseService<Label>{
	/**
	 * <p>名称：findManyByProductId</p> 
	 * <p>描述：通过产品Id获取标签列表</p>
	 * @author：ChenGuang
	 * @param id
	 * @return
	 */
    List<Label> findManyByProductId(Long id);
    
	/**
	 * 
	 * <p>名称：根据名称获取数据</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
    List<Label> findByName(String name);
}
