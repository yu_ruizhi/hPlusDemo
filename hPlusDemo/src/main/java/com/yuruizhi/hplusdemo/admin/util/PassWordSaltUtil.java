/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;


import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p>名称: PassWordSaltUtil</p>
 * <p>说明: TODO 密码操作的工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月24日下午4:33:30   
 * @version: 1.0
 */
@Component
public final class PassWordSaltUtil {


    //todo 根据用户密码和创建时间进行加盐操作

   public String  passWordSalt(String password,Date createDate){
       return CipherUtil.generatePassword(CipherUtil.generatePassword(password + createDate.getTime()) + createDate.getTime());
   }


    //todo 判断密码是否一致

    public Boolean  equalPassword(String dataBasePassword,String loginPassword,Date createDate){  //一样为真
        boolean isEqualPassword=false;
        if(CipherUtil.generatePassword(CipherUtil.generatePassword(loginPassword+createDate.getTime())+createDate.getTime()).equals(dataBasePassword))
            isEqualPassword=true;
        return isEqualPassword;
    }

    //todo 格式化存入数据库的时间
    public  Date getFormatDate(Date dateTime){
        try{
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String date=simpleDateFormat.format(dateTime);
            return simpleDateFormat.parse(date);
        }catch(Exception e){
            return new Date();
        }
    }

}

