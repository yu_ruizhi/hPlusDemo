/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailMember;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: EmailMemberService</p>
 * <p>说明: 邮件会员关系服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:15:57   
 * @version: 1.0
 */
public interface EmailMemberService extends BaseService<EmailMember> {
}
