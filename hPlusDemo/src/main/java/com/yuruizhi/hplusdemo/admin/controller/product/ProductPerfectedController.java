package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.*;
import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.product.*;
import com.yuruizhi.hplusdemo.admin.service.provider.SupplierService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;
import com.yuruizhi.hplusdemo.admin.util.ZipUtil;
import com.yuruizhi.hplusdemo.admin.validate.ProductValidate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.ponly.common.util.FileUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/*import com.yuruizhi.hplusdemo.admin.entity.member.Provider;*/
/*import com.yuruizhi.hplusdemo.admin.service.member.ProviderService;*/

/**
 * Product Management Controller
 *
 * @author vacoor
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/productPerfected")
public class ProductPerfectedController extends BaseController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ProductImgService productImgService;
    @Autowired
    private LabelService labelService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private ModelService modelService;
    @Autowired
    private WorksService worksService;
    @Autowired
    private SupplierService supplierService;
    /*
     * @Autowired private DetailTemplateService templateService;
     */
    @Autowired
    private ProductAttributeValueService pavService;
    @Autowired
    private SysLogService sysLogService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private SpecValueService speValueService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AttributeService attributeService;
    @Autowired
    private AttributeValueService attrbuteValueService;
    @Autowired
    private SpecService specService;
    @Autowired
    private GoodsCheckService goodsCheckService;
    
    
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
        binder.registerCustomEditor(Category.class, "category", new CustomStringBeanPropertyEditor(Category.class));
        binder.registerCustomEditor(Model.class, "model", new CustomStringBeanPropertyEditor(Model.class));
        binder.registerCustomEditor(Brand.class, "brand", new CustomStringBeanPropertyEditor(Brand.class));
        binder.registerCustomEditor(Works.class, "works", new CustomStringBeanPropertyEditor(Works.class));
  /*      binder.registerCustomEditor(Provider.class, "provider", new CustomStringBeanPropertyEditor(Provider.class));*/
        binder.registerCustomEditor(List.class, "labels", new CustomStringCollectionBeanCollectionPropertyEditor(
                List.class, Label.class
        ));
    }
	
    @RequestMapping("index")
    public void index() {
    }

    @ResponseBody
    @RequestMapping("list")
    public Page<Product> list(@Qualifier("B.") Filters brandFilters, @Qualifier("M.") Filters modelFilters,
                              @Qualifier("C.") Filters categoryFilters, @Qualifier("W.") Filters worksFilters,
                              @Qualifier("PR.") Filters supplierFilters,@Qualifier("P.") Filters productFilters,
                              @Qualifier("R.") Filters rFilters,
                              @RequestParam(value = "goodsCode", required = false) String goodsCode,
                              Pageable pageable) {
    	
        return productService.findPage(productFilters, brandFilters, modelFilters,
        		                                      categoryFilters,worksFilters,supplierFilters, pageable,rFilters, goodsCode,"perfected" );
    }

    @RequestMapping("add")
    public void add(Map<String, Object> model) {
        // model.put("groupedBrands", getGroupedBrands());
        model.put("labels", labelService.findAll());
        model.put("baseModel", modelService.findBaseModel());
        model.put("providerList", supplierService.findAll());
        model.put("worksList", worksService.findAll());
        model.put("brandList", brandService.findMany("enabled", true));

        /* model.put("templates", templateService.findAll()); */
    }

	/*
	 * 保存商品
	 */
    @RequestMapping("save")
    public String save(Product product,
                       @RequestParam(value = "attributeValueId", required = false) String[] attrValIds,
                       @RequestParam(value = "value", required = false) String[] value, RedirectAttributes redirectAttr) {

        attrValIds = clean(attrValIds);

        clean(product);
        productService.saveOrUpdate(product);
        redirectAttr.addFlashAttribute("message", "保存成功");
        return redirectViewPath("index");
    }

    @RequestMapping("edit/{id}")
    public String edit(@PathVariable("id") Long id, Map<String, Object> model) {
        Product product = productService.find(id);
        if (null == product) {
            return null;
        }

        Category category = product.getCategory();
        if (category != null) {
            model.put("category", category);
            //获取自定义模型属性值处理
            Model m = category.getModel();
            if (m != null) {
                m = modelService.findDetail(m.getId());
                List<Attribute> attributes = m.getAttributes();
                attributes = null != attributes ? attributes : Collections.<Attribute>emptyList();
                for (int j = 0; j < attributes.size(); j++) {
                    Attribute attribute = attributes.get(j);
                    if (1 == attribute.getDisplayType()) {
                        ProductAttributeValue pav = new ProductAttributeValue();
                        pav.setAttribute(attribute);
                        pav.setProduct(product);
                        if (null != pavService.getAttributeValueByProductIdAndAttributeId(pav)) {
                            attribute.setAttributeValues(
                                    null == pavService.getAttributeValueByProductIdAndAttributeId(pav) ? null
                                            : pavService.getAttributeValueByProductIdAndAttributeId(pav)
                                            .getAttributeValueList());
                        } else {
                            attribute.setAttributeValues(null);
                        }

                    }
                }
                if(m.getParameters()!=null&&m.getParameters().size()>0){
                    List<Parameter> parameters=new ArrayList<Parameter>();
                    for(Parameter parameter:m.getParameters()){
                        if(null!=parameter.getChildren()&&parameter.getChildren().size()>0){
                            parameters.add(parameter);
                        }
                    }
                    m.setParameters(parameters);
                }
                model.put("model", m);
            }
        }
      //获取基本模型属性值处理
        Model base = modelService.findBaseModel();
        List<Attribute> attributes = base.getAttributes();
        for (int i = 0; i < attributes.size(); i++) {
            Attribute attribute = attributes.get(i);
            if (attribute.getDisplayType() == 1) {
                ProductAttributeValue pav = new ProductAttributeValue();
                pav.setAttribute(attribute);
                pav.setProduct(product);
                pav = pavService.getAttributeValueByProductIdAndAttributeId(pav);
                attribute.setAttributeValues(null == pav ? null : pav.getAttributeValueList());
            }
        }
        // 商品标签
        List<Label> labelList = labelService.findManyByProductId(product.getId());
        product.setLabels(labelList);
        model.put("product", product);
        // model.put("groupedBrands", getGroupedBrands());
        model.put("labels", labelService.findAll());
        // model.put("baseModel", modelService.findBaseModel());
        model.put("baseModel", base);
        /* model.put("templates", templateService.findAll()); */

        Filters filter = Filters.create().eq("product_id", product.getId());
        List<Goods> goods = goodsService.findMany(filter, (Pageable) null);

        //现在即使不启用规格，单品表中也是有一条数据的
        /*final int size = null != goods ? goods.size() : 0;
        boolean specEnabled = true;
        if (1 > size) {
            specEnabled = false;
        } else if (1 == size) {
            Boolean isDefault = goods.get(0).getIsDefault();
            if(null != isDefault && isDefault) {
                specEnabled = false;
            }
        }
        product.setEnableSpecs(specEnabled);*/

        model.put("goods", goods);

        StringBuilder buff = new StringBuilder();
        if (null != goods) {
            for (int i = 0; i < goods.size(); i++) {
                Goods g = goods.get(i);
                List<SpecValue> values = g.getSpecValues();
                if (null != values) {
                    for (SpecValue value : values) {
                        buff.append(value.getId()).append(",");
                    }
                }
            }
            if (buff.length() > 0) {
                buff.deleteCharAt(buff.length() - 1);
            }
        }
        model.put("selectedSpecValueIds", buff.toString());
        
        model.put("worksList", worksService.findAll());
        model.put("brandList", brandService.findMany("enabled", true));
       model.put("providerList", supplierService.findAll());

        return getRelativeViewPath("edit");
    }

    private String[] clean(String[] array) {
        List<String> values = Lists.newArrayList();
        for (String value : values) {
            if (StringUtils.hasText(value)) {
                values.add(value);
            }
        }
        return values.toArray(new String[values.size()]);
    }

    private void clean(Product product) {
        List<ParameterValue> pvals = product.getParameterValues();
        if (null != pvals) {
            Iterator<ParameterValue> it = pvals.iterator();
            while (it.hasNext()) {
                ParameterValue value = it.next();
                Parameter param = value.getParameter();
                if (null == param || null == param.getId()) {
                    it.remove();
                }
            }
        }
		/* 没有站点了 */
		/* cleanTransients(product.getWebsites()); */
        List<ProductImg> images = product.getProductImgs();
        if (!CollectionUtils.isEmpty(images)) {
            Iterator<ProductImg> it = images.iterator();
            while (it.hasNext()) {
                ProductImg image = it.next();
                if (null == image || (null == image.getId() && !StringUtils.hasText(image.getPath()))) {
                    it.remove();
                }
            }
        }
    }

    @ResponseBody
    @RequestMapping("delete")
    public Map<String, Object> delete(@RequestParam("id") Long[] ids) {
        productService.delete(ids);
        StringBuffer idStr = new StringBuffer();
        for(Long id : ids){
        	idStr.append(id + ",");
        }
        idStr.deleteCharAt(idStr.length()-1);
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "商品管理-删除-ID:" + idStr , "商品管理");
        return ImmutableMap.<String, Object>of("success", true);
    }
    
    /**
     * 
     * <p>名称：检测单品编码的唯一性</p> 
     * <p>描述：</p>
     * @author：wentan
     * @param goodsId
     * @param code
     * @return
     */
    @RequestMapping("checkIsGoodsCodeExist")
    @ResponseBody
    public Map<String, Object> checkIsGoodsCodeExist(Long goodsId, String code) {
    	Filters filters = Filters.create();
		if (null != goodsId) {
			filters.ne("id", goodsId);
		}
		if (StringUtils.hasText(code)) {
			filters.eq("code", code);
		}
		Boolean isExist = goodsService.count(filters) >= 1;
		Map<String, Object> model = Maps.newHashMap();
		model.put("isExist", isExist);
		return model;
    }
    
    /**
     * 
     * <p>名称：检测商品编码的唯一性</p> 
     * <p>描述：</p>
     * @author：wentan
     * @param code
     * @return
     */
    @RequestMapping("checkIsCodeExist")
    @ResponseBody
    public Boolean checkIsCodeExist(Long id, String code) {
    	Filters filters = Filters.create();
		if (null != id) {
			filters.ne("id", id);
		}
		if (StringUtils.hasText(code)) {
			filters.eq("code", code);
		}
		return productService.count(filters) < 1;
    }
    
    /**
     * 
     * <p>名称：批量修改再贩次数</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param ids
     * @param reSellingNum
     * @return
     */
    @ResponseBody
    @RequestMapping("updateReSellingNum")
    public Map<String,Object> updateReSellingNum(@RequestParam("id") Long[] ids,Integer reSellingNum,String registerDate,String reserveDeadline) {
    	Map<String,Object> map = Maps.newHashMap();
    	for (Long id : ids) {
    		Product product = productService.find(id);
    		int oldReSellingNum = (null ==  product.getReSellingNum() ? 0 :  product.getReSellingNum());
    		product.setReSellingNum(oldReSellingNum + reSellingNum);
    		product.setIsReSelling(true);
            if(null!=registerDate&&registerDate.trim().length()>0) {
                try {
                    product.setRegisterDate(new SimpleDateFormat("yyyy-MM-dd").parse(registerDate));
                } catch (Exception e) {
                    product.setRegisterDate(new Date());
                }
            }
    		productService.update(product);
    	}
        try {
            map.put("reserveDeadline",new SimpleDateFormat("yyyy-MM-dd").parse(reserveDeadline));
        } catch (Exception e) {
            map.put("reserveDeadline",new Date());
        }
        map.put("productIds",ids);
goodsService.updateReserveDeadline(map);
        map.remove("reserveDeadline");
        map.remove("productIds");
    	map.put("success", true);
    	return map;
    }
    
    /**
     *
     * <p>名称：商品导出</p>
     * <p>描述：TODO未完成</p>
     * @author：wuqi
     * @param ids
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("exportData")
    public void exportData(@RequestParam(value="idArr") Long[] ids,HttpServletRequest request,HttpServletResponse response) {
        String[] headers = {"商品编号","商品名称","商品状态","商品规格","商品参数","商品类别","商品品牌","所属作品","供应商名称","日元成本价","人民币销售价","设计师",
                "日语名称","再贩次数","库存","截止时间","发售时间","购买范围","预定数量","预定截止时间","重量(kg)","库存预警(:以下)","商品定位"};
        List<String> productList = new ArrayList<>();
        Product product = new Product();
        List<Goods> goodsList = null;
        String str = "";
        for (Long id : ids) {
            product = productService.find(id);
            goodsList = goodsService.findByProductId(id);
            searchStatus(productList, goodsList, product);
        }
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
        CSVUtil.exportCsv(headers, productList, response, fileName);;

    }

    /**
     *
     * <p>名称：根据商品截止日期商品状态导出</p>
     * <p>描述：</p>
     * @author yuruizhi
     * @param cutOffStartTime
     * @param cutOffEndTime
     * @param status
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("searchGoodsExport")
    public void searchGoodsExport(
            @RequestParam(required = false ,defaultValue = "") String cutOffStartTime,
            @RequestParam(required = false ,defaultValue = "") String cutOffEndTime,
            @RequestParam(value="status") int status,
            HttpServletRequest request,
            HttpServletResponse response) {
    	String[] headers = {"商品ID","商品编码","商品标题","商品类型","是否上架","商品类别","商品品牌","所属作品","日元成本价","人民币销售价","截止时间","发售时间","库存"};
    	//查询商品 status:1-现货 spot；2-预定 reserved；3-限购 limited
        Map<String,Object> parmsMap = new HashMap<String,Object>();
        if(0 == status){
        	epExcel(headers,null,response);
        	return;
        }else{
            if(1 == status){
                parmsMap.put("spot", 1);
                parmsMap.put("reserved", null);
                parmsMap.put("limited", null);
            }else if(2 == status){ //字符串null
            	if(!"".equals(cutOffStartTime) && !"null".equals(cutOffStartTime) ){
            		parmsMap.put("cutOffStartTime",cutOffStartTime);
            	}
            	if(!"".equals(cutOffEndTime) && !"null".equals(cutOffEndTime)){
            		parmsMap.put("cutOffEndTime",cutOffEndTime);
            	}
                parmsMap.put("spot", null);
                parmsMap.put("reserved", 1);
                parmsMap.put("limited", null);
            }else if(3 == status){
                parmsMap.put("spot", null);
                parmsMap.put("reserved", null);
                parmsMap.put("limited", 1);
            }
        }
        List<Map<String,Object>> goodsList = productService.searchGoodsExcel(parmsMap);
        
        epExcel(headers,goodsList,response);
    }

    private void searchStatus(List<String> productList, List<Goods> goodsList, Product product) {
        String str;//预定商品
        if (true == product.getReserved()) {
            for (Goods goods : goodsList) {
                str = goodsStr(goods, product, "预定");
                productList.add(str);
            }
        }
        //限购商品
        if (true == product.getLimited()) {
            for (Goods goods : goodsList) {
                str = goodsStr(goods, product, "限购");
                productList.add(str);
            }
        }
        //现货商品
        if (true == product.getSpot()) {
            for (Goods goods : goodsList) {
                str = goodsStr(goods, product, "现货");
                productList.add(str);
            }
        }
    }
    
    private void epExcel(String[] str,List<Map<String,Object>> goodsList,HttpServletResponse response){

    	HSSFWorkbook workbook = new HSSFWorkbook();
    	HSSFSheet sheet = null ;
    	HSSFRow row = null ;
    	
        if(null != goodsList){
        	int j=1;
        	int count = 30000;
        	for(int i=0;i<goodsList.size();i++){
        		if(i==0){
        			sheet = workbook.createSheet("sheet1");
           	        row = sheet.createRow(0);
    	       	    for(int k=0;k<str.length;k++){
    	       	    	row.createCell(k).setCellValue(str[k]);
    	            }
        		}else{
        			if(i%count==0){
            			j++;
            			sheet = workbook.createSheet("sheet"+ j);
            			row = sheet.createRow(0);
         	       	    for(int k=0;k<str.length;k++){
         	       	    	row.createCell(k).setCellValue(str[k]);
         	            }
            		}
        		}
            	row = sheet.createRow(i%count+1);
            	row.createCell(0).setCellValue(goodsList.get(i).get("GOODS_ID").toString()); 
            	row.createCell(1).setCellValue(goodsList.get(i).get("GOODS_CODE").toString()); 
            	row.createCell(2).setCellValue(goodsList.get(i).get("GOODS_TITLE").toString()); 
            	row.createCell(3).setCellValue(goodsList.get(i).get("GOODS_TYPE").toString()); 
            	row.createCell(4).setCellValue(goodsList.get(i).get("ON_SELL").toString()); 
            	row.createCell(5).setCellValue(goodsList.get(i).get("CATEGORY_NAME").toString()); 
            	row.createCell(6).setCellValue(goodsList.get(i).get("BRAND_NAME").toString()); 
            	row.createCell(7).setCellValue(goodsList.get(i).get("WORK_NAME").toString()); 
            	row.createCell(8).setCellValue(goodsList.get(i).get("GOODS_PRICE").toString()); 
            	row.createCell(9).setCellValue(goodsList.get(i).get("GOODS_MARKET_PRICE").toString()); 
            	row.createCell(10).setCellValue(goodsList.get(i).get("RESERVE_DEADLINE").toString()); 
            	row.createCell(11).setCellValue(goodsList.get(i).get("REGISTER_DATE").toString()); 
            	row.createCell(12).setCellValue(goodsList.get(i).get("QTY").toString()); 
            }
        }
        
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";

        OutputStream ouputStream = null;
 		try {
 			response.setContentType("application/vnd.ms-excel");   
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);   
 		    ouputStream = response.getOutputStream();   
 		    workbook.write(ouputStream);   
 		    ouputStream.flush();   
 		    ouputStream.close();  
 		    if(ouputStream!=null)ouputStream.flush();
 			if(ouputStream!=null)ouputStream.close();

 		}  catch (IOException e) {
         
        }finally {
        	try {
        		if(ouputStream!=null)ouputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         }
    }

    /**
     * 
     * <p>名称：根据商品状态分类</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param goods
     * @param product
     * @param spot
     * @return
     */
    private String goodsStr(Goods goods,Product product,String spot) {
    	 String str = goods.getCode() + "\t," +
					  goods.getTitle() + "\t," +
					  spot + "," +
					  getSvStr(goods.getSvStr()) + "," +
					  getParameter(product.getParameterValues()) + "," +
					  (null == product.getCategory() ? "" : product.getCategory().getName()) + "," +
					  (null == product.getBrand() ? "" : product.getBrand().getName()) + "," +
					  (null == product.getWorks() ? "" : product.getWorks().getName()) + "," +
					  (null == goods.getSuppliers() ? "" : goods.getSuppliers()) + "," +
					  (null == goods.getPrice() ? "" : goods.getPrice()) + "," +
					  (null == goods.getMarketPrice() ? "" : goods.getMarketPrice()) + "," +
					  product.getDesigner() + "," +
					  product.getNameJp() + "," +
					  (null == product.getBoxNumber() ? "" : product.getBoxNumber()) + "," +
					  (null == product.getReSellingNum() ? "" : product.getReSellingNum()) + "," +
					  (null == goods.getStock() ? "" : goods.getStock()) + "," +
					  (null ==  product.getSalesOffDate() ? "" : DateUtils.date2Str( product.getSalesOffDate(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
					  (null ==  product.getRegisterDate() ? "" : DateUtils.date2Str( product.getRegisterDate(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
					  getScope(product.getRetail(), product.getDealer()) + "," +
					  (null == goods.getReserveNum() ? "" : goods.getReserveNum()) + "," +
					  (null == goods.getReserveDeadline() ? "" : DateUtils.date2Str(goods.getReserveDeadline(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS)) + "," +
					  goods.getWeight() + "," +
					  goods.getStockForewarn() + "," +
					  getTrend(goods.getTrend());
    	 return str;
    }
    
    /**
     * 
     * <p>名称：商品购买范围</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param retail
     * @param dealer
     * @return
     */
    private String getScope (Boolean retail,Boolean dealer) {
    	String str = "";
    	if (true == retail) {
    		str += "零售;";
    	}
    	if (true == dealer) {
    		str += "经销商;";
    	}
    	str = str.substring(0, str.length()-1);
    	return str;
    }
    
    /**
     * 
     * <p>名称：商品定位</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param trend
     * @return
     */
    private String getTrend (Integer trend) {
    	if (new Integer(0).equals(trend)) {
    		return "无差别";
    	} else if (new Integer(1).equals(trend)) {
    		return "趋向男性";
    	} else if (new Integer(2).equals(trend)) {
    		return "趋向女性";
    	} else {
    		return "";
    	}
    }
    
    /**
     * 
     * <p>名称：商品规格</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param svStr
     * @return
     */
    private String getSvStr(String svStr) {
    	String str = "";
    	if(null != svStr) {
    		String[] specList = svStr.split(";");
        	for (int i=0; i<specList.length;i++) {
        		String[] specValueList = specList[i].split(":");
        		String id = specValueList[1];
        		SpecValue specValue = speValueService.specValueBean(id);
        		str += (null == specValue.getSpec() ? "" : specValue.getSpec().getName()) + ":" +
        			   (null == specValue.getName() ? "" : specValue.getName()) + ";";
        	}
    	}
    	return str;
    }
    
    /**
     * 
     * <p>名称：商品参数</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param parameterValueList
     * @return
     */
    private String getParameter (List<ParameterValue> parameterValueList) {
    	String str = "";
    	for (ParameterValue parameterValue : parameterValueList) {
    		String name = (null == parameterValue.getParameter() ? "" : parameterValue.getParameter().getName());
    		String value = (null == parameterValue.getValue() ? "" : parameterValue.getValue());
    		str += name + ":" + value + ";";
    	}
    	return str;	
    }

    /**
     *
     * <p>名称：商品模型下载</p>
     * <p>描述：</p>
     * @author：wuqi
     * @param request
     * @param response
     */
    @RequestMapping("downLoadModel")
    public void downLoadModel (HttpServletRequest request,HttpServletResponse response) {
        String[] headers = {"商品名称","商品编号","商品状态","商品品牌","所属作品","商品标签","入箱数","日元成本价","发售日期","是否再贩","再贩次数","设计师",
                "日语名称","购买范围","排序","所属分类","属性","是否启用规格","规格","商品标题","商品编号","供应商名称","日元成本价","库存",
                "限购数量","预定截止时间","重量(kg)","库存预警(:以下)","商品定位"};
        List<String> dataList = new ArrayList<>();
        String data = "火影忍者衬衫,H10000001,限购|现货|预定,有妖气,火影忍者,现货,11,100,2016/1/22,是,2,岸本,火影忍者のシャツ,零售|经销商,1,手办模型,产地:东京大阪|是否艾漫出品:否,是,"
                + "颜色:红色|材质:塑料|高度:10CM,火影忍者衬衫,H0000001,xxxx供应商,100,20,20,2016/12/22,0.2,2,无差别";
        dataList.add(data);
        data = ",,,,,,,,,,,,,,,,,,颜色:蓝色|材质:橡胶|高度:20CM,火影忍者衬衫,H0000001,xxxx供应商,100,20,20,2016/12/22,0.2,2,趋向男性";
        dataList.add(data);
        data = ",,,,,,,,,,,,,,,,,,颜色:白色|材质:铝制|高度:10CM,火影忍者衬衫,H0000001,xxxx供应商,100,20,20,2016/12/22,0.2,2,趋向女性";
        dataList.add(data);
        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".xls";
        ExcelUtil.exportExcel(headers, dataList, response, fileName);
    }

    /**
     *
     * <p>名称：再贩单品导入模型</p>
     * <p>描述：</p>
     * @author：wuqi
     * @param request
     * @param response
     */
    @RequestMapping("goodsZaifan")
    public void goodsZaifan (HttpServletRequest request,HttpServletResponse response) {
        String [] headers={"单品货号","供应商编号","截单日期","发售日期"};
        List<Goods> goodsList=new ArrayList<Goods>();
        for(int i=0;i<1000;i++) {
            Goods goods = new Goods();
            goods.setCode(null);
            goods.setTitle(null);//充个数
            goods.setCreatedBy(null);//充个数
            goods.setLastModifiedBy(null);//充个数
            goodsList.add(goods);
        }
        String [] methosName={"code","title","createdBy","lastModifiedBy"};
        try {
        ExcelUtil.outExcel("再贩单品导入模板", headers, goodsList, methosName, response);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    @ResponseBody
    @RequestMapping(value = "/zaifandaoru",  method = RequestMethod.POST)
    public Map<String,Object> zaifandaoru(@RequestPart("file") MultipartFile multipart,
                                         HttpServletRequest request) throws IOException {
        Map<String,Object> map=new HashMap<String,Object>();
        InputStream inputStream = multipart.getInputStream();
        List<String> list = ExcelUtil.importExcel(inputStream, 1, 4,"zc");
        StringBuilder moreGoods=new StringBuilder("");
        StringBuilder notFindGoods=new StringBuilder("");
        StringBuilder notFindGoodsCode=new StringBuilder("");
        int  count=0;
        
        if(null == list || list.size() < 1){
        	map.put("error", "模板内容为空");
            return map;
        }
        String content[] = null;
        for(int i=1;i<list.size();i++){
        	
        	if(null == list.get(i)){
        		continue;
        	}
        	content = list.get(i).split(",");
        	if(content.length != 4 || content[0].trim().length()<=0){
        		continue;
        	}
        	
            map.put("goodsCode",content[0].trim());
            map.put("suppCode",content[1].trim());
            int retCount = goodsService.findGoodsCount(map);
            if(retCount <= 0){
            	//商品未找到
            	notFindGoods.append("<br/>商品编号"+content[0].trim()+",供应商编号"+content[1].trim()+"  未能找到对应商品");
            	notFindGoodsCode.append(content[0].trim()+",");
            }else if(retCount == 1){
            	
            	try {
					map.put("reserveDeadline",new SimpleDateFormat("yyyy-MM-dd").parse(content[2].trim()));
					goodsService.updategoods(map);
	                map.put("registerDate",new SimpleDateFormat("yyyy-MM-dd").parse(content[3].trim()));
	                map.put("reSellingNum",1);
	                goodsService.updateproduct(map);
	                goodsCheckService.updateChecked(map);
	                count++;
				} catch (ParseException e) {
					e.printStackTrace();
					map.put("error", "导入异常，可能是因为数据格式等不正确引起");
			        return map;
				}
                
            }else {
            	//商品重复
            	moreGoods.append("<br/>商品编号"+content[0].trim()+",供应商编号"+content[1].trim()+" 存在重复货号，无法再贩");
            }

        }
        map.put("error", "成功导入"+count+"个商品"+notFindGoods+moreGoods);
        map.put("notFindGoodsCode", notFindGoodsCode);
        
        return map;
    }
    
    
    @RequestMapping(value = "/exportNotFindGoods",  method = RequestMethod.POST)
    public void exportNotFindGoods(String notFindGoodsCode,HttpServletRequest request,HttpServletResponse response) {
    	HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("sheet1");
        HSSFCellStyle style = workbook.createCellStyle();
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 创建一个居中格式
        HSSFRow row = sheet.createRow(0);
        row.setRowStyle(style);
          
        row.createCell(0).setCellValue("商品货号");
        String[] arrCode = notFindGoodsCode.split(",");
     	for(int i=0;i<arrCode.length;i++){
         	row = sheet.createRow(i+1);
         	row.createCell(0).setCellValue(arrCode[i]); 
        }

        String fileName = "product" + DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
        OutputStream ouputStream = null;
  		try {
  			response.setContentType("application/vnd.ms-excel");   
             response.setHeader("Content-disposition", "attachment;filename=" + fileName);   
  		    ouputStream = response.getOutputStream();   
  		    workbook.write(ouputStream);   
  		    ouputStream.flush();   
  		    ouputStream.close();  
  		    if(ouputStream!=null)ouputStream.flush();
  			if(ouputStream!=null)ouputStream.close();

  		}  catch (IOException e) {
          
        }finally {
         	try {
         		if(ouputStream!=null)ouputStream.close();
 			} catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
        }
    }
     
    
    @ResponseBody
    @RequestMapping(value = "/importData",  method = RequestMethod.POST)
    public Map<String,Object> importData(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	Map<String,Object> map = Maps.newHashMap();
    	InputStream inputStream = multipart.getInputStream();
    	List<String> list = ExcelUtil.importExcel(inputStream, 1, 29);
    	String error = "";
    	if (null != list && 0 < list.size()) {
    		Product product = new Product();
    		List<Goods> goodsList = new ArrayList<>();
    		Boolean productSave = true;
    		for (int i=0;i<list.size();i++) {
    			String data[] = null;
    			Goods goods = new Goods();
    			Boolean goodsSave = true;
    			if (null != list.get(i)) {
    				data = list.get(i).split(",");
    				//商品名称
    				if (null != data[0] && !"".equals(data[0])) {
    					if (null != product.getTitle()) {
    						if (productSave) {
        			    		product.setGoods(goodsList);
        			    		productService.saveOrUpdate(product);
    						}
    			    		product = new Product();
    			    		goodsList = new ArrayList<>();
    			    		productSave = true;
    			    		goodsSave = true;
    					}
    					if (250 < data[0].length()) {
							error += "第"+(i+1)+"行数据导入失败，原因为商品名称超过250个字符<br>";
							productSave = false;
							continue;
						}
    					product.setTitle(data[0]);
    				}
    				//商品品牌
    				if (null != data[3] && !"".equals(data[3])) {
    					List<Brand> brands = brandService.findByName(data[3]);
    					if (null != brands && 0 < brands.size()) {
    						product.setBrand(brands.get(0));
    					} else {
    						error += "第"+(i+1)+"行数据导入失败，原因为商品品牌错误<br>";
    						productSave = false;
							continue;
    					}
    				}
    				//所属作品
    				if (null != data[4] && !"".equals(data[4])) {
    					List<Works> works = worksService.findByName(data[4]);
    					if (null != works && 0 < works.size()) {
    						product.setWorks(works.get(0));
    					} else {
    						error += "第"+(i+1)+"行数据导入失败，原因为商品所遇作品错误<br>";
    						productSave = false;
							continue;
    					}
    				}
    				//商品标签
    				if (null != data[5] && !"".equals(data[5])) {
    					List<Label> labels = labelService.findByName(data[5]);
    					if (null != labels && 0 < labels.size()) {
    						product.setLabels(labels);
    					} else {
    						error += "第"+(i+1)+"行数据导入失败，原因为商品标签错误<br>";
    						productSave = false;
							continue;
    					}
    				}
    				//基础数据验证
    				Map<String,Object> productMap = ProductValidate.productValidate(product, data, error, i, productSave);
    				product = (Product) productMap.get("product");
    				error = (String) productMap.get("error");
    				productSave = (Boolean) productMap.get("productSave");
    				
    				//所属分类
    				if (null != data[15] && !"".equals(data[15])) {
    					List<Category> categories = categoryService.findByName(data[15]);
    					if (null != categories && 0 < categories.size()) {
    						product.setCategory(categories.get(0));
    					} else {
    						error += "第"+(i+1)+"行数据导入失败，原因为商品所属分类错误<br>";
    						productSave = false;
    						continue;
    					}
    				}
    				//属性
    				if (null != data[16] && !"".equals(data[16])) {
    					String values[] = data[16].split("\\|");
    					//获取到属性名
    					if (null != product.getCategory() && null != product.getCategory().getModel()) {
    						List<AttributeValue> attributeValues = new ArrayList<>();
    						for (int j=0;j<values.length;j++) {
        						String value = values[j].split(":")[0];
        						Attribute attribute = new Attribute();
        						AttributeValue attributeValue = new AttributeValue();
        						//是否为基础数据
        						attribute = attributeService.findByNameAndIsBase(value);
        						if (null != attribute) {
        							if (1 == attribute.getDisplayType()) {
        								attributeValue.setValue(values[j].split(":")[1]);
            							attributeValue.setAttribute(attribute);
        							} else {
        								attributeValue = attrbuteValueService.findByValueAndAttrId(values[j].split(":")[1], attribute.getId());
        								attributeValue.setAttribute(attribute);
        							}
            						attributeValues.add(attributeValue);
        						} else {
        							attribute = attributeService.findByNameAndModelId(value, product.getCategory().getModel().getId());
            						if (null != attribute) {
                						if (1 == attribute.getDisplayType()) {
                							attributeValue.setValue(values[j].split(":")[1]);
                							attributeValue.setAttribute(attribute);
                						} else {
                							attributeValue = attrbuteValueService.findByValueAndAttrId(values[j].split(":")[1], attribute.getId());
                						}
                						attributeValues.add(attributeValue);
            						} 
        						}
        					}
    						product.setAttributeValues(attributeValues);
    					} else {
    						error += "第"+(i+1)+"行数据导入失败，原因为商品属性错误<br>";
    						productSave = false;
    						continue;
    					}
    					
     				}
    				
    				if (true == productSave) {
    					//规格
        				if (null != data[18] && !"".equals(data[18])) {
        					String values[] = data[18].split("\\|");
        					List<SpecValue> specValues = new ArrayList<>();
        					for (int j=0;j<values.length;j++) {
        						//获取规格名称
        						String str = values[j].split(":")[0];
        						//获取规格值
        						String value = values[j].split(":")[1];
        						if ((null != str && !"".equals(str)) || (null != value && !"".equals(value))) {
        							List<Spec> specs = specService.findByName(str);
        							if (null != specs && 0 < specs.size()) {
        								SpecValue specValue = speValueService.findBySpecIdAndName(specs.get(0).getId(), value);
        								if (null != specValue) {
        									specValues.add(specValue);
        								} else {
        									error += "第"+(i+1)+"行数据导入失败，原因为商品规格错误<br>";
        		    						goodsSave = false;
        								}
        							} else {
        								error += "第"+(i+1)+"行数据导入失败，原因为商品规格错误<br>";
        								goodsSave = false;
        							}
        						} else {
        							error += "第"+(i+1)+"行数据导入失败，原因为商品规格错误<br>";
        							goodsSave = false;
        						}
        					}
        					goods.setSpecValues(specValues);
        				}

        				//供应商名称
        				if (null != data[21] && !"".equals(data[21])) {
        					List<Supplier> supplier = supplierService.findBySupplierName(data[21]);
        					if (null != supplier&&supplier.size()>0) {
        						goods.setSupplierId(supplier.get(0).getId());
        					} else {
        						error += "第"+(i+1)+"行数据单品供应商无法找到<br>";
//        						continue;
        					}
        				}
        				
        				Map<String,Object> goodsMap = ProductValidate.goodsValidate(data, error, goodsSave, goods, i);
        				goods = (Goods) goodsMap.get("goods");
        				error = (String) goodsMap.get("error");
        				goodsSave = (Boolean) goodsMap.get("goodsSave");
        				if (goodsSave) {
            				goodsList.add(goods);
        				}
    				}
    			}
    		}
    		if (productSave) {
        		product.setGoods(goodsList);
        		productService.saveOrUpdate(product);
    		}
    	}
    	map.put("error", error);
    	return map;
    }
    
    

    
    /**
	 * <p>名称：批量更新商品图片</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @return
	 */
    @RequestMapping("updateImage")
    public String updateImage(){
    	
        return "product/updateImage";
    }
    
    /**
     * <p>名称：处理上传的zip图片压缩包并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param multipart
     * @param request
     * @throws IOException
     */
    @RequestMapping(value = "/uploadImage",  method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> uploadImage(@RequestPart("file") MultipartFile multipart,
            HttpServletRequest request) throws IOException {
    	
    	InputStream inputStream = multipart.getInputStream();
    	
    	String savePath = request.getSession().getServletContext().getRealPath("/WEB-INF/import/iamgeZip");
    	
    	String zipFilePath = ZipUtil.copyZipFileStreamToServer(inputStream, savePath);
    	//随机解压文件名
    	String unzipFolderName = DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS);
    	List<String> imagePathList = ZipUtil.handleZip(zipFilePath, savePath+"/"+unzipFolderName);
    	
    	Map<String,Object> resultMap = Maps.newHashMap();
    	//处理图片
    	if(null != imagePathList && imagePathList.size() > 0){
    		List<String> resultList = handleImageList(imagePathList);
    		resultMap.put("resultList", resultList);
    	}
    	
    	return resultMap;
    }
    
    /**
     * <p>名称：处理批量图片并更新数据</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param imagePathList
     * @return
     */
    private  List<String> handleImageList(List<String> imagePathList){
    	String filePath = null;
   	    String productIdStr = null;
   	    String type = null;
   	    String imgSort = null;
   	    Product product = null;
   	    Goods goods = null;
   	    List<String> resultList = new ArrayList<String>();
   	    StringBuilder sb = new StringBuilder();
    	for(String path : imagePathList){
    		  productIdStr = FileUtils.getNameWithoutExtension(path);
	  		  //先判断是否符合数字加下划线加1/2的格式
	  		  if(productIdStr.matches("[12]_[0-9]{1,11}_[1-6]")){
	  			type = productIdStr.split("_")[0];
	  			imgSort = productIdStr.split("_")[2];
	  			productIdStr = productIdStr.split("_")[1];
	  		  }else{
	  			  sb.delete(0, sb.length());
	  			  sb.append(FileUtils.getName(path) + "命名格式不正确,请检查");
	  			  resultList.add(sb.toString());
	  			  continue ;
	  		  }
	  		  //将图片放到服务器对应位置并更新数据
  			  filePath = CSVUtil.CopyFileToServer(path);
  			  if("1".equals(type)){
  				 product = productService.find(Long.valueOf(productIdStr));
  			  }else if("2".equals(type)){
  				 goods = goodsService.find(Long.valueOf(productIdStr));
  			  }
  			  if(null != product 
  					  && "1".equals(type)
  					  && "1".equals(imgSort)){
  				  product.setImage(filePath);
  				  productService.onlyUpdate(product);
  			  }else if(null != product 
  					  && "1".equals(type)
  					  && !"1".equals(imgSort)){
  				  //更新商品图片
  				  ProductImg  productImg  = productImgService.findImgByProIdAndSort(Long.valueOf(productIdStr), Integer.valueOf(imgSort)-2);
  				  if(null != productImg){
  					productImg.setPath(filePath);
  					productImgService.update(productImg);
  				  }else{
  					productImg = new ProductImg();
  					productImg.setPath(filePath);
  					productImg.setProduct(product);
  					productImg.setSort(Integer.valueOf(imgSort)-2);
  					productImgService.save(productImg);
  				  }
  			    
  			  }else if(null != goods 
  					  && "2".equals(type)){
  				  //更新产品图片
  				 //更新商品图片
  				  ProductImg  productImg  = productImgService.findImgByGoodsIdAndSort(Long.valueOf(productIdStr), Integer.valueOf(imgSort)-1);
  				  if(null != productImg){
  					productImg.setPath(filePath);
  					productImgService.update(productImg);
  				  }else{
  					productImg = new ProductImg();
  					productImg.setPath(filePath);
  					productImg.setGoods(goods);
  					productImg.setSort(Integer.valueOf(imgSort)-1);
  					productImgService.save(productImg);
  				  }
  			  }else{
  				  sb.delete(0, sb.length());
	  			  sb.append(FileUtils.getName(path) + "图片对应的商品/产品找不到,请检查此商品/产品ID是否存在");
	  			  resultList.add(sb.toString());
  			  }
  	    }	
    	
    	return resultList ;
    }
    
}
