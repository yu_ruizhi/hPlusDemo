/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ActivityGoodsServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月21日 下午5:14:35 
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.marketing.ActivityGoodsDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ActivityGoods;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.ActivityGoodsService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 活动商品ServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月21日 下午5:14:35  
 * @version: 1.0
 */
@Service
public class ActivityGoodsServiceImpl extends BaseServiceImpl<ActivityGoods> implements ActivityGoodsService{

	private ActivityGoodsDao activityGoodsDao;
	
	@Autowired
	public void setActivityGoodsDao(ActivityGoodsDao activityGoodsDao){
		this.activityGoodsDao = activityGoodsDao;
		setCrudDao(activityGoodsDao);
	}

	@Override
	public List<ActivityGoods> findByActivityId(Long activityId) {
		return activityGoodsDao.findByActivityId(activityId);
	}

	@Override
	public ActivityGoods findByGoodsIdAndActivityId(Long goodsId,Long activityId) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("goodsId", goodsId);
		map.put("activityId",activityId);
		return activityGoodsDao.findByGoodsIdAndActivityId(map);	
	}

}
