/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.SpecDao;
import com.yuruizhi.hplusdemo.admin.dao.product.SpecValueDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Spec;
import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.SpecService;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>名称: SpecServiceImpl</p>
 * <p>说明: 产品规格服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:58:01   
 * @version: 1.0
 */
@Service
public class SpecServiceImpl extends BaseServiceImpl<Spec> implements SpecService {
    @Autowired
    private SpecValueDao specValueDao;
    private SpecDao specDao;

    @Autowired
    public void setSpecDao(SpecDao specDao) {
        this.specDao = specDao;
        setCrudDao(specDao);
    }

    @Override
    public void saveSpecValue(SpecValue spv) {
        specValueDao.save(spv);
    }

    @Override
    public void updateSpecValue(SpecValue spv) {
        specValueDao.update(spv);
    }

    @Override
    public List<SpecValue> findSpecValById(Long specId) {
        return specValueDao.findSpecValByspId(specId);
    }

    @Override
    public boolean saveSpecAndSpecVal(Spec spec) {
        boolean isOk = false;
        if (null != spec) {
            spec.setLastModifiedDate(new Date());
            Map<String, Object> map = new HashMap<String, Object>();
            if(null!=spec.getAlias()&&spec.getAlias().trim().length()<1)
                spec.setAlias(null);
            specDao.save(spec);
            map.put("name", spec.getName());
            Spec spe = specDao.findOne(map);
            List<SpecValue> SpecVal = spec.getSpecValues();
            for (SpecValue sv : SpecVal) {
                sv.setSpec(spe);
                specValueDao.save(sv);
            }
            isOk = true;
        }
        return isOk;
    }

	@Override
	public List<Spec> findByName(String name) {
		return specDao.findByName(name);
	}

	@Override
	public String getIsExistsSpec(Map<String,Object> m) {
		return specDao.getIsExistsSpec(m);
	}
}