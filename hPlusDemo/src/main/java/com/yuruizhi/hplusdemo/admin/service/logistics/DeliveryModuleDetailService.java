package com.yuruizhi.hplusdemo.admin.service.logistics;


import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModuleDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by ASUS on 2015/1/13.
 */
public interface DeliveryModuleDetailService extends BaseService<DeliveryModuleDetail> {

    void deleteByModuleId(Long moduleId);
}
