/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.controller.cms;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.cms.Channel;
import com.yuruizhi.hplusdemo.admin.service.cms.ChannelService;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>名称: ChannelController</p>
 * <p>说明: 栏目管理Controller</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月4日下午3:31:05   
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/channel")
public class ChannelController extends AdminBaseController<Channel> {
    @Autowired
    private ChannelService channelService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Channel.class, "parent", new CustomStringBeanPropertyEditor(Channel.class));
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
    }
    
    
    @Override
    public void input(Channel channel, Map<String, Object> model) {
        super.input(channel, model);
        //model.put("channels", channelService.findAll());
    }
    
    @Override
    public String _compat(@PathVariable("id") Long id, Map<String, Object> model) {
    	
    	Channel channel = channelService.find(id);
    	Channel limit = null;
    	if(null!=channel.getParent()){
    		limit = channelService.find(channel.getParent().getId());
    		model.put("limit", limit);
    	}
    	
    	// TODO null
    	
    	model.put("channel", channel);
    	
    	return getRelativeViewPath("edit");
    }
    
    /**
     * <p>名称：addSubchannel</p> 
     * <p>描述：跳转到新增页</p>
     * @author：ChenGuang
     * @param code
     * @param model
     * @return
     */
    @RequestMapping("{channel}/add")
    public String addSubchannel(@PathVariable("channel") String code, Map<String, Object> model) {
    	model.put("limit", channelService.findOne("code", code));
    	return getRelativeViewPath("edit");
    }

   
    
}
