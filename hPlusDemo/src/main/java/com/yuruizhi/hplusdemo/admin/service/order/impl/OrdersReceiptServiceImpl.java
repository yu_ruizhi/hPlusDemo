/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import com.yuruizhi.hplusdemo.admin.dao.product.ProductDao;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.order.OrdersReceiptDao;
import com.yuruizhi.hplusdemo.admin.dao.order.OrdersReceiptDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService;
import com.google.common.collect.Maps;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>名称: OrdersReceiptServiceImpl</p>
 * <p>说明: 订单服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:29:17   
 * @version: 1.0
 */
@Service
public class OrdersReceiptServiceImpl extends BaseServiceImpl<OrdersReceipt> implements OrdersReceiptService {

	@Autowired
	private OrdersReceiptDetailDao ordersReceiptDetailDao;
	@Autowired
	private OrdersReceiptDao ordersReceiptDao;

	@Autowired
	public void setOrdersReceiptDao(OrdersReceiptDao ordersReceiptDao) {
		super.setBaseDao(ordersReceiptDao);
		this.ordersReceiptDao = ordersReceiptDao;
	}

	/*
	 * (non-Javadoc) <p>Title: booleanLogisticsNo</p> <p>author：Sun.Fan</p>
	 * <p>Description: </p>
	 * 
	 * @param logisticsNo
	 * 
	 * @return
	 * 
	 * @see com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService#
	 * booleanLogisticsNo(java.lang.String)
	 */
	@Override
	public Integer booleanLogisticsNo(String logisticsNo) {
		return ordersReceiptDao.booleanLogisticsNo(logisticsNo);
	}

	@Override
	public void addLogisticsNoz(OrdersReceipt ordersReceipt) {
		ordersReceiptDao.addLogisticsNoz(ordersReceipt);
	}

	@Override
	public void updateStatus(Long id) {
		ordersReceiptDao.updateStatus(id);
	}

	@Override
	public void goCargo(OrdersReceipt ordersReceipt) {
		this.addLogisticsNoz(ordersReceipt);
		this.updateStatus(ordersReceipt.getId());
	}

	@Override
	public int getCountByParams(Map<String, Object> paramMap) {
		String timeArea = (String) paramMap.get("timeArea");
		Map<String, Object> params = new HashMap<String, Object>();
		Long userId = (Long) paramMap.get("userId");
		Member member = new Member();
		member.setId(userId);
		params.put("member", member);
		params.putAll(paramMap);

		if (StringUtils.hasText(timeArea)) {
			int renewalsdata = 0;
			int number[] = { 6, 5, 4, 3, 2, 1 };
			for (int i : number) {
				if (i == Integer.parseInt(timeArea)) {
					renewalsdata = i;
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date now = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(now);
			params.put("endTime", sdf.format(calendar.getTime()));
			calendar.add(Calendar.MONTH, -renewalsdata);
			params.put("stateTime", sdf.format(calendar.getTime()));
		}

		String nameOrCode = (String) paramMap.get("nameOrCode");
		if (null != nameOrCode) {
			Pattern pattern = Pattern.compile("^([\u4E00-\uFA29]|[\uE7C7-\uE7F3]){2,5}$");
			Matcher matcher = pattern.matcher(nameOrCode);
			if (matcher.find()) {// 中文
				nameOrCode = "%" + nameOrCode + "%";
				params.put("productName", nameOrCode);
			} else {
				nameOrCode = "%" + nameOrCode + "%";
				params.put("orderCode", nameOrCode);
			}
		}
		return ordersReceiptDao.getCountByParams(params);
	}

	@Override
	public OrdersReceipt getOrdersReceiptById(Long id) {
		OrdersReceipt ordersReceipt = ordersReceiptDao.findDetailById(id);
		return ordersReceipt;
	}

	@Override
	public List<OrdersReceipt> selectAll() {
		return ordersReceiptDao.selectAll();
	}

	/**
	 * 批量验证订单是否过期.
	 *
	 * // * @see
	 * com.glanway.yihe.service.OrdersReceiptService#checkExpirationDate(java.
	 * util.List)
	 */
	@Override
	public List<OrdersReceipt> checkExpirationDate(List<OrdersReceipt> list) {
		if (list.size() <= 0)
			return null;

		List<OrdersReceipt> result = new ArrayList<OrdersReceipt>();
		for (OrdersReceipt model : list) {
			if (!StringUtils.hasText(model.getExpirationTime().toString())) {
				continue;
			}
			if (model.getStatus() == 1 && new Date().compareTo(model.getExpirationTime()) == 1) {
				ordersReceiptDao.updateOrdersReceipt(model.getId(), Constant.ORDERS_STATUS_CANCELLED);
				model.setStatus(Constant.ORDERS_STATUS_CANCELLED);
			}
			result.add(model);
		}
		return result;
	}

	/**
	 * id为订单详情id,status为要修改的状态
	 *
	 * // * @see
	 * com.glanway.yihe.service.OrdersReceiptService#updateDetailStatus(String,
	 * String)
	 */
	@Override
	public int updateDetailStatus(String id, String status) {
		int result = 0;
		if (id == "" || status == "") {
			return result;
		}
		result = ordersReceiptDetailDao.updateDetailStatus(id, status);
		return result;
	}

	/**
	 * 更改订单状态
	 * 
	 * @param id
	 * @param status
	 */
	/*
	 * (non-Javadoc) <p>名称: updateOrdersStatus</p> <p>描述: </p>
	 * <p>author：Sun.Fan</p>
	 * 
	 * @param id
	 * 
	 * @param status
	 * 
	 * @see com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService#
	 * updateOrdersStatus(java.lang.Long, java.lang.Integer)
	 */
	@Override
	public void updateOrdersStatus(Long id, Integer status) {
		OrdersReceipt ordersReceipt = new OrdersReceipt();
		ordersReceipt.setId(id);
		ordersReceipt.setStatus(status);
		update(ordersReceipt);
	}

    @Override
    public OrdersReceipt findOrdersReceipt(Long id) {
        return ordersReceiptDao.findOrdersReceipt(id);
    }

    @Override
    public void updateActualAmount(OrdersReceipt ordersReceipt) {
        ordersReceiptDao.updateActualAmount(ordersReceipt);
    }

	@Override
	public void cancelOrder(OrdersReceipt or) {
		ordersReceiptDao.cancelOrder(or);
	}

	@Override
	public void updateDiscountAmount(OrdersReceipt or) {
		ordersReceiptDao.updateDiscountAmount(or);
	}

	@Override
	public Page<OrdersReceipt> findPage(Filters filters, Pageable pageable) {
		Map<String, Object> paramsMap = createParamsMap();
		if(null!=filters&&null!=filters.getFilterFor("orderType")&&filters.getFilterFor("orderType").toString().indexOf("2")!=-1) {
			if (null != filters && null != filters.getFilterFor("status") && filters.getFilterFor("status").toString().indexOf("1") != -1) {
				paramsMap.put("daizhifudj", "daizhifudj");
			}
//			if (null != filters && null != filters.getFilterFor("status") && filters.getFilterFor("status").toString().indexOf("2") != -1) {
//				paramsMap.put("weikuanjiu", "weikuanjiu");
//			}
		}
		filters = new IterateNamingTransformFilters(filters);
		if (null != filters&&filters.size()>0) {
			paramsMap.put("_filters", filters);
		}
		return super.findPage(paramsMap,pageable);
	}

	@Override
	public Long[] findIdByTime(String beginDate, String endDate) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		return ordersReceiptDao.findIdByTime(map);
	}
}
