/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.logistics;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: DeliveryAreaDetail</p>
 * <p>说明: 物流区域明细实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:46:37   
 * @version: 1.0
 */
public class DeliveryAreaDetail extends BaseEntity {

	private static final long serialVersionUID = 1L;

	//邮局区域id
    private Long deliveryAreaId;

    //包含区ID
    private String areaId;

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}


	private Boolean deleted;

    public Long getDeliveryAreaId() {
        return deliveryAreaId;
    }

    public void setDeliveryAreaId(Long deliveryAreaId) {
        this.deliveryAreaId = deliveryAreaId;
    }


    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
