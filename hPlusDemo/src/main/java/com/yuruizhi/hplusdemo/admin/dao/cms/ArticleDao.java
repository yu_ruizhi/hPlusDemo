/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.cms;

import java.util.List;
import java.util.Map;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.cms.Article;

/**
 * Created on 2016-02-23 11:01:34
 *
 * @author glanway copyer
 */
@MyBatisMapper
public interface ArticleDao extends BaseDao<Article> {

	/**
	 * <p>名称：getInfo</p> 
	 * <p>描述：通过参数获取新闻资讯信息</p>
	 * @author：ChenGuang
	 * @param params
	 * @return
	 */
	List<Article> getInfo(Map<String, Object> params);
	/**
	 * <p>名称：gitInfoCount</p> 
	 * <p>描述：通过参数获取资讯数量</p>
	 * @author：ChenGuang
	 * @param params
	 * @return
	 */
	int getInfoCount(Map<String, Object> params);
}