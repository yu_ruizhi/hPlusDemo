package com.yuruizhi.hplusdemo.admin.controller.product;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductComment;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductCommentService;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * <p>名称: ConsultController</p>
 * <p>说明: 商咨询控制器</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：lushanshan
 * @date：2016年7月5日上午10:17:27   
 * @version: 1.0 
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/consult")
public class ConsultController extends BaseController{

    @Autowired
    private ProductCommentService productCommentService;
    @Autowired
    private GoodsService goodsService;

    @Autowired
    private MemberService memberService;

    /**
     * <p>名称：咨询首页</p> 
     * <p>描述：TODO</p>
     * @author：lushanshan
     * @return
     */
    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index() {
        return getViewPath("index");
    }
    @RequestMapping("list")
	@ResponseBody
	public Page<ProductComment> list(Filters filters, Pageable pageable){
        Page<ProductComment> list=productCommentService.findPage(filters,pageable);
		return list;
	}

    /**
     * <p>名称：edit</p>
     * <p>描述：编辑</p>
     * @author：lushanshan
     * @return
     */
    @RequestMapping("edit/{id}")
    public ModelAndView edit(@PathVariable(value="id") Long id, ModelMap map){
        ModelAndView mav = new ModelAndView();
        ProductComment productComment = productCommentService.find(id);
        mav.addObject("productComment", productComment);
        mav.setViewName(getViewPath("/edit"));
        return mav;
    }

    @RequestMapping("update")
    public String update(ProductComment productComment) {
        productComment.setReplyTime(new Date(System.currentTimeMillis()));
        productComment.setLastModifiedDate(new Date());
        productCommentService.update(productComment);
        return redirectViewPath("index");
    }


    @RequestMapping("updateInEdit")
    public String updateInEdit(ProductComment productComment){
        //如果回复内容不变，则不更新回复时间
        ProductComment productComment1 = productCommentService.find(productComment.getId());
        if (null!=productComment1.getReply()&& !productComment1.getReply().equals(productComment.getReply())) {
            productComment.setReplyTime(new Date(System.currentTimeMillis()));
        }
        productCommentService.update(productComment);
        return redirectViewPath("index");
    }
}


