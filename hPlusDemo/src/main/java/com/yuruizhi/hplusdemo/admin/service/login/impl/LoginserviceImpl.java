/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.login.impl;

import com.yuruizhi.hplusdemo.admin.dao.perm.UserDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.entity.perm.Role;
import com.yuruizhi.hplusdemo.admin.entity.perm.RoleCategory;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.login.AdminLoginService;
import com.yuruizhi.hplusdemo.admin.service.perm.PermissionService;
import com.yuruizhi.hplusdemo.admin.service.perm.RoleCategoryService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import com.yuruizhi.hplusdemo.admin.util.CipherUtil;
import com.yuruizhi.hplusdemo.admin.util.GeetestUtil;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * <p>名称: LoginserviceImpl</p>
 * <p>说明: 登录服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:07:43   
 * @version: 1.0
 */
@Service
public class LoginserviceImpl implements AdminLoginService {
    @Autowired
    private UserService userService;

    @Autowired
    private PermissionService permissionService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleCategoryService roleCategoryService;
    @Autowired
    private CategoryService categoryService;

    @Override
    public Map<String, Object> loginAndTimeUpdate(String username,
                                                  String password,
                                                  String geetest_challenge,
                                                  String geetest_validate,
                                                  String geetest_seccode,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response,
                                                  HttpSession session)  throws IOException {
        Map<String, Object> result = new HashMap<String, Object>();

        if (null == username || null == password || username.trim().length() < 1 || password.trim().length() < 1) {
            result.put("result", false);
            result.put("errorMsg", "用户名密码不能为空");
            return result;
        }

        Map<String, String> params = ImmutableMap.of("loginName", username);
        User loginUser = userDao.findOne(params);

        if (StringUtils.isEmpty(loginUser)) { //未找到用户
            result.put("result", false);
            result.put("errorMsg", "用户名密码不匹配");
            return result;
        }
        
        if (!CipherUtil.validatePassword(loginUser.getPassword(), password+loginUser.getCreatedDate().getTime())) { //密码错误
            result.put("result", false);
            result.put("errorMsg", "用户名密码不匹配");
            return result;
        }

        if (loginUser.getIsFreeze()) { //账户被冻结
            result.put("result", false);
            result.put("errorMsg", "该账户已被冻结");
            return result;
        }

        if(StringUtils.isEmpty(geetest_challenge) || StringUtils.isEmpty(geetest_validate) || StringUtils.isEmpty(geetest_seccode)){
            result.put("result", false);
            result.put("errorMsg", "验证信息参数有误");
            return result;
        }

        Boolean isVerifySucess = GeetestUtil.VerifyGeetest(geetest_challenge,geetest_validate,geetest_seccode,request,response,session);

        if(!isVerifySucess){
            result.put("result", false);
            result.put("errorMsg", "验证错误");
            return result;
        }

        Long currentUserId = loginUser.getId();
        loginUser.setLastLoginTime(new Date());
        userService.saveLastLoginTime(loginUser);
        UserUtils.setCurrentUserId(currentUserId); //成功登陆，将用户ID放入缓存

        List<Module> permissions = permissionService.findPermissionByUserId(currentUserId);

        UserUtils.cachePermissions(currentUserId, permissions);

        //如果用户为商品部分可见状态，查询可见分类
        if(!loginUser.getIslookAllGoods()){
            if(null!=loginUser.getRole()&&loginUser.getRole().size()>0) {
                Set<Long> category=new HashSet<Long>();
                for(Role role:loginUser.getRole()) {
                    List<RoleCategory> roleCategories = roleCategoryService.findInRole(role.getId());
                    if(null!=roleCategories&&roleCategories.size()>0){
                        for(RoleCategory roleCategory:roleCategories){
                            category.add(roleCategory.getCategoryId());
                            List<Long> categoryChildrens=categoryService.findChildrens(roleCategory.getCategoryId());
                            if(null!=categoryChildrens&&categoryChildrens.size()>0){
                                category.addAll(categoryChildrens) ;
                            }
                        }
                    }
                }
                if(category.size()>0){
                    UserUtils.setRoleCategoryIds(category);
                }
            }
        }

        result.put("result", true);
        return result;
    }

    @Override
    public void logout() {
        UserUtils.cleanup();
    }

    @Override
    public User getCurrentUser() {
        Long currentUserId = UserUtils.getCurrentUserId();
        User user = userService.find(currentUserId);
        if (StringUtils.isEmpty(user)) {
            return null;
        }
        user.setPassword("");
        return user;
    }

}
