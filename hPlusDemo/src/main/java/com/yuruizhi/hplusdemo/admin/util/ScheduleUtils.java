/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.DateBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yuruizhi.hplusdemo.admin.job.JobExecute;

/**
 * <p>名称: ScheduleUtils</p>
 * <p>说明: 定时任务工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:34:19   
 * @version: 1.0
 */
public class ScheduleUtils {
	/** 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(ScheduleUtils.class);



    /**
     * 获取触发器key
     *
     * @param jobName
     * @param jobGroup
     * @return
     */
    public static TriggerKey getTriggerKey(String jobName, String jobGroup) {
        return TriggerKey.triggerKey(jobName, jobGroup);
    }

    /**
     * 获取SimpleTrigger触发器
     * @param scheduler the scheduler
     * @param jobName the job name
     * @param jobGroup the job group
     * @param  date 触发时间
     * @return SimpleTrigger
     */
    public static SimpleTrigger getTrigger(Scheduler scheduler, Long jobName, String jobGroup,Date date) {
        Map<String, Integer> dateMap = dateConverter(date);

        /*public static java.util.Date dateOf(int hour,int minute,int second,int dayOfMonth,int month,int year)*/
        Date startTime = DateBuilder.dateOf(
                dateMap.get("hour")
                ,dateMap.get("minute")
                ,dateMap.get("second")
                ,dateMap.get("dayOfMonth")
                ,dateMap.get("month")
                ,dateMap.get("year"));
        /**
         *  Triggeer 下有两个子类 CronTrigger 和 SimpleTrigger 两种用法，
         *  创建一个SimpleTrigger实例，指定该Trigger在Scheduler中所属组及名称。
         *  接着设置调度的时间规则.当前时间15秒后运行，每10秒运行一次，共运行5次
         *  SimpleTrigger trigger = TriggerBuilder.newTrigger()
         *                      .withIdentity(jobName, jobGroup)
         *                      .startAt(startTime)
         *                      .withSchedule(
         *                          simpleSchedule()
         *                          .withIntervalInSeconds(10)
         *                          .withRepeatCount(5)
         *                       ).build();
         *  */
        SimpleTrigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(jobName+"", jobGroup)
                .startAt(startTime)
                .withSchedule(
                        simpleSchedule()
                ).build();
        return trigger;
    }
    /**
     *  日期解析器指定Map
     *  @Date date  如果有某些没有设置值，默认会被转换成0
     * */
    public static Map<String,Integer> dateConverter(Date date){
        if(null == date){
            throw new  NullPointerException("请填写日期");
        }
        //(int hour,int minute,int second,int dayOfMonth,int month,int year)
        SimpleDateFormat sdf = new SimpleDateFormat("HH mm ss dd MM yyyy");
        String strDate=sdf.format(date);
        String[] tempDate = strDate.split(" ");
        Map<String,Integer> dateMap = new HashMap<>();
        dateMap.put("hour",Integer.parseInt(tempDate[0]));
        dateMap.put("minute",Integer.parseInt(tempDate[1]));
        dateMap.put("second",Integer.parseInt(tempDate[2]));
        dateMap.put("dayOfMonth",Integer.parseInt(tempDate[3]));
        dateMap.put("month",Integer.parseInt(tempDate[4]));
        dateMap.put("year",Integer.parseInt(tempDate[5]));
        return dateMap;
    }
    /**
     * 创建定时任务 *可以多次执行*
     * @param scheduler the scheduler
     * @param jobName the job name
     * @param jobGroup the job group
     * @param date the 定时时间
     */
    public static void createScheduleJob(Scheduler scheduler, Long jobName, String jobGroup,Date date) {
        //构建job信息
        JobDetail jobDetail = JobBuilder.newJob(JobExecute.class).withIdentity(jobName+"", jobGroup).build();
        SimpleTrigger trigger = getTrigger(scheduler, jobName, jobGroup, date);
        try {
            scheduler.scheduleJob(jobDetail, trigger);
            LOG.debug("创建新任务成功！");
        } catch (SchedulerException e) {
            LOG.error("创建定时任务失败", e);
        }
    }
    /**
     * 运行一次任务
     *  这个好像不能设置触发器，还是自己不会
     * @param scheduler
     * @param jobName
     * @param jobGroup
     */
    public static void runOnce(Scheduler scheduler, Long jobName, String jobGroup) {
        JobKey jobKey = JobKey.jobKey(jobName+"", jobGroup);
        try {
            scheduler.triggerJob(jobKey);
        } catch (SchedulerException e) {
            LOG.error("运行一次定时任务失败", e);
        }
    }
    /**
     * 暂停任务
     *
     * @param scheduler
     * @param jobName
     * @param jobGroup
     */
    public static void pauseJob(Scheduler scheduler, String jobName, String jobGroup) {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        try {
            scheduler.pauseJob(jobKey);
        } catch (SchedulerException e) {
            LOG.error("暂停定时任务失败", e);
        }
    }

    /**
     * 恢复任务
     *
     * @param scheduler
     * @param jobName
     * @param jobGroup
     */
    public static void resumeJob(Scheduler scheduler, String jobName, String jobGroup) {
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        try {
            scheduler.resumeJob(jobKey);
        } catch (SchedulerException e) {
            LOG.error("暂停定时任务失败", e);
        }
    }

    /**
     * 获取jobKey
     *
     * @param jobName the job name
     * @param jobGroup the job group
     * @return the job key
     */
    public static JobKey getJobKey(String jobName, String jobGroup) {
        return JobKey.jobKey(jobName, jobGroup);
    }


    /**
     * 更新定时任务
     *
     * @param scheduler the scheduler
     * @param jobName the job name
     * @param jobGroup the job group
     */
    public static void updateScheduleJob(Scheduler scheduler, Long jobName, String jobGroup,Date date) {
        try {
            TriggerKey triggerKey = getTriggerKey(jobName+"", jobGroup);
            SimpleTrigger trigger = getTrigger( scheduler,  jobName,  jobGroup,date);
            //按新的trigger重新设置job执行
            scheduler.rescheduleJob(triggerKey, trigger);
            LOG.debug("更新任务成功！");
        } catch (SchedulerException e) {
            LOG.error("更新定时任务失败", e);
        }
    }

    /**
     * 删除定时任务
     *
     * @param scheduler
     * @param jobName
     * @param jobGroup
     */
    public static void deleteScheduleJob(Scheduler scheduler, String jobName, String jobGroup) {
        try {
            scheduler.deleteJob(getJobKey(jobName, jobGroup));
        } catch (SchedulerException e) {
            LOG.error("删除定时任务失败", e);
        }
    }
}
