package com.yuruizhi.hplusdemo.admin.entity.logistics;



import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.io.Serializable;

/**
 * Created by ASUS on 2015/1/13.
 */
public class DeliveryModuleDetail extends BaseEntity implements Serializable {

    private Long moduleId;

    private Integer top;

    private Integer left;

    private Integer width;

    private Integer height;

    private String content;

    private Boolean deleted;

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getTop() {
        return top;
    }

    public void setTop(Integer top) {
        this.top = top;
    }

    public Integer getLeft() {
        return left;
    }

    public void setLeft(Integer left) {
        this.left = left;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
