/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Works.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.product
 * Date: 2016年4月18日 下午5:24:24
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: 商品所属作品实体类</p>
 * <p>说明: 对应数据库表 T_WORKS</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月18日 下午5:24:54    
 * @version: 1.0
 */
public class Works extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields name : 作品名称
	 * @Column NAME
	 * @Size 255
	 */
	private String name;
	
	/**
	 * @Fields image : 作品图片
	 * @Column IMAGE
	 * @Size 255
	 */
	private String image;
	
	/**
	 * @Fields sort : 排序
	 * @Column SORT
	 * @Size 11
	 */
	private Integer sort;
	
	/**
	 * @Fields deleted : 删除标示
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}
	
}
