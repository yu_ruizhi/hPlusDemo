package com.yuruizhi.hplusdemo.admin.controller;

import org.ponly.webbase.controller.MixedController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 */
public abstract class AdminBaseController<E extends BaseEntity> extends MixedController<E, Long> {

    @Autowired
    public void setBaseService(BaseService<E> baseService) {
        this.crudService = baseService;
    }
    
    @RequestMapping("index")
    public void _index() {}


}
