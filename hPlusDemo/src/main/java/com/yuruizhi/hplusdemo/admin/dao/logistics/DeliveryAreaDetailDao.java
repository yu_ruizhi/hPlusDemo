package com.yuruizhi.hplusdemo.admin.dao.logistics;


import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryAreaDetail;

/**
 * Created by ASUS on 2014/12/8.
 */
public interface DeliveryAreaDetailDao extends BaseDao<DeliveryAreaDetail> {

	/** 
	 * @Title: deletedByAreaId 
	 * @Description: TODO
	 * @param fkid
	 * @return: void
	 */
	void deletedByAreaId(Long fkid);

	/** 
	 * @Title: deletedByParent 
	 * @Description: TODO
	 * @param deliveryAreaId
	 * @return: void
	 */
	void deletedByParent(Long deliveryAreaId);
}
