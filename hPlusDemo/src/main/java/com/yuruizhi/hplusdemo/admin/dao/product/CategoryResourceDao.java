/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.product;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.CategoryResource;


/**
 * Created on 2015-07-09 13:46:41
 *
 * @author vacoor
 */
@MyBatisMapper
public interface CategoryResourceDao extends BaseDao<CategoryResource> {
}