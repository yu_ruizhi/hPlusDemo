/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.product.*;
import com.yuruizhi.hplusdemo.admin.entity.product.*;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsCheckService;
import com.yuruizhi.hplusdemo.admin.service.product.GoodsService;
import com.yuruizhi.hplusdemo.admin.service.product.ProductService;
import com.yuruizhi.hplusdemo.admin.service.product.SpecService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.ponly.webbase.service.ServiceException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import java.util.*;

/**
 * <p>名称: ProductServiceImpl</p>
 * <p>说明: 产品服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:57:20   
 * @version: 1.0
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements ProductService {
    
	public static final String DEFAULT_PRODUCT_IMAGE_PATH = "storage/images/default.jpg";

    private String defaultProductImagePath = DEFAULT_PRODUCT_IMAGE_PATH;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private ProductImgDao productImageDao;
    @Autowired
    private AttributeValueDao attributeValueDao;
    @Autowired
    private ParameterValueDao parameterValueDao;
    @Autowired
    private ProductAttributeValueDao productAttributeValueDao;
    @Autowired
    private SpecValueDao specValueDao;
    @Autowired
    private SpecService specService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsCheckService goodsCheckService;
    @Autowired
    private SysLogService sysLogService;
    @Override
    public void delete(Product product) {
        Long pid = product.getId();

        // categoryDao.delteProductCategoryByProductId(pid);   // 分类
        /*productDao.deleteProductLabelByProductId(pid);  */    // 标签
        attributeValueDao.deleteProductAttributeValue(pid); // 属性值
        parameterValueDao.deleteParameterValueByProductId(pid);
        productImageDao.deleteProductImage(pid);
        goodsDao.deleteProductGoods(pid);
//        productAccessoryDao.deleteByProductId(pid);
//        productAccessoryDetailDao.deleteProductAccessoryDetail(pid);
        super.delete(product);
    }


    @Override
    public Float getWeight(String goodsId) {
        return productDao.getWeight(goodsId);
    }

    @Override
    public void save(Product product) {
//        super.save(product);
        saveOrUpdate(product);
    }

    @Override
    public void update(Product product) {
        saveOrUpdate(product);
    }

    @Override
    public void saveOrUpdate(Product product) {
        doSaveOrUpdate(product, null == product.getId());
    }

    protected void doSaveOrUpdate(Product pro, boolean create) {
        Product original = create ? null : find(pro.getId());
        if (!create && null == original) {
            throw new ServiceException("不能发现要更新数据");
        }
//        if (pro.getPrice() == null) {
//            pro.setPrice(pro.getMarketPrice());
//        }
        if (null != original) {
            Map<String, Object> map = new HashMap<String, Object>();
            /*Product p = new Product();
            p.setId(original.getId());
            p.setEnableSpecs(original.getEnableSpecs());*/
            map.put("product.id", original.getId());

            original.setGoods(goodsService.findMany(map));
        }

        Boolean onSell = pro.getOnSell();
        onSell = null != onSell && onSell;
        pro.setOnSell(onSell);

        // 上下架时间更新
        Boolean originalOnSell = null != original ? original.getOnSell() : null;
        originalOnSell = null != originalOnSell && originalOnSell;   // 如果不存在则为下架
        // 上架 -> 下架, 更新下架时间
        if (originalOnSell && !onSell) {
            pro.setSalesOffDate(new Date());
        } else if (!originalOnSell && onSell) {
            // 下架 -> 上架, 更新上架时间
            pro.setRegisterDate(new Date());
        }

        // 预处理图片, 清理无效图片信息或使用默认图片
        List<ProductImg> images = pro.getProductImgs();
        pro.setProductImgs(images = doCleanSortOrCreateDefaultImage(images, true));
        
        /*
         * update20160503 wentan 
         * 产品对象的（pro）image（主图）属性独立于产品图片
         * 解决产品图片集未上传图片时主图（image）值被替换的bug
         */
        //pro.setImage(0 < images.size() ? images.get(0).getPath() : null);   // 默认图片

        //TODO
        List<Label> labels = pro.getLabels();
        List<AttributeValue> attributeValues = pro.getAttributeValues();
        List<ParameterValue> parameterValues = pro.getParameterValues();
        //不要站点
        /*List<Website> websites = pro.getWebsites();*/

        // 预处理 SKU
        List<Goods> items = null;
        // 如果没有启用规格, 且只有一个默认单品
        if (!pro.getEnableSpecs() && !CollectionUtils.isEmpty(pro.getGoods()) && pro.getGoods().size() == 1) {
            
        	//现在不启用规格保存时就库存的信息放在单品中
        	/*Map<String, Object> map = new HashMap<String, Object>();
            Product p = new Product();
            p.setId(original.getId());
            p.setEnableSpecs(original.getEnableSpecs());
            map.put("product.id", original.getId());
            items = goodsService.findMany(map);
            
            // 如果没有启用规则, 此时的库存维护是在产品中维护-----> : 没启用规格
            for (Goods goods : items) {
                goods.setStock(p.getStock());
            }*/
        	items = pro.getGoods();
        } else {
            items = doCleanOrCreateDefaultItem(pro);
            
            long count = 0;
            if (null!=items&&items.size()>0) {
                for (Goods goods : items) {
                    Long stock = goods.getStock();
                    stock = null != stock ? stock : 0;
                    count += stock;
                }
                pro.setStock(count);
                pro.setGoods(items);
            }
        }

        // 如果是更新且没有启用规格, 则应该已经存在默认的 SKU
        // 此时使用该 SKU 直接更新 (有可能没有提交这个SKU信息, 因此获取直接更新即可, 否则新建ID变化会有其他影响)
        Boolean enableSpecs = pro.getEnableSpecs();
        enableSpecs = null != enableSpecs && enableSpecs;
        List<Goods> originalItems = original != null ? original.getGoods() : null;
        if (!create && !enableSpecs) {

            // 直接拷贝到已有, 设置为新的
            if (null != originalItems && 0 < originalItems.size() && 0 < items.size()) {
                Goods origDefaultItem = originalItems.get(0);
                Goods item = items.get(0);
                Goods copy = new Goods();
                Long id = origDefaultItem.getId();
                BeanUtils.copyProperties(pro, copy);
                copy.setId(id);
                
                //未使用规格时页面上设置的单品的属性复制给要更新的
                copy.setStock(item.getStock());
                copy.setStockForewarn(item.getStockForewarn());
                copy.setReserveNum(item.getReserveNum());
                copy.setReserveDeadline(item.getReserveDeadline());
                copy.setWeight(item.getWeight());
                copy.setSupplierId(pro.getSupplier().getId());
//                BeanMap copyBeanMap = BeanMap.create(copy);
//                copyBeanMap.putAll(BeanMap.create(origDefaultItem)); // 将原始值进行拷贝
//                copyBeanMap.putAll(BeanMap.create(items.get(0)));    // 最新值覆盖拷贝
//                copy.setId(""); // 确保 ID 没有被覆盖

                items.clear();
                items.add(copy);
            }
        }
        long prostock = 0;
        if (pro.getEnableSpecs()) {
            if(null!=pro.getGoods()) {
                //开启规格的之后才能单品库存相加
                for (Goods goods : pro.getGoods()) {
                    Long stock = goods.getStock();
                    if (stock == null) {
                        stock = 0L;
                    }
                    prostock += stock;
                }
                pro.setStock(prostock);
            }
        } else {
            pro.getGoods().get(0).setStock(pro.getStock());
        }

        List<ProductImg> originalImages = null != original ? original.getProductImgs() : null;
        List<AttributeValue> originalAttributeValues = null != original ? original.getAttributeValues() : null;
        List<ParameterValue> originalParameterValues = null != original ? original.getParameterValues() : null;
        //todo
        List<Label> originalLabels = null != original ? original.getLabels() : null;
        //List<Website> originalWebsites = null != original ? original.getWebsites() : null;
        
        //在保存或更新商品（pro）后，originalItems会变更为新提交的List<goods>！不明白为什么
        //List<Goods> originalItems = null != original ? original.getGoods() : null;

        // 保存商品   update20160504 由240行移至此处，解决商品进行保存或更新操作后使“original..”数据变化的问题
        if (create) {
            super.save(pro);
            
            //update20160504 wentan 对商品的保存记录到操作日志中
            sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "商品管理-新增-ID:"+pro.getId()+"商品货号为："+pro.getCode(), "商品管理");
        } else {
            super.update(pro);
            
            //update20160504 wentan 对商品的更新记录到操作日志中
            sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "商品管理-修改-ID:"+pro.getId()+"商品货号为："+pro.getCode(), "商品管理");
        }
        
        // 保存商品 - 图片
        doSaveOrUpdateDiffImages(images, originalImages, pro, null);
        // 保存商品 - 属性值

        doSaveOrUpdateDiffAttributeValues(attributeValues, originalAttributeValues, pro);
        // 保存商品 - 参数值
        doSaveOrUpdateDiffParameterValues(parameterValues, originalParameterValues, pro);
        // 保存商品 - 标签值
        //todo
        doSaveOrUpdateDiffLabels(labels, originalLabels, pro);
        // 保存商品 - 站点 todo 不需要和站点关联
        //doSaveOrUpdateDiffWebSites(websites, originalWebsites, pro);
        // 保存 SKU - 商品
        //
        if(null!=items&&items.size()>0) {
            for (Goods g : items) {
                doCleanSortOrCreateDefaultImage(g.getProductImgs(), true);
                if (!create) {
                    g.setOnSell(null);
                }
            }
        }
            doSaveOrUpdateDiffItems(items, originalItems, pro);

    }

    // 保存或更新有差异的标签
    protected void doSaveOrUpdateDiffLabels(final List<Label> labels, final List<Label> original, final Product pro) {
        productDao.deleteProductLabelByProductId(pro.getId());
        if (null == labels) {
            return;
        }
        for (Label label : labels) {
            if (null != label && null != label.getId()) {
                Map<String, Object> paramsMap = createParamsMap();
                paramsMap.put("pid", pro.getId());
                paramsMap.put("lid", label.getId());
                productDao.saveProductLabel(paramsMap);
            }
        }
    }

    // 清理无效图片信息, 如果没有则使用默认图片
    protected List<ProductImg> doCleanSortOrCreateDefaultImage(List<ProductImg> images, boolean needSort) {
        images = null != images ? images : Lists.<ProductImg>newArrayList();

        int order = 0;
        for (Iterator<ProductImg> it = images.iterator(); it.hasNext(); ) {
            ProductImg img = it.next();
            // 如果对象是空或者没有有效路径则清理
            if (null == img || (null == img.getId() && !StringUtils.hasText(img.getPath()))) {
                it.remove();
                continue;
            }
            if (needSort) img.setSort(order++);
        }
        // 没有有效图片, 使用默认图片
        // if (!images.iterator().hasNext()) {
        if (1 > images.size()) {
            ProductImg img = doCreateDefaultImage();
            img.setSort(0);
            images.add(img);
        }
        return images;
    }

    // 创建默认图片
    protected ProductImg doCreateDefaultImage() {
        ProductImg img = new ProductImg();
        img.setPath(getDefaultProductImagePath());
        return img;
    }

    public String getDefaultProductImagePath() {
        return defaultProductImagePath;
    }

    public void setDefaultProductImagePath(String defaultProductImagePath) {
        this.defaultProductImagePath = defaultProductImagePath;
    }

    // 预处理 SKU, 拷贝属性等等
    protected List<Goods> doCleanOrCreateDefaultItem(Product pro) {
        if (null == pro) {
            return Collections.emptyList();
        }
        Boolean specEnabled = pro.getEnableSpecs();
        List<Goods> items = pro.getGoods();
        specEnabled = null != specEnabled && specEnabled;
        items = null != items ? items : Lists.<Goods>newArrayList();
        // 没有启用规格
        if (!specEnabled ) {
            items.add(doCreateDefaultItem(pro));
        } else {
            //这一块看样子是为了兼容，现在如果启用规格，但是没有单品，直接就不给单品

                for (Iterator<Goods> it = items.iterator(); it.hasNext(); ) {
                    Goods item = it.next();
                    if (null == item) {
                        it.remove();
                        continue;
                    }
                    item.setBigImage(pro.getBigImage());
                    item.setSuppliers(pro.getSuppliers());
                    item.setProduction(pro.getProduction());
                    item.setProduct(pro);
                }

        }
        return items;
    }

    protected Goods doCreateDefaultItem(Product pro) {

        Goods goods = new Goods();
        goods.setProduct(pro);
        goods.setTitle(pro.getTitle());
        goods.setIntro(pro.getIntro());
        goods.setCode(pro.getCode());     // 直接使用作为 code
        goods.setPrice(pro.getPrice());
        goods.setMarketPrice(pro.getMarketPrice());
        goods.setSuppliers(pro.getSuppliers());
        goods.setProduction(pro.getProduction());
        goods.setStock(pro.getStock());
        goods.setProductImgs(pro.getProductImgs());
        goods.setImage(pro.getImage());// 主图
        goods.setBigImage(pro.getBigImage());// 产品大图
        goods.setIsDefault(true);
        return goods;
    }

    // 保存或更新有差异的图片
    protected void doSaveOrUpdateDiffImages(final List<ProductImg> images, final List<ProductImg> original, final Product pro, final Goods item) {
        new DiffHandler<ProductImg>() {
            @Override
            protected void doBothHas(ProductImg left, ProductImg right) {
                left.setProduct(pro);
                left.setGoods(item);
                productImageDao.update(left);
            }

            @Override
            protected void doOnlyLeftHas(ProductImg left) {
                left.setProduct(pro);
                left.setGoods(item);
                left.setId(null);
                productImageDao.save(left);
            }

            @Override
            protected void doOnlyRightHas(ProductImg right) {
                productImageDao.delete(right);
            }
        }.doSwitch(images, original);
    }

    // 保存或更新差异的商品 - 属性值关系
    protected void doSaveOrUpdateDiffAttributeValues(final List<AttributeValue> values, final List<AttributeValue> original, final Product pro) {
        // 删除自定义属性值
        attributeValueDao.deleteProductCustomAttributeValueByProductId(pro.getId());
        attributeValueDao.deleteProductAttributeValue(pro.getId());

        if (null != values) {
            for (AttributeValue value : values) {
                if (null != value) {
                    Map<String, Object> paramsMap = Maps.newHashMap();

                    // 值不为空，则为自定义属性值
                    if (null != value.getValue()) {
                        // 更新
//                        if (null != value.getId()) {
//                            attributeValueDao.update(value);
//                        } else {
                        value.setId(null);
                        attributeValueDao.save(value);
//                        }
                        Attribute attr = value.getAttribute();
                        paramsMap.put("ATTRIBUTE_ID", null != attr ? attr.getId() : null);
                    }

                    //
                    if (null != value.getId()) {
                        paramsMap.put("productId", pro.getId());
                        paramsMap.put("attributeValueId", value.getId());
                        paramsMap.put("deleted", false);
                        attributeValueDao.saveProductAttributeValue(paramsMap);
                    }
                }
            /*
            if (null != value && StringUtils.hasText(value.getId())) {
                Map<String, Object> paramsMap = Maps.newHashMap();
                paramsMap.put("productId", pro.getId());
                paramsMap.put("attributeValueId", value.getId());
                paramsMap.put("deleted",false);
                attributeValueDao.saveProductAttributeValue(paramsMap);
            }
            */
            }
        }
    }

    // 保存 参数值
    protected void doSaveOrUpdateDiffParameterValues(final List<ParameterValue> values, final List<ParameterValue> original, final Product pro) {
        new DiffHandler<ParameterValue>() {
            @Override
            protected void doBothHas(ParameterValue left, ParameterValue right) {
                left.setProduct(pro);
                parameterValueDao.update(left);
            }

            @Override
            protected void doOnlyLeftHas(ParameterValue left) {
                left.setProduct(pro);
                parameterValueDao.save(left);
            }

            @Override
            protected void doOnlyRightHas(ParameterValue right) {
                parameterValueDao.delete(right);
            }
        }.doSwitch(values, original);
    }

    protected void doUpdateItem(Goods item, Goods original) {
        applyProductPropertiesIfNecessary(item);
        item.setSvStr(buildSkuStr(item.getSpecValues()));
        goodsService.update(item);
        goodsService.updateShoppingGoods(item);
        //update20160504 wentan 对单品的修改记录在操作日志中
        sysLogService.saveSysLog(Constant.SYSLOG_UPDATE, "商品管理-单品-修改-ID:"+item.getId()+"商品货号为："+item.getCode(), "商品管理");
        
        //todo 加序号
        doSaveOrUpdateDiffImages(item.getProductImgs(), original.getProductImgs(), null, item);
        
        // UPDATE
        goodsDao.deleteGoodsSpecValues(item.getId());
        saveGoodsSpecValues(item, item.getSpecValues());

        Product pro = item.getProduct();
        Boolean putaway = null != pro ? pro.getOnSell() : null;
        putaway = null != putaway && putaway;
        
        // 上架更新, 下架删除
//        indexingService.updateIndex(item.getId() + "", putaway);
    }

    // 保存或更新有差异的 SKU
    protected void doSaveOrUpdateDiffItems(final List<Goods> values, final List<Goods> original, final Product pro) {
    	
        new DiffHandler<Goods>() {
            @Override
            protected void doBothHas(Goods left, Goods right) {
                left.setProduct(pro);
                doUpdateItem(left, right);
            }

            @Override
            protected void doOnlyLeftHas(Goods left) {
                if(null!=left.getCode()) {
                    left.setProduct(pro);
                    if (null == left.getIsDefault()) {
                        left.setIsDefault(false);
                    }
                    doSaveItem(left);
                }
            }

            @Override
            protected void doOnlyRightHas(Goods right) {
                right.setProduct(pro);
                doDeleteItem(right);
                super.doOnlyRightHas(right);
            }
        }.doSwitch(values, original);
    }

    // 应用 Product 信息 如果 SKU 信息为空
    protected void applyProductPropertiesIfNecessary(Goods item) {
        Product pro = item.getProduct();
        if (null == pro) return;

        if (!StringUtils.hasText(item.getTitle())) {
            item.setTitle(pro.getTitle());
        }
        if (!StringUtils.hasText(item.getCode())) {
            item.setCode(pro.getCode());
        }
        if (null == item.getPrice()) {
            item.setPrice(pro.getPrice());
        }
        if (null == item.getMarketPrice()) {
            item.setMarketPrice(pro.getMarketPrice());
        }
        if (null == item.getBigImage()) {
            item.setBigImage(pro.getBigImage());
        }
        if (null == item.getSuppliers()) {
            item.setSuppliers(pro.getSuppliers());
        }

        List<ProductImg> images = item.getProductImgs();
        if (null == images || 1 > images.size()) {
            item.setProductImgs(images = pro.getProductImgs());
        }
        if (null == item.getImage()) {
            ProductImg img = null != images && 0 < images.size() ? images.get(0) : null;
            item.setImage(null != img ? img.getPath() : null);
        }
    }

    private void saveGoodsSpecValues(Goods goods, List<SpecValue> values) {
        if (null != values) {
            for (SpecValue value : values) {
                Map<String, Object> paramsMap = createParamsMap();
                paramsMap.put("goodsId", goods.getId());
                paramsMap.put("specValueId", value.getId());
//                paramsMap.put("image", value.getProductSpecValue().getImage());
                specValueDao.saveGoodsSpecValue(paramsMap);
            }
        }
    }
    // ----------------------
    //  TODO 移动到 GoodsServiceImpl

    protected void doSaveItem(Goods item) {
        applyProductPropertiesIfNecessary(item);
        item.setSvStr(buildSkuStr(item.getSpecValues()));
        goodsService.save(item);
        sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "商品管理-单品-新增-ID:"+item.getId()+"商品货号为："+item.getCode(), "商品管理");
        doSaveOrUpdateDiffImages(item.getProductImgs(), null, null, item);
        saveGoodsSpecValues(item, item.getSpecValues());

        //update20160427 wentan 保存单品的到核价表 
        GoodsCheck gc = new GoodsCheck();
        gc.setGoodsId(item.getId());
        gc.setChecked(false);
        goodsCheckService.save(gc);
        
         //TODO 暂时去掉，有问题
//        Product pro = item.getProduct();
//        Boolean putaway = null != pro ? pro.getOnSell() : null;
//        // 上架状态
//        if (null != putaway && putaway) {
//            indexingService.updateIndex(item.getId() + "", false);
//        }
    }

    protected String buildSkuStr(List<SpecValue> specValues) {
        if (null == specValues) {
            return null;
        }
        StringBuilder buff = new StringBuilder();
        for (SpecValue sv : specValues) {
            if (null == sv.getSpec()) {
                sv = specValueDao.find(sv.getId());
            }
            if (null != sv && null != sv.getSpec()) {
                buff.append(sv.getSpec().getId()).append(":").append(sv.getId()).append(";");
            }
        }
        return 1 > buff.length() ? null : buff.toString();
    }

    protected void doDeleteItem(Goods item) {
        productImageDao.deleteGoodsImage(item.getId());
        goodsDao.deleteGoodsSpecValues(item.getId());
        goodsDao.delete(item);
        
        //update20160504 wentan 对单品的删除添加到系统日志中
        sysLogService.saveSysLog(Constant.SYSLOG_DELETE, "商品管理-单品-删除-ID:"+item.getId()+"商品货号为："+item.getCode(), "商品管理");
        
        //update20160427 wentan 删除单品对应的核价记录
        goodsCheckService.deleteByGoodsId(item.getId());
        
        // 删除从索引中
        //todo 杨大大说不要了 以后再看
        // indexingService.updateIndex(item.getId(), true);
    }

    protected String transformDynamicProperty(String property) {
        String prop = org.ponly.common.util.StringUtils.camelCaseToUnderscore(property, true);
        return prop;
    }

    @SuppressWarnings("deprecation")
	@Override
    public Page<Product> findPage(
            Filters productFilters,
            Filters brandFilters,
            Filters modelFilters,
            Filters categoryFilters,
            Filters worksFilters,
            Filters supplierFilters,
            Pageable pageable,
            Filters rFilters,/* 商品类型过滤条件 */
            String goodsCode) {
        brandFilters = new IterateNamingTransformFilters(brandFilters);
        modelFilters = new IterateNamingTransformFilters(modelFilters);
        categoryFilters = new IterateNamingTransformFilters(categoryFilters);
        worksFilters = new IterateNamingTransformFilters(worksFilters);
        supplierFilters = new IterateNamingTransformFilters(supplierFilters);
        rFilters=new IterateNamingTransformFilters(rFilters);

        Map<String, Object> paramsMap = createParamsMap();
        if (null != rFilters&&rFilters.size()>0) {
            if(null!=rFilters.getFilterFor("reserved")){
                if(null!=rFilters.getFilterFor("reserved").iterator().next()){
                    if(rFilters.getFilterFor("reserved").iterator().next().toString().indexOf("0")!=-1){
                        paramsMap.put("goodsTypez",0);
                    }
                    if(rFilters.getFilterFor("reserved").iterator().next().toString().indexOf("1")!=-1){
                        paramsMap.put("goodsTypez",1);
                    }
                    if(rFilters.getFilterFor("reserved").iterator().next().toString().indexOf("2")!=-1){
                        paramsMap.put("goodsTypez",2);
                    }
                }
            }
        }
        if (null != productFilters&&productFilters.size()>0) {
            paramsMap.put(ProductDao.PRODUCT_FILTERS_PROP, productFilters);
        }
        if (null != brandFilters&&brandFilters.size()>0) {
            paramsMap.put(ProductDao.BRAND_FILTERS_PROP, brandFilters);
        }
        if (null != modelFilters&&modelFilters.size()>0) {
            paramsMap.put(ProductDao.MODEL_FILTERS_PROP, modelFilters);
        }
        if (null != categoryFilters&&categoryFilters.size()>0) {
            paramsMap.put(ProductDao.CATEGORY_FILTERS_PROP, categoryFilters);
        }
        
        if (null != worksFilters&&worksFilters.size()>0) {
            paramsMap.put(ProductDao.WORKS_FILTERS_PROP, worksFilters);
        }
        
        if (null != supplierFilters&&supplierFilters.size()>0) {
            paramsMap.put(ProductDao.SUPPLIER_FILTERS_PROP, supplierFilters);
        }
        if(null!=goodsCode&&goodsCode.trim().length()>0){
            paramsMap.put("goodsCode","%"+goodsCode+"%");
        }

        if (null != pageable) {
            paramsMap.put(ProductDao.OFFSET_PROP, pageable.getOffset());
            paramsMap.put(ProductDao.MAX_RESULTS_PROP, pageable.getPageSize());

            Sort sort = pageable.getSort();
            if (null != sort) {
                /*for (Sort.Order order : sort) {
                    order.setProperty(order.getProperty().toUpperCase());
                }*/
                paramsMap.put(ProductDao.SORT_PROP, new IterateNamingTransformSort(sort));
            }
        }
        if(null!=UserUtils.getRoleCategoryIds()&&UserUtils.getRoleCategoryIds().size()>0){
            paramsMap.put("categoryIds", UserUtils.getRoleCategoryIds());
        }

        //update20160504 wentan 解决商品列表页的排序错乱问题
        
        /*int total = count(paramsMap);
        List<Product> data = total > 0 ? findMany(paramsMap) : Collections.<Product>emptyList();
        return new SimplePage<Product>(pageable, data, total);*/
        
        int total = productDao.countForIndexList(paramsMap);
        List<Product> data = total > 0 ? productDao.findManyForIndexList(paramsMap) : Collections.<Product>emptyList();

        return new SimplePage<Product>(pageable, data, total);
    }

    @SuppressWarnings("deprecation")
    @Override
    public Page<Product> findPage(Filters productFilters, Filters brandFilters, Filters modelFilters, Filters categoryFilters, Filters worksFilters, Filters supplierFilters, Pageable pageable, Filters rFilters, String goodsCode, String perfected) {
        brandFilters = new IterateNamingTransformFilters(brandFilters);
        modelFilters = new IterateNamingTransformFilters(modelFilters);
        categoryFilters = new IterateNamingTransformFilters(categoryFilters);
        worksFilters = new IterateNamingTransformFilters(worksFilters);
        supplierFilters = new IterateNamingTransformFilters(supplierFilters);
        rFilters=new IterateNamingTransformFilters(rFilters);

        Map<String, Object> paramsMap = createParamsMap();
        if (null != rFilters&&rFilters.size()>0) {
            if(null!=rFilters.getFilterFor("reserved")){
                if(null!=rFilters.getFilterFor("reserved").iterator().next()){
                    if(rFilters.getFilterFor("reserved").iterator().next().toString().indexOf("0")!=-1){
                        paramsMap.put("goodsTypez",0);
                    }
                    if(rFilters.getFilterFor("reserved").iterator().next().toString().indexOf("1")!=-1){
                        paramsMap.put("goodsTypez",1);
                    }
                    if(rFilters.getFilterFor("reserved").iterator().next().toString().indexOf("2")!=-1){
                        paramsMap.put("goodsTypez",2);
                    }
                }
            }
        }
        if (null != productFilters&&productFilters.size()>0) {
            paramsMap.put(ProductDao.PRODUCT_FILTERS_PROP, productFilters);
        }
        if (null != brandFilters&&brandFilters.size()>0) {
            paramsMap.put(ProductDao.BRAND_FILTERS_PROP, brandFilters);
        }
        if (null != modelFilters&&modelFilters.size()>0) {
            paramsMap.put(ProductDao.MODEL_FILTERS_PROP, modelFilters);
        }
        if (null != categoryFilters&&categoryFilters.size()>0) {
            paramsMap.put(ProductDao.CATEGORY_FILTERS_PROP, categoryFilters);
        }

        if (null != worksFilters&&worksFilters.size()>0) {
            paramsMap.put(ProductDao.WORKS_FILTERS_PROP, worksFilters);
        }

        if (null != supplierFilters&&supplierFilters.size()>0) {
            paramsMap.put(ProductDao.SUPPLIER_FILTERS_PROP, supplierFilters);
        }
        if(null!=goodsCode&&goodsCode.trim().length()>0){
            paramsMap.put("goodsCode","%"+goodsCode+"%");
        }

        if (null != pageable) {
            paramsMap.put(ProductDao.OFFSET_PROP, pageable.getOffset());
            paramsMap.put(ProductDao.MAX_RESULTS_PROP, pageable.getPageSize());

            Sort sort = pageable.getSort();
            if (null != sort) {
                /*for (Sort.Order order : sort) {
                    order.setProperty(order.getProperty().toUpperCase());
                }*/
                paramsMap.put(ProductDao.SORT_PROP, new IterateNamingTransformSort(sort));
            }
        }
        if(null!=UserUtils.getRoleCategoryIds()&&UserUtils.getRoleCategoryIds().size()>0){
            paramsMap.put("categoryIds", UserUtils.getRoleCategoryIds());
        }

        //update20160504 wentan 解决商品列表页的排序错乱问题

        /*int total = count(paramsMap);
        List<Product> data = total > 0 ? findMany(paramsMap) : Collections.<Product>emptyList();
        return new SimplePage<Product>(pageable, data, total);*/
        paramsMap.put("perfected",perfected);
        int total = productDao.countForIndexList(paramsMap);
        List<Product> data = total > 0 ? productDao.findManyForIndexList(paramsMap) : Collections.<Product>emptyList();

        return new SimplePage<Product>(pageable, data, total);
    }

	@Override
	public void onlyUpdate(Product product) {
		product.setLastModifiedDate(new Date());
		productDao.update(product);
	}

	@Override
	public List<Product> findByCategoryId(Long categoryId) {
		return productDao.findByCategoryId(categoryId);
	}

    @Override
    public List<Product> searchProductByTimeAndStatus(Map<String,Object> parmsMap) {
        return productDao.searchProductByTimeAndStatus( parmsMap);
    }
    
    @Override
    public List<Map<String,Object>> searchGoodsExcel(Map<String,Object> parmsMap) {
        return productDao.searchGoodsExcel( parmsMap);
    }

    @Override
    public Integer findProductCount(Long modelId) {
        return productDao.findProductCount(modelId);
    }

    @Override
    public void dr(List<Product> productList,List<Product> products) {
         if(null!=productList&&productList.size()>0){
             for(Product product:productList){
                 if(null!=products&&products.size()>0){
                     int zc=0;
                     for(Product p:products){
                         if(product.getId().equals(p.getId())){
                             zc++;
                         }
                     }
                     if(zc!=0){
                         continue;
                     }
                 }

                 int i=0;
                 if(null!=product.getGoods()&&product.getGoods().size()>0){
                     List<Spec> specList=new ArrayList<Spec>();
                     for(Goods goods:product.getGoods()) {
                         if (null != goods.getError()) {
                             i++;
                         }else{
                         for (SpecValue specValue : goods.getSpecValues()) {
                             if (specList.size() < 1) {
                                 List<SpecValue> svs = new ArrayList<SpecValue>();
                                 svs.add(specValue);
                                 specValue.getSpec().setSpecValues(svs);
                                 specList.add(specValue.getSpec());
                             } else {
                                 int num = 0;
                                 for (Spec s : specList) {
                                     if (s.getName().equals(specValue.getSpec().getName())) {
                                         int y = 0;
                                         for (SpecValue specValue1 : s.getSpecValues()) {
                                             if (specValue1.getName().equals(specValue.getName())) {
                                                 y = 1;
                                             }
                                         }
                                         if (y == 0)
                                             s.getSpecValues().add(specValue);
                                         num = 1;
                                     }
                                 }
                                 if (num == 0) {
                                     List<SpecValue> svs = new ArrayList<SpecValue>();
                                     svs.add(specValue);
                                     specValue.getSpec().setSpecValues(svs);
                                     specList.add(specValue.getSpec());
                                 }
                             }
                         }
                     }
       }
                     if(i!=0){
                         continue;
                     }
                   //存入product
                     product.setId(null);
                 super.save(product);
                     //记录日志
                     sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "商品管理-导入新增-ID:"+product.getId()+"商品货号为："+product.getCode(), "商品管理");
                  //存入属性同product关系
                 if(null!=product.getCategory().getModel().getAttributes()&&product.getCategory().getModel().getAttributes().size()>0){
                     for(Attribute attribute:product.getCategory().getModel().getAttributes()){
                         //如果是文本存入属性值
                         if(attribute.getDisplayType()==1){
                             attribute.getAttributeValues().get(0).setSort(1);
                             attribute.getAttributeValues().get(0).setAttribute(attribute);
                             attribute.getAttributeValues().get(0).setCreatedDate(new Date());
                             attributeValueDao.save( attribute.getAttributeValues().get(0));
                         }
                         ProductAttributeValue productAttributeValue=new ProductAttributeValue();
                         productAttributeValue.setProduct(product);
                         productAttributeValue.setAttribute(attribute);
                         productAttributeValue.setAttributeValue(attribute.getAttributeValues().get(0));
                         productAttributeValue.setDeleted(false);
                         productAttributeValueDao.insert(productAttributeValue);
                     }
                 }
                     //存入临时规格
                     if(specList.size()>0){
                         for(Spec spec:specList){
                             spec.setIstemporary(true);
                             spec.setDisplayType(0);
                             spec.setSort(1);
                             spec.setCreatedDate(new Date());
                             spec.setDeleted(false);
                             specService.save(spec);
                             if(null!=spec.getSpecValues()){
                                 for(SpecValue specValue:spec.getSpecValues()){
                                     specValue.setSpec(spec);
                                     specValue.setSort(1);
                                     specValue.setDeleted(false);
                                     specValue.setCreatedDate(new Date());
                                     specValueDao.save(specValue);
                                 }
                             }
                         }
                     }
                     //存入单品
                     for(Goods gs:product.getGoods()){
                         gs.setProduct(product);
                         gs.setCreatedDate(new Date());
                         gs.setStock(0L);
                         goodsService.save(gs);
                         //插入图片
                         if(null!=gs.getProductImgs()&&gs.getProductImgs().size()>0){
                             for(ProductImg productImg:gs.getProductImgs()){
                                 if(null!=productImg&&null!=productImg.getPath()&&productImg.getPath().trim().length()>0){
                                     productImg.setSort(1);
                                     productImg.setGoods(gs);
                                     productImg.setCreatedDate(new Date());
                                     productImageDao.save(productImg);
                                 }
                             }
                         }
                       //存入规格商品关系
                         StringBuffer buffer=new StringBuffer();
                        for(SpecValue specValue:gs.getSpecValues()){
                            Map<String, Object> goodsSpecValue=new HashMap<String, Object>();
                            goodsSpecValue.put("id",null);
                            goodsSpecValue.put("goodsId",gs.getId());
                            for(Spec spec:specList){
                                if(specValue.getSpec().getName().equals(spec.getName())){
                                    buffer.append(spec.getId().toString());
                                  for(SpecValue sv:spec.getSpecValues()){
                                      if(specValue.getName().equals(sv.getName())){
                                          buffer.append(":"+sv.getId()+";");
                                          goodsSpecValue.put("specValueId",sv.getId());
                                      }
                                  }
                                }
                            }
                            specValueDao.saveGoodsSpecValue(goodsSpecValue);
                        }
                         gs.setSvStr(buffer.toString());
                         goodsService.update(gs);
                         GoodsCheck gc = new GoodsCheck();
                         gc.setGoodsId(gs.getId());
                         gc.setChecked(false);
                         goodsCheckService.save(gc);
                         //记录日志
                         sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "商品管理-单品-导入新增-ID:"+gs.getId()+"商品货号为："+gs.getCode(), "商品管理");
                     }
                 }else{
                     //存入product
                     super.save(product);
                     //记录日志
                     sysLogService.saveSysLog(Constant.SYSLOG_SAVE, "商品管理-导入新增-ID:"+product.getId()+"商品货号为："+product.getCode(), "商品管理");
                     //存入属性同product关系
                     if(null!=product.getCategory().getModel().getAttributes()&&product.getCategory().getModel().getAttributes().size()>0){
                         for(Attribute attribute:product.getCategory().getModel().getAttributes()){
                             //如果是文本存入属性值
                             if(attribute.getDisplayType()==1){
                                 attribute.getAttributeValues().get(0).setSort(1);
                                 attribute.getAttributeValues().get(0).setAttribute(attribute);
                                 attribute.getAttributeValues().get(0).setCreatedDate(new Date());
                                 attributeValueDao.save( attribute.getAttributeValues().get(0));
                             }
                             ProductAttributeValue productAttributeValue=new ProductAttributeValue();
                             productAttributeValue.setProduct(product);
                             productAttributeValue.setAttribute(attribute);
                             productAttributeValue.setAttributeValue(attribute.getAttributeValues().get(0));
                             productAttributeValue.setDeleted(false);
                             productAttributeValueDao.insert(productAttributeValue);
                         }
                     }
                 }
             }
         }
    }
}
