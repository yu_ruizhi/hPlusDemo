/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: CityServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area.impl
 * Date: 2016年4月18日下午7:43:31
 **/
package com.yuruizhi.hplusdemo.admin.service.area.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.area.CityDao;
import com.yuruizhi.hplusdemo.admin.entity.area.City;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.area.CityService;

/**
 * 
 * <p>名称: 城市Service实现</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午7:44:01
 * @version: 1.0
 */
@Service
public class CityServiceImpl extends BaseServiceImpl<City> implements CityService{
	@Autowired
	private CityDao cityDao;
	
	@Override
	public City findByCityId(String cityId) {
		return cityDao.findByCityId(cityId);
	}

	@Override
	public List<City> findByPid(String pid) {
		List<City> cityList = cityDao.findByPid(pid);
		return cityList;
	}
	
}
