
package com.yuruizhi.hplusdemo.admin.dao.order;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.order.ProblemOrder;

/**
 * Created on 2016-03-07 14:38:27
 *
 * @author 路姗姗
 */
public interface ProblemOrderDao extends BaseDao<ProblemOrder> {


    void updateProblemOrderStatus(ProblemOrder problemOrder);
    
    /**
     * 
     * <p>名称：根据创建时间来查找id</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    Long[] findIdByTime(Map<String,Object> map);
}