/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm;

import com.yuruizhi.hplusdemo.admin.entity.perm.SysLog;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: SysLogService</p>
 * <p>说明: 后台系统日志业务服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:44:09   
 * @version: 1.0
 */
public interface SysLogService extends BaseService<SysLog> {

public boolean saveSysLog(Integer type,String operation,String moduleName);//记录日志及是否成功


}
