/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * <p>名称: ExcelUtil</p>
 * <p>说明: EXCEL处理工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:32:00   
 * @version: 1.0
 */
public class ExcelUtil {

	private static final Logger logger = Logger.getLogger(ExcelUtil.class);
	
	/**
	 * <p>名称：</p> 
	 * <p>描述：TODO</p>
	 * @author：Sun.Fan
	 * @param name 表格名称
	 * @param titles 表格列名
	 * @param entity 数据集合
	 * @param attributeName
	 * @param response 属性名字集合
	 * @throws Exception
	 */
	public static <T> void outExcel(String name, String[] titles, List<T> entity, String[] attributeName, 
			HttpServletResponse response) throws Exception {

		// 创建HSSFWorkbook对象(excel的文档对象)
		HSSFWorkbook wkb = new HSSFWorkbook();
		HSSFCellStyle style = wkb.createCellStyle();
		HSSFDataFormat format = wkb.createDataFormat();
		style.setDataFormat(format.getFormat("@"));
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);// 左右居中
		// 建立新的sheet对象（excel的表单）
		HSSFSheet sheet = wkb.createSheet(name);
		// 在sheet里创建第一行，参数为行索引(excel的行)，可以是0～65535之间的任何一个
		HSSFRow row1 = sheet.createRow(0);
		// 创建单元格（excel的单元格，参数为列索引，可以是0～255之间的任何一个
		HSSFCell cell = row1.createCell(0);
		cell.setCellStyle(style);
		// 设置单元格内容
		cell.setCellValue(name);
		// 合并单元格CellRangeAddress构造参数依次表示起始行，截至行，起始列， 截至列
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, titles.length - 1));
		// 在sheet里创建第二行
		HSSFRow row2 = sheet.createRow(1);
		// 创建单元格并设置单元格内容
		for (int i = 0; i < titles.length; i++) {
			HSSFCell c=	row2.createCell(i);
			c.setCellStyle(style);
			c.setCellValue(titles[i]);
		}
		// 在sheet里创建第三行
		if (null != entity && entity.size() > 0) {
			for (int i = 0; i < entity.size(); i++) {
				HSSFRow row = sheet.createRow(i + 2);
				for (int j = 0; j < attributeName.length; j++) {
					HSSFCell c=	row.createCell(j);
					c.setCellStyle(style);
					String methodName = "get" + attributeName[j].substring(0, 1).toUpperCase()
							+ attributeName[j].substring(1);
					Object o = entity.get(i).getClass().getMethod(methodName).invoke(entity.get(i));
					c.setCellValue(null == o ? " " : o.toString());
				}
			}
		}
		try {
			OutputStream output = response.getOutputStream();
			response.reset();
			response.setContentType("application/x-msdownload");
			response.setHeader("Content-Disposition", "attachment;filename="
					+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "alltoStock.xls");
			wkb.write(output);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * <p>名称：导出excel</p> 
	 * <p>描述：通过实体类get导出数据的excel</p>
	 * @author：wuqi
	 * @param title 标题
	 * @param headers ecxel头标题
	 * @param dataset 
	 * @param map 需要的字段
	 * @param code 下载
	 * @param response
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static <T> void exportExcel(String title, String[] headers,  
			List<T> dataset,Map<String,Object> map,String code,HttpServletResponse response)  
    {  
        // 声明一个工作薄  
        HSSFWorkbook workbook = new HSSFWorkbook();  
        // 生成一个表格  
        HSSFSheet sheet = workbook.createSheet(title);  
  
        // 产生表格标题行  
        HSSFRow row = sheet.createRow(0);  
        for (short i = 0; i < headers.length; i++)  
        {  
            @SuppressWarnings("deprecation")
			HSSFCell cell = row.createCell(i); 
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);  
            cell.setCellValue(text);  
        }  
  
        // 遍历集合数据，产生数据行  
        Iterator<T> it = dataset.iterator();  
        int index = 0;  
        while (it.hasNext()) {  
            index++;  
            row = sheet.createRow(index);  
            T t = (T) it.next();  
            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值  
            Field[] fields = t.getClass().getDeclaredFields();  
            short rows = 0;
            for (short i = 0; i < fields.length; i++) {  
                Field field = fields[i];  
                String fieldName = field.getName();
                if (null == map.get(fieldName)) {
                	continue;
                }
                @SuppressWarnings("deprecation")
				HSSFCell cell = row.createCell(rows); 
                rows++;
                String getMethodName = "get"  
                        + fieldName.substring(0, 1).toUpperCase()  
                        + fieldName.substring(1);  
                try {  
                    Class tCls = t.getClass();  
                    Method getMethod = tCls.getMethod(getMethodName,  new Class[]{});  
                    Object value = getMethod.invoke(t, new Object[]{});  
                    
                    // 判断值的类型后进行强制类型转换  
                    String textValue = null;  
                    ObjectChange(textValue, value,cell,workbook);
                } catch (Exception ex) {
                	logger.info("Exception:"+ex);
                	ex.printStackTrace();
				} finally {  
                    
                }  
            }  
        }  
        download(code,workbook,response);
    }  
	
	/**
	 * 
	 * <p>名称：调用浏览器下载器</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param code 下载文件名
	 * @param workbook excel数据流
	 * @param response
	 */
	public static void download(String code, HSSFWorkbook workbook, HttpServletResponse response) {
	    response.reset();     
	    response.setContentType("application/x-msdownload");
	    OutputStream ouputStream = null;
	    try {
			response.setHeader("Content-Disposition","attachment; filename="+code+".xls");
			ouputStream = response.getOutputStream();    
			workbook.write(ouputStream); 
	    } catch (Exception e) {
	    	logger.info("Exception:"+e);
	    	e.printStackTrace();
		} finally{
        	if(ouputStream!=null){
   		       try {
   		    	ouputStream.close();
				} catch (IOException e) {
					logger.info("Export demand excel close Exception:"+e);
					e.printStackTrace();
				} 
   	         }
		}
	}
	
	/**
	 * 
	 * <p>名称：将不同类型进行转换</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param textValue 转换好的值
	 * @param value 需要转换的值
	 * @param cell excel文件
	 * @param workbook excel数据流
	 */
	public static void ObjectChange(String textValue, Object value, HSSFCell cell, HSSFWorkbook workbook) {
		if (value instanceof Date) {  
            textValue = DateUtils.date2Str((Date) value, DateUtils.DATE_FORMAT_YYYY_MM_DD); 
        }  else if (null == value){
        	textValue = "";
        } else {  
            // 其它数据类型都当作字符串简单处理  
            textValue = value.toString();  
        }  
        // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成  
        if (textValue != null) {  
            Pattern p = Pattern.compile("^//d+(//.//d+)?$");  
            Matcher matcher = p.matcher(textValue);  
            if (matcher.matches()) {  
                // 是数字当作double处理  
                cell.setCellValue(Double.parseDouble(textValue));  
            }  
            else {  
                HSSFRichTextString richString = new HSSFRichTextString(  
                        textValue);  
                HSSFFont font3 = workbook.createFont();  
                font3.setColor(HSSFColor.BLUE.index);  
                richString.applyFont(font3);  
                cell.setCellValue(richString);  
            }  
        }
	}
	
	/**
	 * <p>名称：调用浏览器下载器导出excel文件</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
     * @param headers 
     * @param dataList 
     * @param response 
     * @param fileName
	 */
    public static void exportExcel(String[] headers, List<String> dataList,HttpServletResponse response,String fileName){  
        // 声明一个工作薄  
        HSSFWorkbook workbook = new HSSFWorkbook();  
        // 生成一个表格  
        HSSFSheet sheet = workbook.createSheet("default");  
        // 设置表格默认列宽度为15个字节  
        sheet.setDefaultColumnWidth(15);  
        // 生成一个样式  
        HSSFCellStyle style = workbook.createCellStyle();  
        // 设置这些样式  
        style.setFillForegroundColor(HSSFColor.SKY_BLUE.index);  
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);  
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);  
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);  
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);  
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);  
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);  
        // 生成一个字体  
        HSSFFont font = workbook.createFont();  
        font.setColor(HSSFColor.VIOLET.index);  
        font.setFontHeightInPoints((short) 12);  
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);  
        // 把字体应用到当前的样式  
        style.setFont(font);   
  
        // 产生表格标题行  
        HSSFRow row = sheet.createRow(0); 
        HSSFCell cell = null ;
        HSSFRichTextString text = null;
        for (int i = 0; i < headers.length; i++){  
            cell = row.createCell(i);  
            cell.setCellStyle(style);  
            text = new HSSFRichTextString(headers[i]);  
            cell.setCellValue(text);  
        }  
  
      //写入数据部分
        if(null != dataList
        		&& !dataList.isEmpty()){
        	String[] lineData = null;
            for(int j = 0; j < dataList.size(); j++){
            	 row = sheet.createRow(1+j);
            	 lineData = dataList.get(j).split(",");
            	 if(null != lineData && lineData.length > 0){
            		 for(int m = 0; m < lineData.length; m++){
                    	 cell = row.createCell(m);  
                         text = new HSSFRichTextString(lineData[m]);  
                         cell.setCellValue(text); 
                     } 
            	 }
            }
        } 
        
	    OutputStream ouputStream = null;
		try {
			response.reset();     
			response.setContentType("application/octet-stream;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Content-Disposition", "attachment; filename="+fileName);
			
			ouputStream = response.getOutputStream();    
			workbook.write(ouputStream); 
		}catch (IOException e) {
	 	logger.debug("ExportReport IOException:"+e);
	 }finally {
	     //关闭流
	     IOUtils.closeQuietly(ouputStream);
	 }
  }  
    
    /**
	 * <p>名称：读取csv,返回string list</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param input 
	 * @param startLine 从哪一行开始
	 * @param cellNum   读多少列数据
	 */
   public static List<String> importExcel(InputStream input, int startLine , int cellNum){
	   HSSFWorkbook workbook =  null ;
	   List<String> dataList = new ArrayList<String>();
       try {
		 workbook = new HSSFWorkbook(input);
	   } catch (IOException e) {
			logger.debug("importExcel IOException:"+e);
	   } 
       
       if(null != workbook){
    	    HSSFSheet readSheet = workbook.getSheetAt(0);  
    	    HSSFRow readRow = null; 
    	    HSSFCell readCell = null;
    	    StringBuilder buff = new StringBuilder();
    	    for(int i = startLine ; i < readSheet.getLastRowNum()+1 ; i ++){
    	    	 buff.delete( 0, buff.length());
    	    	 readRow =readSheet.getRow(i); 
    	    	 for(int j = 0 ; j < cellNum ; j ++){
    	    		  readCell = readRow.getCell(j); 
    	    		  if( j < (cellNum-1)){
						  if(null==readCell){
							  buff.append(",");
						  } else if(j!=7&&j!=23) {
							  if (null != readCell) {
								  readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
								  buff.append(readCell.getStringCellValue() + ",");
							  } else {
								  buff.append(",");
							  }
						  }else{
							  if(readCell.getCellType()!=0) {
								  readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
								  buff.append(readCell.getStringCellValue().replace("/","-") + ",");
							  } else{
								  try{
									  buff.append(new SimpleDateFormat("yyyy-MM-dd").format(readCell.getDateCellValue()) + ",");
								  }catch(Exception e){
									  readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
									  buff.append(readCell.getStringCellValue() + ",");
								  }
							  }
						  }
    	    		  }else{
						  if(null==readCell){
							  buff.append(",");
						  } else  if(j!=7&&j!=23) {
							  if (null != readCell) {
								  readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
								  buff.append(readCell.getStringCellValue() + ",");
							  } else {
								  buff.append(",");
							  }
						  }else{
							  if(readCell.getCellType()!=0) {
								  readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
								  buff.append(readCell.getStringCellValue().replace("/","-") + ",");
							  } else{
								  try{
									  buff.append(new SimpleDateFormat("yyyy-MM-dd").format(readCell.getDateCellValue()) + ",");
								  }catch(Exception e){
									  readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
									  buff.append(readCell.getStringCellValue() + ",");
								  }
							  }
						  }
    	    		  }
    	    	 }
    	    	 dataList.add(buff.toString());
    	    }
    	   
       }
      
       return dataList;
   }

	public static List<String> importExcel(InputStream input, int startLine , int cellNum,String zc){
		HSSFWorkbook workbook =  null ;
		List<String> dataList = new ArrayList<String>();
		try {
			workbook = new HSSFWorkbook(input);
		} catch (IOException e) {
			logger.debug("importExcel IOException:"+e);
		}

		if(null != workbook){
			HSSFSheet readSheet = workbook.getSheetAt(0);
			HSSFRow readRow = null;
			HSSFCell readCell = null;
			StringBuilder buff = new StringBuilder();
			for(int i = startLine ; i < readSheet.getLastRowNum()+1 ; i ++){
				buff.delete( 0, buff.length());
				readRow =readSheet.getRow(i);
				for(int j = 0 ; j < cellNum ; j ++){
					readCell = readRow.getCell(j);
					if( j < (cellNum-1)){
						if(null != readCell){
							if(readCell.getCellType()!=0||j<2) {
								readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
								buff.append(readCell.getStringCellValue().replace("/","-") + ",");
							}else{
								try{
									buff.append(new SimpleDateFormat("yyyy-MM-dd").format(readCell.getDateCellValue()) + ",");
								}catch(Exception e){
									readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
									buff.append(readCell.getStringCellValue() + ",");
								}
							}
						}else{
							buff.append(",");
						}
					}else{
						if(null != readCell){
							if(readCell.getCellType()!=0||j<2) {
								readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
								buff.append(readCell.getStringCellValue().replace("/","-") + ",");
							}else{
								try{
									buff.append(new SimpleDateFormat("yyyy-MM-dd").format(readCell.getDateCellValue()) + ",");
								}catch(Exception e){
									readCell.setCellType(HSSFCell.CELL_TYPE_STRING);
									buff.append(readCell.getStringCellValue() + ",");
								}
							}
						}else{
							buff.append(" ");
						}
					}
				}
				dataList.add(buff.toString());
			}

		}

		return dataList;
	}
	
}
