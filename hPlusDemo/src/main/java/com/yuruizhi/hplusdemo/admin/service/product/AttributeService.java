/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: AttributeService</p>
 * <p>说明: 商品属性服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:59:17   
 * @version: 1.0
 */
public interface AttributeService extends BaseService<Attribute> {
	
    /**
     * 
     * <p>名称：根据名称及模型id获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    Attribute findByNameAndModelId (String name,Long modelId);
	
    /**
     * 
     * <p>名称：根据名称获取基础数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    Attribute findByNameAndIsBase(String name);

    List<Attribute> findModelAttributes(Long modelId);
}
