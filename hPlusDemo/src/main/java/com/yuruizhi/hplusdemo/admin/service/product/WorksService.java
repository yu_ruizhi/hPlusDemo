/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: WorksService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.product
 * Date: 2016年4月18日 下午5:35:26 
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Works;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 作品Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月18日 下午5:35:46   
 * @version: 1.0
 */
public interface WorksService extends BaseService<Works> {

	/**
	 * 
	 * <p>名称：检查同名作品是否存在</p> 
	 * <p>描述：根据作品名称检查该作品是否存在</p>
	 * @author：wentan
	 * @param name 作品名
	 * @return 如果该作品存在，key("isExists")对应value是true，反之为false。
	 */
	Map<String, Boolean> checkIsWorksExists(String name);

	/**
	 * 
	 * <p>名称：检查除了当前作品，是否还存在其他同名作品</p> 
	 * <p>描述：根据作品名和ID检查是否还存在其他同名的作品</p>
	 * @author：wentan
	 * @param id 当前作品ID
	 * @param name 当前作品名
	 * @return 如果存在其他同名作品返回true,否则返回false
	 */
	Boolean checkIsAnotherWorksExists(Long id, String name);
	
	/**
	 * 
	 * <p>名称：根据名称进行查找</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	List<Works> findByName(String name);
}
