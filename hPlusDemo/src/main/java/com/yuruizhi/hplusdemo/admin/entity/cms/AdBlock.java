/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.cms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: AdBlock</p>
 * <p>说明: 广告位实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:40:52   
 * @version: 1.0
 */
public class AdBlock extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 6105924278297253974L;

	/**
     * 广告位名称
     * 
     * @ViewField editor=input 
     * @Column NAME
     */
    @NotNull
    @Size(max = 255)
    private String name;

    /**
     * 广告位编码
     * 
     * @ViewField editor=input 
     * @Column CODE
     */
    @Size(max = 255)
    private String code;

    /**
     * 广告位宽度
     * 
     * @ViewField editor=spinner 
     * @Column WIDTH
     */
    @NotNull
    private Integer width;

    /**
     * 广告位高度
     * 
     * @ViewField editor=spinner 
     * @Column HEIGHT
     */
    @NotNull
    private Integer height;

    /**
     * 允许大小
     * 
     * @ViewField editor=input 
     * @Column LIMIT_SIZE
     */
    @Size(max = 255)
    private String limitSize;

    /**
     * 广告位描述
     * 
     * @ViewField editor=input 
     * @Column NOTE
     */
    @Size(max = 255)
    private String note;

    /**
     * 示意图
     * 
     * @ViewField editor=input 
     * @Column IMAGE
     */
    @Size(max = 255)
    private String image;

    /**
     * 广告模板(预留)
     * 
     * @ViewField editor=input 
     * @Column TEMPLATE
     */
    @Size(max = 255)
    private String template;

    /**
     * 广告数量(预留)
     * 
     * @ViewField editor=spinner 
     * @Column MAX_QTY
     */
    private Integer maxQty;

    /**
     * 展示方式(预留)
     * 
     * @ViewField editor=input 
     * @Column DISPLAY_TYPE
     */
    @Size(max = 255)
    private String displayType;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getWidth() {
        return this.width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return this.height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getLimitSize() {
        return this.limitSize;
    }

    public void setLimitSize(String limitSize) {
        this.limitSize = limitSize;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTemplate() {
        return this.template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public Integer getMaxQty() {
        return this.maxQty;
    }

    public void setMaxQty(Integer maxQty) {
        this.maxQty = maxQty;
    }

    public String getDisplayType() {
        return this.displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
