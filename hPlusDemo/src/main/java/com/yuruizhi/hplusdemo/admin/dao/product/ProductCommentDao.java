package com.yuruizhi.hplusdemo.admin.dao.product;

import java.util.ArrayList;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.ProductComment;

/**
 * Project Name：
 * Description：
 * Created By：      Argevolle
 * Created Date：    2016/06/21 13:16
 * Modified By：
 * Modified Date：
 * Modified Remark：
 * Version：         v 0.1
 */
public interface ProductCommentDao extends BaseDao<ProductComment> {
	ArrayList<Object> getProductCommentsByGoodsId(Map<String, Object> map);
	void addComment(Map<String,Object> map);
	int getCommentsCountByGoodsId(long id);
}