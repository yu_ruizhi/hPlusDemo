/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-inoutstock 
 * FileName: SupplierServiceImpl.java 
 * PackageName: com.glanway.inoutstock.service.impl
 * Date: 2016年5月9日下午3:57:23   
 **/
package com.yuruizhi.hplusdemo.admin.service.provider.impI;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.provider.SupplierDao;
import com.yuruizhi.hplusdemo.admin.dao.provider.SupplierDetailDao;
import com.yuruizhi.hplusdemo.admin.dao.provider.SupplierGoodsListDao;
import com.yuruizhi.hplusdemo.admin.entity.provider.Supplier;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierDetail;
import com.yuruizhi.hplusdemo.admin.entity.provider.SupplierGoodsList;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import com.yuruizhi.hplusdemo.admin.service.provider.SupplierService;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.ponly.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**      
 * <p>名称:SupplierServiceImpl </p>
 * <p>说明: 供应商管理的Service实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：lushanshan
 * @date：2016年5月9日下午3:58:31   
 * @version: 1.0 
 */
@Service
@Transactional
public class SupplierServiceImpl extends BaseServiceImpl<Supplier> implements SupplierService {

    @Autowired
    public void setSupplierDao(SupplierDao supplierDao) {
        super.setBaseDao(supplierDao);
    }

    @Autowired
    private SupplierGoodsListDao supplierGoodsListDao;

    @Autowired
    private SupplierDao supplierDao;
    @Autowired
    private SupplierDetailDao supplierDetailDao;

    @Autowired
    private SysLogService sysLogService;

    /*(non-Javadoc) 
     * <p>名称: getListByGoodsId</p> 
     * <p>描述: 根据单品code获取提供此单品的供应商信息.</p> 
     * <p>author：lushanshan</p> 
     * @param goodsId
     * @return 
     * @see com.glanway.inoutstock.service.SupplierService#getListByGoodsId(java.lang.String) 
     */ 
    @Override
    public List<Supplier> getListByGoodsId(String goodsId) {
        List<Supplier> result = new ArrayList<Supplier>();
        if (StringUtils.hasText(goodsId)) {
            List<SupplierGoodsList> supplierList = supplierGoodsListDao.getListByGoodsId(goodsId);
            if (supplierList.size() > 0) {
                for (int i = 0; i < supplierList.size(); i++) {
                    result.add(supplierDao.getById(supplierList.get(i).getSupplierId()));
                }
            }
        }
        return result;
    }



    /*(non-Javadoc) 
     * <p>名称: saveSupplierGoodsList</p> 
     * <p>描述: 保存供应商商品列表</p> 
     * <p>author：lushanshan</p> 
     * @param supplier 
     * @see com.glanway.inoutstock.service.SupplierService#saveSupplierGoodsList(com.glanway.inoutstock.entity.Supplier) 
     */ 
    @Override
    public void saveSupplierGoodsList(Supplier supplier) {
        String content="";
        List<SupplierGoodsList> newDetails = supplier.getDetails();
        String supplierID = newDetails.get(0).getSupplierId();
        List<SupplierGoodsList> origDetails = supplierGoodsListDao.findMany(ImmutableMap.<String, Object>of("supplierId", supplierID));
        Map<String, SupplierGoodsList> origMap = Maps.newHashMap(Maps.uniqueIndex(origDetails, new Function<SupplierGoodsList, String>() {
            @Override
            public String apply(SupplierGoodsList input) {
                return null /*!= input ? input.getId() : null*/;
            }
        }));

        Iterator<SupplierGoodsList> newIt = newDetails.iterator();
        while (newIt.hasNext()) {
            SupplierGoodsList newSgl = newIt.next();
            if (null == newSgl) {
                continue;
            }
            long id =newSgl.getId();
            // 已经存在, 则更新
            if (null != origMap.get(id)) {
                supplierGoodsListDao.update(newSgl);
                content="供应商管理-编辑-商品"+newSgl.getGoodsName()+"的商品";
                sysLogService.saveSysLog(Constant.SYSLOG_UPDATE,content,"供应商管理");
                origMap.remove(id);
            } else {
                // 不存在, 保存

                supplierGoodsListDao.save(newSgl);
                content="供应商管理-新增-商品"+newSgl.getGoodsName()+"的商品";
                sysLogService.saveSysLog(Constant.SYSLOG_SAVE,content,"供应商管理");
            }
            newIt.remove();
        }
        // 原始列表中剩余的为需要删除的
        for (SupplierGoodsList removed : origMap.values()) {
            content="供应商管理-删除-商品"+removed.getGoodsName()+"的商品";
            sysLogService.saveSysLog(Constant.SYSLOG_DELETE,content,"供应商管理");
            supplierGoodsListDao.delete(removed);
        }

    }
    
    /*(non-Javadoc) 
     * <p>名称: getIdByName</p> 
     * <p>描述:根据供应商的名字获得id </p> 
     * <p>author：lushanshan</p> 
     * @param supplierName
     * @return 
     * @see com.glanway.inoutstock.service.SupplierService#getIdByName(java.lang.String) 
     */ 
    @Override
    public String getIdByName(String supplierName) {
        return supplierDao.getIdByName(supplierName);
    }
    
    /**
     * <p>名称：existsInSupplierGoodsList</p> 
     * <p>描述：是否存在供应商商品列表</p>
     * @author：lushanshan
     * @param qList
     * @param id
     * @return
     */
/*    private static Boolean existsInSupplierGoodsList(List<SupplierGoodsList> qList, String id) {
        for (SupplierGoodsList model : qList) {
            if (id == model.getId()) {
                return true;
            }
        }
        return false;
    }*/

    /*(non-Javadoc) 
     * <p>名称: findManyByGoodsId</p> 
     * <p>描述: 根据商品id查找商品列表</p> 
     * <p>author：lushanshan</p> 
     * @param gid
     * @return 
     * @see com.glanway.inoutstock.service.SupplierService#findManyByGoodsId(java.lang.String) 
     */ 
    @Override
    public List<Supplier> findManyByGoodsId(String gid) {
        return supplierDao.findManyByGoodsId(gid);
    }
    
    /*(non-Javadoc) 
     * <p>名称: getBySupplierId</p> 
     * <p>描述: 根据供应商id获取供应商</p> 
     * <p>author：lushanshan</p> 
     * @param supplierId
     * @return 
     * @see com.glanway.inoutstock.service.SupplierService#getBySupplierId(java.lang.String) 
     */ 
    @Override
    public List<SupplierDetail> getBySupplierId(String supplierId) {
        if (!supplierId.equals("")) {
            return supplierDetailDao.getBySupplierId(supplierId);
        }
        return null;
    }



	@Override
	public List<Supplier> findBySupplierName(String supplierName) {
		return supplierDao.findBySupplierName(supplierName);
	}

    @Override
    public List<Supplier> findBySupplierCode(String supplierCode) {
        return supplierDao.getSupplierExistsBycode(supplierCode);
    }

    @Override
    public Map<String, Boolean> checkCodeIsExits(String code) {
        Map<String, Boolean> model = Maps.newHashMap();
        List<Supplier> supplierList = supplierDao.getSupplierExistsBycode(code);

        if (StringUtils.hasText(code) && supplierList.size() > 0) {
            model.put("isExists", true);
        } else {
            model.put("isExists", false);
        }
        return model;
    }


}
