/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.order.impl;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.order.OrderAllotDao;
import com.yuruizhi.hplusdemo.admin.dao.order.OrdersReceiptDao;
import com.yuruizhi.hplusdemo.admin.dao.order.OrdersReceiptDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;
import com.yuruizhi.hplusdemo.admin.entity.order.OrderAllot;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.member.MemberService;
import com.yuruizhi.hplusdemo.admin.service.order.OrderAllotService;
import com.yuruizhi.hplusdemo.admin.service.perm.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>名称: OrderAllotServiceImpl</p>
 * <p>说明: 订单分配服务及接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:27:46   
 * @version: 1.0
 */
@Service
public class OrderAllotServiceImpl extends BaseServiceImpl<OrderAllot> implements OrderAllotService{

    @Autowired
    private OrderAllotDao orderAllotDao;

    @Autowired
    private OrdersReceiptDao ordersReceiptDao;
    @Autowired
    private OrdersReceiptDetailDao ordersReceiptDetailDao;
    @Autowired
    private SysLogService sysLogService;
    @Autowired
    private MemberService memberService;
    @Autowired
    public void setOrderAllotDao(OrderAllotDao orderAllotDao) {
        super.setBaseDao(orderAllotDao);
        this.orderAllotDao = orderAllotDao;
    }

    //根据相关条件进入仓库查询商品数量
    @Override
    public Integer goodsInWarehouseNum(Map<String, Object> map) {


        return orderAllotDao.goodsInWarehouseNum(map);
    }

    @Override
    public void update(OrderAllot orderAllot) {
        orderAllotDao.update(orderAllot);
    }

    @Override
    public void update(Map<String, Object> map) {
        orderAllotDao.updateMap(map);
    }

    @Override
    public String checkedOrderIsPay(Map<String, Object> map) {
        List<OrderAllot> orderAllots=orderAllotDao.orderAllotInOrder(map);
        if(null==orderAllots||orderAllots.size()<1)
        return "数据异常";
        List<String> codes=new ArrayList<String>(0);
        for(OrderAllot orderAllot:orderAllots){
            //如果通知的配单，前台用户已经生成了尾款订单那么不能撤销，判断相关订单为尾款订单，同时订单详情中和新尾款订单相互关联字段不为null，新生成的订单正常存在
            //判断新尾款订单是否合格
            boolean noquxiao=false;
            if(null!=orderAllot.getOrdersReceipt().getOrdersReceiptDetail().getRelOrderID()) {
                OrdersReceipt o = ordersReceiptDao.findDetailById(orderAllot.getOrdersReceipt().getOrdersReceiptDetail().getRelOrderID());
                if (null != o && o.getStatus() != Constant.ORDERS_STATUS_CANCELLED) {
                    noquxiao=true;
                }
            }
            if(orderAllot.getOrdersReceipt().getStatus()== Constant.ORDERS_STATUS_SUBMIT&&(orderAllot.getOrdersReceipt().getPresaleStatus()== OrdersReceipt.PRESALE_STATUS_RETAINAGE ||orderAllot.getOrdersReceipt().getPresaleStatus()== OrdersReceipt.PRESALE_STATUS_PAYRETAINAGE )&&noquxiao){
                //订单已经支付
                codes.add(orderAllot.getOrdersReceipt().getOrderCode());
            }
        }
        if(codes.size()<1){
            return "success";
        }else{
            StringBuilder stringBuilder=new StringBuilder("订单编号为：");
            for(String code:codes){
                stringBuilder.append(code+",");
            }
            stringBuilder.append("的尾款商品用户已经生成尾款订单，不能撤销");
            return stringBuilder.toString();
        }
    }

    @Override
    public Integer advanceStock(Map<String, Object> map) {
        return orderAllotDao.advanceStock(map);
    }

    @Override
    public void updatePresaleStatus(Long[] orderAllotIds, Integer presaleStatus,String type) {//预售订单状态：1-定金订单；2-已支付定金；3-已配单；4-尾款订单；5-已支付尾款
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("ids",orderAllotIds);
        List<OrderAllot> orderAllots=orderAllotDao.orderAllotInOrder(map);
        //获取订单id集合
        List<Long> orderIds=new ArrayList<>();
        List<Long> lingShouOrderIds=new ArrayList<>(0);
        List<Long> jingxiaoOrderIds=new ArrayList<>(0);
        for(OrderAllot orderAllot:orderAllots){
            orderIds.add(orderAllot.getOrdersReceipt().getId());
            if(type.equals("分配")){
               sysLogService.saveSysLog(Constant.SYSLOG_UPDATE,"配单管理 - 修改 - 订单号为"+orderAllot.getOrdersReceipt().getOrderCode()+"的定金订单分配了商品","配单管理");
            }else if (type.equals("提醒")){
                //只有零售商的尾款订单才会过期.这里判断订单是否属于零售商，如果属于，就提取订单id
                Member member=memberService.find(orderAllot.getOrdersReceipt().getUserId());
                if(member.getUserType().equals("0")||(member.getUserType().equals("1")&&(null==member.getIsAudit()|| !member.getIsAudit()))){
                    lingShouOrderIds.add(orderAllot.getOrdersReceipt().getId());
                }else if(member.getUserType().equals("1")&&null!=member.getIsAudit()&&member.getIsAudit()){
                    //经销商给到分配时间就行
                    jingxiaoOrderIds.add(orderAllot.getOrdersReceipt().getId());
                }
                map.put("zc",1);
               sysLogService.saveSysLog(Constant.SYSLOG_UPDATE,"配单管理 - 修改 - 订单号为"+orderAllot.getOrdersReceipt().getOrderCode()+"的定金订单已经提醒，成为尾款订单","配单管理");
            }else if(type.equals("撤销")){
                //只有零售商的尾款订单才会过期.这里判断订单是否属于零售商，如果属于，就提取订单id
                Member member=memberService.find(orderAllot.getOrdersReceipt().getUserId());
                if(member.getUserType().equals("0")||(member.getUserType().equals("1")&&(null==member.getIsAudit()|| !member.getIsAudit()))){
                    lingShouOrderIds.add(orderAllot.getOrdersReceipt().getId());
                }
                map.put("zc",2);
                sysLogService.saveSysLog(Constant.SYSLOG_UPDATE,"配单管理 - 修改 - 订单号为"+orderAllot.getOrdersReceipt().getOrderCode()+"的定金订单撤销分配","配单管理");
            }
        }
        map.remove("ids");
        map.put("orderIds",orderIds);
        map.put("presaleStatus",presaleStatus);
        ordersReceiptDao.updatePresaleStatus(map);
        map.remove("orderIds");
        if (lingShouOrderIds.size()>0){
            map.put("lingShouOrderIds",lingShouOrderIds);
            ordersReceiptDetailDao.updateRetainageTime(map);
            ordersReceiptDao.updateExpiredTime(map);
        }
        if (jingxiaoOrderIds.size()>0){
            map.put("lingShouOrderIds",jingxiaoOrderIds);
            ordersReceiptDetailDao.updateRetainageTime(map);
        }
    }

    @Override
    public Long getOrderId(Long id) {
        return orderAllotDao.getOrderId(id);
    }

    @Override
    public void updateOrderActualAmount(Map<String, Object> map) {
        List<OrderAllot> orderAllots=orderAllotDao.orderAllotInOrder(map);

        //如果还原的是砍单数据，那么更改相应的订单价格
        for(OrderAllot orderAllot:orderAllots){

            if(null!=orderAllot.getIsCut()&&orderAllot.getIsCut()){
                OrdersReceipt ordersReceipt=ordersReceiptDao.findOrdersReceipt(orderAllot.getOrdersReceipt().getId());
                OrdersReceiptDetail ordersReceiptDetail=ordersReceiptDetailDao.getOrdersReceiptDetailByOrderId(orderAllot.getOrdersReceipt().getId());
                // TODO 保证金经销商是没有缴纳定金的，所以砍单后计算价格不应该在加上定金
                Member member=memberService.find(ordersReceipt.getUserId());
                if(member.getUserType().equals("0")||(member.getUserType().equals("1")&&(null==member.getIsAudit()|| !member.getIsAudit()))||(member.getUserType().equals("1")&&(null == member.getIsMargin() || !member.getIsMargin()))) {
                    //零售商或者没有审核的经销商，或者不是保证金经销商
                    ordersReceipt.setActualAmount(ordersReceipt.getActualAmount().add(ordersReceiptDetail.getGoodsPrice().multiply(new BigDecimal((orderAllot.getCutNum() + "").trim()))).subtract(ordersReceiptDetail.getGoodsPrice().multiply(new BigDecimal((orderAllot.getCutNum() + "").trim())).multiply(new BigDecimal(Constant.DEPOSIT))).setScale(2, BigDecimal.ROUND_HALF_UP));
                }else{
                    ordersReceipt.setActualAmount(ordersReceipt.getActualAmount().add(ordersReceiptDetail.getGoodsPrice().multiply(new BigDecimal((orderAllot.getCutNum() + "").trim()))).setScale(2, BigDecimal.ROUND_HALF_UP));
                }
                ordersReceiptDao.updateActualAmount(ordersReceipt);
                ordersReceiptDetail.setCutNum(0);
                ordersReceiptDetailDao.update(ordersReceiptDetail);
            }

        }

    }
}
