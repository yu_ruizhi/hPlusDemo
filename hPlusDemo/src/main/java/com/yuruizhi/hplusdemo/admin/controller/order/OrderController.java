package com.yuruizhi.hplusdemo.admin.controller.order;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.yuruizhi.hplusdemo.admin.dao.product.ProductDao;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.dto.OrdersReceiptVO;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceipt;
import com.yuruizhi.hplusdemo.admin.entity.order.OrdersReceiptDetail;
import com.yuruizhi.hplusdemo.admin.entity.perm.Module;
import com.yuruizhi.hplusdemo.admin.service.order.OrdersReceiptService;
import com.yuruizhi.hplusdemo.admin.service.perm.ModuleService;
import com.yuruizhi.hplusdemo.admin.util.CSVUtil;
import com.yuruizhi.hplusdemo.admin.util.DateUtils;
import com.yuruizhi.hplusdemo.admin.util.ExcelUtil;

import net.sf.json.JSONObject;

@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/order")
public class OrderController extends BaseController {
	@Autowired
	private ModuleService moduleService;
	@Autowired
	private OrdersReceiptService ordersReceiptService;

	@RequestMapping(value = "addLogisticsNo", method = RequestMethod.POST)
	public ModelAndView addLogisticsNo(OrdersReceipt order) {
		ModelAndView mav = new ModelAndView();
		ordersReceiptService.goCargo(order);
		mav.setViewName(redirectViewPath("edit/" + order.getId()));
		return mav;
	}

	/**
	 * <p>
	 * 名称：进入订单管理首页
	 * </p>
	 * <p>
	 * 描述：
	 * </p>
	 * 
	 * @author：Sun.Fan
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", 4);
		map.put("deleted", 0);
		request.setAttribute("total", ordersReceiptService.count(map));
		return getViewPath("index");
	}

	/**
	 * <p>
	 * 名称：添加
	 * </p>
	 * <p>
	 * 描述：TODO
	 * </p>
	 */
	@RequestMapping(value = "add", method = RequestMethod.GET)
	public void add() {

	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String save(String name, String sortId, String remark, HttpServletRequest request,
			HttpServletResponse response) {
		if (null == name || name.trim().length() < 1 || null == sortId || sortId.trim().length() < 1) {

		}
		Module module = new Module();
		module.setName(name);
		if (null != remark && remark.trim().length() > 0) {
			module.setRemark(remark);
		}
		try {
			module.setSortId(Integer.parseInt(sortId));
		} catch (Exception e) {
			module.setSortId(1);
		}
		moduleService.save(module);
		return redirectViewPath("index");
	}

	/**
	 * <p>名称：首页订单列表</p> 
	 * <p>描述：显示首页订单列表，加入过滤条件</p>
	 * @author：Sun.Fan
	 * @param filters 
	 * @param pageable 
	 * @param beginDate 开始日期
	 * @param endDate  截止日期
	 * @param orderType 订单类型，不为空时查带催款订单
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public Page<OrdersReceipt> list(Filters filters, Pageable pageable,
			@RequestParam(value = "beginDate", required = false) String beginDate,
			@RequestParam(value = "endDate", required = false)  String endDate,
			@RequestParam(value = "orderType", required = false) String orderType) {
		if (StringUtils.isNotEmpty(beginDate)) {
			filters.ge("createdDate", beginDate);
		}
		if (StringUtils.isNotEmpty(endDate)) {
			filters.le("createdDate", endDate);
		}
		if(StringUtils.isNotEmpty(orderType)){
			//催款单
			filters.eq("presaleStatus", OrdersReceipt.PRESALE_STATUS_RETAINAGE);
            filters.eq("status",Constant.ORDERS_STATUS_SUBMIT);
		}
		for (Sort.Order order : pageable.getSort()) {
			if (pageable.toString().indexOf("nickName") != -1)
				order.setProperty("u_nick_name");
			if (pageable.toString().indexOf("member.phone") != -1)
				order.setProperty("u_phone");
			if (pageable.toString().indexOf("orderCode") != -1)
				order.setProperty("order_code");
		}
		Iterator<Filters.Filter> iterator = filters.iterator();
		while (iterator.hasNext()){
			Filters.Filter next = iterator.next();
			String property = next.getProperty();
			if("orderType".equals(property)){
				Object[] values = (Object[]) next.getValues();
				if(values.length ==1 && Integer.parseInt((String)values[0]) ==3){
					iterator.remove();
					filters.eq("presaleStatus", OrdersReceipt.PRESALE_STATUS_NEWWEIKUAN);
					break;
				}
				if(values.length ==1 &&Integer.parseInt((String)values[0]) ==2){
					filters.ne("presaleStatus", OrdersReceipt.PRESALE_STATUS_NEWWEIKUAN);
					break;
				}
			}
			if("status".equals(property)){
				Object[] values = (Object[]) next.getValues();
				if(values.length ==1 && ((String)values[0]).equals("100")){
					iterator.remove();
					filters.eq("orderType", OrdersReceipt.ORDERTYPE_RESERVE);
					filters.eq("presaleStatus", OrdersReceipt.PRESALE_STATUS_PAYRETAINAGE);
					break;
				}
			}
		}

		Page<OrdersReceipt> mlist = ordersReceiptService.findPage(filters, pageable);
        for(OrdersReceipt ordersReceipt:mlist){
			if(null!=ordersReceipt.getOrderType()&&null!=ordersReceipt.getPresaleStatus()) {
				if (ordersReceipt.getOrderType() == OrdersReceipt.ORDERTYPE_RESERVE && ordersReceipt.getPresaleStatus() == OrdersReceipt.PRESALE_STATUS_PAYRETAINAGE) {
					ordersReceipt.setStatus(100);
				}
				if (ordersReceipt.getOrderType() == OrdersReceipt.ORDERTYPE_RESERVE && ordersReceipt.getPresaleStatus() == OrdersReceipt.PRESALE_STATUS_NEWWEIKUAN) {
					ordersReceipt.setOrderType(3);
				}
			}
		}
		return mlist;
	}
	
	@RequestMapping("deleted")
	@ResponseBody
	public OrdersReceipt delete(Long id) {

		return ordersReceiptService.getOrdersReceiptById(id);
	}

	@RequestMapping("isLogisticsNo")
	@ResponseBody
	public Boolean isLogisticsNo(String logisticsNo) {
		Integer count = ordersReceiptService.booleanLogisticsNo(logisticsNo);
		return count == 0;
	}

	@RequestMapping("deleteModuleByIds")
	@ResponseBody
	public boolean deleteModuleByIds(Long[] ids) {
		if (ids == null) {
			return false;
		}

		return moduleService.deleteModuleByIds(ids);

	}

	/**
	 * <p>
	 * 名称：进入编辑页
	 * </p>
	 * <p>
	 * 描述：根据订单编号查询订单详情，进入编辑页面
	 * </p>
	 * 
	 * @author：Sun.Fan
	 * @param id
	 * @return
	 */
	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		OrdersReceipt order = ordersReceiptService.getOrdersReceiptById(id);
		//如果是尾款订单，并且已经生成新的尾款订单，除非新的尾款订单被取消或者删除了，否则不能取消
		if(order.getOrderType()==OrdersReceipt.ORDERTYPE_RESERVE&&order.getPresaleStatus()==OrdersReceipt.PRESALE_STATUS_RETAINAGE&&order.getStatus()==Constant.ORDERS_STATUS_SUBMIT&& null!=order.getOrdersReceiptDetails().get(0).getRelOrderID()){
			//判断新尾款订单是否合格
			OrdersReceipt o=ordersReceiptService.getOrdersReceiptById(order.getOrdersReceiptDetails().get(0).getRelOrderID());
			if(null!=o&&o.getStatus()!=Constant.ORDERS_STATUS_CANCELLED){
				mav.addObject("noquxiao", "noquxiao");
			}
		}
		mav.addObject("order", order);
		mav.setViewName(getViewPath("/edit"));
		return mav;
	}

	@RequestMapping("setUp/{id}/{type}")
	public String  setUp(@PathVariable(value = "id") Long id,@PathVariable(value = "type") Integer type) {
	     //type  为1 直接改订单为待收货状态
		OrdersReceipt ordersReceipt=new OrdersReceipt();
		ordersReceipt.setId(id);
		if(type==1){
			ordersReceipt.setStatus(Constant.ORDERS_STATUS_COMPLETE_PACKAGE);
		}else if(type==2){
			ordersReceipt.setPresaleStatus(OrdersReceipt.PRESALE_STATUS_RETAINAGE);
		}
		ordersReceiptService.update(ordersReceipt);
		return  "redirect:/order/edit/"+id;
	}

	/**
	 * <p>名称：导出excel表单</p> 
	 * <p>描述：导出当前选中的查询结果</p>
	 * @author：Sun.Fan
	 * @param filters 
	 * @param pageable
	 * @param beginDate
	 * @param endDate
	 * @param orderType
	 * @return
	 */
	@ResponseBody
	@RequestMapping("export")
	public void export(Filters filters, Pageable pageable,
			@RequestParam(value = "beginDate", required = false) String beginDate,
			@RequestParam(value = "endDate", required = false)  String endDate,
			@RequestParam(value = "orderType", required = false) String orderType,
			HttpServletResponse response) {
		if (StringUtils.isNotEmpty(beginDate)) {
			filters.ge("createdDate", beginDate);
		}
		if (StringUtils.isNotEmpty(endDate)) {
			filters.le("createdDate", endDate);
		}
		if(StringUtils.isNotEmpty(orderType)){
			//催款单
			filters.eq("presaleStatus", OrdersReceipt.PRESALE_STATUS_RETAINAGE);
		}
		for (Sort.Order order : pageable.getSort()) {
			if (pageable.toString().indexOf("nickName") != -1)
				order.setProperty("u_nick_name");
			if (pageable.toString().indexOf("member.phone") != -1)
				order.setProperty("u_phone");
			if (pageable.toString().indexOf("orderCode") != -1)
				order.setProperty("order_code");
		}
		Page<OrdersReceipt> orderList = ordersReceiptService.findPage(filters, pageable);
		
	    List<OrdersReceiptVO> excelDtoList= new ArrayList<>();

	    if (null != orderList && orderList.getData().size() > 0) {
	        for(OrdersReceipt orders : orderList.getData()){
	        	OrdersReceiptVO orderVO = new OrdersReceiptVO();
	        	orderVO.setOrderCode(orders.getOrderCode());
	        	orderVO.setOrderType(orders.getOrderType());
	        	orderVO.setStatus(orders.getStatus());
	        	orderVO.setGoodType(orders.getGoodType());
	        	orderVO.setOriginate(orders.getOriginate());
	        	orderVO.setUserName(orders.getUserName());
	        	orderVO.setShippingAddress(orders.getShippingAddress());
	        	orderVO.setTotalPrice(orders.getTotalPrice());
	        	orderVO.setDepositAmount(orders.getDepositAmount());
	        	orderVO.setDiscountAmount(orders.getDiscountAmount());
	        	orderVO.setExhibitionAmount(orders.getExhibitionAmount());
	        	orderVO.setCreatedTime(new SimpleDateFormat("yyyy-MM-dd").format(orders.getCreatedDate()));
	        	//以下set方法需要调用前提，具体请查看方法内容
	        	orderVO.setOrderTypeContext(null);
	        	orderVO.setStatusContext(null);
	        	orderVO.setGoodTypeContext(null);
	        	orderVO.setOrderTypeContext(null);
	        	orderVO.setAddress(null);
	        	orderVO.setRecvUserName(null);
	        	orderVO.setRecvUserPhone(null);
	        	excelDtoList.add(orderVO) ;
	        }
	    }

	    String[] titles = {"订单编号","订单类型","订单状态","订单商品状态",
	    					"订单来源","用户名","收货人","收货电话","收货地址",
	    					"订单金额", "定金金额", "现货金额", "优惠价格", "展会总金额", "下单时间"};
	    String[] methosName = {"orderCode","orderTypeContext","statusContext","goodTypeContext",
	    					"originateContext","userName","recvUserName","recvUserPhone","address",
	    					"totalPrice", "depositAmount", "totalPrice", "discountAmount", "exhibitionAmount", "createdTime"};
	    try {
	        ExcelUtil.outExcel("订单明细", titles, excelDtoList, methosName, response);
	    }catch(Exception e){
	        e.printStackTrace();
	    }
		
	}

	@ResponseBody
	@RequestMapping("cancel")
	public String cancelOrder(@RequestParam("id") Long orderId,@RequestParam("cancelReason") String cancelReason) {
		if (null != orderId) {
			OrdersReceipt or = new OrdersReceipt();
			or.setId(orderId);
			or.setStatus(Constant.ORDERS_STATUS_CANCELLED);
			or.setCancelReason(cancelReason);
			ordersReceiptService.cancelOrder(or);
			return "success";
		}
		return "error";
	}
	

	// 对待支付的订单可以进行修改总价
	@ResponseBody
	@RequestMapping("updateprice")
	public String updatePrice(Long id, BigDecimal discountAmount) {
		if(null == id || null == discountAmount){
			return "error";
		}
		OrdersReceipt order = ordersReceiptService.getOrdersReceiptById(id);
		if(Constant.ORDERS_STATUS_SUBMIT.equals(order.getStatus())){
			OrdersReceipt ordersReceipt = new OrdersReceipt();
			ordersReceipt.setId(id);
			ordersReceipt.setDiscountAmount(discountAmount);
			ordersReceiptService.updateDiscountAmount(ordersReceipt);
			return "success";
		}
		return "error";
	}

	@ResponseBody
	@RequestMapping("findAllPage")
	public List<Map<String, Object>> findAllPage() {
		return moduleService.findAllPage();
	}

	/**
	 * <p>名称：</p> 
	 * <p>描述：优惠金额</p>
	 * @author：wangchen
	 * @param 
	 * @return
	 */
	@ResponseBody
	@RequestMapping("updateExhibitionAmount")
	public ModelAndView updateDiscountAmount(@RequestParam(required = false, defaultValue = "") String discountAmount,
											   @RequestParam(required = false, defaultValue = "") String orderId) {
		ModelAndView mav = new ModelAndView();
		//moduleService.update(module);
		mav.setViewName(redirectViewPath("index"));
		return mav;
	}
	
	/**
	 * 
	 * <p>名称：根据id导出csv格式文件</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ids
	 * @param request
	 * @param response
	 */
	@RequestMapping("exportData")
	public void exportData(@RequestParam(value="idArr")Long[] ids,HttpServletRequest request,HttpServletResponse response) {
		exportDataCsv(ids, request, response);
	}
	
	/**
	 * 
	 * <p>名称：根据日期导出</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param beginDate
	 * @param endDate
	 * @param request
	 * @param response
	 */
	@RequestMapping("exportDataByTime")
	public void exportDataByTime(String beginDate,String endDate,HttpServletRequest request,HttpServletResponse response) {
		Long[] ids = ordersReceiptService.findIdByTime(beginDate, endDate);
		exportDataCsv(ids, request, response);
	}
	
	public void exportDataCsv(Long[] ids,HttpServletRequest request,HttpServletResponse response) {
		String[] headers = {"订单编号","订单类型","订单状态","订单商品状态","订单来源","用户名","收货人","收货电话","收货地址","订单金额","现货金额","展会总金额",
							"下单时间","商品编号","商品名称","商品规格","数量","单价","展会价","首付","小计","展会小计","预定截止日期","预定到货日期"};
		List<String> orderList = new ArrayList<String>();
		OrdersReceipt order = new OrdersReceipt();
		String str = "";
		for (Long id : ids) {
			int count = 0;
			order = ordersReceiptService.getOrdersReceiptById(id);
			if(order.getOrderType()==OrdersReceipt.ORDERTYPE_RESERVE&&order.getPresaleStatus()==OrdersReceipt.PRESALE_STATUS_PAYRETAINAGE){
				order.setStatus(100);
			}
			if(order.getOrderType()==OrdersReceipt.ORDERTYPE_RESERVE&&order.getPresaleStatus()==OrdersReceipt.PRESALE_STATUS_NEWWEIKUAN){
				order.setOrderType(3);
			}
			if (null != order) {
				str = order.getOrderCode() + "\t," +
					  getOrderType(order.getOrderType()) + "," +
					  getStatus(order.getStatus()) + "," +
					  getGoodsType(order.getGoodType()) + "," +
					  getOriginate(order.getOriginate()) + "," +
					  order.getUserName() + "\t," +
					  (null == order.getShippingAddress() ? "" : getShippingAddress(order.getShippingAddress(),"firstName") )+ "," +
					  (null == order.getShippingAddress() ? "" : getShippingAddress(order.getShippingAddress(),"phone1") )+ "\t," +
					  (null == order.getShippingAddress() ? "" : getShippingAddress(order.getShippingAddress(),"addressLine") )+ "," +
					  (null == order.getTotalPrice() ? "" : order.getTotalPrice()) + "," +
					  (null == order.getTotalPrice() ? "" : order.getTotalPrice()) + "," +
					  (null == order.getExhibitionAmount() ? "" : order.getExhibitionAmount()) + "," +
					  (null == order.getCreatedDate() ? "" : DateUtils.date2Str(order.getCreatedDate(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS));
					  for (OrdersReceiptDetail ordersReceiptDetail : order.getOrdersReceiptDetails()) {
						  String newStr = ",,,,,,,,,,,,";
						  if (count < 1) {
							  str += getOrdersReceiptDetail(ordersReceiptDetail,order);
							  orderList.add(str);
							  count ++;
						  } else {
							  newStr += getOrdersReceiptDetail(ordersReceiptDetail,order);
							  orderList.add(newStr);
						  }
					  }
					  
			}
		}
		String fileName = "order"+DateUtils.date2Str(new Date(), DateUtils.DATETIME_FORMAT_YYYYMMDDHHMMSS) + ".csv";
		CSVUtil.exportCsv(headers, orderList, response, fileName);
	}
	
	/**
	 * 
	 * <p>名称：获取订单下单品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param ordersReceiptDetail
	 * @param order
	 * @return
	 */
	private String getOrdersReceiptDetail(OrdersReceiptDetail ordersReceiptDetail,OrdersReceipt order) {
		String str = "," + ordersReceiptDetail.getGoodsCode() + "," +
					ordersReceiptDetail.getProductName() + "," +
					ordersReceiptDetail.getSpecValue().replaceAll(",", ";") + "," +
					ordersReceiptDetail.getGoodsNum() + "," +
					(null == ordersReceiptDetail.getGoodsPrice() ? "" : ordersReceiptDetail.getGoodsPrice()) + ",,," +
					//订单商品展会价及首付暂无显示
//					(null == order.getExhibitionAmount() ? "" : order.getExhibitionAmount()) + "," +
//					(null == order.getDepositAmount() ? "" : order.getDepositAmount()) + "," +
					(null == ordersReceiptDetail.getGtotalPrice() ? "" : ordersReceiptDetail.getGtotalPrice()) + ",," +
//					(null == order.getExhibitionAmount() || null == ordersReceiptDetail.getGoodsNum() ? "" : order.getExhibitionAmount().multiply(BigDecimal.valueOf(ordersReceiptDetail.getGoodsNum()))) + "," +
					(null == ordersReceiptDetail.getGoods().getReserveDeadline() ? "" : DateUtils.date2Str(ordersReceiptDetail.getGoods().getReserveDeadline(), DateUtils.DATETIME_FORMAT_YYYY_MM_DD_HHMMSS));
		return str;
	}
	
	/**
	 * 
	 * <p>名称：根据订单类型获取</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param orderType
	 * @return
	 */
	private String getOrderType(Integer orderType) {
		if (new Integer(1).equals(orderType)) {
			return "普通订单";
		} else if (new Integer(2).equals(orderType)) {
			return "定金订单";
		} else if (new Integer(4).equals(orderType)) {
			return "汇总订单";
		}else if (new Integer(3).equals(orderType)) {
			return "尾款订单";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：获取订单信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param status
	 * @return
	 */
	private String getStatus(Integer status) {
		if (new Integer(1).equals(status)) {
			return "待支付";
		} else if (new Integer(2).equals(status) || new Integer(3).equals(status) || new Integer(4).equals(status) ||
				new Integer(5).equals(status) || new Integer(6).equals(status)) {
			return "待发货";
		} else if (new Integer(7).equals(status)) {
			return "待收货";
		} else if (new Integer(8).equals(status)) {
			return "已完成";
		} else if (new Integer(10).equals(status)) {
			return "已取消";
		} else if (new Integer(11).equals(status)) {
			return "已评论";
		} else if (new Integer(100).equals(status)) {
			return "已支付";
		}else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：获取商品类型</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param goodsType
	 * @return
	 */
	private String getGoodsType(Integer goodsType) {
		if (new Integer(1).equals(goodsType)) {
			return "预订";
		} else if (new Integer(2).equals(goodsType)) {
			return "现货";
		} else if (new Integer(3).equals(goodsType)) {
			return "限购";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：获取下单方式</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param originate
	 * @return
	 */
	private String getOriginate(Integer originate) {
		if (new Integer(1).equals(originate)) {
			return "PC";
		} else if (new Integer(2).equals(originate)) {
			return "APP";
		} else if (new Integer(3).equals(originate)) {
			return "阅文";
		} else {
			return "";
		}
	}
	
	/**
	 * 
	 * <p>名称：解析收货方式</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param shippingAddress
	 * @param keyName
	 * @return
	 */
	private String getShippingAddress(String shippingAddress,String keyName) {
		JSONObject jsonObject=JSONObject.fromObject(shippingAddress);
		return jsonObject.getString(keyName);
	}
}
