/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * <p>名称: ProblemOrder</p>
 * <p>说明: 配单管理实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:12:24   
 * @version: 1.0
 */
public class ProblemOrder extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = -5961299730284479736L;

	/**
     * 订单实体
     * 
     * @ViewField
     * @Column ORDER_ID
     */
    private OrdersReceipt ordersReceipt;

    private long orderId;


    /**
     * 砍单数量
     *
     * @ViewField
     * @Column CUT_NUM;
     */
     private Integer cutNum;

    /**
     * 是否砍单
     *
     * @ViewField
     * @Column IS_CUT;
     */
    private Boolean isCut;

    /**
     * 砍单备注
     *
     * @ViewField
     * @Column CUT_REMARK;
     */
    private String cutRemark;

    /**
     * 砍单处理状态
     *
     * @ViewField
     * @Column CUT_STATUS;
     */
    private Integer cutStatus;

    /**
     * 配单状态
     *
     * @ViewField
     * @Column ALLOT_STATUS;
     */
    private Integer allotStatus;

    /**
     * 撤销配单备注
     *
     * @ViewField
     * @Column CANCEL_CUT_REMARK;
     */
    private String cancelCutRemark;

    /**
     * 删除
     * 1 删除
     * 0 未删除
     */
    private Boolean deleted;

    private BigDecimal price;

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getAllotStatus() {
        return allotStatus;
    }

    public void setAllotStatus(Integer allotStatus) {
        this.allotStatus = allotStatus;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public OrdersReceipt getOrdersReceipt() {
        return ordersReceipt;
    }

    public void setOrdersReceipt(OrdersReceipt ordersReceipt) {
        this.ordersReceipt = ordersReceipt;
    }

    public Integer getCutNum() {
        return cutNum;
    }

    public void setCutNum(Integer cutNum) {
        this.cutNum = cutNum;
    }

    public Boolean getIsCut() {
        return isCut;
    }

    public void setIsCut(Boolean isCut) {
        this.isCut = isCut;
    }

    public String getCutRemark() {
        return cutRemark;
    }

    public void setCutRemark(String cutRemark) {
        this.cutRemark = cutRemark;
    }

    public Integer getCutStatus() {
        return cutStatus;
    }

    public void setCutStatus(Integer cutStatus) {
        this.cutStatus = cutStatus;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getCancelCutRemark() {
        return cancelCutRemark;
    }

    public void setCancelCutRemark(String cancelCutRemark) {
        this.cancelCutRemark = cancelCutRemark;
    }
}
