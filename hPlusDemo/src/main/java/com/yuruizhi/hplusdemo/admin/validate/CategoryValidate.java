/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: CategoryValidate.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.validate
 * Date: 2016年8月18日上午10:41:34
 **/
package com.yuruizhi.hplusdemo.admin.validate;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.Model;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 商品分类导入验证</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年8月18日上午10:42:03   
 * @version: 1.0
 */
public class CategoryValidate {

	public static Map<String,Object> categoryValidate(String[] data,String error,int i,Boolean success) {
		Map<String,Object> map = Maps.newHashMap();
		Category category = new Category();
		if ( 5 != data.length) {
			error += "第"+(i+1)+"行数据导入失败，原因为字段数量不符合<br>";
			success = false;
			map.put("category", category);
			map.put("success", success);
			map.put("error", error);
			return map;
		}
		
		if (null != data[0] && !"".equals(data[0])) {
			if (250 < data[0].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为名称超过250个字符<br>";
				success = false;
			}
			category.setName(data[0]);
		}
		//排序
		if (null != data[3] && !"".equals(data[3])) {
			try {
				if (250 < data[3].length()) {
					error += "第"+(i+1)+"行数据导入失败，原因为排序超过250个字符<br>";
					success = false;
				}
				category.setSort(Integer.valueOf(data[3]));
			} catch (Exception e) {
				error += "第"+(i+1)+"行数据导入失败，原因为排序格式错误<br>";
				success = false;
			}
		}
		//是否显示
		if (null != data[4] && !"".equals(data[4])) {
			if ("是".equals(data[4])) {
				category.setVisible(true);
			} else {
				category.setVisible(false);
			}
		}
		map.put("category", category);
		map.put("success", success);
		map.put("error", error);
		return map;
	}
	
}
