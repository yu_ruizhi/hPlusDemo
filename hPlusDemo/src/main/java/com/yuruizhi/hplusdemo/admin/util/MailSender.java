/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.util;


import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <p>名称: MailSender</p>
 * <p>说明: 邮件发送工具类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:33:59   
 * @version: 1.0
 */
@Component
public class MailSender{
	
	@Value("${mail.isDebug}")
	private String isDebug;
	
	
	@Value("${mail.username}")
	private String userName;
	
	@Value("${mail.password}")
	private String password;
	
	@Value("${mail.host}")
	private String hostName;
	
	@Value("${mail.sendFrom}")
	private String sendFrom;
	
	@Value("${mail.charset}")
	private String charset;
	
	
	public void sendEmail(String subject, String emailAddress, String Content) throws EmailException{
		
			HtmlEmail email = new HtmlEmail();
			email.setDebug(Boolean.parseBoolean(isDebug));  
			email.setHostName(hostName);//smtp.sina.com.cn
			email.setAuthenticator(new DefaultAuthenticator(userName, password)); //邮箱验证
			email.addTo(emailAddress);      //接收的邮箱
			email.setFrom(sendFrom);    //发送的邮箱
			email.setCharset(charset);        //编码
			email.setSubject(subject);     //标题
			email.setHtmlMsg(Content);		//邮件内容
			
			email.send();
	}
	
	public Boolean sendEmails(String subject,String[] contents,String[] emailNos)throws EmailException{
		if(null != emailNos){
			
			
			for (int i = 0; i < emailNos.length; i++) {
				HtmlEmail email = new HtmlEmail();
				
				email.setDebug(Boolean.parseBoolean(isDebug));  
				email.setHostName(hostName);//smtp.sina.com.cn
				email.setAuthenticator(new DefaultAuthenticator(userName, password)); //邮箱验证
				email.setFrom(sendFrom);    //发送的邮箱
				email.setCharset(charset);        //编码
				email.setSubject(subject);     //标题
				email.addTo(emailNos[i]);
				email.setHtmlMsg(contents[i]);		//邮件内容
				@SuppressWarnings("unused")
				String result = email.send();
			}
		}
		return null;
	}
	
	/**
	 * @return the isDebug
	 */
	public String getIsDebug() {
		return isDebug;
	}

	/**
	 * @param isDebug the isDebug to set
	 */
	public void setIsDebug(String isDebug) {
		this.isDebug = isDebug;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @param hostName the hostName to set
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	/**
	 * @return the sendFrom
	 */
	public String getSendFrom() {
		return sendFrom;
	}

	/**
	 * @param sendFrom the sendFrom to set
	 */
	public void setSendFrom(String sendFrom) {
		this.sendFrom = sendFrom;
	}

	/**
	 * @return the charset
	 */
	public String getCharset() {
		return charset;
	}

	/**
	 * @param charset the charset to set
	 */
	public void setCharset(String charset) {
		this.charset = charset;
	}

	

}
