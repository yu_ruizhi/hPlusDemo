/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: MessageMemberDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月20日上午10:55:16
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MessageMember;

/**
 * 
 * <p>名称: 私信-会员Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月20日上午10:55:53 
 * @version: 1.0
 */
@MyBatisMapper
public interface MessageMemberDao extends BaseDao<MessageMember> {
	
	/**
	 * 
	 * <p>名称：根据会员id、私信id查询记录</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param messageMember 私信-会员实体
	 * @return
	 */
	MessageMember findByMemberIddMessageId(MessageMember messageMember);
}
