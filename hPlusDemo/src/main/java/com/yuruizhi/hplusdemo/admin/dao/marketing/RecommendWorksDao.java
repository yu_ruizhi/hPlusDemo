/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: RecommendWorksDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月21日 上午11:55:21 
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.RecommendWorks;

/**
 * 
 * <p>名称: 推荐作品 DAO</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月21日 上午11:55:21 
 * @version: 1.0
 */
public interface RecommendWorksDao extends BaseDao<RecommendWorks> {

	String BASE_FILTERS_PROP = "_recommend_works_filters";
	String WORKS_FILTERS_PROP = "_works_filters";
}
