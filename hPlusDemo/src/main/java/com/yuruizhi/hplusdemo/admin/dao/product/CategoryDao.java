/*
 * Copyright (c) 2005, 2014 vacoor
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.yuruizhi.hplusdemo.admin.dao.product;

import org.ponly.webbase.dao.support.mbt.MyBatisMapper;

import com.yuruizhi.hplusdemo.admin.dao.TreeNodeBaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.BrandCategory;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.WorksCategory;

import java.util.List;

/**
 * Created on 2015-07-09 16:28:55
 *
 * @author vacoor
 */
@MyBatisMapper
public interface CategoryDao extends TreeNodeBaseDao<Category> {

    void saveCategoryBrand(Long id, Long id2);

    List<BrandCategory> findBrandByCateId(Long id);

    void deleteBrandCateByid(Long bid);

    List<Category> findWebsiteCategories();

    List<Category> findManyTopmost();
    
    void saveCategoryWorks(Long id,Long id2);
    
    List<WorksCategory> findWorksByCateId(Long id);
    
    void deleteWorksCateByid(Long wid);

    Integer childrenCount(Long id);
    
    /**
     * 
     * <p>名称：根据名称查找数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    List<Category> findByName(String name);
    
    List<Category> findByModelId(Long modelId);

    List<Long> findChildrens(Long categoryId);
}