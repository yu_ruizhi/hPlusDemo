/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: CityService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area
 * Date: 2016年4月18日下午7:33:01
 **/
package com.yuruizhi.hplusdemo.admin.service.area;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.area.City;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 城市Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午7:33:23
 * @version: 1.0
 */
public interface CityService extends BaseService<City>{
	
	/**
	 * 
	 * <p>名称：通过城市编号查找城市</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param cityId 城市编号
	 * @return 城市
	 */
	City findByCityId(String cityId);
	
	/**
	 * 
	 * <p>名称：通过省份编号查找城市</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param pid 省份编号
	 * @return 城市列表
	 */
	List<City> findByPid(String pid);
}
