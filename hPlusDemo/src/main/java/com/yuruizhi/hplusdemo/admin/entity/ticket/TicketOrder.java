/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-service-interfaces 
 * FileName: Activity.java 
 * PackageName: com.yuruizhi.hplusdemo.entity.activity
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.ticket;




import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.member.Member;

import java.math.BigDecimal;
import java.util.List;

/**
 * 票务规格表
 */
public class TicketOrder extends BaseEntity {

    private Member member;

    private String orderNo;

    private BigDecimal price;

    private Integer qty;

    private Integer status;//0未支付，1已支付',

    private Boolean deleted;

    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    private List<TicketOrderDetail> ticketOrderDetails;

    public List<TicketOrderDetail> getTicketOrderDetails() {
        return ticketOrderDetails;
    }

    public void setTicketOrderDetails(List<TicketOrderDetail> ticketOrderDetails) {
        this.ticketOrderDetails = ticketOrderDetails;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
