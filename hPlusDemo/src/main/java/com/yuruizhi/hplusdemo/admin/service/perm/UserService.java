/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: UserService</p>
 * <p>说明: 后台用户服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:44:32   
 * @version: 1.0
 */
public interface UserService extends BaseService<User> {
    void saveAdminUser(User user, String roleIds);

    void updateAdminUser(User user, String roleIds);

    boolean deleteAdminUser(Long id);

    boolean deleteAminUserByIds(Long[] ids);

    void saveLastLoginTime(User user);

    Map<String,Object> changePassword(String origPassword, String newPassword);

    List<User> getBaseUser(Map<String, Object> paramMap);

    Map<String, Boolean> checkIsUserExists(String name);
    
    /**
     * 
     * <p>名称：根据角色名称获取用户信息</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    List<User> findByRoleName(String name);
}
