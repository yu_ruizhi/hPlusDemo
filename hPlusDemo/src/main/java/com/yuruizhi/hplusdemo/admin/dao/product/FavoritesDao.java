package com.yuruizhi.hplusdemo.admin.dao.product;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Favorites;



public interface FavoritesDao extends BaseDao<Favorites> {
}
