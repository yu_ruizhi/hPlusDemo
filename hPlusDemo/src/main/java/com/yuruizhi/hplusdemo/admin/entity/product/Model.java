/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Model</p>
 * <p>说明: 产品模型实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:22:27   
 * @version: 1.0
 */
public class Model extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -6093624906716291723L;
	/**
     * 产品模型名称
     * @Column
     */
    private String name;
    /**
     * 产品模型别名
     * @Column
     */
    private String alias;
    /**
     * 是否启用属性
     * @Column
     */
    private Boolean useAttribute = false;
    /**
     * 是否启用规格
     * @Column
     */
    private Boolean useSpec = false;
    /**
     * 是否启用参数
     * @Column
     */
    private Boolean useParameter = false;

    /**
     * @OneToMany fetch=eager optional=true
     */
    private List<Attribute> attributes;
    /**
     * @OneToMany fetch=eager optional=true
     */
    private List<Parameter> parameters;
    /**
     * @OneToMany fetch=eager optional=true
     */
    private List<ModelSpec> modelSpecs;

    /**
     * @Column
     */
    private Boolean deleted = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Boolean getUseAttribute() {
        return useAttribute;
    }

    public void setUseAttribute(Boolean useAttribute) {
        this.useAttribute = useAttribute;
    }

    public Boolean getUseSpec() {
        return useSpec;
    }

    public void setUseSpec(Boolean useSpec) {
        this.useSpec = useSpec;
    }

    public Boolean getUseParameter() {
        return useParameter;
    }

    public void setUseParameter(Boolean useParameter) {
        this.useParameter = useParameter;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public List<ModelSpec> getModelSpecs() {
        return modelSpecs;
    }

    public void setModelSpecs(List<ModelSpec> modelSpecs) {
        this.modelSpecs = modelSpecs;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
