/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.entity.product.Product;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ProductService</p>
 * <p>说明: 产品服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:07:55   
 * @version: 1.0
 */
public interface ProductService extends BaseService<Product> {

    Float getWeight(String goodsId);

    void saveOrUpdate(Product product);

    Page<Product> findPage(
            Filters baseFilters,    /* 产品条件过滤器 */
            Filters brandFilters,   /* 品牌条件过滤器 */
            Filters modelFilters,   /* 模型条件过滤器 */
            Filters categoryFilters,/* 分类条件过滤器 */
            Filters worksFilters,   /* 作品条件过滤器 */
            Filters supplierFilters,/* 供应商条件过滤器 */
            Pageable pageable ,
            Filters rFilters,/* 商品类型过滤条件 */
           String goodsCode
    );

    Page<Product> findPage(
            Filters baseFilters,    /* 产品条件过滤器 */
            Filters brandFilters,   /* 品牌条件过滤器 */
            Filters modelFilters,   /* 模型条件过滤器 */
            Filters categoryFilters,/* 分类条件过滤器 */
            Filters worksFilters,   /* 作品条件过滤器 */
            Filters supplierFilters,/* 供应商条件过滤器 */
            Pageable pageable ,
            Filters rFilters,/* 商品类型过滤条件 */
            String goodsCode,
            String perfected
    );
    
    /**
     * 
     * <p>名称：根据分类id获取商品信息</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param categoryId
     * @return
     */
    List<Product> findByCategoryId(Long categoryId);
    
    /**
     * <p>名称：只更新product表信息</p> 
     * <p>描述：TODO</p>
     * @author：zuoyang
     * @param product
     */
    void onlyUpdate(Product product);

    /**
     *
     * <p>名称：根据商品截止日期商品状态查询商品</p>
     * <p>描述：</p>
     * @author yuruizhi
     * @param parmsMap
     * @return
     */
    List<Product> searchProductByTimeAndStatus(Map<String,Object> parmsMap);
    
    List<Map<String,Object>> searchGoodsExcel(Map<String,Object> parmsMap);

    Integer findProductCount(Long modelId);

    void dr( List<Product> productList,List<Product> products);
    
}
