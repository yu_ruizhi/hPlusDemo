/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: WorksController.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.controller.product
 * Date: 2016年4月18日 下午5:48:00 
 **/
package com.yuruizhi.hplusdemo.admin.controller.product;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.AdminBaseController;
import com.yuruizhi.hplusdemo.admin.entity.product.Works;
import com.yuruizhi.hplusdemo.admin.service.product.WorksService;

/**
 * 
 * <p>名称: 作品Controller</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月18日 下午5:48:23  
 * @version: 1.0
 */
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/works")
public class WorksController extends AdminBaseController<Works> {

	@Autowired
	private WorksService worksService;

	/**
	 * 
	 * <p>名称：验证作品名称唯一性</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param name 需验证的作品名
	 * @return 如果该作品存在，key("isExists")对应value是true，反之为false。
	 */
	@RequestMapping("checkIsWorksExists")
	@ResponseBody
	public Map<String, Boolean> checkIsWorksExists(String name) {
		return worksService.checkIsWorksExists(name);
	}
	
	/**
	 * 
	 * <p>名称：验证除当前作品外，该作品名是否还存在</p> 
	 * <p>描述：</p>
	 * @author：wentan
	 * @param id 当前作品ID
	 * @param name 当前作品名
	 * @return 如果存在其他同名作品返回true,否则返回false
	 */
	@RequestMapping("checkIsAnotherWorksExists")
	@ResponseBody
	public Boolean checkIsAnotherWorksExists(Long id, String name) {
		return worksService.checkIsAnotherWorksExists(id, name);
	}
}
