/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: NewOrHotService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing
 * Date: 2016年4月20日 上午9:30:52
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.entity.marketing.NewOrHot;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 新品上架\APP首页艾曼热卖 Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wentan
 * @date：2016年4月20日 上午9:30:52   
 * @version: 1.0
 */
public interface NewOrHotService extends BaseService<NewOrHot> {

	/**
	 * 
	 * <p>名称：新品上架\APP首页艾曼热卖列表数据获取</p> 
	 * <p>描述：新品上架\APP首页艾曼热卖列表页异步获取的数据</p>
	 * @author：wentan
	 * @param filters 新品上架\APP首页艾曼热卖对象过滤器
	 * @param goodsFilters 单品对象过滤器
	 * @param categoryFilters 分类对象过滤器
	 * @param pageable 
	 * @return
	 */
	Page<NewOrHot> findPage(Filters filters, Filters goodsFilters, 
								Filters categoryFilters, Pageable pageable);

}
