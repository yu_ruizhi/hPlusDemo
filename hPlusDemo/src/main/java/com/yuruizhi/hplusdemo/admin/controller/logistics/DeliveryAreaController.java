package com.yuruizhi.hplusdemo.admin.controller.logistics;

import static com.yuruizhi.hplusdemo.admin.common.Constant.ADMIN_PREFIX;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryArea;
import com.yuruizhi.hplusdemo.admin.entity.area.Area;
import com.yuruizhi.hplusdemo.admin.entity.area.City;
import com.yuruizhi.hplusdemo.admin.entity.area.Province;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryAreaDetailService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryAreaService;
import com.yuruizhi.hplusdemo.admin.service.area.ProvinceService;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;
import com.google.common.collect.ImmutableMap;

@SuppressWarnings("deprecation")
@Controller
@RequestMapping(ADMIN_PREFIX + "/deliveryArea")
public class DeliveryAreaController extends BaseController {
    @Autowired
    private DeliveryAreaService deliveryAreaService;
    @Autowired
    private DeliveryAreaDetailService deliveryAreaDetailService;
    @Autowired
    private ProvinceService provinceService;
    @Autowired
    private UserService userService;

    @RequestMapping("index")
    public void index() {}

    @RequestMapping("list")
    @ResponseBody
    public Page<DeliveryArea> list(Filters filters, Pageable pageable) {
        Page<DeliveryArea> dlist = deliveryAreaService.findPage(filters, pageable);

        return dlist;
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(HttpServletRequest request, HttpServletResponse response) {
        List<Province> provinces = provinceService.listPvcToCity();
        List<Province> zx = provinceService.listMunicipalities();
        provinces.addAll(zx);
        request.setAttribute("provinces", provinces);
        return getViewPath("add");
    }

    @RequestMapping(value = "save")
    public String save(DeliveryArea deliveryArea, HttpServletRequest request,
                       HttpServletResponse response, HttpSession session) {
        Long currentUser = UserUtils.getCurrentUserId();
        User user = userService.find(currentUser);
        if (null == deliveryArea.getId()) {
            deliveryArea.setCreatedBy(user.getLoginName());
            deliveryArea.setDeleted(false);
            deliveryArea.setCreatedDate(new Date());
            deliveryAreaService.addArea(deliveryArea);
        } else {
            deliveryArea.setLastModifiedBy(user.getLoginName());
            deliveryArea.setLastModifiedDate(new Date());
            deliveryAreaService.bianji(deliveryArea);
        }
        return redirectViewPath("index");
    }

    @RequestMapping("delete")
    @ResponseBody
    public Map<String, Object> delete(@RequestParam("id") Long[] id) {
        deliveryAreaService.delete(id);
        for (Long iid : id) {
            deliveryAreaDetailService.deletedByAreaId(iid);
        }
        return ImmutableMap.<String, Object>of("success", true);
    }

    @RequestMapping("edit/{id}")
    public ModelAndView edit(@PathVariable(value = "id") Long id) {
        ModelAndView mav = new ModelAndView();
        DeliveryArea deliveryArea = deliveryAreaService.find(id);
        List<Province> provinces = provinceService.listPvcToCity();
        List<Province> zx = provinceService.listMunicipalities();
        List<Area> areaList1 = new ArrayList<Area>();
        List<Area> areaList2 = new ArrayList<Area>();
        List<Area> areaList3 = new ArrayList<Area>();
        List<Area> areaList4 = new ArrayList<Area>();
        for (Province province : zx) {
            List<City> cityList = province.getCities();
            if (province.getProvinceName().equals("北京市")) {
                for (City city : cityList) {
                    Area area = new Area();
                    area.setAreaId(city.getCityId());
                    area.setAreaName(city.getCityName());
                    areaList1.add(area);
                    city.setAreaList(areaList1);
                }
            }
            if (province.getProvinceName().equals("天津市")) {
                for (City city : cityList) {
                    Area area = new Area();
                    area.setAreaId(city.getCityId());
                    area.setAreaName(city.getCityName());
                    areaList2.add(area);
                    city.setAreaList(areaList2);
                }
            }
            if (province.getProvinceName().equals("上海市")) {
                for (City city : cityList) {
                    Area area = new Area();
                    area.setAreaId(city.getCityId());
                    area.setAreaName(city.getCityName());
                    areaList3.add(area);
                    city.setAreaList(areaList3);
                }
            }
            if (province.getProvinceName().equals("重庆市")) {
                for (City city : cityList) {
                    Area area = new Area();
                    area.setAreaId(city.getCityId());
                    area.setAreaName(city.getCityName());
                    areaList4.add(area);
                    city.setAreaList(areaList4);
                }
            }


        }

        provinces.addAll(zx);
        mav.addObject("provinces", provinces);
        mav.addObject("deliveryArea", deliveryArea);
        mav.setViewName(getViewPath("edit"));
        return mav;
    }
    
    /**
	 * 检测用户名
	 * @param name
	 * @return
	 */
	@RequestMapping("checkIsNameExists")
	@ResponseBody
	public Map<String, Boolean> checkIsNameExists(String name){
		return deliveryAreaService.checkIsNameExists(name);
	}
	
	@RequestMapping("hasAnnotherName")
	@ResponseBody
	public Map<String, Boolean> hasAnnotherName(String[] idAndName){
		Map<String, Boolean> map = deliveryAreaService.hasAnnotherName(idAndName);
		return map;
	}
}
