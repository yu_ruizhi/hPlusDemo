/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;

import java.util.Date;

import org.ponly.common.codec.Base64;
import org.ponly.common.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;


import com.yuruizhi.hplusdemo.admin.entity.marketing.Coupon;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CouponType;
import com.yuruizhi.hplusdemo.admin.entity.marketing.MemberCoupon;
import com.yuruizhi.hplusdemo.admin.dao.marketing.CouponDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.CouponTypeDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.MemberCouponDao;
import com.yuruizhi.hplusdemo.admin.service.marketing.CouponService;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;

/**
 * <p>名称: CouponServiceImpl</p>
 * <p>说明: 优惠券服务实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:13:54   
 * @version: 1.0
 */
@Service
public class CouponServiceImpl extends BaseServiceImpl<Coupon> implements CouponService {

	@Autowired
	private CouponDao couponDao;
	@Autowired
	private MemberCouponDao memberCouponDao;
	@Autowired
	private CouponTypeDao couponTypeDao;
	
	
	@Override
	public void couponSend(Long[] memberIds, Coupon coupon ) {
		Date now = new Date();
		CouponType couponType = new CouponType();
		//couponType.setId(coupon.getCouponTypeId());
		couponType = couponTypeDao.find(coupon.getCouponTypeId());
		if(null != memberIds&&coupon != null){
			Coupon tempCou = new Coupon();
			tempCou.setCouponType(couponType);
			tempCou.setDeadTime(coupon.getDeadTime());
			tempCou.setName(couponType.getName());
			tempCou.setAmount(couponType.getAmount());
			tempCou.setMinUsageAmount(couponType.getMinUsageAmount());
			tempCou.setSendTime(now);
			tempCou.setCreatedDate(now);
			tempCou.setLastModifiedDate(now);
			tempCou.setDeleted(false);
			tempCou.setStatus(2);
			tempCou.setCode("");
			for (int i = 0; i < memberIds.length; i++) {
				tempCou.setId(null);
				couponDao.save(tempCou);
				String code = createCouponCode(tempCou.getId(),new Date());
				tempCou.setCode(code);
				couponDao.update(tempCou);
				
				MemberCoupon tempMc = new MemberCoupon();
				tempMc.setMemberId(memberIds[i]);
				tempMc.setCoupon(tempCou);
				tempMc.setCreatedDate(now);
				tempMc.setLastModifiedDate(now);
				memberCouponDao.save(tempMc);
				
			}
		}
	}
	
	@Override
	public void batchSave(Coupon coupon, int createCount) {
		Date now = new Date();
		CouponType couponType = new CouponType();
		couponType = couponTypeDao.find(coupon.getCouponTypeId());
		
		Coupon tempCou = new Coupon();
		tempCou.setCouponType(couponType);
		tempCou.setDeadTime(coupon.getDeadTime());
		tempCou.setName(couponType.getName());
		tempCou.setAmount(couponType.getAmount());
		tempCou.setMinUsageAmount(couponType.getMinUsageAmount());
		tempCou.setSendTime(now);
		tempCou.setCreatedDate(now);
		tempCou.setLastModifiedDate(now);
		tempCou.setDeleted(false);
		tempCou.setStatus(1);
		tempCou.setCode("");
		for (int i = 0; i < createCount; i++) {
			tempCou.setId(null);
			couponDao.save(tempCou);
			String code = createCouponCode(tempCou.getId(),new Date());
			tempCou.setCode(code);
			couponDao.update(tempCou);
		}
		
	}

	private String createCouponCode(Long id, Date date) {
		String dateStr = date.getTime()+"";
		int len = dateStr.length();
		dateStr = dateStr.substring(len-5,len-1)+dateStr.substring(0,len-6);
		String result = ""+id+dateStr;
		//result = Bytes.toString(Base64.base64ToByteArray(result));
		result = Bytes.toString(Base64.encode(Bytes.toBytes(result)));
		result = result.substring(0, 10);
		return result;
	}


	
}
