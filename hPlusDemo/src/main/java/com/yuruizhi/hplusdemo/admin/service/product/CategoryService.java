/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.product.BrandCategory;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.WorksCategory;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: CategoryService</p>
 * <p>说明: 分类服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:02:23   
 * @version: 1.0
 */
public interface CategoryService extends BaseService<Category> {

    void saveCategoryBrand(Long id, Long id2);

    List<BrandCategory> findBrandByCateId(Long id);

    void deleteBrandCateById(Long bid);

    List<Category> findWebsiteCategoryTree();

    List<Category> findManyTopmost();
    	
    void saveCategoryWorks(Long id,Long id2);
    
    List<WorksCategory> findWorksByCateId(Long id);
    
    void deleteWorksCateByid(Long wid);

    void delete (Long[] ids);
  
    /**
     * 
     * <p>名称：根据名称查找数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
	List<Category> findByName(String name);
  
	List<Category> findByModelId(Long modelId);

    List<Long> findChildrens(Long categoryId);
}
