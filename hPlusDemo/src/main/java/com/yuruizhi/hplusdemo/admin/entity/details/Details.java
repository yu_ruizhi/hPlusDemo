/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.details;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryAreaDetail;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * <p>名称: Details</p>
 * <p>说明: 文书资料表</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：赵创
 * @date：2016年9月14日
 * @version: 1.0
 */
public class Details extends BaseEntity {

    private String name;  //文件名称

    private String url;//文件路径

    private Boolean deleted;

    private Integer isrelease;//是否发布

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getIsrelease() {
        return isrelease;
    }

    public void setIsrelease(Integer isrelease) {
        this.isrelease = isrelease;
    }
}
