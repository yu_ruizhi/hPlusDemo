/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.marketing.Promotion;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.PromotionService;

/**
 * <p>名称: PromotionServiceImpl</p>
 * <p>说明: 促销服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:23:16   
 * @version: 1.0
 */
@Service
public class PromotionServiceImpl extends BaseServiceImpl<Promotion> implements PromotionService {
}
