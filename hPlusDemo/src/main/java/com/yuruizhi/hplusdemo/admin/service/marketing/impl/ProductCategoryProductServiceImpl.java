/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryProductServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.marketing.impl
 * Date: 2016年4月20日 上午10:42:24  
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.marketing.ProductCategoryProductDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategoryProduct;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.ProductCategoryProductService;
import com.google.common.collect.Maps;

/**
 * 
 * <p>名称: 分类商品-商品表ServiceImpl</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:42:24   
 * @version: 1.0
 */
@Service
public class ProductCategoryProductServiceImpl extends BaseServiceImpl<ProductCategoryProduct> implements ProductCategoryProductService {

	private ProductCategoryProductDao productCategoryProductDao;
	
	@Autowired
	public void setProductCategoryProductDao(ProductCategoryProductDao productCategoryProductDao){
		this.productCategoryProductDao = productCategoryProductDao;
		setCrudDao(productCategoryProductDao);
	}

	@Override
	public List<ProductCategoryProduct> findByCategoryId(Long categoryId) {
		return productCategoryProductDao.findByCategoryId(categoryId);
	}

	@Override
	public ProductCategoryProduct findByIdAndCategoryId(Long goodsId, Long categoryId) {
		Map<String,Object> map = Maps.newHashMap();
		map.put("goodsId", goodsId);
		map.put("categoryId", categoryId);
		return productCategoryProductDao.findByIdAndCategoryId(map);
	}

}
