/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ModelValidate.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.validate
 * Date: 2016年8月18日下午1:09:47
 **/
package com.yuruizhi.hplusdemo.admin.validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.Attribute;
import com.yuruizhi.hplusdemo.admin.entity.product.AttributeValue;
import com.yuruizhi.hplusdemo.admin.entity.product.Model;
import com.yuruizhi.hplusdemo.admin.entity.product.ModelSpec;
import com.yuruizhi.hplusdemo.admin.entity.product.Parameter;
import com.google.common.collect.Maps;

public class ModelValidate {

	public static Map<String,Object> modelValidate(Model model,Attribute attribute,List<Attribute> attributeList,
													String[] data,String error,int i,Boolean success) {
		Map<String,Object> map = Maps.newHashMap();
		if (15 != data.length) {
			error += "第"+(i+1)+"行数据导入失败，原因为字段数不符合<br>";
			map.put("model", model);
			map.put("error", error);
			map.put("success", success);
			map.put("attribute", attribute);
			map.put("attributeList", attributeList);
			return map;
		}
		//是否启用属性
		if (null != data[1] && !"".equals(data[1])) {
			if ("是".equals(data[1])) {
				model.setUseAttribute(true);
			} else {
				model.setUseAttribute(false);
			}
		}
		//属性名称
		if (null != data[2] && !"".equals(data[2])) {
			if (250 < data[2].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为属性名称超过250个字符<br>";
				success = false;
			}
			attribute.setName(data[2]);
		}
		//属性别名
		if (null != data[3] && !"".equals(data[3])) {
			if (250 < data[3].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为属性别名超过250个字符<br>";
				success = false;
			}
			attribute.setAlias(data[3]);
		}
		//展示类型
		if (null != data[4] && !"".equals(data[4])) {
			int displayType = getDisplayType(data[4]);
			if (100 == displayType) {
				error += "第"+(i+1)+"行数据导入失败，原因为属性展示类型错误<br>";
				success = false;
			} else {
				attribute.setDisplayType(displayType);
			}
		}
		//属性值
		if (null != data[5] && !"".equals(data[5])) {
			if (250 < data[5].length()) {
				error += "第"+(i+1)+"行数据导入失败，原因为属性值超过250个字符<br>";
				success = false;
			}
			List<AttributeValue> attributeValues = new ArrayList<>();
			String value[] = data[5].split("\\|");
			//选择类型的属性值
			for (int j=0;j<value.length;j++) {
				AttributeValue attributeValue = new AttributeValue();
				attributeValue.setValue(value[j]);
				attributeValue.setSort(j+1);
				attributeValues.add(attributeValue);
			}
			attribute.setAttributeValues(attributeValues);
		}
		//排序
		if (null != data[6] && !"".equals(data[6])) {
			try {
				attribute.setSort(Integer.valueOf(data[6]));
			} catch (Exception e) {
				error += "第"+(i+1)+"行数据导入失败，原因为排序类型错误<br>";
				success = false;
			}
		}
		//属性是否显示
		if (null != data[7] && !"".equals(data[7])) {
			if ("是".equals(data[7])) {
				attribute.setVisible(true);
			} else {
				attribute.setVisible(false);
			}
			attributeList.add(attribute);
		}
		//是否启用规格
		if (null != data[8] && !"".equals(data[8])) {
			//当启用规格时，才对以下进行导入
			if ("是".equals(data[8])) {
				model.setUseSpec(true);
			} else {
				model.setUseSpec(false);
			}
		}
		map.put("model", model);
		map.put("error", error);
		map.put("success", success);
		map.put("attribute", attribute);
		map.put("attributeList", attributeList);
		return map;
	}
	
	/**
	 * 
	 * <p>名称：根据属性类型将String转为int</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param name
	 * @return
	 */
	private static int getDisplayType (String name) {
		if ("选择".equals(name)) {
			return 0;
		} else if ("文本".equals(name)) {
			return 1;
		} else if ("区间".equals(name)) {
			return 2;
		}
		return 100;
	}
}
