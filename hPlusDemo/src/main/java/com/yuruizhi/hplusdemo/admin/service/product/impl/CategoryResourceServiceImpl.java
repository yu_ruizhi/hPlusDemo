/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.CategoryResourceDao;
import com.yuruizhi.hplusdemo.admin.entity.product.CategoryResource;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryResourceService;

/**
 * <p>名称: CategoryResourceServiceImpl</p>
 * <p>说明: 分类资源服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:49:58   
 * @version: 1.0
 */
@Service
public class CategoryResourceServiceImpl extends BaseServiceImpl<CategoryResource> implements CategoryResourceService {

    @Autowired
    public void setCategoryDao(CategoryResourceDao categoryResourceDao) {
        super.setBaseDao(categoryResourceDao);
    }

}
