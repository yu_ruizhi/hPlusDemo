/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProvinceServiceImpl.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area.impl
 * Date: 2016年4月18日 下午7:30:00
 **/
package com.yuruizhi.hplusdemo.admin.service.area.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.area.ProvinceDao;
import com.yuruizhi.hplusdemo.admin.entity.area.Province;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.area.ProvinceService;

/**
 * 
 * <p>名称: 省份Service实现类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午7:30:29
 * @version: 1.0
 */
@Service
public class ProvinceServiceImpl extends BaseServiceImpl<Province> implements ProvinceService {
	@Autowired
	private ProvinceDao provinceDao;

	@Override
	public Province findByProvinceId(String provinceId) {
		return provinceDao.findByProvinceId(provinceId);
	}

	@Override
	public List<Province> listPvcToCity() {
		return provinceDao.listPvcToCity();
	}

	@Override
	public List<Province> listMunicipalities() {
		return provinceDao.listMunicipalities();
	}

}
