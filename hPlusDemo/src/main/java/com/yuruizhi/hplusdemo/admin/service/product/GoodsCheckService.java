/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsCheckService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.product
 * Date: 2016年4月26日 上午11:46:10 
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.math.BigDecimal;
import java.util.Map;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.entity.product.GoodsCheck;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 商品核价Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月26日 上午11:46:10   
 * @version: 1.0
 */
public interface GoodsCheckService extends BaseService<GoodsCheck> {

	/**
	 * 
	 * <p>名称：根据筛选条件获取核价商品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param filters 核价单品
	 * @param goodsFilters 单品
	 * @param productFilters 商品
	 * @param categoryFilters 商品分类
	 * @param brandFilters 商品品牌
	 * @param worksFilters 商品作品
	 * @param pageable 页码
	 * @return
	 */
	Page<GoodsCheck> findPage(Filters filters,  Filters goodsFilters, 
						Filters productFilters,Filters categoryFilters,
						Filters brandFilters,Filters worksFilters,Pageable pageable, String spotSell);

	/**
	 * 
	 * <p>名称：根据单品ID删除核价信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param id 单品id
	 */
	void deleteByGoodsId(Long id);
	
	
	/**
	 * 
	 * <p>名称：根据单品ID修改为未核价</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param id 单品id
	 */
	void updateChecked(Map<String,Object> map);
	
	/**
	 * 
	 * <p>名称：计算物流费用</p> 
	 * <p>描述：物流费用 = 每公斤物流费 * 重量</p>
	 * @author：wuqi
	 * @param logisticPrice 每公斤物流费
	 * @param weight 重量
	 * @return 返回物流费用
	 */
	BigDecimal getLogisticFee(BigDecimal logisticPrice,BigDecimal weight);
	
	/**
	 * 
	 * <p>名称：计算RMB成本价</p> 
	 * <p>描述：RMB成本价 = 日元成本价 / 汇率</p>
	 * @author：wuqi
	 * @param productPrice 日元成本价
	 * @param rate 汇率
	 * @return 返回RMB成本价
	 */
	BigDecimal getRmbCostPrice(BigDecimal productPrice,BigDecimal rate);
	
	/**
	 * 
	 * <p>名称：计算到岸成本价</p> 
	 * <p>描述：到岸成本价 = RMB 成本价 + 物流费用</p>
	 * @author：wuqi
	 * @param logisticFee  物流费用
	 * @param rmbCostPrice RMB 成本价
	 * @return 返回到岸成本价
	 */
	BigDecimal getLandedGoodsCost(BigDecimal logisticFee,BigDecimal rmbCostPrice);
	
	/**
	 * 
	 * <p>名称：计算经销商价格</p> 
	 * <p>描述：经销商价格 = RMB 成本价 / (1-经销毛利率-未成箱加点)+ 物流费用</p>
	 * @author：wuqi
	 * @param rmbCostPrice RMB 成本价
	 * @param sellGrossMargin 经销毛利率
	 * @param boxPoint 未成箱加点
	 * @param logisticFee 物流费用
	 * @return 返回经销商价格
	 */
	BigDecimal getSellPrice(BigDecimal rmbCostPrice,BigDecimal sellGrossMargin,BigDecimal boxPoint,BigDecimal logisticFee);
	
	/**
	 * 
	 * <p>名称：计算零售销售价格</p> 
	 * <p>描述：零售销售价格 = RMB 成本价 / (1-零售毛利率) + 物流费用</p>
	 * @author：wuqi
	 * @param rmbCostPrice RMB 成本价
	 * @param retailGrossMargin 零售毛利率
	 * @param logisticFee 物流费用
	 * @return 返回零售销售价格
	 */
	BigDecimal getRetailPrice(BigDecimal rmbCostPrice,BigDecimal retailGrossMargin,BigDecimal logisticFee);
	
	/**
	 * 
	 * <p>名称：计算淘宝销售价格</p> 
	 * <p>描述：淘宝销售价格 = RMB 成本价 / (1-淘宝毛利率) + 物流费用</p>
	 * @author：wuqi
	 * @param rmbCostPrice RMB 成本价
	 * @param taobaoGrossMargin 淘宝毛利率
	 * @param logisticFee 物流费用
	 * @return 返回淘宝销售价格
	 */
	BigDecimal getTaobaoPrice(BigDecimal rmbCostPrice,BigDecimal taobaoGrossMargin,BigDecimal logisticFee);
	
	/**
	 * 
	 * <p>名称：计算成箱价格</p> 
	 * <p>描述：成箱价格 = RMB 成本价 / (1 - 经销毛利率) + 物流费用</p>
	 * @author：wuqi
	 * @param rmbCostPrice RMB 成本价
	 * @param sellGrossMargin 经销毛利率
	 * @param logisticFee 物流费用
	 * @return 返回成箱价格
	 */
	BigDecimal getBoxPrice(BigDecimal rmbCostPrice,BigDecimal sellGrossMargin,BigDecimal logisticFee);

	/**
	 * 
	 * <p>名称：判断值是否为null</p> 
	 * <p>描述：判断传入的bigDecimal值是否为null，如果是null，返回0</p>
	 * @author：wuqi
	 * @param bigDecimal 传入的值
	 * @return 返回0或者bigDecimal值
	 */
	BigDecimal isNull(BigDecimal bigDecimal);
	
}
