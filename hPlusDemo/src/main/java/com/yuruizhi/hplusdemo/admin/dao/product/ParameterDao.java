/*
 * Copyright (c) 2014, by Besture All right reserved. 
 *
 */
package com.yuruizhi.hplusdemo.admin.dao.product;


import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Parameter;

/**
 * Created on 2014-07-25 09:11:01
 *
 * @author crud generated
 */
public interface ParameterDao extends BaseDao<Parameter> {
    List<Parameter> findModelParameterDetail(Long mid);
    void deleteByParentId(Long pid);
    void deleteModelParameters(Long mid);
}