package com.yuruizhi.hplusdemo.admin.dao.logistics;



import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethod;

/**
 * Created by ASUS on 2014/12/9.
 */
public interface DeliveryMethodDao extends BaseDao<DeliveryMethod> {
    List<DeliveryMethod> selectMethodGoCityId(String areaId);
    
    List<DeliveryMethod> findByName(String name);
}
