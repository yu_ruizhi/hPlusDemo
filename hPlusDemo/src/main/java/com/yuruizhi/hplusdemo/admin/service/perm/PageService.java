/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>名称: PageService</p>
 * <p>说明: 后台页面管理服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:42:29   
 * @version: 1.0
 */
public interface PageService extends BaseService<AdminPage> {
    void savePage(AdminPage adminPage);

    boolean deleteAdminPage(Long id);

    Page<AdminPage> findPage(
            Filters filters,
            Filters moduleFilters,
            Pageable pageable
    );

    boolean deleteAdminPageByIds(Long[] pageIds);

    List<AdminPage> getBasePage(Map<String, Object> paramMap);

    Map<String, Boolean> checkIsPageExists(String name);
}
