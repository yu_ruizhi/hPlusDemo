/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: AreaService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.area
 * Date: 2016年4月18日下午7:40:44
 **/
package com.yuruizhi.hplusdemo.admin.service.area;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.area.Area;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * 
 * <p>名称: 区Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年4月18日下午7:41:18
 * @version: 1.0
 */
public interface AreaService extends BaseService<Area> {
	
	/**
	 * 
	 * <p>名称：通过区编号查找区</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param areaId 区编号
	 * @return 区
	 */
	Area findByAreaId(String areaId);
	
	/**
	 * 
	 * <p>名称：通过城市编号查找区</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param pid 城市编号（数据表T_AREA中城市编号为pid，故与数据表一致）
	 * @return 区列表
	 */
	List<Area> findByCid(String pid);
}
