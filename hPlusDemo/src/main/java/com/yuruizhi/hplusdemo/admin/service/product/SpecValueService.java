/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.Map;

import com.yuruizhi.hplusdemo.admin.entity.product.SpecValue;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: SpecValueService</p>
 * <p>说明: 产品规格值服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:08:38   
 * @version: 1.0
 */
public interface SpecValueService extends BaseService<SpecValue> {
    SpecValue specValueBean(String id);
    
    /**
     * <p>名称：通过规格值编码查找</p> 
     * <p>描述：</p>
     * @author：qinzhongliang
     * @param code
     * @return
     */
    Map<String, Boolean> checkIsCodeExist(String code);
    
    /**
     * <p>名称：通过规格值编码查找</p> 
     * <p>描述：编辑页时需要判断已存在的是否就是自身</p>
     * @author：qinzhongliang
     * @param id
     * @param code
     * @return
     */
    Map<String, Boolean> editCheckIsCodeExist(Long id, String code);
    
    /**
     * 
     * <p>名称：根据规格id及名称获取数量</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param map
     * @return
     */
    SpecValue findBySpecIdAndName(Long specId,String name);
}
