package com.yuruizhi.hplusdemo.admin.dao.product;


import java.util.List;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.product.Model;


/**
 * Created on 2014-07-17 16:54:02
 *
 * @author crud generated
 */
public interface ModelDao extends BaseDao<Model> {

    Boolean isReferenced(Long mid);

    /**
     * 
     * <p>名称：根据名称获取数据</p> 
     * <p>描述：</p>
     * @author：wuqi
     * @param name
     * @return
     */
    List<Model> findByName(String name);

}
