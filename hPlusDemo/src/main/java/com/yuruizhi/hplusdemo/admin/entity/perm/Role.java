/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.perm;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: Role</p>
 * <p>说明: 后台角色实体 类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:15:39   
 * @version: 1.0
 */
public class Role extends BaseEntity {

    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = -8592094564693394049L;

	private String name;

    private String remark;

    private String auditPerm;

    private Boolean deleted;

    private List<AdminPage> pages;

    private List<RoleCategory> roleCategories;

    public List<RoleCategory> getRoleCategories() {
        return roleCategories;
    }

    public void setRoleCategories(List<RoleCategory> roleCategories) {
        this.roleCategories = roleCategories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<AdminPage> getPages() {
        return pages;
    }

    public void setPages(List<AdminPage> pages) {
        this.pages = pages;
    }

    public String getAuditPerm() {
        return auditPerm;
    }

    public void setAuditPerm(String auditPerm) {
        this.auditPerm = auditPerm;
    }
}
