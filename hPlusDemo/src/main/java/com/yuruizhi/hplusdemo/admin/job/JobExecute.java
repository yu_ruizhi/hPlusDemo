/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.yuruizhi.hplusdemo.admin.service.marketing.EmailService;
import com.yuruizhi.hplusdemo.admin.util.ScheduleUtils;

/**
 * <p>名称: JobExecute</p>
 * <p>说明: 定时任务执行类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:48:55   
 * @version: 1.0
 */
public class JobExecute extends QuartzJobBean{
    @SuppressWarnings("unused")
	private  static String baseLocation="";
    @Autowired
    private  static EmailService emailService;
    @SuppressWarnings("static-access")
	public void setBaseLocation(String baseLocation) {
        this.baseLocation = baseLocation;
    }

    @SuppressWarnings("static-access")
	public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    /**
     * <p>名称：send</p> 
     * <p>描述：邮件发送</p>
     * @author：ChenGuang
     * @param id
     */
    public void send(Long id){
    	Long[] ids = new Long[1];
    	ids[0] = id;
        emailService.emailSend(ids);
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("任务调度");
        String name = jobExecutionContext.getJobDetail().getKey().getName();
        String group = jobExecutionContext.getJobDetail().getKey().getGroup();
        Long[] ids = new Long[1];
        ids[0] = Long.parseLong(name);
        emailService.emailSend(ids);
        ScheduleUtils.deleteScheduleJob(jobExecutionContext.getScheduler(),name,group);
    }
}
