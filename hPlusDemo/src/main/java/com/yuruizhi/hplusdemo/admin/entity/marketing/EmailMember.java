/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.marketing;

import javax.validation.constraints.NotNull;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: EmailMember</p>
 * <p>说明: 邮件会员关系实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午3:56:21   
 * @version: 1.0
 */
public class EmailMember extends BaseEntity {

    /**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = 8574326852807754994L;

	/**
     * 邮件ID
     * 邮件主题
     * 
     * @ViewField editor=spinner 
     * @Column EMAIL_ID
     */
    @NotNull
    private Long emailId;

    /**
     * 会员ID
     * 邮件内容
     * 
     * @ViewField editor=spinner 
     * @Column MEMBER_ID
     */
    @NotNull
    private Long memberId;

    /**
     * 是否删除
     * 
     * @ViewField editor=input 
     * @Column DELETED
     */
    @NotNull
    private Boolean deleted = Boolean.FALSE; ;

    public Long getEmailId() {
        return this.emailId;
    }

    public void setEmailId(Long emailId) {
        this.emailId = emailId;
    }

    public Long getMemberId() {
        return this.memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Boolean getDeleted() {
        return this.deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}
