/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm;

import org.springframework.transaction.annotation.Transactional;

import com.yuruizhi.hplusdemo.admin.entity.perm.Role;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>名称: RoleService</p>
 * <p>说明: 后台角色管理服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:43:36   
 * @version: 1.0
 */
public interface RoleService extends BaseService<Role> {
    void saveRole(Role role, String pages);

    boolean deleteRole(Long id);

    @Transactional
    boolean deleteRoleByIds(Long[] ids);

    void updateRole(Role role, String pageIds);

    List<Map<String, Object>> findAllRoleTree();

    List<Role> getBaseRole(Map<String, Object> paramMap);

    Map<String, Boolean> checkIsRoleExists(String name);
}
