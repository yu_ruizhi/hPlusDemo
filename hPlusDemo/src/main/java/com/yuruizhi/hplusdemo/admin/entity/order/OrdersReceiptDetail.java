/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;

/**
 * <p>名称: OrdersReceiptDetail</p>
 * <p>说明: 订单明细实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:11:07   
 * @version: 1.0
 */
public class OrdersReceiptDetail extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 订单 id.
     */
    private String orderId;

    /**
     * 订单
     */
    private OrdersReceipt order;

    /**
     * 单品 id.
     */
    private String goodsId;

    /**
     * 单品货号.
     */
    private String goodsCode;

    /**
     * 单品名称.
     */
    private String productName;

    /**
     * 单品图片.
     */
    private String goodsPictureUrl;

    /**
     * 商品单价.
     */
    private BigDecimal goodsPrice;

    /**
     * 购买数量.
     */
    private Integer goodsNum;

    /**
     * 小计.
     */
    private BigDecimal gtotalPrice;

    /**
     * 备注.
     */
    private String remark;

    /**
     * 批次号.
     */
    private String batchCode;

    /**
     * 单品规格属性值.
     */
    private String specValue;

    /**
     * 订单类型
     */
    private String orderType;

    /**
     * 状态
     */
    private Integer status;

    @JsonUnwrapped(prefix = "goods.")
    private Goods goods;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    /**
     * 单品所属类别
     */
    private String category;

    private Boolean deleted;
    private  Boolean commented=false;

    /**
     * 商品类型（预定，现货，限购）
     */
    private Integer goodType;

    private Integer cutNum;  //砍单后剩余数量

    private Long relOrderID;//双向订单id存储字段，用于追踪定金订单和尾款订单

    public Integer getGoodType() {
        return goodType;
    }

    public void setGoodType(Integer goodType) {
        this.goodType = goodType;
    }

    public Integer getCutNum() {
        return cutNum;
    }

    public void setCutNum(Integer cutNum) {
        this.cutNum = cutNum;
    }

    public Long getRelOrderID() {
        return relOrderID;
    }

    public void setRelOrderID(Long relOrderID) {
        this.relOrderID = relOrderID;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public OrdersReceipt getOrder() {
        return order;
    }

    public void setOrder(OrdersReceipt order) {
        this.order = order;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getGoodsPictureUrl() {
        return goodsPictureUrl;
    }

    public void setGoodsPictureUrl(String goodsPictureUrl) {
        this.goodsPictureUrl = goodsPictureUrl;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public BigDecimal getGtotalPrice() {
        return gtotalPrice;
    }

    public void setGtotalPrice(BigDecimal gtotalPrice) {
        this.gtotalPrice = gtotalPrice;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getSpecValue() {
        return specValue;
    }

    public void setSpecValue(String specValue) {
        this.specValue = specValue;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getCommented() {
        return commented;
    }

    public void setCommented(Boolean commented) {
        this.commented = commented;
    }
}
