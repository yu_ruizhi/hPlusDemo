package com.yuruizhi.hplusdemo.admin.service.logistics;


import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethodDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * Created by ASUS on 2014/12/9.
 */
public interface DeliveryMethodDetailService extends BaseService<DeliveryMethodDetail> {
    void deletedByMethodId(Long deliveryMethodId);
    void  deletedMethodId(Long deliveryMethodId);
}
