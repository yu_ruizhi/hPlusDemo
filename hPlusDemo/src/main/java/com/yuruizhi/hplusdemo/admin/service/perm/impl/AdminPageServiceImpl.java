/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.perm.impl;

import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Pageable;
import org.ponly.webbase.domain.Sort;
import org.ponly.webbase.domain.support.SimplePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.util.StringUtils;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.dao.perm.PageDao;
import com.yuruizhi.hplusdemo.admin.entity.perm.AdminPage;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.perm.PageService;

import java.util.*;

/**
 * <p>名称: AdminPageServiceImpl</p>
 * <p>说明: 后台页面管理服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:38:34   
 * @version: 1.0
 */
@Service
public class AdminPageServiceImpl extends BaseServiceImpl<AdminPage> implements PageService {

    private PageDao pageDao;

    @Autowired
    public void setAdminPageDao(PageDao pageDao){
        this.pageDao = pageDao;
        setBaseDao(pageDao);
    }

    @Override
    public void savePage(AdminPage adminPage) {
        adminPage.setLastModifiedDate(new Date());
        adminPage.setDeleted(Constant.NOT_DELETED);
        save(adminPage);
    }

    @Override
    public boolean deleteAdminPage(Long id) {
        if(null == id){
            return false;
        }

        AdminPage adminPage = new AdminPage();
        adminPage.setId(id);
        delete(adminPage);
        return true;
    }

	@SuppressWarnings("deprecation")
	@Override
    public org.ponly.webbase.domain.Page<AdminPage> findPage(
            Filters filters,
            Filters moduleFilters,
            Pageable pageable
    ) {
        filters = new IterateNamingTransformFilters(filters);
        moduleFilters = new IterateNamingTransformFilters(moduleFilters);

        Map<String, Object> paramsMap = createParamsMap();
        if (null != filters) {
            paramsMap.put(PageDao.PAGE_FILTERS_PROP, filters);
        }
        if (null != moduleFilters) {
            paramsMap.put(PageDao.MODULE_FILTERS_PROP, moduleFilters);
        }

        if (null != pageable) {
            paramsMap.put(BaseDao.OFFSET_PROP, pageable.getOffset());
            paramsMap.put(BaseDao.MAX_RESULTS_PROP, pageable.getPageSize());

            Sort sort = pageable.getSort();
            sort = new IterateNamingTransformSort(sort);
            if (null != sort) {
                paramsMap.put(BaseDao.SORT_PROP, sort);
            }
        }

        int total = count(paramsMap);
        List<AdminPage> data = total > 0 ? findMany(paramsMap) : Collections.<AdminPage>emptyList();

        return new SimplePage<AdminPage>(pageable, data, total);
    }

    @Override
    
    public boolean deleteAdminPageByIds(Long[] pageIds) {
        if(StringUtils.isEmpty(pageIds)){
            return false;
        }
        for(Long id: pageIds){
            deleteAdminPage(id);
        }
        return true;
    }

    @Override
    public List<AdminPage> getBasePage(Map<String, Object> paramMap) {
        return pageDao.getBasePage(paramMap);
    }

    @Override
    public Map<String, Boolean> checkIsPageExists(String name){
        Map<String, Boolean> result = new HashMap<String, Boolean>();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("deleted", false);
        paramMap.put("name", name);
        List<AdminPage> pages = getBasePage(paramMap);
        if(pages.size()>0){
            result.put("isExists", true);
        }else {
            result.put("isExists", false);
        }
        return result;
    }
}
