package com.yuruizhi.hplusdemo.admin.service.logistics.impl;

import com.yuruizhi.hplusdemo.admin.dao.logistics.DeliveryModuleDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModule;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryModuleDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryModuleDetailService;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by ASUS on 2015/1/13.
 */
@Service
public class DeliveryModuleServiceImpl extends BaseServiceImpl<DeliveryModule> implements DeliveryModuleService {
    @Autowired
    private DeliveryModuleDetailService deliveryModuleDetailService;

    private DeliveryModuleDao deliveryModuleDao;
    @Autowired
    public void setAdminUserDao(DeliveryModuleDao deliveryModuleDao){
        this.deliveryModuleDao = deliveryModuleDao;
        setBaseDao(deliveryModuleDao);
    }

    @Override
    @Transactional
    public void saveModule(DeliveryModule deliveryModule) {
        if(StringUtils.isEmpty(deliveryModule)){
            return;
        }
        List<DeliveryModuleDetail> deliveryModuleDetails = deliveryModule.getDeliveryModuleDetails();
        deliveryModule.setDeleted(false);
        save(deliveryModule);

        for(int i=0; i<deliveryModuleDetails.size(); i++){
            DeliveryModuleDetail deliveryModuleDetail = deliveryModuleDetails.get(i);
            deliveryModuleDetail.setDeleted(false);
            deliveryModuleDetail.setModuleId(deliveryModule.getId());
            deliveryModuleDetailService.save(deliveryModuleDetail);
        }
    }

    @Override
    public void updateModule(DeliveryModule deliveryModule) {
        if(StringUtils.isEmpty(deliveryModule)){
            return;
        }
        update(deliveryModule);
        Long moduleId = deliveryModule.getId();
        deliveryModuleDetailService.deleteByModuleId(moduleId);

        List<DeliveryModuleDetail> deliveryModuleDetails = deliveryModule.getDeliveryModuleDetails();
        for(int i=0; i<deliveryModuleDetails.size(); i++){
            DeliveryModuleDetail deliveryModuleDetail = deliveryModuleDetails.get(i);
            deliveryModuleDetail.setDeleted(false);
            deliveryModuleDetail.setModuleId(deliveryModule.getId());
            deliveryModuleDetailService.save(deliveryModuleDetail);
        }
    }

}
