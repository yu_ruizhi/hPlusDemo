/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;


import com.yuruizhi.hplusdemo.admin.dao.marketing.CombineDao;
import com.yuruizhi.hplusdemo.admin.dao.marketing.CombineDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Combine;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CombineDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.CombineService;

/**
 * <p>名称: CombineServiceImpl</p>
 * <p>说明: 商品组合服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:09:46   
 * @version: 1.0
 */
@Service
public class CombineServiceImpl extends BaseServiceImpl<Combine> implements CombineService {

	@Autowired
	private CombineDao combineDao;
	@Autowired
	private CombineDetailDao combineDetailDao;
	
	@Override
	public Map<String, Object> combineSave(Combine combine, String goodsIds, String productIds,String channel) {
		// TODO Auto-generated method stub
		if(channel.equals("combo")){
			combine.setType(Combine.COMBO);
		}else if(channel.equals("accessory")){
			combine.setType(Combine.ACCESSORY);
			combine.setPrice(0d);
		}
		Date now = new Date();
		combine.setLastModifiedDate(now);
		combine.setCreatedDate(now);
		combineDao.save(combine);
		String[] goodsIdArr  = goodsIds.split(",");
		String[] productIdArr = goodsIds.split(",");
		CombineDetail comdePry = new CombineDetail();
		comdePry.setCombine(combine);
		comdePry.setIsPrimary(true);
		comdePry.setGoodsId(combine.getPrimaryGoodsId());
		comdePry.setProductId(combine.getPrimaryProductId());
		comdePry.setPosition(1);
		comdePry.setCreatedDate(now);
		comdePry.setLastModifiedDate(now);
		combineDetailDao.save(comdePry);
		for (int i = 0; i < goodsIdArr.length; i++) {
			CombineDetail combineDetail = new CombineDetail();
			combineDetail.setCombine(combine);
			combineDetail.setGoodsId(Long.valueOf(goodsIdArr[i]));
			combineDetail.setProductId(Long.valueOf(productIdArr[i]));
			combineDetail.setIsPrimary(false);
			combineDetail.setPosition(i+2);
			combineDetail.setCreatedDate(now);
			combineDetail.setLastModifiedDate(now);
			combineDetailDao.save(combineDetail);
		}
		return null;
	}

	@Override
	public Map<String, Object> combineUpdate(Combine combine, String goodsIds, String productIds, String ids,String channel) {
		if(channel.equals("combo")){
			combine.setType(Combine.COMBO);
		}else if(channel.equals("accessory")){
			combine.setType(Combine.ACCESSORY);
			combine.setPrice(0d);
		}
		Date now = new Date();
		combine.setLastModifiedDate(now);
		combineDao.update(combine);
		CombineDetail comdePry = new CombineDetail();
		comdePry.setCombine(combine);
		comdePry.setIsPrimary(true);
		comdePry.setGoodsId(combine.getPrimaryGoodsId());
		comdePry.setProductId(combine.getPrimaryProductId());
		comdePry.setPosition(1);
		comdePry.setLastModifiedDate(now);
		combineDetailDao.update(comdePry);
		combineDetailDao.deleteByCombineId(combine.getId());
		String[] goodsIdArr  = goodsIds.split(",");
		String[] productIdArr = goodsIds.split(",");
		@SuppressWarnings("unused")
		String[] idArr = ids.split(",");
		for (int i = 0; i < goodsIdArr.length; i++) {
			CombineDetail combineDetail = new CombineDetail();
			combineDetail.setCombine(combine);
			combineDetail.setGoodsId(Long.valueOf(goodsIdArr[i]));
			combineDetail.setProductId(Long.valueOf(productIdArr[i]));
			combineDetail.setIsPrimary(false);
			combineDetail.setPosition(i+2);
			combineDetail.setCreatedDate(now);
			combineDetail.setLastModifiedDate(now);
			combineDetailDao.save(combineDetail);
		}
		return null;
	}

	@Override
	public Map<String, Object> combineDelete(Long[] id) {
		for (int i = 0; i < id.length; i++) {
			combineDetailDao.deleteAllByCombineId(id[i]);
			combineDao.deleteById(id[i]);
		}
		return null;
	}
}
