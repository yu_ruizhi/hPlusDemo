/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: aimon-inoutstock 
 * FileName: Supplier.java 
 * PackageName: com.glanway.inoutstock.entity
 * Date: 2016年5月9日下午4:04:23   
 **/
package com.yuruizhi.hplusdemo.admin.entity.provider;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**      
 * <p>名称:Supplier </p>
 * <p>说明: 供应商</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：lushanshan
 * @date：2016年5月9日下午4:13:04   
 * @version: 1.0 
 */
public class Supplier extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    /**@Fields supplierName : 供应商名称 */ 
    private String supplierName;

    /**@Fields abbreviation :供应商简称 */ 
    private String abbreviation;

    /**@Fields website : 供应商站点 */ 
    private String website;
  
    /**@Fields payBank : 付款银行 */ 
    private String payBank;

    /**@Fields payCode : 付款账号 */ 
    private String payCode;

    /**@Fields tariff : 税号 */ 
    private String tariff;

    /**@Fields rank : 供应商等级 */ 
    private String rank;

    /**@Fields organizationCode : 组织机构代码 */ 
    private String organizationCode;

    /**@Fields legalName : 法人代表 */ 
    private String legalName;

    /**@Fields rating : 综合评分 */ 
    private BigDecimal rating = new BigDecimal(100);

    /**@Fields type : 类型 */ 
    private String type;

    /**@Fields results : 经营业绩 */ 
    private String results;

    /**@Fields registeredCapital : 注册资本 */ 
    private String registeredCapital;

    /**@Fields managementLevel : 管理水平 */ 
    private String managementLevel;

    /**@Fields productCategory : 产品分类*/ 
    private String productCategory;

    /**@Fields productLicensing :特殊产品许可 */ 
    private String productLicensing;

    /**@Fields userName : 联系人 */ 
    private String userName;

    /**@Fields email : 联系邮箱*/ 
    private String email;
    
    /**@Fields phone : 联系电话*/ 
    private String phone;

    /**@Fields address : 地址 */ 
    private String address;

    /**@Fields remark : 备注 */ 
    private String remark;
    
    /**@Fields fax : 传真*/ 
    private String fax;

    /**@Fields numberCode :供应商编号*/ 
    private Long numberCode;

    /**@Fields receiptContacts : 收货联系人 */ 
    private String receiptContacts;

    /**@Fields receiptTel : 收货电话 */ 
    private String receiptTel;

    /**@Fields receiptAddress : 收货地址 */ 
    private String receiptAddress;

    /**@Fields zipCode : 收货邮编 */ 
    private String zipCode;

    /**@Fields deleted : 删除标志 */ 
    private Boolean deleted = Boolean.FALSE;

    /**@Fields details : 供应商商品详情 */ 
    private List<SupplierGoodsList> details;

    public List<SupplierGoodsList> getDetails() {
        return details;
    }

    public void setDetails(List<SupplierGoodsList> details) {
        this.details = details;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierName() {
        return this.supplierName;
    }

    public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getPayBank() {
		return payBank;
	}

	public void setPayBank(String payBank) {
		this.payBank = payBank;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public String getTariff() {
		return tariff;
	}

	public void setTariff(String tariff) {
		this.tariff = tariff;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public void setOrganizationCode(String organizationCode) {
        this.organizationCode = organizationCode;
    }

    // @Column(name = "ORGANIZATION_CODE")
    public String getOrganizationCode() {
        return this.organizationCode;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    // @Column(name = "LEGAL_NAME")
    public String getLegalName() {
        return this.legalName;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    // @Column(name = "RATING")
    public BigDecimal getRating() {
        return this.rating;
    }

    public void setType(String type) {
        this.type = type;
    }

    // @Column(name = "TYPE")
    public String getType() {
        return this.type;
    }

    public void setResults(String results) {
        this.results = results;
    }

    // @Column(name = "RESULTS")
    public String getResults() {
        return this.results;
    }

    public void setRegisteredCapital(String registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    // @Column(name = "REGISTERED_CAPITAL")
    public String getRegisteredCapital() {
        return this.registeredCapital;
    }

    public void setManagementLevel(String managementLevel) {
        this.managementLevel = managementLevel;
    }

    // @Column(name = "MANAGEMENT_LEVEL")
    public String getManagementLevel() {
        return this.managementLevel;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    // @Column(name = "PRODUCT_CATEGORY")
    public String getProductCategory() {
        return this.productCategory;
    }

    public void setProductLicensing(String productLicensing) {
        this.productLicensing = productLicensing;
    }

    // @Column(name = "PRODUCT_LICENSING")
    public String getProductLicensing() {
        return this.productLicensing;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    // @Column(name = "USER_NAME")
    public String getUserName() {
        return this.userName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // @Column(name = "EMAIL")
    public String getEmail() {
        return this.email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    // @Column(name = "PHONE")
    public String getPhone() {
        return this.phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    // @Column(name = "ADDRESS")
    public String getAddress() {
        return this.address;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    // @Column(name = "REMARK")
    public String getRemark() {
        return this.remark;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    // @Column(name = "DELETED")
    public Boolean getDeleted() {
        return this.deleted;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Long getNumberCode() {
        return numberCode;
    }

    public void setNumberCode(Long numberCode) {
        this.numberCode = numberCode;
    }

    public String getReceiptContacts() {
        return receiptContacts;
    }

    public void setReceiptContacts(String receiptContacts) {
        this.receiptContacts = receiptContacts;
    }

    public String getReceiptTel() {
        return receiptTel;
    }

    public void setReceiptTel(String receiptTel) {
        this.receiptTel = receiptTel;
    }

    public String getReceiptAddress() {
        return receiptAddress;
    }

    public void setReceiptAddress(String receiptAddress) {
        this.receiptAddress = receiptAddress;
    }
}
