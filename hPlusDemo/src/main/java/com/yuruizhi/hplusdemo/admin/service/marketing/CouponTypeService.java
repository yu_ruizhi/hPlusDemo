/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing;

import com.yuruizhi.hplusdemo.admin.service.BaseService;
import com.yuruizhi.hplusdemo.admin.entity.marketing.CouponType;

/**
 * <p>名称: CouponTypeService</p>
 * <p>说明: 优惠券模板服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:13:32   
 * @version: 1.0
 */
public interface CouponTypeService extends BaseService<CouponType> {
}
