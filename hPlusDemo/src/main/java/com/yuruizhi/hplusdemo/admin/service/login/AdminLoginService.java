/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.login;

import com.yuruizhi.hplusdemo.admin.entity.perm.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * <p>名称: AdminLoginService</p>
 * <p>说明: 后台用户登录服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:08:01   
 * @version: 1.0
 */
public interface AdminLoginService {
    java.util.Map<String, Object> loginAndTimeUpdate(String username,
                                                     String password,
                                                     String geetest_challenge,
                                                     String geetest_validate,
                                                     String geetest_seccode,
                                                     HttpServletRequest request,
                                                     HttpServletResponse response,
                                                     HttpSession session)  throws IOException;

    void logout();

    User getCurrentUser();
}
