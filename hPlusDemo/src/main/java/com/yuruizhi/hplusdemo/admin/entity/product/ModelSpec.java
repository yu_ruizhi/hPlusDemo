/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.product;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * <p>名称: ModelSpec</p>
 * <p>说明: 产品模型规格实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:22:52   
 * @version: 1.0
 */
public class ModelSpec extends BaseEntity {
    /**
	 * @fieldName: serialVersionUID
	 * @fieldType: long
	 * @Description: TODO
	 */
	private static final long serialVersionUID = 1692819825373813398L;
	/**
     * @OneToOne
     */
    private Model model;
    /**
     * @OneToOne
     */
    private Spec spec;
    /**
     * @Column
     */
    private Boolean isAffectGoods;
    /**
     * @Column
     */
    private Integer sort;
    /**
     * @Column
     */
    private Boolean deleted = false;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Spec getSpec() {
        return spec;
    }

    public void setSpec(Spec spec) {
        this.spec = spec;
    }

    public Boolean getIsAffectGoods() {
        return isAffectGoods;
    }

    public void setIsAffectGoods(Boolean isAffectGoods) {
        this.isAffectGoods = isAffectGoods;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
