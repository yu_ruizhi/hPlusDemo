/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.marketing.FlashSale;
import com.yuruizhi.hplusdemo.admin.entity.marketing.FlashSaleDetail;
import com.yuruizhi.hplusdemo.admin.entity.marketing.Promotion;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.FlashSaleDetailService;
import com.yuruizhi.hplusdemo.admin.service.marketing.FlashSaleService;
import com.yuruizhi.hplusdemo.admin.service.marketing.PromotionService;

import java.util.Date;
import java.util.List;

/**
 * <p>名称: FlashSaleServiceImpl</p>
 * <p>说明: 活动优惠等服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:21:23   
 * @version: 1.0
 */
@Service
public class FlashSaleServiceImpl extends BaseServiceImpl<FlashSale> implements FlashSaleService {
    @Autowired
    private PromotionService promotionService;
    @Autowired
    private FlashSaleDetailService flashSaleDetailService;

    @Override
    public void save(final FlashSale flashSale) {
        flashSale.setMode(Promotion.PROMOTION_MODE_FLASH_SALE);
        flashSale.setDeleted(false);
        Date now = new Date();
        Date startTime = flashSale.getStartTime();
        Date endTime = flashSale.getEndTime();
        startTime = null != startTime ? startTime : now;
        endTime = null != endTime ? endTime : now;


        if (endTime.getTime() <= now.getTime()) {               // 已经结束

        } else if (startTime.getTime() >= now.getTime()) {      // 已开始

        } else {    // 未开始

        }

        promotionService.save(flashSale);
        super.save(flashSale);

        new Merger<FlashSaleDetail>(flashSaleDetailService) {
            @Override
            protected FlashSaleDetail apply(FlashSaleDetail left, FlashSaleDetail right) {
                FlashSaleDetail detail = super.apply(left, right);
                detail.setFlashSale(flashSale);
                detail.setDeleted(false);
                return detail;
            }
        }.doMerge(flashSale.getFlashSaleDetails(), null);
    }

    @Override
    public void update(final FlashSale flashSale) {
        promotionService.update(flashSale);
        super.update(flashSale);
        List<FlashSaleDetail> details = flashSaleDetailService.findMany("promotionId", flashSale.getId());

        new Merger<FlashSaleDetail>(flashSaleDetailService) {
            @Override
            protected FlashSaleDetail apply(FlashSaleDetail left, FlashSaleDetail right) {
                FlashSaleDetail detail = super.apply(left, right);
                detail.setFlashSale(flashSale);
                if (null == right) {
                    detail.setDeleted(false);
                }
                return detail;
            }
        }.doMerge(flashSale.getFlashSaleDetails(), details);
    }

    @Override
    public void delete(FlashSale flashSale) {
        flashSaleDetailService.delete("promotionId", flashSale.getId());
        super.delete(flashSale);
        promotionService.delete(flashSale);
    }
}
