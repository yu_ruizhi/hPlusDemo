/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.member;

import com.yuruizhi.hplusdemo.admin.entity.member.MemberAddress;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: MemberAddressService</p>
 * <p>说明: 会员地址服务接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:25:31   
 * @version: 1.0
 */
public interface MemberAddressService extends BaseService<MemberAddress> {
	
	/**
	 * 新增
	 */
	public void insert(MemberAddress memberAddress);
	
	/**
	 * 修改默认地址
	 */
	public int updateDefault(MemberAddress memberAddress);
	
	/**
	 * 删除默认
	 */
	public int deleteDefault(MemberAddress memberAddress);
	

}
