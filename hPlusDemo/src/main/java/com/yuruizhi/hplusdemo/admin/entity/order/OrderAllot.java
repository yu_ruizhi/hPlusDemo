/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.entity.order;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

import java.util.Date;

/**
 * <p>名称: OrderAllot</p>
 * <p>说明: 配单管理实体类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午4:10:19   
 * @version: 1.0
 */
public class OrderAllot extends BaseEntity {



/**@Fields serialVersionUID : TODO */ 
	private static final long serialVersionUID = -3503435930647307313L;
	//配单状态
    public static int ALLOT_NOT_ARRIVING  =1  ;// 未到货
    public static  int        ALLOT_NOT_REMIND  =2  ;  // 到货未提醒
    public static int ALLOT_OTHER    =3 ;   // 已提醒
//砍单处理状态
public static int CUT_NOT_REFUND  =1  ;// 待退款
    public static  int CUT_REFUND  =2  ;  // 已退款
    /**
     * 订单实体
     * 
     * @ViewField
     * @Column ORDER_ID
     */
    @JsonUnwrapped(prefix = "ordersReceipt.")
    private OrdersReceipt ordersReceipt;

    /**
     * 砍单数量
     *
     * @ViewField
     * @Column CUT_NUM;
     */
     private Integer cutNum;

    /**
     * 是否砍单
     *
     * @ViewField
     * @Column IS_CUT;
     */
    private Boolean isCut;

    /**
     * 砍单备注
     *
     * @ViewField
     * @Column CUT_REMARK;
     */
    private String cutRemark;

    /**
     * 砍单处理状态
     *
     * @ViewField
     * @Column CUT_STATUS;
     */
    private Integer cutStatus;

    /**
     * 配单状态
     *
     * @ViewField
     * @Column ALLOT_STATUS;
     */
    private Integer allotStatus;

    /**
     * 撤销配单备注
     *
     * @ViewField
     * @Column CANCEL_CUT_REMARK;
     */
    private String cancelCutRemark;

    /**
     * 到货日期
     *
     * @ViewField
     * @Column ARRIVAL_TIME;
     */
     private Date arrivalTime;

    /**
     * 到货数量
     *
     * @ViewField
     * @Column ALLOT_NUM;
     */
     private Integer allotNum;

    private Date allotTime;//下单时间

    public Date getAllotTime() {
        return allotTime;
    }

    public void setAllotTime(Date allotTime) {
        this.allotTime = allotTime;
    }

    public Integer getAllotNum() {
        return allotNum;
    }

    public void setAllotNum(Integer allotNum) {
        this.allotNum = allotNum;
    }

    private Boolean deleted;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public OrdersReceipt getOrdersReceipt() {
        return ordersReceipt;
    }

    public void setOrdersReceipt(OrdersReceipt ordersReceipt) {
        this.ordersReceipt = ordersReceipt;
    }

    public Integer getCutNum() {
        return cutNum;
    }

    public void setCutNum(Integer cutNum) {
        this.cutNum = cutNum;
    }

    public Boolean getIsCut() {
        return isCut;
    }

    public void setIsCut(Boolean isCut) {
        this.isCut = isCut;
    }

    public String getCutRemark() {
        return cutRemark;
    }

    public void setCutRemark(String cutRemark) {
        this.cutRemark = cutRemark;
    }

    public Integer getCutStatus() {
        return cutStatus;
    }

    public void setCutStatus(Integer cutStatus) {
        this.cutStatus = cutStatus;
    }


    public String getCancelCutRemark() {
        return cancelCutRemark;
    }

    public void setCancelCutRemark(String cancelCutRemark) {
        this.cancelCutRemark = cancelCutRemark;
    }

	public Integer getAllotStatus() {
		return allotStatus;
	}

	public void setAllotStatus(Integer allotStatus) {
		this.allotStatus = allotStatus;
	}

}
