package com.yuruizhi.hplusdemo.admin.service.logistics.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.logistics.DeliveryMethodDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryMethodDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryMethodDetailService;

/**
 * Created by ASUS on 2014/12/9.
 */
@Service
public class DeliveryMethodDetailServiceImpl extends BaseServiceImpl<DeliveryMethodDetail> implements DeliveryMethodDetailService {
   private DeliveryMethodDetailDao deliveryMethodDetailDao;

    @Autowired
    public void setAdminUserDao(DeliveryMethodDetailDao deliveryMethodDetailDao){
        this.deliveryMethodDetailDao = deliveryMethodDetailDao;
        setBaseDao(deliveryMethodDetailDao);
    }

    @Override
    public void deletedByMethodId(Long deliveryMethodId) {
        deliveryMethodDetailDao.deletedByMethodId(deliveryMethodId);
    }

    @Override
    public void deletedMethodId(Long deliveryMethodId) {
        deliveryMethodDetailDao.deletedMethodId(deliveryMethodId);
    }
}
