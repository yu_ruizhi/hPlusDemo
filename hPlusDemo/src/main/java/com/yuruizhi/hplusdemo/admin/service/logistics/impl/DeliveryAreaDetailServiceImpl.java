package com.yuruizhi.hplusdemo.admin.service.logistics.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuruizhi.hplusdemo.admin.dao.logistics.DeliveryAreaDetailDao;
import com.yuruizhi.hplusdemo.admin.entity.logistics.DeliveryAreaDetail;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.logistics.DeliveryAreaDetailService;

/**
 * Created by ASUS on 2014/12/9.
 */
@Service
public class DeliveryAreaDetailServiceImpl extends BaseServiceImpl<DeliveryAreaDetail> implements DeliveryAreaDetailService{

    private DeliveryAreaDetailDao deliveryAreaDetailDao;

    @Autowired
    public void setAdminUserDao(DeliveryAreaDetailDao deliveryAreaDetailDao){
        this.deliveryAreaDetailDao = deliveryAreaDetailDao;
        setBaseDao(deliveryAreaDetailDao);
    }

    @Override
    public void deletedByAreaId(Long fkid) {
        deliveryAreaDetailDao.deletedByAreaId(fkid);
    }

    @Override
    public void deletedByParent(Long deliveryAreaId) {
        deliveryAreaDetailDao.deletedByParent(deliveryAreaId);
    }
}
