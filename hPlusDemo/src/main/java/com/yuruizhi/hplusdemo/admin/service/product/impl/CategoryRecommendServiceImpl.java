/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.dao.product.CategoryRecommendDao;
import com.yuruizhi.hplusdemo.admin.entity.product.CategoryRecommend;
import com.yuruizhi.hplusdemo.admin.entity.product.Goods;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryRecommendService;

import java.util.Date;
import java.util.List;

/**
 * <p>名称: CategoryRecommendServiceImpl</p>
 * <p>说明: 分类推荐服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:49:19   
 * @version: 1.0
 */
@Service
public class CategoryRecommendServiceImpl extends BaseServiceImpl<CategoryRecommend> implements CategoryRecommendService {

    private CategoryRecommendDao categoryRecommendDao;
    @Autowired
    public void setCategoryDao(CategoryRecommendDao categoryRecommendDao) {
        super.setBaseDao(categoryRecommendDao);
        this.categoryRecommendDao = categoryRecommendDao;
    }
    @Override
    public void forSave(CategoryRecommend categoryRecommend) {
        categoryRecommendDao.save(categoryRecommend);
    }

    @Override
    public void updateForSave(CategoryRecommend categoryRecommend, Long[] goodsId, Long[] crId, String[] locations) {
        CategoryRecommend cc=find(crId[0]);
        this.deletec(cc.getCategory().getId());
        if(null!=goodsId&&goodsId.length>0){
            categoryRecommend.setGoods(new Goods());
            categoryRecommend.setDeleted(false);
            categoryRecommend.setCreatedDate(new Date());
            for(int i=0;i<goodsId.length;i++){
                categoryRecommend.getGoods().setId(goodsId[i]);
                categoryRecommend.setLocation(Integer.parseInt(locations[i]));
                categoryRecommend.setId(null);
               forSave(categoryRecommend);
            }
        }
    }

    @Override
    public List<CategoryRecommend> cselectAll(Long cid) {
        return categoryRecommendDao.cselectAll(cid);
    }

    @Override
    public void deletec(Long cid) {
        categoryRecommendDao.deletec(cid);
    }

    @Override
    public Integer isYou(Long cid) {
        return categoryRecommendDao.isYou(cid);
    }
}
