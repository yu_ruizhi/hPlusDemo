/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product;


import com.yuruizhi.hplusdemo.admin.entity.product.ProductImg;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: ProductImgService</p>
 * <p>说明: 产品图片服务 接口</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午10:07:30   
 * @version: 1.0
 */
public interface ProductImgService extends BaseService<ProductImg> {
	
	/**
	 * <p>名称：根据商品Id和排序获取图片</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param productId
	 * @param sort
	 * @return
	 */
	ProductImg findImgByProIdAndSort(Long productId , Integer sort);
	
	/**
	 * <p>名称：根据产品Id和排序获取图片</p> 
	 * <p>描述：TODO</p>
	 * @author：zuoyang
	 * @param goodsId
	 * @param sort
	 * @return
	 */
	ProductImg findImgByGoodsIdAndSort(Long goodsId , Integer sort);

}
