/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: GoodsMemberService.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.service.product
 * Date: 2016年6月16日下午3:31:24
 **/
package com.yuruizhi.hplusdemo.admin.service.product;

import java.util.List;

import com.yuruizhi.hplusdemo.admin.entity.product.GoodsMember;
import com.yuruizhi.hplusdemo.admin.service.BaseService;

/**
 * <p>名称: 预售商品截单权限Service</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：qinzhongliang
 * @date：2016年6月16日下午3:31:27   
 * @version: 1.0
 */
public interface GoodsMemberService extends BaseService<GoodsMember> {
	
	/**
	 * <p>名称：通过单品id查找记录</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param goodsId	单品id
	 * @return
	 */
	List<GoodsMember> findByGoodsId(Long goodsId);
	
	/**
	 * <p>名称：通过单品id、会员id查找记录</p> 
	 * <p>描述：</p>
	 * @author：qinzhongliang
	 * @param goodsId	单品id
	 * @param memberId	会员id
	 * @return
	 */
	List<GoodsMember> findByGoodsIdMemberId(Long goodsId, Long memberId);
}

