package com.yuruizhi.hplusdemo.admin.controller.perm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.perm.Role;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.google.common.collect.ImmutableMap;

/**
 * Created by ASUS on 2014/12/23. 后台用户管理
 */
@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/user")
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@RequestMapping("add")
	public void add() {
	}

	@RequestMapping("index")
	public void index() {
	}

	@RequestMapping("save")
	public String save(User user, String roleIds) {
		user.setLoginName(user.getLoginName().trim());
		user.setPassword(user.getPassword().trim());
		userService.saveAdminUser(user, roleIds);
		return redirectViewPath("index");
	}

	@RequestMapping("list")
	@ResponseBody
	public Page<User> list(Filters filters, Pageable pageable) {
		return userService.findPage(filters, pageable);
	}

	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		User user = userService.find(id);
		mav.addObject("user", user);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	@RequestMapping("update")
	public String update(User user, String roleIds) {
		user.setLoginName(user.getLoginName().trim());
		//user.setPassword(user.getPassword().trim());
		userService.updateAdminUser(user, roleIds);
		return redirectViewPath("index");
	}

	@RequestMapping("delete")
	@ResponseBody
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		userService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}

	@RequestMapping("deleteAdminUserByIds")
	@ResponseBody
	public boolean deleteAdminUserByIds(Long[] ids) {
		return userService.deleteAminUserByIds(ids);
	}

	@ResponseBody
	@RequestMapping("getRolesByUserId")
	public List<Role> getRolesByUserId(Long id) {
		List<Role> roles = new ArrayList<Role>();
		User user = userService.find(id);
		if (StringUtils.isEmpty(user)) {
			return roles;
		}

		roles = user.getRole();
		return roles;
	}

	@ResponseBody
	@RequestMapping("checkIsUserExists")
	public Map<String, Boolean> checkIsUserExists(String loginName) {
		return userService.checkIsUserExists(loginName);
	}

}
