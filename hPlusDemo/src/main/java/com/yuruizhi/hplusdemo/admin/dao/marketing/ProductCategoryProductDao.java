/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: ProductCategoryProductDao.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.dao.marketing
 * Date: 2016年4月20日 上午10:34:51 
 **/
package com.yuruizhi.hplusdemo.admin.dao.marketing;

import java.util.List;
import java.util.Map;

import com.yuruizhi.hplusdemo.admin.dao.BaseDao;
import com.yuruizhi.hplusdemo.admin.entity.marketing.ProductCategoryProduct;

/**
 * 
 * <p>名称: 分类商品-商品Dao</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：wuqi
 * @date：2016年4月20日 上午10:34:51   
 * @version: 1.0
 */
public interface ProductCategoryProductDao extends BaseDao<ProductCategoryProduct>{

	/**
	 * 
	 * <p>名称：根据分类推荐id获取商品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param categoryId 分类推荐id
	 * @return 返回商品信息
	 */
	List<ProductCategoryProduct> findByCategoryId(Long categoryId);
	
	/**
	 * 
	 * <p>名称：根据商品id及分类推荐id获取商品信息</p> 
	 * <p>描述：</p>
	 * @author：wuqi
	 * @param map 商品id及分类推荐id 
	 * @return 返回商品信息
	 */
	ProductCategoryProduct findByIdAndCategoryId(Map<String,Object> map);
}
