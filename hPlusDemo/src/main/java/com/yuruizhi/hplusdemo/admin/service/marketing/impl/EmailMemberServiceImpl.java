/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.marketing.impl;

import org.springframework.stereotype.Service;


import com.yuruizhi.hplusdemo.admin.entity.marketing.EmailMember;
import com.yuruizhi.hplusdemo.admin.service.BaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.marketing.EmailMemberService;

/**
 * <p>名称: EmailMemberServiceImpl</p>
 * <p>说明: 邮件会员关系服务接口实现</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月30日下午5:18:27   
 * @version: 1.0
 */
@Service
public class EmailMemberServiceImpl extends BaseServiceImpl<EmailMember> implements EmailMemberService {
}
