package com.yuruizhi.hplusdemo.admin.controller.cms;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.ponly.webbase.domain.Filters;
import org.ponly.webbase.domain.Page;
import org.ponly.webbase.domain.Pageable;

import com.yuruizhi.hplusdemo.admin.common.Constant;
import com.yuruizhi.hplusdemo.admin.controller.BaseController;
import com.yuruizhi.hplusdemo.admin.entity.perm.User;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.CategoryResource;
import com.yuruizhi.hplusdemo.admin.service.perm.UserService;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryResourceService;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import com.yuruizhi.hplusdemo.admin.util.UserUtils;
import com.google.common.collect.ImmutableMap;

@SuppressWarnings("deprecation")
@Controller
@RequestMapping(Constant.ADMIN_PREFIX + "/advertising")
public class AdvertisingController extends BaseController {
	@Autowired
	private UserService userService;
	@Autowired
	private CategoryResourceService categoryResourceService;

	@Autowired
	private CategoryService categoryService;

	/*
	 * 跳到首页
	 */
	@RequestMapping("index")
	private String index() {
		return getViewPath("index");
	}

	/*
	 * 跳到增加页面
	 */
	@RequestMapping("add")
	private String add(HttpServletRequest request, HttpServletResponse response) {
		List<Category> clist = categoryService.findAll();

		request.setAttribute("clist", clist);
		return getViewPath("/add");
	}

	@ResponseBody
	@RequestMapping("list")
	public Page<CategoryResource> list(Filters filetes, Pageable pageable) {
		Page<CategoryResource> cist = categoryResourceService.findPage(filetes, pageable);
		return cist;
	}

	/**
	 * 增加
	 */
	@RequestMapping("save")
	public String save(CategoryResource categoryResource) {
		Long currentUser = UserUtils.getCurrentUserId();
		User user = userService.find(currentUser);
		categoryResource.setCreatedBy(user.getLoginName());
		if (null == categoryResource.getId()) {
			categoryResourceService.save(categoryResource);
		} else {
			categoryResourceService.update(categoryResource);
		}
		return redirectViewPath("index");
	}

	/**
	 * 删除
	 */

	@ResponseBody
	@RequestMapping("delete")
	public Map<String, Object> delete(@RequestParam("id") Long[] id) {
		categoryResourceService.delete(id);
		return ImmutableMap.<String, Object> of("success", true);
	}

	/**
	 * 修改
	 */
	@RequestMapping("edit/{id}")
	public ModelAndView edit(@PathVariable(value = "id") Long id) {
		ModelAndView mav = new ModelAndView();
		CategoryResource categoryResource = categoryResourceService.find(id);
		List<Category> clist = categoryService.findAll();
		mav.addObject("clist", clist);
		mav.addObject("categoryResource", categoryResource);
		mav.setViewName(getViewPath("edit"));
		return mav;
	}

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(
				Date.class,
				new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), false)
        );
	}

}
