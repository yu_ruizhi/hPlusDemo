/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * Date: 2016年5月10日下午3:27:58
 **/
package com.yuruizhi.hplusdemo.admin.service.product.impl;

import com.yuruizhi.hplusdemo.admin.dao.product.CategoryDao;
import com.yuruizhi.hplusdemo.admin.entity.product.BrandCategory;
import com.yuruizhi.hplusdemo.admin.entity.product.Category;
import com.yuruizhi.hplusdemo.admin.entity.product.WorksCategory;
import com.yuruizhi.hplusdemo.admin.service.TreeNodeBaseServiceImpl;
import com.yuruizhi.hplusdemo.admin.service.product.CategoryService;
import org.ponly.webbase.service.ServiceException;
import org.ponly.webbase.util.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;

/**
 * <p>名称: CategoryServiceImpl</p>
 * <p>说明: 分类服务接口实现类</p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>  
 * 
 * @author：ChenGuang
 * @date：2016年5月31日上午9:51:01   
 * @version: 1.0
 */
@Service
public class CategoryServiceImpl extends TreeNodeBaseServiceImpl<Category> implements CategoryService {
    private static final String PATH_NAME_SEP = "/$";
    @Autowired
    private CategoryDao categoryDao;

    protected CategoryServiceImpl() {
        super(null);
    }

    @Autowired
    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    public void save(Category category) {
        super.save(category);

        // 保存分类品牌关系

        //update 2016-07-15 暂时商品分类不关丽娜品牌和分类
   /*     List<Brand> brands = category.getBrands();
        for (Brand brand : brands) {
            if (null == brand) {
                continue;
            }
            saveCategoryBrand(category.getId(), brand.getId());
        }
        
        //保存分类作品关系
        List<Works> works = category.getWorks();
        for (Works work :  works){
        	if(null == work){
        		continue;
        	}
        	saveCategoryWorks(category.getId(), work.getId());
        }*/
    }

    @Override
    public void update(Category category) {
        super.update(category);

        // 更新分类品牌关系
        // lushanshan 2016-07-15 性能优化，将品牌和作品隐藏
  /*      deleteBrandCateById(category.getId());
        for (Brand brand : category.getBrands()) {
            if (null == brand) {
                continue;
            }
            saveCategoryBrand(category.getId(), brand.getId());
        }
        
        //更新商品分类-作品关系
        deleteWorksCateByid(category.getId());
        for(Works work : category.getWorks()){
        	if(null==work){
        		continue;
        	}
        	saveCategoryWorks(category.getId(), work.getId());
        }*/
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Category doMountNode(Category node) {
        node = super.doMountNode(node);
        Category parent = node.getParent();
        String parentPathNames = parent.getPathNames();

        node.setPathNames(parentPathNames + PATH_NAME_SEP + node.getName());
        return node;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Category doUnmountNode(Category beforeStateNode) {
        return super.doUnmountNode(beforeStateNode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPathChanged(Category snapshot, Category current) {
        // 如果名字修改也需要修改后代
        return super.isPathChanged(snapshot, current) || ObjectUtils.nullSafeEquals(snapshot.getName(), current.getName());
    }

    @Override
    protected Map<String, Object> buildDescendantUpdateParams(Category snapshot, Category current) {
        Map<String, Object> paramsMap = super.buildDescendantUpdateParams(snapshot, current);
        paramsMap.put("_oldPathNames", snapshot.getPathNames() + PATH_NAME_SEP);
        paramsMap.put("_newPathNames", current.getPathNames() + PATH_NAME_SEP);
        return paramsMap;
    }

    @Override
    protected Category createDefaultRootNode() {
        Category root = super.createDefaultRootNode();
        root.setPathNames("");
        return root;
    }

    @Override
    public void saveCategoryBrand(Long categoryId, Long brandId) {
        categoryDao.saveCategoryBrand(categoryId, brandId);
    }

    @Override
    public List<BrandCategory> findBrandByCateId(Long id) {
        return categoryDao.findBrandByCateId(id);
    }

    @Override
    public List<Category> findWebsiteCategoryTree() {
//		List<Category> categories = findAll();
        List<Category> categories = categoryDao.findWebsiteCategories();
        Category root = new Category();
        root.setId(null);

        TreeUtils.buildTree(root, categories);
        return root.getChildren();
    }

    @Override
    public List<Category> findManyTopmost() {
        return categoryDao.findManyTopmost();
    }

    @Override
    public void deleteBrandCateById(Long bid) {
        categoryDao.deleteBrandCateByid(bid);
    }

	@Override
	public void saveCategoryWorks(Long categoryId, Long worksId) {
		categoryDao.saveCategoryWorks(categoryId, worksId);
	}

	@Override
	public List<WorksCategory> findWorksByCateId(Long id) {
		return categoryDao.findWorksByCateId(id);
	}

	@Override
	public void deleteWorksCateByid(Long wid) {
		categoryDao.deleteWorksCateByid(wid);
	}

    @Override
    public void delete(Long[] ids) {
       //判断选中的分类有没有包含子分类的，如果有不能删除
        StringBuilder stringBuilder=new StringBuilder("您选择的分类：");
        int count=0;
        for(int i=0;i<ids.length;i++){
            Integer childrenCount=categoryDao.childrenCount(ids[i]);
          if(null!=childrenCount&&childrenCount>0){
              count++;
              Category category=categoryDao.find(ids[i]);
              if(count>1){
                  stringBuilder.append("，");
              }
              stringBuilder.append(category.getName());
          }
        }
        stringBuilder.append("包含子分类不能删除，请先删除子分类");
        if(count!=0){
            throw new ServiceException(stringBuilder.toString());
        }
        super.delete(ids);
    }

	@Override
	public List<Category> findByName(String name) {
		return categoryDao.findByName(name);
	}

	@Override
	public List<Category> findByModelId(Long modelId) {
		return categoryDao.findByModelId(modelId);
	}

    @Override
    public List<Long> findChildrens(Long categoryId) {
        return categoryDao.findChildrens(categoryId);
    }
}
