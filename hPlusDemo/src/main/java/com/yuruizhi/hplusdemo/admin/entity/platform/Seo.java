/**
 * Copyright (c) 2016, YuRuizhi Inc. All rights reserved.
 *
 * ProjectName: hPlusDemo
 * FileName: Seo.java 
 * PackageName: com.yuruizhi.hplusdemo.admin.entity.platform
 * Date: 2016年4月23日上午10:21:15
 **/
package com.yuruizhi.hplusdemo.admin.entity.platform;

import com.yuruizhi.hplusdemo.admin.entity.BaseEntity;

/**
 * 
 * <p>名称: SEO实体类</p>
 * <p>说明: </p>
 * <p>修改记录：（修改日期 - 修改人 - 修改内容）</p>
 * 
 * @author：qinzhongliang
 * @date：2016年4月23日上午10:21:46
 * @version: 1.0
 */
public class Seo extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * @Fields name : 页面名称
	 * @Column NAME
	 */
	private String name;

	/**
	 * @Fields platform : 平台
	 * @Column PLATFORM
	 */
	private String platform;

	/**
	 * @Fields url : URL
	 * @Column URL
	 */
	private String url;

	/**
	 * @Fields title : SEO标题
	 * @Column TITLE
	 */
	private String title;

	/**
	 * @Fields keyword : SEO关键字
	 * @Column KEYWORD
	 */
	private String keyword;

	/**
	 * @Fields description : SEO描述
	 * @Column DESCRIBE
	 */
	private String description;

	/**
	 * @Fields deleted : 是否删除
	 * @Column DELETED
	 */
	private Boolean deleted = Boolean.FALSE;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}
